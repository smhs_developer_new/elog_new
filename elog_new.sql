-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 10, 2020 at 08:20 AM
-- Server version: 5.7.26
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elog_new`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `getstart_stoped_data`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `getstart_stoped_data`;
CREATE TABLE IF NOT EXISTS `getstart_stoped_data` (
`status` int(11)
,`area_code` varchar(50)
,`room_code` varchar(50)
,`status_name` varchar(250)
,`product_description` varchar(200)
,`document_no` varchar(50)
,`batch_no` varchar(50)
,`lot_no` varchar(50)
,`created_on` timestamp
,`operation_type` varchar(200)
,`created_by` varchar(250)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `getstatusrecord`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `getstatusrecord`;
CREATE TABLE IF NOT EXISTS `getstatusrecord` (
`warid` int(11)
,`header_id` int(11)
,`status` int(11)
,`area_code` varchar(50)
,`room_code` varchar(50)
,`status_name` varchar(250)
,`product_description` varchar(200)
,`document_no` varchar(50)
,`batch_no` varchar(50)
,`lot_no` varchar(50)
,`created_on` timestamp
,`operation_type` varchar(200)
,`doneby` varchar(200)
,`actionby` varchar(200)
,`roleid` varchar(50)
,`actiontime` datetime
,`type` varchar(50)
);

-- --------------------------------------------------------

--
-- Table structure for table `log_table`
--

DROP TABLE IF EXISTS `log_table`;
CREATE TABLE IF NOT EXISTS `log_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(500) DEFAULT NULL,
  `activity` varchar(500) DEFAULT NULL,
  `oldval` varchar(500) DEFAULT NULL,
  `newval` varchar(500) DEFAULT NULL,
  `created_by` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `log_tableaction`
--

DROP TABLE IF EXISTS `log_tableaction`;
CREATE TABLE IF NOT EXISTS `log_tableaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(200) DEFAULT NULL,
  `tablename` varchar(500) DEFAULT NULL,
  `tableid` int(11) DEFAULT NULL,
  `created_by` varchar(500) DEFAULT NULL,
  `created_on` varchar(500) DEFAULT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=514 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_tableaction`
--

INSERT INTO `log_tableaction` (`id`, `action`, `tablename`, `tableid`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(1, 'Updated', 'mst_activity', 270, 'Superadmin', '2020-01-15 10:53:15', 'Superadmin', '2020-03-13 13:47:30'),
(2, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(3, 'Updated', 'mst_drainpoint', 555, 'Superadmin', '2020-01-15 10:53:21', 'Superadmin', '2019-10-30 19:26:12'),
(4, 'Updated', 'mst_drainpoint', 556, 'Superadmin', '2020-01-15 10:53:21', 'Superadmin', '2019-10-30 19:26:12'),
(5, 'Updated', 'mst_plant', 4, 'Admin', '2020-01-15 10:53:28', 'Superadmin', '2020-04-03 17:50:36'),
(6, 'Updated', 'mst_product', 724, 'Superadmin', '2020-01-15 10:53:30', NULL, NULL),
(7, 'Updated', 'mst_product', 725, 'Superadmin', '2020-01-15 10:53:30', NULL, NULL),
(8, 'Updated', 'mst_product', 726, 'Superadmin', '2020-01-15 10:53:30', NULL, NULL),
(9, 'Updated', 'mst_product', 727, 'Superadmin', '2020-01-15 10:53:30', 'Superadmin', '2019-10-31 01:07:12'),
(10, 'Updated', 'mst_product', 728, 'Superadmin', '2020-01-15 10:53:30', 'Superadmin', '2019-10-31 01:07:12'),
(11, 'Updated', 'mst_product', 729, 'Superadmin', '2020-01-15 10:53:30', 'Superadmin', '2019-10-31 01:07:12'),
(12, 'Updated', 'mst_product', 730, 'Superadmin', '2020-01-15 10:53:30', 'Superadmin', '2019-10-31 01:07:12'),
(13, 'Updated', 'mst_product', 731, 'Superadmin', '2020-01-15 10:53:30', 'Superadmin', '2019-10-31 01:07:12'),
(14, 'Updated', 'mst_product', 732, 'Superadmin', '2020-01-15 10:53:30', 'Superadmin', '2019-10-31 01:07:12'),
(15, 'Updated', 'mst_product', 733, 'Superadmin', '2020-01-15 10:53:30', 'Superadmin', '2019-10-31 01:07:12'),
(16, 'Updated', 'mst_product', 734, 'Superadmin', '2020-01-15 10:53:30', 'Superadmin', '2019-10-31 01:07:12'),
(17, 'Updated', 'mst_product', 735, 'Superadmin', '2020-01-15 10:53:30', 'Superadmin', '2019-10-31 01:07:12'),
(18, 'Deleted', 'mst_room', 57, 'Superadmin', '2020-04-03 11:12:12', 'Superadmin', '2020-04-05 13:54:53'),
(19, 'Deleted', 'mst_room', 42, 'Superadmin', '2020-01-15 10:53:33', 'Superadmin', '2020-04-05 13:55:37'),
(20, 'Deleted', 'mst_block', 13, 'Superadmin', '2020-01-15 10:53:18', 'Superadmin', '2020-04-05 13:56:06'),
(21, 'Deleted', 'mst_block', 15, 'Superadmin', '2020-01-15 10:53:18', 'Superadmin', '2020-04-05 13:57:55'),
(22, 'Updated', 'mst_block', 15, 'Superadmin', '2020-01-15 10:53:18', 'Superadmin', '2020-04-05 13:57:55'),
(23, 'Updated', 'mst_block', 13, 'Superadmin', '2020-01-15 10:53:18', 'Superadmin', '2020-04-05 13:56:06'),
(24, 'Deleted', 'mst_block', 15, 'Superadmin', '2020-01-15 10:53:18', 'Superadmin', '2020-04-05 14:00:03'),
(25, 'Deleted', 'mst_block', 13, 'Superadmin', '2020-01-15 10:53:18', 'Superadmin', '2020-04-05 14:00:35'),
(26, 'Deleted', 'mst_block', 16, 'Superadmin', '2020-04-06 12:39:54', 'Superadmin', '2020-04-06 12:40:05'),
(27, 'Deleted', 'mst_block', 17, 'Superadmin', '2020-04-06 12:58:18', 'Superadmin', '2020-04-06 12:58:27'),
(28, 'Updated', 'mst_area', 30, 'Superadmin', '2020-01-15 10:53:17', 'Superadmin', '2020-04-07 07:19:28'),
(29, 'Updated', 'mst_area', 43, 'Superadmin', '2020-01-15 10:53:17', 'Superadmin', '2020-04-07 07:19:48'),
(30, 'Updated', 'mst_area', 41, 'Superadmin', '2020-01-15 10:53:17', 'Superadmin', '2020-04-07 15:45:10'),
(31, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(32, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(33, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(34, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(35, 'Updated', 'mst_equipment', 170, 'Superadmin', '2020-04-09 14:15:34', NULL, NULL),
(36, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(37, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(38, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(39, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(40, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(41, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(42, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(43, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(44, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(45, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(46, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(47, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(48, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(49, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(50, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(51, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(52, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(53, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(54, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(55, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(56, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(57, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(58, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(59, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(60, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(61, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(62, 'Updated', 'mst_block', 14, 'Superadmin', '2020-01-15 10:53:18', 'Superadmin', '2020-05-02 13:56:28'),
(63, 'Updated', 'mst_block', 14, 'Superadmin', '2020-01-15 10:53:18', 'Superadmin', '2020-05-02 13:56:41'),
(64, 'Updated', 'mst_block', 14, 'Superadmin', '2020-01-15 10:53:18', 'Superadmin', '2020-05-02 13:56:49'),
(65, 'Updated', 'mst_product', 732, 'Superadmin', '2020-01-15 10:53:30', 'Superadmin', '2020-05-02 14:09:21'),
(66, 'Updated', 'mst_department', 20, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:06'),
(67, 'Updated', 'mst_department', 21, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:06'),
(68, 'Updated', 'mst_department', 22, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:06'),
(69, 'Updated', 'mst_department', 23, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:06'),
(70, 'Updated', 'mst_department', 25, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:06'),
(71, 'Updated', 'mst_department', 26, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:06'),
(72, 'Updated', 'mst_department', 27, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:06'),
(73, 'Updated', 'mst_department', 28, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:06'),
(74, 'Updated', 'mst_department', 29, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:06'),
(75, 'Updated', 'mst_department', 30, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:06'),
(76, 'Updated', 'mst_department', 31, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:06'),
(77, 'Updated', 'mst_department', 32, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:06'),
(78, 'Updated', 'mst_department', 20, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:18'),
(79, 'Updated', 'mst_department', 21, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:18'),
(80, 'Updated', 'mst_department', 22, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:18'),
(81, 'Updated', 'mst_department', 23, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:18'),
(82, 'Updated', 'mst_department', 25, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:18'),
(83, 'Updated', 'mst_department', 26, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:18'),
(84, 'Updated', 'mst_department', 27, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:18'),
(85, 'Updated', 'mst_department', 28, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:18'),
(86, 'Updated', 'mst_department', 29, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:18'),
(87, 'Updated', 'mst_department', 30, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:18'),
(88, 'Updated', 'mst_department', 31, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:18'),
(89, 'Updated', 'mst_department', 32, 'Superadmin', '2020-01-15 10:53:19', 'Superadmin', '2020-05-04 09:12:18'),
(90, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(91, 'Updated', 'mst_equipment', 102, 'Admin', '2020-01-15 10:53:23', 'Null', NULL),
(92, 'Updated', 'mst_equipment', 125, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(93, 'Updated', 'mst_equipment', 126, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(94, 'Updated', 'mst_equipment', 127, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(95, 'Updated', 'mst_equipment', 128, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(96, 'Updated', 'mst_equipment', 129, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(97, 'Updated', 'mst_equipment', 149, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 14:02:03'),
(98, 'Updated', 'mst_equipment', 150, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(99, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(100, 'Updated', 'mst_equipment', 152, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:56'),
(101, 'Updated', 'mst_equipment', 153, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(102, 'Updated', 'mst_equipment', 154, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(103, 'Updated', 'mst_equipment', 155, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(104, 'Updated', 'mst_equipment', 156, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(105, 'Updated', 'mst_equipment', 157, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(106, 'Updated', 'mst_equipment', 158, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(107, 'Updated', 'mst_equipment', 159, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(108, 'Updated', 'mst_equipment', 160, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(109, 'Updated', 'mst_equipment', 161, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(110, 'Updated', 'mst_equipment', 162, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:13'),
(111, 'Updated', 'mst_equipment', 163, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(112, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(113, 'Updated', 'mst_equipment', 165, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:37'),
(114, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(115, 'Updated', 'mst_equipment', 167, 'Superadmin', '2020-01-15 11:53:59', 'Superadmin', '2020-01-15 12:08:12'),
(116, 'Updated', 'mst_equipment', 168, 'Superadmin', '2020-02-24 13:54:36', 'Superadmin', '2020-02-24 13:56:12'),
(117, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(118, 'Updated', 'mst_equipment', 170, 'Superadmin', '2020-04-09 14:15:34', NULL, NULL),
(119, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(120, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(121, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(122, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(123, 'Updated', 'mst_equipment', 102, 'Admin', '2020-01-15 10:53:23', 'Null', NULL),
(124, 'Updated', 'mst_equipment', 125, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(125, 'Updated', 'mst_equipment', 126, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(126, 'Updated', 'mst_equipment', 127, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(127, 'Updated', 'mst_equipment', 128, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(128, 'Updated', 'mst_equipment', 129, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(129, 'Updated', 'mst_equipment', 149, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 14:02:03'),
(130, 'Updated', 'mst_equipment', 150, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(131, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(132, 'Updated', 'mst_equipment', 152, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:56'),
(133, 'Updated', 'mst_equipment', 153, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(134, 'Updated', 'mst_equipment', 154, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(135, 'Updated', 'mst_equipment', 155, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(136, 'Updated', 'mst_equipment', 156, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(137, 'Updated', 'mst_equipment', 157, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(138, 'Updated', 'mst_equipment', 158, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(139, 'Updated', 'mst_equipment', 159, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(140, 'Updated', 'mst_equipment', 160, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(141, 'Updated', 'mst_equipment', 161, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(142, 'Updated', 'mst_equipment', 162, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:13'),
(143, 'Updated', 'mst_equipment', 163, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(144, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(145, 'Updated', 'mst_equipment', 165, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:37'),
(146, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(147, 'Updated', 'mst_equipment', 167, 'Superadmin', '2020-01-15 11:53:59', 'Superadmin', '2020-01-15 12:08:12'),
(148, 'Updated', 'mst_equipment', 168, 'Superadmin', '2020-02-24 13:54:36', 'Superadmin', '2020-02-24 13:56:12'),
(149, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(150, 'Updated', 'mst_equipment', 170, 'Superadmin', '2020-04-09 14:15:34', NULL, NULL),
(151, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(152, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(153, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(154, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(155, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(156, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(157, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(158, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(159, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(160, 'Updated', 'mst_equipment', 102, 'Admin', '2020-01-15 10:53:23', 'Null', NULL),
(161, 'Updated', 'mst_equipment', 125, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(162, 'Updated', 'mst_equipment', 126, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(163, 'Updated', 'mst_equipment', 127, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(164, 'Updated', 'mst_equipment', 128, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(165, 'Updated', 'mst_equipment', 129, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(166, 'Updated', 'mst_equipment', 149, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 14:02:03'),
(167, 'Updated', 'mst_equipment', 150, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(168, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(169, 'Updated', 'mst_equipment', 152, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:56'),
(170, 'Updated', 'mst_equipment', 153, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(171, 'Updated', 'mst_equipment', 154, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(172, 'Updated', 'mst_equipment', 155, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(173, 'Updated', 'mst_equipment', 156, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(174, 'Updated', 'mst_equipment', 157, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(175, 'Updated', 'mst_equipment', 158, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(176, 'Updated', 'mst_equipment', 159, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(177, 'Updated', 'mst_equipment', 160, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(178, 'Updated', 'mst_equipment', 161, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(179, 'Updated', 'mst_equipment', 162, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:13'),
(180, 'Updated', 'mst_equipment', 163, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(181, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(182, 'Updated', 'mst_equipment', 165, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:37'),
(183, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(184, 'Updated', 'mst_equipment', 167, 'Superadmin', '2020-01-15 11:53:59', 'Superadmin', '2020-01-15 12:08:12'),
(185, 'Updated', 'mst_equipment', 168, 'Superadmin', '2020-02-24 13:54:36', 'Superadmin', '2020-02-24 13:56:12'),
(186, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(187, 'Updated', 'mst_equipment', 170, 'Superadmin', '2020-04-09 14:15:34', NULL, NULL),
(188, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(189, 'Updated', 'mst_equipment', 102, 'Admin', '2020-01-15 10:53:23', 'Null', NULL),
(190, 'Updated', 'mst_equipment', 125, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(191, 'Updated', 'mst_equipment', 126, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(192, 'Updated', 'mst_equipment', 127, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(193, 'Updated', 'mst_equipment', 128, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(194, 'Updated', 'mst_equipment', 129, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(195, 'Updated', 'mst_equipment', 149, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 14:02:03'),
(196, 'Updated', 'mst_equipment', 150, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(197, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(198, 'Updated', 'mst_equipment', 152, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:56'),
(199, 'Updated', 'mst_equipment', 153, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(200, 'Updated', 'mst_equipment', 154, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(201, 'Updated', 'mst_equipment', 155, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(202, 'Updated', 'mst_equipment', 156, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(203, 'Updated', 'mst_equipment', 157, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(204, 'Updated', 'mst_equipment', 158, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(205, 'Updated', 'mst_equipment', 159, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(206, 'Updated', 'mst_equipment', 160, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(207, 'Updated', 'mst_equipment', 161, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(208, 'Updated', 'mst_equipment', 162, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:13'),
(209, 'Updated', 'mst_equipment', 163, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(210, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(211, 'Updated', 'mst_equipment', 165, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:37'),
(212, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(213, 'Updated', 'mst_equipment', 167, 'Superadmin', '2020-01-15 11:53:59', 'Superadmin', '2020-01-15 12:08:12'),
(214, 'Updated', 'mst_equipment', 168, 'Superadmin', '2020-02-24 13:54:36', 'Superadmin', '2020-02-24 13:56:12'),
(215, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(216, 'Updated', 'mst_equipment', 170, 'Superadmin', '2020-04-09 14:15:34', NULL, NULL),
(217, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(218, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(219, 'Updated', 'mst_equipment', 102, 'Admin', '2020-01-15 10:53:23', 'Null', NULL),
(220, 'Updated', 'mst_equipment', 125, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(221, 'Updated', 'mst_equipment', 126, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(222, 'Updated', 'mst_equipment', 127, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(223, 'Updated', 'mst_equipment', 128, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(224, 'Updated', 'mst_equipment', 129, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(225, 'Updated', 'mst_equipment', 149, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 14:02:03'),
(226, 'Updated', 'mst_equipment', 150, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(227, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(228, 'Updated', 'mst_equipment', 152, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:56'),
(229, 'Updated', 'mst_equipment', 153, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(230, 'Updated', 'mst_equipment', 154, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(231, 'Updated', 'mst_equipment', 155, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(232, 'Updated', 'mst_equipment', 156, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(233, 'Updated', 'mst_equipment', 157, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(234, 'Updated', 'mst_equipment', 158, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(235, 'Updated', 'mst_equipment', 159, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(236, 'Updated', 'mst_equipment', 160, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(237, 'Updated', 'mst_equipment', 161, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(238, 'Updated', 'mst_equipment', 162, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:13'),
(239, 'Updated', 'mst_equipment', 163, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(240, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(241, 'Updated', 'mst_equipment', 165, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:37'),
(242, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(243, 'Updated', 'mst_equipment', 167, 'Superadmin', '2020-01-15 11:53:59', 'Superadmin', '2020-01-15 12:08:12'),
(244, 'Updated', 'mst_equipment', 168, 'Superadmin', '2020-02-24 13:54:36', 'Superadmin', '2020-02-24 13:56:12'),
(245, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(246, 'Updated', 'mst_equipment', 170, 'Superadmin', '2020-04-09 14:15:34', NULL, NULL),
(247, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(248, 'Updated', 'mst_equipment', 102, 'Admin', '2020-01-15 10:53:23', 'Null', NULL),
(249, 'Updated', 'mst_equipment', 125, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(250, 'Updated', 'mst_equipment', 126, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(251, 'Updated', 'mst_equipment', 127, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(252, 'Updated', 'mst_equipment', 128, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(253, 'Updated', 'mst_equipment', 129, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(254, 'Updated', 'mst_equipment', 149, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 14:02:03'),
(255, 'Updated', 'mst_equipment', 150, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(256, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(257, 'Updated', 'mst_equipment', 152, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:56'),
(258, 'Updated', 'mst_equipment', 153, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(259, 'Updated', 'mst_equipment', 154, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(260, 'Updated', 'mst_equipment', 155, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(261, 'Updated', 'mst_equipment', 156, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(262, 'Updated', 'mst_equipment', 157, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(263, 'Updated', 'mst_equipment', 158, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(264, 'Updated', 'mst_equipment', 159, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(265, 'Updated', 'mst_equipment', 160, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(266, 'Updated', 'mst_equipment', 161, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(267, 'Updated', 'mst_equipment', 162, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:13'),
(268, 'Updated', 'mst_equipment', 163, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(269, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(270, 'Updated', 'mst_equipment', 165, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:37'),
(271, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(272, 'Updated', 'mst_equipment', 167, 'Superadmin', '2020-01-15 11:53:59', 'Superadmin', '2020-01-15 12:08:12'),
(273, 'Updated', 'mst_equipment', 168, 'Superadmin', '2020-02-24 13:54:36', 'Superadmin', '2020-02-24 13:56:12'),
(274, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(275, 'Updated', 'mst_equipment', 170, 'Superadmin', '2020-04-09 14:15:34', NULL, NULL),
(276, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(277, 'Updated', 'mst_equipment', 102, 'Admin', '2020-01-15 10:53:23', 'Null', NULL),
(278, 'Updated', 'mst_equipment', 125, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(279, 'Updated', 'mst_equipment', 126, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(280, 'Updated', 'mst_equipment', 127, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(281, 'Updated', 'mst_equipment', 128, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(282, 'Updated', 'mst_equipment', 129, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(283, 'Updated', 'mst_equipment', 149, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 14:02:03'),
(284, 'Updated', 'mst_equipment', 150, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(285, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(286, 'Updated', 'mst_equipment', 152, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:56'),
(287, 'Updated', 'mst_equipment', 153, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(288, 'Updated', 'mst_equipment', 154, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(289, 'Updated', 'mst_equipment', 155, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(290, 'Updated', 'mst_equipment', 156, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(291, 'Updated', 'mst_equipment', 157, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(292, 'Updated', 'mst_equipment', 158, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(293, 'Updated', 'mst_equipment', 159, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(294, 'Updated', 'mst_equipment', 160, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(295, 'Updated', 'mst_equipment', 161, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(296, 'Updated', 'mst_equipment', 162, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:13'),
(297, 'Updated', 'mst_equipment', 163, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(298, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(299, 'Updated', 'mst_equipment', 165, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:37'),
(300, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(301, 'Updated', 'mst_equipment', 167, 'Superadmin', '2020-01-15 11:53:59', 'Superadmin', '2020-01-15 12:08:12'),
(302, 'Updated', 'mst_equipment', 168, 'Superadmin', '2020-02-24 13:54:36', 'Superadmin', '2020-02-24 13:56:12'),
(303, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(304, 'Updated', 'mst_equipment', 170, 'Superadmin', '2020-04-09 14:15:34', NULL, NULL),
(305, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(306, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(307, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(308, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(309, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(310, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(311, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(312, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(313, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(314, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(315, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(316, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(317, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(318, 'Updated', 'mst_equipment', 102, 'Admin', '2020-01-15 10:53:23', 'Null', NULL),
(319, 'Updated', 'mst_equipment', 125, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(320, 'Updated', 'mst_equipment', 126, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(321, 'Updated', 'mst_equipment', 127, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(322, 'Updated', 'mst_equipment', 128, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(323, 'Updated', 'mst_equipment', 129, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(324, 'Updated', 'mst_equipment', 149, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 14:02:03'),
(325, 'Updated', 'mst_equipment', 150, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(326, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(327, 'Updated', 'mst_equipment', 152, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:56'),
(328, 'Updated', 'mst_equipment', 153, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(329, 'Updated', 'mst_equipment', 154, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(330, 'Updated', 'mst_equipment', 155, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(331, 'Updated', 'mst_equipment', 156, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(332, 'Updated', 'mst_equipment', 157, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(333, 'Updated', 'mst_equipment', 158, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(334, 'Updated', 'mst_equipment', 159, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(335, 'Updated', 'mst_equipment', 160, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(336, 'Updated', 'mst_equipment', 161, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(337, 'Updated', 'mst_equipment', 162, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:13'),
(338, 'Updated', 'mst_equipment', 163, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(339, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(340, 'Updated', 'mst_equipment', 165, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:37'),
(341, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(342, 'Updated', 'mst_equipment', 167, 'Superadmin', '2020-01-15 11:53:59', 'Superadmin', '2020-01-15 12:08:12'),
(343, 'Updated', 'mst_equipment', 168, 'Superadmin', '2020-02-24 13:54:36', 'Superadmin', '2020-02-24 13:56:12'),
(344, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(345, 'Updated', 'mst_equipment', 170, 'Superadmin', '2020-04-09 14:15:34', NULL, NULL),
(346, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(347, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(348, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(349, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(350, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(351, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(352, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(353, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(354, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(355, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(356, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(357, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(358, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(359, 'Updated', 'mst_equipment', 102, 'Admin', '2020-01-15 10:53:23', 'Null', NULL),
(360, 'Updated', 'mst_equipment', 125, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(361, 'Updated', 'mst_equipment', 126, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(362, 'Updated', 'mst_equipment', 127, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(363, 'Updated', 'mst_equipment', 128, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(364, 'Updated', 'mst_equipment', 129, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(365, 'Updated', 'mst_equipment', 149, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 14:02:03'),
(366, 'Updated', 'mst_equipment', 150, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(367, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(368, 'Updated', 'mst_equipment', 152, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:56'),
(369, 'Updated', 'mst_equipment', 153, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(370, 'Updated', 'mst_equipment', 154, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(371, 'Updated', 'mst_equipment', 155, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(372, 'Updated', 'mst_equipment', 156, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(373, 'Updated', 'mst_equipment', 157, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(374, 'Updated', 'mst_equipment', 158, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(375, 'Updated', 'mst_equipment', 159, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(376, 'Updated', 'mst_equipment', 160, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(377, 'Updated', 'mst_equipment', 161, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(378, 'Updated', 'mst_equipment', 162, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:13'),
(379, 'Updated', 'mst_equipment', 163, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(380, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(381, 'Updated', 'mst_equipment', 165, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:37'),
(382, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(383, 'Updated', 'mst_equipment', 167, 'Superadmin', '2020-01-15 11:53:59', 'Superadmin', '2020-01-15 12:08:12'),
(384, 'Updated', 'mst_equipment', 168, 'Superadmin', '2020-02-24 13:54:36', 'Superadmin', '2020-02-24 13:56:12'),
(385, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(386, 'Updated', 'mst_equipment', 170, 'Superadmin', '2020-04-09 14:15:34', NULL, NULL),
(387, 'Updated', 'mst_equipment', 171, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(388, 'Updated', 'mst_equipment', 172, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(389, 'Updated', 'mst_equipment', 173, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(390, 'Updated', 'mst_equipment', 174, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(391, 'Updated', 'mst_equipment', 175, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(392, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(393, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(394, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(395, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(396, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(397, 'Updated', 'mst_equipment', 171, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(398, 'Updated', 'mst_equipment', 172, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(399, 'Updated', 'mst_equipment', 172, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(400, 'Updated', 'mst_equipment', 173, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(401, 'Updated', 'mst_equipment', 173, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(402, 'Updated', 'mst_equipment', 173, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(403, 'Updated', 'mst_equipment', 173, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(404, 'Updated', 'mst_equipment', 175, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(405, 'Updated', 'mst_equipment', 175, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(406, 'Updated', 'mst_equipment', 175, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(407, 'Updated', 'mst_equipment', 175, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(408, 'Updated', 'mst_equipment', 175, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(409, 'Updated', 'mst_equipment', 175, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(410, 'Updated', 'mst_equipment', 175, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(411, 'Updated', 'mst_equipment', 175, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(412, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(413, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(414, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(415, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(416, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(417, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(418, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(419, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(420, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(421, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(422, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(423, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(424, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(425, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(426, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(427, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(428, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(429, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(430, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(431, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(432, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(433, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(434, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(435, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(436, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(437, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(438, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(439, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(440, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(441, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(442, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(443, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(444, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(445, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(446, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(447, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(448, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(449, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(450, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(451, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(452, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(453, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(454, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(455, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(456, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(457, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(458, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(459, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(460, 'Updated', 'mst_equipment', 102, 'Admin', '2020-01-15 10:53:23', 'Null', NULL),
(461, 'Updated', 'mst_equipment', 125, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(462, 'Updated', 'mst_equipment', 126, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(463, 'Updated', 'mst_equipment', 127, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(464, 'Updated', 'mst_equipment', 128, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(465, 'Updated', 'mst_equipment', 129, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(466, 'Updated', 'mst_equipment', 149, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 14:02:03'),
(467, 'Updated', 'mst_equipment', 150, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(468, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(469, 'Updated', 'mst_equipment', 152, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:56'),
(470, 'Updated', 'mst_equipment', 153, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57');
INSERT INTO `log_tableaction` (`id`, `action`, `tablename`, `tableid`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(471, 'Updated', 'mst_equipment', 154, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(472, 'Updated', 'mst_equipment', 155, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(473, 'Updated', 'mst_equipment', 156, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(474, 'Updated', 'mst_equipment', 157, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(475, 'Updated', 'mst_equipment', 158, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(476, 'Updated', 'mst_equipment', 159, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(477, 'Updated', 'mst_equipment', 160, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(478, 'Updated', 'mst_equipment', 161, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(479, 'Updated', 'mst_equipment', 162, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:13'),
(480, 'Updated', 'mst_equipment', 163, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(481, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(482, 'Updated', 'mst_equipment', 165, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2020-01-15 11:51:37'),
(483, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(484, 'Updated', 'mst_equipment', 167, 'Superadmin', '2020-01-15 11:53:59', 'Superadmin', '2020-01-15 12:08:12'),
(485, 'Updated', 'mst_equipment', 168, 'Superadmin', '2020-02-24 13:54:36', 'Superadmin', '2020-02-24 13:56:12'),
(486, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(487, 'Updated', 'mst_equipment', 170, 'Superadmin', '2020-04-09 14:15:34', NULL, NULL),
(488, 'Updated', 'mst_equipment', 171, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(489, 'Updated', 'mst_equipment', 172, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(490, 'Updated', 'mst_equipment', 173, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(491, 'Updated', 'mst_equipment', 174, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-10-31 00:44:57'),
(492, 'Updated', 'mst_equipment', 175, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(493, 'Updated', 'mst_equipment', 176, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(494, 'Updated', 'mst_equipment', 177, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(495, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(496, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(497, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(498, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(499, 'Updated', 'mst_equipment', 171, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(500, 'Updated', 'mst_equipment', 172, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(501, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(502, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(503, 'Updated', 'mst_equipment', 171, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(504, 'Updated', 'mst_equipment', 172, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(505, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(506, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(507, 'Updated', 'mst_equipment', 164, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-12-06 22:29:14'),
(508, 'Updated', 'mst_equipment', 151, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(509, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(510, 'Updated', 'mst_equipment', 171, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53'),
(511, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(512, 'Updated', 'mst_equipment', 166, 'Superadmin', '2020-01-15 10:53:23', NULL, NULL),
(513, 'Updated', 'mst_equipment', 169, 'Superadmin', '2020-01-15 10:53:23', 'Superadmin', '2019-11-26 12:55:53');

-- --------------------------------------------------------

--
-- Table structure for table `log_tablechanges`
--

DROP TABLE IF EXISTS `log_tablechanges`;
CREATE TABLE IF NOT EXISTS `log_tablechanges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log_id` int(11) DEFAULT NULL,
  `fieldname` varchar(500) DEFAULT NULL,
  `oldvalue` text,
  `newvalue` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_tablechanges`
--

INSERT INTO `log_tablechanges` (`id`, `log_id`, `fieldname`, `oldvalue`, `newvalue`) VALUES
(1, 1, 'activity_code', '2', '2222'),
(2, 1, 'activity_name', 'Type-A', 'Type-AA'),
(3, 1, 'activityfunc_tocall', '', 'test'),
(4, 1, 'activity_remark', 'First time plant set up', 'First time plant set up test'),
(5, 2, 'equipment_code', 'CPD-032-A', 'CPD-032-AA'),
(6, 5, 'number_of_blocks', '4', '5'),
(7, 18, 'is_active', '', '\0'),
(8, 19, 'is_active', '', '\0'),
(9, 20, 'is_active', '1', '0'),
(10, 21, 'is_active', '1', '0'),
(11, 24, 'is_active', '1', '0'),
(12, 25, 'is_active', '1', '0'),
(13, 26, 'is_active', '1', '0'),
(14, 27, 'is_active', '1', '0'),
(15, 35, 'room_code', 'TAA-79', 'TAA-78');

-- --------------------------------------------------------

--
-- Table structure for table `mst_accessory_criticalparts`
--

DROP TABLE IF EXISTS `mst_accessory_criticalparts`;
CREATE TABLE IF NOT EXISTS `mst_accessory_criticalparts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `part_code` varchar(50) DEFAULT NULL,
  `part_name` varchar(200) DEFAULT NULL,
  `part_description` varchar(2000) DEFAULT NULL,
  `part_type` varchar(200) DEFAULT NULL,
  `part_remark` varchar(1000) DEFAULT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_actions`
--

DROP TABLE IF EXISTS `mst_actions`;
CREATE TABLE IF NOT EXISTS `mst_actions` (
  `id` int(11) NOT NULL,
  `actions` varchar(300) NOT NULL,
  `created_by` varchar(300) NOT NULL DEFAULT 'Admin',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(300) NOT NULL DEFAULT 'Admin',
  `modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_activity`
--

DROP TABLE IF EXISTS `mst_activity`;
CREATE TABLE IF NOT EXISTS `mst_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activity_code` varchar(50) DEFAULT NULL,
  `activity_name` varchar(500) DEFAULT NULL,
  `activitytype_id` bigint(20) DEFAULT NULL,
  `activity_type` varchar(500) DEFAULT NULL,
  `activityfunc_tocall` varchar(256) NOT NULL,
  `activity_icon` varchar(500) DEFAULT NULL,
  `activity_nature` varchar(2000) DEFAULT NULL,
  `activity_duration` varchar(200) DEFAULT NULL,
  `activity_remark` varchar(2000) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2610 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_activity`
--

INSERT INTO `mst_activity` (`id`, `activity_code`, `activity_name`, `activitytype_id`, `activity_type`, `activityfunc_tocall`, `activity_icon`, `activity_nature`, `activity_duration`, `activity_remark`, `area_code`, `created_by`, `created_on`, `modified_by`, `modified_on`, `is_active`) VALUES
(107, '99', 'First Time Plant Set-up', 15, 'First Time Plant Set-up', 'No function', '031f688cddc9b0171bdb1387b9ac4f77.png', NULL, NULL, 'First time plant set up', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'0'),
(114, '1002', 'Create Block ', 15, 'Create Plant data', 'Admin/mstblock', '919e0e83e4a2fb5d37bbd8b936daf838.png', NULL, NULL, 'Post Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(115, '1003', 'Create Area ', 15, 'Create Plant data', 'Admin/mstArea', '89ab2a5fd8cec0255b5650c3257fcbe6.png', NULL, NULL, 'Post Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(116, '1004', 'Create Room ', 15, 'Create Plant data', 'Admin/mstRoom', '976b96687d66eab0ca8584933801215b.png', NULL, NULL, 'Post Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-09 16:44:07', b'1'),
(119, '1007', 'Create Activity Area Mapping', 15, 'Create Plant data', 'Admin/activityMapping', 'aaf0dfaf83223fb1f8d5d280e1236516.png', NULL, NULL, 'Post Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(123, '1008', 'Create Role Activity Mapping', 15, 'Create Plant data', 'Activity/activityMapping', '6f66cc3fff5499b0bfc5fab1fc2fe33c.png', NULL, NULL, 'Post Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(124, '1013.5', 'Create Employee ', 15, 'Create Plant data', 'Admin/AddEmployee', 'e5ab0be6756f03bb83256edd7d8a9990.png', NULL, NULL, 'Post Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(125, '1004.6', 'Create Equipment', 15, 'Create Plant data', 'Admin/mstEquipment', '3191f7ca1a6d601a9f6992c5a1f4ed32.png', NULL, NULL, 'Post Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(126, '1003.7', 'Create Drainpoint', 15, 'Create Plant data', 'Admin/mstDrainpoint', '701dc7a7e72d0724edf82cbdd809dde6.png', NULL, NULL, 'Post Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(127, '1004.5', 'Create SOP ', 15, 'Create Plant data', 'Admin/AddSop', '02a26fe96bdb87d3819f80ee65b25e38.png', NULL, NULL, 'Post Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(128, '1013', 'Create Workflow ', 15, 'Create Plant data', 'user/workflow', 'aad754af7cd288b8e226ddc1f931353b.png', NULL, NULL, 'Post Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(129, '1014', 'Add & Edit Plant Data', 16, 'Create Master Data', 'admin/mstPlantview', '8c45bcdc81c95e0672529e93fe6ada70.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-09 16:07:43', b'1'),
(130, '1015', 'Add & Edit Block Data', 16, 'Create Master Data', 'Admin/mstBlock', 'ed9ba8473586a551464a5a2ebb657d6f.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-09 16:07:28', b'1'),
(131, '1016', 'Add & Edit Area Data', 16, 'Create Master Data', 'Admin/mstArea', '27bbab95e65db3bf146672052ef990ef.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-09 16:07:01', b'1'),
(132, '1017', 'Add & Edit Room Data', 16, 'Create Master Data', 'Admin/mstRoom', 'a0915a7cbabc2a40933c7039af94c85b.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-09 16:06:42', b'1'),
(133, '1018', 'Add & Edit Roleid Workflow Mapping Data', 16, 'Create Master Data', 'Admin/AddRolestatusMapping', '661ecc3079b9f54993992288df45c4f4.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-09 16:06:27', b'0'),
(134, '1019', 'Add & Edit Activity Type Data', 16, 'Create Master Data', 'Admin/AddCategory', 'bb0ac4d47751578513150e6ab72a2a63.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-09 16:06:08', b'1'),
(135, '1020', 'Add & Edit Activity Data', 16, 'Create Master Data', 'Admin/AddActivity', 'b277c4eb7d721bd94e2cbed15a1d92af.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-09 16:05:45', b'1'),
(136, '1021', 'Add & Edit Activity Role Data', 16, 'Create Master Data', 'Activity/activityMapping', '957643a7da768636a49f79e02eb01509.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-09 16:05:30', b'1'),
(137, '1022', 'Add & Edit Employee Data', 16, 'Create Master Data', 'Admin/AddEmployee', '895e3bae54587fd1e81770f0fffa1315.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-09 16:05:09', b'1'),
(138, '1023', 'Add & Edit Equipment Data', 16, 'Create Master Data', 'Admin/mstEquipment', '011f9a40ff8efad397534ef772e91d39.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-09 16:04:53', b'1'),
(139, '1024', 'Add & Edit Drainpoint Data', 16, 'Create Master Data', 'Admin/mstDrainpoint', '4360c30a96c59c55efd63582920c7ad3.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-09 16:04:28', b'1'),
(140, '1025', 'Add & Edit SOP Data', 16, 'Create Master Data', 'Admin/AddSop', '6bae0d82adbf7306e6f468092dbeb6d2.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-09 16:04:10', b'1'),
(141, '1026', 'Add & Edit Workflow Data', 16, 'Create Master Data', 'user/workflow', '60ea8f6fc6d969b611f7e086bf751125.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(142, '1027', 'View & Edit Role Status Data', 16, 'Create Master Data', 'Admin/Add_Status', 'b27e4418a992a248a9dda0f13a49ba46.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-09 16:12:52', b'0'),
(143, '1028', 'View & Edit Document Data', 16, 'Create Master Data', 'Function to call', '3fe03625cecc4fafe159a6f8f76aa3f0.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-09 16:12:30', b'0'),
(144, '1029', 'View & Edit Sub-block', 16, 'Create Master Data', 'Admin/mstDepartment', 'e2850b9bc54e2bccb86b2612a64b37fa.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(145, '1030', 'View & Edit Role Data', 16, 'Create Master Data', 'Admin/Addrole', 'ba5bb5bd5979c3cba2ab5958176d4455.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(146, '1031', 'View & Edit Product Data', 16, 'Create Master Data', 'Admin/AddProduct', 'edc7eb582bace89a031cb69f58f42628.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(147, '1032', 'View & Edit Message Data', 16, 'Create Master Data', 'Admin/AddMsg', '41969f327bae40558e01252dd5bee098.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'0'),
(148, '1033', 'View & Edit Solution Data', 16, 'Create Master Data', 'Admin/AddSolution', 'a8a76d856f144a7dc0212bd27648e25f.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(149, '1034', 'View & Edit Batch Data', 16, 'Create Master Data', 'Function to call', '0070e271877672b8452f0be1a65f8f63.png', NULL, NULL, 'Pre-Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'0'),
(150, '129', 'Create Master Data', 16, 'Create Master Data', 'Function to call', NULL, NULL, NULL, NULL, 'A-000', NULL, '2020-01-15 05:23:15', NULL, NULL, b'0'),
(151, '39', 'QA Approval Activity', 21, 'QA Approvals', 'User/AS', '8630c3fc2b1e936a3cb331edb1ad335d.png', NULL, NULL, 'QA Approvals Activity', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-10 13:59:04', b'1'),
(152, '1001', 'Create Plant ', 15, 'Create Plant data', 'Admin/mstPlant', '908d2d5b2f19cc5b694b94687ff4e29a.png', NULL, NULL, 'Create plant data', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(153, '1004.8', 'Create Solution ', 15, 'Create Plant data', 'Admin/AddSolution', 'fe8d14400db960d23d216136bc3c9caf.png', NULL, NULL, 'Testing', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-10 16:47:00', b'1'),
(247, '1002.5', 'Create Sub-Block ', 15, 'Create Sub-Block data', 'Admin/mstDepartment', '919e0e83e4a2fb5d37bbd8b936daf838.png', NULL, NULL, 'Post Loaded', 'A-000', 'Superadmin', '2020-01-15 05:23:15', NULL, NULL, b'1'),
(265, '37', 'Reports', 17, 'Reports', '', 'd37a87bbe5a683fd7c1b8a0a86bc8e99.png', NULL, NULL, 'First time plant set up', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', NULL, b'1'),
(269, '1', 'Operation', 14, 'Production', '', 'd23c2919d26a15f67aecc997ed530d80.png', NULL, NULL, 'First time plant set up', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', NULL, b'1'),
(270, '2222', 'Type-AA', 14, 'Production', 'test', '61f16e92dd1765ed93d856c0d9b3cecd.png', NULL, NULL, 'First time plant set up test', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2020-03-13 08:17:30', b'1'),
(271, '3', 'Type-B', 14, 'Production', '', '286bd3f88d1ee62c1eded74cce670299.png', NULL, NULL, 'First time plant set up', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', NULL, b'1'),
(272, '4', 'In-process container cleaning', 14, 'Production', '', '1e667541028aaf93884c2e5c45aef9f3.png', NULL, NULL, 'First time plant set up', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', NULL, b'1'),
(273, '5', 'Portable Equipment Cleaning', 14, 'Production', '', '781d3b0796eb3a6fb9f39ed048697d41.png', NULL, NULL, 'First time plant set up', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', NULL, b'1'),
(274, '26', 'Accessory Cleaning', 14, 'Production', '', '722f5f4f9e31c7fe3144124e3e2678f3.png', NULL, NULL, 'First time plant set up', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', NULL, b'1'),
(275, '27', 'Solution Preparation', 20, 'Sanitization', 'solutionPreparationview', '8511c88168a76868441ddad90ac9a320.png', NULL, NULL, 'First time plant set up', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', NULL, b'1'),
(276, '29', 'Drain Points Cleaning', 20, 'Sanitization', 'drainPointview', '9a7637245bf0d6826e1f6bf64b4bdf7d.png', NULL, NULL, 'First time plant set up', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', NULL, b'1'),
(277, '30', 'Daily Cleaning', 20, 'Sanitization', 'dailyCleaningview', '970eade053d5f7435765eb46f097f1c2.png', NULL, NULL, 'First time plant set up', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', NULL, b'1'),
(278, '1005', 'Create Product', 15, 'Create Plant data', 'Admin/AddProduct', 'fe8d14400db960d23d216136bc3c9caf.png', NULL, NULL, 'Testing', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-10-10 16:47:00', b'1'),
(574, '38', 'Approvals', 18, 'Approvals', 'No function', 'd37a87bbe5a683fd7c1b8a0a86bc8e99.png', NULL, NULL, 'First time plant set up testing', 'A-000', 'Superadmin', '2020-01-15 05:23:15', 'Superadmin', '2019-11-10 15:20:53', b'1'),
(2557, '1', 'Operation', 14, 'Production', '', 'd23c2919d26a15f67aecc997ed530d80.png', NULL, NULL, 'First time plant set up', 'A-001', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2558, '1', 'Operation', 14, 'Production', '', 'd23c2919d26a15f67aecc997ed530d80.png', NULL, NULL, 'First time plant set up', 'A-002', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2559, '1', 'Operation', 14, 'Production', '', 'd23c2919d26a15f67aecc997ed530d80.png', NULL, NULL, 'First time plant set up', 'A-003', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2560, '1', 'Operation', 14, 'Production', '', 'd23c2919d26a15f67aecc997ed530d80.png', NULL, NULL, 'First time plant set up', 'A-004', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2561, '1', 'Operation', 14, 'Production', '', 'd23c2919d26a15f67aecc997ed530d80.png', NULL, NULL, 'First time plant set up', 'A-111', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2562, '1', 'Operation', 14, 'Production', '', 'd23c2919d26a15f67aecc997ed530d80.png', NULL, NULL, 'First time plant set up', 'A-112', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2563, '1', 'Operation', 14, 'Production', '', 'd23c2919d26a15f67aecc997ed530d80.png', NULL, NULL, 'First time plant set up', 'A-113', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2564, '1', 'Operation', 14, 'Production', '', 'd23c2919d26a15f67aecc997ed530d80.png', NULL, NULL, 'First time plant set up', 'A-114', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2565, '1', 'Operation', 14, 'Production', '', 'd23c2919d26a15f67aecc997ed530d80.png', NULL, NULL, 'First time plant set up', 'A-115', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2566, '1', 'Operation', 14, 'Production', '', 'd23c2919d26a15f67aecc997ed530d80.png', NULL, NULL, 'First time plant set up', 'A-116', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2567, '1', 'Operation', 14, 'Production', '', 'd23c2919d26a15f67aecc997ed530d80.png', NULL, NULL, 'First time plant set up', 'A-117', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2568, '1', 'Operation', 14, 'Production', '', 'd23c2919d26a15f67aecc997ed530d80.png', NULL, NULL, 'First time plant set up', 'A-118', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2569, '2', 'Type-A', 14, 'Production', '', '61f16e92dd1765ed93d856c0d9b3cecd.png', NULL, NULL, 'First time plant set up', 'A-001', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2570, '2', 'Type-A', 14, 'Production', '', '61f16e92dd1765ed93d856c0d9b3cecd.png', NULL, NULL, 'First time plant set up', 'A-002', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2571, '2', 'Type-A', 14, 'Production', '', '61f16e92dd1765ed93d856c0d9b3cecd.png', NULL, NULL, 'First time plant set up', 'A-003', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2572, '2', 'Type-A', 14, 'Production', '', '61f16e92dd1765ed93d856c0d9b3cecd.png', NULL, NULL, 'First time plant set up', 'A-004', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2573, '2', 'Type-A', 14, 'Production', '', '61f16e92dd1765ed93d856c0d9b3cecd.png', NULL, NULL, 'First time plant set up', 'A-111', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2574, '2', 'Type-A', 14, 'Production', '', '61f16e92dd1765ed93d856c0d9b3cecd.png', NULL, NULL, 'First time plant set up', 'A-112', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2575, '2', 'Type-A', 14, 'Production', '', '61f16e92dd1765ed93d856c0d9b3cecd.png', NULL, NULL, 'First time plant set up', 'A-113', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2576, '2', 'Type-A', 14, 'Production', '', '61f16e92dd1765ed93d856c0d9b3cecd.png', NULL, NULL, 'First time plant set up', 'A-114', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2577, '2', 'Type-A', 14, 'Production', '', '61f16e92dd1765ed93d856c0d9b3cecd.png', NULL, NULL, 'First time plant set up', 'A-115', 'Superadmin', '2020-01-15 06:12:06', 'Superadmin', NULL, b'1'),
(2578, '2', 'Type-A', 14, 'Production', '', '61f16e92dd1765ed93d856c0d9b3cecd.png', NULL, NULL, 'First time plant set up', 'A-116', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2579, '2', 'Type-A', 14, 'Production', '', '61f16e92dd1765ed93d856c0d9b3cecd.png', NULL, NULL, 'First time plant set up', 'A-117', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2580, '2', 'Type-A', 14, 'Production', '', '61f16e92dd1765ed93d856c0d9b3cecd.png', NULL, NULL, 'First time plant set up', 'A-118', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2581, '26', 'Accessory Cleaning', 14, 'Production', '', '722f5f4f9e31c7fe3144124e3e2678f3.png', NULL, NULL, 'First time plant set up', 'A-001', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2582, '26', 'Accessory Cleaning', 14, 'Production', '', '722f5f4f9e31c7fe3144124e3e2678f3.png', NULL, NULL, 'First time plant set up', 'A-002', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2583, '26', 'Accessory Cleaning', 14, 'Production', '', '722f5f4f9e31c7fe3144124e3e2678f3.png', NULL, NULL, 'First time plant set up', 'A-003', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2584, '26', 'Accessory Cleaning', 14, 'Production', '', '722f5f4f9e31c7fe3144124e3e2678f3.png', NULL, NULL, 'First time plant set up', 'A-004', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2585, '26', 'Accessory Cleaning', 14, 'Production', '', '722f5f4f9e31c7fe3144124e3e2678f3.png', NULL, NULL, 'First time plant set up', 'A-111', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2586, '26', 'Accessory Cleaning', 14, 'Production', '', '722f5f4f9e31c7fe3144124e3e2678f3.png', NULL, NULL, 'First time plant set up', 'A-112', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2587, '26', 'Accessory Cleaning', 14, 'Production', '', '722f5f4f9e31c7fe3144124e3e2678f3.png', NULL, NULL, 'First time plant set up', 'A-113', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2588, '26', 'Accessory Cleaning', 14, 'Production', '', '722f5f4f9e31c7fe3144124e3e2678f3.png', NULL, NULL, 'First time plant set up', 'A-114', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2589, '26', 'Accessory Cleaning', 14, 'Production', '', '722f5f4f9e31c7fe3144124e3e2678f3.png', NULL, NULL, 'First time plant set up', 'A-115', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2590, '26', 'Accessory Cleaning', 14, 'Production', '', '722f5f4f9e31c7fe3144124e3e2678f3.png', NULL, NULL, 'First time plant set up', 'A-116', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2591, '26', 'Accessory Cleaning', 14, 'Production', '', '722f5f4f9e31c7fe3144124e3e2678f3.png', NULL, NULL, 'First time plant set up', 'A-117', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2592, '26', 'Accessory Cleaning', 14, 'Production', '', '722f5f4f9e31c7fe3144124e3e2678f3.png', NULL, NULL, 'First time plant set up', 'A-118', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2593, '27', 'Solution Preparation', 20, 'Sanitization', 'solutionPreparationview', '8511c88168a76868441ddad90ac9a320.png', NULL, NULL, 'First time plant set up', 'A-001', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2594, '27', 'Solution Preparation', 20, 'Sanitization', 'solutionPreparationview', '8511c88168a76868441ddad90ac9a320.png', NULL, NULL, 'First time plant set up', 'A-002', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2595, '29', 'Drain Points Cleaning', 20, 'Sanitization', 'drainPointview', '9a7637245bf0d6826e1f6bf64b4bdf7d.png', NULL, NULL, 'First time plant set up', 'A-001', 'Superadmin', '2020-01-15 06:12:07', 'Superadmin', NULL, b'1'),
(2596, '29', 'Drain Points Cleaning', 20, 'Sanitization', 'drainPointview', '9a7637245bf0d6826e1f6bf64b4bdf7d.png', NULL, NULL, 'First time plant set up', 'A-002', 'Superadmin', '2020-01-15 06:12:08', 'Superadmin', NULL, b'1'),
(2597, '3', 'Type-B', 14, 'Production', '', '286bd3f88d1ee62c1eded74cce670299.png', NULL, NULL, 'First time plant set up', 'A-001', 'Superadmin', '2020-01-15 06:12:08', 'Superadmin', NULL, b'1'),
(2598, '3', 'Type-B', 14, 'Production', '', '286bd3f88d1ee62c1eded74cce670299.png', NULL, NULL, 'First time plant set up', 'A-002', 'Superadmin', '2020-01-15 06:12:08', 'Superadmin', NULL, b'1'),
(2599, '30', 'Daily Cleaning', 20, 'Sanitization', 'dailyCleaningview', '970eade053d5f7435765eb46f097f1c2.png', NULL, NULL, 'First time plant set up', 'A-001', 'Superadmin', '2020-01-15 06:12:08', 'Superadmin', NULL, b'1'),
(2600, '30', 'Daily Cleaning', 20, 'Sanitization', 'dailyCleaningview', '970eade053d5f7435765eb46f097f1c2.png', NULL, NULL, 'First time plant set up', 'A-002', 'Superadmin', '2020-01-15 06:12:08', 'Superadmin', NULL, b'1'),
(2601, '37', 'Reports', 17, 'Reports', '', 'd37a87bbe5a683fd7c1b8a0a86bc8e99.png', NULL, NULL, 'First time plant set up', 'A-001', 'Superadmin', '2020-01-15 06:12:08', 'Superadmin', NULL, b'1'),
(2602, '37', 'Reports', 17, 'Reports', '', 'd37a87bbe5a683fd7c1b8a0a86bc8e99.png', NULL, NULL, 'First time plant set up', 'A-002', 'Superadmin', '2020-01-15 06:12:08', 'Superadmin', NULL, b'1'),
(2603, '38', 'Approvals', 18, 'Approvals', 'No function', 'd37a87bbe5a683fd7c1b8a0a86bc8e99.png', NULL, NULL, 'First time plant set up testing', 'A-001', 'Superadmin', '2020-01-15 06:12:08', 'Superadmin', NULL, b'1'),
(2604, '38', 'Approvals', 18, 'Approvals', 'No function', 'd37a87bbe5a683fd7c1b8a0a86bc8e99.png', NULL, NULL, 'First time plant set up testing', 'A-002', 'Superadmin', '2020-01-15 06:12:08', 'Superadmin', NULL, b'1'),
(2605, '4', 'In-process container cleaning', 14, 'Production', '', '1e667541028aaf93884c2e5c45aef9f3.png', NULL, NULL, 'First time plant set up', 'A-001', 'Superadmin', '2020-01-15 06:12:08', 'Superadmin', NULL, b'1'),
(2606, '4', 'In-process container cleaning', 14, 'Production', '', '1e667541028aaf93884c2e5c45aef9f3.png', NULL, NULL, 'First time plant set up', 'A-002', 'Superadmin', '2020-01-15 06:12:08', 'Superadmin', NULL, b'1'),
(2607, '5', 'Portable Equipment Cleaning', 14, 'Production', '', '781d3b0796eb3a6fb9f39ed048697d41.png', NULL, NULL, 'First time plant set up', 'A-001', 'Superadmin', '2020-01-15 06:12:08', 'Superadmin', NULL, b'1'),
(2608, '5', 'Portable Equipment Cleaning', 14, 'Production', '', '781d3b0796eb3a6fb9f39ed048697d41.png', NULL, NULL, 'First time plant set up', 'A-002', 'Superadmin', '2020-01-15 06:12:08', 'Superadmin', NULL, b'1'),
(2609, 'act0099', 'ActName0099', 14, 'Production', 'test', 'c269ab713ff64808bc05af9c6651ced6.png', NULL, NULL, 'remark test', 'A-000', 'Superadmin', '2020-03-13 12:59:30', NULL, NULL, b'1');

--
-- Triggers `mst_activity`
--
DROP TRIGGER IF EXISTS `au_activityactivity`;
DELIMITER $$
CREATE TRIGGER `au_activityactivity` AFTER UPDATE ON `mst_activity` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_activity',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.activity_code != NEW.activity_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_code', OLD.activity_code, NEW.activity_code);
	  END IF;
	  
	  IF(OLD.activity_name != NEW.activity_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_name', OLD.activity_name, NEW.activity_name);
	  END IF;
	  
	  IF(OLD.activitytype_id != NEW.activitytype_id)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activitytype_id', OLD.activitytype_id, NEW.activitytype_id);
	  END IF;
	  
	 IF(OLD.activity_type != NEW.activity_type)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_type', OLD.activity_type, NEW.activity_type);
	  END IF; 
	  
	  IF(OLD.activityfunc_tocall != NEW.activityfunc_tocall)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activityfunc_tocall', OLD.activityfunc_tocall, NEW.activityfunc_tocall);
	  END IF;
	  IF(OLD.activity_icon != NEW.activity_icon)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_icon', OLD.activity_icon, NEW.activity_icon);
	  END IF;
	  IF(OLD.activity_nature != NEW.activity_nature)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_nature', OLD.activity_nature, NEW.activity_nature);
	  END IF;
	  IF(OLD.activity_duration != NEW.activity_duration)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_duration', OLD.activity_duration, NEW.activity_duration);
	  END IF;
             
          IF(OLD.activity_remark != NEW.activity_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_remark', OLD.activity_remark, NEW.activity_remark);
	  END IF;
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_code', OLD.area_code, NEW.area_code);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_activity', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_activitytype`
--

DROP TABLE IF EXISTS `mst_activitytype`;
CREATE TABLE IF NOT EXISTS `mst_activitytype` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activitytype_name` varchar(500) DEFAULT NULL,
  `activitytype_icon` varchar(500) DEFAULT NULL,
  `activityfunction_tocall` varchar(500) DEFAULT NULL,
  `activity_remarks` text,
  `activity_blockcolor` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(250) DEFAULT NULL,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='Activity Type Master Table';

--
-- Dumping data for table `mst_activitytype`
--

INSERT INTO `mst_activitytype` (`id`, `activitytype_name`, `activitytype_icon`, `activityfunction_tocall`, `activity_remarks`, `activity_blockcolor`, `created_on`, `created_by`, `modified_by`, `modified_on`, `is_active`) VALUES
(14, 'Production', 'aff22bbcb7c3b4edfe07c87d577cb01d.png', 'Production/area', 'First time plant set up', '#ff6f61', '2020-01-15 05:23:16', 'Superadmin', NULL, NULL, b'1'),
(15, 'First time Plant Set-up', '7150947c9217d597e9a8c41c201407b8.png', 'Admin/Activity/15', 'First time plant set up', '#ff6f61', '2020-01-15 05:23:16', 'Superadmin', NULL, NULL, b'1'),
(16, 'Create Master Data', '0d140305421992f5859a9a51de63e750.png', 'Admin/Activity/16', 'Testing again', '#ff6f61', '2020-01-15 05:23:16', 'Superadmin', NULL, NULL, b'1'),
(17, 'Reports', 'd5ba01aff184809802df09d3b4594e91.png', 'Report/Reports', 'First time plant set up', '#ff6f61', '2020-01-15 05:23:16', 'Superadmin', NULL, NULL, b'1'),
(18, 'Approvals', 'd1af133d417cbfbc1c04665d33ae4e15.png', 'User/AS', 'First time plant set up', '#88b04b', '2020-01-15 05:23:16', 'Superadmin', NULL, NULL, b'1'),
(20, 'Sanitization', '3e4c5dadd4a2172cd759bc508c864b68.png', 'Sanitization/area', 'First time plant set up', '#88b04b', '2020-01-15 05:23:16', 'Superadmin', NULL, NULL, b'1'),
(21, 'QA Approvals', '38bdb0a3a3f3fd33190ed28e6460f1d2.png', 'User/AS', 'QA Approvals', '#ff6f61', '2020-01-15 05:23:16', 'Superadmin', 'Superadmin', '2019-10-11 06:50:31', b'1'),
(22, 'Aseptic', 'a6338a29acb1179868af3b1dfa11c0fb.png', 'injectable/activities', ' Injectibles set up', '#ff6f61', '2020-01-15 05:23:16', 'Superadmin', NULL, NULL, b'1'),
(23, 'Clean Area', 'b613e685eb3d2202f002ae31d34ce400.png', 'injectable/activities', 'injectibles set up', '#ff6f61', '2020-01-15 05:23:16', 'Superadmin', NULL, NULL, b'1'),
(24, 'Grade D', '2b10e1060b0888c76fb7e2e6c8caccb2.png', 'injectable/activities', 'injectibles set up', '#ff6f61', '2020-01-15 05:23:16', 'Superadmin', NULL, NULL, b'1'),
(25, 'Non-Classified', 'c5eed8274e9c230f6805a56801f13e2d.png', 'injectable/activities', ' Injectibles set up', '#ff6f61', '2020-01-15 05:23:16', 'Superadmin', NULL, NULL, b'1');

--
-- Triggers `mst_activitytype`
--
DROP TRIGGER IF EXISTS `au_activitytypeactivity`;
DELIMITER $$
CREATE TRIGGER `au_activitytypeactivity` AFTER UPDATE ON `mst_activitytype` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_activitytype',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.activitytype_name != NEW.activitytype_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activitytype_name', OLD.activitytype_name, NEW.activitytype_name);
	  END IF;
	  IF(OLD.activitytype_icon != NEW.activitytype_icon)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activitytype_icon', OLD.activitytype_icon, NEW.activitytype_icon);
	  END IF;
	  
	  IF(OLD.activityfunction_tocall != NEW.activityfunction_tocall)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activityfunction_tocall', OLD.activityfunction_tocall, NEW.activityfunction_tocall);
	  END IF;
	  IF(OLD.activity_remarks != NEW.activity_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_remarks', OLD.activity_remarks, NEW.activity_remarks);
	  END IF;
	  IF(OLD.activity_blockcolor != NEW.activity_blockcolor)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_blockcolor', OLD.activity_blockcolor, NEW.activity_blockcolor);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_activitytype', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_area`
--

DROP TABLE IF EXISTS `mst_area`;
CREATE TABLE IF NOT EXISTS `mst_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_code` varchar(50) DEFAULT NULL,
  `area_name` varchar(250) DEFAULT NULL,
  `area_address` varchar(4000) DEFAULT NULL,
  `area_remarks` varchar(2000) DEFAULT NULL,
  `area_contact` varchar(20) DEFAULT NULL,
  `area_sop_no` varchar(20) NOT NULL,
  `block_code` varchar(50) DEFAULT NULL,
  `department_code` varchar(200) NOT NULL,
  `number_of_rooms` int(11) NOT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_area`
--

INSERT INTO `mst_area` (`id`, `area_code`, `area_name`, `area_address`, `area_remarks`, `area_contact`, `area_sop_no`, `block_code`, `department_code`, `number_of_rooms`, `is_active`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(20, 'A-000', 'Super admin', 'Mayur Vihar Ph 1\r\n', 'Test data', '9900990011', 'SOP-223', 'Block-000', '', 2, b'0', 'Superadmin', '2020-01-15 05:23:17', NULL, NULL),
(30, 'A_195', 'Injectibles Area', NULL, 'Injectibles', '9900990011', 'TAB-002', 'Block-Inj', 'SB-inj1', 8, b'1', 'Superadmin', '2020-01-15 05:23:17', 'Superadmin', '2020-04-07 07:19:28'),
(31, 'A-001', 'Gel Mass Area', NULL, '', '', 'TAB-023', 'Block-OSD', 'OSD-1', 2, b'1', 'Superadmin', '2020-01-15 05:23:17', NULL, NULL),
(32, 'A-002', 'Fluidized Bed Dryer Room', NULL, 'Testing', '9900990011', 'TAB-023', 'Block-OSD', 'OSD-1', 2, b'1', 'Superadmin', '2020-01-15 05:23:17', NULL, NULL),
(33, 'A-003', 'Granulation Area', NULL, 'Testing', '9900990011', 'TAB-023', 'Block-OSD', 'OSD-1', 2, b'1', 'Superadmin', '2020-01-15 05:23:17', NULL, NULL),
(34, 'A-004', 'Blending and Sifting', NULL, 'Testing', '9900990011', 'TAB-023', 'Block-OSD', 'OSD-1', 2, b'1', 'Superadmin', '2020-01-15 05:23:17', NULL, NULL),
(35, 'A-006', 'Coating', NULL, '', '', 'TAB-023', 'Block-OSD2', 'OSD-2', 2, b'1', 'Superadmin', '2020-01-15 05:23:17', NULL, NULL),
(36, 'A-111', 'Test Area1', NULL, '', '', 'TAB-23', 'Block-OSD', 'OSD-1', 1, b'1', 'Superadmin', '2020-01-15 05:23:17', NULL, NULL),
(37, 'A-112', 'Test Area2', NULL, '', '', 'TAB-23', 'Block-OSD', 'OSD-1', 1, b'1', 'Superadmin', '2020-01-15 05:23:17', NULL, NULL),
(38, 'A-113', 'Test Area3', NULL, '', '', 'TAB-23', 'Block-OSD', 'OSD-1', 1, b'1', 'Superadmin', '2020-01-15 05:23:17', NULL, NULL),
(39, 'A-114', 'Test Area4', NULL, '', '', 'TAB-23', 'Block-OSD', 'OSD-1', 1, b'1', 'Superadmin', '2020-01-15 05:23:17', NULL, NULL),
(40, 'A-115', 'Test Area5', NULL, '', '', 'TAB-23', 'Block-OSD', 'OSD-1', 1, b'1', 'Superadmin', '2020-01-15 05:23:17', NULL, NULL),
(41, 'A-116', 'Test Area6', NULL, '', '', 'TAB-23', 'Block-OSD', 'OSD-1', 1, b'1', 'Superadmin', '2020-01-15 05:23:17', 'Superadmin', '2020-04-07 15:45:10'),
(42, 'A-117', 'Test Area7', NULL, '', '', 'TAB-23', 'Block-OSD', 'OSD-1', 1, b'1', 'Superadmin', '2020-01-15 05:23:17', NULL, NULL),
(43, 'A-118', 'Test Area8', NULL, '', '', 'TAB-23', 'Block-OSD', 'OSD-1', 1, b'1', 'Superadmin', '2020-01-15 05:23:17', 'Superadmin', '2020-04-07 07:19:48');

--
-- Triggers `mst_area`
--
DROP TRIGGER IF EXISTS `au_areaactivity`;
DELIMITER $$
CREATE TRIGGER `au_areaactivity` AFTER UPDATE ON `mst_area` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_area',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_code', OLD.area_code, NEW.area_code);
	  END IF;
	  
	  IF(OLD.area_name != NEW.area_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_name', OLD.area_name, NEW.area_name);
	  END IF;
	  
	  IF(OLD.area_address != NEW.area_address)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_address', OLD.area_address, NEW.area_address);
	  END IF;
	  
	 IF(OLD.area_contact != NEW.area_contact)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_contact', OLD.area_contact, NEW.area_contact);
	  END IF; 
	  
	  
	  IF(OLD.area_remarks != NEW.area_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_remarks', OLD.area_remarks, NEW.area_remarks);
	  END IF; 
	  
	  IF(OLD.area_sop_no != NEW.area_sop_no)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_sop_no', OLD.area_sop_no, NEW.area_sop_no);
	  END IF;
	  IF(OLD.number_of_rooms != NEW.number_of_rooms)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'number_of_rooms', OLD.number_of_rooms, NEW.number_of_rooms);
	  END IF;
	  IF(OLD.block_code != NEW.block_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_code', OLD.block_code, NEW.block_code);
	  END IF; 
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_area', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_block`
--

DROP TABLE IF EXISTS `mst_block`;
CREATE TABLE IF NOT EXISTS `mst_block` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `block_code` varchar(50) DEFAULT NULL,
  `block_name` varchar(2000) DEFAULT NULL,
  `block_contact` varchar(20) DEFAULT NULL,
  `block_remarks` varchar(2000) DEFAULT NULL,
  `number_of_areas` int(11) DEFAULT NULL,
  `plant_code` varchar(50) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_block`
--

INSERT INTO `mst_block` (`id`, `block_code`, `block_name`, `block_contact`, `block_remarks`, `number_of_areas`, `plant_code`, `is_active`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(3, 'Block-000', 'sa block', '9', 'Pre-loaded', 1, 'PL001', 1, 'Admin', '2020-01-15 05:23:18', 'Admin', NULL),
(13, 'Block-Inj', 'Injectibles', '9900990099', 'Injectibles', 1, 'PL001', 0, 'Superadmin', '2020-01-15 05:23:18', 'Superadmin', '2020-04-05 08:30:35'),
(14, 'Block-OSD', 'OSD Block', '9900990097', '', 3, 'PL001', 1, 'Superadmin', '2020-01-15 05:23:18', 'Superadmin', '2020-05-02 08:26:49'),
(15, 'Block-OSD2', 'New Block', '9999955555', 'testing', 1, 'PL001', 0, 'Superadmin', '2020-01-15 05:23:18', 'Superadmin', '2020-04-05 08:30:03'),
(16, 'bvfsjfvj', 'jhfdfd', '9999999999', 'sdfsdfs', 5, 'PL001', 0, 'Superadmin', '2020-04-06 07:09:54', 'Superadmin', '2020-04-06 07:10:05'),
(17, 'text', 'ddddd', '9999999999', 'sadsfasda', 4, 'PL001', 0, 'Superadmin', '2020-04-06 07:28:18', 'Superadmin', '2020-04-06 07:28:27');

--
-- Triggers `mst_block`
--
DROP TRIGGER IF EXISTS `au_blockactivity`;
DELIMITER $$
CREATE TRIGGER `au_blockactivity` AFTER UPDATE ON `mst_block` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_block',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	 IF(OLD.block_code != NEW.block_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_code', OLD.block_code, NEW.block_code);
	  END IF;
	  
	  IF(OLD.block_name != NEW.block_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_name', OLD.block_name, NEW.block_name);
	  END IF; 
	  
	 IF(OLD.block_contact != NEW.block_contact)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_contact', OLD.block_contact, NEW.block_contact);
	  END IF; 
	  
	  
	  IF(OLD.block_remarks != NEW.block_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_remarks', OLD.block_remarks, NEW.block_remarks);
	  END IF; 
	  
	  
	  IF(OLD.number_of_areas != NEW.number_of_areas)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'number_of_areas', OLD.number_of_areas, NEW.number_of_areas);
	  END IF;
	  IF(OLD.plant_code != NEW.plant_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_code', OLD.plant_code, NEW.plant_code);
	  END IF; 
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_block', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_department`
--

DROP TABLE IF EXISTS `mst_department`;
CREATE TABLE IF NOT EXISTS `mst_department` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `department_code` varchar(50) DEFAULT NULL,
  `department_name` varchar(500) DEFAULT NULL,
  `department_address` varchar(2000) DEFAULT NULL,
  `department_contact` varchar(20) DEFAULT NULL,
  `department_remark` varchar(2000) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `block_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_department`
--

INSERT INTO `mst_department` (`id`, `department_code`, `department_name`, `department_address`, `department_contact`, `department_remark`, `area_code`, `block_code`, `created_by`, `created_on`, `modified_by`, `modified_on`, `is_active`) VALUES
(19, 'SB-inj1', 'Sub Block Injectible', NULL, '9900990011', 'Injectibles', 'A_195', 'Block-Inj', 'Superadmin', '2020-01-15 05:23:19', 'Superadmin', '2020-01-15 17:55:09', b'1'),
(20, 'OSD-1', 'OSD-1', NULL, '9900990011', '', 'A-001', 'Block-OSD', 'Superadmin', '2020-01-15 05:23:19', 'Superadmin', '2020-05-04 09:12:18', b'1'),
(21, 'OSD-1', 'OSD-1', NULL, '9900990011', '', 'A-002', 'Block-OSD', 'Superadmin', '2020-01-15 05:23:19', 'Superadmin', '2020-05-04 09:12:18', b'1'),
(22, 'OSD-1', 'OSD-1', NULL, '9900990011', '', 'A-003', 'Block-OSD', 'Superadmin', '2020-01-15 05:23:19', 'Superadmin', '2020-05-04 09:12:18', b'1'),
(23, 'OSD-1', 'OSD-1', NULL, '9900990011', '', 'A-004', 'Block-OSD', 'Superadmin', '2020-01-15 05:23:19', 'Superadmin', '2020-05-04 09:12:18', b'1'),
(24, 'OSD-2', 'New Sub Block for OSD', NULL, '', '', 'A-006', 'Block-OSD2', 'Superadmin', '2020-01-15 05:23:19', NULL, NULL, b'1'),
(25, 'OSD-1', 'OSD-1', NULL, '9900990011', '', 'A-111', 'Block-OSD', 'Superadmin', '2020-01-15 05:23:19', 'Superadmin', '2020-05-04 09:12:18', b'1'),
(26, 'OSD-1', 'OSD-1', NULL, '9900990011', '', 'A-112', 'Block-OSD', 'Superadmin', '2020-01-15 05:23:19', 'Superadmin', '2020-05-04 09:12:18', b'1'),
(27, 'OSD-1', 'OSD-1', NULL, '9900990011', '', 'A-113', 'Block-OSD', 'Superadmin', '2020-01-15 05:23:19', 'Superadmin', '2020-05-04 09:12:18', b'1'),
(28, 'OSD-1', 'OSD-1', NULL, '9900990011', '', 'A-114', 'Block-OSD', 'Superadmin', '2020-01-15 05:23:19', 'Superadmin', '2020-05-04 09:12:18', b'1'),
(29, 'OSD-1', 'OSD-1', NULL, '9900990011', '', 'A-115', 'Block-OSD', 'Superadmin', '2020-01-15 05:23:19', 'Superadmin', '2020-05-04 09:12:18', b'1'),
(30, 'OSD-1', 'OSD-1', NULL, '9900990011', '', 'A-116', 'Block-OSD', 'Superadmin', '2020-01-15 05:23:19', 'Superadmin', '2020-05-04 09:12:18', b'1'),
(31, 'OSD-1', 'OSD-1', NULL, '9900990011', '', 'A-117', 'Block-OSD', 'Superadmin', '2020-01-15 05:23:19', 'Superadmin', '2020-05-04 09:12:18', b'1'),
(32, 'OSD-1', 'OSD-1', NULL, '9900990011', '', 'A-118', 'Block-OSD', 'Superadmin', '2020-01-15 05:23:19', 'Superadmin', '2020-05-04 09:12:18', b'1');

--
-- Triggers `mst_department`
--
DROP TRIGGER IF EXISTS `au_departmentactivity`;
DELIMITER $$
CREATE TRIGGER `au_departmentactivity` AFTER UPDATE ON `mst_department` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_department',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	 IF(OLD.department_code != NEW.department_code)
	  THEN
	  
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'department_code', OLD.department_code, NEW.department_code);
	  END IF;
	  IF(OLD.department_name != NEW.department_name)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'department_name', OLD.department_name, NEW.department_name);
	  END IF; 
	  IF(OLD.department_address != NEW.department_address)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'department_address', OLD.department_address, NEW.department_address);
	  END IF; 
	  IF(OLD.department_contact != NEW.department_contact)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'department_contact', OLD.department_contact, NEW.department_contact);
	  END IF; 
	  IF(OLD.department_remark != NEW.department_remark)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'department_remark', OLD.department_remark, NEW.department_remark);
	  END IF; 
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_code', OLD.area_code, NEW.area_code);
	  END IF; 
	  IF(OLD.block_code != NEW.block_code)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_code', OLD.block_code, NEW.block_code);
	  END IF; 
	  END IF;
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_department', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_document`
--

DROP TABLE IF EXISTS `mst_document`;
CREATE TABLE IF NOT EXISTS `mst_document` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `doc_id` varchar(50) DEFAULT NULL,
  `remarks` varchar(2000) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_document`
--
DROP TRIGGER IF EXISTS `au_documentactivity`;
DELIMITER $$
CREATE TRIGGER `au_documentactivity` AFTER UPDATE ON `mst_document` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_document',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	 IF(OLD.doc_id != NEW.doc_id)
	  THEN
	  
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'doc_id', OLD.doc_id, NEW.doc_id);
	  END IF;
	  IF(OLD.remarks != NEW.remarks)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'remarks', OLD.remarks, NEW.remarks);
	  END IF; 
	  END IF;
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_document', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_drainpoint`
--

DROP TABLE IF EXISTS `mst_drainpoint`;
CREATE TABLE IF NOT EXISTS `mst_drainpoint` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `drainpoint_code` varchar(50) DEFAULT NULL,
  `drainpoint_name` varchar(500) DEFAULT NULL,
  `drainpoint_remark` varchar(2000) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `solution_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=567 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_drainpoint`
--

INSERT INTO `mst_drainpoint` (`id`, `drainpoint_code`, `drainpoint_name`, `drainpoint_remark`, `room_code`, `solution_code`, `created_by`, `created_on`, `modified_by`, `modified_on`, `is_active`) VALUES
(549, 'Drain1234', 'Drain point test', 'd', NULL, '', 'Superadmin', '2020-01-15 05:23:21', NULL, NULL, b'1'),
(550, 'D/PKG-CPD024/001', 'Gel Mass Area 1', '', 'TAA-79', '', 'Superadmin', '2020-01-15 05:23:21', NULL, NULL, b'1'),
(551, 'D/PKG-CPD029/009', 'Gel Mass Area 1', '', 'TAA-58,TAA-59', '', 'Superadmin', '2020-01-15 05:23:21', NULL, NULL, b'1'),
(552, 'D/PD-74/01', 'Gel Mass area Room', NULL, 'TAA-79', NULL, 'Superadmin', '2020-01-15 05:23:21', 'Superadmin', '2019-10-30 13:54:44', b'1'),
(553, 'TAA-168/01', NULL, NULL, 'TAA-79', NULL, 'Superadmin', '2020-01-15 05:23:21', 'Superadmin', '2019-10-30 13:56:12', b'1'),
(554, 'D/SH-024/01', NULL, NULL, 'IPQA', NULL, 'Superadmin', '2020-01-15 05:23:21', 'Superadmin', '2019-10-30 13:56:12', b'1'),
(555, 'D/PD-55/01', NULL, NULL, 'ttestroom001', NULL, 'Superadmin', '2020-01-15 05:23:21', 'Superadmin', '2019-10-30 13:56:12', b'1'),
(556, 'D/SH-012/01', NULL, NULL, 'ttestroom001', NULL, 'Superadmin', '2020-01-15 05:23:21', 'Superadmin', '2019-10-30 13:56:12', b'1'),
(557, 'D/TAA-069/01', NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:21', 'Superadmin', '2019-10-30 13:56:13', b'1'),
(558, 'D/TAA-067/01', NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:21', 'Superadmin', '2019-10-30 13:56:13', b'1'),
(559, 'D/SH-033/01', NULL, NULL, 'TAA-80', NULL, 'Superadmin', '2020-01-15 05:23:21', 'Superadmin', '2019-10-30 13:56:13', b'1'),
(560, 'D/TAA-059/01', NULL, NULL, '', NULL, 'Superadmin', '2020-01-15 05:23:21', 'Superadmin', '2019-10-30 13:56:13', b'1'),
(561, 'D/PD-75/01', 'Gel Mass area Room', 'excel upload test data', '', NULL, 'Superadmin', '2020-01-15 05:23:21', 'Superadmin', '2019-10-30 13:56:12', b'1'),
(562, 'D/PD-74/02', 'Drain Points for Coating area', '', 'TAA-78', 'DIS_0002', 'Superadmin', '2020-01-15 05:23:21', NULL, NULL, b'1'),
(563, 'D/PD-75/02', 'Drain Points for Coating area', '', 'TAA-52,TAA-53', 'DIS_0003', 'Superadmin', '2020-01-15 05:23:21', NULL, NULL, b'1'),
(564, 'KHP/DP/100', 'DP-Compression-Coating', NULL, '', 'LYSOL 2.5 %', 'Superadmin', '2020-01-15 05:23:21', NULL, NULL, b'1'),
(565, 'KHP/DP/101', 'DP-Compression-Coating', NULL, 'TAA-58,TAA-59', 'LYSOL 2.5 %', 'Superadmin', '2020-01-15 05:23:21', NULL, NULL, b'1'),
(566, 'KHP/DP/102', 'DP-Compression-Coating', NULL, 'TAA-52,TAA-53', 'LYSOL 2.5 %', 'Superadmin', '2020-01-15 05:23:21', NULL, NULL, b'1');

--
-- Triggers `mst_drainpoint`
--
DROP TRIGGER IF EXISTS `au_drainpointactivity`;
DELIMITER $$
CREATE TRIGGER `au_drainpointactivity` AFTER UPDATE ON `mst_drainpoint` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_drainpoint',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.drainpoint_code != NEW.drainpoint_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'drainpoint_code', OLD.drainpoint_code, NEW.drainpoint_code);
	  END IF;
	  
	  IF(OLD.drainpoint_name != NEW.drainpoint_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'drainpoint_name', OLD.drainpoint_name, NEW.drainpoint_name);
	  END IF;
	  
	  IF(OLD.drainpoint_remark != NEW.drainpoint_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'drainpoint_remark', OLD.drainpoint_remark, NEW.drainpoint_remark);
	  END IF; 
	  
	  
	  IF(OLD.room_code != NEW.room_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_code', OLD.room_code, NEW.room_code);
	  END IF; 
          
	  IF(OLD.solution_code != NEW.solution_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'solution_code', OLD.solution_code, NEW.solution_code);
	  END IF;	 
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_drainpoint', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_employee`
--

DROP TABLE IF EXISTS `mst_employee`;
CREATE TABLE IF NOT EXISTS `mst_employee` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `emp_code` varchar(50) DEFAULT NULL,
  `emp_name` varchar(250) DEFAULT NULL,
  `email2` varchar(200) DEFAULT NULL,
  `emp_address` varchar(2000) DEFAULT NULL,
  `emp_contact` bigint(20) DEFAULT NULL,
  `emp_email` varchar(500) DEFAULT NULL,
  `emp_password` varchar(500) DEFAULT NULL,
  `designation_code` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `role_code` varchar(50) DEFAULT NULL,
  `block_code` varchar(2000) DEFAULT NULL,
  `Sub_block_code` varchar(2000) DEFAULT NULL,
  `area_code` varchar(2000) DEFAULT NULL,
  `emp_remark` varchar(2000) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `is_blocked` bit(1) NOT NULL DEFAULT b'0',
  `login_attampt` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_employee`
--

INSERT INTO `mst_employee` (`id`, `emp_code`, `emp_name`, `email2`, `emp_address`, `emp_contact`, `emp_email`, `emp_password`, `designation_code`, `role_id`, `role_code`, `block_code`, `Sub_block_code`, `area_code`, `emp_remark`, `created_by`, `created_on`, `modified_by`, `modified_on`, `is_active`, `is_blocked`, `login_attampt`) VALUES
(10, 'Superadmin', 'Super Admin', 'sa@sunpharma.in', NULL, 9, 'sa', '329a424d6a0bb26caaea10cc033b64b4c1ab45a6874196554cc16d705949e033', 'superadmin', 6, 'Role6', 'Block-000', NULL, 'A-033', 'This is pre-loaded data', 'Admin', '2020-01-15 05:23:22', NULL, NULL, b'1', b'0', 0),
(32, 'emp-inj', 'Tushar', 'tushar@sunpharma.in', 'Mayur Vihar Ph 1', 9900990099, 'tushar', '74921a389455ca019cae1af73c8a8a6ef4ebedd081a347372bc54a04bc058069', 'Senior trainee', 1, NULL, 'Block-Inj', 'SB-inj1', 'A_195', 'Injectables Testing', 'Superadmin', '2020-01-15 05:23:22', NULL, NULL, b'1', b'0', 0),
(33, 'emp-002', 'sid', 'sid@sunpharma.in', 'Mayur Vihar Ph 1', 9900990099, 'Rahul.Chauhan', '2e7d82708f7e37a8c75ed3b624e3e97aa38e570ca9a1a2083261fa42972a3740', 'Senior trainee', 1, NULL, 'Block-OSD', 'OSD-1', 'A-002,A-001', 'First time plant setup', 'Superadmin', '2020-01-15 05:23:22', 'Superadmin', '2020-05-28 15:54:29', b'1', b'0', 1),
(34, 'emp-005', 'abdul ', 'abdul@sunpharma.in', 'Sahyog Apartments', 9900990099, 'abdul', 'bf9e8e7955e8c87d5fe571ad0be25008654157a58a7a34353b81d747e8c6577a', 'Senior trainee', 2, NULL, 'Block-OSD', 'OSD-1', 'A-004,A-003,A-002,A-001', '', 'Superadmin', '2020-01-15 05:23:22', 'Superadmin', '2019-12-18 11:00:50', b'1', b'0', 0),
(35, 'emp-003', 'rajiv', 'rajiv@sunpharma.in', 'Test address ', 9900990099, 'rajiv', '0274612f68d9f666a0120db2d4a7819ba945193dddcf2806e496ccb58c80ddd3', 'Senior trainee', 1, NULL, 'Block-OSD', 'OSD-1', 'A-002,A-001', '', 'Superadmin', '2020-01-15 05:23:22', 'Superadmin', '2019-12-02 12:32:58', b'1', b'0', 0),
(36, 'E-001', 'suraksha', '', '182', 9900990000, 'suraksha', 'f7c082d760237846d9f7438cb985cf3e3adc16c4f2fbcfdf6ebc91f46ea38590', 'trainee', 10, NULL, 'Block-OSD', 'OSD-1', 'A-004,A-003,A-002,A-001', '', 'Superadmin', '2020-01-15 05:23:22', 'Superadmin', '2020-04-15 10:53:22', b'0', b'0', 0),
(37, 'emp-006', 'sid1', 'sid1@sunpharma.in', '1234', 0, 'sid1', 'bf9e8e7955e8c87d5fe571ad0be25008654157a58a7a34353b81d747e8c6577a', 'trainee', 1, NULL, 'Block-OSD2', 'OSD-2', 'A-006', '', 'Superadmin', '2020-01-15 05:23:22', NULL, NULL, b'1', b'0', 0),
(38, 'emp-007', 'rajiv1', '', '', 0, 'rajiv2', '0274612f68d9f666a0120db2d4a7819ba945193dddcf2806e496ccb58c80ddd3', 'trainee', 1, NULL, 'Block-OSD2', 'OSD-2', 'A-006', '', 'Superadmin', '2020-01-15 05:23:22', NULL, NULL, b'1', b'0', 0),
(39, 'emp-008', 'abdul2', '', '1234', 0, 'abdul2', '5e5bcfe4f6bdcbab0f87fd45d24de6bf6cc800ec20c7cf65dbce72dcf00ea3a3', 'Manager', 2, NULL, 'Block-OSD2', 'OSD-2', 'A-006', '', 'Superadmin', '2020-01-15 05:23:22', NULL, NULL, b'1', b'0', 0),
(40, 'emp-009', 'meera1', '', '123', 0, 'meera1', '5fd8b7d3584042a843bf341ae2dcca285a3924ea11b35e2d9f686f780ce122ee', 'Manager', 3, NULL, 'Block-OSD2', 'OSD-2', 'A-006', '', 'Superadmin', '2020-01-15 05:23:22', NULL, NULL, b'1', b'0', 0),
(41, 'emp-004', 'meera', '', '1234', 9900990000, 'meera', 'bf9e8e7955e8c87d5fe571ad0be25008654157a58a7a34353b81d747e8c6577a', 'Manager', 3, NULL, 'Block-OSD', 'OSD-1,OSD-1,OSD-1,OSD-1,OSD-1,OSD-1,OSD-1,OSD-1,OSD-1,OSD-1,OSD-1,OSD-1', 'A-006,A-004,A-003,A-002,A-001', '', 'Superadmin', '2020-01-15 05:23:22', 'Superadmin', '2020-05-22 10:38:16', b'1', b'0', 1),
(42, 'emp-011', 'radha', '', '', 9900990000, 'radha', 'f1348790235786fd1ecda2c2e380181938d64bdbb6fb2000febbe14562f0f779', 'Manager', 5, NULL, 'Block-OSD', 'OSD-1', 'A-116,A-115,A-114,A-113,A-112,A-111,A-004,A-003,A-002,A-001', '', 'Superadmin', '2020-01-15 05:23:22', 'Superadmin', '2019-12-18 19:00:30', b'1', b'0', 0),
(43, 'emp-012', 'tarun', '', '', 9999999990, 'tarun', 'a172611891c84205afdfad6964e26772da61e76e855bddad03e7f331b5bf0dca', 'trainee', 4, NULL, 'Block-OSD', 'OSD-1', 'A-002,A-001', '', 'Superadmin', '2020-01-15 05:23:22', 'Superadmin', '2019-12-18 19:15:38', b'1', b'0', 0),
(44, 'rahul002888', 'Rahul Chauhan22', 'rahul.chauhan66@smhs.motherson.com', 'Ghaziabaddd', 9718280028, 'rahul0028', '9a73f0609eb6197596d89581efb2e9cdd6640987b5490e84471c5f43f68ae1af', 'tester', 2, NULL, 'Block-OSD', 'OSD-1', 'A-001,A-002,A-003,A-004,A-111,A-112,A-113,A-114,A-115,A-116,A-117,A-118', 'test supervisor2', 'Superadmin', '2020-01-15 05:36:41', 'Superadmin', '2020-03-16 17:32:36', b'1', b'0', 0),
(45, 'shivam001', 'Shival Saini', 'shivam.saini@smhs.motherson.com', 'Ghaziabad', 9718280028, 'shivam001', '663509b68716c8b50dd20a564763a1b1b9248cc9e6210b739818586c63e71ea1', 'test opertor', 1, NULL, 'Block-OSD', 'OSD-1', 'A-001,A-002,A-003,A-004,A-111,A-112,A-113,A-114,A-115,A-116,A-117,A-118', 'test Operator', 'Superadmin', '2020-01-15 05:38:22', 'Superadmin', '2020-01-15 11:54:40', b'1', b'0', 0),
(46, 'emp1111', 'Bhupendra kumar', 'Bhupeekr.77777@gmail.comm', 'Delhi', 9910717495, 'Bhupee', 'a5a03efcb7da3fdaf68fcb1fc2ef07ae29add8f96a9dde88b75e05afce6f8d65', 'BH', 11, 'Block Head', 'Block-OSD2', 'OSD-2', 'A-006', 'Testing Audit Trail', NULL, '2020-02-03 09:29:58', 'Superadmin', '2020-03-13 18:14:29', b'1', b'0', 0),
(47, 'emp5', 'QA', 'QA@gmail.com', 'delhi', 9988775544, 'QA', 'bf9e8e7955e8c87d5fe571ad0be25008654157a58a7a34353b81d747e8c6577a', NULL, 5, 'Role5', NULL, NULL, NULL, NULL, NULL, '2020-02-03 09:43:21', NULL, NULL, b'1', b'0', 0),
(48, 'emp-098098', 'jaggi', 'jaggi@gmail.com', 'delhi', 9876543210, 'jaggi', '58436e2f57f2d20a8e70d67e72d07371060fd0010075e973da38887b16b26781', 'op', 1, NULL, 'Block-OSD2', 'OSD-2', 'A-006', 'test', 'Superadmin', '2020-03-16 12:10:45', NULL, NULL, b'1', b'0', 0),
(49, 'ut', 'tyut', 'utuyt@hjjd.com', 'tyytt', 0, 'uyty', '4e5e83c3a8f501801831d56f9f8d9c8e7d9dd430fd142db4b73cff3bb545df21', 'ty', 11, NULL, 'Block-Inj', 'SB-inj1', 'A_195', 'tyt', 'Superadmin', '2020-03-16 12:18:07', 'Superadmin', '2020-03-16 17:48:53', b'0', b'0', 0),
(50, 'emp006', 'Ayush Kumar', 'ayush@gmail.com', 'hdgfhsdf', 9999999999, 'Ayush', 'af24ad3aad1be6e5e3edcd122145eb2f68670ba8ddd079237541dbdb26ce2c37', 'OT', 1, NULL, 'Block-OSD', 'OSD-1', 'A-116,A-117', '', 'Superadmin', '2020-04-03 05:58:28', NULL, NULL, b'1', b'0', 0),
(52, 'test001', 'testuser', 'test@gmail.com', '', 9999999999, 'test98', '23810a325304076e09bb6445c8b67a3e4f6ad8856e6cb569bea8e49a383a0a09', 'Developer', 1, NULL, 'Block-OSD', 'OSD-1', 'A-001,A-002,A-003,A-004,A-111,A-112,A-113,A-114,A-115,A-116,A-117,A-118', '', 'Superadmin', '2020-06-09 09:31:22', NULL, NULL, b'1', b'0', 0);

--
-- Triggers `mst_employee`
--
DROP TRIGGER IF EXISTS `emp_audit_log`;
DELIMITER $$
CREATE TRIGGER `emp_audit_log` AFTER UPDATE ON `mst_employee` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  IF(OLD.emp_code != NEW.emp_code)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','emp_code','update',NEW.created_on, NEW.created_by, OLD.emp_code, NEW.emp_code);
	  END IF;
	  
	  IF(OLD.emp_name != NEW.emp_name)
	  THEN	
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','emp_name','update',NEW.created_on, NEW.created_by, OLD.emp_name, NEW.emp_name);
	  END IF;
	  
	  IF(OLD.email2 != NEW.email2)
	  THEN	 
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','email2','update',NEW.created_on, NEW.created_by, OLD.email2, NEW.email2);
	  END IF;
	  
	  IF(OLD.emp_address != NEW.emp_address)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','emp_address','update',NEW.created_on, NEW.created_by, OLD.emp_address, NEW.emp_address);
	  END IF; 
	  
	  
	  IF(OLD.emp_contact != NEW.emp_contact)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','emp_contact','update',NEW.created_on, NEW.created_by, OLD.emp_contact, NEW.emp_contact);
	  END IF; 
	  
	  IF(OLD.emp_email != NEW.emp_email)
	  THEN	 
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','emp_email','update',NEW.created_on, NEW.created_by, OLD.emp_email, NEW.emp_email);
	  END IF;
	  
	  IF(OLD.emp_password != NEW.emp_password)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','emp_password','update',NEW.created_on, NEW.created_by, OLD.emp_password, NEW.emp_password);	 
	  END IF;
	  
	  IF(OLD.designation_code != NEW.designation_code)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','designation_code','update',NEW.created_on, NEW.created_by, OLD.designation_code, NEW.designation_code);	 	 
	  END IF; 
	  
	  IF(OLD.role_id != NEW.role_id)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','role_id','update',NEW.created_on, NEW.created_by, OLD.role_id, NEW.role_id);	 	 
	  END IF; 
	  
	  IF(OLD.role_code != NEW.role_code)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','role_code','update',NEW.created_on, NEW.created_by, OLD.role_code, NEW.role_code);	
	  END IF; 
	  
	  IF(OLD.block_code != NEW.block_code)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','block_code','update',NEW.created_on, NEW.created_by, OLD.block_code, NEW.block_code);
	  END IF; 
	  
	  IF(OLD.Sub_block_code != NEW.Sub_block_code)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','Sub_block_code','update',NEW.created_on, NEW.created_by, OLD.Sub_block_code, NEW.Sub_block_code);
	  END IF;
	  
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','area_code','update',NEW.created_on, NEW.created_by, OLD.area_code, NEW.area_code);
	  END IF; 
	  
	  IF(OLD.emp_remark != NEW.emp_remark)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','emp_remark','update',NEW.created_on, NEW.created_by, OLD.emp_remark, NEW.emp_remark);
	  END IF; 
	  
	  IF(OLD.is_blocked != NEW.is_blocked)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','is_blocked','update',NEW.created_on, NEW.created_by, OLD.is_blocked, NEW.is_blocked);	  
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','is_active','update',NEW.created_on, NEW.created_by, OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `emp_insert_audit_log`;
DELIMITER $$
CREATE TRIGGER `emp_insert_audit_log` AFTER INSERT ON `mst_employee` FOR EACH ROW BEGIN
INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','Employee','Insert',NEW.created_on, NEW.created_by, 'N/A', 'N/A');
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_equipment`
--

DROP TABLE IF EXISTS `mst_equipment`;
CREATE TABLE IF NOT EXISTS `mst_equipment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `equipment_code` varchar(50) DEFAULT NULL,
  `equipment_name` varchar(250) DEFAULT NULL,
  `equipment_icon` varchar(500) DEFAULT NULL,
  `equipment_type` varchar(500) DEFAULT NULL,
  `equipment_remarks` varchar(2000) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `block_code` varchar(50) DEFAULT NULL,
  `drain_point_code` varchar(50) DEFAULT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_inused` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_equipment`
--

INSERT INTO `mst_equipment` (`id`, `equipment_code`, `equipment_name`, `equipment_icon`, `equipment_type`, `equipment_remarks`, `room_code`, `block_code`, `drain_point_code`, `sop_code`, `is_active`, `created_by`, `created_on`, `modified_by`, `modified_on`, `is_inused`) VALUES
(102, 'M-1006', 'S S Tank', NULL, 'Fixed', 'Injectibles', 'PMA-64', NULL, '', 'TAB-062', b'1', 'Admin', '2020-01-15 05:23:23', 'Null', NULL, b'0'),
(125, 'SHSP-015', 'Induction sealing machine', 'Octoganal-Blender-machine2.png', 'Fixed', 'Testing', 'TAA-51,TAA-52', NULL, 'D/PKG-CPD024/001', 'CPD-999', b'1', 'Superadmin', '2020-01-15 05:23:23', NULL, NULL, b'0'),
(126, 'HC-050', 'Contabins', 'Tipper3.png', 'Fixed', NULL, 'TAA-52,TAA-53', NULL, NULL, 'CPD-028', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-10-31 00:44:57', b'0'),
(127, 'OSPE-005', 'Conta Blender-II', 'Rapid-Mixer-Granulator11.png', 'Vaccum', NULL, 'TAA-58,TAA-59', NULL, NULL, 'TAB-008', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-10-31 00:44:57', b'0'),
(128, 'OSPE-006', 'Conta Blender-II', 'Contabins7.png', 'Apparatus', '', 'TAA-58,TAA-59', 'Block-OSD', 'D/PKG-CPD024/001', 'CPD-124', b'1', 'Superadmin', '2020-01-15 05:23:23', NULL, NULL, b'0'),
(129, 'OSPE-007', 'Induction sealing machine', 'Rapid-Mixer-Granulator12.png', 'Instrument', '', 'TAA-58,TAA-59', NULL, 'D/PD-74/01', 'CPD-130', b'1', 'Superadmin', '2020-01-15 05:23:23', NULL, NULL, b'0'),
(149, 'CPD-171', 'Conta Blender', 'default.png', 'Apparatus', 'testing', 'TAA-58,TAA-59', 'Block-OSD', 'D/PD-74/01', 'CPD-124', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-11-26 14:02:03', b'0'),
(150, 'CPD-150', 'Conta Blender - II', 'default.png', 'Booth', NULL, 'TAA-58,TAA-59', NULL, NULL, 'PKG-125', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-10-31 00:44:57', b'0'),
(151, 'CPD-032-AA', 'IPC Bins - II', 'default.png', 'Fixed', '', 'TAA-78', NULL, 'D/TAA-067/01', 'CPD-124', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-11-26 12:55:53', b'1'),
(152, 'DT-017', 'Sifter', 'default.png', 'Fixed', NULL, 'TAA-51,TAA-52', NULL, NULL, 'TAB-062', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-10-31 00:44:56', b'0'),
(153, 'HC101', 'Sifter 30', 'default.png', 'Instrument', NULL, 'TAA-58,TAA-59', NULL, NULL, 'TAB-062', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-10-31 00:44:57', b'0'),
(154, 'HC102', 'Sifter 30', 'default.png', 'Fixed', NULL, 'TAA-52,TAA-53', NULL, NULL, 'CPD-028', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-10-31 00:44:57', b'0'),
(155, 'DT-014', 'Sifter Cum Multi Million', 'default.png', 'Fixed', NULL, 'TAA-52,TAA-53', NULL, NULL, 'CPD-999', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-10-31 00:44:57', b'0'),
(156, 'HC-003', 'Tipper', 'default.png', 'Fixed', NULL, 'TAA-52,TAA-53', NULL, NULL, 'CPD-022', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-10-31 00:44:57', b'0'),
(157, 'HC061', 'Conta Blender', 'default.png', 'Fixed', NULL, 'TAA-51,TAA-52', NULL, NULL, 'SS-056', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-10-31 00:44:57', b'0'),
(158, 'CPD-020', 'IPC Bins', 'default.png', 'Vaccum', NULL, 'TAA-58,TAA-59', NULL, NULL, 'CPD011', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-10-31 00:44:57', b'0'),
(159, 'CPD-187', 'Rapid Mixer Granulator', 'Sifter4.png', 'Instrument', '', 'TAA-78', NULL, 'D/PD-74/02', 'CPD-023', b'1', 'Superadmin', '2020-01-15 05:23:23', NULL, NULL, b'0'),
(160, 'CPD-188', 'Empty Capsule Sorter', 'Rapid-Mixer-Granulator2.png', 'Vaccum', '', 'TAA-58,TAA-59', 'Block-OSD', 'D/PD-75/02', 'CPD-023', b'1', 'Superadmin', '2020-01-15 05:23:23', NULL, NULL, b'0'),
(161, 'CPD-200', 'Empty Capsule Sorter (ECS)', 'Accesorry-Cleaning-small.png', 'Apparatus', 'Testing ', 'TAA-78', 'Block-OSD', 'KHP/DP/100', 'CPD-016', b'1', 'Superadmin', '2020-01-15 05:23:23', NULL, NULL, b'0'),
(162, 'CPD-157', 'Fluid Bed Dryer', 'Sifter5.png', 'Fixed', 'Testing', 'TAA-80', 'Block-OSD', 'D/SH-033/01', 'CPD-017', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2020-01-15 11:51:13', b'0'),
(163, 'CPD-035', 'Octagonal Blender  machine-II', 'Tipper4.png', 'Fixed', 'testing', 'TAA-79', NULL, 'D/SH-033/01', 'CPD-999', b'1', 'Superadmin', '2020-01-15 05:23:23', NULL, NULL, b'0'),
(164, 'SH-282-15L-A', 'Manual Counter', 'type-B-small.png', 'Portable', 'Testing the portable change', '', 'Block-OSD', 'D/PD-75/01', 'CPD-124', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-12-06 22:29:14', b'1'),
(165, 'CPD-198', 'Dynamic pass box', 'Sifter6.png', 'Fixed', 'testing portable equipment change', 'TAA-80', 'Block-OSD', 'D/PD-74/01', 'TAB-023', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2020-01-15 11:51:37', b'0'),
(166, 'SH-282-10L', 'Manual counter', 'Tipper5.png', 'Portable', 'test', '', 'Block-OSD', '', 'CPD-017', b'1', 'Superadmin', '2020-01-15 05:23:23', NULL, NULL, b'0'),
(167, 'KHP/001', 'RMG(400Lts)', 'default.png', 'Fixed', '', 'TAA-80', 'Block-OSD', 'KHP/DP/102', 'CPD-019', b'1', 'Superadmin', '2020-01-15 06:23:59', 'Superadmin', '2020-01-15 12:08:12', b'0'),
(168, 'Vaccum_001', 'Vaccum_Name_test', 'default.png', 'Vaccum', 'fjhwttw', NULL, 'Block-OSD2', 'Drain5', 'SOP_code_test', b'1', 'Superadmin', '2020-02-24 08:24:36', 'Superadmin', '2020-02-24 13:56:12', b'0'),
(169, 'CPD-032-A', 'IPC Bins - III', 'default.png', 'Fixed', '', 'TAA-78', NULL, 'D/TAA-067/01', 'CPD-124', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-11-26 12:55:53', b'0'),
(170, 'DISP1', 'DISP001', 'default.png', 'Booth', 'sdsds', 'TAA-78', 'Block-OSD', 'sdsds', 'sadssd', b'1', 'Superadmin', '2020-04-09 08:45:34', NULL, NULL, b'0'),
(171, 'CPD-033-A', 'IPC Bins - Iv', 'default.png', 'Fixed', '', 'TAA-78', NULL, 'D/TAA-067/01', 'CPD-124', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-11-26 12:55:53', b'1'),
(172, 'CPD-034-A', 'IPC Bins - v', 'default.png', 'Fixed', '', 'TAA-78', NULL, 'D/TAA-067/01', 'CPD-124', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-11-26 12:55:53', b'0'),
(173, 'CPD-036-AA', 'IPC Bins - hh', 'default.png', 'Fixed', '', 'TAA-78', NULL, 'D/TAA-067/01', 'CPD-124', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-11-26 12:55:53', b'0'),
(174, 'CPD-02022', 'IPC Bins 2', 'default.png', 'Vaccum', NULL, 'TAA-58,TAA-59', NULL, NULL, 'CPD011', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-10-31 00:44:57', b'0'),
(175, 'CPD-037-A', 'IPC Bins - Ij', 'default.png', 'Fixed', '', 'TAA-78', NULL, 'D/TAA-067/01', 'CPD-124', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-11-26 12:55:53', b'0'),
(176, 'CPD-038-A', 'IPC Bins - GT', 'default.png', 'Fixed', '', 'TAA-78', NULL, 'D/TAA-067/01', 'CPD-124', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-11-26 12:55:53', b'0'),
(177, 'CPD-039-A', 'IPC Bins - TY', 'default.png', 'Fixed', '', 'TAA-78', NULL, 'D/TAA-067/01', 'CPD-124', b'1', 'Superadmin', '2020-01-15 05:23:23', 'Superadmin', '2019-11-26 12:55:53', b'0');

--
-- Triggers `mst_equipment`
--
DROP TRIGGER IF EXISTS `au_equipmentactivity`;
DELIMITER $$
CREATE TRIGGER `au_equipmentactivity` AFTER UPDATE ON `mst_equipment` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_equipment',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.equipment_code != NEW.equipment_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'equipment_code', OLD.equipment_code, NEW.equipment_code);
	  END IF;
	  
	  IF(OLD.equipment_name != NEW.equipment_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'equipment_name', OLD.equipment_name, NEW.equipment_name);
	  END IF;
	  
	  IF(OLD.equipment_icon != NEW.equipment_icon)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'equipment_icon', OLD.equipment_icon, NEW.equipment_icon);
	  END IF;
	  
	 IF(OLD.equipment_type != NEW.equipment_type)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'equipment_type', OLD.equipment_type, NEW.equipment_type);
	  END IF; 
	  
	  
	  IF(OLD.equipment_remarks != NEW.equipment_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'equipment_remarks', OLD.equipment_remarks, NEW.equipment_remarks);
	  END IF; 
	  
	  
	  IF(OLD.room_code != NEW.room_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_code', OLD.room_code, NEW.room_code);
	  END IF; 
          
	  IF(OLD.drain_point_code != NEW.drain_point_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'drain_point_code', OLD.drain_point_code, NEW.drain_point_code);
	  END IF; 
	  
	  IF(OLD.sop_code != NEW.sop_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_code', OLD.sop_code, NEW.sop_code);
	  END IF; 
	  
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_equipment', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_equipment-old`
--

DROP TABLE IF EXISTS `mst_equipment-old`;
CREATE TABLE IF NOT EXISTS `mst_equipment-old` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `equipment_code` varchar(50) DEFAULT NULL,
  `equipment_name` varchar(250) DEFAULT NULL,
  `equipment_icon` varchar(500) DEFAULT NULL,
  `equipment_type` varchar(500) DEFAULT NULL,
  `equipment_remarks` varchar(2000) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `drain_point_code` varchar(50) DEFAULT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_holiday`
--

DROP TABLE IF EXISTS `mst_holiday`;
CREATE TABLE IF NOT EXISTS `mst_holiday` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `holiday_code` varchar(50) DEFAULT NULL,
  `holiday_name` varchar(250) DEFAULT NULL,
  `holiday_date` datetime DEFAULT NULL,
  `holiday_type` varchar(500) DEFAULT NULL,
  `holiday_remark` varchar(2000) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_holiday`
--
DROP TRIGGER IF EXISTS `au_holidayactivity`;
DELIMITER $$
CREATE TRIGGER `au_holidayactivity` AFTER UPDATE ON `mst_holiday` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_holiday',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.holiday_code != NEW.holiday_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'holiday_code', OLD.holiday_code, NEW.holiday_code);
	  END IF;
	  
	  IF(OLD.holiday_name != NEW.holiday_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'holiday_name', OLD.holiday_name, NEW.holiday_name);
	  END IF;
	  
	  IF(OLD.holiday_date != NEW.holiday_date)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'holiday_date', OLD.holiday_date, NEW.holiday_date);
	  END IF;
	  
	 IF(OLD.holiday_type != NEW.holiday_type)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'holiday_type', OLD.holiday_type, NEW.holiday_type);
	  END IF; 
	  
	  
	  IF(OLD.holiday_remark != NEW.holiday_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'holiday_remark', OLD.holiday_remark, NEW.holiday_remark);
	  END IF; 
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_holiday', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_inj_cleaning`
--

DROP TABLE IF EXISTS `mst_inj_cleaning`;
CREATE TABLE IF NOT EXISTS `mst_inj_cleaning` (
  `id` int(20) NOT NULL,
  `description` varchar(300) NOT NULL,
  `type` varchar(100) NOT NULL,
  `seq` int(10) NOT NULL,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_inj_cleaning`
--

INSERT INTO `mst_inj_cleaning` (`id`, `description`, `type`, `seq`, `priority`) VALUES
(0, 'With WFI', 'with WFI', -1, 3),
(1, 'LAF/FFM Ceiling/HEPA Filter Holding Frame,Light Fixture Grade-A $ Daily (Before Use)', 'Daily', 1, 1),
(2, 'Inner Side door/guard of equipment and inner side of glove part grade -A $ daily(Before Use)', 'Daily', 2, 1),
(3, '@ Equipment Surface/ Plate Form and attached change parts Grade A $ Daily (Before Use)', 'Daily', 3, 1),
(4, 'Curtain/Rigid partition $ Daily (Before Use)', 'Daily', 4, 1),
(5, 'Ceiling/ Light Fixture Grade B $ Weekly (Tuesday)', 'Weekly', 5, 2),
(6, 'HEPA Supply and Return Air Grill (internal) & Fortnightly (14th and 28th Day of month)', 'Fortnightly', 7, 3),
(7, 'HEPA Supply and Return Air Grill (External) & weekly (Tuesday)', 'Weekly', 6, 2),
(8, 'Wall/View Panel Grade-B $ weekly (Tuesday)', 'Weekly', 8, 2),
(9, 'Door / Door Closure $ weekly (Tuesday)', 'Weekly', 9, 2),
(10, '@ Equipment, Trolley/ Discard Bin $ Daily (After Use) / $ Weekly(Tuesday)', 'Daily', 10, 1),
(11, 'SS Furniture/ SS Cupboard/ SS Plat (External) $ Daily ', 'Daily ', 11, 1),
(12, 'SS Furniture/ SS Cupboard/ SS Plat (Internal) $ Monthly (28th Day of Month) ', 'Monthly', 12, 4),
(13, 'Utility Pendant/ Utility Pipeline $ Weekly (Tuesday)', 'Weekly', 13, 2),
(14, 'Floor $ (Daily)', 'Daily', 14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mst_inj_solution`
--

DROP TABLE IF EXISTS `mst_inj_solution`;
CREATE TABLE IF NOT EXISTS `mst_inj_solution` (
  `id` bigint(20) NOT NULL,
  `solution_code` varchar(50) DEFAULT NULL,
  `solution_name` varchar(500) DEFAULT NULL,
  `Block_Code` varchar(50) DEFAULT NULL,
  `Precautions` varchar(1000) DEFAULT NULL,
  `Short_Name` varchar(200) DEFAULT NULL,
  `sol_qty` varchar(200) DEFAULT NULL,
  `water_qty` varchar(200) DEFAULT NULL,
  `wfi_qs` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_inj_solution`
--

INSERT INTO `mst_inj_solution` (`id`, `solution_code`, `solution_name`, `Block_Code`, `Precautions`, `Short_Name`, `sol_qty`, `water_qty`, `wfi_qs`, `is_active`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(41, 'DIS_0041', '50% hydrogen peroxide 6% solution', 'Block-B', '', '50% hydrogen peroxide 6% solution', '1200', '8000', '10000 ', b'1', 'Admin', '2020-01-15 05:23:26', 'Null', NULL),
(42, 'DIS_0042', '50% hydrogen peroxide 4% solution', 'Block-B', '', '50% hydrogen peroxide 4% solution', '800', '8000', '10000 ', b'1', 'Admin', '2020-01-15 05:23:26', 'Null', NULL),
(43, 'DIS_0043', '50% hydrogen peroxide 2% solution', 'Block-B', '', '50% hydrogen peroxide 2% solution', '400', '8000', '10000', b'1', 'Admin', '2020-01-15 05:23:26', 'Null', NULL),
(44, 'DIS_0044', '32% hydrogen peroxide 6% solution', 'Block-B', '', '32% hydrogen peroxide 6% solution', '1875', '7000', '10000', b'1', 'Admin', '2020-01-15 05:23:26', 'Null', NULL),
(45, 'DIS_0045', '32% hydrogen peroxide 4% solution', 'Block-B', '', '32% hydrogen peroxide 4% solution', '1250', '7000', '10000', b'1', 'Admin', '2020-01-15 05:23:26', 'Null', NULL),
(46, 'DIS_0046', '32% hydrogen peroxide 2% solution', 'Block-B', '', '32% hydrogen peroxide 2% solution', '625', '5000', '10000', b'1', 'Admin', '2020-01-15 05:23:26', 'Null', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mst_messages`
--

DROP TABLE IF EXISTS `mst_messages`;
CREATE TABLE IF NOT EXISTS `mst_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg-header` varchar(50) NOT NULL,
  `message` varchar(500) DEFAULT NULL,
  `created_by` varchar(200) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_messages`
--

INSERT INTO `mst_messages` (`id`, `msg-header`, `message`, `created_by`, `created_on`, `modified_by`, `modified_on`, `is_active`) VALUES
(1, 'Start', 'Batch operation is started successfully.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(3, 'Checked By', 'Batch operation is successfully taken over by you.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(4, 'Stop', 'Batch operation is successfully completed.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(5, 'Start', 'Type A cleaning is successfully started.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(6, 'Checked By', 'Type-B Cleaning is successfully taken over by you.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(7, 'Stop', 'Type-B cleaning is successfully completed.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(8, 'Checked By', 'Type-A Cleaning is successfully taken over by you.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(9, 'Stop', 'Type-A cleaning is successfully completed.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(10, 'Start', 'Portable equipments cleaning is successfully started.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(11, 'Stop', 'Portable equipments cleaning is successfully completed.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(12, 'Checked By', 'Portable equipments cleaning is successfully taken over by you.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(13, 'Start', 'In-process Container cleaning is successfully started.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(14, 'Stop', 'In-process container cleaning is successfully completed.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(15, 'Checked By', 'In-process container cleaning is successfully taken over by you.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(16, 'Stop', 'Daily cleaning is successfully completed.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(17, 'Start', 'Daily cleaning is successfully started.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(18, 'Checked By', 'Daily cleaning is successfully taken oevr by you.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(19, 'Submission', 'Solution Preparation form is successfully submitted.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(20, 'Checked By', 'Solution Preparation is successfully taken over by you.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(21, 'Destroy Solution', 'Remaining solution is successfully destroyed ', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(22, 'Start', 'Drain points cleaning is successfully started.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(23, 'Stop', 'Drain points cleaning is successfully completed.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(24, 'Checked By', 'Drain points cleaning is successfully taken over by you.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(25, 'Start', 'Accessory cleaning is successfully started.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(26, 'Stop', 'Accessory cleaning is successfully completed.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(27, 'Checked By', 'Accessory cleaning is successfully taken over by you.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(28, 'Warning', 'This will finish the current task. Do you really want to finish it?', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(29, 'Exception', 'The operation for this lot is already finished successfully.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(30, 'Error', 'User id or paasword is incorrect. ', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(31, 'Request', 'You must change your password using the Profile option in the system. Remember the password. Next time when you login use this password.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(32, 'Error', 'You cannot finish an operation when there is breakdown. First close the breakdown.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(33, 'Error', 'You cannot log a breakdown without taking over.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(34, 'Sequential Log for Area and Equipments', 'There is no data in the database tables.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(35, 'Sequential Log for Area and Equipments', 'The cleaning time has exceeded. Please do cleaning again.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(36, 'Block Master Maintenance', 'Missing Block data', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(37, 'Block Master Maintenance', 'Block data updated', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(38, 'Block Master Maintenance', 'Block data added', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(39, 'Block Master Maintenance', 'Block data deleted', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(40, 'Role master', 'Role data deleted', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(41, 'Role master', 'Role data added', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(42, 'Role master', 'Role data deleted', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(43, 'Role master', 'Missing role data', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(44, 'Start', 'Type B cleaning is successfully started.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(45, 'Exception handing', 'This is a dummy message 9991', 'Admin', '2020-01-15 05:23:27', 'Admin', '2019-10-05 11:30:29', b'0'),
(46, 'Activity Master', 'Activity Successfully Saved', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(47, 'Activity Master', 'Activity Successfully Deleted', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(48, 'Blank data', 'Opps Please Contact Administrator.We are facing issued with Data validity Or Blank Data in following Table(s)', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(49, 'ERR_solutionPreparation', 'No valid solutions are defined in the Solution Master table. Please contact administrator.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(50, 'ERR_drainPoint', 'Drain Points have not been defined for this room. Please contact the administrator.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(51, 'ERR_drainPointsolstock', 'No valid solution is available for cleaning. Either the solutions do not exist or have expired.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(55, 'ERR_dailyCleaning', 'Either there is no solution in Solution Stock or it has crossed its expiry date. Please create a new solution.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(56, 'ERR_accessoryCleaning', 'No Accessories avaialable', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(57, 'ERR_inprocessContainercleaning', 'No Equipments avaialable', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(58, 'ERR_docid', 'Document number does not exist in Document master. Please contact administrator.', 'Admin', '2020-01-15 05:23:27', NULL, NULL, b'1'),
(59, 'ERR_InEquipmentNotFoundByRoomcode', 'Either equipments or SOPs for equipment do not exist in master tables. Please contact administrator. ', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(60, 'ERR_AreaNotfoundByBlockcode', 'This block does not have any areas. Please contact area administrator.', NULL, '2020-01-15 05:23:27', NULL, NULL, b'1'),
(61, 'ERR_ActivityNotFound', 'This activity is not defined for this area. Please contact the administrator.', 'Admin', '2020-01-15 05:23:27', NULL, NULL, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `mst_plant`
--

DROP TABLE IF EXISTS `mst_plant`;
CREATE TABLE IF NOT EXISTS `mst_plant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `plant_code` varchar(50) DEFAULT NULL,
  `plant_name` varchar(250) DEFAULT NULL,
  `plant_address` varchar(2000) DEFAULT NULL,
  `plant_remarks` varchar(4000) DEFAULT NULL,
  `plant_contact` bigint(20) DEFAULT NULL,
  `number_of_blocks` int(11) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_plant`
--

INSERT INTO `mst_plant` (`id`, `plant_code`, `plant_name`, `plant_address`, `plant_remarks`, `plant_contact`, `number_of_blocks`, `is_active`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(4, 'PL001', 'Halol', '302b Jai Bhavani CBS\r\nGD Ambedkar Road, Baroda, Gujarat.', 'Added by Sanchayita for testing', 9999089900, 5, b'1', 'Admin', '2020-01-15 05:23:28', 'Superadmin', '2020-04-03 17:50:36'),
(5, 'p001', 'plant test', 'Delhi', 'test', 99887766, 1, b'0', 'Superadmin', '2020-03-11 06:08:11', 'Superadmin', '2020-03-11 12:30:03');

--
-- Triggers `mst_plant`
--
DROP TRIGGER IF EXISTS `au_plantactivity`;
DELIMITER $$
CREATE TRIGGER `au_plantactivity` AFTER UPDATE ON `mst_plant` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_plant',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	 IF(OLD.plant_code != NEW.plant_code)
	  THEN
	  
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_code', OLD.plant_code, NEW.plant_code);
	  END IF;
	  IF(OLD.plant_name != NEW.plant_name)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_name', OLD.plant_name, NEW.plant_name);
	  END IF; 
	  
	  IF(OLD.plant_address != NEW.plant_address)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_address', OLD.plant_address, NEW.plant_address);
	  END IF; 
	  
	  IF(OLD.plant_contact != NEW.plant_contact)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_contact', OLD.plant_contact, NEW.plant_contact);
	  END IF; 
	  
	  
	  IF(OLD.plant_remarks != NEW.plant_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_remarks', OLD.plant_remarks, NEW.plant_remarks);
	  END IF; 
	  
	  
	  IF(OLD.number_of_blocks != NEW.number_of_blocks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'number_of_blocks', OLD.number_of_blocks, NEW.number_of_blocks);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_plant', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_procedure`
--

DROP TABLE IF EXISTS `mst_procedure`;
CREATE TABLE IF NOT EXISTS `mst_procedure` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `procedure_code` varchar(50) DEFAULT NULL,
  `procedure_name` varchar(1000) DEFAULT NULL,
  `solution_code` varchar(50) DEFAULT NULL,
  `is_active` bit(1) DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_product`
--

DROP TABLE IF EXISTS `mst_product`;
CREATE TABLE IF NOT EXISTS `mst_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(50) DEFAULT NULL,
  `product_name` varchar(500) DEFAULT NULL,
  `product_market` varchar(500) DEFAULT NULL,
  `product_remark` varchar(2000) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=736 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_product`
--

INSERT INTO `mst_product` (`id`, `product_code`, `product_name`, `product_market`, `product_remark`, `created_by`, `created_on`, `modified_by`, `modified_on`, `is_active`) VALUES
(724, 'PL001', 'METFORMIN HCl EXTENDED RELEASE TABLETS', 'India', 'Testing', 'Superadmin', '2020-01-15 05:23:30', NULL, NULL, b'1'),
(725, 'PL002', 'ONDANSETRON HYDROCHLORIDE TABLETS, 8MG\'', 'India', 'Testing', 'Superadmin', '2020-01-15 05:23:30', NULL, NULL, b'1'),
(726, '0011001A', 'METFORMIN HYDROCHLORIDE TABLETS, USP 1000MG', 'India', NULL, 'Superadmin', '2020-01-15 05:23:30', NULL, NULL, b'1'),
(727, '0011002A', 'ONDANSETRON HYDROCHLORIDE TABLETS, 4MG', 'India', NULL, 'Superadmin', '2020-01-15 05:23:30', 'Superadmin', '2019-10-31 01:07:12', b'1'),
(728, '0011047B', 'CARBIDOPA AND LEVODOPA EXTENDED RELEASE TABLETS 25 MG / 100 MG', 'India', NULL, 'Superadmin', '2020-01-15 05:23:30', 'Superadmin', '2019-10-31 01:07:12', b'1'),
(729, '0011055A', 'RIVASTIGMINE TARTRATE CAPSULES 4.5MG', 'India', NULL, 'Superadmin', '2020-01-15 05:23:30', 'Superadmin', '2019-10-31 01:07:12', b'1'),
(730, '0011055B', 'Desmopressin Acetate Nasal solution, 0.01 (Nasal spray), 5ml/vial', 'Ireland', NULL, 'Superadmin', '2020-01-15 05:23:30', 'Superadmin', '2019-10-31 01:07:12', b'1'),
(731, '0011058B', 'ONDANSETRON ORALLY DISINTEGRATING TABLETS,4MG', 'Ireland', NULL, 'Superadmin', '2020-01-15 05:23:30', 'Superadmin', '2019-10-31 01:07:12', b'1'),
(732, '0011060C', 'METFORMIN HCl EXTENDED RELEASE TABLETS, 750mg', 'Ireland', '', 'Superadmin', '2020-01-15 05:23:30', 'Superadmin', '2020-05-02 14:09:21', b'1'),
(733, '0011061D', 'OXCARBAZEPINE TABLETS,600MG', 'Ireland', NULL, 'Superadmin', '2020-01-15 05:23:30', 'Superadmin', '2019-10-31 01:07:12', b'1'),
(734, '0011119B', 'CARBIDOPA AND LEVODOPA ORALLY DISINTEGRATING TABLETS, 25MG/100MG', 'Ireland', NULL, 'Superadmin', '2020-01-15 05:23:30', 'Superadmin', '2019-10-31 01:07:12', b'1'),
(735, '0011001D', 'METFORMIN HYDROCHLORIDE TABLETS, USP 1000MG', 'Ireland', NULL, 'Superadmin', '2020-01-15 05:23:30', 'Superadmin', '2019-10-31 01:07:12', b'1');

--
-- Triggers `mst_product`
--
DROP TRIGGER IF EXISTS `au_productactivity`;
DELIMITER $$
CREATE TRIGGER `au_productactivity` AFTER UPDATE ON `mst_product` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_product',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.product_code != NEW.product_code)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'product_code', OLD.product_code, NEW.product_code);
	  END IF;
	  IF(OLD.product_name != NEW.product_name)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'product_name', OLD.product_name, NEW.product_name);
	  END IF;
	  
	  IF(OLD.product_market != NEW.product_market)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'product_market', OLD.product_market, NEW.product_market);
	  END IF;
	  IF(OLD.product_remark != NEW.product_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'product_remark', OLD.product_remark, NEW.product_remark);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_product', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_product_process`
--

DROP TABLE IF EXISTS `mst_product_process`;
CREATE TABLE IF NOT EXISTS `mst_product_process` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `process_code` varchar(50) DEFAULT NULL,
  `process_name` varchar(500) DEFAULT NULL,
  `process_type` varchar(500) DEFAULT NULL,
  `process_duration` varchar(200) DEFAULT NULL,
  `process_remark` varchar(2000) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_role`
--

DROP TABLE IF EXISTS `mst_role`;
CREATE TABLE IF NOT EXISTS `mst_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_level` int(11) DEFAULT NULL,
  `role_code` varchar(50) DEFAULT NULL,
  `role_type` varchar(500) DEFAULT NULL,
  `role_description` varchar(2000) DEFAULT NULL,
  `role_remark` varchar(2000) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_role`
--

INSERT INTO `mst_role` (`id`, `role_level`, `role_code`, `role_type`, `role_description`, `role_remark`, `created_by`, `created_on`, `modified_by`, `modified_on`, `is_active`) VALUES
(1, 1, 'Role1', '', 'Operator', '', 'Admin', '2020-01-15 05:23:31', NULL, '2019-09-04 11:00:00', 1),
(2, 2, 'Role2', '', 'Area Supervisor', '', 'Admin', '2020-01-15 05:23:31', NULL, '2019-09-04 11:00:00', 1),
(3, 5, 'Role3', '', 'Quatity Control', '', 'Admin', '2020-01-15 05:23:31', NULL, '2019-09-04 11:00:00', 1),
(4, 3, 'Role4', '', 'Production Manager', '', 'Admin', '2020-01-15 05:23:31', NULL, '2019-09-04 11:00:00', 1),
(5, 6, 'Role5', '', 'Quality Assurance', '', 'Admin', '2020-01-15 05:23:31', NULL, '2019-09-04 11:00:00', 1),
(6, NULL, 'Role6', NULL, 'Super Admin', NULL, 'Admin', '2020-01-15 05:23:31', NULL, '2019-09-04 11:00:00', 2),
(11, 4, 'Role7', NULL, 'BlockHead', NULL, 'Admin', '2020-01-31 06:53:08', NULL, NULL, 1);

--
-- Triggers `mst_role`
--
DROP TRIGGER IF EXISTS `au_roleactivity`;
DELIMITER $$
CREATE TRIGGER `au_roleactivity` AFTER UPDATE ON `mst_role` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_role',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.role_code != NEW.role_code)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'role_code', OLD.role_code, NEW.role_code);
	  END IF;
	  IF(OLD.role_type != NEW.role_type)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'role_type', OLD.role_type, NEW.role_type);
	  END IF;
	  
	  IF(OLD.role_description != NEW.role_description)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'role_description', OLD.role_description, NEW.role_description);
	  END IF;
	  IF(OLD.role_remark != NEW.role_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'role_remark', OLD.role_remark, NEW.role_remark);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_role', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_roleid_workflow_mapping`
--

DROP TABLE IF EXISTS `mst_roleid_workflow_mapping`;
CREATE TABLE IF NOT EXISTS `mst_roleid_workflow_mapping` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(500) NOT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_roleid_workflow_mapping`
--

INSERT INTO `mst_roleid_workflow_mapping` (`id`, `status_id`, `role_id`, `created_on`, `created_by`, `modified_by`, `modified_on`, `is_active`) VALUES
(23, 1, 1, '2020-01-15 05:23:32', 'Superadmin', 'Superadmin', '2019-10-10 22:35:47', b'0'),
(24, 2, 1, '2020-01-15 05:23:32', 'Superadmin', NULL, NULL, b'1'),
(25, 3, 2, '2020-01-15 05:23:32', 'Superadmin', NULL, NULL, b'1'),
(26, 5, 3, '2020-01-15 05:23:32', 'Superadmin', NULL, NULL, b'1'),
(27, 4, 4, '2020-01-15 05:23:32', 'Superadmin', NULL, NULL, b'1'),
(28, 6, 5, '2020-01-15 05:23:32', 'Superadmin', NULL, NULL, b'1'),
(29, 7, 11, '2020-01-31 11:10:03', 'SuperAdmin', NULL, NULL, b'1');

--
-- Triggers `mst_roleid_workflow_mapping`
--
DROP TRIGGER IF EXISTS `au_roleworkflowactivity`;
DELIMITER $$
CREATE TRIGGER `au_roleworkflowactivity` AFTER UPDATE ON `mst_roleid_workflow_mapping` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_roleid_workflow_mapping',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.status_id != NEW.status_id)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'status_id', OLD.status_id, NEW.status_id);
	  END IF;
	  IF(OLD.role_id != NEW.role_id)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'role_id', OLD.role_id, NEW.role_id);
	  END IF;	  
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_roleid_workflow_mapping', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_room`
--

DROP TABLE IF EXISTS `mst_room`;
CREATE TABLE IF NOT EXISTS `mst_room` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `room_code` varchar(50) DEFAULT NULL,
  `room_name` varchar(250) DEFAULT NULL,
  `room_address` varchar(4000) DEFAULT NULL,
  `room_remarks` varchar(2000) DEFAULT NULL,
  `room_contact` bigint(20) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `in_use` int(1) DEFAULT '0',
  `inuse_activity` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_room`
--

INSERT INTO `mst_room` (`id`, `room_code`, `room_name`, `room_address`, `room_remarks`, `room_contact`, `area_code`, `is_active`, `created_by`, `created_on`, `modified_by`, `modified_on`, `in_use`, `inuse_activity`) VALUES
(42, 'PMA-64', 'Injectibles room 1', NULL, 'Injectibles room 1', NULL, 'A_195', b'0', 'Superadmin', '2020-01-15 05:23:33', 'Superadmin', '2020-04-05 13:55:37', 0, NULL),
(43, 'PMA-97', 'Injectibles room 2', NULL, 'Injectibles room 2', NULL, 'A_195', b'1', 'Superadmin', '2020-01-15 05:23:33', NULL, NULL, 0, NULL),
(44, 'PMA-51', 'Injectibles room 3', NULL, 'Injectibles room 3', NULL, 'A_195', b'1', 'Superadmin', '2020-01-15 05:23:33', NULL, NULL, 0, NULL),
(45, 'PMA-102', 'Injectibles room 4', NULL, 'Injectibles room 4', NULL, 'A_195', b'1', 'Superadmin', '2020-01-15 05:23:33', NULL, NULL, 0, NULL),
(46, 'PMA-57', 'Injectibles room 5', NULL, 'Injectibles room 5', NULL, 'A_195', b'1', 'Superadmin', '2020-01-15 05:23:33', NULL, NULL, 0, NULL),
(47, 'PMA-101', 'Injectibles room 6', NULL, 'Injectibles room 6', NULL, 'A_195', b'1', 'Superadmin', '2020-01-15 05:23:33', NULL, NULL, 0, NULL),
(48, 'PMA-158', 'Injectibles room 7', NULL, 'Injectibles room 7', NULL, 'A_195', b'1', 'Superadmin', '2020-01-15 05:23:33', NULL, NULL, 0, NULL),
(49, 'PMA-159', 'Injectibles room 8', NULL, 'Injectibles room 8', NULL, 'A_195', b'1', 'Superadmin', '2020-01-15 05:23:33', NULL, NULL, 0, NULL),
(50, 'TAA-51,TAA-52', 'Gel Mass Area room1', NULL, 'testing', NULL, 'A-001', b'1', 'Superadmin', '2020-01-15 05:23:33', 'Superadmin', '2019-10-29 12:51:47', 1, 'In process Container Cleaning In Progress'),
(51, 'TAA-52,TAA-53', 'Gel Mass Area room2', NULL, 'Testing', NULL, 'A-001', b'1', 'Superadmin', '2020-01-15 05:23:33', 'Superadmin', '2019-12-18 17:59:54', 0, NULL),
(52, 'TAA-58,TAA-59', 'FBE Room 1', NULL, '', NULL, 'A-002', b'1', 'Superadmin', '2020-01-15 05:23:33', 'Superadmin', '2019-12-18 17:59:32', 1, 'Operation is in progress'),
(53, 'TAA-78', 'Room 1 for coating ', NULL, 'Test new block', NULL, 'A-006', b'1', 'Superadmin', '2020-01-15 05:23:33', 'Superadmin', '2019-12-18 18:15:10', 0, NULL),
(54, 'TAA-79', 'FBE Room2', NULL, 'Testing', NULL, 'A-002', b'1', 'Superadmin', '2020-01-15 05:23:33', 'Superadmin', '2019-12-18 13:57:36', 1, 'Type A is in progress'),
(55, 'TAA-80', 'FBE Room3', NULL, 'test', NULL, 'A-003', b'1', 'Superadmin', '2020-01-15 05:23:33', 'Superadmin', '2020-01-15 14:37:16', 0, NULL),
(56, 'IPQA', 'IPQA', NULL, '', NULL, 'A-006', b'1', 'Superadmin', '2020-03-03 06:49:17', NULL, NULL, 0, NULL),
(57, 'ttestroom001', 'test new room', NULL, 'jdgjsafsjdf', NULL, 'A-116', b'0', 'Superadmin', '2020-04-03 05:42:12', 'Superadmin', '2020-04-05 13:54:53', 0, NULL);

--
-- Triggers `mst_room`
--
DROP TRIGGER IF EXISTS `au_roomactivity`;
DELIMITER $$
CREATE TRIGGER `au_roomactivity` AFTER UPDATE ON `mst_room` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_room',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.room_code != NEW.room_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_code', OLD.room_code, NEW.room_code);
	  END IF;
	  
	  IF(OLD.room_name != NEW.room_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_name', OLD.room_name, NEW.room_name);
	  END IF;
	  
	  IF(OLD.room_address != NEW.room_address)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_address', OLD.room_address, NEW.room_address);
	  END IF;
	  
	 IF(OLD.room_contact != NEW.room_contact)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_contact', OLD.room_contact, NEW.room_contact);
	  END IF; 
	  
	  
	  IF(OLD.room_remarks != NEW.room_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_remarks', OLD.room_remarks, NEW.room_remarks);
	  END IF; 
	  
	  
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_code', OLD.area_code, NEW.area_code);
	  END IF; 
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_room', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_shift`
--

DROP TABLE IF EXISTS `mst_shift`;
CREATE TABLE IF NOT EXISTS `mst_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shift_code` varchar(50) DEFAULT NULL,
  `shift_starttime` datetime DEFAULT NULL,
  `shift_endtime` datetime DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_solution`
--

DROP TABLE IF EXISTS `mst_solution`;
CREATE TABLE IF NOT EXISTS `mst_solution` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `solution_code` varchar(50) DEFAULT NULL,
  `solution_name` varchar(500) DEFAULT NULL,
  `Block_Code` varchar(50) DEFAULT NULL,
  `Precautions` varchar(1000) DEFAULT NULL,
  `Short_Name` varchar(200) DEFAULT NULL,
  `sol_qty` varchar(200) DEFAULT NULL,
  `water_qty` varchar(200) DEFAULT NULL,
  `tot_qty` varchar(200) DEFAULT NULL,
  `UoM` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_solution`
--

INSERT INTO `mst_solution` (`id`, `solution_code`, `solution_name`, `Block_Code`, `Precautions`, `Short_Name`, `sol_qty`, `water_qty`, `tot_qty`, `UoM`, `is_active`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(49, 'DIS_0001', 'Dettol 98%', 'Block-OSD', '', 'Dettol', '0.35', '25.36', '30', 'ml', b'1', 'Superadmin', '2020-01-15 05:23:36', NULL, NULL),
(50, 'DIS_0002', 'Envodil 23% with water', 'Block-OSD', 'Allow the solution to cool', 'Envodil', '0.25', '25.36', '28.00', 'ml', b'1', 'Superadmin', '2020-01-15 05:23:36', NULL, NULL),
(51, 'DIS_0003', 'Hemtop 98% diluted HCL', 'Block-OSD', 'Allow the solution to cool', 'Hemtop', '25', '5', '30', 'ml', b'1', 'Superadmin', '2020-01-15 05:23:36', NULL, NULL),
(52, 'DIS-004', 'Preparation of 0.25% v/v Hypo-Chlor Solution', 'Block-OSD2', '', 'Hypo-Chlor 5.25%', '10', '100', '125', 'ml', b'1', 'Superadmin', '2020-01-15 05:23:36', NULL, NULL);

--
-- Triggers `mst_solution`
--
DROP TRIGGER IF EXISTS `au_solutionactivity`;
DELIMITER $$
CREATE TRIGGER `au_solutionactivity` AFTER UPDATE ON `mst_solution` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_solution',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.solution_code != NEW.solution_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'solution_code', OLD.solution_code, NEW.solution_code);
	  END IF;
	  
	  IF(OLD.solution_name != NEW.solution_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'solution_name', OLD.solution_name, NEW.solution_name);
	  END IF;
	  
	  IF(OLD.Block_Code != NEW.Block_Code)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'Block_Code', OLD.Block_Code, NEW.Block_Code);
	  END IF;
	  
	 IF(OLD.Precautions != NEW.Precautions)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'Precautions', OLD.Precautions, NEW.Precautions);
	  END IF; 
	  
	  IF(OLD.Short_Name != NEW.Short_Name)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'Short_Name', OLD.Short_Name, NEW.Short_Name);
	  END IF;
	  IF(OLD.sol_qty != NEW.sol_qty)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sol_qty', OLD.sol_qty, NEW.sol_qty);
	  END IF;
	  IF(OLD.water_qty != NEW.water_qty)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'water_qty', OLD.water_qty, NEW.water_qty);
	  END IF;
	  IF(OLD.tot_qty != NEW.tot_qty)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'tot_qty', OLD.tot_qty, NEW.tot_qty);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_solution', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_sop`
--

DROP TABLE IF EXISTS `mst_sop`;
CREATE TABLE IF NOT EXISTS `mst_sop` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sop_code` varchar(50) DEFAULT NULL,
  `sop_name` varchar(500) DEFAULT NULL,
  `sop_type` varchar(500) DEFAULT NULL,
  `sop_frequency` varchar(500) DEFAULT NULL,
  `sop_remark` varchar(2000) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `activity_code` varchar(50) DEFAULT NULL,
  `filepath` varchar(1000) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=743 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_sop`
--

INSERT INTO `mst_sop` (`id`, `sop_code`, `sop_name`, `sop_type`, `sop_frequency`, `sop_remark`, `area_code`, `activity_code`, `filepath`, `created_by`, `created_on`, `modified_by`, `modified_on`, `is_active`) VALUES
(698, 'TAB-023', 'Room cleaning SOP OSD Area', 'Room', 'twicee weekly', 'testing', NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', NULL, NULL, b'1'),
(699, 'CPD-999', 'Dummy Cleaning and Operation of Non Destructive leak tester machine for bottle pack (SEPHA)', 'Equipment', 'twicee weekly', 'OSD', NULL, NULL, '24991e7b322b83756d5ae7b3aa4702e6.pdf', 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-11-03 21:40:07', b'1'),
(700, 'CPD-124', 'Cleaning and operating procedure of Checkweigher Model-CW1200 (LCD Display)', 'Equipment', 'Once Monthly', 'OSD', NULL, NULL, 'cc6555a2229a1bddbf7d71870320d037.pdf', 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-11-03 21:35:45', b'1'),
(701, 'CPD-130', 'FBE Cleaning and Operation of Non Destructive leak tester machine for bottle pack (SEPHA)', 'Equipment', 'twicee weekly', 'testing', NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', NULL, NULL, b'1'),
(726, 'CPD-015', 'Cleaning and Operation procedure of tablet lifter (counted)', NULL, NULL, NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-10-29 23:04:49', b'1'),
(727, 'CPD-016', 'Cleaning and operating procedure of rotary Re-torquer (COUNTEC)', NULL, NULL, NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-10-29 23:04:49', b'1'),
(728, 'CPD-017', 'Cleaning and operating procedure of outsert pasting machine with gum dispenser and camera system', NULL, NULL, NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-10-29 23:04:49', b'1'),
(729, 'CPD-018', 'Cleaning and operating procedure of Blister packing machine (BQS) with ACG inspection system.', NULL, NULL, NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-10-29 23:04:49', b'1'),
(730, 'CPD-019', 'Procedure for de cartoning of packing material', NULL, NULL, NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-10-29 23:04:49', b'1'),
(731, 'CPD-020', 'Cleaning and Operation of Strip packing Machine.', NULL, NULL, NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-10-29 23:04:49', b'1'),
(732, 'CPD-021', 'Cleaning and operation procedure of Semi-Automatic cartooning machine (Parle).', NULL, NULL, NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-10-29 23:04:49', b'1'),
(733, 'CPD-022', 'Cleaning and operation procedure of Automatic cartoning machine (elmach).', NULL, NULL, NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-10-29 23:04:49', b'1'),
(734, 'CPD-023', 'Cleaning and Operation procedure of De-Dusting unit.', NULL, NULL, NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-10-29 23:04:49', b'1'),
(735, 'CPD-024', 'Cleaning and Operating procedure of lifting, Positioning and Tilting devise (PHARMAQUIP)', NULL, NULL, NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-10-29 23:04:49', b'1'),
(736, 'CPD-025', 'Cleaning and Operation procedure of tablet lifter (counted)', NULL, NULL, NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-10-31 00:40:57', b'1'),
(737, 'CPD-026', 'Cleaning and operating procedure of rotary Re-torquer (COUNTEC)', NULL, NULL, NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-10-31 00:40:57', b'1'),
(738, 'CPD-027', 'Cleaning and operating procedure of outsert pasting machine with gum dispenser and camera system', NULL, NULL, NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-10-31 00:40:58', b'1'),
(739, 'CPD-028', 'Cleaning and operating procedure of Blister packing machine (BQS) with ACG inspection system.', NULL, NULL, NULL, NULL, NULL, NULL, 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-10-31 00:40:58', b'1'),
(740, 'CPD-030', 'SOP for coating area', 'Room', '', '', NULL, NULL, '6d5b827b4d337233f90a0cb523b8b1f2.pdf', 'Superadmin', '2020-01-15 05:23:38', 'Superadmin', '2019-11-11 02:48:51', b'1'),
(741, 'CPD-039', 'Test SOP', 'Room', '', 'testing', NULL, NULL, 'bd97c00401f20ba8f85945657a396d66.pdf', 'Superadmin', '2020-01-15 05:23:38', NULL, NULL, b'1'),
(742, 'CPD-040', 'Test SOP 2', 'Equipment', 'Monthly', 'testing', NULL, NULL, '85b861b155475a357ded29391fb27ba1.pdf', 'Superadmin', '2020-01-15 05:23:38', NULL, NULL, b'1');

--
-- Triggers `mst_sop`
--
DROP TRIGGER IF EXISTS `au_sopactivity`;
DELIMITER $$
CREATE TRIGGER `au_sopactivity` AFTER UPDATE ON `mst_sop` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_sop',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.sop_code != NEW.sop_code)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_code', OLD.sop_code, NEW.sop_code);
	  END IF;
	  IF(OLD.sop_name != NEW.sop_name)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_name', OLD.sop_name, NEW.sop_name);
	  END IF;
	  
	  IF(OLD.sop_type != NEW.sop_type)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_type', OLD.sop_type, NEW.sop_type);
	  END IF;
	  IF(OLD.sop_frequency != NEW.sop_frequency)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_frequency', OLD.sop_frequency, NEW.sop_frequency);
	  END IF;
	  IF(OLD.sop_remark != NEW.sop_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_remark', OLD.sop_remark, NEW.sop_remark);
	  END IF;
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_code', OLD.area_code, NEW.area_code);
	  END IF;
	  IF(OLD.activity_code != NEW.activity_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_code', OLD.activity_code, NEW.activity_code);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_sop', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_status`
--

DROP TABLE IF EXISTS `mst_status`;
CREATE TABLE IF NOT EXISTS `mst_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(250) DEFAULT NULL,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_status`
--

INSERT INTO `mst_status` (`id`, `status_name`, `created_on`, `created_by`, `modified_by`, `modified_on`, `is_active`) VALUES
(1, 'START', '2020-01-15 05:23:39', 'ADMIN', 'ADMIN', '2019-08-30 18:33:18', b'1'),
(2, 'STOP', '2020-01-15 05:23:39', 'ADMIN', 'ADMIN', '2019-08-30 18:36:48', b'1'),
(3, 'Approved by AS', '2020-01-15 05:23:39', 'ADMIN', 'ADMIN', '2019-08-30 18:36:48', b'1'),
(4, 'Approved by PM', '2020-01-15 05:23:39', 'ADMIN', 'ADMIN', '2019-08-30 18:36:48', b'1'),
(5, 'Approved by QC', '2020-01-15 05:23:39', 'ADMIN', 'ADMIN', '2019-08-30 18:36:48', b'1'),
(6, 'Approved by QA', '2020-01-15 05:23:39', 'ADMIN', 'ADMIN', '2019-08-30 18:36:48', b'1');

--
-- Triggers `mst_status`
--
DROP TRIGGER IF EXISTS `au_statusactivity`;
DELIMITER $$
CREATE TRIGGER `au_statusactivity` AFTER UPDATE ON `mst_status` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_status',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.status_name != NEW.status_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'status_name', OLD.status_name, NEW.status_name);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_status', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `operationheader_audit_log`
--

DROP TABLE IF EXISTS `operationheader_audit_log`;
CREATE TABLE IF NOT EXISTS `operationheader_audit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tablename` varchar(250) NOT NULL,
  `tableid` int(11) NOT NULL,
  `action` varchar(250) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=794 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `operationheader_audit_log`
--

INSERT INTO `operationheader_audit_log` (`id`, `tablename`, `tableid`, `action`, `createdon`, `createdby`) VALUES
(287, 'operationlogheader', 108, 'inserted', '2019-10-23 23:40:42', 'emp-001'),
(288, 'operationlogheader', 108, 'updated', '2019-10-23 23:40:51', 'emp-001'),
(289, 'operationlogheader', 109, 'inserted', '2019-10-24 14:27:56', 'emp-001'),
(290, 'operationlogheader', 110, 'inserted', '2019-10-25 14:41:40', 'emp-002'),
(291, 'operationlogheader', 108, 'updated', '2019-10-25 15:09:57', 'emp-001'),
(292, 'operationlogheader', 111, 'inserted', '2019-10-29 14:17:17', 'emp-002'),
(293, 'operationlogheader', 111, 'updated', '2019-10-29 14:17:32', 'emp-002'),
(294, 'operationlogheader', 112, 'inserted', '2019-10-29 15:10:30', 'emp-002'),
(295, 'operationlogheader', 112, 'updated', '2019-10-29 15:10:40', 'emp-002'),
(296, 'operationlogheader', 113, 'inserted', '2019-10-29 15:11:03', 'emp-002'),
(297, 'operationlogheader', 113, 'updated', '2019-10-29 15:11:18', 'emp-002'),
(298, 'operationlogheader', 114, 'inserted', '2019-10-29 15:11:47', 'emp-002'),
(299, 'operationlogheader', 114, 'updated', '2019-10-29 15:11:55', 'emp-002'),
(300, 'operationlogheader', 115, 'inserted', '2019-10-30 10:03:00', 'emp-002'),
(301, 'operationlogheader', 116, 'inserted', '2019-10-30 10:04:08', 'emp-002'),
(302, 'operationlogheader', 116, 'updated', '2019-10-30 10:05:07', 'emp-002'),
(303, 'operationlogheader', 117, 'inserted', '2019-10-30 11:36:59', 'emp-002'),
(304, 'operationlogheader', 117, 'updated', '2019-10-30 11:37:04', 'emp-002'),
(305, 'operationlogheader', 115, 'updated', '2019-10-30 11:37:22', 'emp-002'),
(306, 'operationlogheader', 111, 'updated', '2019-10-30 13:31:42', 'emp-002'),
(307, 'operationlogheader', 112, 'updated', '2019-10-30 13:31:45', 'emp-002'),
(308, 'operationlogheader', 113, 'updated', '2019-10-30 13:31:50', 'emp-002'),
(309, 'operationlogheader', 115, 'updated', '2019-10-30 13:31:54', 'emp-002'),
(310, 'operationlogheader', 118, 'inserted', '2019-10-31 15:09:47', 'emp-002'),
(311, 'operationlogheader', 118, 'updated', '2019-10-31 15:11:09', 'emp-002'),
(312, 'operationlogheader', 119, 'inserted', '2019-10-31 15:11:45', 'emp-002'),
(313, 'operationlogheader', 119, 'updated', '2019-11-01 10:39:22', 'emp-002'),
(314, 'operationlogheader', 120, 'inserted', '2019-11-01 10:41:13', 'emp-002'),
(315, 'operationlogheader', 120, 'updated', '2019-11-01 10:41:31', 'emp-002'),
(316, 'operationlogheader', 121, 'inserted', '2019-11-01 10:42:05', 'emp-002'),
(317, 'operationlogheader', 121, 'updated', '2019-11-01 10:42:51', 'emp-002'),
(318, 'operationlogheader', 122, 'inserted', '2019-11-01 11:56:44', 'emp-002'),
(319, 'operationlogheader', 122, 'updated', '2019-11-01 12:48:21', 'emp-002'),
(320, 'operationlogheader', 123, 'inserted', '2019-11-01 13:10:00', 'emp-002'),
(321, 'operationlogheader', 123, 'updated', '2019-11-01 13:10:26', 'emp-002'),
(322, 'operationlogheader', 124, 'inserted', '2019-11-01 13:10:57', 'emp-002'),
(323, 'operationlogheader', 124, 'updated', '2019-11-01 13:11:08', 'emp-002'),
(324, 'operationlogheader', 125, 'inserted', '2019-11-01 18:13:16', 'emp-002'),
(325, 'operationlogheader', 125, 'updated', '2019-11-01 18:14:18', 'emp-002'),
(326, 'operationlogheader', 126, 'inserted', '2019-11-01 18:15:18', 'emp-002'),
(327, 'operationlogheader', 126, 'updated', '2019-11-01 18:15:55', 'emp-002'),
(328, 'operationlogheader', 127, 'inserted', '2019-11-01 18:17:42', 'emp-002'),
(329, 'operationlogheader', 127, 'updated', '2019-11-01 18:18:08', 'emp-002'),
(330, 'operationlogheader', 128, 'inserted', '2019-11-01 21:35:35', 'emp-002'),
(331, 'operationlogheader', 128, 'updated', '2019-11-01 21:36:38', 'emp-002'),
(332, 'operationlogheader', 129, 'inserted', '2019-11-01 21:37:33', 'emp-002'),
(333, 'operationlogheader', 129, 'updated', '2019-11-01 21:37:46', 'emp-002'),
(334, 'operationlogheader', 130, 'inserted', '2019-11-01 21:38:19', 'emp-002'),
(335, 'operationlogheader', 130, 'updated', '2019-11-01 21:39:11', 'emp-002'),
(336, 'operationlogheader', 131, 'inserted', '2019-11-01 21:51:56', 'emp-002'),
(337, 'operationlogheader', 131, 'updated', '2019-11-01 21:53:02', 'emp-002'),
(338, 'operationlogheader', 132, 'inserted', '2019-11-01 21:54:22', 'emp-002'),
(339, 'operationlogheader', 132, 'updated', '2019-11-01 21:55:14', 'emp-002'),
(340, 'operationlogheader', 133, 'inserted', '2019-11-01 21:55:42', 'emp-003'),
(341, 'operationlogheader', 133, 'updated', '2019-11-01 21:55:54', 'emp-003'),
(342, 'operationlogheader', 134, 'inserted', '2019-11-02 12:01:30', 'emp-002'),
(343, 'operationlogheader', 134, 'updated', '2019-11-02 12:04:35', 'emp-002'),
(344, 'operationlogheader', 135, 'inserted', '2019-11-02 12:05:27', 'emp-002'),
(345, 'operationlogheader', 135, 'updated', '2019-11-02 12:05:35', 'emp-002'),
(346, 'operationlogheader', 136, 'inserted', '2019-11-04 10:48:17', 'emp-002'),
(347, 'operationlogheader', 136, 'updated', '2019-11-04 10:54:12', 'emp-002'),
(348, 'operationlogheader', 137, 'inserted', '2019-11-04 10:55:20', 'emp-002'),
(349, 'operationlogheader', 137, 'updated', '2019-11-04 10:56:30', 'emp-002'),
(350, 'operationlogheader', 138, 'inserted', '2019-11-04 14:15:47', 'emp-002'),
(351, 'operationlogheader', 138, 'updated', '2019-11-04 14:17:14', 'emp-002'),
(352, 'operationlogheader', 139, 'inserted', '2019-11-04 21:27:37', 'emp-002'),
(353, 'operationlogheader', 139, 'updated', '2019-11-04 21:29:32', 'emp-002'),
(354, 'operationlogheader', 140, 'inserted', '2019-11-04 22:19:25', 'emp-002'),
(355, 'operationlogheader', 140, 'updated', '2019-11-04 22:19:37', 'emp-002'),
(356, 'operationlogheader', 141, 'inserted', '2019-11-04 22:50:45', 'emp-002'),
(357, 'operationlogheader', 141, 'updated', '2019-11-04 22:51:53', 'emp-002'),
(358, 'operationlogheader', 142, 'inserted', '2019-11-05 13:04:31', 'emp-002'),
(359, 'operationlogheader', 142, 'updated', '2019-11-05 13:05:33', 'emp-002'),
(360, 'operationlogheader', 143, 'inserted', '2019-11-05 13:46:35', 'emp-002'),
(361, 'operationlogheader', 143, 'updated', '2019-11-05 13:47:46', 'emp-002'),
(362, 'operationlogheader', 144, 'inserted', '2019-11-05 14:01:19', 'emp-002'),
(363, 'operationlogheader', 144, 'updated', '2019-11-05 14:01:31', 'emp-002'),
(364, 'operationlogheader', 145, 'inserted', '2019-11-05 15:18:59', 'emp-002'),
(365, 'operationlogheader', 145, 'updated', '2019-11-05 15:19:15', 'emp-002'),
(366, 'operationlogheader', 146, 'inserted', '2019-11-05 15:34:23', 'emp-002'),
(367, 'operationlogheader', 146, 'updated', '2019-11-05 15:34:39', 'emp-002'),
(368, 'operationlogheader', 147, 'inserted', '2019-11-05 16:04:14', 'emp-002'),
(369, 'operationlogheader', 147, 'updated', '2019-11-05 16:04:27', 'emp-002'),
(370, 'operationlogheader', 148, 'inserted', '2019-11-05 16:06:01', 'emp-002'),
(371, 'operationlogheader', 148, 'updated', '2019-11-05 16:06:13', 'emp-002'),
(372, 'operationlogheader', 149, 'inserted', '2019-11-05 16:21:26', 'emp-002'),
(373, 'operationlogheader', 149, 'updated', '2019-11-05 16:21:42', 'emp-002'),
(374, 'operationlogheader', 150, 'inserted', '2019-11-05 16:27:58', 'emp-002'),
(375, 'operationlogheader', 150, 'updated', '2019-11-05 16:28:09', 'emp-002'),
(376, 'operationlogheader', 151, 'inserted', '2019-11-05 16:34:23', 'emp-002'),
(377, 'operationlogheader', 151, 'updated', '2019-11-05 16:34:35', 'emp-002'),
(378, 'operationlogheader', 152, 'inserted', '2019-11-05 17:30:36', 'emp-002'),
(379, 'operationlogheader', 152, 'updated', '2019-11-05 17:30:46', 'emp-002'),
(380, 'operationlogheader', 153, 'inserted', '2019-11-05 17:32:43', 'emp-002'),
(381, 'operationlogheader', 153, 'updated', '2019-11-05 17:32:53', 'emp-002'),
(382, 'operationlogheader', 154, 'inserted', '2019-11-05 17:35:23', 'emp-002'),
(383, 'operationlogheader', 154, 'updated', '2019-11-05 17:35:34', 'emp-002'),
(384, 'operationlogheader', 155, 'inserted', '2019-11-05 17:37:57', 'emp-002'),
(385, 'operationlogheader', 155, 'updated', '2019-11-05 17:38:09', 'emp-002'),
(386, 'operationlogheader', 156, 'inserted', '2019-11-05 17:39:53', 'emp-002'),
(387, 'operationlogheader', 156, 'updated', '2019-11-05 17:40:04', 'emp-002'),
(388, 'operationlogheader', 157, 'inserted', '2019-11-05 17:42:04', 'emp-002'),
(389, 'operationlogheader', 157, 'updated', '2019-11-05 17:42:16', 'emp-002'),
(390, 'operationlogheader', 158, 'inserted', '2019-11-06 11:21:14', 'emp-002'),
(391, 'operationlogheader', 158, 'updated', '2019-11-06 11:21:25', 'emp-002'),
(392, 'operationlogheader', 159, 'inserted', '2019-11-06 11:30:58', 'emp-002'),
(393, 'operationlogheader', 159, 'updated', '2019-11-06 11:41:51', 'emp-002'),
(394, 'operationlogheader', 160, 'inserted', '2019-11-06 11:53:24', 'emp-002'),
(395, 'operationlogheader', 160, 'updated', '2019-11-06 11:53:37', 'emp-002'),
(396, 'operationlogheader', 161, 'inserted', '2019-11-06 12:01:10', 'emp-002'),
(397, 'operationlogheader', 161, 'updated', '2019-11-06 12:01:22', 'emp-002'),
(398, 'operationlogheader', 162, 'inserted', '2019-11-06 12:18:02', 'emp-002'),
(399, 'operationlogheader', 162, 'updated', '2019-11-06 12:18:13', 'emp-002'),
(400, 'operationlogheader', 163, 'inserted', '2019-11-06 12:31:06', 'emp-002'),
(401, 'operationlogheader', 163, 'updated', '2019-11-06 12:31:16', 'emp-002'),
(402, 'operationlogheader', 164, 'inserted', '2019-11-06 12:39:28', 'emp-002'),
(403, 'operationlogheader', 164, 'updated', '2019-11-06 12:39:42', 'emp-002'),
(404, 'operationlogheader', 165, 'inserted', '2019-11-06 12:55:22', 'emp-002'),
(405, 'operationlogheader', 165, 'updated', '2019-11-06 12:55:33', 'emp-002'),
(406, 'operationlogheader', 166, 'inserted', '2019-11-06 12:59:13', 'emp-002'),
(407, 'operationlogheader', 166, 'updated', '2019-11-06 12:59:26', 'emp-002'),
(408, 'operationlogheader', 167, 'inserted', '2019-11-06 16:19:51', 'emp-002'),
(409, 'operationlogheader', 167, 'updated', '2019-11-06 16:20:03', 'emp-002'),
(410, 'operationlogheader', 168, 'inserted', '2019-11-06 16:55:39', 'emp-002'),
(411, 'operationlogheader', 168, 'updated', '2019-11-06 16:55:49', 'emp-002'),
(412, 'operationlogheader', 169, 'inserted', '2019-11-06 17:35:38', 'emp-002'),
(413, 'operationlogheader', 169, 'updated', '2019-11-06 17:35:47', 'emp-002'),
(414, 'operationlogheader', 170, 'inserted', '2019-11-06 17:40:20', 'emp-002'),
(415, 'operationlogheader', 170, 'updated', '2019-11-06 17:40:29', 'emp-002'),
(416, 'operationlogheader', 171, 'inserted', '2019-11-06 18:00:19', 'emp-002'),
(417, 'operationlogheader', 171, 'updated', '2019-11-06 18:00:29', 'emp-002'),
(418, 'operationlogheader', 172, 'inserted', '2019-11-06 18:41:50', 'emp-002'),
(419, 'operationlogheader', 172, 'updated', '2019-11-06 18:42:01', 'emp-002'),
(420, 'operationlogheader', 173, 'inserted', '2019-11-07 10:55:33', 'emp-002'),
(421, 'operationlogheader', 173, 'updated', '2019-11-07 10:55:48', 'emp-002'),
(422, 'operationlogheader', 174, 'inserted', '2019-11-07 10:56:31', 'emp-002'),
(423, 'operationlogheader', 174, 'updated', '2019-11-07 10:57:25', 'emp-002'),
(424, 'operationlogheader', 175, 'inserted', '2019-11-07 12:27:34', 'emp-002'),
(425, 'operationlogheader', 175, 'updated', '2019-11-07 12:27:52', 'emp-002'),
(426, 'operationlogheader', 123, 'updated', '2019-11-07 12:30:31', 'emp-002'),
(427, 'operationlogheader', 176, 'inserted', '2019-11-08 10:06:40', 'emp-002'),
(428, 'operationlogheader', 176, 'updated', '2019-11-08 10:07:32', 'emp-002'),
(429, 'operationlogheader', 177, 'inserted', '2019-11-08 15:00:35', 'emp-002'),
(430, 'operationlogheader', 177, 'updated', '2019-11-08 15:01:51', 'emp-002'),
(431, 'operationlogheader', 178, 'inserted', '2019-11-09 11:56:36', 'emp-002'),
(432, 'operationlogheader', 178, 'updated', '2019-11-09 11:59:09', 'emp-002'),
(433, 'operationlogheader', 179, 'inserted', '2019-11-09 12:00:03', 'emp-002'),
(434, 'operationlogheader', 179, 'updated', '2019-11-09 12:00:18', 'emp-002'),
(435, 'operationlogheader', 180, 'inserted', '2019-11-11 14:18:11', 'emp-002'),
(436, 'operationlogheader', 181, 'inserted', '2019-11-11 16:08:45', 'emp-002'),
(437, 'operationlogheader', 180, 'updated', '2019-11-11 17:31:48', 'emp-002'),
(438, 'operationlogheader', 181, 'updated', '2019-11-11 22:24:37', 'emp-002'),
(439, 'operationlogheader', 182, 'inserted', '2019-11-11 22:27:33', 'emp-002'),
(440, 'operationlogheader', 183, 'inserted', '2019-11-12 12:41:14', 'emp-002'),
(441, 'operationlogheader', 183, 'updated', '2019-11-12 12:43:35', 'emp-002'),
(442, 'operationlogheader', 182, 'updated', '2019-11-13 10:09:49', 'emp-002'),
(443, 'operationlogheader', 184, 'inserted', '2019-11-13 10:10:24', 'emp-002'),
(444, 'operationlogheader', 184, 'updated', '2019-11-13 10:11:47', 'emp-002'),
(445, 'operationlogheader', 185, 'inserted', '2019-11-13 10:12:27', 'emp-002'),
(446, 'operationlogheader', 185, 'updated', '2019-11-13 10:15:15', 'emp-002'),
(447, 'operationlogheader', 186, 'inserted', '2019-11-13 10:16:23', 'emp-003'),
(448, 'operationlogheader', 186, 'updated', '2019-11-13 10:16:42', 'emp-003'),
(449, 'operationlogheader', 187, 'inserted', '2019-11-13 10:22:50', 'emp-003'),
(450, 'operationlogheader', 187, 'updated', '2019-11-13 10:23:41', 'emp-003'),
(451, 'operationlogheader', 188, 'inserted', '2019-11-13 10:33:46', 'emp-002'),
(452, 'operationlogheader', 189, 'inserted', '2019-11-13 10:52:30', 'emp-002'),
(453, 'operationlogheader', 189, 'updated', '2019-11-13 10:53:16', 'emp-002'),
(454, 'operationlogheader', 190, 'inserted', '2019-11-13 10:56:55', 'emp-002'),
(455, 'operationlogheader', 190, 'updated', '2019-11-13 10:57:36', 'emp-002'),
(456, 'operationlogheader', 191, 'inserted', '2019-11-13 10:58:52', 'emp-002'),
(457, 'operationlogheader', 191, 'updated', '2019-11-13 10:59:44', 'emp-002'),
(458, 'operationlogheader', 192, 'inserted', '2019-11-13 11:04:07', 'emp-002'),
(459, 'operationlogheader', 192, 'updated', '2019-11-13 11:04:22', 'emp-002'),
(460, 'operationlogheader', 193, 'inserted', '2019-11-13 11:06:13', 'emp-002'),
(461, 'operationlogheader', 193, 'updated', '2019-11-13 11:07:08', 'emp-002'),
(462, 'operationlogheader', 194, 'inserted', '2019-11-13 11:10:06', 'emp-002'),
(463, 'operationlogheader', 194, 'updated', '2019-11-13 11:10:46', 'emp-002'),
(464, 'operationlogheader', 195, 'inserted', '2019-11-13 11:12:39', 'emp-002'),
(465, 'operationlogheader', 195, 'updated', '2019-11-13 11:14:04', 'emp-002'),
(466, 'operationlogheader', 196, 'inserted', '2019-11-13 11:16:00', 'emp-002'),
(467, 'operationlogheader', 196, 'updated', '2019-11-13 11:16:13', 'emp-002'),
(468, 'operationlogheader', 197, 'inserted', '2019-11-13 11:19:07', 'emp-002'),
(469, 'operationlogheader', 197, 'updated', '2019-11-13 11:19:33', 'emp-002'),
(470, 'operationlogheader', 198, 'inserted', '2019-11-13 11:23:13', 'emp-002'),
(471, 'operationlogheader', 198, 'updated', '2019-11-13 11:24:29', 'emp-002'),
(472, 'operationlogheader', 199, 'inserted', '2019-11-13 11:33:51', 'emp-002'),
(473, 'operationlogheader', 199, 'updated', '2019-11-13 11:35:54', 'emp-002'),
(474, 'operationlogheader', 200, 'inserted', '2019-11-13 12:06:56', 'emp-002'),
(475, 'operationlogheader', 200, 'updated', '2019-11-13 12:07:05', 'emp-002'),
(476, 'operationlogheader', 201, 'inserted', '2019-11-13 12:07:49', 'emp-002'),
(477, 'operationlogheader', 201, 'updated', '2019-11-13 14:19:50', 'emp-002'),
(478, 'operationlogheader', 202, 'inserted', '2019-11-13 14:33:27', 'emp-002'),
(479, 'operationlogheader', 202, 'updated', '2019-11-13 14:34:54', 'emp-002'),
(480, 'operationlogheader', 203, 'inserted', '2019-11-13 14:36:55', 'emp-002'),
(481, 'operationlogheader', 203, 'updated', '2019-11-13 15:10:34', 'emp-002'),
(482, 'operationlogheader', 204, 'inserted', '2019-11-13 15:18:14', 'emp-002'),
(483, 'operationlogheader', 205, 'inserted', '2019-11-13 15:20:52', 'emp-002'),
(484, 'operationlogheader', 204, 'updated', '2019-11-13 15:25:11', 'emp-002'),
(485, 'operationlogheader', 205, 'updated', '2019-11-13 15:27:44', 'emp-002'),
(486, 'operationlogheader', 206, 'inserted', '2019-11-13 15:51:58', 'emp-002'),
(487, 'operationlogheader', 206, 'updated', '2019-11-13 15:57:12', 'emp-002'),
(488, 'operationlogheader', 207, 'inserted', '2019-11-13 16:21:47', 'emp-002'),
(489, 'operationlogheader', 207, 'updated', '2019-11-13 16:21:58', 'emp-002'),
(490, 'operationlogheader', 208, 'inserted', '2019-11-13 16:22:48', 'emp-002'),
(491, 'operationlogheader', 208, 'updated', '2019-11-13 16:22:57', 'emp-002'),
(492, 'operationlogheader', 209, 'inserted', '2019-11-13 16:43:52', 'emp-002'),
(493, 'operationlogheader', 210, 'inserted', '2019-11-13 17:45:14', 'emp-002'),
(494, 'operationlogheader', 210, 'updated', '2019-11-13 17:45:23', 'emp-002'),
(495, 'operationlogheader', 211, 'inserted', '2019-11-13 17:46:51', 'emp-002'),
(496, 'operationlogheader', 211, 'updated', '2019-11-13 17:46:58', 'emp-002'),
(497, 'operationlogheader', 209, 'updated', '2019-11-13 17:53:23', 'emp-002'),
(498, 'operationlogheader', 212, 'inserted', '2019-11-13 17:56:28', 'emp-002'),
(499, 'operationlogheader', 212, 'updated', '2019-11-13 17:56:36', 'emp-002'),
(500, 'operationlogheader', 188, 'updated', '2019-11-13 18:02:01', 'emp-002'),
(501, 'operationlogheader', 188, 'updated', '2019-11-13 18:02:15', 'emp-002'),
(502, 'operationlogheader', 188, 'updated', '2019-11-13 18:03:27', 'emp-002'),
(503, 'operationlogheader', 188, 'updated', '2019-11-13 18:03:31', 'emp-002'),
(504, 'operationlogheader', 213, 'inserted', '2019-11-13 18:23:51', 'emp-002'),
(505, 'operationlogheader', 213, 'updated', '2019-11-13 18:26:04', 'emp-002'),
(506, 'operationlogheader', 213, 'updated', '2019-11-13 18:26:17', 'emp-002'),
(507, 'operationlogheader', 214, 'inserted', '2019-11-13 19:20:29', 'emp-002'),
(508, 'operationlogheader', 214, 'updated', '2019-11-13 19:21:03', 'emp-002'),
(509, 'operationlogheader', 215, 'inserted', '2019-11-13 19:23:01', 'emp-002'),
(510, 'operationlogheader', 216, 'inserted', '2019-11-14 10:34:20', 'emp-002'),
(511, 'operationlogheader', 217, 'inserted', '2019-11-14 10:37:04', 'emp-002'),
(512, 'operationlogheader', 216, 'updated', '2019-11-14 10:37:27', 'emp-002'),
(513, 'operationlogheader', 217, 'updated', '2019-11-14 10:37:46', 'emp-002'),
(514, 'operationlogheader', 218, 'inserted', '2019-11-15 16:17:34', 'emp-002'),
(515, 'operationlogheader', 219, 'inserted', '2019-11-15 16:21:30', 'emp-003'),
(516, 'operationlogheader', 219, 'updated', '2019-11-15 16:21:40', 'emp-003'),
(517, 'operationlogheader', 218, 'updated', '2019-11-16 10:40:10', 'emp-002'),
(518, 'operationlogheader', 220, 'inserted', '2019-11-16 17:35:07', 'emp-003'),
(519, 'operationlogheader', 220, 'updated', '2019-11-16 17:36:57', 'emp-003'),
(520, 'operationlogheader', 221, 'inserted', '2019-11-16 17:40:54', 'emp-002'),
(521, 'operationlogheader', 221, 'updated', '2019-11-16 17:42:34', 'emp-002'),
(522, 'operationlogheader', 220, 'updated', '2019-11-16 17:52:16', 'emp-003'),
(523, 'operationlogheader', 222, 'inserted', '2019-11-17 09:00:00', 'emp-002'),
(524, 'operationlogheader', 223, 'inserted', '2019-11-17 09:00:39', 'emp-002'),
(525, 'operationlogheader', 224, 'inserted', '2019-11-17 12:16:44', 'emp-003'),
(526, 'operationlogheader', 224, 'updated', '2019-11-17 14:45:37', 'emp-003'),
(527, 'operationlogheader', 225, 'inserted', '2019-11-17 14:59:31', 'emp-002'),
(528, 'operationlogheader', 225, 'updated', '2019-11-17 15:07:12', 'emp-002'),
(529, 'operationlogheader', 226, 'inserted', '2019-11-17 15:08:47', 'emp-002'),
(530, 'operationlogheader', 226, 'updated', '2019-11-17 15:09:15', 'emp-002'),
(531, 'operationlogheader', 227, 'inserted', '2019-11-17 15:11:09', 'emp-002'),
(532, 'operationlogheader', 227, 'updated', '2019-11-17 15:12:01', 'emp-002'),
(533, 'operationlogheader', 228, 'inserted', '2019-11-17 15:12:55', 'emp-002'),
(534, 'operationlogheader', 228, 'updated', '2019-11-17 15:13:14', 'emp-002'),
(535, 'operationlogheader', 229, 'inserted', '2019-11-17 15:14:30', 'emp-002'),
(536, 'operationlogheader', 229, 'updated', '2019-11-17 15:14:45', 'emp-002'),
(537, 'operationlogheader', 230, 'inserted', '2019-11-17 15:16:48', 'emp-002'),
(538, 'operationlogheader', 230, 'updated', '2019-11-17 15:17:15', 'emp-002'),
(539, 'operationlogheader', 231, 'inserted', '2019-11-17 15:17:58', 'emp-002'),
(540, 'operationlogheader', 231, 'updated', '2019-11-17 15:19:10', 'emp-002'),
(541, 'operationlogheader', 232, 'inserted', '2019-11-17 15:20:35', 'emp-002'),
(542, 'operationlogheader', 232, 'updated', '2019-11-17 15:20:44', 'emp-002'),
(543, 'operationlogheader', 233, 'inserted', '2019-11-17 15:22:30', 'emp-002'),
(544, 'operationlogheader', 233, 'updated', '2019-11-17 15:22:39', 'emp-002'),
(545, 'operationlogheader', 234, 'inserted', '2019-11-17 15:33:42', 'emp-002'),
(546, 'operationlogheader', 234, 'updated', '2019-11-17 15:33:51', 'emp-002'),
(547, 'operationlogheader', 235, 'inserted', '2019-11-17 18:57:54', 'emp-002'),
(548, 'operationlogheader', 235, 'updated', '2019-11-17 19:43:28', 'emp-002'),
(549, 'operationlogheader', 225, 'updated', '2019-11-17 19:43:53', 'emp-002'),
(550, 'operationlogheader', 236, 'inserted', '2019-11-17 19:44:36', 'emp-002'),
(551, 'operationlogheader', 236, 'updated', '2019-11-17 19:49:52', 'emp-002'),
(552, 'operationlogheader', 237, 'inserted', '2019-11-17 19:52:02', 'emp-002'),
(553, 'operationlogheader', 224, 'updated', '2019-11-17 19:53:13', 'emp-003'),
(554, 'operationlogheader', 227, 'updated', '2019-11-17 19:53:30', 'emp-002'),
(555, 'operationlogheader', 237, 'updated', '2019-11-17 20:09:23', 'emp-002'),
(556, 'operationlogheader', 238, 'inserted', '2019-11-17 20:10:42', 'emp-002'),
(557, 'operationlogheader', 238, 'updated', '2019-11-17 20:20:25', 'emp-002'),
(558, 'operationlogheader', 239, 'inserted', '2019-11-17 20:23:07', 'emp-002'),
(559, 'operationlogheader', 239, 'updated', '2019-11-17 22:10:43', 'emp-002'),
(560, 'operationlogheader', 240, 'inserted', '2019-11-17 22:23:40', 'emp-002'),
(561, 'operationlogheader', 240, 'updated', '2019-11-17 22:31:51', 'emp-002'),
(562, 'operationlogheader', 241, 'inserted', '2019-11-17 22:44:25', 'emp-002'),
(563, 'operationlogheader', 241, 'updated', '2019-11-17 22:46:45', 'emp-002'),
(564, 'operationlogheader', 226, 'updated', '2019-11-19 11:57:36', 'emp-002'),
(565, 'operationlogheader', 228, 'updated', '2019-11-19 12:09:20', 'emp-002'),
(566, 'operationlogheader', 242, 'inserted', '2019-11-21 13:26:44', 'emp-003'),
(567, 'operationlogheader', 242, 'updated', '2019-11-21 13:26:57', 'emp-003'),
(568, 'operationlogheader', 243, 'inserted', '2019-11-21 13:27:36', 'emp-003'),
(569, 'operationlogheader', 243, 'updated', '2019-11-21 13:27:50', 'emp-003'),
(570, 'operationlogheader', 244, 'inserted', '2019-11-21 14:17:56', 'emp-003'),
(571, 'operationlogheader', 244, 'updated', '2019-11-21 14:18:54', 'emp-003'),
(572, 'operationlogheader', 245, 'inserted', '2019-11-21 14:19:43', 'emp-002'),
(573, 'operationlogheader', 245, 'updated', '2019-11-21 14:20:06', 'emp-002'),
(574, 'operationlogheader', 246, 'inserted', '2019-11-21 14:40:16', 'emp-002'),
(575, 'operationlogheader', 246, 'updated', '2019-11-21 14:40:26', 'emp-002'),
(576, 'operationlogheader', 247, 'inserted', '2019-11-21 14:40:55', 'emp-002'),
(577, 'operationlogheader', 247, 'updated', '2019-11-21 14:41:07', 'emp-002'),
(578, 'operationlogheader', 248, 'inserted', '2019-11-21 14:41:33', 'emp-002'),
(579, 'operationlogheader', 248, 'updated', '2019-11-21 14:41:43', 'emp-002'),
(580, 'operationlogheader', 229, 'updated', '2019-11-21 14:43:21', 'emp-002'),
(581, 'operationlogheader', 230, 'updated', '2019-11-21 14:43:26', 'emp-002'),
(582, 'operationlogheader', 246, 'updated', '2019-11-21 14:43:39', 'emp-002'),
(583, 'operationlogheader', 248, 'updated', '2019-11-21 14:51:04', 'emp-002'),
(584, 'operationlogheader', 225, 'updated', '2019-11-21 14:52:17', 'emp-002'),
(585, 'operationlogheader', 220, 'updated', '2019-11-21 14:53:07', 'emp-003'),
(586, 'operationlogheader', 249, 'inserted', '2019-11-21 15:17:15', 'emp-002'),
(587, 'operationlogheader', 249, 'updated', '2019-11-21 15:17:26', 'emp-002'),
(588, 'operationlogheader', 250, 'inserted', '2019-11-21 15:17:55', 'emp-002'),
(589, 'operationlogheader', 250, 'updated', '2019-11-21 15:18:05', 'emp-002'),
(590, 'operationlogheader', 238, 'updated', '2019-11-22 12:10:06', 'emp-002'),
(591, 'operationlogheader', 239, 'updated', '2019-11-22 12:10:12', 'emp-002'),
(592, 'operationlogheader', 244, 'updated', '2019-11-22 12:10:19', 'emp-003'),
(593, 'operationlogheader', 245, 'updated', '2019-11-22 12:10:22', 'emp-002'),
(594, 'operationlogheader', 249, 'updated', '2019-11-22 12:10:26', 'emp-002'),
(595, 'operationlogheader', 231, 'updated', '2019-11-22 12:33:50', 'emp-002'),
(596, 'operationlogheader', 232, 'updated', '2019-11-22 12:33:55', 'emp-002'),
(597, 'operationlogheader', 233, 'updated', '2019-11-22 12:33:59', 'emp-002'),
(598, 'operationlogheader', 234, 'updated', '2019-11-22 12:34:03', 'emp-002'),
(599, 'operationlogheader', 235, 'updated', '2019-11-22 12:34:06', 'emp-002'),
(600, 'operationlogheader', 236, 'updated', '2019-11-22 12:34:10', 'emp-002'),
(601, 'operationlogheader', 237, 'updated', '2019-11-22 12:34:13', 'emp-002'),
(602, 'operationlogheader', 240, 'updated', '2019-11-22 12:34:17', 'emp-002'),
(603, 'operationlogheader', 241, 'updated', '2019-11-22 12:34:20', 'emp-002'),
(604, 'operationlogheader', 242, 'updated', '2019-11-22 12:34:31', 'emp-003'),
(605, 'operationlogheader', 243, 'updated', '2019-11-22 12:34:41', 'emp-003'),
(606, 'operationlogheader', 247, 'updated', '2019-11-25 11:01:32', 'emp-002'),
(607, 'operationlogheader', 240, 'updated', '2019-11-25 12:12:26', 'emp-002'),
(608, 'operationlogheader', 241, 'updated', '2019-11-25 12:18:02', 'emp-002'),
(609, 'operationlogheader', 244, 'updated', '2019-11-25 12:18:41', 'emp-003'),
(610, 'operationlogheader', 251, 'inserted', '2019-11-26 11:44:50', 'emp-002'),
(611, 'operationlogheader', 251, 'updated', '2019-11-26 11:45:05', 'emp-002'),
(612, 'operationlogheader', 252, 'inserted', '2019-11-26 11:45:49', 'emp-002'),
(613, 'operationlogheader', 252, 'updated', '2019-11-26 11:46:34', 'emp-002'),
(614, 'operationlogheader', 253, 'inserted', '2019-11-26 11:47:13', 'emp-003'),
(615, 'operationlogheader', 253, 'updated', '2019-11-26 11:47:27', 'emp-003'),
(616, 'operationlogheader', 254, 'inserted', '2019-11-26 11:48:03', 'emp-003'),
(617, 'operationlogheader', 254, 'updated', '2019-11-26 11:48:14', 'emp-003'),
(618, 'operationlogheader', 255, 'inserted', '2019-11-26 11:49:11', 'emp-003'),
(619, 'operationlogheader', 255, 'updated', '2019-11-26 11:50:04', 'emp-003'),
(620, 'operationlogheader', 250, 'updated', '2019-11-26 11:55:29', 'emp-002'),
(621, 'operationlogheader', 251, 'updated', '2019-11-26 11:56:18', 'emp-002'),
(622, 'operationlogheader', 252, 'updated', '2019-11-26 11:56:52', 'emp-002'),
(623, 'operationlogheader', 253, 'updated', '2019-11-26 12:07:51', 'emp-003'),
(624, 'operationlogheader', 254, 'updated', '2019-11-26 12:08:05', 'emp-003'),
(625, 'operationlogheader', 255, 'updated', '2019-11-26 12:08:09', 'emp-003'),
(626, 'operationlogheader', 245, 'updated', '2019-11-26 12:19:47', 'emp-002'),
(627, 'operationlogheader', 256, 'inserted', '2019-11-26 12:50:36', 'emp-002'),
(628, 'operationlogheader', 256, 'updated', '2019-11-26 12:53:01', 'emp-002'),
(629, 'operationlogheader', 257, 'inserted', '2019-11-26 12:58:22', 'emp-002'),
(630, 'operationlogheader', 257, 'updated', '2019-11-26 12:58:39', 'emp-002'),
(631, 'operationlogheader', 258, 'inserted', '2019-11-26 13:00:20', 'emp-002'),
(632, 'operationlogheader', 258, 'updated', '2019-11-26 13:00:35', 'emp-002'),
(633, 'operationlogheader', 259, 'inserted', '2019-11-26 13:01:16', 'emp-002'),
(634, 'operationlogheader', 259, 'updated', '2019-11-26 13:01:30', 'emp-002'),
(635, 'operationlogheader', 256, 'updated', '2019-11-26 18:31:31', 'emp-002'),
(636, 'operationlogheader', 260, 'inserted', '2019-11-27 16:25:12', 'emp-002'),
(637, 'operationlogheader', 260, 'updated', '2019-11-27 16:27:20', 'emp-002'),
(638, 'operationlogheader', 261, 'inserted', '2019-11-27 16:34:51', 'emp-002'),
(639, 'operationlogheader', 261, 'updated', '2019-11-27 16:35:54', 'emp-002'),
(640, 'operationlogheader', 262, 'inserted', '2019-11-27 16:37:00', 'emp-003'),
(641, 'operationlogheader', 262, 'updated', '2019-11-27 16:39:23', 'emp-003'),
(642, 'operationlogheader', 263, 'inserted', '2019-11-29 10:25:06', 'emp-003'),
(643, 'operationlogheader', 263, 'updated', '2019-11-29 10:28:52', 'emp-003'),
(644, 'operationlogheader', 264, 'inserted', '2019-11-29 10:31:02', 'emp-003'),
(645, 'operationlogheader', 265, 'inserted', '2019-11-29 10:38:13', 'emp-002'),
(646, 'operationlogheader', 265, 'updated', '2019-11-29 10:40:18', 'emp-002'),
(647, 'operationlogheader', 266, 'inserted', '2019-11-29 10:44:34', 'emp-003'),
(648, 'operationlogheader', 266, 'updated', '2019-11-29 10:44:45', 'emp-003'),
(649, 'operationlogheader', 264, 'updated', '2019-12-04 17:30:09', 'emp-003'),
(650, 'operationlogheader', 267, 'inserted', '2019-12-04 17:56:51', 'emp-003'),
(651, 'operationlogheader', 267, 'updated', '2019-12-04 18:05:25', 'emp-003'),
(652, 'operationlogheader', 268, 'inserted', '2019-12-04 19:41:05', 'emp-002'),
(653, 'operationlogheader', 268, 'updated', '2019-12-04 19:42:53', 'emp-002'),
(654, 'operationlogheader', 269, 'inserted', '2019-12-05 13:20:38', 'emp-002'),
(655, 'operationlogheader', 270, 'inserted', '2019-12-06 10:53:34', 'emp-002'),
(656, 'operationlogheader', 270, 'updated', '2019-12-06 10:53:46', 'emp-002'),
(657, 'operationlogheader', 271, 'inserted', '2019-12-06 11:49:09', 'emp-002'),
(658, 'operationlogheader', 271, 'updated', '2019-12-06 16:36:22', 'emp-002'),
(659, 'operationlogheader', 269, 'updated', '2019-12-06 21:53:29', 'emp-002'),
(660, 'operationlogheader', 272, 'inserted', '2019-12-06 21:55:29', 'emp-002'),
(661, 'operationlogheader', 272, 'updated', '2019-12-06 21:55:44', 'emp-002'),
(662, 'operationlogheader', 273, 'inserted', '2019-12-06 21:56:34', 'emp-002'),
(663, 'operationlogheader', 273, 'updated', '2019-12-06 21:58:32', 'emp-002'),
(664, 'operationlogheader', 274, 'inserted', '2019-12-06 22:00:22', 'emp-003'),
(665, 'operationlogheader', 274, 'updated', '2019-12-06 22:02:39', 'emp-003'),
(666, 'operationlogheader', 275, 'inserted', '2019-12-08 14:59:02', 'emp-002'),
(667, 'operationlogheader', 276, 'inserted', '2019-12-08 14:59:33', 'emp-002'),
(668, 'operationlogheader', 276, 'updated', '2019-12-08 15:07:49', 'emp-002'),
(669, 'operationlogheader', 277, 'inserted', '2019-12-08 15:19:40', 'emp-002'),
(670, 'operationlogheader', 277, 'updated', '2019-12-08 15:20:41', 'emp-002'),
(671, 'operationlogheader', 278, 'inserted', '2019-12-08 15:21:06', 'emp-002'),
(672, 'operationlogheader', 278, 'updated', '2019-12-08 15:21:36', 'emp-002'),
(673, 'operationlogheader', 275, 'updated', '2019-12-08 15:22:57', 'emp-002'),
(674, 'operationlogheader', 246, 'updated', '2019-12-10 15:32:14', 'emp-002'),
(675, 'operationlogheader', 252, 'updated', '2019-12-10 15:32:58', 'emp-002'),
(676, 'operationlogheader', 249, 'updated', '2019-12-10 16:25:01', 'emp-002'),
(677, 'operationlogheader', 257, 'updated', '2019-12-11 10:59:32', 'emp-002'),
(678, 'operationlogheader', 262, 'updated', '2019-12-11 10:59:42', 'emp-003'),
(679, 'operationlogheader', 268, 'updated', '2019-12-11 11:00:00', 'emp-002'),
(680, 'operationlogheader', 270, 'updated', '2019-12-11 11:00:03', 'emp-002'),
(681, 'operationlogheader', 258, 'updated', '2019-12-11 11:00:17', 'emp-002'),
(682, 'operationlogheader', 260, 'updated', '2019-12-11 11:00:20', 'emp-002'),
(683, 'operationlogheader', 275, 'updated', '2019-12-11 11:00:24', 'emp-002'),
(684, 'operationlogheader', 261, 'updated', '2019-12-11 11:00:28', 'emp-002'),
(685, 'operationlogheader', 266, 'updated', '2019-12-11 11:00:33', 'emp-003'),
(686, 'operationlogheader', 276, 'updated', '2019-12-11 11:00:37', 'emp-002'),
(687, 'operationlogheader', 278, 'updated', '2019-12-11 11:00:41', 'emp-002'),
(688, 'operationlogheader', 257, 'updated', '2019-12-11 11:04:14', 'emp-002'),
(689, 'operationlogheader', 262, 'updated', '2019-12-11 11:04:52', 'emp-003'),
(690, 'operationlogheader', 258, 'updated', '2019-12-11 11:05:30', 'emp-002'),
(691, 'operationlogheader', 266, 'updated', '2019-12-12 12:59:34', 'emp-003'),
(692, 'operationlogheader', 276, 'updated', '2019-12-12 13:00:04', 'emp-002'),
(693, 'operationlogheader', 261, 'updated', '2019-12-12 13:19:23', 'emp-002'),
(694, 'operationlogheader', 260, 'updated', '2019-12-12 13:20:56', 'emp-002'),
(695, 'operationlogheader', 268, 'updated', '2019-12-12 13:36:31', 'emp-002'),
(696, 'operationlogheader', 278, 'updated', '2019-12-12 13:41:56', 'emp-002'),
(697, 'operationlogheader', 270, 'updated', '2019-12-12 13:42:31', 'emp-002'),
(698, 'operationlogheader', 264, 'updated', '2019-12-12 13:43:13', 'emp-003'),
(699, 'operationlogheader', 263, 'updated', '2019-12-12 13:43:30', 'emp-003'),
(700, 'operationlogheader', 259, 'updated', '2019-12-12 13:43:47', 'emp-002'),
(701, 'operationlogheader', 264, 'updated', '2019-12-13 10:37:14', 'emp-003'),
(702, 'operationlogheader', 259, 'updated', '2019-12-13 10:37:39', 'emp-002'),
(703, 'operationlogheader', 275, 'updated', '2019-12-13 10:38:14', 'emp-002'),
(704, 'operationlogheader', 279, 'inserted', '2019-12-16 07:54:20', 'emp-002'),
(705, 'operationlogheader', 279, 'updated', '2019-12-16 07:54:34', 'emp-002'),
(706, 'operationlogheader', 280, 'inserted', '2019-12-16 12:08:24', 'emp-002'),
(707, 'operationlogheader', 280, 'updated', '2019-12-16 12:09:18', 'emp-002'),
(708, 'operationlogheader', 281, 'inserted', '2019-12-16 12:10:13', 'emp-003'),
(709, 'operationlogheader', 281, 'updated', '2019-12-16 12:11:18', 'emp-003'),
(710, 'operationlogheader', 282, 'inserted', '2019-12-16 12:13:35', 'emp-002'),
(711, 'operationlogheader', 282, 'updated', '2019-12-16 12:15:07', 'emp-002'),
(712, 'operationlogheader', 283, 'inserted', '2019-12-16 13:05:54', 'emp-002'),
(713, 'operationlogheader', 283, 'updated', '2019-12-16 14:20:03', 'emp-002'),
(714, 'operationlogheader', 284, 'inserted', '2019-12-16 14:51:58', 'emp-002'),
(715, 'operationlogheader', 284, 'updated', '2019-12-16 16:12:55', 'emp-002'),
(716, 'operationlogheader', 285, 'inserted', '2019-12-16 16:39:03', 'emp-002'),
(717, 'operationlogheader', 285, 'updated', '2019-12-16 17:53:23', 'emp-002'),
(718, 'operationlogheader', 286, 'inserted', '2019-12-16 18:31:22', 'emp-002'),
(719, 'operationlogheader', 286, 'updated', '2019-12-17 12:19:39', 'emp-002'),
(720, 'operationlogheader', 287, 'inserted', '2019-12-17 12:56:13', 'emp-002'),
(721, 'operationlogheader', 288, 'inserted', '2019-12-17 12:57:48', 'emp-002'),
(722, 'operationlogheader', 287, 'updated', '2019-12-20 19:28:50', 'emp-002'),
(723, 'operationlogheader', 288, 'updated', '2019-12-20 19:29:18', 'emp-002'),
(724, 'operationlogheader', 289, 'inserted', '2019-12-20 19:29:48', 'emp-002'),
(725, 'operationlogheader', 289, 'updated', '2019-12-20 19:31:20', 'emp-002'),
(726, 'operationlogheader', 290, 'inserted', '2019-12-20 19:33:25', 'emp-002'),
(727, 'operationlogheader', 290, 'updated', '2019-12-20 19:35:43', 'emp-002'),
(728, 'operationlogheader', 291, 'inserted', '2019-12-23 12:41:57', 'emp-002'),
(729, 'operationlogheader', 291, 'updated', '2019-12-24 11:30:10', 'emp-002'),
(730, 'operationlogheader', 292, 'inserted', '2019-12-24 18:42:04', 'emp-002'),
(731, 'operationlogheader', 292, 'updated', '2019-12-24 18:45:31', 'emp-002'),
(732, 'operationlogheader', 292, 'updated', '2019-12-24 18:46:15', 'emp-002'),
(733, 'operationlogheader', 291, 'updated', '2019-12-24 18:46:42', 'emp-002'),
(734, 'operationlogheader', 287, 'updated', '2019-12-24 18:47:02', 'emp-002'),
(735, 'operationlogheader', 292, 'updated', '2019-12-24 18:50:07', 'emp-002'),
(736, 'operationlogheader', 293, 'inserted', '2019-12-26 10:36:11', 'emp-002'),
(737, 'operationlogheader', 293, 'updated', '2019-12-26 10:36:29', 'emp-002'),
(738, 'operationlogheader', 293, 'updated', '2019-12-26 10:40:02', 'emp-002'),
(739, 'operationlogheader', 293, 'updated', '2019-12-26 10:41:06', 'emp-002'),
(740, 'operationlogheader', 294, 'inserted', '2019-12-26 10:41:59', 'emp-002'),
(741, 'operationlogheader', 294, 'updated', '2019-12-26 10:42:23', 'emp-002'),
(742, 'operationlogheader', 293, 'updated', '2019-12-26 11:17:43', 'emp-002'),
(743, 'operationlogheader', 293, 'updated', '2019-12-26 11:18:01', 'emp-002'),
(744, 'operationlogheader', 295, 'inserted', '2019-12-26 11:19:35', 'emp-002'),
(745, 'operationlogheader', 293, 'updated', '2019-12-26 11:20:17', 'emp-002'),
(746, 'operationlogheader', 295, 'updated', '2019-12-26 11:30:35', 'emp-002'),
(747, 'operationlogheader', 296, 'inserted', '2019-12-26 11:31:55', 'emp-002'),
(748, 'operationlogheader', 293, 'updated', '2019-12-26 11:38:04', 'emp-002'),
(749, 'operationlogheader', 293, 'updated', '2019-12-26 11:38:23', 'emp-002'),
(750, 'operationlogheader', 293, 'updated', '2019-12-26 11:38:30', 'emp-002'),
(751, 'operationlogheader', 296, 'updated', '2019-12-26 11:43:05', 'emp-002'),
(752, 'operationlogheader', 296, 'updated', '2019-12-26 11:56:58', 'emp-002'),
(753, 'operationlogheader', 296, 'updated', '2019-12-26 11:57:52', 'emp-002'),
(754, 'operationlogheader', 297, 'inserted', '2019-12-26 14:56:43', 'emp-002'),
(755, 'operationlogheader', 298, 'inserted', '2019-12-26 15:04:26', 'emp-002'),
(756, 'operationlogheader', 298, 'updated', '2019-12-26 15:04:37', 'emp-002'),
(757, 'operationlogheader', 298, 'updated', '2019-12-26 15:05:15', 'emp-002'),
(758, 'operationlogheader', 298, 'updated', '2019-12-26 15:05:54', 'emp-002'),
(759, 'operationlogheader', 299, 'inserted', '2019-12-26 15:07:15', 'emp-002'),
(760, 'operationlogheader', 299, 'updated', '2019-12-26 15:07:33', 'emp-002'),
(761, 'operationlogheader', 300, 'inserted', '2019-12-26 15:10:51', 'emp-002'),
(762, 'operationlogheader', 300, 'updated', '2019-12-26 15:11:03', 'emp-002'),
(763, 'operationlogheader', 301, 'inserted', '2019-12-26 17:41:37', 'emp-002'),
(764, 'operationlogheader', 301, 'updated', '2019-12-26 17:42:36', 'emp-002'),
(765, 'operationlogheader', 302, 'inserted', '2019-12-27 11:13:02', 'emp-002'),
(766, 'operationlogheader', 302, 'updated', '2019-12-27 11:13:15', 'emp-002'),
(767, 'operationlogheader', 302, 'updated', '2019-12-27 11:14:14', 'emp-002'),
(768, 'operationlogheader', 302, 'updated', '2019-12-27 11:14:56', 'emp-002'),
(769, 'operationlogheader', 303, 'inserted', '2019-12-27 11:43:02', 'emp-002'),
(770, 'operationlogheader', 303, 'updated', '2019-12-27 11:43:15', 'emp-002'),
(771, 'operationlogheader', 304, 'inserted', '2019-12-27 11:50:52', 'emp-002'),
(772, 'operationlogheader', 304, 'updated', '2019-12-27 11:51:06', 'emp-002'),
(773, 'operationlogheader', 304, 'updated', '2019-12-27 11:51:34', 'emp-002'),
(774, 'operationlogheader', 304, 'updated', '2019-12-27 11:52:24', 'emp-002'),
(775, 'operationlogheader', 305, 'inserted', '2019-12-27 11:53:30', 'emp-002'),
(776, 'operationlogheader', 306, 'inserted', '2020-01-15 11:13:38', 'shivam001'),
(777, 'operationlogheader', 306, 'updated', '2020-01-15 11:15:32', 'shivam001'),
(778, 'operationlogheader', 307, 'inserted', '2020-01-15 11:16:53', 'shivam001'),
(779, 'operationlogheader', 307, 'updated', '2020-01-15 11:20:44', 'shivam001'),
(780, 'operationlogheader', 308, 'inserted', '2020-01-15 11:56:40', 'shivam001'),
(781, 'operationlogheader', 308, 'updated', '2020-01-15 11:59:38', 'shivam001'),
(782, 'operationlogheader', 309, 'inserted', '2020-01-15 12:10:36', 'shivam001'),
(783, 'operationlogheader', 309, 'updated', '2020-01-15 12:10:56', 'shivam001'),
(784, 'operationlogheader', 310, 'inserted', '2020-01-15 12:12:36', 'shivam001'),
(785, 'operationlogheader', 310, 'updated', '2020-01-15 12:13:16', 'shivam001'),
(786, 'operationlogheader', 311, 'inserted', '2020-01-15 12:34:04', 'shivam001'),
(787, 'operationlogheader', 312, 'inserted', '2020-01-15 12:35:24', 'shivam001'),
(788, 'operationlogheader', 311, 'updated', '2020-01-15 13:55:33', 'shivam001'),
(789, 'operationlogheader', 313, 'inserted', '2020-01-15 13:56:44', 'shivam001'),
(790, 'operationlogheader', 313, 'updated', '2020-01-15 13:57:04', 'shivam001'),
(791, 'operationlogheader', 314, 'inserted', '2020-01-15 15:26:55', 'shivam001'),
(792, 'operationlogheader', 303, 'updated', '2020-01-15 15:52:22', 'emp-002'),
(793, 'operationlogheader', 314, 'updated', '2020-01-23 15:42:13', 'shivam001');

-- --------------------------------------------------------

--
-- Stand-in structure for view `operationsqnlog`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `operationsqnlog`;
CREATE TABLE IF NOT EXISTS `operationsqnlog` (
`id` bigint(20)
,`document_no` varchar(50)
,`lot_no` varchar(50)
,`area_code` varchar(50)
,`room_code` varchar(50)
,`batch_no` varchar(50)
,`product_description` varchar(200)
,`operation_type` varchar(200)
,`start_time` varchar(50)
,`end_time` varchar(50)
,`started_by` varchar(250)
,`ended_by` varchar(250)
,`names` text
,`approvedby` text
,`approvedon` text
,`equipments` text
);

-- --------------------------------------------------------

--
-- Table structure for table `pts_audit_log`
--

DROP TABLE IF EXISTS `pts_audit_log`;
CREATE TABLE IF NOT EXISTS `pts_audit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `terminalID` varchar(500) DEFAULT NULL,
  `processname_master` varchar(500) DEFAULT NULL,
  `activity_sub_task_actions` varchar(500) DEFAULT NULL,
  `action_performed` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(500) DEFAULT NULL,
  `oldval` varchar(500) DEFAULT NULL,
  `newval` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1327 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pts_audit_log`
--

INSERT INTO `pts_audit_log` (`id`, `terminalID`, `processname_master`, `activity_sub_task_actions`, `action_performed`, `created_on`, `created_by`, `oldval`, `newval`) VALUES
(1, 'N/A', 'Employee Master', 'emp_password', 'update', '2020-01-15 05:36:41', 'Superadmin', 'a5a03efcb7da3fdaf68fcb1fc2ef07ae29add8f96a9dde88b75e05afce6f8d65', '9a73f0609eb6197596d89581efb2e9cdd6640987b5490e84471c5f43f68ae1af'),
(2, 'N/A', 'Employee Master', 'emp_code', 'update', '2020-01-15 05:36:41', 'Superadmin', 'rahul00288', 'rahul002888'),
(3, 'N/A', 'Employee Master', 'emp_name', 'update', '2020-01-15 05:36:41', 'Superadmin', 'Rahul Chauhan2', 'Rahul Chauhan22'),
(4, 'N/A', 'Employee Master', 'emp_address', 'update', '2020-01-15 05:36:41', 'Superadmin', 'Ghaziabadd', 'Ghaziabaddd'),
(5, 'N/A', 'Employee Master', 'role_id', 'update', '2020-01-15 05:36:41', 'Superadmin', '1', '2'),
(6, 'N/A', 'Employee Master', 'block_code', 'update', '2020-01-15 05:36:41', 'Superadmin', 'Block-OSD2', 'Block-OSD'),
(7, 'N/A', 'Employee Master', 'Sub_block_code', 'update', '2020-01-15 05:36:41', 'Superadmin', 'OSD-2', 'OSD-1'),
(8, 'N/A', 'Employee Master', 'area_code', 'update', '2020-01-15 05:36:41', 'Superadmin', 'A-006', 'A-001,A-002,A-003,A-004,A-111,A-112,A-113,A-114,A-115,A-116,A-117,A-118'),
(9, 'N/A', 'Employee Master', 'emp_remark', 'update', '2020-01-15 05:36:41', 'Superadmin', 'test supervisor', 'test supervisor2'),
(10, 'N/A', 'Employee Master', 'Employee', 'Insert', '2020-03-16 12:18:07', 'Superadmin', 'N/A', 'N/A'),
(11, 'N/A', 'Employee Master', 'is_active', 'update', '2020-03-16 12:18:07', 'Superadmin', '1', '0'),
(12, 'N/A', 'Room In', 'Employee', 'Insert', '2020-03-16 12:52:05', '33', 'N/A', 'N/A'),
(13, 'N/A', 'Room Out', 'Employee', 'Insert', '2020-03-02 09:12:04', '33', '0', '0'),
(14, 'N/A', 'Room Out', 'Employee', 'Insert', '2020-03-03 07:29:09', '33', '0', '0'),
(15, 'N/A', 'Room Out', 'Employee', 'Insert', '2020-03-03 09:43:08', '33', '0', '0'),
(16, 'N/A', 'Room Out', 'Employee', 'Insert', '2020-03-03 10:11:15', '33', '0', '0'),
(17, 'N/A', 'Room Out', 'Employee', 'Insert', '2020-03-05 08:25:32', '33', '0', '0'),
(18, 'N/A', 'Room Out', 'Employee', 'Insert', '2020-03-16 12:52:05', '33', '1', '0'),
(19, 'N/A', 'Room In', 'Employee', 'Insert', '2020-03-16 12:59:54', '33', 'N/A', 'N/A'),
(20, 'N/A', 'Room Out', 'Employee', 'Insert', '2020-03-02 09:12:04', '33', '0', '0'),
(21, 'N/A', 'Room Out', 'Employee', 'Insert', '2020-03-03 07:29:09', '33', '0', '0'),
(22, 'N/A', 'Room Out', 'Employee', 'Insert', '2020-03-03 09:43:08', '33', '0', '0'),
(23, 'N/A', 'Room Out', 'Employee', 'Insert', '2020-03-03 10:11:15', '33', '0', '0'),
(24, 'N/A', 'Room Out', 'Employee', 'Insert', '2020-03-05 08:25:32', '33', '0', '0'),
(25, 'N/A', 'Room Out', 'Employee', 'Insert', '2020-03-16 12:52:05', '33', '0', '0'),
(26, 'N/A', 'Room Out', 'Employee', 'Insert', '2020-03-16 12:59:54', '33', '1', '0'),
(27, 'N/A', 'Room In', 'Employee', 'Insert', '2020-03-16 13:12:44', '33', 'N/A', 'N/A'),
(28, 'TAA-78', '50', '6', 'Insert', '2020-03-16 15:57:17', 'sid', 'N/A', 'N/A'),
(29, 'N/A', 'Room In', 'Employee', 'Insert', '2020-03-16 16:40:19', '34', 'N/A', 'N/A'),
(30, 'N/A', '2', 'start', 'approve', '2020-03-16 16:40:46', 'abdul ', 'N/A', 'N/A'),
(31, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-16 17:44:03', 'sid', 'N/A', 'N/A'),
(32, 'TAA-78', '50', '1', 'Activity Start', '2020-03-16 17:44:03', 'sid', 'N/A', 'N/A'),
(33, 'N/A', '4', 'start', 'N/A', '2020-03-16 17:44:03', 'sid', 'N/A', 'N/A'),
(34, 'N/A', 'User Login', 'N/A', 'Login Fail due to credential not match', '2020-03-16 17:44:15', 'sid', 'N/A', 'N/A'),
(35, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-17 06:20:52', 'sid', 'N/A', 'N/A'),
(36, 'TAA-78', '50', '1', 'Activity Start', '2020-03-17 06:20:53', 'sid', 'N/A', 'N/A'),
(37, 'N/A', '5', 'start', 'N/A', '2020-03-17 06:20:53', 'sid', 'N/A', 'N/A'),
(38, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong password', '2020-03-17 06:21:11', 'sid/8ad64bacd53088776e515f6514ee49c3b1791124755fe0c4056fe8d8a97974fe', 'N/A', 'N/A'),
(39, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong userid', '2020-03-17 06:21:22', 'sid23/bf9e8e7955e8c87d5fe571ad0be25008654157a58a7a34353b81d747e8c6577a', 'N/A', 'N/A'),
(40, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-17 06:36:07', 'sid', 'N/A', 'N/A'),
(41, 'N/A', '5', 'stop', 'N/A', '2020-03-17 06:36:08', 'sid', 'N/A', 'N/A'),
(42, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-17 06:40:53', 'sid', 'N/A', 'N/A'),
(43, 'TAA-78', '50', '6', 'Activity Start', '2020-03-17 06:40:53', 'sid', 'N/A', 'N/A'),
(44, 'N/A', '6', 'start', 'N/A', '2020-03-17 06:40:53', 'sid', 'N/A', 'N/A'),
(45, 'TAA-78', 'process log id = 50', 'Activity = 6name isProduction', 'Activity Successfully Started.', '2020-03-17 06:40:53', 'sid', 'N/A', 'N/A'),
(46, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-17 06:51:35', 'sid', 'N/A', 'N/A'),
(47, 'TAA-78', 'process log id = 50', 'Activity = 1name isType A Cleaning', 'Activity Successfully Started.', '2020-03-17 06:51:35', 'sid', 'N/A', 'N/A'),
(48, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-17 07:42:06', 'sid', 'N/A', 'N/A'),
(49, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-17 07:43:49', 'sid', 'N/A', 'N/A'),
(50, 'TAA-78', 'process log id = 64', 'Activity = 6name isProduction', 'Activity Successfully Started.', '2020-03-17 07:43:49', 'sid', 'N/A', 'N/A'),
(51, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-17 07:45:01', 'sid', 'N/A', 'N/A'),
(52, 'TAA-78', 'process log id = 64', 'Activity = 4name isType D Cleaning', 'Activity Successfully Started.', '2020-03-17 07:45:01', 'sid', 'N/A', 'N/A'),
(53, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-17 08:01:39', 'sid', 'N/A', 'N/A'),
(54, 'TAA-78', 'process log id = 64 Activity = 6 name is Production', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-03-17 08:01:40', 'sid', 'N/A', 'N/A'),
(55, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong password which is a5e98e1081296287be24c374dc167e5f66feb14719d1b166ee8667b93cabaced', '2020-03-25 07:35:14', 'sid', 'N/A', 'N/A'),
(56, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong password which is 7b39769cd131e5b2f4a5ba1d105746cf3f66c0ddc17473d25073ca33956fc553', '2020-03-25 07:35:33', 'sid', 'N/A', 'N/A'),
(57, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-25 07:37:46', 'sid', 'N/A', 'N/A'),
(58, 'TAA-78', 'process log id = 50 Activity = 6 name is Production', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-03-25 07:37:46', 'sid', 'N/A', 'N/A'),
(59, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-25 07:38:37', 'abdul', 'N/A', 'N/A'),
(60, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-25 07:51:39', 'sid', 'N/A', 'N/A'),
(61, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-03-25 07:51:39', 'sid', 'N/A', 'N/A'),
(62, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-25 08:23:18', 'sid', 'N/A', 'N/A'),
(63, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 05:01:13', 'sa', 'N/A', 'N/A'),
(64, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 05:01:26', 'sid', 'N/A', 'N/A'),
(65, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 05:02:24', 'sid', 'N/A', 'N/A'),
(66, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 05:02:47', 'sid', 'N/A', 'N/A'),
(67, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 05:07:33', 'sid', 'N/A', 'N/A'),
(68, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 05:10:50', 'sid', 'N/A', 'N/A'),
(69, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 06:42:15', 'sid', 'N/A', 'N/A'),
(70, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 08:41:11', 'sid', 'N/A', 'N/A'),
(71, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 08:41:34', 'sid', 'N/A', 'N/A'),
(72, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 08:41:57', 'sid', 'N/A', 'N/A'),
(73, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 08:42:10', 'sid', 'N/A', 'N/A'),
(74, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 08:42:40', 'sid', 'N/A', 'N/A'),
(75, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 08:43:10', 'sid', 'N/A', 'N/A'),
(76, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 09:11:10', 'sid', 'N/A', 'N/A'),
(77, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 09:44:51', 'sid', 'N/A', 'N/A'),
(78, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-26 09:45:22', 'sid', 'N/A', 'N/A'),
(79, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-27 05:02:32', 'sid', 'N/A', 'N/A'),
(80, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-03-27 05:02:33', 'sid', 'N/A', 'N/A'),
(81, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong password which is c39fa39d630058832d5e58e0f1bfb18de07ebc09a61361bd02d53be79e12ebf6', '2020-03-27 05:03:50', 'abdul', 'N/A', 'N/A'),
(82, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong password which is c39fa39d630058832d5e58e0f1bfb18de07ebc09a61361bd02d53be79e12ebf6', '2020-03-27 05:04:19', 'abdul', 'N/A', 'N/A'),
(83, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 05:49:57', 'sid', 'N/A', 'N/A'),
(84, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-03-31 05:49:58', 'sid', 'N/A', 'N/A'),
(85, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 06:11:30', 'sid', 'N/A', 'N/A'),
(86, 'TAA-78', 'process log id = 50 Activity = 3 name is Type C Cleaning', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-03-31 06:11:31', 'sid', 'N/A', 'N/A'),
(87, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 06:20:53', 'sid', 'N/A', 'N/A'),
(88, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 06:21:11', 'sid', 'N/A', 'N/A'),
(89, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 06:22:36', 'sa', 'N/A', 'N/A'),
(90, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011047B and batch no is: 1212', 'Activity Start fail because user was not room in', '2020-03-31 06:22:36', 'Super Admin', 'N/A', 'N/A'),
(91, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 06:22:55', 'sid', 'N/A', 'N/A'),
(92, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-03-31 06:22:55', 'sid', 'N/A', 'N/A'),
(93, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 06:23:19', 'sid', 'N/A', 'N/A'),
(94, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong password which is c39fa39d630058832d5e58e0f1bfb18de07ebc09a61361bd02d53be79e12ebf6', '2020-03-31 06:24:40', 'abdul', 'N/A', 'N/A'),
(95, 'N/A', 'Employee Master', 'emp_password', 'update', '2020-01-15 05:23:22', 'Superadmin', '5e5bcfe4f6bdcbab0f87fd45d24de6bf6cc800ec20c7cf65dbce72dcf00ea3a3', 'bf9e8e7955e8c87d5fe571ad0be25008654157a58a7a34353b81d747e8c6577a'),
(96, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 06:27:22', 'abdul', 'N/A', 'N/A'),
(97, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 06:29:24', 'sid', 'N/A', 'N/A'),
(98, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-03-31 06:29:25', 'sid', 'N/A', 'N/A'),
(99, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 06:29:48', 'abdul', 'N/A', 'N/A'),
(100, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 06:30:02', 'sid', 'N/A', 'N/A'),
(101, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 06:30:18', 'abdul', 'N/A', 'N/A'),
(102, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 07:22:36', 'sid', 'N/A', 'N/A'),
(103, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 07:23:57', 'abdul', 'N/A', 'N/A'),
(104, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 07:24:24', 'abdul', 'N/A', 'N/A'),
(105, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 12:54:22', 'sid', 'N/A', 'N/A'),
(106, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-03-31 12:56:40', 'abdul', 'N/A', 'N/A'),
(107, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 04:47:06', 'sid', 'N/A', 'N/A'),
(108, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-04-01 04:47:07', 'sid', 'N/A', 'N/A'),
(109, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 09:41:47', 'sid', 'N/A', 'N/A'),
(110, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/undefined', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-04-01 09:41:48', 'sid', 'N/A', 'N/A'),
(111, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 09:42:01', 'sid', 'N/A', 'N/A'),
(112, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 09:43:40', 'abdul', 'N/A', 'N/A'),
(113, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 09:44:06', 'abdul', 'N/A', 'N/A'),
(114, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 09:47:11', 'sid', 'N/A', 'N/A'),
(115, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/undefined', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-04-01 09:47:11', 'sid', 'N/A', 'N/A'),
(116, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 09:48:01', 'sid', 'N/A', 'N/A'),
(117, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 09:48:15', 'abdul', 'N/A', 'N/A'),
(118, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong password which is a5e98e1081296287be24c374dc167e5f66feb14719d1b166ee8667b93cabaced', '2020-04-01 09:51:01', 'QA', 'N/A', 'N/A'),
(119, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 09:51:16', 'QA', 'N/A', 'N/A'),
(120, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 10:36:15', 'sid', 'N/A', 'N/A'),
(121, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 10:36:27', 'abdul', 'N/A', 'N/A'),
(122, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 10:52:28', 'sid', 'N/A', 'N/A'),
(123, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 10:53:21', 'abdul', 'N/A', 'N/A'),
(124, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 11:03:44', 'sid', 'N/A', 'N/A'),
(125, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 11:04:15', 'abdul', 'N/A', 'N/A'),
(126, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 17:13:46', 'sid', 'N/A', 'N/A'),
(127, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 17:19:13', 'sid', 'N/A', 'N/A'),
(128, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 17:21:25', 'sid', 'N/A', 'N/A'),
(129, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 17:23:04', 'sid', 'N/A', 'N/A'),
(130, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 17:24:00', 'abdul', 'N/A', 'N/A'),
(131, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 17:32:52', 'sid', 'N/A', 'N/A'),
(132, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 17:33:10', 'abdul', 'N/A', 'N/A'),
(133, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 17:34:33', 'sid', 'N/A', 'N/A'),
(134, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-01 17:35:37', 'abdul', 'N/A', 'N/A'),
(135, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-02 07:41:17', 'QA', 'N/A', 'N/A'),
(136, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-02 16:13:58', 'sid', 'N/A', 'N/A'),
(137, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-02 16:19:44', 'sid', 'N/A', 'N/A'),
(138, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 04:29:40', 'sid', 'N/A', 'N/A'),
(139, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 05:03:34', 'abdul', 'N/A', 'N/A'),
(140, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 05:05:14', 'sid', 'N/A', 'N/A'),
(141, 'N/A', 'Employee Master', 'Employee', 'Insert', '2020-04-03 05:58:28', 'Superadmin', 'N/A', 'N/A'),
(142, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 08:24:53', 'sid', 'N/A', 'N/A'),
(143, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/undefined', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-04-03 08:24:55', 'sid', 'N/A', 'N/A'),
(144, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 08:25:12', 'abdul', 'N/A', 'N/A'),
(145, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 08:26:14', 'abdul', 'N/A', 'N/A'),
(146, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 08:26:28', 'QA', 'N/A', 'N/A'),
(147, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 08:37:06', 'abdul', 'N/A', 'N/A'),
(148, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/undefined', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-04-03 08:37:06', 'abdul ', 'N/A', 'N/A'),
(149, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 09:24:36', 'abdul', 'N/A', 'N/A'),
(150, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 09:25:16', 'sid', 'N/A', 'N/A'),
(151, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 09:44:22', 'abdul', 'N/A', 'N/A'),
(152, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 09:44:51', 'sid', 'N/A', 'N/A'),
(153, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 09:52:41', 'abdul', 'N/A', 'N/A'),
(154, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 09:53:06', 'sid', 'N/A', 'N/A'),
(155, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 09:53:20', 'abdul', 'N/A', 'N/A'),
(156, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 09:54:27', 'sid', 'N/A', 'N/A'),
(157, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 09:54:43', 'abdul', 'N/A', 'N/A'),
(158, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 09:56:14', 'sid', 'N/A', 'N/A'),
(159, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 09:56:37', 'abdul', 'N/A', 'N/A'),
(160, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 10:25:45', 'sid', 'N/A', 'N/A'),
(161, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-03 10:26:02', 'abdul', 'N/A', 'N/A'),
(162, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 11:26:17', 'sid', 'N/A', 'N/A'),
(163, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 11:31:32', 'sid', 'N/A', 'N/A'),
(164, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 13:11:49', 'sid', 'N/A', 'N/A'),
(165, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-04-04 13:11:49', 'sid', 'N/A', 'N/A'),
(166, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 13:13:42', 'sid', 'N/A', 'N/A'),
(167, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-04-04 13:13:42', 'sid', 'N/A', 'N/A'),
(168, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 13:16:53', 'sid', 'N/A', 'N/A'),
(169, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 13:20:41', 'abdul', 'N/A', 'N/A'),
(170, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 13:21:16', 'abdul', 'N/A', 'N/A'),
(171, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 13:29:36', 'abdul', 'N/A', 'N/A'),
(172, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 13:30:42', 'abdul', 'N/A', 'N/A'),
(173, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 13:36:14', 'abdul', 'N/A', 'N/A'),
(174, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 13:40:25', 'abdul', 'N/A', 'N/A'),
(175, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 13:41:51', 'sid', 'N/A', 'N/A'),
(176, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 13:47:44', 'sid', 'N/A', 'N/A'),
(177, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 13:48:15', 'abdul', 'N/A', 'N/A'),
(178, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 13:49:27', 'abdul', 'N/A', 'N/A'),
(179, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-04 13:49:52', 'sid', 'N/A', 'N/A'),
(180, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-07 01:17:22', 'sid', 'N/A', 'N/A'),
(181, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-07 01:28:38', 'sid', 'N/A', 'N/A'),
(182, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 06:00:03', 'sid', 'N/A', 'N/A'),
(183, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 06:00:48', 'sid', 'N/A', 'N/A'),
(184, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 06:02:02', 'sid', 'N/A', 'N/A'),
(185, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-04-09 06:02:03', 'sid', 'N/A', 'N/A'),
(186, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 06:03:31', 'sid', 'N/A', 'N/A'),
(187, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 06:04:40', 'sid', 'N/A', 'N/A'),
(188, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 06:05:30', 'sid', 'N/A', 'N/A'),
(189, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 06:24:28', 'sid', 'N/A', 'N/A'),
(190, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011047B and batch no is: 1212', 'Activity Successfully Started.', '2020-04-09 06:24:29', 'sid', 'N/A', 'N/A'),
(191, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 06:24:49', 'abdul', 'N/A', 'N/A'),
(192, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 06:25:39', 'abdul', 'N/A', 'N/A'),
(193, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 06:26:09', 'abdul', 'N/A', 'N/A'),
(194, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 06:26:28', 'sid', 'N/A', 'N/A'),
(195, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 06:26:58', 'sid', 'N/A', 'N/A'),
(196, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 06:27:10', 'abdul', 'N/A', 'N/A'),
(197, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 07:08:19', 'sid', 'N/A', 'N/A'),
(198, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 07:09:48', 'sid', 'N/A', 'N/A'),
(199, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 07:10:57', 'sid', 'N/A', 'N/A'),
(200, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 07:12:36', 'sid', 'N/A', 'N/A'),
(201, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 07:14:07', 'sid', 'N/A', 'N/A'),
(202, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 07:14:56', 'sid', 'N/A', 'N/A'),
(203, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 07:16:12', 'sid', 'N/A', 'N/A'),
(204, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 07:16:42', 'sid', 'N/A', 'N/A'),
(205, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 07:17:30', 'sid', 'N/A', 'N/A'),
(206, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 07:18:15', 'sid', 'N/A', 'N/A'),
(207, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 07:19:10', 'sid', 'N/A', 'N/A'),
(208, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 07:19:41', 'sid', 'N/A', 'N/A'),
(209, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 07:20:32', 'sid', 'N/A', 'N/A'),
(210, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 08:13:44', 'sid', 'N/A', 'N/A'),
(211, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 08:17:20', 'sid', 'N/A', 'N/A'),
(212, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 08:19:18', 'sid', 'N/A', 'N/A'),
(213, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 08:20:59', 'sid', 'N/A', 'N/A'),
(214, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 08:22:38', 'sid', 'N/A', 'N/A'),
(215, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 08:24:56', 'sid', 'N/A', 'N/A'),
(216, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 08:25:18', 'sid', 'N/A', 'N/A'),
(217, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 08:33:12', 'sid', 'N/A', 'N/A'),
(218, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 08:37:23', 'sid', 'N/A', 'N/A'),
(219, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 08:40:17', 'sid', 'N/A', 'N/A'),
(220, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 08:41:51', 'abdul', 'N/A', 'N/A'),
(221, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 08:43:28', 'sid', 'N/A', 'N/A'),
(222, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 08:46:28', 'sid', 'N/A', 'N/A'),
(223, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 09:11:20', 'abdul', 'N/A', 'N/A'),
(224, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 09:11:42', 'abdul', 'N/A', 'N/A'),
(225, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 09:57:19', 'abdul', 'N/A', 'N/A'),
(226, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-09 11:30:56', 'abdul', 'N/A', 'N/A'),
(227, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 05:16:10', 'sid', 'N/A', 'N/A'),
(228, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011047B and batch no is: JKT1212', 'Activity Successfully Started.', '2020-04-13 05:16:11', 'sid', 'N/A', 'N/A'),
(229, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 05:21:05', 'abdul', 'N/A', 'N/A'),
(230, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 05:21:25', 'sid', 'N/A', 'N/A'),
(231, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 05:22:11', 'abdul', 'N/A', 'N/A'),
(232, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 05:22:39', 'QA', 'N/A', 'N/A'),
(233, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 07:11:06', 'sid', 'N/A', 'N/A'),
(234, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011047B and batch no is: JKT1212', 'Activity Successfully Started.', '2020-04-13 07:11:06', 'sid', 'N/A', 'N/A'),
(235, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 07:11:28', 'abdul', 'N/A', 'N/A'),
(236, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 07:11:46', 'sid', 'N/A', 'N/A'),
(237, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 07:12:17', 'abdul', 'N/A', 'N/A'),
(238, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 07:18:44', 'QA', 'N/A', 'N/A'),
(239, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 07:21:21', 'sid', 'N/A', 'N/A'),
(240, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011047B and batch no is: JKT1212', 'Activity Successfully Started.', '2020-04-13 07:21:22', 'sid', 'N/A', 'N/A'),
(241, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 07:21:37', 'abdul', 'N/A', 'N/A'),
(242, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 07:21:52', 'sid', 'N/A', 'N/A'),
(243, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 07:22:09', 'abdul', 'N/A', 'N/A'),
(244, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 09:01:54', 'sid', 'N/A', 'N/A'),
(245, 'TAA-78', 'process log id = 51 Activity = 1 name is Type A Cleaning', 'where as product is :0011047B and batch no is: JKT1212', 'Activity Successfully Started.', '2020-04-13 09:01:55', 'sid', 'N/A', 'N/A'),
(246, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 09:02:24', 'abdul', 'N/A', 'N/A'),
(247, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 09:02:49', 'sid', 'N/A', 'N/A'),
(248, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 09:03:12', 'abdul', 'N/A', 'N/A'),
(249, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 09:04:33', 'QA', 'N/A', 'N/A'),
(250, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 09:17:38', 'sid', 'N/A', 'N/A'),
(251, 'TAA-78', 'process log id = 64 Activity = 1 name is Type A Cleaning', 'where as product is :0011047B and batch no is: JKT1212', 'Activity Successfully Started.', '2020-04-13 09:17:38', 'sid', 'N/A', 'N/A'),
(252, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 09:18:00', 'abdul', 'N/A', 'N/A'),
(253, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 09:18:25', 'sid', 'N/A', 'N/A'),
(254, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 09:18:49', 'abdul', 'N/A', 'N/A'),
(255, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 09:48:00', 'sid', 'N/A', 'N/A'),
(256, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 10:01:39', 'sid', 'N/A', 'N/A'),
(257, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :0011047B and batch no is: JKT1212', 'Activity Successfully Started.', '2020-04-13 10:01:39', 'sid', 'N/A', 'N/A'),
(258, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 10:03:37', 'sid', 'N/A', 'N/A'),
(259, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 10:04:26', 'abdul', 'N/A', 'N/A'),
(260, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 10:26:38', 'QA', 'N/A', 'N/A'),
(261, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 15:13:32', 'sid', 'N/A', 'N/A'),
(262, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011047B and batch no is: JKT1212', 'Activity Successfully Started.', '2020-04-13 15:13:33', 'sid', 'N/A', 'N/A'),
(263, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 15:13:51', 'abdul', 'N/A', 'N/A'),
(264, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 15:14:06', 'sid', 'N/A', 'N/A'),
(265, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-13 15:14:25', 'abdul', 'N/A', 'N/A'),
(266, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 06:40:52', 'sid', 'N/A', 'N/A'),
(267, 'TAA-78', 'process log id = 50 Activity = 6 name is Production', 'where as product is :0011047B and batch no is: JKT1212', 'Activity Successfully Started.', '2020-04-14 06:40:53', 'sid', 'N/A', 'N/A'),
(268, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 06:41:32', 'abdul', 'N/A', 'N/A'),
(269, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 06:42:06', 'sid', 'N/A', 'N/A'),
(270, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 06:46:40', 'sid', 'N/A', 'N/A'),
(271, 'TAA-78', 'process log id = 50 Activity = 6 name is Production', 'where as product is :0011047B and batch no is: JKT1212', 'Activity Successfully Started.', '2020-04-14 06:46:40', 'sid', 'N/A', 'N/A'),
(272, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 06:46:59', 'abdul', 'N/A', 'N/A'),
(273, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 06:47:27', 'sid', 'N/A', 'N/A'),
(274, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 06:47:53', 'abdul', 'N/A', 'N/A'),
(275, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 08:31:02', 'sid', 'N/A', 'N/A'),
(276, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 08:56:17', 'abdul', 'N/A', 'N/A'),
(277, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 09:27:27', 'sid', 'N/A', 'N/A'),
(278, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 09:27:51', 'abdul', 'N/A', 'N/A'),
(279, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 10:44:44', 'sid', 'N/A', 'N/A'),
(280, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 10:45:02', 'abdul', 'N/A', 'N/A'),
(281, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 11:26:03', 'sid', 'N/A', 'N/A'),
(282, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 11:33:02', 'sid', 'N/A', 'N/A'),
(283, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 11:33:29', 'sid', 'N/A', 'N/A'),
(284, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-14 11:33:51', 'abdul', 'N/A', 'N/A'),
(285, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 04:04:44', 'abdul', 'N/A', 'N/A'),
(286, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 04:57:07', 'sid', 'N/A', 'N/A'),
(287, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 05:10:02', 'sid', 'N/A', 'N/A'),
(288, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 05:11:01', 'abdul', 'N/A', 'N/A'),
(289, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 05:25:19', 'sid', 'N/A', 'N/A'),
(290, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 05:27:17', 'abdul', 'N/A', 'N/A'),
(291, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 06:30:45', 'sid', 'N/A', 'N/A'),
(292, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 06:31:01', 'abdul', 'N/A', 'N/A'),
(293, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 07:16:23', 'QA', 'N/A', 'N/A'),
(294, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 07:58:13', 'sid', 'N/A', 'N/A'),
(295, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 07:59:06', 'abdul', 'N/A', 'N/A'),
(296, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 08:30:54', 'sid', 'N/A', 'N/A'),
(297, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 08:31:14', 'abdul', 'N/A', 'N/A'),
(298, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 10:58:29', 'sid', 'N/A', 'N/A'),
(299, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 10:58:47', 'abdul', 'N/A', 'N/A'),
(300, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 11:13:05', 'QA', 'N/A', 'N/A'),
(301, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 11:44:29', 'sid', 'N/A', 'N/A'),
(302, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 11:44:51', 'abdul', 'N/A', 'N/A'),
(303, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 12:10:04', 'sid', 'N/A', 'N/A'),
(304, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 12:10:17', 'sid', 'N/A', 'N/A'),
(305, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 12:10:31', 'abdul', 'N/A', 'N/A'),
(306, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 12:11:56', 'QA', 'N/A', 'N/A'),
(307, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 12:13:36', 'QA', 'N/A', 'N/A'),
(308, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 12:15:24', 'sid', 'N/A', 'N/A'),
(309, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 12:15:36', 'sid', 'N/A', 'N/A'),
(310, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 12:15:53', 'abdul', 'N/A', 'N/A'),
(311, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 12:16:27', 'sid', 'N/A', 'N/A'),
(312, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 12:16:41', 'abdul', 'N/A', 'N/A'),
(313, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 12:17:22', 'QA', 'N/A', 'N/A'),
(314, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 12:20:21', 'QA', 'N/A', 'N/A'),
(315, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 15:50:13', 'sid', 'N/A', 'N/A'),
(316, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 15:51:01', 'abdul', 'N/A', 'N/A'),
(317, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-15 15:52:42', 'QA', 'N/A', 'N/A'),
(318, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-16 04:54:08', 'sid', 'N/A', 'N/A'),
(319, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-16 04:54:26', 'sid', 'N/A', 'N/A'),
(320, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-16 04:54:54', 'abdul', 'N/A', 'N/A'),
(321, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-16 05:00:41', 'QA', 'N/A', 'N/A'),
(322, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-16 05:01:58', 'sid', 'N/A', 'N/A'),
(323, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-16 05:02:51', 'abdul', 'N/A', 'N/A'),
(324, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-16 05:05:43', 'QA', 'N/A', 'N/A'),
(325, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-16 08:47:33', 'sid', 'N/A', 'N/A'),
(326, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-16 08:47:55', 'abdul', 'N/A', 'N/A'),
(327, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-16 09:08:47', 'QA', 'N/A', 'N/A'),
(328, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-17 05:27:13', 'sid', 'N/A', 'N/A'),
(329, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-17 05:27:46', 'abdul', 'N/A', 'N/A'),
(330, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-20 03:50:10', 'sid', 'N/A', 'N/A'),
(331, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:23:19', 'sid', 'N/A', 'N/A'),
(332, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:39:39', 'sid', 'N/A', 'N/A'),
(333, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:41:16', 'sid', 'N/A', 'N/A'),
(334, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:41:47', 'sid', 'N/A', 'N/A'),
(335, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:42:19', 'sid', 'N/A', 'N/A'),
(336, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:42:48', 'sid', 'N/A', 'N/A'),
(337, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:43:05', 'sid', 'N/A', 'N/A'),
(338, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:43:50', 'sid', 'N/A', 'N/A'),
(339, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:44:56', 'sid', 'N/A', 'N/A'),
(340, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:45:31', 'sid', 'N/A', 'N/A'),
(341, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:47:23', 'sid', 'N/A', 'N/A'),
(342, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:47:53', 'sid', 'N/A', 'N/A'),
(343, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:54:17', 'sid', 'N/A', 'N/A'),
(344, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:54:32', 'sid', 'N/A', 'N/A'),
(345, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:55:22', 'sid', 'N/A', 'N/A'),
(346, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:56:46', 'sid', 'N/A', 'N/A'),
(347, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-21 04:57:20', 'sid', 'N/A', 'N/A'),
(348, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-22 06:09:18', 'sid', 'N/A', 'N/A'),
(349, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-22 11:05:05', 'sid', 'N/A', 'N/A'),
(350, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-22 11:05:06', 'sid', 'N/A', 'N/A'),
(351, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-22 11:37:55', 'abdul', 'N/A', 'N/A'),
(352, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-22 11:57:16', 'sid', 'N/A', 'N/A'),
(353, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-23 05:58:39', 'sid', 'N/A', 'N/A'),
(354, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-23 07:00:07', 'sid', 'N/A', 'N/A'),
(355, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-23 07:01:39', 'abdul', 'N/A', 'N/A'),
(356, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-23 10:28:17', 'sid', 'N/A', 'N/A'),
(357, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-23 10:29:35', 'sid', 'N/A', 'N/A'),
(358, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-23 10:29:59', 'sid', 'N/A', 'N/A'),
(359, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-23 10:31:17', 'sid', 'N/A', 'N/A'),
(360, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-23 10:56:28', 'sid', 'N/A', 'N/A'),
(361, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-23 10:56:29', 'sid', 'N/A', 'N/A'),
(362, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-23 11:03:38', 'sid', 'N/A', 'N/A'),
(363, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-27 04:05:36', 'sid', 'N/A', 'N/A'),
(364, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-27 04:05:37', 'sid', 'N/A', 'N/A'),
(365, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-27 04:08:44', 'sid', 'N/A', 'N/A'),
(366, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-27 05:00:56', 'sid', 'N/A', 'N/A'),
(367, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-27 05:00:57', 'sid', 'N/A', 'N/A'),
(368, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-27 05:02:45', 'sid', 'N/A', 'N/A'),
(369, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-27 05:03:02', 'abdul', 'N/A', 'N/A'),
(370, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-27 05:03:23', 'abdul', 'N/A', 'N/A'),
(371, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-27 05:03:44', 'abdul', 'N/A', 'N/A'),
(372, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-27 05:04:23', 'sid', 'N/A', 'N/A'),
(373, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-27 05:04:23', 'sid', 'N/A', 'N/A'),
(374, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-27 05:06:14', 'sid', 'N/A', 'N/A'),
(375, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-27 05:06:30', 'abdul', 'N/A', 'N/A'),
(376, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-27 05:06:52', 'sid', 'N/A', 'N/A'),
(377, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-27 05:06:53', 'sid', 'N/A', 'N/A'),
(378, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-27 06:32:46', 'sid', 'N/A', 'N/A'),
(379, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-27 11:48:40', 'sid', 'N/A', 'N/A'),
(380, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-28 09:35:04', 'sid', 'N/A', 'N/A'),
(381, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-28 09:35:04', 'sid', 'N/A', 'N/A'),
(382, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-28 09:35:18', 'abdul', 'N/A', 'N/A'),
(383, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-28 09:35:33', 'sid', 'N/A', 'N/A'),
(384, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-28 09:35:48', 'abdul', 'N/A', 'N/A'),
(385, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 04:49:57', 'sid', 'N/A', 'N/A'),
(386, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-29 04:49:57', 'sid', 'N/A', 'N/A'),
(387, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 04:50:41', 'abdul', 'N/A', 'N/A'),
(388, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 04:55:59', 'sid', 'N/A', 'N/A'),
(389, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 07:32:59', 'abdul', 'N/A', 'N/A'),
(390, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 07:59:22', 'sid', 'N/A', 'N/A'),
(391, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 08:00:44', 'sid', 'N/A', 'N/A'),
(392, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 08:11:04', 'sid', 'N/A', 'N/A'),
(393, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 08:12:29', 'sid', 'N/A', 'N/A'),
(394, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 08:13:45', 'sid', 'N/A', 'N/A'),
(395, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Start fail because already activity in progress', '2020-04-29 08:13:46', 'sid', 'N/A', 'N/A'),
(396, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 08:14:28', 'sid', 'N/A', 'N/A'),
(397, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:00:10', 'sid', 'N/A', 'N/A'),
(398, 'TAA-78', 'process log id = 50 Activity = 4 name is Type D Cleaning', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Start fail because already activity in progress', '2020-04-29 09:00:10', 'sid', 'N/A', 'N/A'),
(399, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:01:52', 'sid', 'N/A', 'N/A'),
(400, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:03:01', 'sid', 'N/A', 'N/A'),
(401, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:04:05', 'sid', 'N/A', 'N/A'),
(402, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:04:38', 'sid', 'N/A', 'N/A'),
(403, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:05:11', 'sid', 'N/A', 'N/A'),
(404, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:07:26', 'sid', 'N/A', 'N/A'),
(405, '', 'process log id =  Activity =  name is ', 'where as product is : and batch no is: ', 'Activity Start fail because user was not room in', '2020-04-29 09:07:35', '', 'N/A', 'N/A'),
(406, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:08:08', 'sid', 'N/A', 'N/A'),
(407, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:10:13', 'sid', 'N/A', 'N/A'),
(408, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:10:42', 'sid', 'N/A', 'N/A'),
(409, 'TAA-78', 'process log id = 50 Activity = 4 name is Type D Cleaning', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Start fail because already activity in progress', '2020-04-29 09:10:43', 'sid', 'N/A', 'N/A'),
(410, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:11:37', 'sid', 'N/A', 'N/A'),
(411, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:15:01', 'sid', 'N/A', 'N/A'),
(412, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:16:56', 'sid', 'N/A', 'N/A'),
(413, 'TAA-78', 'process log id = 51 Activity = 2 name is Type B Cleaning', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Start fail because already activity in progress', '2020-04-29 09:16:56', 'sid', 'N/A', 'N/A'),
(414, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:17:16', 'sid', 'N/A', 'N/A'),
(415, 'TAA-78', 'process log id = 51 Activity = 1 name is Type A Cleaning', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-29 09:17:17', 'sid', 'N/A', 'N/A'),
(416, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:17:29', 'abdul', 'N/A', 'N/A'),
(417, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:17:41', 'sid', 'N/A', 'N/A'),
(418, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:17:57', 'abdul', 'N/A', 'N/A'),
(419, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:36:00', 'sid', 'N/A', 'N/A'),
(420, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:36:24', 'abdul', 'N/A', 'N/A'),
(421, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:39:45', 'sid', 'N/A', 'N/A'),
(422, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-29 09:39:45', 'sid', 'N/A', 'N/A'),
(423, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:45:10', 'sid', 'N/A', 'N/A'),
(424, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-29 09:45:11', 'sid', 'N/A', 'N/A'),
(425, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:46:21', 'sid', 'N/A', 'N/A'),
(426, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:47:23', 'abdul', 'N/A', 'N/A'),
(427, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:54:31', 'sid', 'N/A', 'N/A'),
(428, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-29 09:54:31', 'sid', 'N/A', 'N/A'),
(429, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:54:58', 'sid', 'N/A', 'N/A'),
(430, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:55:23', 'abdul', 'N/A', 'N/A'),
(431, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:56:35', 'sid', 'N/A', 'N/A'),
(432, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 09:57:43', 'sid', 'N/A', 'N/A'),
(433, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:01:56', 'sid', 'N/A', 'N/A'),
(434, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:02:35', 'sid', 'N/A', 'N/A'),
(435, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:03:29', 'sid', 'N/A', 'N/A'),
(436, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:04:23', 'sid', 'N/A', 'N/A'),
(437, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B/Type B/Type B/Type B', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-29 10:04:24', 'sid', 'N/A', 'N/A'),
(438, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:04:44', 'sid', 'N/A', 'N/A'),
(439, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:04:58', 'abdul', 'N/A', 'N/A'),
(440, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:06:55', 'sid', 'N/A', 'N/A'),
(441, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:10:14', 'sid', 'N/A', 'N/A'),
(442, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:11:41', 'sid', 'N/A', 'N/A'),
(443, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:12:28', 'sid', 'N/A', 'N/A'),
(444, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:12:59', 'sid', 'N/A', 'N/A'),
(445, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:13:56', 'sid', 'N/A', 'N/A'),
(446, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:25:41', 'sid', 'N/A', 'N/A'),
(447, 'TAA-78', 'process log id = 59 Activity = 19 name is Vertical Sampler and Dies Cleaning Usages Log/Type C', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Start fail because already activity in progress', '2020-04-29 10:25:41', 'sid', 'N/A', 'N/A'),
(448, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:26:11', 'sid', 'N/A', 'N/A'),
(449, 'TAA-78', 'process log id = 59 Activity = 15 name is Vertical Sampler and Dies Cleaning Usages Log/Type C/Type A', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Start fail because already activity in progress', '2020-04-29 10:26:12', 'sid', 'N/A', 'N/A');
INSERT INTO `pts_audit_log` (`id`, `terminalID`, `processname_master`, `activity_sub_task_actions`, `action_performed`, `created_on`, `created_by`, `oldval`, `newval`) VALUES
(450, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:27:25', 'sid', 'N/A', 'N/A'),
(451, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:28:39', 'sid', 'N/A', 'N/A'),
(452, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:31:14', 'sid', 'N/A', 'N/A'),
(453, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:33:16', 'sid', 'N/A', 'N/A'),
(454, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:37:05', 'sid', 'N/A', 'N/A'),
(455, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:42:35', 'sid', 'N/A', 'N/A'),
(456, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:43:31', 'sid', 'N/A', 'N/A'),
(457, 'TAA-78', 'process log id = 59 Activity = 18 name is Vertical Sampler and Dies Cleaning Usages Log/Type C/Type A/Type B/Type B/Type B/Type B/Type B/Type B/Type B', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Start fail because already activity in progress', '2020-04-29 10:43:31', 'sid', 'N/A', 'N/A'),
(458, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:44:10', 'sid', 'N/A', 'N/A'),
(459, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-29 10:44:56', 'sid', 'N/A', 'N/A'),
(460, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Start fail because already activity in progress', '2020-04-29 10:44:56', 'sid', 'N/A', 'N/A'),
(461, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 06:52:55', 'sid', 'N/A', 'N/A'),
(462, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 06:54:55', 'sid', 'N/A', 'N/A'),
(463, 'TAA-78', 'process log id = 59 Activity = 18 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Start fail because already activity in progress', '2020-04-30 06:54:56', 'sid', 'N/A', 'N/A'),
(464, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 06:58:52', 'sid', 'N/A', 'N/A'),
(465, 'TAA-78', 'process log id = 59 Activity = 18 name is Vertical Sampler and Dies Cleaning Usages Log/Type B/Type B', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-30 06:58:52', 'sid', 'N/A', 'N/A'),
(466, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:00:13', 'sid', 'N/A', 'N/A'),
(467, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:08:23', 'abdul', 'N/A', 'N/A'),
(468, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:09:36', 'sid', 'N/A', 'N/A'),
(469, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-30 07:09:37', 'sid', 'N/A', 'N/A'),
(470, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:39:29', 'abdul', 'N/A', 'N/A'),
(471, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:40:04', 'abdul', 'N/A', 'N/A'),
(472, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:40:56', 'abdul', 'N/A', 'N/A'),
(473, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:41:25', 'abdul', 'N/A', 'N/A'),
(474, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:43:09', 'abdul', 'N/A', 'N/A'),
(475, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:43:30', 'abdul', 'N/A', 'N/A'),
(476, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:44:07', 'abdul', 'N/A', 'N/A'),
(477, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:44:53', 'abdul', 'N/A', 'N/A'),
(478, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:45:46', 'abdul', 'N/A', 'N/A'),
(479, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:46:05', 'abdul', 'N/A', 'N/A'),
(480, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:58:16', 'abdul', 'N/A', 'N/A'),
(481, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:58:48', 'abdul', 'N/A', 'N/A'),
(482, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:59:08', 'abdul', 'N/A', 'N/A'),
(483, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:59:36', 'abdul', 'N/A', 'N/A'),
(484, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 07:59:50', 'sid', 'N/A', 'N/A'),
(485, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:00:11', 'abdul', 'N/A', 'N/A'),
(486, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:15:46', 'sid', 'N/A', 'N/A'),
(487, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-30 08:15:46', 'sid', 'N/A', 'N/A'),
(488, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:16:12', 'abdul', 'N/A', 'N/A'),
(489, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:17:10', 'sid', 'N/A', 'N/A'),
(490, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:17:30', 'abdul', 'N/A', 'N/A'),
(491, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:18:27', 'sid', 'N/A', 'N/A'),
(492, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-04-30 08:18:28', 'sid', 'N/A', 'N/A'),
(493, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:18:48', 'abdul', 'N/A', 'N/A'),
(494, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:19:09', 'sid', 'N/A', 'N/A'),
(495, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:19:33', 'abdul', 'N/A', 'N/A'),
(496, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:19:55', 'QA', 'N/A', 'N/A'),
(497, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:20:38', 'QA', 'N/A', 'N/A'),
(498, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:21:24', 'QA', 'N/A', 'N/A'),
(499, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:24:19', 'QA', 'N/A', 'N/A'),
(500, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:26:03', 'QA', 'N/A', 'N/A'),
(501, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:26:42', 'QA', 'N/A', 'N/A'),
(502, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:28:00', 'QA', 'N/A', 'N/A'),
(503, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:29:13', 'QA', 'N/A', 'N/A'),
(504, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:30:08', 'QA', 'N/A', 'N/A'),
(505, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:30:57', 'QA', 'N/A', 'N/A'),
(506, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:35:25', 'QA', 'N/A', 'N/A'),
(507, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:41:22', 'QA', 'N/A', 'N/A'),
(508, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:42:48', 'QA', 'N/A', 'N/A'),
(509, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:46:42', 'QA', 'N/A', 'N/A'),
(510, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 08:47:26', 'QA', 'N/A', 'N/A'),
(511, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 10:09:26', 'sid', 'N/A', 'N/A'),
(512, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-04-30 10:09:45', 'abdul', 'N/A', 'N/A'),
(513, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-01 03:39:36', 'sid', 'N/A', 'N/A'),
(514, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-01 03:39:36', 'sid', 'N/A', 'N/A'),
(515, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-01 03:42:46', 'abdul', 'N/A', 'N/A'),
(516, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-01 03:43:09', 'sid', 'N/A', 'N/A'),
(517, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-01 06:27:57', 'abdul', 'N/A', 'N/A'),
(518, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-01 06:32:21', 'sid', 'N/A', 'N/A'),
(519, 'TAA-78', 'process log id = 51 Activity = 6 name is Production', 'where as product is :0011055B and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-01 06:32:22', 'sid', 'N/A', 'N/A'),
(520, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-01 06:32:43', 'abdul', 'N/A', 'N/A'),
(521, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-01 06:33:10', 'abdul', 'N/A', 'N/A'),
(522, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-01 06:33:41', 'abdul', 'N/A', 'N/A'),
(523, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-01 06:37:41', 'abdul', 'N/A', 'N/A'),
(524, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-01 06:50:39', 'abdul', 'N/A', 'N/A'),
(525, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-01 08:14:12', 'abdul', 'N/A', 'N/A'),
(526, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-01 08:18:28', 'QA', 'N/A', 'N/A'),
(527, 'N/A', 'Room In', 'Employee', 'Insert', '2020-03-05 08:42:34', '47', 'N/A', 'N/A'),
(528, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong password which is bf9e8e7955e8c87d5fe571ad0be25008654157a58a7a34353b81d747e8c6577a', '2020-05-01 08:18:54', 'meera', 'N/A', 'N/A'),
(529, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-01 08:20:27', 'QA', 'N/A', 'N/A'),
(530, 'N/A', 'Room In', 'Employee', 'Insert', '2020-05-01 08:20:28', '47', 'N/A', 'N/A'),
(531, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-01 08:20:54', 'QA', 'N/A', 'N/A'),
(532, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-01 09:37:36', 'abdul', 'N/A', 'N/A'),
(533, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 05:41:41', 'sid', 'N/A', 'N/A'),
(534, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 05:42:32', 'sid', 'N/A', 'N/A'),
(535, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 05:42:54', 'sid', 'N/A', 'N/A'),
(536, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 05:43:56', 'sid', 'N/A', 'N/A'),
(537, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 05:51:19', 'sid', 'N/A', 'N/A'),
(538, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 05:59:46', 'sid', 'N/A', 'N/A'),
(539, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 06:01:22', 'sid', 'N/A', 'N/A'),
(540, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 06:03:34', 'sid', 'N/A', 'N/A'),
(541, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 06:04:39', 'sid', 'N/A', 'N/A'),
(542, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 06:08:21', 'sid', 'N/A', 'N/A'),
(543, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 06:09:12', 'sid', 'N/A', 'N/A'),
(544, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 06:15:29', 'sid', 'N/A', 'N/A'),
(545, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 06:46:25', 'sid', 'N/A', 'N/A'),
(546, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 06:49:53', 'sid', 'N/A', 'N/A'),
(547, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 06:52:20', 'sid', 'N/A', 'N/A'),
(548, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:02:57', 'sid', 'N/A', 'N/A'),
(549, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:03:15', 'sid', 'N/A', 'N/A'),
(550, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:04:39', 'sid', 'N/A', 'N/A'),
(551, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:05:08', 'sid', 'N/A', 'N/A'),
(552, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:06:50', 'sid', 'N/A', 'N/A'),
(553, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:07:06', 'sid', 'N/A', 'N/A'),
(554, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:07:38', 'sid', 'N/A', 'N/A'),
(555, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:10:44', 'sid', 'N/A', 'N/A'),
(556, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:20:16', 'sid', 'N/A', 'N/A'),
(557, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:35:14', 'sid', 'N/A', 'N/A'),
(558, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:37:01', 'sid', 'N/A', 'N/A'),
(559, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:38:00', 'sid', 'N/A', 'N/A'),
(560, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:39:25', 'sid', 'N/A', 'N/A'),
(561, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:40:09', 'sid', 'N/A', 'N/A'),
(562, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:40:37', 'sid', 'N/A', 'N/A'),
(563, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:41:06', 'sid', 'N/A', 'N/A'),
(564, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:41:51', 'sid', 'N/A', 'N/A'),
(565, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:42:49', 'sid', 'N/A', 'N/A'),
(566, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:44:21', 'sid', 'N/A', 'N/A'),
(567, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:44:55', 'sid', 'N/A', 'N/A'),
(568, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:50:40', 'sid', 'N/A', 'N/A'),
(569, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:52:51', 'sid', 'N/A', 'N/A'),
(570, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:54:36', 'sid', 'N/A', 'N/A'),
(571, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 07:57:37', 'sid', 'N/A', 'N/A'),
(572, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 08:09:21', 'sid', 'N/A', 'N/A'),
(573, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 08:10:00', 'sid', 'N/A', 'N/A'),
(574, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 09:08:50', 'abdul', 'N/A', 'N/A'),
(575, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 09:30:37', 'sid', 'N/A', 'N/A'),
(576, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 09:30:54', 'sid', 'N/A', 'N/A'),
(577, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 09:46:39', 'sid', 'N/A', 'N/A'),
(578, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 09:47:02', 'sid', 'N/A', 'N/A'),
(579, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 09:51:02', 'sid', 'N/A', 'N/A'),
(580, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 09:51:33', 'abdul', 'N/A', 'N/A'),
(581, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 09:58:22', 'abdul', 'N/A', 'N/A'),
(582, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 09:58:42', 'abdul', 'N/A', 'N/A'),
(583, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:00:52', 'abdul', 'N/A', 'N/A'),
(584, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:01:19', 'abdul', 'N/A', 'N/A'),
(585, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:04:42', 'abdul', 'N/A', 'N/A'),
(586, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:11:36', 'abdul', 'N/A', 'N/A'),
(587, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:12:03', 'abdul', 'N/A', 'N/A'),
(588, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:12:48', 'sid', 'N/A', 'N/A'),
(589, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:44:02', 'abdul', 'N/A', 'N/A'),
(590, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:44:17', 'abdul', 'N/A', 'N/A'),
(591, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:44:48', 'abdul', 'N/A', 'N/A'),
(592, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:45:25', 'abdul', 'N/A', 'N/A'),
(593, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:45:39', 'abdul', 'N/A', 'N/A'),
(594, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:46:04', 'sid', 'N/A', 'N/A'),
(595, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:46:39', 'sid', 'N/A', 'N/A'),
(596, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:47:07', 'sid', 'N/A', 'N/A'),
(597, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:47:44', 'sid', 'N/A', 'N/A'),
(598, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:48:05', 'sid', 'N/A', 'N/A'),
(599, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:49:46', 'sid', 'N/A', 'N/A'),
(600, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:57:09', 'sid', 'N/A', 'N/A'),
(601, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:59:31', 'sid', 'N/A', 'N/A'),
(602, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 10:59:45', 'sid', 'N/A', 'N/A'),
(603, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 12:21:29', 'sid', 'N/A', 'N/A'),
(604, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-04 12:21:29', 'sid', 'N/A', 'N/A'),
(605, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 12:21:55', 'abdul', 'N/A', 'N/A'),
(606, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 13:16:00', 'sid', 'N/A', 'N/A'),
(607, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-04 13:16:00', 'sid', 'N/A', 'N/A'),
(608, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-04 13:16:44', 'abdul', 'N/A', 'N/A'),
(609, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 04:07:53', 'sid', 'N/A', 'N/A'),
(610, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 04:23:30', 'sid', 'N/A', 'N/A'),
(611, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 04:28:23', 'sid', 'N/A', 'N/A'),
(612, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 04:34:54', 'sid', 'N/A', 'N/A'),
(613, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 04:59:33', 'sid', 'N/A', 'N/A'),
(614, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 05:13:53', 'sid', 'N/A', 'N/A'),
(615, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 05:47:31', 'sid', 'N/A', 'N/A'),
(616, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 05:48:07', 'sid', 'N/A', 'N/A'),
(617, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 05:49:30', 'sid', 'N/A', 'N/A'),
(618, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 05:50:22', 'sid', 'N/A', 'N/A'),
(619, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 05:52:16', 'abdul', 'N/A', 'N/A'),
(620, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 05:54:41', 'sid', 'N/A', 'N/A'),
(621, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 06:05:52', 'sid', 'N/A', 'N/A'),
(622, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 06:06:33', 'sid', 'N/A', 'N/A'),
(623, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 06:07:56', 'sid', 'N/A', 'N/A'),
(624, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 06:08:39', 'sid', 'N/A', 'N/A'),
(625, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 06:08:57', 'sid', 'N/A', 'N/A'),
(626, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 06:18:38', 'sid', 'N/A', 'N/A'),
(627, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 06:18:57', 'sid', 'N/A', 'N/A'),
(628, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 06:20:03', 'sid', 'N/A', 'N/A'),
(629, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 06:20:20', 'sid', 'N/A', 'N/A'),
(630, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 06:23:06', 'sid', 'N/A', 'N/A'),
(631, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 06:23:25', 'sid', 'N/A', 'N/A'),
(632, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 06:23:42', 'sid', 'N/A', 'N/A'),
(633, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 06:24:01', 'abdul', 'N/A', 'N/A'),
(634, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 07:41:55', 'sid', 'N/A', 'N/A'),
(635, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 08:22:06', 'sid', 'N/A', 'N/A'),
(636, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 08:26:42', 'sid', 'N/A', 'N/A'),
(637, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 08:28:08', 'sid', 'N/A', 'N/A'),
(638, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 08:43:49', 'sid', 'N/A', 'N/A'),
(639, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 08:44:48', 'sid', 'N/A', 'N/A'),
(640, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 08:47:17', 'sid', 'N/A', 'N/A'),
(641, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 08:49:27', 'sid', 'N/A', 'N/A'),
(642, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 08:49:49', 'sid', 'N/A', 'N/A'),
(643, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 08:54:13', 'abdul', 'N/A', 'N/A'),
(644, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 09:19:43', 'sid', 'N/A', 'N/A'),
(645, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-05 09:19:43', 'sid', 'N/A', 'N/A'),
(646, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 09:33:20', 'sid', 'N/A', 'N/A'),
(647, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 09:38:36', 'sid', 'N/A', 'N/A'),
(648, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 09:39:18', 'sid', 'N/A', 'N/A'),
(649, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 09:42:42', 'sid', 'N/A', 'N/A'),
(650, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 09:54:22', 'sid', 'N/A', 'N/A'),
(651, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 09:54:50', 'sid', 'N/A', 'N/A'),
(652, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 09:56:26', 'sid', 'N/A', 'N/A'),
(653, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 09:59:16', 'sid', 'N/A', 'N/A'),
(654, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 10:07:14', 'sid', 'N/A', 'N/A'),
(655, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 10:08:19', 'sid', 'N/A', 'N/A'),
(656, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-05 10:08:38', 'abdul', 'N/A', 'N/A'),
(657, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 05:30:34', 'sid', 'N/A', 'N/A'),
(658, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 05:34:56', 'sid', 'N/A', 'N/A'),
(659, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 05:37:08', 'sid', 'N/A', 'N/A'),
(660, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 05:40:43', 'sid', 'N/A', 'N/A'),
(661, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 05:42:38', 'sid', 'N/A', 'N/A'),
(662, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 06:15:00', 'sid', 'N/A', 'N/A'),
(663, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 06:19:20', 'sid', 'N/A', 'N/A'),
(664, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 06:27:03', 'sid', 'N/A', 'N/A'),
(665, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 06:27:19', 'sid', 'N/A', 'N/A'),
(666, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 06:29:01', 'sid', 'N/A', 'N/A'),
(667, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 06:30:02', 'sid', 'N/A', 'N/A'),
(668, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong userid which is test', '2020-05-06 06:58:20', 'test', 'N/A', 'N/A'),
(669, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 06:58:36', 'sid', 'N/A', 'N/A'),
(670, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 06:59:06', 'sid', 'N/A', 'N/A'),
(671, 'TAA-78', NULL, NULL, NULL, '2020-05-06 06:59:07', NULL, NULL, NULL),
(672, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 06:59:59', 'sid', 'N/A', 'N/A'),
(673, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 07:02:39', 'sid', 'N/A', 'N/A'),
(674, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 07:03:03', 'sid', 'N/A', 'N/A'),
(675, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 07:06:42', 'sid', 'N/A', 'N/A'),
(676, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 07:16:35', 'sid', 'N/A', 'N/A'),
(677, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 07:17:47', 'abdul', 'N/A', 'N/A'),
(678, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 07:19:06', 'abdul', 'N/A', 'N/A'),
(679, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:05:35', 'sid', 'N/A', 'N/A'),
(680, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 08:05:37', 'sid', 'N/A', 'N/A'),
(681, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:12:11', 'sid', 'N/A', 'N/A'),
(682, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 08:12:11', 'sid', 'N/A', 'N/A'),
(683, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:13:58', 'sid', 'N/A', 'N/A'),
(684, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:14:09', 'abdul', 'N/A', 'N/A'),
(685, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:21:57', 'sid', 'N/A', 'N/A'),
(686, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 08:21:57', 'sid', 'N/A', 'N/A'),
(687, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:22:34', 'sid', 'N/A', 'N/A'),
(688, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:22:45', 'abdul', 'N/A', 'N/A'),
(689, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:24:40', 'abdul', 'N/A', 'N/A'),
(690, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 08:24:40', 'abdul ', 'N/A', 'N/A'),
(691, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:25:44', 'sid', 'N/A', 'N/A'),
(692, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong userid which is test', '2020-05-06 08:26:00', 'test', 'N/A', 'N/A'),
(693, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:26:10', 'abdul', 'N/A', 'N/A'),
(694, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:27:24', 'sid', 'N/A', 'N/A'),
(695, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type C', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 08:27:25', 'sid', 'N/A', 'N/A'),
(696, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:27:51', 'sid', 'N/A', 'N/A'),
(697, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:28:10', 'abdul', 'N/A', 'N/A'),
(698, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:33:42', 'sid', 'N/A', 'N/A'),
(699, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 08:33:42', 'sid', 'N/A', 'N/A'),
(700, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:34:28', 'sid', 'N/A', 'N/A'),
(701, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:34:40', 'abdul', 'N/A', 'N/A'),
(702, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:35:19', 'sid', 'N/A', 'N/A'),
(703, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 08:35:20', 'sid', 'N/A', 'N/A'),
(704, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:36:13', 'sid', 'N/A', 'N/A'),
(705, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:36:25', 'abdul', 'N/A', 'N/A'),
(706, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:37:06', 'sid', 'N/A', 'N/A'),
(707, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B/Type B', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 08:37:08', 'sid', 'N/A', 'N/A'),
(708, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:37:42', 'sid', 'N/A', 'N/A'),
(709, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:37:53', 'abdul', 'N/A', 'N/A'),
(710, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:38:44', 'sid', 'N/A', 'N/A'),
(711, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 08:38:45', 'sid', 'N/A', 'N/A'),
(712, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:41:25', 'sid', 'N/A', 'N/A'),
(713, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:41:35', 'abdul', 'N/A', 'N/A'),
(714, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:42:46', 'sid', 'N/A', 'N/A'),
(715, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 08:42:46', 'sid', 'N/A', 'N/A'),
(716, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:45:15', 'sid', 'N/A', 'N/A'),
(717, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:45:28', 'abdul', 'N/A', 'N/A'),
(718, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:46:24', 'sid', 'N/A', 'N/A'),
(719, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type C', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 08:46:24', 'sid', 'N/A', 'N/A'),
(720, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:49:51', 'sid', 'N/A', 'N/A'),
(721, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:50:04', 'abdul', 'N/A', 'N/A'),
(722, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:55:36', 'sid', 'N/A', 'N/A'),
(723, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 08:55:36', 'sid', 'N/A', 'N/A'),
(724, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:58:06', 'sid', 'N/A', 'N/A'),
(725, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 08:58:25', 'abdul', 'N/A', 'N/A'),
(726, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:01:20', 'sid', 'N/A', 'N/A'),
(727, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 09:01:20', 'sid', 'N/A', 'N/A'),
(728, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:07:42', 'sid', 'N/A', 'N/A'),
(729, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:08:50', 'sid', 'N/A', 'N/A'),
(730, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:09:46', 'sid', 'N/A', 'N/A'),
(731, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:10:47', 'abdul', 'N/A', 'N/A'),
(732, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:12:04', 'sid', 'N/A', 'N/A'),
(733, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 09:12:04', 'sid', 'N/A', 'N/A'),
(734, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:14:48', 'sid', 'N/A', 'N/A'),
(735, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:15:04', 'abdul', 'N/A', 'N/A'),
(736, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:18:17', 'sid', 'N/A', 'N/A'),
(737, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type C', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 09:18:18', 'sid', 'N/A', 'N/A'),
(738, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:20:38', 'sid', 'N/A', 'N/A'),
(739, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:20:58', 'abdul', 'N/A', 'N/A'),
(740, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:21:39', 'sid', 'N/A', 'N/A'),
(741, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type C', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 09:21:39', 'sid', 'N/A', 'N/A'),
(742, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:25:27', 'sid', 'N/A', 'N/A'),
(743, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:25:46', 'abdul', 'N/A', 'N/A'),
(744, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:26:41', 'sid', 'N/A', 'N/A'),
(745, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 09:26:42', 'sid', 'N/A', 'N/A'),
(746, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:28:02', 'sid', 'N/A', 'N/A'),
(747, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:28:21', 'abdul', 'N/A', 'N/A'),
(748, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:28:49', 'sid', 'N/A', 'N/A'),
(749, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 09:28:50', 'sid', 'N/A', 'N/A'),
(750, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:36:51', 'sid', 'N/A', 'N/A'),
(751, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:37:03', 'abdul', 'N/A', 'N/A'),
(752, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:48:48', 'sid', 'N/A', 'N/A'),
(753, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 09:48:48', 'sid', 'N/A', 'N/A'),
(754, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:54:56', 'sid', 'N/A', 'N/A'),
(755, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:55:08', 'abdul', 'N/A', 'N/A'),
(756, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 09:55:44', 'sid', 'N/A', 'N/A'),
(757, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 09:55:45', 'sid', 'N/A', 'N/A'),
(758, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:01:04', 'sid', 'N/A', 'N/A'),
(759, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:01:39', 'abdul', 'N/A', 'N/A'),
(760, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:10:36', 'sid', 'N/A', 'N/A'),
(761, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type C', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 10:10:37', 'sid', 'N/A', 'N/A'),
(762, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:13:11', 'sid', 'N/A', 'N/A'),
(763, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:13:24', 'abdul', 'N/A', 'N/A'),
(764, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:14:31', 'sid', 'N/A', 'N/A'),
(765, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 10:14:32', 'sid', 'N/A', 'N/A'),
(766, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:31:01', 'sid', 'N/A', 'N/A'),
(767, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:31:13', 'abdul', 'N/A', 'N/A'),
(768, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:31:34', 'sid', 'N/A', 'N/A'),
(769, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type C', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 10:31:34', 'sid', 'N/A', 'N/A'),
(770, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:33:59', 'sid', 'N/A', 'N/A'),
(771, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:34:10', 'abdul', 'N/A', 'N/A'),
(772, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:34:58', 'sid', 'N/A', 'N/A'),
(773, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type C', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 10:34:58', 'sid', 'N/A', 'N/A'),
(774, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:36:50', 'sid', 'N/A', 'N/A'),
(775, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:37:02', 'abdul', 'N/A', 'N/A'),
(776, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:37:23', 'sid', 'N/A', 'N/A'),
(777, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 10:37:24', 'sid', 'N/A', 'N/A'),
(778, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:38:40', 'sid', 'N/A', 'N/A'),
(779, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:38:51', 'abdul', 'N/A', 'N/A'),
(780, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:39:15', 'sid', 'N/A', 'N/A'),
(781, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 10:39:15', 'sid', 'N/A', 'N/A'),
(782, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:40:31', 'sid', 'N/A', 'N/A'),
(783, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:40:40', 'abdul', 'N/A', 'N/A'),
(784, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 10:41:02', 'sid', 'N/A', 'N/A'),
(785, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type B', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-06 10:41:03', 'sid', 'N/A', 'N/A'),
(786, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 11:02:50', 'sid', 'N/A', 'N/A'),
(787, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 11:24:46', 'sid', 'N/A', 'N/A'),
(788, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 11:26:25', 'sid', 'N/A', 'N/A'),
(789, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 11:26:50', 'sid', 'N/A', 'N/A'),
(790, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 11:46:44', 'sid', 'N/A', 'N/A'),
(791, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 11:46:53', 'sid', 'N/A', 'N/A'),
(792, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 11:49:20', 'sid', 'N/A', 'N/A'),
(793, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 11:49:33', 'abdul', 'N/A', 'N/A'),
(794, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 11:50:11', 'sid', 'N/A', 'N/A'),
(795, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 11:50:56', 'sid', 'N/A', 'N/A'),
(796, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 11:51:21', 'sid', 'N/A', 'N/A'),
(797, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 11:53:34', 'sid', 'N/A', 'N/A'),
(798, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 11:59:27', 'sid', 'N/A', 'N/A'),
(799, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:01:06', 'sid', 'N/A', 'N/A'),
(800, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:03:23', 'sid', 'N/A', 'N/A'),
(801, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:05:25', 'sid', 'N/A', 'N/A'),
(802, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:06:48', 'sid', 'N/A', 'N/A'),
(803, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:08:25', 'sid', 'N/A', 'N/A'),
(804, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:08:47', 'sid', 'N/A', 'N/A'),
(805, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:13:38', 'sid', 'N/A', 'N/A'),
(806, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:16:24', 'sid', 'N/A', 'N/A'),
(807, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:16:45', 'sid', 'N/A', 'N/A'),
(808, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:18:53', 'sid', 'N/A', 'N/A'),
(809, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:31:12', 'sid', 'N/A', 'N/A'),
(810, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:31:37', 'sid', 'N/A', 'N/A'),
(811, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:33:07', 'sid', 'N/A', 'N/A'),
(812, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:36:39', 'sid', 'N/A', 'N/A'),
(813, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:36:56', 'sid', 'N/A', 'N/A'),
(814, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:50:53', 'sid', 'N/A', 'N/A'),
(815, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:51:20', 'sid', 'N/A', 'N/A'),
(816, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:51:27', 'sid', 'N/A', 'N/A'),
(817, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:51:39', 'sid', 'N/A', 'N/A'),
(818, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-06 12:56:01', 'sid', 'N/A', 'N/A'),
(819, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:01:00', 'abdul', 'N/A', 'N/A'),
(820, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:02:35', 'abdul', 'N/A', 'N/A'),
(821, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:04:56', 'abdul', 'N/A', 'N/A'),
(822, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:05:25', 'abdul', 'N/A', 'N/A'),
(823, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:07:57', 'abdul', 'N/A', 'N/A'),
(824, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:08:48', 'abdul', 'N/A', 'N/A'),
(825, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:11:12', 'abdul', 'N/A', 'N/A'),
(826, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:17:58', 'abdul', 'N/A', 'N/A'),
(827, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:19:42', 'abdul', 'N/A', 'N/A'),
(828, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:32:28', 'abdul', 'N/A', 'N/A'),
(829, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:33:28', 'abdul', 'N/A', 'N/A'),
(830, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:34:26', 'abdul', 'N/A', 'N/A'),
(831, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:35:34', 'abdul', 'N/A', 'N/A'),
(832, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:37:50', 'abdul', 'N/A', 'N/A'),
(833, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:38:08', 'abdul', 'N/A', 'N/A'),
(834, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:52:12', 'abdul', 'N/A', 'N/A'),
(835, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 05:54:07', 'abdul', 'N/A', 'N/A'),
(836, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:06:03', 'abdul', 'N/A', 'N/A'),
(837, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:06:39', 'abdul', 'N/A', 'N/A'),
(838, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:11:39', 'abdul', 'N/A', 'N/A'),
(839, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:13:30', 'abdul', 'N/A', 'N/A'),
(840, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:22:37', 'abdul', 'N/A', 'N/A'),
(841, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:23:20', 'abdul', 'N/A', 'N/A'),
(842, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:26:15', 'abdul', 'N/A', 'N/A'),
(843, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:26:31', 'abdul', 'N/A', 'N/A'),
(844, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:29:22', 'abdul', 'N/A', 'N/A'),
(845, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:29:43', 'abdul', 'N/A', 'N/A'),
(846, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:30:53', 'abdul', 'N/A', 'N/A'),
(847, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:31:25', 'abdul', 'N/A', 'N/A'),
(848, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:32:32', 'abdul', 'N/A', 'N/A'),
(849, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:35:44', 'abdul', 'N/A', 'N/A'),
(850, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:37:22', 'abdul', 'N/A', 'N/A'),
(851, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:38:41', 'abdul', 'N/A', 'N/A'),
(852, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:40:24', 'abdul', 'N/A', 'N/A'),
(853, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Start fail because already activity in progress', '2020-05-07 06:40:25', 'abdul ', 'N/A', 'N/A'),
(854, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:40:51', 'abdul', 'N/A', 'N/A'),
(855, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:41:51', 'sid', 'N/A', 'N/A'),
(856, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-07 06:41:51', 'sid', 'N/A', 'N/A'),
(857, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:45:36', 'abdul', 'N/A', 'N/A'),
(858, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:51:52', 'abdul', 'N/A', 'N/A'),
(859, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:52:19', 'abdul', 'N/A', 'N/A'),
(860, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:54:24', 'sid', 'N/A', 'N/A'),
(861, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:55:52', 'sid', 'N/A', 'N/A'),
(862, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-07 06:55:53', 'sid', 'N/A', 'N/A'),
(863, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:56:30', 'abdul', 'N/A', 'N/A'),
(864, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:56:52', 'sid', 'N/A', 'N/A'),
(865, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:58:53', 'sid', 'N/A', 'N/A'),
(866, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-07 06:58:53', 'sid', 'N/A', 'N/A'),
(867, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 06:59:35', 'abdul', 'N/A', 'N/A'),
(868, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 07:00:16', 'sid', 'N/A', 'N/A'),
(869, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 07:05:00', 'sid', 'N/A', 'N/A'),
(870, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-07 07:05:01', 'sid', 'N/A', 'N/A'),
(871, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 07:05:10', 'sid', 'N/A', 'N/A'),
(872, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 07:05:25', 'abdul', 'N/A', 'N/A'),
(873, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 07:06:16', 'sid', 'N/A', 'N/A'),
(874, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-07 07:06:18', 'sid', 'N/A', 'N/A'),
(875, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 07:06:36', 'abdul', 'N/A', 'N/A'),
(876, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 07:08:43', 'sid', 'N/A', 'N/A'),
(877, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 07:11:33', 'sid', 'N/A', 'N/A'),
(878, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 07:13:54', 'sid', 'N/A', 'N/A'),
(879, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 07:14:37', 'sid', 'N/A', 'N/A'),
(880, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 07:15:03', 'abdul', 'N/A', 'N/A'),
(881, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 07:23:30', 'sid', 'N/A', 'N/A'),
(882, 'TAA-78', 'process log id = 51 Activity = 2 name is Type B Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-07 07:23:31', 'sid', 'N/A', 'N/A'),
(883, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 08:01:48', 'abdul', 'N/A', 'N/A'),
(884, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 08:03:53', 'abdul', 'N/A', 'N/A'),
(885, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 08:06:27', 'abdul', 'N/A', 'N/A'),
(886, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 08:09:39', 'abdul', 'N/A', 'N/A'),
(887, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 08:10:16', 'abdul', 'N/A', 'N/A'),
(888, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 08:11:16', 'abdul', 'N/A', 'N/A'),
(889, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 08:40:38', 'sid', 'N/A', 'N/A'),
(890, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 08:41:07', 'abdul', 'N/A', 'N/A'),
(891, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 08:45:45', 'sid', 'N/A', 'N/A'),
(892, 'TAA-78', 'process log id = 64 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-07 08:45:46', 'sid', 'N/A', 'N/A'),
(893, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 09:06:41', 'abdul', 'N/A', 'N/A'),
(894, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 09:07:23', 'abdul', 'N/A', 'N/A'),
(895, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 09:08:26', 'abdul', 'N/A', 'N/A'),
(896, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 09:19:17', 'abdul', 'N/A', 'N/A'),
(897, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 09:19:47', 'abdul', 'N/A', 'N/A'),
(898, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 09:20:25', 'abdul', 'N/A', 'N/A'),
(899, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 09:41:21', 'abdul', 'N/A', 'N/A'),
(900, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 09:41:33', 'abdul', 'N/A', 'N/A');
INSERT INTO `pts_audit_log` (`id`, `terminalID`, `processname_master`, `activity_sub_task_actions`, `action_performed`, `created_on`, `created_by`, `oldval`, `newval`) VALUES
(901, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 09:42:20', 'sid', 'N/A', 'N/A'),
(902, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 10:14:08', 'abdul', 'N/A', 'N/A'),
(903, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 10:17:08', 'abdul', 'N/A', 'N/A'),
(904, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 10:17:24', 'abdul', 'N/A', 'N/A'),
(905, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 10:19:16', 'abdul', 'N/A', 'N/A'),
(906, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 10:19:38', 'abdul', 'N/A', 'N/A'),
(907, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 10:23:12', 'abdul', 'N/A', 'N/A'),
(908, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 10:23:33', 'abdul', 'N/A', 'N/A'),
(909, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 10:24:47', 'abdul', 'N/A', 'N/A'),
(910, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 10:25:21', 'abdul', 'N/A', 'N/A'),
(911, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 10:25:44', 'abdul', 'N/A', 'N/A'),
(912, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-07 10:26:15', 'abdul', 'N/A', 'N/A'),
(913, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 02:42:21', 'abdul', 'N/A', 'N/A'),
(914, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 02:46:10', 'sid', 'N/A', 'N/A'),
(915, 'TAA-78', 'process log id = 64 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-08 02:46:11', 'sid', 'N/A', 'N/A'),
(916, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 02:46:25', 'sid', 'N/A', 'N/A'),
(917, 'TAA-78', 'process log id = 64 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-08 02:46:26', 'sid', 'N/A', 'N/A'),
(918, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 02:49:47', 'sid', 'N/A', 'N/A'),
(919, 'TAA-78', 'process log id = 64 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-08 02:49:48', 'sid', 'N/A', 'N/A'),
(920, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 02:51:06', 'sid', 'N/A', 'N/A'),
(921, 'TAA-78', 'process log id = 64 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-08 02:51:06', 'sid', 'N/A', 'N/A'),
(922, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 02:54:57', 'sid', 'N/A', 'N/A'),
(923, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :PL001 and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-08 02:54:58', 'sid', 'N/A', 'N/A'),
(924, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:04:18', 'sid', 'N/A', 'N/A'),
(925, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :PL002 and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-08 03:04:18', 'sid', 'N/A', 'N/A'),
(926, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:04:32', 'sid', 'N/A', 'N/A'),
(927, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:04:47', 'abdul', 'N/A', 'N/A'),
(928, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:05:25', 'abdul', 'N/A', 'N/A'),
(929, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:06:58', 'abdul', 'N/A', 'N/A'),
(930, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:07:30', 'abdul', 'N/A', 'N/A'),
(931, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:10:58', 'abdul', 'N/A', 'N/A'),
(932, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:13:03', 'abdul', 'N/A', 'N/A'),
(933, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:13:34', 'abdul', 'N/A', 'N/A'),
(934, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:18:05', 'abdul', 'N/A', 'N/A'),
(935, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:18:51', 'abdul', 'N/A', 'N/A'),
(936, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:20:22', 'abdul', 'N/A', 'N/A'),
(937, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:20:37', 'abdul', 'N/A', 'N/A'),
(938, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:23:19', 'abdul', 'N/A', 'N/A'),
(939, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:23:31', 'abdul', 'N/A', 'N/A'),
(940, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:23:41', 'abdul', 'N/A', 'N/A'),
(941, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:25:16', 'abdul', 'N/A', 'N/A'),
(942, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:26:30', 'abdul', 'N/A', 'N/A'),
(943, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:39:24', 'abdul', 'N/A', 'N/A'),
(944, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:39:42', 'abdul', 'N/A', 'N/A'),
(945, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:48:35', 'abdul', 'N/A', 'N/A'),
(946, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:48:56', 'abdul', 'N/A', 'N/A'),
(947, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:50:33', 'abdul', 'N/A', 'N/A'),
(948, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:51:19', 'abdul', 'N/A', 'N/A'),
(949, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:53:46', 'abdul', 'N/A', 'N/A'),
(950, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:55:42', 'abdul', 'N/A', 'N/A'),
(951, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 03:57:57', 'abdul', 'N/A', 'N/A'),
(952, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:01:28', 'abdul', 'N/A', 'N/A'),
(953, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:01:46', 'abdul', 'N/A', 'N/A'),
(954, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:02:40', 'abdul', 'N/A', 'N/A'),
(955, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:02:54', 'abdul', 'N/A', 'N/A'),
(956, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:05:00', 'abdul', 'N/A', 'N/A'),
(957, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:05:11', 'abdul', 'N/A', 'N/A'),
(958, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:05:49', 'sid', 'N/A', 'N/A'),
(959, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:06:04', 'abdul', 'N/A', 'N/A'),
(960, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:06:56', 'sid', 'N/A', 'N/A'),
(961, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :PL002 and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-08 04:06:57', 'sid', 'N/A', 'N/A'),
(962, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:07:04', 'sid', 'N/A', 'N/A'),
(963, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :PL002 and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-08 04:07:04', 'sid', 'N/A', 'N/A'),
(964, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:11:25', 'sid', 'N/A', 'N/A'),
(965, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :PL001 and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-08 04:11:26', 'sid', 'N/A', 'N/A'),
(966, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:14:26', 'sid', 'N/A', 'N/A'),
(967, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :PL001 and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-08 04:14:27', 'sid', 'N/A', 'N/A'),
(968, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:14:44', 'abdul', 'N/A', 'N/A'),
(969, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:15:23', 'abdul', 'N/A', 'N/A'),
(970, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:16:34', 'abdul', 'N/A', 'N/A'),
(971, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:16:48', 'abdul', 'N/A', 'N/A'),
(972, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:26:50', 'abdul', 'N/A', 'N/A'),
(973, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:27:55', 'abdul', 'N/A', 'N/A'),
(974, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 04:28:15', 'abdul', 'N/A', 'N/A'),
(975, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 11:59:05', 'sid', 'N/A', 'N/A'),
(976, 'TAA-78', 'process log id = 64 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-08 11:59:06', 'sid', 'N/A', 'N/A'),
(977, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 12:05:59', 'abdul', 'N/A', 'N/A'),
(978, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 12:06:33', 'sid', 'N/A', 'N/A'),
(979, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 12:06:48', 'abdul', 'N/A', 'N/A'),
(980, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 12:09:22', 'sid', 'N/A', 'N/A'),
(981, 'TAA-78', 'process log id = 64 Activity = 1 name is Type A Cleaning', 'where as product is :PL002 and batch no is: MNG235', 'Activity Successfully Started.', '2020-05-08 12:09:23', 'sid', 'N/A', 'N/A'),
(982, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 15:11:13', 'sid', 'N/A', 'N/A'),
(983, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 15:11:44', 'sid', 'N/A', 'N/A'),
(984, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-08 15:11:58', 'sid', 'N/A', 'N/A'),
(985, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-09 07:41:39', 'sid', 'N/A', 'N/A'),
(986, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-09 07:42:51', 'sid', 'N/A', 'N/A'),
(987, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-09 07:43:10', 'abdul', 'N/A', 'N/A'),
(988, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-11 05:04:23', 'sid', 'N/A', 'N/A'),
(989, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-11 05:04:43', 'abdul', 'N/A', 'N/A'),
(990, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-15 05:52:53', 'sid', 'N/A', 'N/A'),
(991, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-15 05:53:20', 'sid', 'N/A', 'N/A'),
(992, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-15 05:53:21', 'sid', 'N/A', 'N/A'),
(993, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-15 05:53:57', 'sid', 'N/A', 'N/A'),
(994, 'TAA-78', 'process log id = 51 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-15 05:53:57', 'sid', 'N/A', 'N/A'),
(995, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-15 05:54:34', 'abdul', 'N/A', 'N/A'),
(996, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-15 05:54:47', 'sid', 'N/A', 'N/A'),
(997, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-15 05:55:04', 'abdul', 'N/A', 'N/A'),
(998, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-15 05:55:22', 'sid', 'N/A', 'N/A'),
(999, 'TAA-78', 'process log id = 51 Activity = 2 name is Type B Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-15 05:55:22', 'sid', 'N/A', 'N/A'),
(1000, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 04:08:08', 'abdul', 'N/A', 'N/A'),
(1001, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 04:08:53', 'sid', 'N/A', 'N/A'),
(1002, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 04:09:08', 'abdul', 'N/A', 'N/A'),
(1003, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 04:09:31', 'QA', 'N/A', 'N/A'),
(1004, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 04:09:37', 'QA', 'N/A', 'N/A'),
(1005, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 04:11:34', 'QA', 'N/A', 'N/A'),
(1006, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 04:12:05', 'abdul', 'N/A', 'N/A'),
(1007, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 04:12:24', 'sid', 'N/A', 'N/A'),
(1008, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 04:12:36', 'abdul', 'N/A', 'N/A'),
(1009, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 05:09:28', 'abdul', 'N/A', 'N/A'),
(1010, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 05:32:31', 'sid', 'N/A', 'N/A'),
(1011, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-18 05:32:32', 'sid', 'N/A', 'N/A'),
(1012, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 05:32:57', 'abdul', 'N/A', 'N/A'),
(1013, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 05:41:39', 'abdul', 'N/A', 'N/A'),
(1014, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 05:41:51', 'sid', 'N/A', 'N/A'),
(1015, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 05:42:05', 'abdul', 'N/A', 'N/A'),
(1016, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 05:42:23', 'QA', 'N/A', 'N/A'),
(1017, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:09:11', 'sid', 'N/A', 'N/A'),
(1018, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-18 11:09:11', 'sid', 'N/A', 'N/A'),
(1019, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:15:48', 'abdul', 'N/A', 'N/A'),
(1020, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:15:57', 'sid', 'N/A', 'N/A'),
(1021, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:16:09', 'abdul', 'N/A', 'N/A'),
(1022, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:22:59', 'sid', 'N/A', 'N/A'),
(1023, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011001A and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-18 11:23:00', 'sid', 'N/A', 'N/A'),
(1024, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:37:13', 'sid', 'N/A', 'N/A'),
(1025, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:38:32', 'sid', 'N/A', 'N/A'),
(1026, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:38:41', 'abdul', 'N/A', 'N/A'),
(1027, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:38:51', 'abdul', 'N/A', 'N/A'),
(1028, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:40:49', 'abdul', 'N/A', 'N/A'),
(1029, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:42:25', 'abdul', 'N/A', 'N/A'),
(1030, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:45:26', 'abdul', 'N/A', 'N/A'),
(1031, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:49:36', 'sid', 'N/A', 'N/A'),
(1032, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:50:59', 'sid', 'N/A', 'N/A'),
(1033, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:53:58', 'sid', 'N/A', 'N/A'),
(1034, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 11:55:00', 'sid', 'N/A', 'N/A'),
(1035, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:05:39', 'sid', 'N/A', 'N/A'),
(1036, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:05:50', 'abdul', 'N/A', 'N/A'),
(1037, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:06:31', 'sid', 'N/A', 'N/A'),
(1038, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:06:50', 'sid', 'N/A', 'N/A'),
(1039, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:10:30', 'sid', 'N/A', 'N/A'),
(1040, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:14:24', 'sid', 'N/A', 'N/A'),
(1041, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:17:06', 'sid', 'N/A', 'N/A'),
(1042, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:19:35', 'sid', 'N/A', 'N/A'),
(1043, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:19:55', 'abdul', 'N/A', 'N/A'),
(1044, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:22:34', 'sid', 'N/A', 'N/A'),
(1045, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:23:41', 'sid', 'N/A', 'N/A'),
(1046, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:24:26', 'abdul', 'N/A', 'N/A'),
(1047, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:25:13', 'sid', 'N/A', 'N/A'),
(1048, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:27:33', 'sid', 'N/A', 'N/A'),
(1049, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:28:37', 'sid', 'N/A', 'N/A'),
(1050, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:31:11', 'sid', 'N/A', 'N/A'),
(1051, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 12:31:20', 'abdul', 'N/A', 'N/A'),
(1052, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-18 13:08:04', 'sid', 'N/A', 'N/A'),
(1053, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 04:47:01', 'sid', 'N/A', 'N/A'),
(1054, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 04:47:22', 'abdul', 'N/A', 'N/A'),
(1055, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 04:47:44', 'abdul', 'N/A', 'N/A'),
(1056, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 04:48:01', 'abdul', 'N/A', 'N/A'),
(1057, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 04:49:43', 'abdul', 'N/A', 'N/A'),
(1058, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 04:51:52', 'abdul', 'N/A', 'N/A'),
(1059, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 05:03:45', 'abdul', 'N/A', 'N/A'),
(1060, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 05:09:53', 'sid', 'N/A', 'N/A'),
(1061, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 05:11:11', 'sid', 'N/A', 'N/A'),
(1062, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 05:11:21', 'abdul', 'N/A', 'N/A'),
(1063, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 05:11:44', 'sid', 'N/A', 'N/A'),
(1064, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 05:18:34', 'sid', 'N/A', 'N/A'),
(1065, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 05:18:47', 'abdul', 'N/A', 'N/A'),
(1066, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 05:21:09', 'abdul', 'N/A', 'N/A'),
(1067, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 05:21:58', 'abdul', 'N/A', 'N/A'),
(1068, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 05:23:32', 'abdul', 'N/A', 'N/A'),
(1069, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 05:23:44', 'QA', 'N/A', 'N/A'),
(1070, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 08:49:19', 'sid', 'N/A', 'N/A'),
(1071, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:21:08', 'QA', 'N/A', 'N/A'),
(1072, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:22:37', 'sid', 'N/A', 'N/A'),
(1073, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:23:04', 'sid', 'N/A', 'N/A'),
(1074, 'TAA-78', 'process log id = 50 Activity = 6 name is Production', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Start fail because already activity in progress', '2020-05-19 09:23:04', 'sid', 'N/A', 'N/A'),
(1075, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:26:03', 'sid', 'N/A', 'N/A'),
(1076, 'TAA-78', 'process log id = 50 Activity = 6 name is Production', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Start fail because already activity in progress', '2020-05-19 09:26:04', 'sid', 'N/A', 'N/A'),
(1077, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:26:57', 'sid', 'N/A', 'N/A'),
(1078, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:28:39', 'sid', 'N/A', 'N/A'),
(1079, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:29:45', 'sid', 'N/A', 'N/A'),
(1080, 'TAA-78', 'process log id = 50 Activity = 6 name is Production', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-19 09:29:46', 'sid', 'N/A', 'N/A'),
(1081, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:30:58', 'sid', 'N/A', 'N/A'),
(1082, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:32:59', 'sid', 'N/A', 'N/A'),
(1083, 'TAA-78', 'process log id = 50 Activity = 4 name is Type D Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Start fail because already activity in progress', '2020-05-19 09:32:59', 'sid', 'N/A', 'N/A'),
(1084, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:36:29', 'sid', 'N/A', 'N/A'),
(1085, 'TAA-78', 'process log id = 50 Activity = 4 name is Type D Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Start fail because already activity in progress', '2020-05-19 09:36:30', 'sid', 'N/A', 'N/A'),
(1086, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:39:38', 'sid', 'N/A', 'N/A'),
(1087, 'TAA-78', 'process log id = 50 Activity = 4 name is Type D Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Start fail because already activity in progress', '2020-05-19 09:39:38', 'sid', 'N/A', 'N/A'),
(1088, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:40:47', 'sid', 'N/A', 'N/A'),
(1089, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:41:56', 'abdul', 'N/A', 'N/A'),
(1090, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:47:51', 'sid', 'N/A', 'N/A'),
(1091, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:48:54', 'sid', 'N/A', 'N/A'),
(1092, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:49:45', 'sid', 'N/A', 'N/A'),
(1093, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:50:59', 'sid', 'N/A', 'N/A'),
(1094, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:51:51', 'sid', 'N/A', 'N/A'),
(1095, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 09:53:38', 'sid', 'N/A', 'N/A'),
(1096, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 10:11:34', 'sid', 'N/A', 'N/A'),
(1097, 'TAA-78', 'process log id = 50 Activity = 6 name is Production', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Start fail because already activity in progress', '2020-05-19 10:11:34', 'sid', 'N/A', 'N/A'),
(1098, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 10:12:16', 'sid', 'N/A', 'N/A'),
(1099, 'TAA-78', 'process log id = 50 Activity = 6 name is Production', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-19 10:12:17', 'sid', 'N/A', 'N/A'),
(1100, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 10:14:00', 'abdul', 'N/A', 'N/A'),
(1101, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 10:14:19', 'abdul', 'N/A', 'N/A'),
(1102, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 10:14:33', 'sid', 'N/A', 'N/A'),
(1103, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 10:14:48', 'abdul', 'N/A', 'N/A'),
(1104, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 10:30:18', 'sid', 'N/A', 'N/A'),
(1105, 'TAA-78', 'process log id = 51 Activity = 6 name is Production', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-19 10:30:19', 'sid', 'N/A', 'N/A'),
(1106, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 10:43:09', 'abdul', 'N/A', 'N/A'),
(1107, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 10:45:39', 'sid', 'N/A', 'N/A'),
(1108, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-19 10:46:14', 'sid', 'N/A', 'N/A'),
(1109, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 07:11:17', 'sa', 'N/A', 'N/A'),
(1110, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 07:11:51', 'abdul', 'N/A', 'N/A'),
(1111, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 07:12:18', 'sid', 'N/A', 'N/A'),
(1112, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-20 07:12:18', 'sid', 'N/A', 'N/A'),
(1113, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 07:12:37', 'abdul', 'N/A', 'N/A'),
(1114, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 07:12:47', 'sid', 'N/A', 'N/A'),
(1115, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 07:12:58', 'abdul', 'N/A', 'N/A'),
(1116, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 08:53:10', 'sid', 'N/A', 'N/A'),
(1117, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-20 08:53:11', 'sid', 'N/A', 'N/A'),
(1118, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:23:59', 'abdul', 'N/A', 'N/A'),
(1119, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:24:17', 'sid', 'N/A', 'N/A'),
(1120, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:24:29', 'abdul', 'N/A', 'N/A'),
(1121, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:24:49', 'sid', 'N/A', 'N/A'),
(1122, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:25:15', 'sid', 'N/A', 'N/A'),
(1123, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Start fail because already activity in progress', '2020-05-20 11:25:15', 'sid', 'N/A', 'N/A'),
(1124, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:26:19', 'sid', 'N/A', 'N/A'),
(1125, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:27:17', 'sid', 'N/A', 'N/A'),
(1126, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-20 11:27:18', 'sid', 'N/A', 'N/A'),
(1127, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:27:58', 'sid', 'N/A', 'N/A'),
(1128, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:28:11', 'sid', 'N/A', 'N/A'),
(1129, 'TAA-78', 'process log id = 50 Activity = 4 name is Type D Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-20 11:28:12', 'sid', 'N/A', 'N/A'),
(1130, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:28:38', 'sid', 'N/A', 'N/A'),
(1131, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:29:54', 'sid', 'N/A', 'N/A'),
(1132, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:32:32', 'sid', 'N/A', 'N/A'),
(1133, '', 'process log id =  Activity =  name is ', 'where as product is : and batch no is: ', 'Activity Start fail because user was not room in', '2020-05-20 11:34:59', '', 'N/A', 'N/A'),
(1134, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:35:18', 'sid', 'N/A', 'N/A'),
(1135, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:36:11', 'sid', 'N/A', 'N/A'),
(1136, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:36:50', 'sid', 'N/A', 'N/A'),
(1137, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Start fail because already activity in progress', '2020-05-20 11:36:50', 'sid', 'N/A', 'N/A'),
(1138, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:38:08', 'sid', 'N/A', 'N/A'),
(1139, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:39:12', 'sid', 'N/A', 'N/A'),
(1140, 'TAA-78', 'process log id = 51 Activity = 2 name is Type B Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-20 11:39:12', 'sid', 'N/A', 'N/A'),
(1141, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:39:43', 'sid', 'N/A', 'N/A'),
(1142, 'TAA-78', 'process log id = 51 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-20 11:39:44', 'sid', 'N/A', 'N/A'),
(1143, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:41:44', 'sid', 'N/A', 'N/A'),
(1144, 'TAA-78', 'process log id = 51 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-20 11:41:45', 'sid', 'N/A', 'N/A'),
(1145, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:42:23', 'sid', 'N/A', 'N/A'),
(1146, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:43:00', 'sid', 'N/A', 'N/A'),
(1147, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:44:20', 'sid', 'N/A', 'N/A'),
(1148, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:44:47', 'sid', 'N/A', 'N/A'),
(1149, 'TAA-78', 'process log id = 51 Activity = 5 name is Maintenance', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Start fail because already activity in progress', '2020-05-20 11:44:47', 'sid', 'N/A', 'N/A'),
(1150, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:45:38', 'sid', 'N/A', 'N/A'),
(1151, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:46:02', 'sid', 'N/A', 'N/A'),
(1152, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Start fail because already activity in progress', '2020-05-20 11:46:03', 'sid', 'N/A', 'N/A'),
(1153, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:46:33', 'sid', 'N/A', 'N/A'),
(1154, 'TAA-78', 'process log id = 64 Activity = 6 name is Production', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Start fail because already activity in progress', '2020-05-20 11:46:33', 'sid', 'N/A', 'N/A'),
(1155, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:52:19', 'sid', 'N/A', 'N/A'),
(1156, 'TAA-78', 'process log id = 50 Activity = 2 name is Type B Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Start fail because already activity in progress', '2020-05-20 11:52:19', 'sid', 'N/A', 'N/A'),
(1157, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:52:32', 'sid', 'N/A', 'N/A'),
(1158, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-20 11:52:34', 'sid', 'N/A', 'N/A'),
(1159, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:53:26', 'sid', 'N/A', 'N/A'),
(1160, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-20 11:53:44', 'abdul', 'N/A', 'N/A'),
(1161, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-21 04:52:16', 'sid', 'N/A', 'N/A'),
(1162, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-21 04:52:20', 'sid', 'N/A', 'N/A'),
(1163, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-21 04:56:56', 'sid', 'N/A', 'N/A'),
(1164, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-21 07:30:38', 'sid', 'N/A', 'N/A'),
(1165, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-21 07:30:39', 'sid', 'N/A', 'N/A'),
(1166, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-21 07:30:56', 'abdul', 'N/A', 'N/A'),
(1167, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-21 07:31:12', 'sid', 'N/A', 'N/A'),
(1168, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-21 07:31:31', 'abdul', 'N/A', 'N/A'),
(1169, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-21 12:10:41', 'sid', 'N/A', 'N/A'),
(1170, 'TAA-78', 'process log id = 50 Activity = 6 name is Production', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-21 12:10:41', 'sid', 'N/A', 'N/A'),
(1171, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-21 12:11:39', 'abdul', 'N/A', 'N/A'),
(1172, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-21 12:12:03', 'sid', 'N/A', 'N/A'),
(1173, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-21 12:12:22', 'abdul', 'N/A', 'N/A'),
(1174, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 04:56:13', 'sid', 'N/A', 'N/A'),
(1175, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 04:56:40', 'sid', 'N/A', 'N/A'),
(1176, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-22 04:56:41', 'sid', 'N/A', 'N/A'),
(1177, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 04:58:09', 'sid', 'N/A', 'N/A'),
(1178, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 04:58:42', 'abdul', 'N/A', 'N/A'),
(1179, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 05:03:01', 'sid', 'N/A', 'N/A'),
(1180, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 05:03:43', 'abdul', 'N/A', 'N/A'),
(1181, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 05:03:55', 'QA', 'N/A', 'N/A'),
(1182, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong password which is bf9e8e7955e8c87d5fe571ad0be25008654157a58a7a34353b81d747e8c6577a', '2020-05-22 05:06:12', 'meera', 'N/A', 'N/A'),
(1183, 'N/A', 'Employee Master', 'emp_password', 'update', '2020-01-15 05:23:22', 'Superadmin', '5fd8b7d3584042a843bf341ae2dcca285a3924ea11b35e2d9f686f780ce122ee', 'bf9e8e7955e8c87d5fe571ad0be25008654157a58a7a34353b81d747e8c6577a'),
(1184, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 05:08:29', 'meera', 'N/A', 'N/A'),
(1185, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 05:09:30', 'meera', 'N/A', 'N/A'),
(1186, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 05:10:14', 'abdul', 'N/A', 'N/A'),
(1187, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 05:10:31', 'meera', 'N/A', 'N/A'),
(1188, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 05:12:55', 'abdul', 'N/A', 'N/A'),
(1189, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 05:13:16', 'meera', 'N/A', 'N/A'),
(1190, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 05:13:32', 'QA', 'N/A', 'N/A'),
(1191, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 05:15:57', 'meera', 'N/A', 'N/A'),
(1192, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 05:16:38', 'meera', 'N/A', 'N/A'),
(1193, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 06:06:30', 'sid', 'N/A', 'N/A'),
(1194, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 06:06:48', 'abdul', 'N/A', 'N/A'),
(1195, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 06:07:19', 'meera', 'N/A', 'N/A'),
(1196, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 06:07:38', 'QA', 'N/A', 'N/A'),
(1197, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 06:09:12', 'QA', 'N/A', 'N/A'),
(1198, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 06:09:41', 'abdul', 'N/A', 'N/A'),
(1199, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 06:09:50', 'meera', 'N/A', 'N/A'),
(1200, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 06:09:57', 'QA', 'N/A', 'N/A'),
(1201, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 06:10:13', 'sid', 'N/A', 'N/A'),
(1202, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 06:10:48', 'sid', 'N/A', 'N/A'),
(1203, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 06:13:21', 'sid', 'N/A', 'N/A'),
(1204, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 06:13:32', 'abdul', 'N/A', 'N/A'),
(1205, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 06:13:41', 'meera', 'N/A', 'N/A'),
(1206, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 06:13:49', 'QA', 'N/A', 'N/A'),
(1207, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 06:13:59', 'sid', 'N/A', 'N/A'),
(1208, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 10:30:26', 'QA', 'N/A', 'N/A'),
(1209, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 10:35:09', 'QA', 'N/A', 'N/A'),
(1210, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 10:35:55', 'QA', 'N/A', 'N/A'),
(1211, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 10:44:05', 'QA', 'N/A', 'N/A'),
(1212, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-22 10:44:25', 'QA', 'N/A', 'N/A'),
(1213, 'N/A', 'Employee Master', 'email2', 'update', '2020-01-15 05:36:41', 'Superadmin', 'rahul.chauhan@smhs.motherson.com', 'rahul.chauhan66@smhs.motherson.com'),
(1214, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 05:18:57', 'sid', 'N/A', 'N/A'),
(1215, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 05:19:52', 'sid', 'N/A', 'N/A'),
(1216, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 05:22:05', 'sid', 'N/A', 'N/A'),
(1217, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 05:27:31', 'sid', 'N/A', 'N/A'),
(1218, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 05:28:11', 'abdul', 'N/A', 'N/A'),
(1219, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 05:29:28', 'sid', 'N/A', 'N/A'),
(1220, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 05:33:17', 'sid', 'N/A', 'N/A'),
(1221, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 05:36:52', 'sid', 'N/A', 'N/A'),
(1222, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 05:44:12', 'sid', 'N/A', 'N/A'),
(1223, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 05:44:39', 'sid', 'N/A', 'N/A'),
(1224, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 06:14:04', 'sid', 'N/A', 'N/A'),
(1225, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 06:14:18', 'abdul', 'N/A', 'N/A'),
(1226, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 06:14:50', 'abdul', 'N/A', 'N/A'),
(1227, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 06:15:00', 'QA', 'N/A', 'N/A'),
(1228, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong password which is 2e7d82708f7e37a8c75ed3b624e3e97aa38e570ca9a1a2083261fa42972a3740', '2020-05-27 06:46:37', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1229, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong password which is 2e7d82708f7e37a8c75ed3b624e3e97aa38e570ca9a1a2083261fa42972a3740', '2020-05-27 07:01:46', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1230, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 07:42:51', 'sid', 'N/A', 'N/A'),
(1231, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 07:48:34', 'sid', 'N/A', 'N/A'),
(1232, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 07:48:42', 'abdul', 'N/A', 'N/A'),
(1233, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 07:48:55', 'QA', 'N/A', 'N/A'),
(1234, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 10:18:14', 'sid', 'N/A', 'N/A'),
(1235, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 10:18:37', 'sid', 'N/A', 'N/A'),
(1236, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 10:19:13', 'abdul', 'N/A', 'N/A'),
(1237, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 10:29:05', 'abdul', 'N/A', 'N/A'),
(1238, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 10:29:36', 'abdul', 'N/A', 'N/A'),
(1239, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 10:33:28', 'QA', 'N/A', 'N/A'),
(1240, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 10:34:22', 'QA', 'N/A', 'N/A'),
(1241, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 10:48:38', 'sid', 'N/A', 'N/A'),
(1242, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 10:48:56', 'sid', 'N/A', 'N/A'),
(1243, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 10:49:05', 'abdul', 'N/A', 'N/A'),
(1244, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 10:49:15', 'QA', 'N/A', 'N/A'),
(1245, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 11:17:56', 'sid', 'N/A', 'N/A'),
(1246, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 11:18:14', 'sid', 'N/A', 'N/A'),
(1247, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-27 11:18:29', 'abdul', 'N/A', 'N/A'),
(1248, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-28 08:30:59', 'sid', 'N/A', 'N/A'),
(1249, 'TAA-78', 'process log id = 51 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-28 08:31:00', 'sid', 'N/A', 'N/A'),
(1250, 'N/A', 'Employee Master', 'emp_email', 'update', '2020-01-15 05:23:22', 'Superadmin', 'sid', 'Rahul.Chauhan'),
(1251, 'N/A', 'Employee Master', 'emp_password', 'update', '2020-01-15 05:23:22', 'Superadmin', 'bf9e8e7955e8c87d5fe571ad0be25008654157a58a7a34353b81d747e8c6577a', '2e7d82708f7e37a8c75ed3b624e3e97aa38e570ca9a1a2083261fa42972a3740'),
(1252, 'N/A', 'Employee Master', 'emp_password', 'update', '2020-01-15 05:23:22', 'Superadmin', '2e7d82708f7e37a8c75ed3b624e3e97aa38e570ca9a1a2083261fa42972a3740', '3e3725729dd5bb9a7855ed2ae11602f24528f80d4904474ef26b4d7d5f146b4a'),
(1253, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-28 09:00:17', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1254, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-28 09:00:18', 'sid', 'N/A', 'N/A'),
(1255, 'N/A', 'Employee Master', 'emp_password', 'update', '2020-01-15 05:23:22', 'Superadmin', '3e3725729dd5bb9a7855ed2ae11602f24528f80d4904474ef26b4d7d5f146b4a', '2e7d82708f7e37a8c75ed3b624e3e97aa38e570ca9a1a2083261fa42972a3740'),
(1256, 'N/A', 'Employee Master', 'emp_password', 'update', '2020-01-15 05:23:22', 'Superadmin', '2e7d82708f7e37a8c75ed3b624e3e97aa38e570ca9a1a2083261fa42972a3740', '603cc387e6cc44e9633951cf9fbe4f5e23bdb8a12c1eba0ed405258f9d48623b'),
(1257, 'N/A', 'Employee Master', 'emp_password', 'update', '2020-01-15 05:23:22', 'Superadmin', '603cc387e6cc44e9633951cf9fbe4f5e23bdb8a12c1eba0ed405258f9d48623b', 'a48621bfd1db74b789c149b3f0f35317d31e63c41cca74f2f5d8abc6da606b4c'),
(1258, 'N/A', 'Employee Master', 'emp_password', 'update', '2020-01-15 05:23:22', 'Superadmin', 'a48621bfd1db74b789c149b3f0f35317d31e63c41cca74f2f5d8abc6da606b4c', '0f3b98124b3becbfcdeaf1855ca6b66eed6018772894f90dbf3c33500d59e451'),
(1259, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-28 09:51:45', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1260, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-05-28 09:51:45', 'sid', 'N/A', 'N/A'),
(1261, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong userid which is sid', '2020-05-28 10:22:39', 'sid', 'N/A', 'N/A'),
(1262, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong password which is 2e7d82708f7e37a8c75ed3b624e3e97aa38e570ca9a1a2083261fa42972a3740', '2020-05-28 10:23:17', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1263, 'N/A', 'Employee Master', 'emp_password', 'update', '2020-01-15 05:23:22', 'Superadmin', '0f3b98124b3becbfcdeaf1855ca6b66eed6018772894f90dbf3c33500d59e451', '2e7d82708f7e37a8c75ed3b624e3e97aa38e570ca9a1a2083261fa42972a3740'),
(1264, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-28 10:24:39', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1265, 'TAA-78', 'process log id = 59 Activity = 25 name is Vertical Sampler and Dies Cleaning Usages Log/Type A', 'where as product is :PL002 and batch no is: JKTU0002', 'Activity Successfully Started.', '2020-05-28 10:24:39', 'sid', 'N/A', 'N/A'),
(1266, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-28 10:26:49', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1267, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-28 10:30:47', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1268, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-28 10:35:52', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1269, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-28 10:40:26', 'abdul', 'N/A', 'N/A'),
(1270, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-28 10:42:12', 'abdul', 'N/A', 'N/A'),
(1271, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-28 10:50:21', 'abdul', 'N/A', 'N/A'),
(1272, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-28 11:02:07', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1273, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-28 11:02:39', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1274, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-28 11:03:05', 'abdul', 'N/A', 'N/A'),
(1275, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-28 11:03:34', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1276, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-28 11:03:42', 'abdul', 'N/A', 'N/A'),
(1277, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong userid which is sid', '2020-05-30 00:40:58', 'sid', 'N/A', 'N/A'),
(1278, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-30 00:41:10', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1279, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-30 00:41:35', 'abdul', 'N/A', 'N/A'),
(1280, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-30 00:54:45', 'abdul', 'N/A', 'N/A'),
(1281, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-30 00:55:01', 'QA', 'N/A', 'N/A'),
(1282, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-30 00:55:11', 'abdul', 'N/A', 'N/A'),
(1283, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-30 00:56:39', 'QA', 'N/A', 'N/A'),
(1284, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-30 00:57:38', 'QA', 'N/A', 'N/A'),
(1285, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-30 00:57:55', 'abdul', 'N/A', 'N/A'),
(1286, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-30 00:59:04', 'sa', 'N/A', 'N/A'),
(1287, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-30 01:00:34', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1288, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-30 01:00:43', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1289, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-30 01:00:53', 'abdul', 'N/A', 'N/A'),
(1290, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-05-30 01:01:36', 'abdul', 'N/A', 'N/A'),
(1291, 'N/A', 'User Login', 'N/A', 'Login Fail due to wrong userid which is sid', '2020-06-01 09:12:14', 'sid', 'N/A', 'N/A'),
(1292, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-01 09:12:24', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1293, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-01 09:12:47', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1294, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-01 09:12:56', 'abdul', 'N/A', 'N/A'),
(1295, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-01 11:09:13', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1296, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-01 11:11:11', 'abdul', 'N/A', 'N/A'),
(1297, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-01 11:11:23', 'abdul', 'N/A', 'N/A'),
(1298, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-01 11:12:32', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1299, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-01 11:27:31', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1300, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-01 13:31:30', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1301, 'TAA-78', 'process log id = 50 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-06-01 13:31:30', 'sid', 'N/A', 'N/A'),
(1302, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-01 13:31:43', 'abdul', 'N/A', 'N/A'),
(1303, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-03 08:18:43', 'abdul', 'N/A', 'N/A'),
(1304, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-03 08:35:14', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1305, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-03 08:35:54', 'abdul', 'N/A', 'N/A'),
(1306, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-03 12:12:10', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1307, 'TAA-78', 'process log id = 51 Activity = 1 name is Type A Cleaning', 'where as product is :0011001A and batch no is: JKT0001', 'Activity Successfully Started.', '2020-06-03 12:12:11', 'sid', 'N/A', 'N/A'),
(1308, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-03 12:12:31', 'abdul', 'N/A', 'N/A'),
(1309, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-03 12:12:46', 'QA', 'N/A', 'N/A'),
(1310, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-03 12:14:04', 'QA', 'N/A', 'N/A'),
(1311, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-03 12:16:38', 'QA', 'N/A', 'N/A'),
(1312, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-03 12:16:50', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1313, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-03 12:17:03', 'abdul', 'N/A', 'N/A'),
(1314, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-03 12:31:20', 'abdul', 'N/A', 'N/A'),
(1315, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-03 12:31:34', 'QA', 'N/A', 'N/A'),
(1316, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-03 12:31:45', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1317, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-03 12:31:56', 'abdul', 'N/A', 'N/A'),
(1318, 'N/A', 'Employee Master', 'is_active', 'update', '2020-01-15 05:23:22', 'Superadmin', '0', '0'),
(1319, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-08 12:19:19', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1320, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-08 12:19:45', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1321, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-08 12:21:20', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1322, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-08 12:21:30', 'abdul', 'N/A', 'N/A'),
(1323, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-09 08:21:34', 'Rahul.Chauhan', 'N/A', 'N/A'),
(1324, 'N/A', 'User Login', 'N/A', 'Login Success', '2020-06-09 08:22:16', 'abdul', 'N/A', 'N/A'),
(1325, 'N/A', 'Employee Master', 'Employee', 'Insert', '2020-06-09 09:28:33', 'Superadmin', 'N/A', 'N/A'),
(1326, 'N/A', 'Employee Master', 'Employee', 'Insert', '2020-06-09 09:31:22', 'Superadmin', 'N/A', 'N/A');

-- --------------------------------------------------------

--
-- Table structure for table `pts_elog_history`
--

DROP TABLE IF EXISTS `pts_elog_history`;
CREATE TABLE IF NOT EXISTS `pts_elog_history` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `created_by_user` varchar(50) NOT NULL,
  `action_text` varchar(255) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(80) NOT NULL,
  `type` varchar(50) NOT NULL,
  `table_unique_field1` varchar(100) DEFAULT NULL,
  `table_unique_field2` varchar(100) DEFAULT NULL,
  `table_field3` varchar(100) DEFAULT NULL,
  `command_type` enum('insert','before_update','update','delete') NOT NULL,
  `record_type` enum('master','log','report') NOT NULL DEFAULT 'master',
  `update_count` int(11) NOT NULL,
  `primary_id` int(11) DEFAULT NULL,
  `log_user_activity_id` int(11) DEFAULT '0',
  `table_name` varchar(60) NOT NULL,
  `table_extra_field` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_elog_history`
--

INSERT INTO `pts_elog_history` (`created_on`, `created_by`, `created_by_user`, `action_text`, `id`, `activity_name`, `type`, `table_unique_field1`, `table_unique_field2`, `table_field3`, `command_type`, `record_type`, `update_count`, `primary_id`, `log_user_activity_id`, `table_name`, `table_extra_field`) VALUES
('2020-05-08 06:29:06', 33, 'sid', 'Record created by sid', 1, 'Vertical Sampler and Dies Cleaning Usages Log', 'user_activity_log', 'doc_no:DOC2596', 'equipment_id:Line001', 'pre_barch_no:JKTU0002', 'insert', 'log', 1, 2, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2596\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"Line001\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKTU0002\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test start\",\"is_in_workflow\":\"no\"}'),
('2020-05-08 06:39:23', 33, 'sid', 'Record created by sid', 2, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2597', 'equipment_id:Line001', 'pre_barch_no:MNG235', 'insert', 'log', 1, 3, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2597\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"Line001\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"PL002\",\"pre_barch_no\":\"MNG235\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"type A start\",\"is_in_workflow\":\"no\"}'),
('2020-05-08 09:41:14', 33, 'sid', 'Record created by sid', 3, 'Instrument Log Register', 'instrument_log_register', 'doc_no:DOC2598', 'instrument_code:Instrument001', 'batch_no:JKTU0002', 'insert', 'log', 1, 4, 4, 'pts_trn_instrument_log_register', '{\"doc_no\":\"DOC2598\",\"room_code\":\"TAA-78\",\"product_code\":\"0011001A\",\"batch_no\":\"JKTU0002\",\"instrument_code\":\"Instrument001\",\"stage\":\"stage 2\",\"test\":\"test audit\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-08 10:28:07', 10, '', 'Record created by Super Admin', 4, 'Workflow', 'workflow', 'role_id:6', '', '', 'insert', 'master', 1, 85, 0, 'pts_trn_workflowsteps', '[{\"status_id\":\"2\",\"activity_id\":\"21\",\"workflowtype\":\"stop\",\"role_id\":\"1\",\"next_step\":\"3\",\"created_by\":\"Superadmin\"},{\"status_id\":\"3\",\"activity_id\":\"21\",\"workflowtype\":\"stop\",\"role_id\":\"2\",\"next_step\":\"-1\",\"created_by\":\"Superadmin\"}]'),
('2020-05-10 23:34:25', 33, 'sid', 'Record created by sid', 5, 'Swab Sample Record', 'swab_sample_record', 'doc_no:DOC2600', 'equipment_id:M-1006', 'pre_barch_no:JKTU0002', 'insert', 'log', 1, 4, 6, 'pts_trn_swab_sample_record', '{\"doc_no\":\"DOC2600\",\"room_code\":\"TAA-78\",\"ar_numer\":\"ARN001\",\"equipment_id\":\"M-1006\",\"equipment_desc\":\"S S Tank\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKTU0002\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test swab sample Report\",\"is_in_workflow\":\"no\"}'),
('2020-05-11 23:30:12', 10, '', 'Record created by Super Admin', 6, 'Report Footer Details', 'report_header', 'report_id:2', 'footer_name:OP001568-06/00/05-14', '', 'insert', 'master', 1, 1, 0, 'pts_mst_report_footer', '{\"report_id\":\"2\",\"footer_name\":\"OP001568-06\\/00\\/05-14\",\"created_on\":\"2020-05-12 05:00:12\",\"created_by\":\"10\"}'),
('2020-05-11 23:40:22', 10, '', 'Record deleted by Super Admin', 7, '', '', '', '', '', 'delete', 'master', 1, 1, 0, 'pts_mst_report_footer', '{\"id\":\"1\",\"status\":\"inactive\"}'),
('2020-05-11 23:40:55', 10, '', 'Record updated by Super Admin', 8, 'Report Footer Details', 'report_header', 'report_id:2', 'footer_name:OP001568-06/00/05-14', '', 'update', 'master', 1, 1, 0, 'pts_mst_report_footer', '{\"report_id\":\"2\",\"footer_name\":\"OP001568-06\\/00\\/05-14\"}'),
('2020-05-11 23:58:41', 10, '', 'Record updated by Super Admin', 9, 'Report Footer Details', 'report_header', 'report_id:2', 'footer_name:OP001568-06/00/05-14', '', 'update', 'master', 2, 1, 0, 'pts_mst_report_footer', '{\"report_id\":\"2\",\"footer_name\":\"OP001568-06\\/00\\/05-14\"}'),
('2020-05-12 00:01:10', 10, '', 'Record created by Super Admin', 10, 'Report Footer Details', 'report_header', 'report_id:3', 'footer_name:OP001568-06/00/05-14', '', 'insert', 'master', 1, 2, 0, 'pts_mst_report_footer', '{\"report_id\":\"3\",\"footer_name\":\"OP001568-06\\/00\\/05-14\",\"created_on\":\"2020-05-12 05:31:10\",\"created_by\":\"10\"}'),
('2020-05-12 00:01:23', 10, '', 'Record created by Super Admin', 11, 'Report Footer Details', 'report_header', 'report_id:4', 'footer_name:OP001568-06/00/05-14', '', 'insert', 'master', 1, 3, 0, 'pts_mst_report_footer', '{\"report_id\":\"4\",\"footer_name\":\"OP001568-06\\/00\\/05-14\",\"created_on\":\"2020-05-12 05:31:23\",\"created_by\":\"10\"}'),
('2020-05-12 00:02:18', 10, '', 'Record created by Super Admin', 12, 'Report Footer Details', 'report_header', 'report_id:16', 'footer_name:OP001568-06/00/05-14', '', 'insert', 'master', 1, 4, 0, 'pts_mst_report_footer', '{\"report_id\":\"16\",\"footer_name\":\"OP001568-06\\/00\\/05-14\",\"created_on\":\"2020-05-12 05:32:18\",\"created_by\":\"10\"}'),
('2020-05-12 00:03:12', 10, '', 'Record created by Super Admin', 13, 'Report Footer Details', 'report_header', 'report_id:5', 'footer_name:OP001568-06/00/05-14', '', 'insert', 'master', 1, 5, 0, 'pts_mst_report_footer', '{\"report_id\":\"5\",\"footer_name\":\"OP001568-06\\/00\\/05-14\",\"created_on\":\"2020-05-12 05:33:12\",\"created_by\":\"10\"}'),
('2020-05-15 00:23:21', 33, 'sid', 'Record created by sid', 14, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2601', 'equipment_id:CPD-032-AA', 'pre_barch_no:JKTU0002', 'insert', 'log', 1, 1, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2601\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"CPD-032-AA\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKTU0002\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test type A\",\"is_in_workflow\":\"no\"}'),
('2020-05-15 00:23:57', 33, 'sid', 'Record created by sid', 15, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2602', 'equipment_id:SH-282-15L-A', 'pre_barch_no:JKTU0002', 'insert', 'log', 1, 2, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2602\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"SH-282-15L-A\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKTU0002\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test type A for portable\",\"is_in_workflow\":\"no\"}'),
('2020-05-15 00:25:22', 33, 'sid', 'Record created by sid', 16, 'Type B Cleaning', 'user_activity_log', 'doc_no:DOC2603', 'equipment_id:SH-282-15L-A', 'pre_barch_no:JKTU0002', 'insert', 'log', 1, 3, 2, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2603\",\"room_code\":\"TAA-78\",\"act_id\":\"2\",\"equipment_id\":\"SH-282-15L-A\",\"act_name\":\"Type B Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKTU0002\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-18 00:02:32', 33, 'sid', 'Record created by sid', 17, 'Type B Cleaning', 'user_activity_log', 'doc_no:DOC2604', 'equipment_id:CPD-032-AA', 'pre_barch_no:JKTU0002', 'insert', 'log', 1, 4, 2, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2604\",\"room_code\":\"TAA-78\",\"act_id\":\"2\",\"equipment_id\":\"CPD-032-AA\",\"act_name\":\"Type B Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKTU0002\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"activity start\",\"is_in_workflow\":\"no\"}'),
('2020-05-18 05:39:11', 33, 'sid', 'Record created by sid', 18, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2605', 'equipment_id:CPD-032-AA', 'pre_barch_no:JKTU0002', 'insert', 'log', 1, 5, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2605\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"CPD-032-AA\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKTU0002\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"start\",\"is_in_workflow\":\"no\"}'),
('2020-05-18 05:53:00', 33, 'sid', 'Record created by sid', 19, 'Type B Cleaning', 'user_activity_log', 'doc_no:DOC2606', 'equipment_id:CPD-032-AA', 'pre_barch_no:JKTU0002', 'insert', 'log', 1, 6, 2, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2606\",\"room_code\":\"TAA-78\",\"act_id\":\"2\",\"equipment_id\":\"CPD-032-AA\",\"act_name\":\"Type B Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKTU0002\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"start\",\"is_in_workflow\":\"no\"}'),
('2020-05-19 03:19:19', 33, 'sid', 'Record created by sid', 20, 'Return Air Filter Cleaning Record', 'air_filter_cleaning_record', 'doc_no:DOC2608', 'frequency_id:1', 'batch_no:JKTU0002', 'insert', 'log', 1, 3, 8, 'pts_trn_return_air_filter', '{\"doc_no\":\"DOC2608\",\"room_code\":\"TAA-78\",\"product_code\":\"0011001A\",\"batch_no\":\"JKTU0002\",\"frequency_id\":\"1\",\"filter\":\"F1,F2,F3\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-19 03:59:46', 33, 'sid', 'Record created by sid', 21, 'Production', 'user_activity_log', 'doc_no:DOC2609', 'equipment_id:CPD-032-AA', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 9, 6, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2609\",\"room_code\":\"TAA-78\",\"act_id\":\"6\",\"equipment_id\":\"CPD-032-AA\",\"act_name\":\"Production\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-19 04:12:34', 10, '', 'Record created by Super Admin', 22, 'Workflow', 'workflow', 'role_id:6', '', '', 'insert', 'master', 1, 87, 0, 'pts_trn_workflowsteps', '[{\"status_id\":\"2\",\"activity_id\":\"6\",\"workflowtype\":\"start\",\"role_id\":\"1\",\"next_step\":\"3\",\"created_by\":\"Superadmin\"},{\"status_id\":\"3\",\"activity_id\":\"6\",\"workflowtype\":\"start\",\"role_id\":\"2\",\"next_step\":\"0\",\"created_by\":\"Superadmin\"}]'),
('2020-05-19 04:17:14', 10, '', 'Record created by Super Admin', 23, 'Workflow', 'workflow', 'role_id:6', '', '', 'insert', 'master', 1, 89, 0, 'pts_trn_workflowsteps', '[{\"status_id\":\"2\",\"activity_id\":\"6\",\"workflowtype\":\"start\",\"role_id\":\"1\",\"next_step\":\"3\",\"created_by\":\"Superadmin\"},{\"status_id\":\"3\",\"activity_id\":\"6\",\"workflowtype\":\"start\",\"role_id\":\"2\",\"next_step\":\"0\",\"created_by\":\"Superadmin\"}]'),
('2020-05-19 04:17:23', 10, '', 'Record created by Super Admin', 24, 'Workflow', 'workflow', 'role_id:6', '', '', 'insert', 'master', 1, 91, 0, 'pts_trn_workflowsteps', '[{\"status_id\":\"2\",\"activity_id\":\"6\",\"workflowtype\":\"stop\",\"role_id\":\"1\",\"next_step\":\"3\",\"created_by\":\"Superadmin\"},{\"status_id\":\"3\",\"activity_id\":\"6\",\"workflowtype\":\"stop\",\"role_id\":\"2\",\"next_step\":\"-1\",\"created_by\":\"Superadmin\"}]'),
('2020-05-19 04:42:17', 33, 'sid', 'Record created by sid', 25, 'Production', 'user_activity_log', 'doc_no:DOC2610', 'equipment_id:CPD-032-AA', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 1, 6, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2610\",\"room_code\":\"TAA-78\",\"act_id\":\"6\",\"equipment_id\":\"CPD-032-AA\",\"act_name\":\"Production\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-19 04:44:20', 34, 'abdul ', 'Record updated by abdul ', 26, 'Vertical Sampler and Dies Cleaning Usages Log', 'user_activity_log', 'doc_no:DOC2611', 'equipment_id:CPD-032-AA', 'pre_barch_no:JKT0001', 'update', 'log', 1, 1, 6, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2611\",\"room_code\":\"TAA-78\",\"act_id\":\"6\",\"equipment_id\":\"CPD-032-AA\",\"act_name\":\"Production\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"34\",\"done_by_user_name\":\"abdul \",\"done_by_role_id\":\"2\",\"done_by_email\":\"abdul@sunpharma.in\",\"done_by_remark\":\"test edit\",\"is_in_workflow\":\"no\"}'),
('2020-05-19 05:00:19', 33, 'sid', 'Record created by sid', 27, 'Production', 'user_activity_log', 'doc_no:DOC2612', 'equipment_id:SH-282-15L-A', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 2, 6, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2612\",\"room_code\":\"TAA-78\",\"act_id\":\"6\",\"equipment_id\":\"SH-282-15L-A\",\"act_name\":\"Production\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-20 01:42:18', 33, 'sid', 'Record created by sid', 28, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2613', 'equipment_id:CPD-032-AA', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 3, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2613\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"CPD-032-AA\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"vdfdf\",\"is_in_workflow\":\"no\"}'),
('2020-05-20 03:23:11', 33, 'sid', 'Record created by sid', 29, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2614', 'equipment_id:CPD-032-AA', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 4, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2614\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"CPD-032-AA\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-20 05:54:49', 33, 'sid', 'Record created by sid', 30, 'Return Air Filter Cleaning Record', 'air_filter_cleaning_record', 'doc_no:DOC2615', 'frequency_id:2', 'batch_no:JKT0001', 'insert', 'log', 1, 4, 5, 'pts_trn_return_air_filter', '{\"doc_no\":\"DOC2615\",\"room_code\":\"TAA-78\",\"product_code\":\"0011001A\",\"batch_no\":\"JKT0001\",\"frequency_id\":\"2\",\"filter\":\"F1,F2,F3\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-20 05:57:17', 33, 'sid', 'Record created by sid', 31, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2616', 'equipment_id:CPD-032-AA', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 6, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2616\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"CPD-032-AA\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-20 05:58:12', 33, 'sid', 'Record created by sid', 32, 'Type D Cleaning', 'user_activity_log', 'doc_no:DOC2617', 'equipment_id:CPD-032-A', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 7, 4, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2617\",\"room_code\":\"TAA-78\",\"act_id\":\"4\",\"equipment_id\":\"CPD-032-A\",\"act_name\":\"Type D Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-20 06:09:12', 33, 'sid', 'Record created by sid', 33, 'Type B Cleaning', 'user_activity_log', 'doc_no:DOC2618', 'equipment_id:SH-282-15L-A', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 8, 2, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2618\",\"room_code\":\"TAA-78\",\"act_id\":\"2\",\"equipment_id\":\"SH-282-15L-A\",\"act_name\":\"Type B Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-20 06:09:43', 33, 'sid', 'Record created by sid', 34, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2619', 'equipment_id:SH-282-10L', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 9, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2619\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"SH-282-10L\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-20 06:11:45', 33, 'sid', 'Record created by sid', 35, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2620', 'equipment_id:SH-282-15L-A', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 1, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2620\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"SH-282-15L-A\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-20 06:22:33', 33, 'sid', 'Record created by sid', 36, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2621', 'equipment_id:CPD-032-AA', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 2, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2621\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"CPD-032-AA\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-20 06:23:27', 33, 'sid', 'Record created by sid', 37, 'Vaccum Cleaner Logbook', 'vaccum_cleaner_logbook', 'doc_no:DOC2622', 'vaccum_cleaner_code:OSPE-005', 'batch_no:JKT0001', 'insert', 'log', 1, 12, 3, 'pts_trn_vaccum_cleaner', '{\"doc_no\":\"DOC2622\",\"room_code\":\"TAA-78\",\"product_code\":\"0011001A\",\"batch_no\":\"JKT0001\",\"vaccum_cleaner_code\":\"OSPE-005\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-20 23:22:20', 33, 'sid', 'Record created by sid', 38, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2623', 'equipment_id:CPD-032-A,CPD-033-A,CPD-034-A,CPD-034-A,CPD-036-AA,CPD-036-AA,CPD-036-AA,CPD-036-AA,CPD', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 4, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2623\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"CPD-032-A,CPD-033-A,CPD-034-A,CPD-034-A,CPD-036-AA,CPD-036-AA,CPD-036-AA,CPD-036-AA,CPD-037-A,CPD-037-A,CPD-037-A,CPD-037-A,CPD-037-A,CPD-037-A,CPD-037-A,CPD-037-A,CPD-038-A,CPD-038-A,CPD-038-A,CPD-038-A,CPD-038-A,CPD-038-A,CPD-038-A,CPD-038-A,CPD-038-A,CPD-038-A,CPD-038-A,CPD-038-A,CPD-038-A,CPD-038-A,CPD-038-A,CPD-038-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A,CPD-039-A\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-21 02:00:38', 33, 'sid', 'Record created by sid', 39, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2624', 'equipment_id:CPD-032-AA', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 1, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2624\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"CPD-032-AA\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test start\",\"is_in_workflow\":\"no\"}'),
('2020-05-21 03:51:03', 10, '', 'Record created by Super Admin', 40, 'Report Footer Details', 'report_header', 'report_id:13', 'footer_name:OP001568-06/00/05-14', '', 'insert', 'master', 1, 6, 0, 'pts_mst_report_footer', '{\"report_id\":\"13\",\"footer_name\":\"OP001568-06\\/00\\/05-14\",\"created_on\":\"2020-05-21 09:21:03\",\"created_by\":\"10\"}'),
('2020-05-21 06:40:41', 33, 'sid', 'Record created by sid', 41, 'Production', 'user_activity_log', 'doc_no:DOC2625', 'equipment_id:CPD-032-AA,CPD-032-A,CPD-033-A,CPD-034-A', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 2, 6, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2625\",\"room_code\":\"TAA-78\",\"act_id\":\"6\",\"equipment_id\":\"CPD-032-AA,CPD-032-A,CPD-033-A,CPD-034-A\",\"act_name\":\"Production\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-21 23:23:37', 10, '', 'Record created by Super Admin', 42, 'Workflow', 'workflow', 'role_id:6', '', '', 'insert', 'master', 1, 94, 0, 'pts_trn_workflowsteps', '[{\"status_id\":\"2\",\"activity_id\":\"1\",\"workflowtype\":\"start\",\"role_id\":\"1\",\"next_step\":\"3\",\"created_by\":\"Superadmin\"},{\"status_id\":\"3\",\"activity_id\":\"1\",\"workflowtype\":\"start\",\"role_id\":\"2\",\"next_step\":\"6\",\"created_by\":\"Superadmin\"},{\"status_id\":\"6\",\"activity_id\":\"1\",\"workflowtype\":\"start\",\"role_id\":\"5\",\"next_step\":\"0\",\"created_by\":\"Superadmin\"}]'),
('2020-05-21 23:26:41', 33, 'sid', 'Record created by sid', 43, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2626', 'equipment_id:CPD-032-AA', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 3, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2626\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"CPD-032-AA\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-26 23:49:41', 10, '', 'Record created by Super Admin', 44, 'Workflow', 'workflow', 'role_id:6', '', '', 'insert', 'master', 1, 96, 0, 'pts_trn_workflowsteps', '[{\"status_id\":\"2\",\"activity_id\":\"19\",\"workflowtype\":\"stop\",\"role_id\":\"1\",\"next_step\":\"3\",\"created_by\":\"Superadmin\"},{\"status_id\":\"3\",\"activity_id\":\"19\",\"workflowtype\":\"stop\",\"role_id\":\"2\",\"next_step\":\"-1\",\"created_by\":\"Superadmin\"}]'),
('2020-05-26 23:57:32', 33, 'sid', 'Record created by sid', 45, 'Return Air Filter Cleaning Record', 'air_filter_cleaning_record', 'doc_no:DOC2627', 'frequency_id:2', 'batch_no:NA', 'insert', 'log', 1, 5, 4, 'pts_trn_return_air_filter', '{\"doc_no\":\"DOC2627\",\"room_code\":\"TAA-78\",\"product_code\":\"PL002\",\"batch_no\":\"NA\",\"frequency_id\":\"2\",\"filter\":\"F1,F2,F3\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-27 00:14:39', 33, 'sid', 'Record created by sid', 46, 'Material Retrieval & Relocation Record for Cold Room', 'material_retreival_relocation', 'doc_no:', 'process_id:53', 'pre_barch_no:', 'insert', 'log', 1, 0, 5, 'pts_trn_material_retreival_relocation', '{\"doc_no\":\"\",\"room_code\":\"TAA-78\",\"act_id\":\"19\",\"process_id\":\"53\",\"act_name\":\"Material Retreival and Relocation Record for Cold Room\",\"pre_product_code\":\"\",\"pre_barch_no\":\"\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test edit\",\"is_in_workflow\":\"no\"}'),
('2020-05-27 02:12:52', 33, 'sid', 'Record created by sid', 47, 'Vaccum Cleaner Logbook', 'vaccum_cleaner_logbook', 'doc_no:DOC2629', 'vaccum_cleaner_code:OSPE-005', 'batch_no:JKTU0002', 'insert', 'log', 1, 13, 6, 'pts_trn_vaccum_cleaner', '{\"doc_no\":\"DOC2629\",\"room_code\":\"TAA-78\",\"product_code\":\"PL001\",\"batch_no\":\"JKTU0002\",\"vaccum_cleaner_code\":\"OSPE-005\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-27 04:48:15', 33, 'sid', 'Record created by sid', 48, 'Return Air Filter Cleaning Record', 'air_filter_cleaning_record', 'doc_no:DOC2630', 'frequency_id:2', 'batch_no:NA', 'insert', 'log', 1, 6, 7, 'pts_trn_return_air_filter', '{\"doc_no\":\"DOC2630\",\"room_code\":\"TAA-78\",\"product_code\":\"0011001A\",\"batch_no\":\"NA\",\"frequency_id\":\"2\",\"filter\":\"F1,F2,F3\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-27 04:59:06', 34, 'abdul ', 'Record created by abdul ', 49, 'Equipment/Apparatus Log Register (Friabilator)', 'equipment_apparatus_log_register', 'doc_no:DOC2631', 'equip_appratus_no:Apparatus001', 'batch_no:JKTU0002', 'insert', 'log', 1, 3, 8, 'pts_trn_equip_appratus_log', '{\"doc_no\":\"DOC2631\",\"room_code\":\"TAA-78\",\"product_code\":\"PL002\",\"batch_no\":\"JKTU0002\",\"equip_appratus_no\":\"Apparatus001\",\"stage\":\"stage 2\",\"done_by_user_id\":\"34\",\"done_by_user_name\":\"abdul \",\"done_by_role_id\":\"2\",\"done_by_email\":\"abdul@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\",\"cleaning_verification\":\"notok\"}'),
('2020-05-27 05:09:46', 10, '', 'Record created by Super Admin', 50, 'Workflow', 'workflow', 'role_id:6', '', '', 'insert', 'master', 1, 99, 0, 'pts_trn_workflowsteps', '[{\"status_id\":\"2\",\"activity_id\":\"29\",\"workflowtype\":\"stop\",\"role_id\":\"1\",\"next_step\":\"3\",\"created_by\":\"Superadmin\"},{\"status_id\":\"3\",\"activity_id\":\"29\",\"workflowtype\":\"stop\",\"role_id\":\"2\",\"next_step\":\"6\",\"created_by\":\"Superadmin\"},{\"status_id\":\"6\",\"activity_id\":\"29\",\"workflowtype\":\"stop\",\"role_id\":\"5\",\"next_step\":\"-1\",\"created_by\":\"Superadmin\"}]'),
('2020-05-27 05:18:38', 33, 'sid', 'Record created by sid', 51, 'Pre-Filter Cleaning Record of LAF', 'pre_filter_cleaning', 'doc_no:DOC2632', 'product_code:PL001', 'pre_barch_no:JKTU0002', 'insert', 'log', 1, 5, 9, 'pts_trn_pre_filter_cleaning', '{\"doc_no\":\"DOC2632\",\"room_code\":\"TAA-78\",\"selection_status\":\"from\",\"pre_filter_cleaning\":\"yes\",\"outer_surface_cleaning\":\"yes\",\"product_code\":\"PL001\",\"batch_no\":\"JKTU0002\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-27 05:47:56', 33, 'sid', 'Record created by sid', 52, 'LAF Pressure Differential Record', 'laf_pressure_record', 'doc_no:DOC2633', 'room_code:TAA-78', 'booth_no:DISP1', 'insert', 'log', 1, 8, 10, 'pts_trn_laf_pressure_diff', '{\"doc_no\":\"DOC2633\",\"room_code\":\"TAA-78\",\"m1\":\"5\",\"m2\":\"7\",\"g1\":\"9\",\"g2\":\"7\",\"g3\":\"7\",\"booth_no\":\"DISP1\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test edit\",\"is_in_workflow\":\"no\"}'),
('2020-05-28 03:01:00', 33, 'sid', 'Record created by sid', 53, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2634', 'equipment_id:SH-282-15L-A', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 11, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2634\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"SH-282-15L-A\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-28 03:30:18', 33, 'sid', 'Record created by sid', 54, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2635', 'equipment_id:CPD-032-AA', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 12, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2635\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"CPD-032-AA\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-28 04:21:45', 33, 'sid', 'Record created by sid', 55, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2636', 'equipment_id:CPD-032-A', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 13, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2636\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"CPD-032-A\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-28 04:54:39', 33, 'sid', 'Record created by sid', 56, 'Vertical Sampler and Dies Cleaning Usages Log/Type A', 'user_activity_log', 'doc_no:DOC2637', 'equipment_id:Rode0012', 'pre_barch_no:JKTU0002', 'insert', 'log', 1, 14, 25, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2637\",\"room_code\":\"TAA-78\",\"act_id\":\"25\",\"equipment_id\":\"Rode0012\",\"act_name\":\"Vertical Sampler and Dies Cleaning Usages Log\\/Type A\",\"pre_product_code\":\"PL002\",\"pre_barch_no\":\"JKTU0002\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test edit\",\"is_in_workflow\":\"no\"}'),
('2020-05-28 05:33:43', 34, 'abdul ', 'Record updated by abdul ', 57, 'Vertical Sampler and Dies Cleaning Usages Log', 'user_activity_log', 'doc_no:DOC2637', 'equipment_id:Rode0012', 'pre_barch_no:JKTU0002', 'update', 'log', 1, 14, 25, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2637\",\"room_code\":\"TAA-78\",\"act_id\":\"25\",\"equipment_id\":\"Rode0012\",\"act_name\":\"Vertical Sampler and Dies Cleaning Usages Log\\/\",\"pre_product_code\":\"PL002\",\"pre_barch_no\":\"JKTU0002\",\"done_by_user_id\":\"34\",\"done_by_user_name\":\"abdul \",\"done_by_role_id\":\"2\",\"done_by_email\":\"abdul@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-29 19:11:10', 33, 'sid', 'Record created by sid', 58, 'Instrument Log Register', 'instrument_log_register', 'doc_no:DOC2638', 'instrument_code:Instrument001', 'batch_no:JKTU0002', 'insert', 'log', 1, 5, 15, 'pts_trn_instrument_log_register', '{\"doc_no\":\"DOC2638\",\"room_code\":\"TAA-78\",\"product_code\":\"PL002\",\"batch_no\":\"JKTU0002\",\"instrument_code\":\"Instrument001\",\"stage\":\"stage 1\",\"test\":\"test\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-05-29 19:27:24', 10, '', 'Record created by Super Admin', 59, 'Work Assign Details ', 'work_assign_asper_rolebase', 'role_id:2', '', '', 'insert', 'master', 1, 13, 0, 'pts_mst_role_workassign_to_mult_roles', '[{\"roleid\":\"2\",\"workassign_to_roleid\":\"5\",\"created_by\":\"Super Admin\"}]'),
('2020-05-29 19:28:34', 10, '', 'Record created by Super Admin', 60, 'Work Assign Details ', 'work_assign_asper_rolebase', 'role_id:5', '', '', 'insert', 'master', 1, 14, 0, 'pts_mst_role_workassign_to_mult_roles', '[{\"roleid\":\"5\",\"workassign_to_roleid\":\"11\",\"created_by\":\"Super Admin\"}]'),
('2020-05-29 19:30:09', 10, '', 'Record created by Super Admin', 61, 'Workflow', 'workflow', 'role_id:6', '', '', 'insert', 'master', 1, 101, 0, 'pts_trn_workflowsteps', '[{\"status_id\":\"2\",\"activity_id\":\"29\",\"workflowtype\":\"stop\",\"role_id\":\"1\",\"next_step\":\"3\",\"created_by\":\"Superadmin\"},{\"status_id\":\"3\",\"activity_id\":\"29\",\"workflowtype\":\"stop\",\"role_id\":\"2\",\"next_step\":\"-1\",\"created_by\":\"Superadmin\"}]'),
('2020-05-29 19:30:35', 33, 'sid', 'Record created by sid', 62, 'Pre-Filter Cleaning Record of LAF', 'pre_filter_cleaning', 'doc_no:DOC2639', 'product_code:PL002', 'pre_barch_no:JKTU0002', 'insert', 'log', 1, 1, 16, 'pts_trn_pre_filter_cleaning', '{\"doc_no\":\"DOC2639\",\"room_code\":\"TAA-78\",\"selection_status\":\"from\",\"pre_filter_cleaning\":\"yes\",\"outer_surface_cleaning\":\"yes\",\"product_code\":\"PL002\",\"batch_no\":\"JKTU0002\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"sfdsfdsd\",\"is_in_workflow\":\"no\"}'),
('2020-05-29 19:31:25', 10, '', 'Record created by Super Admin', 63, 'Work Assign Details ', 'work_assign_asper_rolebase', 'role_id:2', '', '', 'insert', 'master', 1, 15, 0, 'pts_mst_role_workassign_to_mult_roles', '[{\"roleid\":\"2\",\"workassign_to_roleid\":\"2\",\"created_by\":\"Super Admin\"},{\"roleid\":\"2\",\"workassign_to_roleid\":\"3\",\"created_by\":\"Super Admin\"}]'),
('2020-06-01 03:42:24', 33, 'sid', 'Record created by sid', 64, 'Instrument Log Register', 'instrument_log_register', 'doc_no:DOC2640', 'instrument_code:Instrument001', 'batch_no:JKTU0002', 'insert', 'log', 1, 6, 17, 'pts_trn_instrument_log_register', '{\"doc_no\":\"DOC2640\",\"room_code\":\"TAA-78\",\"product_code\":\"PL002\",\"batch_no\":\"JKTU0002\",\"instrument_code\":\"Instrument001\",\"stage\":\"stage 1\",\"test\":\"fdgdfgd\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-06-01 08:01:30', 33, 'sid', 'Record created by sid', 65, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC2', 'equipment_id:CPD-033-A', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 20, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC2\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"CPD-033-A\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-06-03 03:05:14', 33, 'sid', 'Record created by sid', 66, 'Swab Sample Record', 'swab_sample_record', 'doc_no:DOC3', 'equipment_id:SHSP-015', 'pre_barch_no:JKTU0002', 'insert', 'log', 1, 5, 21, 'pts_trn_swab_sample_record', '{\"doc_no\":\"DOC3\",\"room_code\":\"TAA-78\",\"ar_numer\":\"ARN001\",\"equipment_id\":\"SHSP-015\",\"equipment_desc\":\"Induction sealing machine\",\"pre_product_code\":\"PL002\",\"pre_barch_no\":\"JKTU0002\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-06-03 06:42:11', 33, 'sid', 'Record created by sid', 67, 'Type A Cleaning', 'user_activity_log', 'doc_no:DOC4', 'equipment_id:SH-282-10L', 'pre_barch_no:JKT0001', 'insert', 'log', 1, 22, 1, 'pts_trn_user_activity_log', '{\"doc_no\":\"DOC4\",\"room_code\":\"TAA-78\",\"act_id\":\"1\",\"equipment_id\":\"SH-282-10L\",\"act_name\":\"Type A Cleaning\",\"pre_product_code\":\"0011001A\",\"pre_barch_no\":\"JKT0001\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-06-03 06:46:19', 10, '', 'Record created by Super Admin', 68, 'Work Assign Details ', 'work_assign_asper_rolebase', 'role_id:5', '', '', 'insert', 'master', 1, 17, 0, 'pts_mst_role_workassign_to_mult_roles', '[{\"roleid\":\"5\",\"workassign_to_roleid\":\"5\",\"created_by\":\"Super Admin\"}]'),
('2020-06-08 04:52:39', 10, '', 'Record created by Super Admin', 69, 'Workflow', 'workflow', 'role_id:6', '', '', 'insert', 'master', 1, 103, 0, 'pts_trn_workflowsteps', '[{\"status_id\":\"2\",\"activity_id\":\"29\",\"workflowtype\":\"stop\",\"role_id\":\"1\",\"next_step\":\"3\",\"created_by\":\"Superadmin\"},{\"status_id\":\"3\",\"activity_id\":\"29\",\"workflowtype\":\"stop\",\"role_id\":\"2\",\"next_step\":\"-1\",\"created_by\":\"Superadmin\"}]'),
('2020-06-08 06:49:20', 33, 'sid', 'Record created by sid', 70, 'Pre-Filter Cleaning Record of LAF', 'pre_filter_cleaning', 'doc_no:DOC5', 'product_code:PL002', 'pre_barch_no:JKTU0002', 'insert', 'log', 1, 1, 1, 'pts_trn_pre_filter_cleaning', '{\"doc_no\":\"DOC5\",\"room_code\":\"TAA-78\",\"selection_status\":\"from\",\"pre_filter_cleaning\":\"yes\",\"outer_surface_cleaning\":\"yes\",\"product_code\":\"PL002\",\"batch_no\":\"JKTU0002\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"test\",\"is_in_workflow\":\"no\"}'),
('2020-06-08 06:51:30', 34, 'abdul ', 'Record updated by abdul ', 71, 'Pre-Filter Cleaning Record of LAF', 'pre_filter_cleaning', 'doc_no:DOC5', 'product_code:PL002', 'pre_barch_no:JKTU0002', 'update', 'log', 1, 1, 1, 'pts_trn_pre_filter_cleaning', '{\"doc_no\":\"DOC5\",\"room_code\":\"TAA-78\",\"selection_status\":\"from\",\"pre_filter_cleaning\":\"yes\",\"outer_surface_cleaning\":\"yes\",\"product_code\":\"PL002\",\"batch_no\":\"JKTU0002\",\"is_in_workflow\":\"yes\",\"updated_by\":\"34\",\"updated_on\":\"2020-06-08 12:21:30\",\"update_remark\":\"test\"}'),
('2020-06-09 02:51:34', 33, 'sid', 'Record created by sid', 72, 'Return Air Filter Cleaning Record', 'air_filter_cleaning_record', 'doc_no:DOC6', 'frequency_id:1', 'batch_no:NA', 'insert', 'log', 1, 7, 2, 'pts_trn_return_air_filter', '{\"doc_no\":\"DOC6\",\"room_code\":\"TAA-78\",\"product_code\":\"0011001A\",\"batch_no\":\"NA\",\"frequency_id\":\"1\",\"filter\":\"F1,F2,F3\",\"done_by_user_id\":\"33\",\"done_by_user_name\":\"sid\",\"done_by_role_id\":\"1\",\"done_by_email\":\"sid@sunpharma.in\",\"done_by_remark\":\"fdfdfdfdf\",\"is_in_workflow\":\"no\"}'),
('2020-06-09 03:58:33', 10, '', 'Record created by Super Admin', 73, 'Employee detail', 'manage_employee', 'emp_code:bfbdfgbdf', 'emp_email:dfdjfjd', 'role_id:1', 'insert', 'master', 1, 51, 0, 'mst_employee', '{\"emp_code\":\"bfbdfgbdf\",\"emp_name\":\"test878\",\"email2\":\"jsdfgffds@dasds.com\",\"emp_address\":\"\",\"emp_contact\":\"9999999999\",\"emp_email\":\"dfdjfjd\",\"emp_password\":\"23810a325304076e09bb6445c8b67a3e4f6ad8856e6cb569bea8e49a383a0a09\",\"designation_code\":\"s\\/w engineer\",\"block_code\":\"Block-OSD\",\"Sub_block_code\":\"OSD-1\",\"area_code\":\"A-001,A-002,A-003,A-004,A-111,A-112,A-113,A-114,A-115,A-116,A-117,A-118\",\"role_id\":\"1\",\"emp_remark\":\"\",\"created_by\":\"Superadmin\"}'),
('2020-06-09 04:01:22', 10, '', 'Record created by Super Admin', 74, 'Employee detail', 'manage_employee', 'emp_code:test001', 'emp_email:test98', 'role_id:1', 'insert', 'master', 1, 52, 0, 'mst_employee', '{\"emp_code\":\"test001\",\"emp_name\":\"testuser\",\"email2\":\"test@gmail.com\",\"emp_address\":\"\",\"emp_contact\":\"9999999999\",\"emp_email\":\"test98\",\"emp_password\":\"23810a325304076e09bb6445c8b67a3e4f6ad8856e6cb569bea8e49a383a0a09\",\"designation_code\":\"Developer\",\"block_code\":\"Block-OSD\",\"Sub_block_code\":\"OSD-1\",\"area_code\":\"A-001,A-002,A-003,A-004,A-111,A-112,A-113,A-114,A-115,A-116,A-117,A-118\",\"role_id\":\"1\",\"emp_remark\":\"\",\"created_by\":\"Superadmin\"}');

-- --------------------------------------------------------

--
-- Table structure for table `pts_log_tableaction`
--

DROP TABLE IF EXISTS `pts_log_tableaction`;
CREATE TABLE IF NOT EXISTS `pts_log_tableaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(200) DEFAULT NULL,
  `tablename` varchar(500) DEFAULT NULL,
  `tableid` int(11) DEFAULT NULL,
  `created_by` varchar(500) DEFAULT NULL,
  `created_on` varchar(500) DEFAULT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_log_tableaction`
--

INSERT INTO `pts_log_tableaction` (`id`, `action`, `tablename`, `tableid`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(1, 'Updated', 'mst_employee', 44, 'Superadmin', '2020-01-15 11:06:41', 'Superadmin', '2020-03-16 15:26:51'),
(2, 'Updated', 'mst_employee', 44, 'Superadmin', '2020-01-15 11:06:41', 'Superadmin', '2020-03-16 15:27:18');

-- --------------------------------------------------------

--
-- Table structure for table `pts_log_tablechanges`
--

DROP TABLE IF EXISTS `pts_log_tablechanges`;
CREATE TABLE IF NOT EXISTS `pts_log_tablechanges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log_id` int(11) DEFAULT NULL,
  `fieldname` varchar(500) DEFAULT NULL,
  `oldvalue` text,
  `newvalue` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_log_tablechanges`
--

INSERT INTO `pts_log_tablechanges` (`id`, `log_id`, `fieldname`, `oldvalue`, `newvalue`) VALUES
(1, 1, 'emp_password', '76b5573f4fa5e70ce8bc70f4af6671e8da3af3e802e83e0a9183c70b3abb007d', 'a5a03efcb7da3fdaf68fcb1fc2ef07ae29add8f96a9dde88b75e05afce6f8d65'),
(2, 2, 'emp_code', 'rahul0028', 'rahul00288'),
(3, 2, 'emp_name', 'Rahul Chauhan', 'Rahul Chauhan2'),
(4, 2, 'emp_address', 'Ghaziabad', 'Ghaziabadd'),
(5, 2, 'role_id', '2', '1'),
(6, 2, 'block_code', 'Block-OSD', 'Block-OSD2'),
(7, 2, 'Sub_block_code', 'OSD-1', 'OSD-2'),
(8, 2, 'area_code', 'A-001,A-002,A-003,A-004,A-111,A-112,A-113,A-114,A-115,A-116,A-117,A-118', 'A-006');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_activity`
--

DROP TABLE IF EXISTS `pts_mst_activity`;
CREATE TABLE IF NOT EXISTS `pts_mst_activity` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(80) DEFAULT NULL,
  `activity_url` varchar(100) DEFAULT NULL,
  `is_need_approval` enum('yes','no') DEFAULT 'yes',
  `workflowtype` varchar(200) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `type` varchar(50) DEFAULT NULL,
  `room_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pts_mst_activity`
--

INSERT INTO `pts_mst_activity` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `activity_name`, `activity_url`, `is_need_approval`, `workflowtype`, `status`, `type`, `room_id`) VALUES
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 1, 'Type A Cleaning', NULL, 'yes', 'onstop', 'active', 'home', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 2, 'Type B Cleaning', NULL, 'yes', 'onstop', 'active', 'home', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 3, 'Type C Cleaning', NULL, 'yes', 'onstop', 'active', 'home', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 4, 'Type D Cleaning', NULL, 'yes', 'onstop', 'active', 'home', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 5, 'Maintenance', NULL, 'yes', 'onstart', 'active', 'home', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 6, 'Production', NULL, 'yes', 'onstart', 'active', 'home', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 7, 'Sampling', NULL, 'yes', 'onstart', 'active', 'home', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 8, 'Issuance', NULL, 'yes', NULL, 'active', 'tabletooling', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 9, 'Cleaning Before Use', NULL, 'yes', NULL, 'active', 'tabletooling', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 10, 'Inspection / Verification Before Use', NULL, 'yes', NULL, 'active', 'tabletooling', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 11, 'Usage', NULL, 'yes', NULL, 'active', 'tabletooling', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 12, 'Cleaning After Use', NULL, 'yes', NULL, 'active', 'tabletooling', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 13, 'Inspection After Use', NULL, 'yes', NULL, 'active', 'tabletooling', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 14, 'No. of Punch/Die Returned to Storage Cabinet', NULL, 'yes', NULL, 'active', 'tabletooling', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 16, 'Process Log', 'processlog_equipment', 'no', NULL, 'active', 'log', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 17, 'Portable Equipment Log', 'process_log_portable_equipment', 'no', NULL, 'active', 'log', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 18, 'Return Air Filter Cleaning Record', 'air_filter_cleaning_record', 'yes', NULL, 'active', 'log', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 19, 'Material Retreival and Relocation Record for Cold Room', 'material_retreival_relocation_record', 'yes', NULL, 'active', 'log', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 20, 'Environmental Condition/Pressure Differential Record', 'environmental_condition_record', 'yes', NULL, 'active', 'log', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 21, 'Vaccum Cleaner Logbook', 'vaccum_cleaner_logbook', 'yes', NULL, 'active', 'log', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 22, 'LAF Pressure Differential Record Sheet', 'laf_pressure_record', 'yes', NULL, 'active', 'log', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 23, 'Tablet Tooling Log Card', 'tablet_tooling_log_card', 'yes', NULL, 'active', 'log', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 24, 'Balance Calibration Record', 'balance_calibration_record', 'yes', NULL, 'active', 'log', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 25, 'Vertical Sampler and Dies Cleaning Usages Log', 'vertical_sampler_dies_cleaning', 'yes', NULL, 'active', 'log', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 26, 'Instrument Log Register', 'instrument_log_register', 'no', NULL, 'active', 'log', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 27, 'Equipment/Apparatus Log Register (Friabilator)', 'equipment_apparatus_log_register', 'no', NULL, 'active', 'log', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 28, 'Swab Sample Record', 'swab_sample_record', 'yes', NULL, 'active', 'log', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 29, 'Pre-Filter Cleaning Record of LAF', 'pre_filter_cleaning_record', 'yes', NULL, 'active', 'log', NULL),
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 30, 'Line Log', 'line_log', 'no', NULL, 'active', 'log', NULL),
('2020-03-17 03:11:45', '', NULL, NULL, 31, 'Equipment/Apparatus Log Register (Leak Test)', 'sf_equipment_apparatus_leaktest', 'no', NULL, 'active', 'log', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_balance_calibration`
--

DROP TABLE IF EXISTS `pts_mst_balance_calibration`;
CREATE TABLE IF NOT EXISTS `pts_mst_balance_calibration` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `room_code` varchar(100) NOT NULL,
  `balance_no` varchar(100) NOT NULL,
  `capacity` varchar(100) NOT NULL,
  `least_count` varchar(100) NOT NULL,
  `acceptance_limit` varchar(50) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_balance_calibration`
--

INSERT INTO `pts_mst_balance_calibration` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `room_id`, `room_code`, `balance_no`, `capacity`, `least_count`, `acceptance_limit`, `status`) VALUES
('2020-03-17 08:41:45', '', '2020-03-18 11:28:17', 'Superadmin', 3, 52, 'TAA-58,TAA-59', 'test bal 1', '50', '52', '45', 'active'),
('2020-04-07 08:50:51', '', '0000-00-00 00:00:00', '', 4, 0, '', 'PBGBL0002', '50', '2', '', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_balance_calibration_standard`
--

DROP TABLE IF EXISTS `pts_mst_balance_calibration_standard`;
CREATE TABLE IF NOT EXISTS `pts_mst_balance_calibration_standard` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `balance_calibration_id` int(11) NOT NULL,
  `measurement_unit` varchar(20) NOT NULL,
  `id_standard` float NOT NULL,
  `frequency_id` int(11) NOT NULL,
  `standard_value` float NOT NULL,
  `min_value` float NOT NULL,
  `max_value` float NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_balance_calibration_standard`
--

INSERT INTO `pts_mst_balance_calibration_standard` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `balance_calibration_id`, `measurement_unit`, `id_standard`, `frequency_id`, `standard_value`, `min_value`, `max_value`, `status`) VALUES
('2020-04-01 08:35:05', '', '0000-00-00 00:00:00', '', 13, 3, 'KG', 56, 1, 23, 22, 55, 'active'),
('2020-04-01 08:35:05', '', '0000-00-00 00:00:00', '', 14, 3, 'KG', 454, 2, 300, 200, 500, 'active'),
('2020-04-07 08:50:51', '', '0000-00-00 00:00:00', '', 15, 4, 'KG', 0, 0, 400, 300, 500, 'active'),
('2020-04-07 08:50:51', '', '0000-00-00 00:00:00', '', 16, 4, 'KG', 0, 0, 500, 200, 500, 'active'),
('2020-04-07 08:50:51', '', '0000-00-00 00:00:00', '', 17, 4, 'KG', 0, 0, 600, 300, 600, 'active'),
('2020-04-07 08:50:51', '', '0000-00-00 00:00:00', '', 18, 4, 'KG', 0, 0, 800, 200, 1000, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_balance_frequency_data`
--

DROP TABLE IF EXISTS `pts_mst_balance_frequency_data`;
CREATE TABLE IF NOT EXISTS `pts_mst_balance_frequency_data` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `balance_no` varchar(100) NOT NULL,
  `frequency_id` int(11) NOT NULL,
  `id_standard` varchar(20) DEFAULT NULL,
  `measurement_unit` varchar(20) NOT NULL,
  `standard_value` varchar(20) NOT NULL,
  `min_value` varchar(20) NOT NULL,
  `max_value` varchar(20) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_balance_frequency_data`
--

INSERT INTO `pts_mst_balance_frequency_data` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `balance_no`, `frequency_id`, `id_standard`, `measurement_unit`, `standard_value`, `min_value`, `max_value`, `status`) VALUES
('2020-04-07 08:41:55', '', '0000-00-00 00:00:00', '', 9, 'test bal 1', 1, '56', 'KG', '23', '22', '55', 'active'),
('2020-04-07 08:44:48', '', '0000-00-00 00:00:00', '', 11, 'test bal 1', 2, '56', 'KG', '23', '22', '55', 'active'),
('2020-04-07 08:44:48', '', '0000-00-00 00:00:00', '', 12, 'test bal 1', 2, '454', 'KG', '300', '200', '500', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_document`
--

DROP TABLE IF EXISTS `pts_mst_document`;
CREATE TABLE IF NOT EXISTS `pts_mst_document` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `prefix` varchar(50) DEFAULT NULL,
  `docno` bigint(50) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pts_mst_document`
--

INSERT INTO `pts_mst_document` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `prefix`, `docno`, `isactive`) VALUES
('2020-03-17 08:41:45', '', '0000-00-00 00:00:00', '', 13, 'DOC', 6, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_filter`
--

DROP TABLE IF EXISTS `pts_mst_filter`;
CREATE TABLE IF NOT EXISTS `pts_mst_filter` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `filter_name` varchar(100) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `filter_desc` varchar(150) DEFAULT NULL,
  `status` enum('on','off') DEFAULT 'on',
  `is_approved` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pts_mst_filter`
--

INSERT INTO `pts_mst_filter` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `filter_name`, `room_code`, `filter_desc`, `status`, `is_approved`) VALUES
('2020-04-02 09:49:39', '', '0000-00-00 00:00:00', '', 40, 'F1', 'IPQA', NULL, 'on', b'1'),
('2020-04-02 09:49:39', '', '0000-00-00 00:00:00', '', 41, 'F2', 'IPQA', NULL, 'on', b'1'),
('2020-04-02 09:49:39', '', '0000-00-00 00:00:00', '', 42, 'F3', 'IPQA', NULL, 'on', b'1'),
('2020-04-02 09:57:50', '', '0000-00-00 00:00:00', '', 53, 'F1', 'PMA-64', NULL, 'on', b'0'),
('2020-04-02 09:57:50', '', '0000-00-00 00:00:00', '', 54, 'F2', 'PMA-64', NULL, 'on', b'0'),
('2020-04-02 09:57:50', '', '0000-00-00 00:00:00', '', 55, 'F3', 'PMA-64', NULL, 'on', b'0'),
('2020-04-02 09:57:50', '', '0000-00-00 00:00:00', '', 56, 'F4', 'PMA-64', NULL, 'on', b'0'),
('2020-04-02 09:57:50', '', '0000-00-00 00:00:00', '', 57, 'F5', 'PMA-64', NULL, 'on', b'0'),
('2020-04-03 09:55:42', '', '0000-00-00 00:00:00', '', 63, 'F1', 'TAA-78', NULL, 'on', b'1'),
('2020-04-03 09:55:42', '', '0000-00-00 00:00:00', '', 64, 'F2', 'TAA-78', NULL, 'on', b'1'),
('2020-04-03 09:55:42', '', '0000-00-00 00:00:00', '', 65, 'F3', 'TAA-78', NULL, 'on', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_frequency`
--

DROP TABLE IF EXISTS `pts_mst_frequency`;
CREATE TABLE IF NOT EXISTS `pts_mst_frequency` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `frequency_name` varchar(100) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pts_mst_frequency`
--

INSERT INTO `pts_mst_frequency` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `frequency_name`, `status`) VALUES
('2020-03-17 08:41:46', '', '0000-00-00 00:00:00', '', 1, 'Daily', 'active'),
('2020-03-17 08:41:46', '', '0000-00-00 00:00:00', '', 2, 'Weekly', 'active'),
('2020-03-17 08:41:46', '', '0000-00-00 00:00:00', '', 3, 'Fortnightly', 'active'),
('2020-03-17 08:41:46', '', '0000-00-00 00:00:00', '', 4, 'Monthly', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_id_standard_weight`
--

DROP TABLE IF EXISTS `pts_mst_id_standard_weight`;
CREATE TABLE IF NOT EXISTS `pts_mst_id_standard_weight` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_no_statndard_weight` varchar(100) NOT NULL,
  `weight` float NOT NULL,
  `measurement_unit` varchar(20) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_id_standard_weight`
--

INSERT INTO `pts_mst_id_standard_weight` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `id_no_statndard_weight`, `weight`, `measurement_unit`, `status`) VALUES
('2020-04-02 14:15:06', '', '0000-00-00 00:00:00', '', 7, 'P1', 300, 'KG', 'active'),
('2020-04-02 14:15:06', '', '0000-00-00 00:00:00', '', 8, 'P2', 300, 'KG', 'active'),
('2020-04-02 14:16:33', '', '0000-00-00 00:00:00', '', 9, 'M1', 150, 'KG', 'active'),
('2020-04-02 14:16:33', '', '0000-00-00 00:00:00', '', 10, 'M2', 150, 'KG', 'active'),
('2020-04-02 16:18:40', '', '0000-00-00 00:00:00', '', 11, '23', 23, 'KG', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_instrument`
--

DROP TABLE IF EXISTS `pts_mst_instrument`;
CREATE TABLE IF NOT EXISTS `pts_mst_instrument` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_code` varchar(20) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `equipment_name` varchar(100) NOT NULL,
  `equipment_code` varchar(100) NOT NULL,
  `equipment_desc` varchar(200) DEFAULT NULL,
  `equipment_type` enum('Instrument','Apparatus','Samplingrod') NOT NULL DEFAULT 'Instrument',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_inused` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_instrument`
--

INSERT INTO `pts_mst_instrument` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `block_code`, `room_code`, `equipment_name`, `equipment_code`, `equipment_desc`, `equipment_type`, `status`, `is_inused`) VALUES
('2020-03-17 08:41:46', '', '2020-03-18 11:39:48', 'Superadmin', 1, 'Block-OSD', NULL, 'Test Rodeid2', 'Rode0012', 'test2', 'Samplingrod', 'active', b'0'),
('2020-04-01 10:28:18', '', '0000-00-00 00:00:00', '', 2, 'Block-OSD', NULL, 'Apparatus001', 'Apparatus001', 'testing.', 'Apparatus', 'active', b'0'),
('2020-04-01 10:50:13', '', '0000-00-00 00:00:00', '', 3, 'Block-OSD', NULL, 'Instrument 1', 'Instrument001', 'testing', 'Instrument', 'active', b'1'),
('2020-04-29 09:44:28', '', '0000-00-00 00:00:00', '', 4, 'Block-OSD', NULL, 'Test Rodeid3', 'Rode0013', 'test', 'Samplingrod', 'active', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_line_log`
--

DROP TABLE IF EXISTS `pts_mst_line_log`;
CREATE TABLE IF NOT EXISTS `pts_mst_line_log` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_code` varchar(50) DEFAULT NULL,
  `area_code` varchar(50) NOT NULL,
  `room_code` varchar(100) NOT NULL,
  `line_log_code` varchar(100) NOT NULL,
  `line_log_name` varchar(100) NOT NULL,
  `equipment_code` varchar(500) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_inused` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_line_log`
--

INSERT INTO `pts_mst_line_log` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `block_code`, `area_code`, `room_code`, `line_log_code`, `line_log_name`, `equipment_code`, `status`, `is_inused`) VALUES
('2020-04-09 08:32:18', '', '0000-00-00 00:00:00', '', 1, 'Block-OSD', 'A-006', 'TAA-78', 'Line001', 'Line1', 'SHSP-015,DT-017,HC061', 'active', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_master`
--

DROP TABLE IF EXISTS `pts_mst_master`;
CREATE TABLE IF NOT EXISTS `pts_mst_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `master_name` varchar(100) NOT NULL,
  `master_url` varchar(100) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL DEFAULT '10',
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_master`
--

INSERT INTO `pts_mst_master` (`id`, `master_name`, `master_url`, `status`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 'Room Configuration', 'manage_room', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(2, 'Room Log Mapping', 'manage_room_activity', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(3, 'Balance Master', 'balance_master', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(4, 'Bal. Calibration Frequency', 'balance_frequency_master', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(5, 'Tablet Tooling Master', 'tablet_tooling_master', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(6, 'Weight Standard ID Mapping', 'manage_id_standard_weight', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(7, 'Work Assign Master', 'work_assign_asper_rolebase', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(8, 'Workflow', 'workflow', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(9, 'Line Master', 'line_log_master', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(10, 'Instrument Master', 'instrument_master', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(11, 'Log Configuration Master', 'stage_master', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(12, 'Report Header/Footer Master', 'report_header', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(13, 'Block Master', 'manage_block', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(14, 'Sub Block Master', 'create_sub_block', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(15, 'Area Master', 'manage_area', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(16, 'Create Drain Point', 'manage_drain_point', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(17, 'Product Master', 'manage_product', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(18, 'User Configuration', 'manage_employee', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(19, 'Room Master', 'create_room', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(20, 'Create Plant', 'create_plant', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(21, 'Equipment Master', 'create_equipment', 'active', '2020-04-06 04:48:14', 10, NULL, NULL),
(22, 'Roles and Responsibility', 'user_management', 'active', '2020-04-06 04:48:14', 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_material_relocation`
--

DROP TABLE IF EXISTS `pts_mst_material_relocation`;
CREATE TABLE IF NOT EXISTS `pts_mst_material_relocation` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `material_name` varchar(100) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `material_code` varchar(100) NOT NULL,
  `material_desc` varchar(150) DEFAULT NULL,
  `material_type` varchar(150) DEFAULT NULL,
  `material_batch_no` varchar(100) DEFAULT NULL,
  `material_relocation_type` varchar(100) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pts_mst_material_relocation`
--

INSERT INTO `pts_mst_material_relocation` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `material_name`, `room_code`, `material_code`, `material_desc`, `material_type`, `material_batch_no`, `material_relocation_type`, `status`) VALUES
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 3, 'asdsabdhfh', 'TAA-78', 'asdsada', 'gcgcc', 'gcgycyc', 'ctycyttu', 'no', 'active'),
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 4, 'abc', 'TAA-58,TAA-59', 'abc001', 'I followed MrJSingh\'s advice and use ng-options, but the problem still happened. I added then an empty and it finally worked', 'A', 'A6474y774', 'no', 'active'),
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 5, 'ghsfdgafdashg', 'TAA-79', 'asdadas', 'ffxtfx', 'ftfxtfrx', 'cycxy', 'no', 'active'),
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 6, 'gdghadfghafdhg', 'TAA-79', 'dxfxyrf', 'dyxyxy', 'cgcgcgu', 'ctudtud', 'no', 'active'),
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 7, 'xyz', 'TAA-58,TAA-59', 'abc002', 'Hope I have understood your problem correctly. I feel that you dont have to write chose seperately', 'C', 'CFR93939', 'no', 'active'),
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 8, 'tvgvda', 'TAA-58,TAA-59', 'abc003', 'ImranMulani opened this issue on Apr 24, 2015 · 2 comments. Closed ... ng-change and ng-show not working with Angular Chosen ', 'E', 'EBD00002', 'no', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_report`
--

DROP TABLE IF EXISTS `pts_mst_report`;
CREATE TABLE IF NOT EXISTS `pts_mst_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_name` varchar(100) NOT NULL,
  `report_url` varchar(100) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL DEFAULT '10',
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_report`
--

INSERT INTO `pts_mst_report` (`id`, `report_name`, `report_url`, `status`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 'Audit Trail Report', 'audit_trail_report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(2, 'Process Room Log', 'process_room_log_report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(3, 'Portable Equipment Log', 'protable_equipment_report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(4, 'Line Log', 'line_log_Report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(5, 'E/A Friabilator Usage Log', 'equipment_apparatus_report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(6, 'Swab Sample Record', 'swab_sample_report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(7, 'Instrument Log Register', 'instrument_log_report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(8, 'VS & DC Usages Log', 'vertical_sampler_report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(9, 'Vacuum Cleaner Logbook', 'vaccume_cleaner_log_report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(10, 'Pre-Filter Cleaning Record of LAF', 'pre_filter_report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(11, 'LAF Pressure Differential Record', 'laf_pressure_differential_report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(12, 'Return Air Filter Cleaning Record', 'air_filter_cleaning_report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(13, 'Balance Calibration Record', 'balance_calibration_report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(14, 'Environmental Condition/Pressure Differential Record', 'environmental_pressure_report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(15, 'Material Retrieval and Relocation Record for Cold Room', 'material_retrieval_relocation', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(16, 'Tablet Tooling Log Card', 'tablet_tooling_report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(17, 'E/A Leak Test Log', 'equipment_apparatus_leaktest_report', 'active', '2020-04-06 05:03:20', 10, NULL, NULL),
(18, 'Active & Inactive User List', 'active_inactive_user_list', 'active', '2020-06-08 05:07:23', 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_report_footer`
--

DROP TABLE IF EXISTS `pts_mst_report_footer`;
CREATE TABLE IF NOT EXISTS `pts_mst_report_footer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_id` int(11) DEFAULT NULL,
  `footer_name` varchar(200) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_report_footer`
--

INSERT INTO `pts_mst_report_footer` (`id`, `report_id`, `footer_name`, `created_by`, `created_on`, `updated_by`, `updated_on`, `status`) VALUES
(1, 2, 'OP001568-06/00/05-14', 10, '2020-05-12 05:00:12', 10, '2020-05-12 05:28:41', 'active'),
(2, 3, 'OP001568-06/00/05-14', 10, '2020-05-12 05:31:10', NULL, NULL, 'active'),
(3, 4, 'OP001568-06/00/05-14', 10, '2020-05-12 05:31:23', NULL, NULL, 'active'),
(4, 16, 'OP001568-06/00/05-14', 10, '2020-05-12 05:32:18', NULL, NULL, 'active'),
(5, 5, 'OP001568-06/00/05-14', 10, '2020-05-12 05:33:12', NULL, NULL, 'active'),
(6, 13, 'OP001568-06/00/05-14', 10, '2020-05-21 09:21:03', NULL, NULL, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_report_header`
--

DROP TABLE IF EXISTS `pts_mst_report_header`;
CREATE TABLE IF NOT EXISTS `pts_mst_report_header` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_no` varchar(100) NOT NULL,
  `version_no` varchar(100) NOT NULL,
  `effective_date` varchar(100) NOT NULL,
  `document_no` varchar(100) NOT NULL,
  `status` enum('active','inactive','default') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_report_header`
--

INSERT INTO `pts_mst_report_header` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `form_no`, `version_no`, `effective_date`, `document_no`, `status`) VALUES
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 1, 'Form042371', '1.0', '2019-07-08', 'SOP019972', 'default');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_role_level`
--

DROP TABLE IF EXISTS `pts_mst_role_level`;
CREATE TABLE IF NOT EXISTS `pts_mst_role_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleid` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(250) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pts_mst_role_level`
--

INSERT INTO `pts_mst_role_level` (`id`, `roleid`, `level`, `created_on`, `created_by`, `modified_on`, `modified_by`, `isactive`) VALUES
(1, 1, 1, '2020-01-31 06:55:33', 'SuperAdmin', NULL, NULL, b'1'),
(2, 2, 2, '2020-01-31 06:55:33', 'SuperAdmin', NULL, NULL, b'1'),
(3, 4, 3, '2020-01-31 06:55:33', 'SuperAdmin', NULL, NULL, b'1'),
(4, 11, 4, '2020-01-31 06:55:33', 'SuperAdmin', NULL, NULL, b'1'),
(5, 3, 5, '2020-01-31 06:55:33', 'SuperAdmin', NULL, NULL, b'1'),
(6, 5, 6, '2020-01-31 06:55:33', 'SuperAdmin', NULL, NULL, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_role_privilege`
--

DROP TABLE IF EXISTS `pts_mst_role_privilege`;
CREATE TABLE IF NOT EXISTS `pts_mst_role_privilege` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_type` enum('master','log','report') DEFAULT NULL,
  `module_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `access_type` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_by` varchar(100) NOT NULL DEFAULT 'Admin',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=137 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_role_privilege`
--

INSERT INTO `pts_mst_role_privilege` (`id`, `module_type`, `module_id`, `role_id`, `access_type`, `created_by`, `created_on`) VALUES
(1, 'log', 16, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(2, 'log', 17, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(3, 'log', 30, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(4, 'log', 18, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(5, 'log', 19, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(6, 'log', 20, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(7, 'log', 21, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(8, 'log', 22, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(9, 'log', 23, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(10, 'log', 24, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(11, 'log', 25, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(12, 'log', 26, 1, 'NO', 'Admin', '2020-06-04 17:12:19'),
(13, 'log', 27, 1, 'NO', 'Admin', '2020-06-04 17:12:19'),
(14, 'log', 28, 1, 'NO', 'Admin', '2020-06-04 17:12:19'),
(15, 'log', 29, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(16, 'log', 31, 1, 'NO', 'Admin', '2020-06-04 17:12:19'),
(17, 'log', 16, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(18, 'log', 17, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(19, 'log', 30, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(20, 'log', 18, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(21, 'log', 19, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(22, 'log', 20, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(23, 'log', 21, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(24, 'log', 22, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(25, 'log', 23, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(26, 'log', 24, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(27, 'log', 25, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(28, 'log', 26, 2, 'NO', 'Admin', '2020-06-04 17:12:19'),
(29, 'log', 27, 2, 'NO', 'Admin', '2020-06-04 17:12:19'),
(30, 'log', 28, 2, 'NO', 'Admin', '2020-06-04 17:12:19'),
(31, 'log', 29, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(32, 'log', 31, 2, 'NO', 'Admin', '2020-06-04 17:12:19'),
(33, 'log', 16, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(34, 'log', 17, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(35, 'log', 30, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(36, 'log', 18, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(37, 'log', 19, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(38, 'log', 20, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(39, 'log', 21, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(40, 'log', 22, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(41, 'log', 23, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(42, 'log', 24, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(43, 'log', 25, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(44, 'log', 26, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(45, 'log', 27, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(46, 'log', 28, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(47, 'log', 29, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(48, 'log', 31, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(49, 'log', 16, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(50, 'log', 17, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(51, 'log', 30, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(52, 'log', 18, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(53, 'log', 19, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(54, 'log', 20, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(55, 'log', 21, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(56, 'log', 22, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(57, 'log', 23, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(58, 'log', 24, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(59, 'log', 25, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(60, 'log', 26, 4, 'NO', 'Admin', '2020-06-04 17:12:19'),
(61, 'log', 27, 4, 'NO', 'Admin', '2020-06-04 17:12:19'),
(62, 'log', 28, 4, 'NO', 'Admin', '2020-06-04 17:12:19'),
(63, 'log', 29, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(64, 'log', 31, 4, 'NO', 'Admin', '2020-06-04 17:12:19'),
(65, 'report', 2, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(66, 'report', 1, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(67, 'report', 3, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(68, 'report', 4, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(69, 'report', 5, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(70, 'report', 6, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(71, 'report', 7, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(72, 'report', 8, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(73, 'report', 9, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(74, 'report', 10, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(75, 'report', 11, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(76, 'report', 12, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(77, 'report', 13, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(78, 'report', 14, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(79, 'report', 15, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(80, 'report', 16, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(81, 'report', 17, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(82, 'report', 18, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(83, 'report', 1, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(84, 'report', 2, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(85, 'report', 3, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(86, 'report', 4, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(87, 'report', 5, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(88, 'report', 6, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(89, 'report', 7, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(90, 'report', 8, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(91, 'report', 9, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(92, 'report', 10, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(93, 'report', 11, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(94, 'report', 12, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(95, 'report', 13, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(96, 'report', 14, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(97, 'report', 15, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(98, 'report', 16, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(99, 'report', 17, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(100, 'report', 18, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(101, 'report', 1, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(102, 'report', 2, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(103, 'report', 3, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(104, 'report', 4, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(105, 'report', 5, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(106, 'report', 6, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(107, 'report', 7, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(108, 'report', 8, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(109, 'report', 9, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(110, 'report', 10, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(111, 'report', 11, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(112, 'report', 12, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(113, 'report', 13, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(114, 'report', 14, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(115, 'report', 15, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(116, 'report', 16, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(117, 'report', 17, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(118, 'report', 18, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(119, 'report', 1, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(120, 'report', 2, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(121, 'report', 3, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(122, 'report', 4, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(123, 'report', 5, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(124, 'report', 6, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(125, 'report', 7, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(126, 'report', 8, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(127, 'report', 9, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(128, 'report', 10, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(129, 'report', 11, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(130, 'report', 12, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(131, 'report', 13, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(132, 'report', 14, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(133, 'report', 15, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(134, 'report', 16, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(135, 'report', 17, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(136, 'report', 18, 4, 'YES', 'Admin', '2020-06-09 13:04:30');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_role_workassign_to_mult_roles`
--

DROP TABLE IF EXISTS `pts_mst_role_workassign_to_mult_roles`;
CREATE TABLE IF NOT EXISTS `pts_mst_role_workassign_to_mult_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleid` int(11) DEFAULT NULL,
  `workassign_to_roleid` int(11) DEFAULT NULL,
  `prty_btn_text` enum('on','off') NOT NULL DEFAULT 'off',
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pts_mst_role_workassign_to_mult_roles`
--

INSERT INTO `pts_mst_role_workassign_to_mult_roles` (`id`, `roleid`, `workassign_to_roleid`, `prty_btn_text`, `isactive`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES
(1, 1, 1, 'off', b'1', '2020-01-31 09:31:18', 'SuperAdmin', NULL, NULL),
(2, 1, 2, 'off', b'1', '2020-01-31 09:31:18', 'SuperAdmin', NULL, NULL),
(3, 1, 4, 'off', b'1', '2020-01-31 09:31:18', 'SuperAdmin', NULL, NULL),
(4, 1, 11, 'off', b'1', '2020-01-31 09:31:18', 'SuperAdmin', NULL, NULL),
(8, 4, 2, 'off', b'1', '2020-01-31 09:31:18', 'SuperAdmin', NULL, NULL),
(9, 4, 3, 'off', b'1', '2020-01-31 09:31:18', 'SuperAdmin', NULL, NULL),
(10, 11, 11, 'off', b'1', '2020-01-31 09:31:18', 'SuperAdmin', NULL, NULL),
(11, 3, 3, 'off', b'1', '2020-01-31 09:31:18', 'SuperAdmin', NULL, NULL),
(15, 2, 2, 'off', b'1', '2020-05-30 01:01:25', 'Super Admin', NULL, NULL),
(16, 2, 3, 'off', b'1', '2020-05-30 01:01:25', 'Super Admin', NULL, NULL),
(17, 5, 5, 'off', b'1', '2020-06-03 12:16:19', 'Super Admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_stages`
--

DROP TABLE IF EXISTS `pts_mst_stages`;
CREATE TABLE IF NOT EXISTS `pts_mst_stages` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log` varchar(50) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `field_value` varchar(100) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_stages`
--

INSERT INTO `pts_mst_stages` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `log`, `field_name`, `field_value`, `status`) VALUES
('2020-04-01 10:29:06', 'Super Admin', '0000-00-00 00:00:00', '', 4, 'Equipment/Apparatus Log Register (Friabilator)', 'Stage', 'stage 1', 'active'),
('2020-04-01 10:29:20', 'Super Admin', '0000-00-00 00:00:00', '', 5, 'Equipment/Apparatus Log Register (Friabilator)', 'Stage', 'stage 2', 'active'),
('2020-04-01 10:29:45', 'Super Admin', '0000-00-00 00:00:00', '', 6, 'Equipment/Apparatus Log Register (Friabilator)', 'cleaning_verification', 'ok', 'active'),
('2020-04-01 10:29:59', 'Super Admin', '0000-00-00 00:00:00', '', 7, 'Equipment/Apparatus Log Register (Friabilator)', 'cleaning_verification', 'notok', 'active'),
('2020-04-01 10:43:48', 'Super Admin', '0000-00-00 00:00:00', '', 8, 'Equipment/Apparatus Log Register (Friabilator)', 'Stage', 'stage 3', 'active'),
('2020-04-01 10:46:08', 'Super Admin', '0000-00-00 00:00:00', '', 9, 'Instrument Log Register', 'Stage', 'stage 1', 'active'),
('2020-04-01 10:47:06', 'Super Admin', '0000-00-00 00:00:00', '', 10, 'Instrument Log Register', 'Stage', 'stage 2', 'active'),
('2020-04-01 10:51:02', 'Super Admin', '0000-00-00 00:00:00', '', 11, 'Instrument Log Register', 'ResultLeft', 'ok', 'active'),
('2020-04-01 10:51:15', 'Super Admin', '0000-00-00 00:00:00', '', 12, 'Instrument Log Register', 'ResultLeft', 'notok', 'active'),
('2020-04-01 10:51:26', 'Super Admin', '0000-00-00 00:00:00', '', 13, 'Instrument Log Register', 'ResultRight', 'ok', 'active'),
('2020-04-01 10:51:37', 'Super Admin', '0000-00-00 00:00:00', '', 14, 'Instrument Log Register', 'ResultRight', 'notok', 'active'),
('2020-04-02 05:34:48', 'Super Admin', '2020-04-02 05:35:28', 'Super Admin', 15, 'Vertical Sampler and Dies Cleaning Usages Log', 'Activity', 'Type A', 'active'),
('2020-04-02 05:35:47', 'Super Admin', '0000-00-00 00:00:00', '', 16, 'Vertical Sampler and Dies Cleaning Usages Log', 'Activity', 'Type B', 'inactive'),
('2020-04-02 05:42:36', 'Super Admin', '0000-00-00 00:00:00', '', 17, 'Vertical Sampler and Dies Cleaning Usages Log', 'Activity', 'Type D', 'inactive'),
('2020-04-02 06:21:08', 'Super Admin', '0000-00-00 00:00:00', '', 18, 'Vertical Sampler and Dies Cleaning Usages Log', 'Activity', 'Type B', 'active'),
('2020-04-29 10:19:40', 'Super Admin', '0000-00-00 00:00:00', '', 19, 'Vertical Sampler and Dies Cleaning Usages Log', 'Activity', 'Type C', 'active'),
('2020-05-04 09:29:33', 'Super Admin', '0000-00-00 00:00:00', '', 20, 'Equipment/Apparatus Log Register (Leak Test)', 'Stage', 'stage 1', 'active'),
('2020-05-04 09:29:47', 'Super Admin', '0000-00-00 00:00:00', '', 21, 'Equipment/Apparatus Log Register (Leak Test)', 'Stage', 'stage 2', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_stop_reason`
--

DROP TABLE IF EXISTS `pts_mst_stop_reason`;
CREATE TABLE IF NOT EXISTS `pts_mst_stop_reason` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `stop_reason` varchar(500) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pts_mst_stop_reason`
--

INSERT INTO `pts_mst_stop_reason` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `stop_reason`, `status`) VALUES
('2020-04-02 07:02:27', 'Super Admin', NULL, NULL, 1, 'Reason 1', 'active'),
('2020-04-02 07:02:27', 'Super Admin', NULL, NULL, 2, 'Reason 2', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_sys_id`
--

DROP TABLE IF EXISTS `pts_mst_sys_id`;
CREATE TABLE IF NOT EXISTS `pts_mst_sys_id` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `status` enum('active','inactive') DEFAULT 'active',
  `room_name` varchar(60) DEFAULT NULL,
  `room_code` varchar(20) DEFAULT NULL,
  `area_id` varchar(60) DEFAULT NULL,
  `room_id` int(10) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL,
  `device_id` varchar(30) DEFAULT NULL,
  `logger_id` varchar(50) DEFAULT NULL,
  `gauge_id` varchar(50) DEFAULT NULL,
  `room_type` enum('room','corridor','IPQA') NOT NULL DEFAULT 'room',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pts_mst_sys_id`
--

INSERT INTO `pts_mst_sys_id` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `status`, `room_name`, `room_code`, `area_id`, `room_id`, `created_date`, `device_id`, `logger_id`, `gauge_id`, `room_type`) VALUES
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 2, 'inactive', 'Injectibles room 2', 'PMA-97', 'A_195', 43, 1580098502, '59.144.179.206', 'LOGGER001,LOGGER002,LOGGER003,LOGGER004', 'GAUGE001,GAUGE002,GAUGE003,GAUGE004', 'room'),
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 3, 'inactive', 'Injectibles room 3', 'PMA-51', 'A_195', 44, 1580098503, '59.144.179.206', 'LOGGER001,LOGGER002,LOGGER003,LOGGER004', 'GAUGE001,GAUGE002,GAUGE003,GAUGE004', 'room'),
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 4, 'inactive', 'Injectibles room 4', 'PMA-102', 'A_195', 45, 1580098504, '59.144.179.206', 'LOGGER001,LOGGER002,LOGGER003,LOGGER004', 'GAUGE001,GAUGE002,GAUGE003,GAUGE004', 'room'),
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 5, 'inactive', 'Injectibles room 5', 'PMA-57', 'A_195', 46, 1580098505, '59.144.179.206', 'LOGGER001,LOGGER002,LOGGER003,LOGGER004', 'GAUGE001,GAUGE002,GAUGE003,GAUGE004', 'room'),
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 6, 'inactive', 'Injectibles room 6', 'PMA-101', 'A_195', 47, 1580098506, '59.144.179.206', 'LOGGER001,LOGGER002,LOGGER003,LOGGER004', 'GAUGE001,GAUGE002,GAUGE003,GAUGE004', 'room'),
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 7, 'inactive', 'Injectibles room 7', 'PMA-158', 'A_195', 48, 1580098507, '59.144.179.206', 'LOGGER001,LOGGER002,LOGGER003,LOGGER004', 'GAUGE001,GAUGE002,GAUGE003,GAUGE004', 'room'),
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 8, 'inactive', 'Injectibles room 8', 'PMA-159', 'A_195', 49, 1580098508, '59.144.179.206', 'LOGGER001,LOGGER002,LOGGER003,LOGGER004', 'GAUGE001,GAUGE002,GAUGE003,GAUGE004', 'room'),
('2020-03-17 08:41:47', '', '2020-03-18 10:16:23', 'Superadmin', 9, 'inactive', 'Gel Mass Area room1', 'TAA-51,TAA-52', 'A-001', 50, 1580098509, '59.144.179.214', 'LOGGER001,LOGGER002,LOGGER003,LOGGER004', 'GAUGE001,GAUGE002,GAUGE003,GAUGE004', 'room'),
('2020-03-17 08:41:47', '', '2020-03-18 10:16:15', 'Superadmin', 10, 'inactive', 'Gel Mass Area room2', 'TAA-52,TAA-53', 'A-001', 51, 1580098510, '59.144.179.214', 'LOGGER001,LOGGER002,LOGGER003,LOGGER004', 'GAUGE001,GAUGE002,GAUGE003,GAUGE004', 'room'),
('2020-03-17 08:41:47', '', '2020-03-18 10:16:28', 'Superadmin', 14, 'inactive', 'FBE Room3', 'TAA-80', 'A-003', 55, 1580098514, '59.144.179.219', 'LOGGER001,LOGGER002,LOGGER003,LOGGER004', 'GAUGE001,GAUGE002,GAUGE003,GAUGE004', 'room'),
('2020-03-17 08:41:47', '', '2020-03-18 10:00:50', 'Superadmin', 24, 'inactive', 'IPQA', 'IPQA', 'A-006', 56, 1585820978, '10.2.3.4', 'd', 'm', 'IPQA'),
('2020-03-17 08:41:47', '', '2020-03-18 10:04:06', 'Superadmin', 26, 'active', 'Room 1 for coating ', 'TAA-78', 'A-006', 53, 1585907742, '10.240.240.240', 'log id 1', 'mag id 1', 'corridor'),
('2020-04-02 09:37:55', '', '2020-04-02 15:24:20', 'Superadmin', 27, 'inactive', 'Injectibles room 1', 'PMA-64', 'A_195', 42, 1585820729, '6767.131.133', '', '', 'room'),
('2020-04-02 09:55:13', '', '2020-04-02 15:26:59', 'Superadmin', 28, 'inactive', 'Injectibles room 1', 'PMA-64', 'A_195', 42, 1585821313, '6767.131.133', 'L1,L2', 'M1,M2', 'room'),
('2020-04-02 09:57:50', '', '2020-05-02 13:59:15', 'Superadmin', 29, 'inactive', 'Injectibles room 1', 'PMA-64', 'A_195', 42, 1585821470, '6767.131.133', ',', ',', 'room');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_tablet_tooling_log`
--

DROP TABLE IF EXISTS `pts_mst_tablet_tooling_log`;
CREATE TABLE IF NOT EXISTS `pts_mst_tablet_tooling_log` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(100) NOT NULL,
  `supplier` varchar(100) NOT NULL,
  `upper_punch_qty` float NOT NULL,
  `lower_punch_qty` float NOT NULL,
  `year` int(11) NOT NULL,
  `dimension` varchar(100) NOT NULL,
  `shape` varchar(100) NOT NULL,
  `machine` varchar(100) NOT NULL,
  `die_quantity` float NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_tablet_tooling_log`
--

INSERT INTO `pts_mst_tablet_tooling_log` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `product_code`, `supplier`, `upper_punch_qty`, `lower_punch_qty`, `year`, `dimension`, `shape`, `machine`, `die_quantity`, `status`) VALUES
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 1, 'PL002', 'Bhupendra', 5000, 2000, 2020, '2D', 'L', 'EQUIP001', 25, 'active'),
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 2, 'PL001', 'Bhupendra', 2000, 500, 2020, '3D', 'Cone', 'EQUIP002', 50, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_user_mgmt`
--

DROP TABLE IF EXISTS `pts_mst_user_mgmt`;
CREATE TABLE IF NOT EXISTS `pts_mst_user_mgmt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `type_id` varchar(200) DEFAULT NULL,
  `module_type` varchar(50) NOT NULL,
  `is_create` int(11) NOT NULL DEFAULT '0',
  `is_edit` int(11) NOT NULL DEFAULT '0',
  `is_view` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1342 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_user_mgmt`
--

INSERT INTO `pts_mst_user_mgmt` (`id`, `user_id`, `module_id`, `room_code`, `type_id`, `module_type`, `is_create`, `is_edit`, `is_view`, `created_on`, `created_by`, `modified_on`, `modified_by`, `status`) VALUES
(11, 33, 16, 'TAA-78', 'Type A Cleaning,Type B Cleaning,Type D Cleaning,Production,Sampling', 'log', 1, 1, 1, '2020-04-21 10:17:37', 10, NULL, NULL, 'active'),
(8, 33, 17, 'TAA-78', 'Type A Cleaning,Type B Cleaning,Type D Cleaning,Maintenance,Production,Sampling', 'log', 1, 1, 1, '2020-04-21 09:38:33', 10, NULL, NULL, 'active'),
(4, 33, 18, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-04-21 09:34:08', 10, NULL, NULL, 'active'),
(7, 33, 21, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-04-21 09:38:25', 10, NULL, NULL, 'active'),
(6, 33, 22, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-04-21 09:38:13', 10, NULL, NULL, 'active'),
(215, 33, 17, NULL, NULL, 'report', 0, 0, 0, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(214, 33, 16, NULL, NULL, 'report', 0, 0, 0, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(213, 33, 1, NULL, NULL, 'report', 0, 0, 1, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(212, 33, 2, NULL, NULL, 'report', 0, 1, 1, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(211, 33, 4, NULL, NULL, 'report', 0, 1, 1, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(210, 33, 3, NULL, NULL, 'report', 0, 1, 1, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(209, 33, 8, NULL, NULL, 'report', 0, 0, 0, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(208, 33, 5, NULL, NULL, 'report', 0, 0, 0, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(207, 33, 6, NULL, NULL, 'report', 0, 0, 0, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(206, 33, 7, NULL, NULL, 'report', 0, 0, 0, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(205, 33, 10, NULL, NULL, 'report', 0, 0, 0, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(204, 33, 9, NULL, NULL, 'report', 0, 0, 0, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(203, 33, 13, NULL, NULL, 'report', 0, 0, 0, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(202, 33, 11, NULL, NULL, 'report', 0, 0, 0, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(201, 33, 12, NULL, NULL, 'report', 0, 0, 0, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(199, 33, 15, NULL, NULL, 'report', 0, 1, 1, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(200, 33, 14, NULL, NULL, 'report', 0, 1, 1, '2020-04-21 13:28:27', 33, NULL, NULL, 'active'),
(1250, 33, 22, NULL, NULL, 'master', 0, 0, 1, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1249, 33, 21, NULL, NULL, 'master', 0, 0, 1, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1248, 33, 2, NULL, NULL, 'master', 0, 1, 1, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1247, 33, 1, NULL, NULL, 'master', 1, 1, 1, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1246, 33, 3, NULL, NULL, 'master', 1, 0, 0, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1245, 33, 4, NULL, NULL, 'master', 0, 1, 1, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1244, 33, 6, NULL, NULL, 'master', 1, 0, 0, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1243, 33, 5, NULL, NULL, 'master', 1, 0, 0, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1242, 33, 7, NULL, NULL, 'master', 1, 0, 0, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1241, 33, 8, NULL, NULL, 'master', 1, 0, 0, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1240, 33, 10, NULL, NULL, 'master', 1, 0, 0, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1239, 33, 9, NULL, NULL, 'master', 1, 0, 0, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1238, 33, 11, NULL, NULL, 'master', 1, 0, 0, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1237, 33, 12, NULL, NULL, 'master', 1, 0, 0, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1236, 33, 14, NULL, NULL, 'master', 0, 1, 1, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1235, 33, 13, NULL, NULL, 'master', 0, 1, 1, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1234, 33, 16, NULL, NULL, 'master', 0, 1, 1, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1233, 33, 15, NULL, NULL, 'master', 1, 0, 0, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1232, 33, 18, NULL, NULL, 'master', 1, 0, 0, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1231, 33, 17, NULL, NULL, 'master', 1, 0, 0, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1229, 33, 20, NULL, NULL, 'master', 1, 0, 0, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1008, 33, 23, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-04-22 11:21:17', 33, NULL, NULL, 'active'),
(1230, 33, 19, NULL, NULL, 'master', 1, 0, 0, '2020-04-22 12:19:21', 33, NULL, NULL, 'active'),
(1251, 33, 25, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-04-30 12:24:00', 10, NULL, NULL, 'active'),
(1252, 34, 25, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-04-30 12:24:36', 10, NULL, NULL, 'active'),
(1273, 34, 16, 'TAA-78', 'Type A Cleaning,Type B Cleaning,Type C Cleaning,Type D Cleaning,Maintenance,Production,Sampling', 'log', 1, 1, 1, '2020-05-07 10:40:22', 10, NULL, NULL, 'active'),
(1258, 34, 17, 'TAA-78', 'Type A Cleaning,Type B Cleaning,Production', 'log', 1, 1, 1, '2020-05-01 12:20:08', 10, NULL, NULL, 'active'),
(1257, 47, 16, 'TAA-78', 'Type A Cleaning,Type B Cleaning', 'log', 1, 1, 1, '2020-04-30 13:50:25', 10, NULL, NULL, 'active'),
(1277, 34, 20, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-09 13:12:45', 10, NULL, NULL, 'active'),
(1260, 33, 26, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-04 12:40:08', 10, NULL, NULL, 'active'),
(1261, 34, 26, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-04 12:40:35', 10, NULL, NULL, 'active'),
(1262, 34, 27, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-04 12:47:47', 10, NULL, NULL, 'active'),
(1263, 34, 31, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-04 12:48:30', 10, NULL, NULL, 'active'),
(1264, 33, 27, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-04 12:48:56', 10, NULL, NULL, 'active'),
(1265, 33, 31, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-04 12:49:05', 10, NULL, NULL, 'active'),
(1266, 34, 18, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-05 09:37:31', 10, '2020-06-09 08:20:25', 10, 'inactive'),
(1267, 34, 21, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-05 11:23:53', 10, NULL, NULL, 'active'),
(1268, 34, 22, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-05 13:11:25', 10, NULL, NULL, 'active'),
(1269, 33, 28, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-05 15:02:16', 10, NULL, NULL, 'active'),
(1270, 34, 28, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-05 15:02:36', 10, NULL, NULL, 'active'),
(1271, 33, 29, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-06 16:14:43', 10, NULL, NULL, 'active'),
(1272, 34, 29, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-06 16:15:10', 10, NULL, NULL, 'active'),
(1274, 33, 30, 'TAA-78', 'Type A Cleaning,Type B Cleaning,Type C Cleaning,Type D Cleaning,Maintenance,Production,Sampling', 'log', 1, 1, 1, '2020-05-07 14:14:33', 10, NULL, NULL, 'active'),
(1275, 34, 30, 'TAA-78', 'Type A Cleaning,Type B Cleaning,Type C Cleaning,Type D Cleaning,Maintenance,Production,Sampling', 'log', 1, 1, 1, '2020-05-07 14:15:17', 10, NULL, NULL, 'active'),
(1276, 33, 20, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-09 13:12:22', 10, NULL, NULL, 'active'),
(1278, 47, 17, 'TAA-78', 'Type A Cleaning,Type B Cleaning,Type C Cleaning,Type D Cleaning,Maintenance,Production,Sampling', 'log', 1, 1, 1, '2020-05-18 09:41:25', 10, NULL, NULL, 'active'),
(1279, 41, 16, 'TAA-78', 'Type A Cleaning,Type B Cleaning,Type C Cleaning,Type D Cleaning,Maintenance,Production,Sampling', 'log', 1, 1, 1, '2020-05-22 10:39:25', 10, NULL, NULL, 'active'),
(1280, 33, 19, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-27 11:08:06', 10, NULL, NULL, 'active'),
(1281, 34, 19, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-27 11:44:42', 10, NULL, NULL, 'active'),
(1282, 47, 27, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-27 16:04:06', 10, NULL, NULL, 'active'),
(1283, 47, 29, 'TAA-78', NULL, 'log', 1, 1, 1, '2020-05-30 06:26:27', 10, NULL, NULL, 'active'),
(1337, 52, 14, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1338, 52, 15, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1335, 52, 12, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1336, 52, 13, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1333, 52, 10, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1334, 52, 11, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1332, 52, 9, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1331, 52, 8, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1330, 52, 7, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1329, 52, 6, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1328, 52, 5, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1327, 52, 4, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1326, 52, 3, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1325, 52, 2, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1324, 52, 29, 'TAA-78', '', 'log', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1323, 52, 25, 'TAA-78', '', 'log', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1322, 52, 24, 'TAA-78', '', 'log', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1321, 52, 23, 'TAA-78', '', 'log', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1320, 52, 22, 'TAA-78', '', 'log', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1319, 52, 21, 'TAA-78', '', 'log', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1318, 52, 20, 'TAA-78', '', 'log', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1317, 52, 19, 'TAA-78', '', 'log', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1316, 52, 18, 'TAA-78', '', 'log', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1315, 52, 30, 'TAA-78', 'Type A Cleaning,Type B Cleaning,Type D Cleaning,Maintenance,Production,Sampling', 'log', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1314, 52, 17, 'TAA-78', 'Type A Cleaning,Type B Cleaning,Type D Cleaning,Maintenance,Production,Sampling', 'log', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1313, 52, 16, 'TAA-78', 'Type A Cleaning,Type B Cleaning,Type D Cleaning,Maintenance,Production,Sampling', 'log', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1339, 52, 16, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1340, 52, 17, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active'),
(1341, 52, 18, 'TAA-78', '', 'report', 1, 1, 1, '2020-06-09 15:01:22', 10, NULL, NULL, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_vertical_sampler`
--

DROP TABLE IF EXISTS `pts_mst_vertical_sampler`;
CREATE TABLE IF NOT EXISTS `pts_mst_vertical_sampler` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sample_rod_id` varchar(100) NOT NULL,
  `sample_rod_desc` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_vertical_sampler`
--

INSERT INTO `pts_mst_vertical_sampler` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `sample_rod_id`, `sample_rod_desc`, `status`) VALUES
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 1, 'sampler001', 'test', '1'),
('2020-03-17 08:41:47', '', '0000-00-00 00:00:00', '', 2, 'sampler002', 'test2', '1');

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_vertical_sampler_activity`
--

DROP TABLE IF EXISTS `pts_mst_vertical_sampler_activity`;
CREATE TABLE IF NOT EXISTS `pts_mst_vertical_sampler_activity` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(80) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pts_mst_vertical_sampler_activity`
--

INSERT INTO `pts_mst_vertical_sampler_activity` (`created_on`, `created_by`, `modified_on`, `modified_by`, `id`, `activity_name`, `status`) VALUES
('2020-03-17 08:41:48', '', '0000-00-00 00:00:00', '', 1, 'Type A Cleaning', 'active'),
('2020-03-17 08:41:48', '', '0000-00-00 00:00:00', '', 2, 'Type B Cleaning', 'active'),
('2020-03-17 08:41:48', '', '0000-00-00 00:00:00', '', 3, 'Type Cc Cleaning', 'active'),
('2020-03-17 08:41:48', '', '0000-00-00 00:00:00', '', 4, 'Type D Cleaning', 'active'),
('2020-03-17 08:41:48', '', '0000-00-00 00:00:00', '', 5, 'Maintenance', 'active'),
('2020-03-17 08:41:48', '', '0000-00-00 00:00:00', '', 6, 'Production', 'active'),
('2020-03-17 08:41:48', '', '0000-00-00 00:00:00', '', 7, 'Sampling', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_activity_approval_log`
--

DROP TABLE IF EXISTS `pts_trn_activity_approval_log`;
CREATE TABLE IF NOT EXISTS `pts_trn_activity_approval_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `usr_activity_log_id` int(11) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `approval_status` enum('approve','reject','N/A') DEFAULT 'N/A',
  `workflow_type` varchar(50) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `activity_remarks` varchar(240) DEFAULT NULL,
  `role_id` int(10) DEFAULT NULL,
  `activity_time` int(10) DEFAULT NULL,
  `reason` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pts_trn_activity_approval_log`
--

INSERT INTO `pts_trn_activity_approval_log` (`id`, `usr_activity_log_id`, `status`, `approval_status`, `workflow_type`, `user_id`, `user_name`, `user_email`, `activity_remarks`, `role_id`, `activity_time`, `reason`, `created_on`) VALUES
(1, 1, 'active', 'N/A', 'start', 33, 'sid', 'sid@sunpharma.in', 'test', 1, 1591618760, NULL, '2020-06-08 12:19:20'),
(2, 1, 'active', 'N/A', 'stop', 33, 'sid', 'sid@sunpharma.in', 'test', 1, 1591618786, '', '2020-06-08 12:19:46'),
(3, 2, 'active', 'N/A', 'start', 33, 'sid', 'sid@sunpharma.in', 'fdfdfdfdf', 1, 1591690894, NULL, '2020-06-09 08:21:34'),
(4, 2, 'active', 'approve', 'stop', 34, 'abdul ', 'abdul@sunpharma.in', 'test', 2, 1591690936, NULL, '2020-06-09 08:22:16');

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_activity_log`
--

DROP TABLE IF EXISTS `pts_trn_activity_log`;
CREATE TABLE IF NOT EXISTS `pts_trn_activity_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `usr_activity_log_id` int(11) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `activity_status` varchar(50) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `remarks` varchar(240) DEFAULT NULL,
  `role_id` int(10) DEFAULT NULL,
  `activity_time` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_balance_calibration`
--

DROP TABLE IF EXISTS `pts_trn_balance_calibration`;
CREATE TABLE IF NOT EXISTS `pts_trn_balance_calibration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `balance_id` int(11) DEFAULT NULL,
  `balance_no` varchar(50) DEFAULT NULL,
  `frequency_id` int(11) DEFAULT NULL,
  `standerd_wt` float DEFAULT NULL,
  `id_no_of_st_wt` varchar(50) DEFAULT NULL,
  `id_no_st_wt_index` varchar(50) DEFAULT NULL,
  `oberved_wt` float DEFAULT NULL,
  `diff` float DEFAULT NULL,
  `sprit_level_status` enum('ok','notok','NA') DEFAULT 'NA',
  `time_status` enum('ok','notok','NA') DEFAULT 'NA',
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pts_trn_balance_calibration`
--

INSERT INTO `pts_trn_balance_calibration` (`id`, `doc_no`, `room_code`, `balance_id`, `balance_no`, `frequency_id`, `standerd_wt`, `id_no_of_st_wt`, `id_no_st_wt_index`, `oberved_wt`, `diff`, `sprit_level_status`, `time_status`, `done_by_user_id`, `done_by_user_name`, `done_by_role_id`, `done_by_email`, `done_by_remark`, `created_on`, `status`, `is_in_workflow`, `updated_on`, `updated_by`, `update_remark`) VALUES
(6, 'DOC2481', 'TAA-78', 3, 'test bal 1', 1, 23, '11', NULL, 67, 44, 'ok', 'notok', 33, 'sid', 1, 'sid@sunpharma.in', 'test balance calibration.', '2020-04-14 14:01:02', 'active', 'no', NULL, NULL, NULL),
(7, 'DOC2482', 'TAA-78', 3, 'test bal 1', 2, 23, '11', NULL, 50, 27, 'notok', 'ok', 33, 'sid', 1, 'sid@sunpharma.in', 'test Balance 2', '2020-04-14 14:57:27', 'active', 'no', NULL, NULL, NULL),
(8, 'DOC2482', 'TAA-78', 3, 'test bal 1', 2, 300, '9,10', NULL, 500, 200, 'notok', 'ok', 33, 'sid', 1, 'sid@sunpharma.in', 'test Balance 2', '2020-04-14 14:57:27', 'active', 'no', NULL, NULL, NULL),
(9, 'DOC2496', 'TAA-78', 3, 'test bal 1', 1, 23, '11', NULL, 50, 27, 'notok', 'ok', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-04-15 21:20:13', 'active', 'no', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_batchin`
--

DROP TABLE IF EXISTS `pts_trn_batchin`;
CREATE TABLE IF NOT EXISTS `pts_trn_batchin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `room_code` varchar(20) DEFAULT NULL,
  `batch_no` varchar(50) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_desc` varchar(300) NOT NULL,
  `emp_code` varchar(50) DEFAULT NULL,
  `emp_name` varchar(250) DEFAULT NULL,
  `emp_email` varchar(500) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `batch_in_time` int(11) DEFAULT NULL,
  `batch_out_time` int(11) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  KEY `room_code` (`room_code`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pts_trn_batchin`
--

INSERT INTO `pts_trn_batchin` (`id`, `room_code`, `batch_no`, `product_code`, `product_desc`, `emp_code`, `emp_name`, `emp_email`, `role_id`, `created_by`, `created_on`, `batch_in_time`, `batch_out_time`, `isactive`) VALUES
(1, 'TAA-78', '123', '0011002A', 'ONDANSETRON HYDROCHLORIDE TABLETS, 4MG', '33', 'sid', 'sid@sunpharma.in', 1, '33', '2020-03-02 09:12:18', 1583140338, 1586416781, b'0'),
(2, 'TAA-58,TAA-59', '14', 'PL001', 'METFORMIN HCl EXTENDED RELEASE TABLETS', '33', 'sid', 'sid@sunpharma.in', 1, '33', '2020-03-03 09:41:14', 1583228474, 1586416781, b'0'),
(3, 'TAA-78', 'a12', '0011001A', 'METFORMIN HYDROCHLORIDE TABLETS, USP 1000MG', '33', 'sid', 'sid@sunpharma.in', 1, '33', '2020-03-03 09:43:26', 1583228606, 1586416781, b'0'),
(4, 'TAA-78', 'a', 'PL001', 'METFORMIN HCl EXTENDED RELEASE TABLETS', '33', 'sid', 'sid@sunpharma.in', 1, '33', '2020-03-03 10:12:23', 1583230343, 1586416781, b'0'),
(5, 'TAA-78', '123', 'PL001', 'METFORMIN HCl EXTENDED RELEASE TABLETS', '33', 'sid', 'sid@sunpharma.in', 1, '33', '2020-03-04 05:17:51', 1583299071, 1586416781, b'0'),
(6, 'TAA-78', '1234578', '0011058B', 'ONDANSETRON ORALLY DISINTEGRATING TABLETS,4MG', '33', 'sid', 'sid@sunpharma.in', 1, '33', '2020-03-15 10:26:45', 1584268005, 1586416781, b'0'),
(7, 'TAA-78', '1234578', '0011058B', 'ONDANSETRON ORALLY DISINTEGRATING TABLETS,4MG', '33', 'sid', 'sid@sunpharma.in', 1, '33', '2020-03-15 10:42:33', 1584268953, 1586416781, b'0'),
(8, 'TAA-78', '1212', '0011047B', 'CARBIDOPA AND LEVODOPA EXTENDED RELEASE TABLETS 25 MG / 100 MG', '33', 'sid', 'sid@sunpharma.in', 1, '33', '2020-03-16 05:32:56', 1584336776, 1586416781, b'0'),
(9, 'TAA-78', '1212', '0011047B', 'CARBIDOPA AND LEVODOPA EXTENDED RELEASE TABLETS 25 MG / 100 MG', '33', 'sid', 'sid@sunpharma.in', 1, '33', '2020-03-16 06:32:52', 1584340372, 1586416781, b'0'),
(10, 'TAA-78', 'JKT1212', '0011047B', 'CARBIDOPA AND LEVODOPA EXTENDED RELEASE TABLETS 25 MG / 100 MG', '33', 'sid', 'sid@sunpharma.in', 1, '33', '2020-04-09 07:20:32', 1586416832, 1586863563, b'0'),
(11, 'TAA-78', 'JKTU0002', '0011055B', 'Desmopressin Acetate Nasal solution, 0.01 (Nasal spray), 5ml/vial', '33', 'sid', 'sid@sunpharma.in', 1, '33', '2020-04-20 03:50:11', 1587354611, 1588586302, b'0'),
(12, 'TAA-78', 'JKTU0002', '0011055B', 'Desmopressin Acetate Nasal solution, 0.01 (Nasal spray), 5ml/vial', '34', 'abdul ', 'abdul@sunpharma.in', 2, '34', '2020-05-04 10:11:36', 1588587096, 1588589043, b'0'),
(13, 'TAA-78', 'JKTU0002', '0011055B', 'Desmopressin Acetate Nasal solution, 0.01 (Nasal spray), 5ml/vial', '34', 'abdul ', 'abdul@sunpharma.in', 2, '34', '2020-05-04 10:45:26', 1588589126, 1588589227, b'0'),
(14, 'TAA-78', 'JKTU0002', '0011001A', 'METFORMIN HYDROCHLORIDE TABLETS, USP 1000MG', '33', 'sid', 'sid@sunpharma.in', 1, '33', '2020-05-04 10:47:44', 1588589264, 1588843187, b'0'),
(15, 'TAA-78', 'JKTU0002', '0011001A', 'METFORMIN HYDROCHLORIDE TABLETS, USP 1000MG', '33', 'sid', 'sid@sunpharma.in', 1, '33', '2020-05-15 05:52:54', 1589521974, 1589778569, b'0'),
(16, 'TAA-78', 'JKT0001', '0011001A', 'METFORMIN HYDROCHLORIDE TABLETS, USP 1000MG', '33', 'sid', 'sid@sunpharma.in', 1, '33', '2020-05-19 09:22:37', 1589880157, 1590123373, b'0');

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_emp_roomin`
--

DROP TABLE IF EXISTS `pts_trn_emp_roomin`;
CREATE TABLE IF NOT EXISTS `pts_trn_emp_roomin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `room_code` varchar(20) DEFAULT NULL,
  `emp_code` varchar(50) DEFAULT NULL,
  `emp_name` varchar(250) DEFAULT NULL,
  `emp_email` varchar(500) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `session_id` varchar(200) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `room_in_time` int(11) DEFAULT NULL,
  `room_out_time` int(11) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pts_trn_emp_roomin`
--

INSERT INTO `pts_trn_emp_roomin` (`id`, `room_code`, `emp_code`, `emp_name`, `emp_email`, `role_id`, `session_id`, `created_by`, `created_on`, `room_in_time`, `room_out_time`, `isactive`) VALUES
(1, 'TAA-78', '33', 'sid', 'sid@sunpharma.in', 1, 'c9e63da42fd9d40a7042632d9965c921', '33', '2020-03-02 09:12:04', 1583140324, 1584363626, b'0'),
(3, 'TAA-58,TAA-59', '33', 'sid', 'sid@sunpharma.in', 1, '41050df6e92e19f4a44e90919fa23f9c', '33', '2020-03-03 07:29:09', 1583220549, 1584363626, b'0'),
(4, 'TAA-78', '33', 'sid', 'sid@sunpharma.in', 1, '41050df6e92e19f4a44e90919fa23f9c', '33', '2020-03-03 09:43:08', 1583228588, 1584363626, b'0'),
(5, 'TAA-78', '33', 'sid', 'sid@sunpharma.in', 1, '41050df6e92e19f4a44e90919fa23f9c', '33', '2020-03-03 10:11:15', 1583230275, 1584363626, b'0'),
(6, 'TAA-78', '33', 'sid', 'sid@sunpharma.in', 1, '3c364e576ddec2cca73c7cffd18bc770', '33', '2020-03-05 08:25:32', 1583396732, 1584363626, b'0'),
(8, 'TAA-78', '41', 'meera', '', 3, '3c364e576ddec2cca73c7cffd18bc770', '41', '2020-03-05 08:42:16', 1583397736, NULL, b'1'),
(9, 'TAA-78', '47', 'QA', 'QA@gmail.com', 5, '3c364e576ddec2cca73c7cffd18bc770', '47', '2020-03-05 08:42:34', 1583397754, 1588321108, b'0'),
(10, 'TAA-78', '33', 'sid', 'sid@sunpharma.in', 1, '1e66df279a92062e8f9faec4cf02e99b', '33', '2020-03-16 12:52:05', 1584363125, 1584363626, b'0'),
(11, 'TAA-78', '33', 'sid', 'sid@sunpharma.in', 1, '1e66df279a92062e8f9faec4cf02e99b', '33', '2020-03-16 12:59:54', 1584363594, 1584363626, b'0'),
(12, 'TAA-78', '33', 'sid', 'sid@sunpharma.in', 1, '1e66df279a92062e8f9faec4cf02e99b', '33', '2020-03-16 13:12:44', 1584364364, NULL, b'1'),
(13, 'TAA-78', '34', 'abdul ', 'abdul@sunpharma.in', 2, 'a7f627958daaf0c15b6f99a95f2bd5bc', '34', '2020-03-16 16:40:19', 1584376819, NULL, b'1'),
(14, 'TAA-78', '47', 'QA', 'QA@gmail.com', 5, '95bb81ab48dd62c3e95acf7fbd762c78', '47', '2020-05-01 08:20:28', 1588321228, NULL, b'1');

--
-- Triggers `pts_trn_emp_roomin`
--
DROP TRIGGER IF EXISTS `emp_roomin_audit_log`;
DELIMITER $$
CREATE TRIGGER `emp_roomin_audit_log` AFTER INSERT ON `pts_trn_emp_roomin` FOR EACH ROW BEGIN
INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Room In','Employee','Insert',NEW.created_on, NEW.created_by, 'N/A', 'N/A');
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `roomout`;
DELIMITER $$
CREATE TRIGGER `roomout` AFTER UPDATE ON `pts_trn_emp_roomin` FOR EACH ROW BEGIN
INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Room In','Employee','Insert',NEW.created_on, NEW.created_by, 'N/A', 'N/A');
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_env_cond_diff`
--

DROP TABLE IF EXISTS `pts_trn_env_cond_diff`;
CREATE TABLE IF NOT EXISTS `pts_trn_env_cond_diff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) NOT NULL,
  `data_log_id` varchar(50) DEFAULT NULL,
  `magnelic_id` varchar(50) DEFAULT NULL,
  `pressure_diff` enum('ok','notok','NA') NOT NULL DEFAULT 'NA',
  `reading` float DEFAULT NULL,
  `temp` float DEFAULT NULL,
  `temp_min` float DEFAULT NULL,
  `temp_max` float DEFAULT NULL,
  `rh` float DEFAULT NULL,
  `rh_min` float DEFAULT NULL,
  `rh_max` float DEFAULT NULL,
  `temp_from` varchar(50) DEFAULT NULL,
  `temp_to` varchar(50) DEFAULT NULL,
  `rh_from` varchar(50) DEFAULT NULL,
  `rh_to` varchar(50) DEFAULT NULL,
  `globe_pressure_diff` enum('PASCAL','MM WC','NA','ZERO CHECK') DEFAULT 'NA',
  `from_pressure` float DEFAULT NULL,
  `to_pressure` float DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(200) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pts_trn_env_cond_diff`
--

INSERT INTO `pts_trn_env_cond_diff` (`id`, `doc_no`, `room_code`, `data_log_id`, `magnelic_id`, `pressure_diff`, `reading`, `temp`, `temp_min`, `temp_max`, `rh`, `rh_min`, `rh_max`, `temp_from`, `temp_to`, `rh_from`, `rh_to`, `globe_pressure_diff`, `from_pressure`, `to_pressure`, `done_by_user_id`, `done_by_user_name`, `done_by_role_id`, `done_by_email`, `done_by_remark`, `created_on`, `status`, `is_in_workflow`, `updated_on`, `updated_by`, `update_remark`) VALUES
(1, 'DOC2517', 'TAA-78', 'log id 1', 'mag id 1', 'notok', 0, 70, 60, 80, 80, 0, 0, 'NLT', '50', 'NLT', '60', 'ZERO CHECK', 0, 0, 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-04-27 17:17:04', 'active', 'no', NULL, NULL, NULL),
(8, NULL, 'TAA-78', 'log id 1', 'mag id 1', 'NA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'NLT', '50', 'NLT', '10', 'PASCAL', 50, 100, NULL, NULL, NULL, NULL, NULL, '2020-05-01 15:40:08', 'active', 'no', NULL, NULL, NULL),
(9, NULL, 'TAA-78', 'log id 1', 'mag id 1', 'NA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'NLT', '50', 'NLT', '10', 'PASCAL', 50, 100, NULL, NULL, NULL, NULL, NULL, '2020-05-01 15:43:37', 'active', 'no', NULL, NULL, NULL),
(10, NULL, 'TAA-78', 'log id 1', 'mag id 1', 'NA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'NLT', '50', 'NLT', '5', 'PASCAL', 50, 100, NULL, NULL, NULL, NULL, NULL, '2020-05-01 15:45:06', 'active', 'no', NULL, NULL, NULL),
(11, 'DOC2599', 'TAA-78', 'log id 1', 'mag id 1', 'ok', 67, 120, 60, 100, 15, 10, 15, 'NLT', '100', 'NLT', '10', 'MM WC', 50, 100, 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-05-09 13:10:46', 'active', 'no', NULL, NULL, NULL),
(12, NULL, 'TAA-78', 'log id 1', 'mag id 1', 'NA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'NLT', '100', 'NLT', '50', 'PASCAL', 50, 100, NULL, NULL, NULL, NULL, NULL, '2020-05-18 18:35:01', 'active', 'no', NULL, NULL, NULL),
(13, 'DOC2607', 'TAA-78', 'log id 1', 'mag id 1', 'ok', 50, 110, 20, 100, 12, 3, 10, 'NLT', '100', 'NLT', '10', 'PASCAL', 50, 100, 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-05-18 18:37:17', 'active', 'yes', NULL, NULL, NULL),
(14, 'DOC2641', 'TAA-78', 'log id 1', 'mag id 1', 'notok', 67, 70, 60, 100, 25, 20, 100, 'NMT', '100', 'NLT', '20', 'PASCAL', 50, 100, 34, 'abdul ', 2, 'abdul@sunpharma.in', 'test', '2020-06-01 16:38:31', 'active', 'no', '2020-06-01 11:11:11', 34, 'test'),
(15, 'DOC1', 'TAA-78', 'log id 1', 'mag id 1', 'ok', 67, 75, 60, 80, 8, 3, 10, 'NMT', '100', 'NMT', '10', 'PASCAL', 50, 100, 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-06-01 16:56:46', 'active', 'yes', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_equip_appratus_leaktest`
--

DROP TABLE IF EXISTS `pts_trn_equip_appratus_leaktest`;
CREATE TABLE IF NOT EXISTS `pts_trn_equip_appratus_leaktest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `equip_appratus_no` varchar(200) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `stage` varchar(250) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pts_trn_equip_appratus_leaktest`
--

INSERT INTO `pts_trn_equip_appratus_leaktest` (`id`, `doc_no`, `room_code`, `equip_appratus_no`, `product_code`, `batch_no`, `stage`, `done_by_user_id`, `done_by_user_name`, `done_by_role_id`, `done_by_email`, `done_by_remark`, `created_on`, `status`, `is_in_workflow`, `updated_on`, `updated_by`, `update_remark`) VALUES
(1, 'DOC2553', 'TAA-78', 'App01', '0011001A', '1456', 'Test1', 33, 'sid', 1, 'sid@sunpharma.in', 'ss', '2020-04-04 23:39:54', 'active', 'no', NULL, NULL, NULL),
(2, 'DOC2562', 'TAA-78', 'App01', 'PL001', '998877', 'Test1', 33, 'sid', 1, 'sid@sunpharma.in', 'sssss', '2020-04-05 00:08:32', 'active', 'no', NULL, NULL, NULL),
(3, 'DOC2566', 'TAA-78', 'App01', 'PL001', '77', 'Test1', 33, 'sid', 1, 'sid@sunpharma.in', 's', '2020-04-05 00:14:02', 'active', 'no', NULL, NULL, NULL),
(4, 'DOC2568', 'TAA-78', 'App01', '0011001A', '78', 'Test1', 33, 'sid', 1, 'sid@sunpharma.in', 's', '2020-04-05 01:33:07', 'active', 'no', NULL, NULL, NULL),
(5, 'DOC2574', 'TAA-78', 'App01', '0011002A', '12', 'Test1', 33, 'sid', 1, 'sid@sunpharma.in', 'sssss', '2020-04-05 01:40:24', 'active', 'no', NULL, NULL, NULL),
(6, 'DOC2490', 'TAA-78', 'Apparatus001', '0011002A', 'JKT1212', 'stage 2', 33, 'sid', 1, 'sid@sunpharma.in', 'test leaktest start', '2020-04-15 14:00:54', 'active', 'no', NULL, NULL, NULL),
(7, 'DOC2534', 'TAA-78', 'Apparatus001', '0011001A', 'JKTU0002', 'stage 1', 33, 'sid', 1, 'sid@sunpharma.in', 'test edit', '2020-05-04 15:00:37', 'active', 'yes', '2020-05-04 10:59:45', 33, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_equip_appratus_log`
--

DROP TABLE IF EXISTS `pts_trn_equip_appratus_log`;
CREATE TABLE IF NOT EXISTS `pts_trn_equip_appratus_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `equip_appratus_no` varchar(200) DEFAULT NULL,
  `cleaning_verification` varchar(40) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `stage` varchar(250) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pts_trn_equip_appratus_log`
--

INSERT INTO `pts_trn_equip_appratus_log` (`id`, `doc_no`, `room_code`, `equip_appratus_no`, `cleaning_verification`, `product_code`, `batch_no`, `stage`, `done_by_user_id`, `done_by_user_name`, `done_by_role_id`, `done_by_email`, `done_by_remark`, `created_on`, `status`, `is_in_workflow`, `updated_on`, `updated_by`, `update_remark`) VALUES
(1, 'DOC2489', 'TAA-78', 'Apparatus001', 'ok', 'PL002', 'JKT1212', 'stage 2', 33, 'sid', 1, 'sid@sunpharma.in', 'test start', '2020-04-15 13:28:14', 'active', 'no', NULL, NULL, NULL),
(2, 'DOC2533', 'TAA-78', 'Apparatus001', 'ok', '0011055B', 'JKTU0002', 'stage 3', 33, 'sid', 1, 'sid@sunpharma.in', 'test edit.', '2020-05-04 12:50:16', 'active', 'no', NULL, NULL, NULL),
(3, 'DOC2631', 'TAA-78', 'Apparatus001', 'notok', 'PL002', 'JKTU0002', 'stage 2', 34, 'abdul ', 2, 'abdul@sunpharma.in', 'test', '2020-05-27 15:59:05', 'active', 'no', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_instrument_log_register`
--

DROP TABLE IF EXISTS `pts_trn_instrument_log_register`;
CREATE TABLE IF NOT EXISTS `pts_trn_instrument_log_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `instrument_code` varchar(50) DEFAULT NULL,
  `stage` varchar(100) DEFAULT NULL,
  `test` varchar(100) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `result_left` varchar(100) NOT NULL DEFAULT 'NA',
  `result_right` varchar(100) NOT NULL DEFAULT 'NA',
  `remark` varchar(500) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pts_trn_instrument_log_register`
--

INSERT INTO `pts_trn_instrument_log_register` (`id`, `doc_no`, `room_code`, `instrument_code`, `stage`, `test`, `product_code`, `batch_no`, `result_left`, `result_right`, `remark`, `done_by_user_id`, `done_by_user_name`, `done_by_role_id`, `done_by_email`, `done_by_remark`, `created_on`, `status`, `is_in_workflow`, `updated_on`, `updated_by`, `update_remark`) VALUES
(1, 'DOC2500', 'TAA-78', 'Instrument001', 'stage 2', 'test instrument', '0011001A', 'MNG235', 'ok', 'ok', NULL, 33, 'sid', 1, 'sid@sunpharma.in', 'test start', '2020-04-17 10:57:13', 'active', 'no', NULL, NULL, NULL),
(2, 'DOC2502', 'TAA-78', 'Instrument001', 'stage 1', 'test instrument edit', '0011055B', 'JKTU0002', 'ok', 'notok', NULL, 33, 'sid', 1, 'sid@sunpharma.in', '2', '2020-04-23 11:28:40', 'active', 'no', NULL, NULL, NULL),
(3, 'DOC2572', 'TAA-78', 'Instrument001', 'stage 1', 'test', '0011001A', 'JKTU0002', 'NA', 'NA', NULL, 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-05-06 18:20:53', 'active', 'yes', NULL, NULL, NULL),
(4, 'DOC2598', 'TAA-78', 'Instrument001', 'stage 2', 'test audit', '0011001A', 'JKTU0002', 'NA', 'NA', NULL, 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-05-08 20:41:14', 'active', 'yes', '2020-05-08 15:11:59', 33, '4'),
(5, 'DOC2638', 'TAA-78', 'Instrument001', 'stage 1', 'test', 'PL002', 'JKTU0002', 'ok', 'ok', NULL, 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-05-30 06:11:10', 'active', 'no', NULL, NULL, NULL),
(6, 'DOC2640', 'TAA-78', 'Instrument001', 'stage 1', 'fdgdfgd', 'PL002', 'JKTU0002', 'NA', 'NA', NULL, 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-06-01 14:42:24', 'active', 'yes', '2020-06-01 09:12:56', 34, '6');

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_laf_pressure_diff`
--

DROP TABLE IF EXISTS `pts_trn_laf_pressure_diff`;
CREATE TABLE IF NOT EXISTS `pts_trn_laf_pressure_diff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(20) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `booth_no` varchar(200) DEFAULT NULL,
  `m1` varchar(100) DEFAULT NULL,
  `m2` varchar(100) DEFAULT NULL,
  `g1` varchar(100) DEFAULT NULL,
  `g2` varchar(100) DEFAULT NULL,
  `g3` varchar(100) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pts_trn_laf_pressure_diff`
--

INSERT INTO `pts_trn_laf_pressure_diff` (`id`, `doc_no`, `room_code`, `booth_no`, `m1`, `m2`, `g1`, `g2`, `g3`, `done_by_user_id`, `done_by_user_name`, `done_by_role_id`, `done_by_email`, `done_by_remark`, `created_on`, `status`, `is_in_workflow`, `updated_on`, `updated_by`, `update_remark`) VALUES
(1, 'DOC1389', 'TAA-58,TAA-59', 'CPD-150', 'yku', 'yu', 'y', 'iy', 'uyi', 33, 'sid', 1, 'sid@sunpharma.in', 'w', '2020-02-13 17:39:56', 'active', 'no', NULL, NULL, NULL),
(2, 'DOC1391', 'TAA-58,TAA-59', 'CPD-150', 'ty', 'ty', 'ti', 'it', 'tui', 33, 'sid', 1, 'sid@sunpharma.in', 'w', '2020-02-13 17:42:07', 'active', 'no', NULL, NULL, NULL),
(3, 'DOC1394', 'TAA-58,TAA-59', 'CPD-150', 'yiuuyuy', 'uyuy', 'yiu', 'yy', 'uiyiuy', 33, 'sid', 1, 'sid@sunpharma.in', 'w', '2020-02-13 17:56:46', 'active', 'no', NULL, NULL, NULL),
(4, 'DOC1397', 'TAA-58,TAA-59', 'CPD-150', 'fjf', 'jff', 'jfj', 'fjf', 'hjfhf', 33, 'sid', 1, 'sid@sunpharma.in', 'w', '2020-02-13 18:01:46', 'active', 'no', NULL, NULL, NULL),
(5, 'DOC1670', 'TAA-58,TAA-59', 'CPD-150', '67', '67', '6786', '8767', '678676', 33, 'sid', 1, 'sid@sunpharma.in', 'w', '2020-02-19 10:57:19', 'active', 'no', NULL, NULL, NULL),
(6, 'DOC2483', 'TAA-78', 'DISP1', '11', '12', '13', '14', '15', 33, 'sid', 1, 'sid@sunpharma.in', 'test LAF', '2020-04-14 16:14:44', 'active', 'no', NULL, NULL, NULL),
(7, 'DOC2539', 'TAA-78', 'DISP1', '11', '12', '13', '14', '16', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-05-05 13:11:56', 'active', 'no', '2020-05-05 08:49:49', 33, 'test'),
(8, 'DOC2633', 'TAA-78', 'DISP1', '5', '7', '9', '7', '7', 33, 'sid', 1, 'sid@sunpharma.in', 'test edit', '2020-05-27 16:47:56', 'active', 'yes', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_material_retreival_relocation`
--

DROP TABLE IF EXISTS `pts_trn_material_retreival_relocation`;
CREATE TABLE IF NOT EXISTS `pts_trn_material_retreival_relocation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) NOT NULL,
  `material_code` varchar(50) DEFAULT NULL,
  `material_condition` varchar(10) DEFAULT NULL,
  `material_batch_no` varchar(50) NOT NULL,
  `material_type` varchar(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pts_trn_material_retreival_relocation`
--

INSERT INTO `pts_trn_material_retreival_relocation` (`id`, `doc_no`, `room_code`, `material_code`, `material_condition`, `material_batch_no`, `material_type`, `created_on`, `status`, `done_by_user_id`, `done_by_user_name`, `done_by_role_id`, `done_by_email`, `done_by_remark`, `is_in_workflow`, `updated_on`, `updated_by`, `update_remark`) VALUES
(1, 'DOC2072', 'TAA-78', '0011047B', 'in', '1212', 'A', '2020-03-26 08:41:34', 'active', 33, 'sid', 1, 'sid@sunpharma.in', 'Material In', 'no', NULL, NULL, NULL),
(4, 'DOC2072', 'TAA-78', '0011047B', 'out', '1212', 'A', '2020-03-26 09:11:11', 'active', 33, 'sid', 1, 'sid@sunpharma.in', 'okay out', 'no', NULL, NULL, NULL),
(5, 'DOC2096', 'TAA-78', '0011055A', 'in', 'JKT005', 'A', '2020-03-26 09:44:51', 'active', 33, 'sid', 1, 'sid@sunpharma.in', 'done', 'no', NULL, NULL, NULL),
(6, 'DOC2096', 'TAA-78', '0011055A', 'out', 'JKT005', 'A', '2020-03-26 09:45:23', 'active', 33, 'sid', 1, 'sid@sunpharma.in', 'done', 'no', NULL, NULL, NULL),
(7, 'DOC2504', 'TAA-78', '0011055B', 'in', 'JKTU0002', 'A', '2020-04-23 10:28:17', 'active', 33, 'sid', 1, 'sid@sunpharma.in', 'start', 'no', NULL, NULL, NULL),
(8, 'DOC2504', 'TAA-78', '0011055B', 'out', 'JKTU0002', 'P', '2020-04-23 10:29:59', 'active', 33, 'sid', 1, 'sid@sunpharma.in', 'start', 'no', NULL, NULL, NULL),
(9, 'DOC2509', 'TAA-78', '0011055B', 'in', 'JKTU0002', 'A', '2020-04-23 10:31:17', 'active', 33, 'sid', 1, 'sid@sunpharma.in', 'start', 'yes', NULL, NULL, NULL),
(10, 'DOC2628', 'TAA-78', '0011001A', 'in', 'JKT001', 'A', '2020-05-27 05:44:39', 'active', 33, 'sid', 1, 'sid@sunpharma.in', 'test edit', 'yes', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_pre_filter_cleaning`
--

DROP TABLE IF EXISTS `pts_trn_pre_filter_cleaning`;
CREATE TABLE IF NOT EXISTS `pts_trn_pre_filter_cleaning` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) NOT NULL,
  `selection_status` enum('from','to') NOT NULL DEFAULT 'from',
  `product_code` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `pre_filter_cleaning` enum('yes','no','NA') NOT NULL DEFAULT 'NA',
  `outer_surface_cleaning` enum('yes','no','NA') NOT NULL DEFAULT 'NA',
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pts_trn_pre_filter_cleaning`
--

INSERT INTO `pts_trn_pre_filter_cleaning` (`id`, `doc_no`, `room_code`, `selection_status`, `product_code`, `batch_no`, `pre_filter_cleaning`, `outer_surface_cleaning`, `done_by_user_id`, `done_by_user_name`, `done_by_role_id`, `done_by_email`, `done_by_remark`, `created_on`, `status`, `is_in_workflow`, `updated_on`, `updated_by`, `update_remark`) VALUES
(1, 'DOC5', 'TAA-78', 'from', 'PL002', 'JKTU0002', 'yes', 'yes', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-06-08 17:49:20', 'active', 'yes', '2020-06-08 12:21:30', 34, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_return_air_filter`
--

DROP TABLE IF EXISTS `pts_trn_return_air_filter`;
CREATE TABLE IF NOT EXISTS `pts_trn_return_air_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(100) NOT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `product_code` varchar(100) NOT NULL,
  `batch_no` varchar(100) NOT NULL,
  `frequency_id` int(11) NOT NULL,
  `filter` varchar(500) NOT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(100) DEFAULT NULL,
  `done_by_role_id` int(11) NOT NULL,
  `done_by_email` varchar(100) NOT NULL,
  `done_by_remark` varchar(200) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_in_workflow` enum('yes','no') DEFAULT 'no',
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_remark` varchar(250) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_trn_return_air_filter`
--

INSERT INTO `pts_trn_return_air_filter` (`id`, `doc_no`, `room_code`, `product_code`, `batch_no`, `frequency_id`, `filter`, `done_by_user_id`, `done_by_user_name`, `done_by_role_id`, `done_by_email`, `done_by_remark`, `created_on`, `is_in_workflow`, `updated_on`, `updated_by`, `update_remark`, `status`) VALUES
(1, 'DOC2488', 'TAA-78', 'PL001', 'JKT1212', 1, 'F1,F2,F3', 33, 'sid', 1, 'sid@sunpharma.in', 'test Return Air Filter.', '2020-04-15 12:00:45', 'no', NULL, NULL, NULL, 'active'),
(2, 'DOC2537', 'TAA-78', '0011001A', 'JKTU0002', 2, 'F1,F2,F3', 33, 'sid', 1, 'sid@sunpharma.in', 'test edit', '2020-05-05 09:37:54', 'no', '2020-05-05 05:50:23', 33, 'update remark', 'active'),
(3, 'DOC2608', 'TAA-78', '0011001A', 'JKTU0002', 1, 'F1,F2,F3', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-05-19 14:19:19', 'yes', NULL, NULL, NULL, 'active'),
(4, 'DOC2615', 'TAA-78', '0011001A', 'JKT0001', 2, 'F1,F2,F3', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-05-20 16:54:49', 'yes', NULL, NULL, NULL, 'active'),
(5, 'DOC2627', 'TAA-78', 'PL002', 'NA', 2, 'F1,F2,F3', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-05-27 10:57:32', 'no', NULL, NULL, NULL, 'active'),
(6, 'DOC2630', 'TAA-78', '0011001A', 'NA', 2, 'F1,F2,F3', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-05-27 15:48:15', 'yes', NULL, NULL, NULL, 'active'),
(7, 'DOC6', 'TAA-78', '0011001A', 'NA', 1, 'F1,F2,F3', 33, 'sid', 1, 'sid@sunpharma.in', 'fdfdfdfdf', '2020-06-09 13:51:34', 'no', NULL, NULL, NULL, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_room_log_activity`
--

DROP TABLE IF EXISTS `pts_trn_room_log_activity`;
CREATE TABLE IF NOT EXISTS `pts_trn_room_log_activity` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(80) DEFAULT NULL,
  `activity_url` varchar(100) NOT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `room_id` int(10) DEFAULT NULL,
  `mst_act_id` int(11) NOT NULL,
  `created_date` int(10) DEFAULT NULL,
  `modified_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pts_trn_room_log_activity`
--

INSERT INTO `pts_trn_room_log_activity` (`id`, `activity_name`, `activity_url`, `status`, `room_id`, `mst_act_id`, `created_date`, `modified_by`, `modified_on`) VALUES
(1, 'Process Log', 'processlog_equipment', 'inactive', 52, 16, 1580108506, NULL, NULL),
(2, 'Portable Equipment Log', 'process_log_portable_equipment', 'inactive', 52, 17, 1580108506, NULL, NULL),
(3, 'Line Log', 'line_log', 'inactive', 52, 30, 1580108506, NULL, NULL),
(5, 'Material Retreival and Relocation Record for Cold Room', 'material_retreival_relocation_record', 'inactive', 52, 19, 1580108506, NULL, NULL),
(6, 'Environmental Condition/Pressure Differential Record', 'environmental_condition_record', 'inactive', 52, 20, 1580108506, NULL, NULL),
(7, 'Vaccum Cleaner Logbook', 'vaccum_cleaner_logbook', 'inactive', 52, 21, 1580108506, NULL, NULL),
(8, 'LAF Pressure Differential Record Sheet', 'laf_pressure_record', 'inactive', 52, 22, 1580108506, NULL, NULL),
(9, 'Tablet Tooling Log Card', 'tablet_tooling_log_card', 'inactive', 52, 23, 1580108506, NULL, NULL),
(10, 'Balance Calibration Record', 'balance_calibration_record', 'inactive', 52, 24, 1580108506, NULL, NULL),
(11, 'Vertical Sampler and Dies Cleaning Usages Log', 'vertical_sampler_dies_cleaning', 'inactive', 52, 25, 1580108506, NULL, NULL),
(12, 'Instrument Log Register', 'instrument_log_register', 'inactive', 52, 26, 1580108506, NULL, NULL),
(13, 'Equipment/Apparatus Log Register', 'equipment_apparatus_log_register', 'inactive', 52, 27, 1580108506, NULL, NULL),
(14, 'Swab Sample Record', 'swab_sample_record', 'inactive', 52, 28, 1580108506, NULL, NULL),
(15, 'Pre-Filter Cleaning Record of LAF', 'pre_filter_cleaning_record', 'inactive', 52, 29, 1580108506, NULL, NULL),
(16, 'Return Air Filter Cleaning Record', 'air_filter_cleaning_record', 'inactive', 52, 18, 1580108506, NULL, NULL),
(49, 'Portable Equipment Log', 'process_log_portable_equipment', 'inactive', 42, 17, 1581316977, NULL, NULL),
(50, 'Process Log', 'processlog_equipment', 'active', 53, 16, 1581318141, NULL, NULL),
(51, 'Portable Equipment Log', 'process_log_portable_equipment', 'active', 53, 17, 1581318153, NULL, NULL),
(52, 'Return Air Filter Cleaning Record', 'air_filter_cleaning_record', 'active', 53, 18, 1581318166, NULL, NULL),
(53, 'Material Retreival and Relocation Record for Cold Room', 'material_retreival_relocation_record', 'active', 53, 19, 1581318178, NULL, NULL),
(54, 'Environmental Condition/Pressure Differential Record', 'environmental_condition_record', 'active', 53, 20, 1581318190, NULL, NULL),
(55, 'Vaccum Cleaner Logbook', 'vaccum_cleaner_logbook', 'active', 53, 21, 1581318236, NULL, NULL),
(56, 'LAF Pressure Differential Record Sheet', 'laf_pressure_record', 'active', 53, 22, 1581318249, NULL, NULL),
(57, 'Tablet Tooling Log Card', 'tablet_tooling_log_card', 'active', 53, 23, 1581318301, NULL, NULL),
(58, 'Balance Calibration Record', 'balance_calibration_record', 'active', 53, 24, 1581318315, NULL, NULL),
(59, 'Vertical Sampler and Dies Cleaning Usages Log', 'vertical_sampler_dies_cleaning', 'active', 53, 25, 1581318330, NULL, NULL),
(60, 'Instrument Log Register', 'instrument_log_register', 'active', 53, 26, 1581318344, NULL, NULL),
(61, 'Equipment/Apparatus Log Register (Friabilator)', 'equipment_apparatus_log_register', 'active', 53, 27, 1581318358, NULL, NULL),
(62, 'Swab Sample Record', 'swab_sample_record', 'active', 53, 28, 1581318377, NULL, NULL),
(63, 'Pre-Filter Cleaning Record of LAF', 'pre_filter_cleaning_record', 'active', 53, 29, 1581318392, NULL, NULL),
(64, 'Line Log', 'line_log', 'active', 53, 30, 1581318405, NULL, NULL),
(65, 'Process Log', 'processlog_equipment', 'inactive', 56, 16, 1583218295, NULL, NULL),
(66, 'Equipment_apparatus_leaktest', 'sf_equipment_apparatus_leaktest', 'active', 53, 31, 1586064969, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_swab_sample_record`
--

DROP TABLE IF EXISTS `pts_trn_swab_sample_record`;
CREATE TABLE IF NOT EXISTS `pts_trn_swab_sample_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `ar_numer` varchar(50) DEFAULT NULL,
  `equipment_id` varchar(50) DEFAULT NULL,
  `equipment_desc` varchar(1000) DEFAULT NULL,
  `pre_product_code` varchar(50) DEFAULT NULL,
  `pre_barch_no` varchar(50) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pts_trn_swab_sample_record`
--

INSERT INTO `pts_trn_swab_sample_record` (`id`, `doc_no`, `room_code`, `ar_numer`, `equipment_id`, `equipment_desc`, `pre_product_code`, `pre_barch_no`, `done_by_user_id`, `done_by_user_name`, `done_by_role_id`, `done_by_email`, `done_by_remark`, `created_on`, `status`, `is_in_workflow`, `updated_on`, `updated_by`, `update_remark`) VALUES
(1, 'DOC2499', 'TAA-78', 'ARN001', 'M-1006', 'S S Tank', '0011001A', 'MNG235', 33, 'sid', 1, 'sid@sunpharma.in', 'test swab sample', '2020-04-16 14:17:33', 'active', 'no', NULL, NULL, NULL),
(2, 'DOC2503', 'TAA-78', 'ARN001', 'OSPE-006', 'Conta Blender-II', '0011001A', 'MNG235', 33, 'sid', 1, 'sid@sunpharma.in', 'test Swab Sample.', '2020-04-23 12:30:07', 'active', 'no', NULL, NULL, NULL),
(3, 'DOC2541', 'TAA-78', 'ARN001', 'M-1006', 'S S Tank', '0011001A', 'NA', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-05-05 15:03:20', 'active', 'no', '2020-05-05 10:08:19', 33, 'test'),
(4, 'DOC2600', 'TAA-78', 'ARN001', 'M-1006', 'S S Tank', '0011001A', 'JKTU0002', 33, 'sid', 1, 'sid@sunpharma.in', 'test swab sample Report', '2020-05-11 10:34:24', 'active', 'no', NULL, NULL, NULL),
(5, 'DOC3', 'TAA-78', 'ARN001', 'SHSP-015', 'Induction sealing machine', 'PL002', 'JKTU0002', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-06-03 14:05:14', 'active', 'no', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_tablet_tooling`
--

DROP TABLE IF EXISTS `pts_trn_tablet_tooling`;
CREATE TABLE IF NOT EXISTS `pts_trn_tablet_tooling` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_matrial_id` int(11) DEFAULT NULL,
  `doc_no` varchar(20) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `product_code` varchar(200) DEFAULT NULL,
  `act_type` varchar(50) DEFAULT NULL,
  `U` varchar(50) DEFAULT NULL,
  `L` varchar(50) DEFAULT NULL,
  `D` varchar(50) DEFAULT NULL,
  `tablet_tooling_from` varchar(50) DEFAULT NULL,
  `tablet_tooling_to` varchar(50) DEFAULT NULL,
  `damaged` varchar(50) DEFAULT NULL,
  `b_no` varchar(50) DEFAULT NULL,
  `tab_qty` varchar(50) DEFAULT NULL,
  `cum_qty` varchar(50) DEFAULT NULL,
  `cum_qty_punch` varchar(50) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `done_by_user_id` varchar(50) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` varchar(50) DEFAULT NULL,
  `done_by_email` varchar(50) DEFAULT NULL,
  `done_by_remark` varchar(50) DEFAULT NULL,
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pts_trn_tablet_tooling`
--

INSERT INTO `pts_trn_tablet_tooling` (`id`, `mst_matrial_id`, `doc_no`, `room_code`, `product_code`, `act_type`, `U`, `L`, `D`, `tablet_tooling_from`, `tablet_tooling_to`, `damaged`, `b_no`, `tab_qty`, `cum_qty`, `cum_qty_punch`, `created_on`, `status`, `done_by_user_id`, `done_by_user_name`, `done_by_role_id`, `done_by_email`, `done_by_remark`, `is_in_workflow`) VALUES
(1, 2, 'DOC1579', 'TAA-78', 'PL002', 'Issuance', '77', '877', '78', '', '', '50', '', '', '', '', '2020-02-18 12:23:09', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'w', 'no'),
(2, 2, 'DOC1580', 'TAA-78', 'PL002', 'Cleaning Before Use', '', '', '', '78', '787', '50', '', '', '', '', '2020-02-18 12:23:20', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'w', 'no'),
(3, 2, 'DOC1581', 'TAA-78', 'PL002', 'Inspection / Verification Before Use', '67', '676', '7676', '', '', '50', '', '', '', '', '2020-02-18 12:23:35', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'w', 'no'),
(4, 2, 'DOC1582', 'TAA-78', 'PL002', 'Usage', '', '', '', '', '', '50', '45', '4564', '4646', '4646', '2020-02-18 12:23:59', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'w', 'no'),
(5, 2, 'DOC1583', 'TAA-78', 'PL002', 'Cleaning After Use', '', '', '', '25', '2542', '50', '', '', '', '', '2020-02-18 12:24:11', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'w', 'no'),
(6, 2, 'DOC1584', 'TAA-78', 'PL002', 'Inspection After Use', '564', '545', '46456', '', '', '50', '', '', '', '', '2020-02-18 12:24:23', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'w', 'no'),
(7, 2, 'DOC1585', 'TAA-78', 'PL002', 'No. of Punch/Die Returned to Storage Cabinet', '3', '4353', '5345', '', '', '50', '', '', '', '', '2020-02-18 12:24:34', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'w', 'no'),
(8, 1, 'DOC1587', 'TAA-78', 'PL002', 'Inspection / Verification Before Use', '6767', '666', '686', '', '', '50', '', '', '', '', '2020-02-18 12:26:11', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'wwww', 'no'),
(9, 1, 'DOC1589', 'TAA-78', 'PL002', 'Inspection / Verification Before Use', '45', '779', '7798', '', '', '50', '', '', '', '', '2020-02-18 12:28:16', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'wwww', 'no'),
(10, 1, 'DOC1590', 'TAA-78', 'PL002', 'Usage', '', '', '', '', '', '50', '67', '676', '7867', '6786', '2020-02-18 12:28:36', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'wwww', 'no'),
(11, 1, 'DOC1591', 'TAA-78', 'PL002', 'Cleaning After Use', '', '', '', '6', '6786', '50', '', '', '', '', '2020-02-18 12:29:11', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'wwww', 'no'),
(12, 1, 'DOC1593', 'TAA-78', 'PL002', 'Issuance', '67', '677676', '7676', '', '', '50', '', '', '', '', '2020-02-18 12:31:48', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'wwww', 'no'),
(13, 2, 'DOC1595', 'TAA-78', 'PL002', 'Inspection / Verification Before Use', '86', '67', '67868', '', '', '50', '', '', '', '', '2020-02-18 12:39:38', 'active', '33', 'sid', '1', 'sid@sunpharma.in', '2222wwww', 'no'),
(14, 1, 'DOC1597', 'TAA-78', 'PL002', 'Cleaning Before Use', '', '', '', 'from test', 'to test', '50', '', '', '', '', '2020-02-18 12:42:12', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'w', 'no'),
(15, 1, 'DOC1599', 'TAA-78', 'PL002', 'Cleaning Before Use', '', '', '', '78', '87', '50', '', '', '', '', '2020-02-18 12:43:22', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'ww', 'no'),
(16, 1, 'DOC1599', 'TAA-78', 'PL002', 'Cleaning Before Use', '', '', '', 'from test', 'to test', '50', '', '', '', '', '2020-02-18 12:43:58', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'ww', 'no'),
(17, 1, 'DOC1600', 'TAA-78', 'PL002', 'Cleaning Before Use', '', '', '', 'from f', 'to t', '50', '', '', '', '', '2020-02-18 12:44:37', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'w', 'no'),
(18, 1, 'DOC1602', 'TAA-78', 'PL002', 'Usage', '', '', '', '', '', '50', '87', '787', '877', '78787', '2020-02-18 12:47:30', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'w', 'no'),
(19, 1, 'DOC1604', 'TAA-78', 'PL002', 'Inspection / Verification Before Use', '78', '7878', '78787', '', '', '50', '', '', '', '', '2020-02-18 12:50:30', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'wwww', 'no'),
(20, 1, 'DOC1605', 'TAA-78', 'PL002', 'Inspection / Verification Before Use', '778', '7879', '789789', '', '', '50', '', '', '', '', '2020-02-18 12:51:32', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'wwww', 'no'),
(21, 2, 'DOC1606', 'TAA-78', 'PL002', 'Cleaning After Use', '', '', '', '67', '67', '50', '', '', '', '', '2020-02-18 12:51:50', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'wwww', 'no'),
(22, 1, 'DOC1668', 'TAA-78', 'PL002', 'Usage', '', '', '', '', '', '50', '689', '77', '89797', '9779', '2020-02-19 10:56:48', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'w', 'no'),
(23, 1, 'DOC2053', 'TAA-78', 'PL002', 'Issuance', '123', '67', '78', '', '', '50', '', '', '', '', '2020-03-25 13:53:18', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'okay', 'no'),
(24, 2, 'DOC2516', 'TAA-78', 'PL001', 'No. of Punch/Die Returned to Storage Cabinet', '233', '333', '40', '', '', '', '', '', '', '', '2020-04-27 12:02:46', 'active', '33', 'sid', '1', 'sid@sunpharma.in', 'test', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_user_activity_log`
--

DROP TABLE IF EXISTS `pts_trn_user_activity_log`;
CREATE TABLE IF NOT EXISTS `pts_trn_user_activity_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) DEFAULT NULL,
  `roomlogactivity` int(10) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `room_id` int(10) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `product_code` varchar(100) NOT NULL,
  `batch_no` varchar(100) NOT NULL,
  `equip_code` varchar(500) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `stop_by` varchar(100) DEFAULT NULL,
  `stop_by_id` int(11) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `activity_remarks` varchar(240) DEFAULT NULL,
  `doc_id` varchar(120) DEFAULT NULL,
  `activity_start` int(10) DEFAULT NULL,
  `activity_pause` int(10) DEFAULT NULL,
  `activity_stop` int(10) DEFAULT NULL,
  `workflowstatus` int(10) DEFAULT NULL,
  `workflownextstep` int(10) DEFAULT NULL,
  `pending_apprvl_for_name` varchar(100) DEFAULT NULL,
  `pending_apprvl_for_code` int(11) DEFAULT NULL,
  `is_qa_approve` enum('yes','no') DEFAULT 'no',
  `sampling_rod_id` varchar(100) DEFAULT NULL,
  `act_name` varchar(200) DEFAULT NULL,
  `dies_no` varchar(100) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pts_trn_user_activity_log`
--

INSERT INTO `pts_trn_user_activity_log` (`id`, `activity_id`, `roomlogactivity`, `status`, `room_id`, `room_code`, `product_code`, `batch_no`, `equip_code`, `user_id`, `user_name`, `stop_by`, `stop_by_id`, `user_email`, `activity_remarks`, `doc_id`, `activity_start`, `activity_pause`, `activity_stop`, `workflowstatus`, `workflownextstep`, `pending_apprvl_for_name`, `pending_apprvl_for_code`, `is_qa_approve`, `sampling_rod_id`, `act_name`, `dies_no`, `created_on`, `updated_on`, `updated_by`, `update_remark`) VALUES
(1, 29, 63, 'active', NULL, 'TAA-78', 'PL002', 'JKTU0002', 'N/A', 33, 'sid', NULL, NULL, 'sid@sunpharma.in', 'Pre-Filter Cleaning Record of LAF is in progress', 'DOC5', 1591618760, NULL, 1591618786, 2, 3, NULL, NULL, 'no', NULL, NULL, NULL, '2020-06-08 12:19:20', NULL, NULL, NULL),
(2, 18, 52, 'active', NULL, 'TAA-78', '0011001A', 'NA', 'N/A', 33, 'sid', NULL, NULL, 'sid@sunpharma.in', 'Return Air Filter Cleaning Record is in progress', 'DOC6', 1591690894, NULL, 1591690936, 3, -1, NULL, NULL, 'no', NULL, NULL, NULL, '2020-06-09 08:21:34', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_vaccum_cleaner`
--

DROP TABLE IF EXISTS `pts_trn_vaccum_cleaner`;
CREATE TABLE IF NOT EXISTS `pts_trn_vaccum_cleaner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `vaccum_cleaner_code` varchar(500) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_no` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `update_remark` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pts_trn_vaccum_cleaner`
--

INSERT INTO `pts_trn_vaccum_cleaner` (`id`, `doc_no`, `room_code`, `vaccum_cleaner_code`, `product_code`, `batch_no`, `done_by_user_id`, `done_by_user_name`, `done_by_role_id`, `done_by_email`, `done_by_remark`, `created_no`, `status`, `is_in_workflow`, `updated_on`, `updated_by`, `update_remark`) VALUES
(1, 'DOC1239', 'TAA-58,TAA-59', 'CPD-188', '0011119B', 'CPDEN-0023', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-02-11 17:23:14', 'active', 'no', NULL, NULL, NULL),
(2, 'DOC1246', 'TAA-58,TAA-59', 'CPD-020', '0011119B', 'CPDEN-0023', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-02-11 17:43:35', 'active', 'no', NULL, NULL, NULL),
(3, 'DOC1287', 'TAA-58,TAA-59', 'OSPE-005', '0011119B', 'CPDEN-0023', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-02-12 11:09:35', 'active', 'no', NULL, NULL, NULL),
(4, 'DOC1672', 'TAA-58,TAA-59', 'OSPE-005', 'PL001', 'cpd59', 33, 'sid', 1, 'sid@sunpharma.in', 'ww', '2020-02-19 10:59:01', 'active', 'no', NULL, NULL, NULL),
(5, 'DOC1831', 'TAA-78', 'OSPE-005', 'PL001', '123', 33, 'sid', 1, 'sid@sunpharma.in', 'a', '2020-02-28 11:34:56', 'active', 'no', NULL, NULL, NULL),
(6, 'DOC1836', 'TAA-78', 'OSPE-005', 'PL001', '1234', 33, 'sid', 1, 'sid@sunpharma.in', 'a', '2020-02-28 11:39:01', 'active', 'no', NULL, NULL, NULL),
(7, 'DOC1842', 'TAA-78', 'OSPE-005', 'PL001', '12434', 33, 'sid', 1, 'sid@sunpharma.in', 'A', '2020-02-28 22:34:34', 'active', 'yes', NULL, NULL, NULL),
(8, 'DOC2043', 'TAA-78', 'OSPE-005', '0011047B', '1212', 33, 'sid', 1, 'sid@sunpharma.in', 'a', '2020-03-17 13:12:06', 'active', 'yes', NULL, NULL, NULL),
(9, 'DOC2408', 'TAA-78', 'OSPE-005', '0011047B', '1212', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-04-04 17:01:32', 'active', 'yes', NULL, NULL, NULL),
(10, 'DOC2484', 'TAA-78', 'OSPE-005', '0011001A', 'JKT1212', 33, 'sid', 1, 'sid@sunpharma.in', 'test Vaccum Cleaner', '2020-04-14 17:03:03', 'active', 'no', NULL, NULL, NULL),
(11, 'DOC2538', 'TAA-78', 'OSPE-005', '0011001A', 'JKTU0002', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-05-05 11:24:42', 'active', 'no', '2020-05-05 06:23:42', 33, 'test stop update'),
(12, 'DOC2622', 'TAA-78', 'OSPE-005', '0011001A', 'JKT0001', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-05-20 17:23:26', 'active', 'yes', NULL, NULL, NULL),
(13, 'DOC2629', 'TAA-78', 'OSPE-005', 'PL001', 'JKTU0002', 33, 'sid', 1, 'sid@sunpharma.in', 'test', '2020-05-27 13:12:51', 'active', 'yes', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_workflowsteps`
--

DROP TABLE IF EXISTS `pts_trn_workflowsteps`;
CREATE TABLE IF NOT EXISTS `pts_trn_workflowsteps` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status_id` bigint(20) NOT NULL,
  `activity_id` bigint(20) NOT NULL COMMENT 'Store Activity code from MST_PTS_Activity',
  `workflowtype` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  `next_step` bigint(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(500) NOT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pts_trn_workflowsteps`
--

INSERT INTO `pts_trn_workflowsteps` (`id`, `status_id`, `activity_id`, `workflowtype`, `role_id`, `next_step`, `created_on`, `created_by`, `modified_by`, `modified_on`, `is_active`) VALUES
(23, 2, 2, 'start', 1, 3, '2020-03-31 06:28:04', 'Superadmin', NULL, NULL, b'1'),
(24, 3, 2, 'start', 2, 0, '2020-03-31 06:28:04', 'Superadmin', NULL, NULL, b'1'),
(29, 2, 25, 'stop', 1, 3, '2020-04-01 09:44:36', 'Superadmin', NULL, NULL, b'1'),
(30, 3, 25, 'stop', 2, -1, '2020-04-01 09:44:36', 'Superadmin', NULL, NULL, b'1'),
(37, 2, 1, 'stop', 1, 3, '2020-04-13 09:01:14', 'Superadmin', NULL, NULL, b'1'),
(38, 3, 1, 'stop', 2, -1, '2020-04-13 09:01:14', 'Superadmin', NULL, NULL, b'1'),
(43, 2, 22, 'stop', 1, 3, '2020-04-14 10:22:33', 'Superadmin', NULL, NULL, b'1'),
(44, 3, 22, 'stop', 2, -1, '2020-04-14 10:22:34', 'Superadmin', NULL, NULL, b'1'),
(45, 2, 20, 'stop', 1, 3, '2020-04-15 05:08:55', 'Superadmin', NULL, NULL, b'1'),
(46, 3, 20, 'stop', 2, -1, '2020-04-15 05:08:55', 'Superadmin', NULL, NULL, b'1'),
(49, 2, 28, 'stop', 1, 3, '2020-04-16 08:45:55', 'Superadmin', NULL, NULL, b'1'),
(50, 3, 28, 'stop', 2, -1, '2020-04-16 08:45:55', 'Superadmin', NULL, NULL, b'1'),
(72, 2, 18, 'stop', 1, 3, '2020-05-02 08:27:09', 'Superadmin', NULL, NULL, b'1'),
(73, 3, 18, 'stop', 2, -1, '2020-05-02 08:27:09', 'Superadmin', NULL, NULL, b'1'),
(81, 2, 2, 'stop', 1, 3, '2020-05-07 06:54:59', 'Superadmin', NULL, NULL, b'1'),
(82, 3, 2, 'stop', 2, 6, '2020-05-07 06:54:59', 'Superadmin', NULL, NULL, b'1'),
(83, 6, 2, 'stop', 5, -1, '2020-05-07 06:54:59', 'Superadmin', NULL, NULL, b'1'),
(84, 2, 21, 'stop', 1, 3, '2020-05-08 15:58:07', 'Superadmin', NULL, NULL, b'1'),
(85, 3, 21, 'stop', 2, -1, '2020-05-08 15:58:07', 'Superadmin', NULL, NULL, b'1'),
(88, 2, 6, 'start', 1, 3, '2020-05-19 09:47:14', 'Superadmin', NULL, NULL, b'1'),
(89, 3, 6, 'start', 2, 0, '2020-05-19 09:47:14', 'Superadmin', NULL, NULL, b'1'),
(90, 2, 6, 'stop', 1, 3, '2020-05-19 09:47:23', 'Superadmin', NULL, NULL, b'1'),
(91, 3, 6, 'stop', 2, -1, '2020-05-19 09:47:23', 'Superadmin', NULL, NULL, b'1'),
(92, 2, 1, 'start', 1, 3, '2020-05-22 04:53:36', 'Superadmin', NULL, NULL, b'1'),
(93, 3, 1, 'start', 2, 6, '2020-05-22 04:53:36', 'Superadmin', NULL, NULL, b'1'),
(94, 6, 1, 'start', 5, 0, '2020-05-22 04:53:36', 'Superadmin', NULL, NULL, b'1'),
(95, 2, 19, 'stop', 1, 3, '2020-05-27 05:19:41', 'Superadmin', NULL, NULL, b'1'),
(96, 3, 19, 'stop', 2, -1, '2020-05-27 05:19:41', 'Superadmin', NULL, NULL, b'1'),
(102, 2, 29, 'stop', 1, 3, '2020-06-08 10:22:38', 'Superadmin', NULL, NULL, b'1'),
(103, 3, 29, 'stop', 2, -1, '2020-06-08 10:22:39', 'Superadmin', NULL, NULL, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `trn_accessorycleaning`
--

DROP TABLE IF EXISTS `trn_accessorycleaning`;
CREATE TABLE IF NOT EXISTS `trn_accessorycleaning` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `document_no` varchar(50) NOT NULL,
  `area_code` varchar(200) DEFAULT NULL,
  `room_code` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `department_name` varchar(50) NOT NULL,
  `product_name` mediumtext NOT NULL,
  `accessorylist` mediumtext,
  `remarks` mediumtext NOT NULL,
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `stop_time` datetime DEFAULT NULL,
  `performed_by` varchar(250) DEFAULT NULL,
  `performed_name` varchar(500) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `checked_name` varchar(500) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `role_id` int(11) NOT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trn_accessorycleaning`
--

INSERT INTO `trn_accessorycleaning` (`id`, `document_no`, `area_code`, `room_code`, `status`, `next_step`, `batch_no`, `department_name`, `product_name`, `accessorylist`, `remarks`, `start_time`, `stop_time`, `performed_by`, `performed_name`, `checked_by`, `checked_name`, `is_active`, `role_id`, `created_by`, `created_name`, `created_on`, `modified_by`, `modified_name`, `modified_on`, `rejection_remarks`, `qc_filename`, `qc_approveremarks`) VALUES
(103, 'WAC00000001', 'A-002', 'TAA-58,TAA-59', 2, 3, 'CPDEN-0092', 'OSD-1', 'ONDANSETRON HYDROCHLORIDE TABLETS, 4MG', 'DT-014 | Sifter Cum Multi Million | Dummy Cleaning and Operation of Non Destructive leak tester machine for bottle pack (SEPHA)@CPD-171 | Conta Blender | Cleaning and operating procedure of Checkweigher Model-CW1200 (LCD Display)@CPD-200 | Empty Capsule Sorter (ECS) | Cleaning and operating procedure of rotary Re-torquer (COUNTEC)', 'Test', '2020-01-15 10:53:41', '2019-12-26 17:46:06', 'emp-002', 'sid', 'emp-002', 'sid', 1, 1, 'emp-002', 'sid', '2020-01-15 05:23:41', NULL, NULL, NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trn_activityrole`
--

DROP TABLE IF EXISTS `trn_activityrole`;
CREATE TABLE IF NOT EXISTS `trn_activityrole` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `activity_typeid` int(20) NOT NULL,
  `activity_id` bigint(20) NOT NULL COMMENT 'Store Code from mst_activitytivitre Ac',
  `block_code` varchar(200) DEFAULT NULL,
  `area_code` varchar(200) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(500) NOT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1047 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trn_activityrole`
--

INSERT INTO `trn_activityrole` (`id`, `role_id`, `activity_typeid`, `activity_id`, `block_code`, `area_code`, `created_on`, `created_by`, `modified_by`, `modified_on`, `is_active`) VALUES
(397, 6, 15, 99, 'Block-000', 'A-000', '2020-01-15 05:23:41', 'Superadmin', 'Superadmin', NULL, b'1'),
(398, 6, 16, 129, 'Block-000', 'A-000', '2020-01-15 05:23:41', 'Superadmin', 'Superadmin', NULL, b'1'),
(967, 1, 14, 1, 'Block-OSD', 'A-004', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(968, 1, 14, 1, 'Block-OSD', 'A-111', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(969, 1, 14, 1, 'Block-OSD', 'A-112', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(970, 1, 14, 1, 'Block-OSD', 'A-113', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(971, 1, 14, 1, 'Block-OSD', 'A-114', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(972, 1, 14, 1, 'Block-OSD', 'A-115', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(973, 1, 14, 1, 'Block-OSD', 'A-116', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(974, 1, 14, 1, 'Block-OSD', 'A-117', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(975, 1, 14, 1, 'Block-OSD', 'A-118', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(976, 1, 14, 1, 'Block-OSD', 'A-001', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(977, 1, 14, 1, 'Block-OSD', 'A-002', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(978, 1, 14, 1, 'Block-OSD', 'A-003', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(979, 1, 14, 2, 'Block-OSD', 'A-001', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(980, 1, 14, 2, 'Block-OSD', 'A-002', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(981, 1, 14, 2, 'Block-OSD', 'A-003', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(982, 1, 14, 2, 'Block-OSD', 'A-004', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(983, 1, 14, 2, 'Block-OSD', 'A-111', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(984, 1, 14, 2, 'Block-OSD', 'A-112', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(985, 1, 14, 2, 'Block-OSD', 'A-113', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(986, 1, 14, 2, 'Block-OSD', 'A-114', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(987, 1, 14, 2, 'Block-OSD', 'A-115', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(988, 1, 14, 2, 'Block-OSD', 'A-116', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(989, 1, 14, 2, 'Block-OSD', 'A-117', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(990, 1, 14, 2, 'Block-OSD', 'A-118', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(991, 1, 14, 26, 'Block-OSD', 'A-001', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(992, 1, 14, 26, 'Block-OSD', 'A-002', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(993, 1, 14, 26, 'Block-OSD', 'A-003', '2020-01-15 09:19:36', 'Superadmin', 'Superadmin', NULL, b'1'),
(994, 1, 14, 26, 'Block-OSD', 'A-004', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(995, 1, 14, 26, 'Block-OSD', 'A-111', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(996, 1, 14, 26, 'Block-OSD', 'A-112', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(997, 1, 14, 26, 'Block-OSD', 'A-113', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(998, 1, 14, 26, 'Block-OSD', 'A-114', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(999, 1, 14, 26, 'Block-OSD', 'A-115', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1000, 1, 14, 26, 'Block-OSD', 'A-116', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1001, 1, 14, 26, 'Block-OSD', 'A-117', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1002, 1, 14, 26, 'Block-OSD', 'A-118', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1003, 1, 14, 3, 'Block-OSD', 'A-001', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1004, 1, 14, 3, 'Block-OSD', 'A-002', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1005, 1, 14, 4, 'Block-OSD', 'A-001', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1006, 1, 14, 4, 'Block-OSD', 'A-002', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1007, 1, 14, 5, 'Block-OSD', 'A-001', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1008, 1, 14, 5, 'Block-OSD', 'A-002', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1009, 1, 20, 27, 'Block-OSD', 'A-001', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1010, 1, 20, 27, 'Block-OSD', 'A-002', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1011, 1, 20, 29, 'Block-OSD', 'A-001', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1012, 1, 20, 29, 'Block-OSD', 'A-002', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1013, 2, 18, 38, 'Block-OSD', 'A-001', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1014, 2, 18, 38, 'Block-OSD', 'A-002', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1015, 2, 17, 37, 'Block-OSD', 'A-001', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1016, 2, 17, 37, 'Block-OSD', 'A-002', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1017, 3, 18, 38, 'Block-OSD', 'A-001', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1018, 3, 18, 38, 'Block-OSD', 'A-002', '2020-01-15 09:19:37', 'Superadmin', 'Superadmin', NULL, b'1'),
(1019, 4, 18, 38, 'Block-OSD', 'A-001', '2020-01-15 09:19:38', 'Superadmin', 'Superadmin', NULL, b'1'),
(1020, 4, 18, 38, 'Block-OSD', 'A-002', '2020-01-15 09:19:38', 'Superadmin', 'Superadmin', NULL, b'1'),
(1021, 5, 18, 38, 'Block-OSD', 'A-001', '2020-01-15 09:19:38', 'Superadmin', 'Superadmin', NULL, b'1'),
(1022, 5, 18, 38, 'Block-OSD', 'A-002', '2020-01-15 09:19:38', 'Superadmin', 'Superadmin', NULL, b'1'),
(1023, 10, 14, 1, 'Block-OSD', 'A-004', '2020-01-15 09:19:38', 'Superadmin', 'Superadmin', NULL, b'1'),
(1024, 10, 14, 1, 'Block-OSD', 'A-111', '2020-01-15 09:19:38', 'Superadmin', 'Superadmin', NULL, b'1'),
(1025, 10, 14, 1, 'Block-OSD', 'A-112', '2020-01-15 09:19:38', 'Superadmin', 'Superadmin', NULL, b'1'),
(1026, 10, 14, 1, 'Block-OSD', 'A-113', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1027, 10, 14, 1, 'Block-OSD', 'A-114', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1028, 10, 14, 1, 'Block-OSD', 'A-115', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1029, 10, 14, 1, 'Block-OSD', 'A-116', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1030, 10, 14, 1, 'Block-OSD', 'A-117', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1031, 10, 14, 1, 'Block-OSD', 'A-118', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1032, 10, 14, 1, 'Block-OSD', 'A-001', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1033, 10, 14, 1, 'Block-OSD', 'A-002', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1034, 10, 14, 1, 'Block-OSD', 'A-003', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1035, 10, 14, 2, 'Block-OSD', 'A-001', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1036, 10, 14, 2, 'Block-OSD', 'A-002', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1037, 10, 14, 2, 'Block-OSD', 'A-003', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1038, 10, 14, 2, 'Block-OSD', 'A-004', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1039, 10, 14, 2, 'Block-OSD', 'A-111', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1040, 10, 14, 2, 'Block-OSD', 'A-112', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1041, 10, 14, 2, 'Block-OSD', 'A-113', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1042, 10, 14, 2, 'Block-OSD', 'A-114', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1043, 10, 14, 2, 'Block-OSD', 'A-115', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1044, 10, 14, 2, 'Block-OSD', 'A-116', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1045, 10, 14, 2, 'Block-OSD', 'A-117', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1'),
(1046, 10, 14, 2, 'Block-OSD', 'A-118', '2020-01-15 09:19:39', 'Superadmin', 'Superadmin', NULL, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `trn_batch`
--

DROP TABLE IF EXISTS `trn_batch`;
CREATE TABLE IF NOT EXISTS `trn_batch` (
  `batch_code` varchar(50) NOT NULL,
  `batch_description` varchar(50) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `product_description` varchar(200) DEFAULT NULL,
  `UOM` varchar(20) DEFAULT NULL,
  `batch_size` int(11) DEFAULT NULL,
  `market` varchar(30) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`batch_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='temporary table for prototype';

--
-- Dumping data for table `trn_batch`
--

INSERT INTO `trn_batch` (`batch_code`, `batch_description`, `product_code`, `product_description`, `UOM`, `batch_size`, `market`, `created_by`, `created_on`, `modified_by`, `modified_on`, `is_active`) VALUES
('12345', 'Paracetamol', '0011001A', 'METFORMIN HYDROCHLORIDE TABLETS, USP 1000MG', 'gram', 1000, 'India', 'shivam001', '2020-01-15 06:25:45', '', '2020-01-15 06:25:45', b'1'),
('cpd59', 'test', 'PL001', 'METFORMIN HCl EXTENDED RELEASE TABLETS', 'mg', 5000, 'India', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN-00033', ' METFORMIN HYDROCHLORIDE TABLETS, USP 1000MG', '0011001A', 'METFORMIN HYDROCHLORIDE TABLETS, USP 1000MG', 'mg', 20000, 'India', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN-0022', 'ONDANSETRON HYDROCHLORIDE TABLETS, 4MG', '0011002A', 'ONDANSETRON HYDROCHLORIDE TABLETS, 4MG', 'mg', 10000, 'India', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN-0023', 'CARBIDOPA AND LEVODOPA ', '0011119B', 'CARBIDOPA AND LEVODOPA ORALLY DISINTEGRATING TABLETS, 25MG/100MG', 'mg', 20000, 'Ireland', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN-0024', 'METFORMIN HCl EXTENDED', '0011060C', 'METFORMIN HCl EXTENDED RELEASE TABLETS, 750mg', 'mg', 10000, 'Ireland', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN-0027', 'METFORMIN', '0011060C', 'METFORMIN HCl EXTENDED RELEASE TABLETS, 750mg', 'mg', 10000, 'Ireland', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN-0090', 'CARBIDOPA AND LEVODOPA', '0011047B', 'CARBIDOPA AND LEVODOPA EXTENDED RELEASE TABLETS 25 MG / 100 MG', 'mg', 10000, 'India', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN-0091', ' RIVASTIGMINE TARTRATE ', '0011055A', 'RIVASTIGMINE TARTRATE CAPSULES 4.5MG', 'mg', 10000, 'India', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN-0092', 'RIVASTIGMINE TARTRATE ', '0011055A', 'RIVASTIGMINE TARTRATE CAPSULES 4.5MG', 'mg', 2000, 'India', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN-0093', 'CAPSULES 4.5MG', '0011055A', 'RIVASTIGMINE TARTRATE CAPSULES 4.5MG', 'mg', 10000, 'India', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN-0099', 'CARBIDOPA AND LEVODOPA', '0011119B', 'CARBIDOPA AND LEVODOPA ORALLY DISINTEGRATING TABLETS, 25MG/100MG', 'mg', 10000, 'Ireland', 'emp-003', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN0002', 'ONDANSETRON ORALLY DISINTEGRATING TABLETS,4MG', '0011058B', 'ONDANSETRON ORALLY DISINTEGRATING TABLETS,4MG', 'mg', 10000, 'Ireland', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN0003', 'RIVASTIGMINE TARTRATE CAPSULES in batch of 40000', '0011055A', 'RIVASTIGMINE TARTRATE CAPSULES 1.5MG', 'milligram', 40000, 'India', 'ADMIN', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN0005', 'CARBIDOPA AND LEVODOPA ORALLY DISINTEGRATING TABLE', '0011119B', 'CARBIDOPA AND LEVODOPA ORALLY DISINTEGRATING TABLETS, 25MG/100MG', 'mg', 10000, 'Ireland', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN0011', 'METFORMIN HCl EXTENDED RELEASE TABLETS, 750mg', '0011060C', 'METFORMIN HCl EXTENDED RELEASE TABLETS, 750mg', 'mg', 10000, 'Ireland', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN0012', 'RIVASTIGMINE TARTRATE CAPSULES 4.5MG', '0011055A', 'RIVASTIGMINE TARTRATE CAPSULES 4.5MG', 'mg', 10000, 'India', 'emp-003', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN0020', 'METFORMIN HYDROCHLORIDE TABLETS, USP 1000MG', '0011001A', 'METFORMIN HYDROCHLORIDE TABLETS, USP 1000MG', 'mg', 10000, 'India', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('CPDEN0025', 'METFORMIN ', '0011001A', 'METFORMIN HYDROCHLORIDE TABLETS, USP 1000MG', 'mg', 10000, 'India', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('JKT3700A', 'Metformin HCL', '0011001A', 'Metformin hydrochloride tablets extended release 500 mg', 'milligram', 40000, 'India', 'Admin', '2020-01-15 05:23:41', 'Admin', '2020-01-15 05:23:41', b'1'),
('Newbatch', 'ONDANSETRON ORALLY DISINTEGRATING TABLETS,4MG', '0011058B', 'ONDANSETRON ORALLY DISINTEGRATING TABLETS,4MG', 'mg', 30000, 'Ireland', 'emp-002', '2020-01-15 05:23:41', '', '2020-01-15 05:23:41', b'1'),
('test123', 'testsss', '0011001D', 'METFORMIN HYDROCHLORIDE TABLETS, USP 1000MG', 'gram', 12222, 'Ireland', 'shivam001', '2020-01-15 08:26:16', '', '2020-01-15 08:26:16', b'1'),
('testnew', 'test', 'PL001', 'METFORMIN HCl EXTENDED RELEASE TABLETS', 'ml', 1000, 'India', 'shivam001', '2020-01-15 06:44:39', '', '2020-01-15 06:44:39', b'1');

-- --------------------------------------------------------

--
-- Table structure for table `trn_cleaningandsanitization`
--

DROP TABLE IF EXISTS `trn_cleaningandsanitization`;
CREATE TABLE IF NOT EXISTS `trn_cleaningandsanitization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipcode` varchar(200) DEFAULT NULL,
  `sop_code` varchar(200) DEFAULT NULL,
  `documentno` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `lotno` varchar(200) DEFAULT NULL,
  `strarttime` time DEFAULT NULL,
  `startby` varchar(200) DEFAULT NULL,
  `endtime` time DEFAULT NULL,
  `endby` varchar(200) DEFAULT NULL,
  `checkedby_code` varchar(200) DEFAULT NULL,
  `checkedby_name` varchar(200) DEFAULT NULL,
  `checkedon` datetime DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(200) DEFAULT NULL,
  `created_byname` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_dailycleaning`
--

DROP TABLE IF EXISTS `trn_dailycleaning`;
CREATE TABLE IF NOT EXISTS `trn_dailycleaning` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_no` varchar(50) NOT NULL,
  `area_code` varchar(100) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `sanitizationused` mediumtext,
  `available_quantity` varchar(10) DEFAULT NULL,
  `expiry_date_solution` datetime DEFAULT NULL,
  `required_to_clean` varchar(10) DEFAULT NULL,
  `roomno` varchar(300) DEFAULT NULL,
  `wastebin` int(1) NOT NULL,
  `floorcovering` int(1) DEFAULT NULL,
  `drain_points` mediumtext,
  `checked_by` varchar(250) DEFAULT NULL,
  `checked_name` varchar(500) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_time` datetime DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trn_dailycleaning`
--

INSERT INTO `trn_dailycleaning` (`id`, `document_no`, `area_code`, `room_code`, `status`, `next_step`, `department_name`, `sanitizationused`, `available_quantity`, `expiry_date_solution`, `required_to_clean`, `roomno`, `wastebin`, `floorcovering`, `drain_points`, `checked_by`, `checked_name`, `is_active`, `start_time`, `end_time`, `role_id`, `created_by`, `created_name`, `created_on`, `modified_by`, `modified_name`, `modified_on`, `rejection_remarks`, `qc_filename`, `qc_approveremarks`) VALUES
(105, 'WDC00000001', 'A-002', 'TAA-58,TAA-59', 2, 3, 'OSD-1', 'WSP00000001|Envodil 23% with water', '4000', '2019-12-28 17:32:06', '2000', 'TAA-58,TAA-59', 1, 1, 'D/PKG-CPD029/009,KHP/DP/101', 'emp-002', 'sid', 1, '2020-01-15 10:53:43', '2019-12-26 17:40:16', 1, 'emp-002', 'sid', '2020-01-15 05:23:43', NULL, NULL, NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trn_dpmonitoring`
--

DROP TABLE IF EXISTS `trn_dpmonitoring`;
CREATE TABLE IF NOT EXISTS `trn_dpmonitoring` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mat_trans_tbl_id` int(11) DEFAULT NULL,
  `equip_code` varchar(50) DEFAULT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `documentno` varchar(50) DEFAULT NULL,
  `date` datetime NOT NULL,
  `time` time NOT NULL,
  `dpvalue` varchar(200) DEFAULT NULL,
  `ensuredby` varchar(200) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trn_dpmonitoring`
--

INSERT INTO `trn_dpmonitoring` (`id`, `mat_trans_tbl_id`, `equip_code`, `sop_code`, `documentno`, `date`, `time`, `dpvalue`, `ensuredby`, `created_on`, `created_by`, `modified_on`, `modified_by`, `is_active`) VALUES
(54, NULL, '0', 'PAR-225', 'F/PAR-225/001/04', '2019-10-30 00:00:00', '17:34:00', '10', 'Tushar', '2020-01-15 05:23:43', 'Tushar', NULL, NULL, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `trn_drainpointdetails`
--

DROP TABLE IF EXISTS `trn_drainpointdetails`;
CREATE TABLE IF NOT EXISTS `trn_drainpointdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_no` varchar(50) NOT NULL,
  `area_code` varchar(100) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `sanitizationused` varchar(300) NOT NULL,
  `available_quantity` varchar(10) DEFAULT NULL,
  `expiry_date_solution` datetime DEFAULT NULL,
  `required_to_clean` varchar(10) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `drain_points` int(11) DEFAULT NULL,
  `identification_drainpoints` mediumtext,
  `check_remark` varchar(2000) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `checked_name` varchar(500) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_time` datetime DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trn_drainpointdetails`
--

INSERT INTO `trn_drainpointdetails` (`id`, `document_no`, `area_code`, `room_code`, `status`, `next_step`, `department_name`, `sanitizationused`, `available_quantity`, `expiry_date_solution`, `required_to_clean`, `batch_no`, `drain_points`, `identification_drainpoints`, `check_remark`, `checked_by`, `checked_name`, `is_active`, `start_time`, `end_time`, `role_id`, `created_by`, `created_name`, `created_on`, `modified_by`, `modified_name`, `modified_on`, `rejection_remarks`, `qc_filename`, `qc_approveremarks`) VALUES
(105, 'WDP00000001', 'A-002', 'TAA-58,TAA-59', 1, 2, 'OSD-1', 'WSP00000001|Envodil 23% with water', '5000', '2019-12-28 17:32:06', '1000', 'CPDEN-0092', 2, 'D/PKG-CPD029/009, KHP/DP/101, ', 'Test', 'emp-003', 'rajiv', 0, '2020-01-15 10:53:43', '2019-12-26 17:37:57', 1, 'emp-002', 'sid', '2020-01-15 05:23:43', NULL, NULL, NULL, '', NULL, NULL),
(106, 'WDP00000001', 'A-002', 'TAA-58,TAA-59', 2, 3, 'OSD-1', 'WSP00000001|Envodil 23% with water', '5000', '2019-12-28 17:32:06', '1000', 'CPDEN-0092', 2, 'D/PKG-CPD029/009, KHP/DP/101, ', ' Test', 'emp-003', 'rajiv', 1, '2020-01-15 10:53:43', '2019-12-26 17:38:06', 1, 'emp-003', 'rajiv', '2020-01-15 05:23:43', NULL, NULL, NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trn_injequipmentdetails`
--

DROP TABLE IF EXISTS `trn_injequipmentdetails`;
CREATE TABLE IF NOT EXISTS `trn_injequipmentdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lot_no` varchar(1000) DEFAULT NULL,
  `sop_no` varchar(1000) DEFAULT NULL,
  `equipmentname` varchar(1000) DEFAULT NULL,
  `code_no` varchar(1000) DEFAULT NULL,
  `cleaning_status` varchar(50) DEFAULT NULL,
  `checked_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `checked_by` varchar(1000) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trn_injequipmentdetails`
--

INSERT INTO `trn_injequipmentdetails` (`id`, `lot_no`, `sop_no`, `equipmentname`, `code_no`, `cleaning_status`, `checked_on`, `checked_by`, `is_active`) VALUES
(2, '1', 'PAR-022', 'Plastic Corbag', 'CP019', 'Cleaned', '2020-01-15 10:53:44', 'Area Supervisor1', 1),
(3, 'Lot C', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(4, 'Lot C', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Mrinal', 1),
(5, '1fil', 'PAR-022', 'Plastic Corbag', 'CP019', 'Cleaned', '2020-01-15 10:53:44', 'Area Supervisor1', 1),
(6, 'Lot C', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Mrinal', 1),
(7, '123', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Bhupendra', 1),
(8, '1fil', 'PAR-022', 's s tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Area Supervisor1', 1),
(9, 'Lot C', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(10, 'dasd', 'PAR-022', 'Plastic Corbag', 'CP019', 'Cleaned', '2020-01-15 10:53:44', 'Area Supervisor1', 1),
(11, '1', 'PAR-022', 'Plastic Corbag', 'CP019', 'Cleaned', '2020-01-15 10:53:44', 'Area Supervisor1', 1),
(12, 'lot c', 'PAR-022', 's s tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Area Supervisor1', 1),
(13, 'Lot C', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(14, 'Lot C', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(15, 'Lot C', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(16, 'Lot C', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(17, 'Lot C', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(18, 'Lot C', 'PAR-022', 'Plastic Corbag', 'CP019', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(19, 'Lot C', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(20, 'LOT Test', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Mrinal', 1),
(21, 'Lot Test', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Mrinal', 1),
(22, '24HP0023092019', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(23, '01HP023092019', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(24, '00HP0123092019', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(25, '01IP23092019', 'PAR-022', 'S S Tank2', 'M-1007', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(26, '02IP023092019', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(27, '03HP023092019', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(28, '00IP023092019', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(29, '01HP024092019', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(30, '0021349', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(31, '06HP024092019', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(32, '0021349', 'PAR-022', 'ss tank', '', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(33, '0021349', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(34, '000100102', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(35, '0021349', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(36, 'Lot C', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Mrinal', 1),
(37, 'LOT Test A', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Mrinal', 1),
(38, 'Lot C', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Mrinal', 1),
(39, '10HP024092019', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(40, '01HP025092019', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(41, '02HP025092019', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(42, '03HP025092019', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(43, 'Lot C', 'PAR-022', 'S S Tank3', 'M-1008', 'Cleaned', '2020-01-15 10:53:44', 'Bhupendra', 1),
(44, '11HP26092019', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(45, '12HP25092019', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(46, '012398', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(47, '012398', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(48, '13HP25092019', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(49, '01HP26092109', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(50, '012398', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(51, '012398', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(52, '213', 'PAR-022', 'dsads', '', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(53, '132', 'PAR-022', 'sd', '', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(54, '132', 'PAR-022', 'wddw', '', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1),
(55, 'LOT A', 'PAR-022', 'S S Tank', 'M-1006', 'Cleaned', '2020-01-15 10:53:44', 'Mrinal', 1),
(56, '3421', 'PAR-022', 'ss316 tank', '', 'Cleaned', '2020-01-15 10:53:44', 'Siddharth', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trn_injfiltrationdetails`
--

DROP TABLE IF EXISTS `trn_injfiltrationdetails`;
CREATE TABLE IF NOT EXISTS `trn_injfiltrationdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) DEFAULT NULL,
  `lot_no` varchar(1000) DEFAULT NULL,
  `pre_limit` varchar(500) DEFAULT NULL,
  `collection_fromtime` time DEFAULT NULL,
  `collection_totime` time DEFAULT NULL,
  `post_limit` varchar(500) DEFAULT NULL,
  `checked_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `checked_by` varchar(500) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trn_injfiltrationdetails`
--

INSERT INTO `trn_injfiltrationdetails` (`id`, `form_id`, `lot_no`, `pre_limit`, `collection_fromtime`, `collection_totime`, `post_limit`, `checked_on`, `checked_by`, `is_active`) VALUES
(2, 2, '1fil', '38', '11:10:00', '11:20:00', '67', '2020-01-15 05:23:45', 'Area Supervisor1', 1),
(3, 6, '079803', '49.31 PSR', '16:20:00', '18:30:00', '47.80PSE', '2020-01-15 05:23:45', 'Mrinal', 1),
(4, 9, '23', '49.31 PSR', '12:30:00', '14:40:00', '47.80PSE', '2020-01-15 05:23:45', 'Mrinal', 1),
(5, 10, 'sda', '45ff', '05:39:00', '04:38:00', '45545454', '2020-01-15 05:23:45', 'Area Supervisor1', 1),
(6, 11, '1fil', '38', '11:01:00', '11:20:00', '67', '2020-01-15 05:23:45', 'PM1', 1),
(7, 15, 'Lot C', '39', '23:24:00', '12:06:00', '38', '2020-01-15 05:23:45', 'Bhupendra', 1),
(8, 18, 'Lot C', '40', '16:43:00', '17:53:00', '38', '2020-01-15 05:23:45', 'Siddharth', 1),
(9, 19, 'Lot C', '39', '08:30:00', '10:30:00', '38', '2020-01-15 05:23:45', 'Mrinal', 1),
(10, 20, 'LOT Test', '42', '18:00:00', '19:00:00', '45', '2020-01-15 05:23:45', 'Mrinal', 1),
(11, 22, '0021349', '42', '13:30:00', '15:30:00', '54', '2020-01-15 05:23:45', 'Siddharth', 1),
(12, 23, '0021340', '42', '14:25:00', '14:50:00', '54', '2020-01-15 05:23:45', 'Siddharth', 1),
(13, 24, '0021349', '42', '13:00:00', '13:42:00', '54', '2020-01-15 05:23:45', 'Siddharth', 1),
(14, 26, '0021349', '42', '13:33:00', '13:45:00', '54', '2020-01-15 05:23:45', 'Siddharth', 1),
(15, 27, '0021349', '42', '12:23:00', '12:24:00', '54', '2020-01-15 05:23:45', 'Siddharth', 1),
(16, 29, '0021349', '42', '10:14:00', '10:20:00', '54', '2020-01-15 05:23:45', 'Siddharth', 1),
(17, 30, '0021349', '42', '15:12:00', '15:20:00', '54', '2020-01-15 05:23:45', 'Siddharth', 1),
(18, 36, 'Lot C', '39', '23:30:00', '00:30:00', '38', '2020-01-15 05:23:45', 'Bhupendra', 1),
(19, 38, 'Lot C', '39', '12:30:00', '01:30:00', '38', '2020-01-15 05:23:45', 'Mrinal', 1),
(20, 39, '0021349', '42', '20:30:00', '20:45:00', '54', '2020-01-15 05:23:45', 'Siddharth', 1),
(21, 40, '0021349', '42', '10:20:00', '10:50:00', '54', '2020-01-15 05:23:45', 'Siddharth', 1),
(22, 41, '0021349', '42', '10:05:00', '10:10:00', '54', '2020-01-15 05:23:45', 'Siddharth', 1),
(23, 42, '012398', '42', '10:20:00', '10:30:00', '75', '2020-01-15 05:23:45', 'Siddharth', 1),
(24, 43, '123', '39', '00:45:00', '01:39:00', '38', '2020-01-15 05:23:45', 'Siddharth', 1),
(25, 44, '012398', '42', '08:40:00', '08:50:00', '75', '2020-01-15 05:23:45', 'Siddharth', 1),
(26, 49, '012398', '54', '10:12:00', '10:15:00', '75', '2020-01-15 05:23:45', 'Siddharth', 1),
(27, 50, '012398', '42', '12:55:00', '12:57:00', '54', '2020-01-15 05:23:45', 'Siddharth', 1),
(28, 51, '012398', '42', '10:20:00', '20:22:00', '75', '2020-01-15 05:23:45', 'Siddharth', 1),
(29, 53, '132', '38', '12:12:00', '13:12:00', '38', '2020-01-15 05:23:45', NULL, 1),
(30, 55, 'Lot4', '42', '13:30:00', '14:30:00', '45', '2020-01-15 05:23:45', NULL, 1),
(31, 56, '3421', '1', '00:11:00', '14:12:00', '1', '2020-01-15 05:23:45', 'Siddharth', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trn_injpar64details`
--

DROP TABLE IF EXISTS `trn_injpar64details`;
CREATE TABLE IF NOT EXISTS `trn_injpar64details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipmentcode` varchar(1000) DEFAULT NULL,
  `sop_no` varchar(500) DEFAULT NULL,
  `cleaning_start` datetime DEFAULT NULL,
  `cleaning_stop` datetime DEFAULT NULL,
  `clean_by` varchar(500) DEFAULT NULL,
  `cleanstop_by` varchar(500) DEFAULT NULL,
  `clean_remarks` varchar(1000) DEFAULT NULL,
  `clean_checkby` varchar(500) DEFAULT NULL,
  `op_productname` varchar(1000) DEFAULT NULL,
  `op_lotno` varchar(1000) DEFAULT NULL,
  `op_output` varchar(500) DEFAULT NULL,
  `op_start` datetime DEFAULT NULL,
  `op_stop` datetime DEFAULT NULL,
  `op_by` varchar(500) DEFAULT NULL,
  `opstop_by` varchar(500) DEFAULT NULL,
  `op_remarks` varchar(1000) DEFAULT NULL,
  `op_checkby` varchar(500) DEFAULT NULL,
  `break_fromtime` datetime DEFAULT NULL,
  `break_totime` datetime DEFAULT NULL,
  `break_remarks` varchar(1000) DEFAULT NULL,
  `break_checkby` varchar(500) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_injsolutiondetails`
--

DROP TABLE IF EXISTS `trn_injsolutiondetails`;
CREATE TABLE IF NOT EXISTS `trn_injsolutiondetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) DEFAULT NULL,
  `doc_id` varchar(50) DEFAULT NULL,
  `solutionname` varchar(300) NOT NULL,
  `required_quantity` varchar(50) DEFAULT NULL,
  `standard_water_qty` varchar(50) DEFAULT NULL,
  `Standard_hydrogen_qty` varchar(1000) DEFAULT NULL,
  `wfi_makeup_volume` varchar(1000) DEFAULT NULL,
  `actual_water_qty` varchar(500) DEFAULT NULL,
  `arn_water` varchar(500) DEFAULT NULL,
  `uom_water` varchar(50) DEFAULT NULL,
  `actual_hydrogen_qty` varchar(500) DEFAULT NULL,
  `arn_hydrogen` varchar(500) DEFAULT NULL,
  `uom_hydrogen` varchar(50) DEFAULT NULL,
  `actual_wfi_makeup_volume` varchar(500) DEFAULT NULL,
  `arn_wfi` varchar(500) DEFAULT NULL,
  `uom_wfi` varchar(50) DEFAULT NULL,
  `expiry_datatime` datetime DEFAULT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stop_by` varchar(500) DEFAULT NULL,
  `stop_on` datetime DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_injsterilizationdetails`
--

DROP TABLE IF EXISTS `trn_injsterilizationdetails`;
CREATE TABLE IF NOT EXISTS `trn_injsterilizationdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) DEFAULT NULL,
  `equipmentname` varchar(1000) DEFAULT NULL,
  `code_no` varchar(1000) DEFAULT NULL,
  `sterilizer_no` varchar(1000) DEFAULT NULL,
  `run_no` int(11) DEFAULT NULL,
  `ref_graph_no` varchar(1000) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `from_time` time DEFAULT NULL,
  `to_time` time DEFAULT NULL,
  `checked_by` varchar(1000) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_inj_roomcleaningheader`
--

DROP TABLE IF EXISTS `trn_inj_roomcleaningheader`;
CREATE TABLE IF NOT EXISTS `trn_inj_roomcleaningheader` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `solution_code` varchar(200) DEFAULT NULL,
  `room_code` varchar(20) DEFAULT NULL,
  `cleaning_activity_type` varchar(20) DEFAULT NULL,
  `lot_no` varchar(20) DEFAULT NULL,
  `starttime` time DEFAULT NULL,
  `endtime` time DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `checkedby_code` varchar(200) DEFAULT NULL,
  `checkedby_name` varchar(200) DEFAULT NULL,
  `checkedon` time DEFAULT NULL,
  `createdby_name` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(200) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trn_inj_roomcleaningheader`
--

INSERT INTO `trn_inj_roomcleaningheader` (`id`, `date`, `solution_code`, `room_code`, `cleaning_activity_type`, `lot_no`, `starttime`, `endtime`, `remark`, `checkedby_code`, `checkedby_name`, `checkedon`, `createdby_name`, `is_active`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(23, '2019-09-24', '50% hydrogen peroxide 6% solution', 'PMA-102', 'Daily', '1122', '07:03:48', '07:03:51', 'checked', 'Bhupendra', 'BhupendraN', '10:34:40', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'BhupendraN', '2019-09-24 10:34:40'),
(24, '2019-09-24', '50% hydrogen peroxide 6% solution', 'PMA-159', 'Daily', '789', '08:12:24', '08:12:34', 'ok tested', 'Area Supervisor1', 'Abdul', '11:43:04', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'Abdul', '2019-09-24 11:43:04'),
(25, '2019-09-24', 'Preparation of 2.5% v/v Dettol Solution_0022', 'PMA-64', 'Monthly', '1212', '09:54:04', '09:54:27', 'done', 'Area Supervisor1', 'Abdul', '13:25:32', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'Abdul', '2019-09-24 13:25:32'),
(27, '2019-09-24', 'Preparation of 2.5% v/v Dettol Solution_0022', 'PMA-64', 'Daily', '247', '10:47:18', '10:47:40', 'ok', 'Bhupendra', 'BhupendraN', '14:17:56', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'BhupendraN', '2019-09-24 14:17:56'),
(28, '2019-09-24', '50% hydrogen peroxide 6% solution', 'PMA-64', 'with WFI', 'Lot C', '02:48:47', '02:49:37', 'Testing ', 'Mrinal', 'MrinalN', '15:20:08', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'MrinalN', '2019-09-24 15:20:08'),
(29, '2019-09-24', '50% hydrogen peroxide 4% solution', 'PMA-97', 'with WFI', 'Lot C', '02:53:48', '02:53:56', '', 'Bhupendra', 'BhupendraN', '15:24:12', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'BhupendraN', '2019-09-24 15:24:12'),
(30, '2019-09-24', '50% hydrogen peroxide 4% solution', 'PMA-51', 'Daily', 'Lot C', '02:58:37', '02:58:42', 'Testing again', 'Siddharth', 'SiddharthN', '15:28:59', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'SiddharthN', '2019-09-24 15:28:59'),
(31, '2019-09-24', '50% hydrogen peroxide 6% solution', 'PMA-64', 'Daily', 'LOT A', '03:11:29', '03:12:11', '', 'Siddharth', 'SiddharthN', '15:42:36', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'SiddharthN', '2019-09-24 15:42:36'),
(32, '2019-09-24', '50% hydrogen peroxide 6% solution', 'PMA-64', 'Daily', '0021349', '03:15:29', '03:16:02', 'done', 'Siddharth', 'SiddharthN', '15:46:50', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'SiddharthN', '2019-09-24 15:46:50'),
(33, '2019-09-24', '50% hydrogen peroxide 6% solution', 'PMA-97', 'Weekly', '0021349', '03:31:20', '03:31:41', 'Completed', 'Siddharth', 'SiddharthN', '16:02:04', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'SiddharthN', '2019-09-24 16:02:04'),
(34, '2019-09-24', '50% hydrogen peroxide 6% solution', 'PMA-51', 'Weekly', '0021349', '03:37:03', '03:37:49', 'done', 'Siddharth', 'SiddharthN', '16:08:12', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'SiddharthN', '2019-09-24 16:08:12'),
(35, '2019-09-24', '50% hydrogen peroxide 6% solution', 'PMA-64', 'Weekly', '0021349', '08:03:39', '08:04:16', 'Completed', 'Siddharth', 'SiddharthN', '20:34:37', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'SiddharthN', '2019-09-24 20:34:37'),
(36, '2019-09-24', '50% hydrogen peroxide 6% solution', 'PMA-64', 'Weekly', '0021349', '20:47:22', '20:47:27', 'Completed', 'Siddharth', 'SiddharthN', '09:17:54', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'SiddharthN', '2019-09-25 09:17:54'),
(37, '2019-09-24', '50% hydrogen peroxide 6% solution', 'PMA-64', 'Weekly', '0021349', '20:48:57', '20:49:20', 'Completed', 'Siddharth', 'SiddharthN', '09:19:40', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'SiddharthN', '2019-09-25 09:19:40'),
(38, '2019-09-24', '50% hydrogen peroxide 6% solution', 'PMA-102', 'Daily', 'Lot C', '21:13:57', '21:14:02', 'Testing', 'Mrinal', 'MrinalN', '09:44:32', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'MrinalN', '2019-09-25 09:44:32'),
(39, '2019-09-25', '50% hydrogen peroxide 6% solution', 'PMA-97', 'Weekly', '012398', '21:25:31', '21:25:36', 'ok', 'Siddharth', 'SiddharthN', '09:56:09', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'SiddharthN', '2019-09-25 09:56:09'),
(40, '2019-09-25', 'Preparation of 2.5% v/v Dettol Solution_0022', 'PMA-64', 'Daily', '6666', '11:00:12', '22:30:16', 'kk', 'Bhupendra', 'BhupendraN', '11:00:28', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'BhupendraN', '2019-09-25 11:00:28'),
(41, '2019-09-25', 'Preparation of 2.5% v/v Dettol Solution_0022', 'PMA-159', 'Daily', 'SV Lot', '11:05:53', '22:36:01', 'Satisfactory SV', 'Mrinal', 'MrinalN', '11:06:31', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'MrinalN', '2019-09-25 11:06:31'),
(42, '2019-09-25', '50% hydrogen peroxide 6% solution', 'PMA-102', 'Weekly', '222', '11:19:28', '11:19:32', 'tested', 'Bhupendra', 'BhupendraN', '11:20:13', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'BhupendraN', '2019-09-25 11:20:13'),
(43, '2019-09-25', '50% hydrogen peroxide 6% solution', 'PMA-97', 'Weekly', '012398', '11:24:36', '11:24:42', 'ok', 'Siddharth', 'SiddharthN', '11:25:00', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'SiddharthN', '2019-09-25 11:25:00'),
(44, '2019-09-26', '50% hydrogen peroxide 6% solution', 'PMA-64', 'Weekly', '012398', '11:24:10', '11:24:39', 'ok', 'Siddharth', 'SiddharthN', '11:24:56', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'SiddharthN', '2019-09-26 11:24:56'),
(45, '2019-09-26', '50% hydrogen peroxide 6% solution', 'PMA-64', 'Weekly', '012398', '12:55:50', '12:56:10', '', 'Siddharth', 'SiddharthN', '12:57:29', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'SiddharthN', '2019-09-26 12:57:29'),
(46, '2019-09-27', '50% hydrogen peroxide 6% solution', 'PMA-97', 'Weekly', '012398', '11:18:41', '11:18:45', 'ok', 'Siddharth', 'SiddharthN', '11:18:59', 'InjEMP', b'1', 'emp001', '2020-01-15 05:23:48', 'SiddharthN', '2019-09-27 11:18:59');

-- --------------------------------------------------------

--
-- Table structure for table `trn_inprocesscontainercleaning`
--

DROP TABLE IF EXISTS `trn_inprocesscontainercleaning`;
CREATE TABLE IF NOT EXISTS `trn_inprocesscontainercleaning` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_no` varchar(50) NOT NULL,
  `area_code` varchar(100) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `batch_no` varchar(100) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `sop_no` varchar(100) DEFAULT NULL,
  `equipment_code` mediumtext,
  `remarks` mediumtext,
  `checked_by` varchar(250) DEFAULT NULL,
  `checked_name` varchar(500) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_time` datetime DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trn_inprocesscontainercleaning`
--

INSERT INTO `trn_inprocesscontainercleaning` (`id`, `document_no`, `area_code`, `room_code`, `status`, `next_step`, `department_name`, `batch_no`, `product_name`, `sop_no`, `equipment_code`, `remarks`, `checked_by`, `checked_name`, `is_active`, `start_time`, `end_time`, `role_id`, `created_by`, `created_name`, `created_on`, `modified_by`, `modified_name`, `modified_on`, `rejection_remarks`, `qc_filename`, `qc_approveremarks`) VALUES
(84, 'WPC00000001', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPDEN-003', 'CARBIDOPA AND LEVODOPA EXTENDED RELEASE TABLETS 25 MG / 100 MG', 'TAB-028', 'DT-017, ', 'Testing', 'emp-003', 'rajiv', 1, '2020-01-15 10:53:49', '2019-11-01 21:56:13', 1, 'emp-003', 'rajiv', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(85, 'WPC00000002', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPDEN-003', 'C', 'TAB-028', 'DT-017', 'Test', 'emp-002', 'sid', 0, '2020-01-15 10:53:49', '2019-11-01 13:13:00', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', 'emp-005', NULL, '2019-11-19 12:09:44', '', NULL, NULL),
(86, 'WPC00000003', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPDEN-003', 'Par', 'CPD-011', 'SH-298', 'Testing', 'emp-002', 'sid', 0, '2020-01-15 10:53:49', '2019-11-01 18:23:59', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', 'emp-005', 'abdul ', '2019-11-26 18:32:42', '', NULL, NULL),
(87, 'WPC00000004', 'A-001', 'TAA-51,TAA-52', 1, 2, '', 'CPDEN-003', 'CARBIDOPA AND LEVODOPA EXTENDED RELEASE TABLETS 25 MG / 100 MG', 'TAB-028', 'DT-017, ', 'Test', 'emp-002', 'sid', 0, '2020-01-15 10:53:49', '2019-11-01 21:40:49', 1, 'emp-003', 'rajiv', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(88, 'WPC00000004', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPDEN-003', 'CARBIDOPA AND LEVODOPA EXTENDED RELEASE TABLETS 25 MG / 100 MG', 'TAB-028', 'DT-017, ', 'Test', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-11-01 21:40:57', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(89, 'WPC00000005', 'A-001', 'TAA-51,TAA-52', 1, 2, '', 'CPDEN-003', 'RIVASTIGMINE TARTRATE CAPSULES 4.5MG', 'TAB-028', 'CPD-171, ', 'testing', 'emp-002', 'sid', 0, '2020-01-15 10:53:49', '2019-11-01 21:57:23', 1, 'emp-003', 'rajiv', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(90, 'WPC00000005', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPDEN-003', 'RIVASTIGMINE TARTRATE CAPSULES 4.5MG', 'TAB-028', 'CPD-171, ', 'testing', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-11-01 21:57:32', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(91, 'WPC00000006', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPDEN-002', 'METFORMIN HCl EXTENDED RELEASE TABLETS, 500mg', 'PAR-022', 'CPD-171', 'Testing', 'emp-002', 'sid', 0, '2020-01-15 10:53:49', '2019-11-02 12:00:41', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', 'emp-005', NULL, '2019-11-26 19:20:48', '', NULL, NULL),
(92, 'WPC00000007', 'A-006', 'TAA-78', 2, 3, '', 'CPDEN-003', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-011', 'CPD-187, DT-014', 'Testing', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-11-09 12:05:11', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(93, 'WPC00000008', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPDEN-003', 'CARBIDOPA AND LEVODOPA ORALLY DISINTEGRATING TABLETS, 25MG/100MG', 'CPD-016', 'SHSP-015, HC061, ', 'test', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-11-10 12:30:38', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(94, 'WPC00000009', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPDEN001', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-130', 'HC061, ', 'Test', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-11-10 12:31:44', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(95, 'WPC00000010', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPDEN-003', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-015', 'SH-298', 'Testing', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-11-12 11:22:06', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(96, 'WPC00000011', 'A-001', 'TAA-51,TAA-52', 1, 2, '', 'CPDEN-003', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-016', 'SHSP-015, ', 'Test', 'emp-003', 'rajiv', 0, '2020-01-15 10:53:49', '2019-11-12 11:29:03', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(97, 'WPC00000011', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPDEN-003', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-016', 'SHSP-015, ', 'Test', 'emp-003', 'rajiv', 1, '2020-01-15 10:53:49', '2019-11-12 11:29:44', 1, 'emp-003', 'rajiv', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(98, 'WPC00000012', 'A-001', 'TAA-51,TAA-52', 1, 2, '', 'CPDEN001', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-023', 'SHSP-015, ', 'Test', 'emp-003', 'rajiv', 0, '2020-01-15 10:53:49', '2019-11-14 20:55:51', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(99, 'WPC00000013', 'A-002', 'TAA-58,TAA-59', 2, 3, '', 'CPDEN-001', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-016', 'OSPE-005, ', 'Testing', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-11-14 20:53:52', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(100, 'WPC00000012', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPDEN001', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-023', 'SHSP-015, ', 'Test', 'emp-003', 'rajiv', 1, '2020-01-15 10:53:49', '2019-11-14 20:56:12', 1, 'emp-003', NULL, '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(101, 'WPC00000014', 'A-001', 'TAA-51,TAA-52', 1, 2, '', 'CPDEN001', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-018', 'SH-298', 'Test', 'emp-003', 'rajiv', 0, '2020-01-15 10:53:49', '2019-11-14 21:00:44', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(102, 'WPC00000014', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPDEN001', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-018', 'SH-298', 'Test', 'emp-003', 'rajiv', 1, '2020-01-15 10:53:49', '2019-11-14 21:01:08', 1, 'emp-003', NULL, '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(103, 'WPC00000015', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPD', 'product01', 'PAR-022', 'm-1069op2', 'testing', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-11-14 22:00:05', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(104, 'WPC00000016', 'A-001', 'TAA-51,TAA-52', 1, 2, '', 'CPD', 'product01', 'PAR-064', 'm-1069op', 'testing', 'emp-003', 'rajiv', 0, '2020-01-15 10:53:49', '2019-11-14 22:04:36', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(105, 'WPC00000016', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPD', 'product01', 'PAR-064', 'm-1069op', 'testing', 'emp-003', 'rajiv', 1, '2020-01-15 10:53:49', '2019-11-14 22:05:16', 1, 'emp-003', 'rajiv', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(106, 'WPC00000017', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPD', 'product01', 'PAR-022', 'm-1069op', 'test', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-11-15 08:09:17', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(107, 'WPC00000018', 'A-006', 'TAA-78', 1, 2, '', 'CPDEN-002', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-020', 'DT-014, ', 'Test', 'emp-003', 'rajiv', 0, '2020-01-15 10:53:49', '2019-11-15 17:51:45', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(108, 'WPC00000018', 'A-006', 'TAA-78', 2, 3, '', 'CPDEN-002', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-020', 'DT-014, ', 'Test', 'emp-003', 'rajiv', 1, '2020-01-15 10:53:49', '2019-11-15 17:51:56', 1, 'emp-003', 'rajiv', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(109, 'WPC00000019', 'A-006', 'TAA-78', 2, 3, '', 'CPDEN-003', 'CARBIDOPA AND LEVODOPA ORALLY DISINTEGRATING TABLETS, 25MG/100MG', 'CPD-015', 'HC-003, ', 'test', 'emp-003', 'rajiv', 1, '2020-01-15 10:53:49', '2019-11-15 17:53:28', 1, 'emp-003', 'rajiv', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(110, 'WPC00000020', 'A-006', 'TAA-78', 1, 2, '', 'CPDEN-001', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-020', 'OSPE-005, ', 'Test', 'emp-002', 'sid', 0, '2020-01-15 10:53:49', '2019-11-16 10:18:28', 1, 'emp-003', 'rajiv', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(111, 'WPC00000020', 'A-006', 'TAA-78', 2, 3, '', 'CPDEN-001', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-020', 'OSPE-005, ', 'Test', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-11-16 10:18:40', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(112, 'WPC00000002', 'A-001', 'TAA-51,TAA-52', 0, 0, '', 'CPDEN-003', 'C', 'TAB-028', 'DT-017', 'Test', NULL, NULL, 1, '2020-01-15 10:53:49', NULL, 2, 'emp-005', NULL, '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(113, 'WPC00000021', 'A-006', 'TAA-78', 2, 3, '', 'CPDEN-003', 'CARBIDOPA AND LEVODOPA ORALLY DISINTEGRATING TABLETS, 25MG/100MG', 'TAB-023', 'SHSP-015, ', 'Test', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-11-21 11:27:32', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(114, 'WPC00000022', 'A-001', 'TAA-51,TAA-52', 1, 2, '', 'CPDEN-003', 'CARBIDOPA AND LEVODOPA ORALLY DISINTEGRATING TABLETS, 25MG/100MG', 'TAB-023', 'SHSP-015, ', 'Testing', 'emp-002', 'sid', 0, '2020-01-15 10:53:49', '2019-11-21 11:29:16', 1, 'emp-003', 'rajiv', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(115, 'WPC00000022', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPDEN-003', 'CARBIDOPA AND LEVODOPA ORALLY DISINTEGRATING TABLETS, 25MG/100MG', 'TAB-023', 'SHSP-015, ', 'Testing', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-11-21 11:29:24', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(116, 'WPC00000023', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPDEN-001', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-023', 'HC061, ', 'test', 'emp-003', 'rajiv', 1, '2020-01-15 10:53:49', '2019-11-21 12:59:29', 1, 'emp-003', 'rajiv', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(117, 'WPC00000024', 'A-001', 'TAA-51,TAA-52', 1, 2, '', 'CPDEN0004', 'Desmopressin Acetate Nasal solution, 0.01 (Nasal spray), 5ml/vial', 'CPD-018', 'HC-003, ', 'test', 'emp-002', 'sid', 0, '2020-01-15 10:53:49', '2019-11-21 13:01:43', 1, 'emp-003', 'rajiv', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(118, 'WPC00000024', 'A-001', 'TAA-51,TAA-52', 2, 3, '', 'CPDEN0004', 'Desmopressin Acetate Nasal solution, 0.01 (Nasal spray), 5ml/vial', 'CPD-018', 'HC-003, ', 'test', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-11-21 13:01:50', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(119, 'WPC00000003', 'A-001', 'TAA-51,TAA-52', 0, 0, '', 'CPDEN-003', 'Par', 'CPD-011', 'SH-298', 'Testing', NULL, NULL, 1, '2020-01-15 10:53:49', NULL, 2, 'emp-005', NULL, '2020-01-15 05:23:49', NULL, NULL, NULL, 'batch failed for IPCC', NULL, NULL),
(120, 'WPC00000006', 'A-001', 'TAA-51,TAA-52', 3, 0, '', 'CPDEN-002', 'METFORMIN HCl EXTENDED RELEASE TABLETS, 500mg', 'PAR-022', 'CPD-171', 'Testing', NULL, NULL, 1, '2020-01-15 10:53:49', NULL, 2, 'emp-005', NULL, '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(121, 'WPC00000025', 'A-002', 'TAA-58,TAA-59', 1, 2, 'OSD-1', 'CPDEN-0020', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-124', 'CPD-171, DT-017, DT-014, ', 'Test', 'emp-002', 'sid', 0, '2020-01-15 10:53:49', '2019-11-27 16:29:33', 1, 'emp-003', 'rajiv', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(122, 'WPC00000025', 'A-002', 'TAA-58,TAA-59', 2, 3, 'OSD-1', 'CPDEN-0020', 'OXCARBAZEPINE TABLETS,600MG', 'CPD-124', 'CPD-171, DT-017, DT-014, ', 'Test', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-11-27 16:29:42', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(123, 'WPC00000026', 'A-002', 'TAA-58,TAA-59', 2, 3, 'OSD-1', 'CPDEN-002', 'CARBIDOPA AND LEVODOPA ORALLY DISINTEGRATING TABLETS, 25MG/100MG', 'TAB-023', 'OSPE-005, ', 'test', 'emp-002', 'sid', 0, '2020-01-15 10:53:49', '2019-12-18 11:36:08', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', 'emp-004', NULL, '2019-12-18 11:38:05', '', NULL, NULL),
(124, 'WPC00000027', 'A-002', 'TAA-58,TAA-59', 1, 2, 'OSD-1', 'CPDEN-0022', 'CARBIDOPA AND LEVODOPA ORALLY DISINTEGRATING TABLETS, 25MG/100MG', 'CPD-015', 'SH-298', 'test', NULL, NULL, 1, '2020-01-15 10:53:49', NULL, 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(125, 'WPC00000028', 'A-002', 'TAA-58,TAA-59', 2, 3, 'OSD-1', 'CPDEN-0004', 'CARBIDOPA AND LEVODOPA EXTENDED RELEASE TABLETS 25 MG / 100 MG', 'CPD-018', 'OSPE-005, ', 'test', 'emp-002', 'sid', 0, '2020-01-15 10:53:49', '2019-12-18 11:25:16', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', 'emp-004', 'meera', '2019-12-18 11:27:23', '', NULL, NULL),
(126, 'WPC00000028', 'A-002', 'TAA-58,TAA-59', 3, 5, 'OSD-1', 'CPDEN-0004', 'CARBIDOPA AND LEVODOPA EXTENDED RELEASE TABLETS 25 MG / 100 MG', 'CPD-018', 'OSPE-005, ', 'test', NULL, NULL, 0, '2020-01-15 10:53:49', NULL, 2, 'emp-005', NULL, '2020-01-15 05:23:49', 'emp-004', 'meera', '2019-12-18 11:27:23', '', NULL, NULL),
(127, 'WPC00000028', 'A-002', 'TAA-58,TAA-59', 0, 0, 'OSD-1', 'CPDEN-0004', 'CARBIDOPA AND LEVODOPA EXTENDED RELEASE TABLETS 25 MG / 100 MG', 'CPD-018', 'OSPE-005, ', 'test', NULL, NULL, 1, '2020-01-15 10:53:49', NULL, 3, 'emp-004', NULL, '2020-01-15 05:23:49', NULL, NULL, NULL, 'Not cleaned', 'b5be1b6278699f1a15484e169acf8ef6.pdf', NULL),
(128, 'WPC00000026', 'A-002', 'TAA-58,TAA-59', 3, 5, 'OSD-1', 'CPDEN-002', 'CARBIDOPA AND LEVODOPA ORALLY DISINTEGRATING TABLETS, 25MG/100MG', 'TAB-023', 'OSPE-005, ', 'test', NULL, NULL, 0, '2020-01-15 10:53:49', NULL, 2, 'emp-005', NULL, '2020-01-15 05:23:49', 'emp-004', NULL, '2019-12-18 11:38:05', '', NULL, NULL),
(129, 'WPC00000026', 'A-002', 'TAA-58,TAA-59', 5, 0, 'OSD-1', 'CPDEN-002', 'CARBIDOPA AND LEVODOPA ORALLY DISINTEGRATING TABLETS, 25MG/100MG', 'TAB-023', 'OSPE-005, ', 'test', NULL, NULL, 1, '2020-01-15 10:53:49', NULL, 3, 'emp-004', NULL, '2020-01-15 05:23:49', NULL, NULL, NULL, '', 'e2f7d02b557dd7ab07561fd292e32c2a.pdf', 'First time plant set up'),
(130, 'WPC00000029', 'A-001', 'TAA-51,TAA-52', 1, 2, 'OSD-1', 'cpd003', 'CARBIDOPA AND LEVODOPA EXTENDED RELEASE TABLETS 25 MG / 100 MG', 'CPD-999', 'SH-009', 'ok', NULL, NULL, 1, '2020-01-15 10:53:49', NULL, 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(131, 'WPC00000030', 'A-002', 'TAA-58,TAA-59', 2, 3, 'OSD-1', 'CPDEN-0023', 'CARBIDOPA AND LEVODOPA ORALLY DISINTEGRATING TABLETS, 25MG/100MG', 'CPD-124', 'OSPE-007, ', 'test', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-12-26 12:09:18', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL),
(132, 'WPC00000031', 'A-002', 'TAA-58,TAA-59', 2, 3, 'OSD-1', 'CPDEN-0091', 'RIVASTIGMINE TARTRATE CAPSULES 4.5MG', 'CPD-019', 'DT-014, ', 'Test', 'emp-002', 'sid', 1, '2020-01-15 10:53:49', '2019-12-26 17:43:56', 1, 'emp-002', 'sid', '2020-01-15 05:23:49', NULL, NULL, NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trn_material_transfer_dtl`
--

DROP TABLE IF EXISTS `trn_material_transfer_dtl`;
CREATE TABLE IF NOT EXISTS `trn_material_transfer_dtl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mat_eqip_dtl` varchar(2000) DEFAULT NULL,
  `mat_eqip_rec_transfer_FT` time DEFAULT NULL,
  `mat_eqip_rec_transfer_TT` time DEFAULT NULL,
  `sanitization_agent` varchar(200) DEFAULT NULL,
  `performedby` varchar(200) DEFAULT NULL,
  `checkedby_code` varchar(200) DEFAULT NULL,
  `checkedby_name` varchar(200) DEFAULT NULL,
  `checkedon` date DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_operationequipmentdetaillog`
--

DROP TABLE IF EXISTS `trn_operationequipmentdetaillog`;
CREATE TABLE IF NOT EXISTS `trn_operationequipmentdetaillog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipmentheader_table_id` int(11) DEFAULT NULL,
  `header_id` int(11) DEFAULT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `equipment_type` varchar(50) DEFAULT NULL,
  `strattime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `memo_number` varchar(250) DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `action_type` varchar(250) DEFAULT NULL,
  `performed_by` varchar(250) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_operationequipmentheaderlog`
--

DROP TABLE IF EXISTS `trn_operationequipmentheaderlog`;
CREATE TABLE IF NOT EXISTS `trn_operationequipmentheaderlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header_id` int(11) DEFAULT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=558 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trn_operationequipmentheaderlog`
--

INSERT INTO `trn_operationequipmentheaderlog` (`id`, `header_id`, `equipment_code`, `sop_code`, `is_active`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(536, 302, 'OSPE-007', 'CPD-130', b'1', 'emp-002', '2020-01-15 05:23:54', NULL, NULL),
(537, 303, 'OSPE-007', 'CPD-130', b'1', 'emp-002', '2020-01-15 05:23:54', NULL, NULL),
(538, 304, 'OSPE-007', 'CPD-130', b'1', 'emp-002', '2020-01-15 05:23:54', NULL, NULL),
(539, 305, 'OSPE-007', 'CPD-130', b'1', 'emp-002', '2020-01-15 05:23:54', NULL, NULL),
(540, 306, 'CPD-171', 'CPD-124', b'1', 'shivam001', '2020-01-15 05:43:38', NULL, NULL),
(541, 307, 'CPD-171', 'CPD-124', b'1', 'shivam001', '2020-01-15 05:46:53', NULL, NULL),
(542, 308, 'CPD-198', 'TAB-023', b'1', 'shivam001', '2020-01-15 06:26:40', NULL, NULL),
(543, 308, 'CPD-157', 'CPD-017', b'1', 'shivam001', '2020-01-15 06:26:41', NULL, NULL),
(544, 309, 'CPD-198', 'TAB-023', b'1', 'shivam001', '2020-01-15 06:40:36', NULL, NULL),
(545, 309, 'CPD-157', 'CPD-017', b'1', 'shivam001', '2020-01-15 06:40:37', NULL, NULL),
(546, 309, 'KHP/001', 'CPD-019', b'1', 'shivam001', '2020-01-15 06:40:37', NULL, NULL),
(547, 310, 'CPD-200', 'CPD-016', b'1', 'shivam001', '2020-01-15 06:42:36', NULL, NULL),
(548, 311, 'CPD-198', 'TAB-023', b'1', 'shivam001', '2020-01-15 07:04:04', NULL, NULL),
(549, 311, 'CPD-157', 'CPD-017', b'1', 'shivam001', '2020-01-15 07:04:05', NULL, NULL),
(550, 311, 'KHP/001', 'CPD-019', b'1', 'shivam001', '2020-01-15 07:04:05', NULL, NULL),
(551, 312, 'CPD-035', 'CPD-999', b'1', 'shivam001', '2020-01-15 07:05:24', NULL, NULL),
(552, 313, 'CPD-198', 'TAB-023', b'1', 'shivam001', '2020-01-15 08:26:44', NULL, NULL),
(553, 313, 'CPD-157', 'CPD-017', b'1', 'shivam001', '2020-01-15 08:26:44', NULL, NULL),
(554, 313, 'KHP/001', 'CPD-019', b'1', 'shivam001', '2020-01-15 08:26:45', NULL, NULL),
(555, 314, 'CPD-198', 'TAB-023', b'1', 'shivam001', '2020-01-15 09:56:55', NULL, NULL),
(556, 314, 'CPD-157', 'CPD-017', b'1', 'shivam001', '2020-01-15 09:56:55', NULL, NULL),
(557, 314, 'KHP/001', 'CPD-019', b'1', 'shivam001', '2020-01-15 09:56:55', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trn_operationequipmentlog`
--

DROP TABLE IF EXISTS `trn_operationequipmentlog`;
CREATE TABLE IF NOT EXISTS `trn_operationequipmentlog` (
  `id` int(11) NOT NULL,
  `header_id` int(11) DEFAULT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `equipment_type` varchar(50) DEFAULT NULL,
  `sop_code` varchar(250) DEFAULT NULL,
  `brkdwn_strattime` datetime DEFAULT NULL,
  `brkdwn_endtime` datetime DEFAULT NULL,
  `brkdwn_checked_by` varchar(250) DEFAULT NULL,
  `memo_number` varchar(250) DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `performed_by` varchar(250) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_operationlogdetail`
--

DROP TABLE IF EXISTS `trn_operationlogdetail`;
CREATE TABLE IF NOT EXISTS `trn_operationlogdetail` (
  `id` bigint(20) NOT NULL,
  `header_id` varchar(50) DEFAULT NULL,
  `start_time` varchar(50) DEFAULT NULL,
  `end_time` varchar(50) DEFAULT NULL,
  `checked_by_time` datetime DEFAULT NULL,
  `performed_by` varchar(250) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trn_operationlogdetail`
--

INSERT INTO `trn_operationlogdetail` (`id`, `header_id`, `start_time`, `end_time`, `checked_by_time`, `performed_by`, `checked_by`, `is_active`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(527, '306', '2020-01-15 11:13:37', '2020-01-15 11:15:32', '2020-01-15 11:15:32', 'Shival Saini', 'Shival Saini', b'1', 'shivam001', '2020-01-15 05:43:38', 'Shival Saini', '2020-01-15 11:15:32'),
(528, '307', '2020-01-15 11:16:53', '2020-01-15 11:20:44', '2020-01-15 11:20:44', 'Shival Saini', 'Shival Saini', b'1', 'shivam001', '2020-01-15 05:46:53', 'Shival Saini', '2020-01-15 11:20:44'),
(529, '308', '2020-01-15 11:56:40', '2020-01-15 11:59:38', '2020-01-15 11:59:38', 'Shival Saini', 'Shival Saini', b'1', 'shivam001', '2020-01-15 06:26:40', 'Shival Saini', '2020-01-15 11:59:38'),
(530, '309', '2020-01-15 12:10:36', '2020-01-15 12:10:56', '2020-01-15 12:10:56', 'Shival Saini', 'Shival Saini', b'1', 'shivam001', '2020-01-15 06:40:36', 'Shival Saini', '2020-01-15 12:10:56'),
(531, '310', '2020-01-15 12:12:36', '2020-01-15 12:13:15', '2020-01-15 12:13:15', 'Shival Saini', 'Shival Saini', b'1', 'shivam001', '2020-01-15 06:42:36', 'Shival Saini', '2020-01-15 12:13:15'),
(532, '311', '2020-01-15 12:34:04', '2020-01-15 13:55:32', '2020-01-15 13:55:32', 'Shival Saini', 'Shival Saini', b'1', 'shivam001', '2020-01-15 07:04:04', 'Shival Saini', '2020-01-15 13:55:32'),
(533, '312', '2020-01-15 12:35:24', '', '0000-00-00 00:00:00', 'Shival Saini', '', b'1', 'shivam001', '2020-01-15 07:05:24', NULL, NULL),
(534, '313', '2020-01-15 13:56:44', '2020-01-15 13:57:04', '2020-01-15 13:57:04', 'Shival Saini', 'Shival Saini', b'1', 'shivam001', '2020-01-15 08:26:44', 'Shival Saini', '2020-01-15 13:57:04'),
(535, '314', '2020-01-15 15:26:55', '2020-01-23 15:42:12', '2020-01-23 15:42:12', 'Shival Saini', 'Shival Saini', b'1', 'shivam001', '2020-01-15 09:56:55', 'Shival Saini', '2020-01-23 15:42:12');

-- --------------------------------------------------------

--
-- Table structure for table `trn_operationlogheader`
--

DROP TABLE IF EXISTS `trn_operationlogheader`;
CREATE TABLE IF NOT EXISTS `trn_operationlogheader` (
  `id` bigint(20) NOT NULL,
  `document_no` varchar(50) NOT NULL,
  `lot_no` varchar(50) DEFAULT NULL,
  `area_code` varchar(50) NOT NULL,
  `room_code` varchar(50) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `activity_code` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `next_step` int(11) DEFAULT NULL,
  `operation_type` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trn_operationlogheader`
--

INSERT INTO `trn_operationlogheader` (`id`, `document_no`, `lot_no`, `area_code`, `room_code`, `batch_no`, `activity_code`, `status`, `next_step`, `operation_type`, `is_active`, `created_by`, `created_on`, `modified_by`, `modified_on`) VALUES
(302, 'WTA00000001', '', 'A-002', 'TAA-58,TAA-59', 'CPDEN-0092', '2', 5, 0, 'type A', b'1', 'emp-002', '2020-01-15 05:24:03', 'meera', '2019-12-27 11:14:55'),
(303, 'WOP00000001', 'N/A', 'A-002', 'TAA-58,TAA-59', 'CPDEN-0092', '1', 3, 5, 'operation', b'1', 'emp-002', '2020-01-15 05:24:03', 'Rahul Chauhan', '2020-01-15 15:52:22'),
(304, 'WTB00000001', '', 'A-002', 'TAA-58,TAA-59', 'CPDEN-0093', '3', 5, 0, 'type B', b'1', 'emp-002', '2020-01-15 05:24:03', 'meera', '2019-12-27 11:52:24'),
(305, 'WOP00000002', 'N/A', 'A-002', 'TAA-58,TAA-59', 'CPDEN-0093', '1', 1, 2, 'operation', b'1', 'emp-002', '2020-01-15 05:24:03', NULL, NULL),
(306, 'WTA00000002', '', 'A-001', 'TAA-52,TAA-53', 'CPDEN-0003', '2', 2, 3, 'type A', b'1', 'shivam001', '2020-01-15 05:43:38', 'Shival Saini', '2020-01-15 11:15:32'),
(307, 'WTA00000003', '', 'A-001', 'TAA-52,TAA-53', 'CPDEN-0003', '2', 2, 3, 'type A', b'1', 'shivam001', '2020-01-15 05:46:53', 'Shival Saini', '2020-01-15 11:20:44'),
(308, 'WTA00000004', '', 'A-003', 'TAA-80', '12345', '2', 2, 3, 'type A', b'1', 'shivam001', '2020-01-15 06:26:40', 'Shival Saini', '2020-01-15 11:59:38'),
(309, 'WTA00000005', '', 'A-003', 'TAA-80', '12345', '2', 2, 3, 'type A', b'1', 'shivam001', '2020-01-15 06:40:36', 'Shival Saini', '2020-01-15 12:10:56'),
(310, 'WTA00000006', '', 'A-001', 'TAA-52,TAA-53', 'CPDEN-0022', '2', 2, 3, 'type A', b'1', 'shivam001', '2020-01-15 06:42:36', 'Shival Saini', '2020-01-15 12:13:15'),
(311, 'WTA00000007', '', 'A-003', 'TAA-80', 'cpd59', '2', 2, 3, 'type A', b'1', 'shivam001', '2020-01-15 07:04:04', 'Shival Saini', '2020-01-15 13:55:32'),
(312, 'WTA00000008', '', 'A-002', 'TAA-79', 'CPDEN-0093', '2', 1, 2, 'type A', b'1', 'shivam001', '2020-01-15 07:05:24', NULL, NULL),
(313, 'WTA00000009', '', 'A-003', 'TAA-80', 'test123', '2', 2, 3, 'type A', b'1', 'shivam001', '2020-01-15 08:26:44', 'Shival Saini', '2020-01-15 13:57:04'),
(314, 'WTA00000010', '', 'A-003', 'TAA-80', 'CPDEN-0093', '2', 2, 3, 'type A', b'1', 'shivam001', '2020-01-15 09:56:55', 'Shival Saini', '2020-01-23 15:42:12');

--
-- Triggers `trn_operationlogheader`
--
DROP TRIGGER IF EXISTS `trn_operationheader_insert_audit_log`;
DELIMITER $$
CREATE TRIGGER `trn_operationheader_insert_audit_log` AFTER INSERT ON `trn_operationlogheader` FOR EACH ROW Begin
INSERT INTO operationheader_audit_log (id,tablename,tableid,action,createdon,createdby)
	  VALUES(null,'operationlogheader',New.id,'inserted',NOW(),New.created_by);
End
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `trn_operationheader_update_audit_log`;
DELIMITER $$
CREATE TRIGGER `trn_operationheader_update_audit_log` AFTER UPDATE ON `trn_operationlogheader` FOR EACH ROW Begin
INSERT INTO operationheader_audit_log (id,tablename,tableid,action,createdon,createdby)
	  VALUES(null,'operationlogheader',New.id,'updated',NOW(),New.created_by);
End
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_operation_workflow_approval_records`
--

DROP TABLE IF EXISTS `trn_operation_workflow_approval_records`;
CREATE TABLE IF NOT EXISTS `trn_operation_workflow_approval_records` (
  `id` int(11) NOT NULL,
  `header_id` int(11) DEFAULT NULL,
  `doneby` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `roleid` varchar(50) DEFAULT NULL,
  `time` datetime NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `remark` varchar(2000) DEFAULT NULL,
  `filepath` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trn_operation_workflow_approval_records`
--

INSERT INTO `trn_operation_workflow_approval_records` (`id`, `header_id`, `doneby`, `name`, `status`, `roleid`, `time`, `type`, `created_on`, `created_by`, `is_active`, `remark`, `filepath`) VALUES
(127, 302, 'emp-005', 'abdul ', 3, '2', '2019-12-27 11:14:14', 'Approved', '2020-01-15 05:24:05', 'emp-005', b'1', NULL, NULL),
(128, 302, 'emp-004', 'meera', 3, '3', '2019-12-27 11:14:55', 'Approved', '2020-01-15 05:24:05', 'emp-004', b'1', '', '10f0d77a9bd517173e4fa187d2b620da.pdf'),
(129, 304, 'emp-005', 'abdul ', 3, '2', '2019-12-27 11:51:34', 'Approved', '2020-01-15 05:24:05', 'emp-005', b'1', NULL, NULL),
(130, 304, 'emp-004', 'meera', 3, '3', '2019-12-27 11:52:24', 'Approved', '2020-01-15 05:24:05', 'emp-004', b'1', '', 'de93609957c1ad0b052d40a39da0d65c.pdf'),
(131, 303, 'rahul0028', 'Rahul Chauhan', 3, '2', '2020-01-15 15:52:22', 'Approved', '2020-01-15 10:22:22', 'rahul0028', b'1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trn_portabledetail`
--

DROP TABLE IF EXISTS `trn_portabledetail`;
CREATE TABLE IF NOT EXISTS `trn_portabledetail` (
  `id` bigint(20) NOT NULL,
  `document_no` varchar(50) DEFAULT NULL,
  `department_name` varchar(50) DEFAULT NULL,
  `batch_code` varchar(100) DEFAULT NULL,
  `area_code` varchar(100) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `performed_by` varchar(250) DEFAULT NULL,
  `performed_name` varchar(500) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `checked_name` varchar(500) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `role_id` int(11) NOT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `checked_on` datetime DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `stoptime` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trn_portabledetail`
--

INSERT INTO `trn_portabledetail` (`id`, `document_no`, `department_name`, `batch_code`, `area_code`, `room_code`, `status`, `next_step`, `product_code`, `performed_by`, `performed_name`, `checked_by`, `checked_name`, `is_active`, `role_id`, `created_by`, `created_name`, `created_on`, `checked_on`, `modified_by`, `modified_name`, `modified_on`, `stoptime`, `rejection_remarks`, `qc_filename`, `qc_approveremarks`) VALUES
(112, 'WPE00000001', 'OSD-1', 'CPDEN-0092', 'A-002', 'TAA-58,TAA-59', 2, 3, '0011055A', 'emp-002', 'sid', 'emp-002', 'sid', 0, 1, 'emp-002', 'sid', '2020-01-15 05:24:11', '2019-12-26 17:44:54', 'rahul0028', NULL, '2020-01-15 14:56:03', '2019-12-26 17:44:54', '', NULL, NULL),
(113, 'WPE00000001', 'OSD-1', 'CPDEN-0092', 'A-002', 'TAA-58,TAA-59', 3, 5, '0011055A', NULL, NULL, NULL, NULL, 1, 2, 'rahul0028', NULL, '2020-01-15 09:26:04', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trn_portableequipmentdetail`
--

DROP TABLE IF EXISTS `trn_portableequipmentdetail`;
CREATE TABLE IF NOT EXISTS `trn_portableequipmentdetail` (
  `id` bigint(20) NOT NULL,
  `document_portable_id` varchar(50) DEFAULT NULL,
  `document_no` varchar(50) DEFAULT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `equipment_type` varchar(300) DEFAULT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `changepart` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trn_portableequipmentdetail`
--

INSERT INTO `trn_portableequipmentdetail` (`id`, `document_portable_id`, `document_no`, `equipment_code`, `equipment_type`, `sop_code`, `changepart`, `created_on`) VALUES
(137, '112', 'WPE00000001', 'CPD-171', 'Portable', 'CPD-124', '', '2020-01-15 05:24:12'),
(138, '112', 'WPE00000001', 'CPD-200', 'Portable', 'CPD-016', '', '2020-01-15 05:24:12'),
(139, '113', 'WPE00000001', 'CPD-171', 'Portable', 'CPD-124', '', '2020-01-15 09:26:04'),
(140, '113', 'WPE00000001', 'CPD-200', 'Portable', 'CPD-016', '', '2020-01-15 09:26:04');

-- --------------------------------------------------------

--
-- Table structure for table `trn_roomcleaning_detail`
--

DROP TABLE IF EXISTS `trn_roomcleaning_detail`;
CREATE TABLE IF NOT EXISTS `trn_roomcleaning_detail` (
  `id` bigint(20) NOT NULL,
  `header_id` bigint(20) DEFAULT NULL,
  `cleaning_activity_code` varchar(200) DEFAULT NULL,
  `status` varchar(200) DEFAULT NULL,
  `created_by` varchar(200) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_solutiondetails`
--

DROP TABLE IF EXISTS `trn_solutiondetails`;
CREATE TABLE IF NOT EXISTS `trn_solutiondetails` (
  `id` int(11) NOT NULL,
  `doc_id` varchar(50) NOT NULL,
  `area_code` varchar(200) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `solutionname` varchar(300) NOT NULL,
  `required_quantity` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `qcar_no` varchar(50) DEFAULT NULL,
  `standard_solution_qty` varchar(50) DEFAULT NULL,
  `purified_water` varchar(1000) DEFAULT NULL,
  `makeup_volume` varchar(1000) DEFAULT NULL,
  `actual_solution_qty` varchar(500) DEFAULT NULL,
  `actual_purified_water` varchar(500) DEFAULT NULL,
  `solution_valid_upto` varchar(500) DEFAULT NULL,
  `expiry_datatime` datetime DEFAULT NULL,
  `check_remark` varchar(2000) DEFAULT NULL,
  `solution_destroy_checked_by` varchar(250) DEFAULT NULL,
  `solution_destroy_checked_name` varchar(500) DEFAULT NULL,
  `checked_on` datetime DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `role_id` int(11) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text,
  `is_destroy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trn_solutiondetails`
--

INSERT INTO `trn_solutiondetails` (`id`, `doc_id`, `area_code`, `room_code`, `status`, `next_step`, `department_name`, `solutionname`, `required_quantity`, `batch_no`, `qcar_no`, `standard_solution_qty`, `purified_water`, `makeup_volume`, `actual_solution_qty`, `actual_purified_water`, `solution_valid_upto`, `expiry_datatime`, `check_remark`, `solution_destroy_checked_by`, `solution_destroy_checked_name`, `checked_on`, `is_active`, `role_id`, `created_by`, `created_name`, `created_on`, `modified_by`, `modified_name`, `modified_on`, `rejection_remarks`, `qc_filename`, `qc_approveremarks`, `is_destroy`) VALUES
(230, 'WSP00000001', 'A-002', 'TAA-58,TAA-59', 1, 3, 'OSD-1', 'Envodil 23% with water', '5000', 'CPDEN-0091', 'QCAR 23', '0.25', '25.36', '28.00', '44.64 ml', '4528.57 ml', '48', '2019-12-28 17:32:06', NULL, NULL, NULL, NULL, 0, 1, 'emp-002', 'sid', '2020-01-15 05:24:13', 'emp-012', 'tarun', '2019-12-26 17:35:20', '', NULL, NULL, 0),
(231, 'WSP00000001', 'A-002', 'TAA-58,TAA-59', 3, 5, 'OSD-1', 'Envodil 23% with water', '5000', 'CPDEN-0091', 'QCAR 23', '0.25', '25.36', '28.00', '44.64 ml', '4528.57 ml', '48', '2019-12-28 17:32:06', NULL, NULL, NULL, NULL, 0, 2, 'emp-005', 'abdul ', '2020-01-15 05:24:13', 'emp-012', 'tarun', '2019-12-26 17:35:20', '', NULL, NULL, 0),
(232, 'WSP00000001', 'A-002', 'TAA-58,TAA-59', 5, 6, 'OSD-1', 'Envodil 23% with water', '5000', 'CPDEN-0091', 'QCAR 23', '0.25', '25.36', '28.00', '44.64 ml', '4528.57 ml', '48', '2019-12-28 17:32:06', NULL, NULL, NULL, NULL, 0, 3, 'emp-004', 'meera', '2020-01-15 05:24:13', 'emp-012', 'tarun', '2019-12-26 17:35:20', '', '775abdf03dc40e63df80939b7a248e73.pdf', '', 0),
(233, 'WSP00000001', 'A-002', 'TAA-58,TAA-59', 6, 4, 'OSD-1', 'Envodil 23% with water', '5000', 'CPDEN-0091', 'QCAR 23', '0.25', '25.36', '28.00', '44.64 ml', '4528.57 ml', '48', '2019-12-28 17:32:06', NULL, NULL, NULL, NULL, 0, 5, 'emp-011', 'radha', '2020-01-15 05:24:13', 'emp-012', 'tarun', '2019-12-26 17:35:20', '', NULL, NULL, 0),
(234, 'WSP00000001', 'A-002', 'TAA-58,TAA-59', 4, 0, 'OSD-1', 'Envodil 23% with water', '5000', 'CPDEN-0091', 'QCAR 23', '0.25', '25.36', '28.00', '44.64 ml', '4528.57 ml', '48', '2019-12-28 17:32:06', NULL, NULL, NULL, NULL, 1, 4, 'emp-012', 'tarun', '2020-01-15 05:24:13', NULL, NULL, NULL, '', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `trn_solutionlog`
--

DROP TABLE IF EXISTS `trn_solutionlog`;
CREATE TABLE IF NOT EXISTS `trn_solutionlog` (
  `id` int(11) NOT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `qcar_no` varchar(50) DEFAULT NULL,
  `standard_solution_qty` varchar(50) DEFAULT NULL,
  `purified_water` varchar(1000) DEFAULT NULL,
  `makeup_volume` varchar(1000) DEFAULT NULL,
  `actual_solution_qty` varchar(500) DEFAULT NULL,
  `actual_purified_water` varchar(500) DEFAULT NULL,
  `solution_valid_upto` varchar(500) DEFAULT NULL,
  `check_remark` varchar(2000) DEFAULT NULL,
  `solution_destroy_checked_by` varchar(250) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_solutionstock`
--

DROP TABLE IF EXISTS `trn_solutionstock`;
CREATE TABLE IF NOT EXISTS `trn_solutionstock` (
  `id` int(11) NOT NULL,
  `document_no` varchar(250) DEFAULT NULL,
  `solutionname` varchar(250) DEFAULT NULL,
  `available_qty` int(11) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `is_active` int(1) DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trn_solutionstock`
--

INSERT INTO `trn_solutionstock` (`id`, `document_no`, `solutionname`, `available_qty`, `expiry_date`, `is_active`) VALUES
(50, 'WSP00000001', 'Envodil 23% with water', 2000, '2019-12-28 17:32:06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trn_weeklydrainpointdetails`
--

DROP TABLE IF EXISTS `trn_weeklydrainpointdetails`;
CREATE TABLE IF NOT EXISTS `trn_weeklydrainpointdetails` (
  `id` int(11) NOT NULL,
  `document_no` varchar(50) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `sanitizationused` varchar(300) NOT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `drain_points` int(11) DEFAULT NULL,
  `identification_drainpoints` mediumtext,
  `check_remark` varchar(2000) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_time` datetime DEFAULT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_workflowsteps`
--

DROP TABLE IF EXISTS `trn_workflowsteps`;
CREATE TABLE IF NOT EXISTS `trn_workflowsteps` (
  `id` bigint(20) NOT NULL,
  `status_id` bigint(20) NOT NULL,
  `activity_id` bigint(20) NOT NULL COMMENT 'Store Activity code from mst_activity',
  `role_id` bigint(20) NOT NULL,
  `alert_roleid` bigint(20) DEFAULT NULL,
  `alert_message` text,
  `next_step` bigint(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(500) NOT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trn_workflowsteps`
--

INSERT INTO `trn_workflowsteps` (`id`, `status_id`, `activity_id`, `role_id`, `alert_roleid`, `alert_message`, `next_step`, `created_on`, `created_by`, `modified_by`, `modified_on`, `is_active`) VALUES
(227, 1, 33, 1, NULL, NULL, 2, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(228, 2, 33, 1, NULL, NULL, 3, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(229, 3, 33, 2, NULL, NULL, 0, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(242, 1, 34, 1, NULL, NULL, 2, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(243, 2, 34, 1, NULL, NULL, 3, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(244, 3, 34, 2, NULL, NULL, 0, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(248, 1, 1, 1, NULL, NULL, 2, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(249, 2, 1, 1, NULL, NULL, 3, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(250, 3, 1, 2, NULL, NULL, 5, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(251, 5, 1, 3, NULL, NULL, 0, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(262, 1, 2, 1, NULL, NULL, 2, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(263, 2, 2, 1, NULL, NULL, 3, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(264, 3, 2, 2, NULL, NULL, 5, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(265, 5, 2, 3, NULL, NULL, 0, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(266, 1, 3, 1, NULL, NULL, 2, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(267, 2, 3, 1, NULL, NULL, 3, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(268, 3, 3, 2, NULL, NULL, 5, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(269, 5, 3, 3, NULL, NULL, 0, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(270, 1, 4, 1, NULL, NULL, 2, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(271, 2, 4, 1, NULL, NULL, 3, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(272, 3, 4, 2, NULL, NULL, 5, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(273, 5, 4, 3, NULL, NULL, 0, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(274, 1, 5, 1, NULL, NULL, 2, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(275, 2, 5, 1, NULL, NULL, 3, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(276, 3, 5, 2, NULL, NULL, 5, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(277, 5, 5, 3, NULL, NULL, 0, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(278, 1, 26, 1, NULL, NULL, 2, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(279, 2, 26, 1, NULL, NULL, 3, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(280, 3, 26, 2, NULL, NULL, 5, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(281, 5, 26, 3, NULL, NULL, 0, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(282, 1, 29, 1, NULL, NULL, 2, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(283, 2, 29, 1, NULL, NULL, 3, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(284, 3, 29, 2, NULL, NULL, 5, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(285, 5, 29, 3, NULL, NULL, 0, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(286, 1, 30, 1, NULL, NULL, 2, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(287, 2, 30, 1, NULL, NULL, 3, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(288, 3, 30, 2, NULL, NULL, 5, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(289, 5, 30, 3, NULL, NULL, 0, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(290, 1, 27, 1, NULL, NULL, 2, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(291, 2, 27, 1, NULL, NULL, 3, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(292, 3, 27, 2, NULL, NULL, 5, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(293, 5, 27, 3, NULL, NULL, 6, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(294, 6, 27, 5, NULL, NULL, 4, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1'),
(295, 4, 27, 4, NULL, NULL, 0, '2020-01-15 05:24:18', 'Superadmin', NULL, NULL, b'1');

-- --------------------------------------------------------

--
-- Structure for view `getstart_stoped_data`
--
DROP TABLE IF EXISTS `getstart_stoped_data`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `getstart_stoped_data`  AS  select `oph`.`status` AS `status`,`oph`.`area_code` AS `area_code`,`oph`.`room_code` AS `room_code`,`s`.`status_name` AS `status_name`,`b`.`product_description` AS `product_description`,`oph`.`document_no` AS `document_no`,`oph`.`batch_no` AS `batch_no`,`oph`.`lot_no` AS `lot_no`,`oph`.`created_on` AS `created_on`,`oph`.`operation_type` AS `operation_type`,(select `mst_employee`.`emp_name` from `mst_employee` where (`mst_employee`.`emp_code` = `oph`.`created_by`)) AS `created_by` from ((`trn_operationlogheader` `oph` join `mst_status` `s` on((`s`.`id` = `oph`.`status`))) join `trn_batch` `b` on((`oph`.`batch_no` = `b`.`batch_code`))) order by `oph`.`created_on` desc ;

-- --------------------------------------------------------

--
-- Structure for view `getstatusrecord`
--
DROP TABLE IF EXISTS `getstatusrecord`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `getstatusrecord`  AS  select `war`.`id` AS `warid`,`war`.`header_id` AS `header_id`,`war`.`status` AS `status`,`oph`.`area_code` AS `area_code`,`oph`.`room_code` AS `room_code`,`s`.`status_name` AS `status_name`,`b`.`product_description` AS `product_description`,`oph`.`document_no` AS `document_no`,`oph`.`batch_no` AS `batch_no`,`oph`.`lot_no` AS `lot_no`,`oph`.`created_on` AS `created_on`,`oph`.`operation_type` AS `operation_type`,`war`.`doneby` AS `doneby`,`war`.`name` AS `actionby`,`war`.`roleid` AS `roleid`,`war`.`time` AS `actiontime`,`war`.`type` AS `type` from (((`trn_operationlogheader` `oph` join `trn_operation_workflow_approval_records` `war` on((`oph`.`id` = `war`.`header_id`))) join `mst_status` `s` on((`s`.`id` = `war`.`status`))) join `trn_batch` `b` on((`oph`.`batch_no` = `b`.`batch_code`))) order by `oph`.`created_on` desc ;

-- --------------------------------------------------------

--
-- Structure for view `operationsqnlog`
--
DROP TABLE IF EXISTS `operationsqnlog`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `operationsqnlog`  AS  select `a`.`id` AS `id`,`a`.`document_no` AS `document_no`,`a`.`lot_no` AS `lot_no`,`a`.`area_code` AS `area_code`,`a`.`room_code` AS `room_code`,`a`.`batch_no` AS `batch_no`,`e`.`product_description` AS `product_description`,`a`.`operation_type` AS `operation_type`,min(`b`.`start_time`) AS `start_time`,max(`b`.`end_time`) AS `end_time`,(select `mst_employee`.`emp_name` from `mst_employee` where (`mst_employee`.`emp_name` = min(`b`.`performed_by`))) AS `started_by`,(select `mst_employee`.`emp_name` from `mst_employee` where (`mst_employee`.`emp_name` = max(`b`.`performed_by`))) AS `ended_by`,group_concat(distinct concat(convert(`b`.`performed_by` using utf8)) separator ',') AS `names`,group_concat(distinct concat(convert(`ar`.`name` using utf8)) separator ',') AS `approvedby`,group_concat(distinct concat(convert(`ar`.`time` using utf8)) separator ',') AS `approvedon`,group_concat(distinct concat(convert(`c`.`equipment_code` using utf8),'###',`d`.`equipment_name`,'###',`d`.`equipment_type`,'###',convert(`c`.`sop_code` using utf8)) separator ',') AS `equipments` from ((((((`trn_operationlogheader` `a` join `trn_operationlogdetail` `b` on((`a`.`id` = `b`.`header_id`))) join `trn_operationequipmentheaderlog` `c` on((`a`.`id` = `c`.`header_id`))) join `mst_equipment` `d` on((convert(`c`.`equipment_code` using utf8) = `d`.`equipment_code`))) join `mst_sop` `sop` on((convert(`c`.`sop_code` using utf8) = `sop`.`sop_code`))) join `trn_batch` `e` on((`a`.`batch_no` = `e`.`batch_code`))) join `trn_operation_workflow_approval_records` `ar` on((`a`.`id` = `ar`.`header_id`))) where (`ar`.`type` = 'Apprved') group by `a`.`id` ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

INSERT INTO `pts_mst_report` (`id`, `report_name`, `report_url`, `status`, `created_on`, `created_by`, `modified_on`, `modified_by`) VALUES (NULL, 'Active & Inactive User List', 'active_inactive_user_list', 'active', CURRENT_TIMESTAMP, '10', NULL, NULL);

DROP TABLE IF EXISTS `pts_mst_role_privilege`;
CREATE TABLE IF NOT EXISTS `pts_mst_role_privilege` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_type` enum('master','log','report') DEFAULT NULL,
  `module_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `access_type` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_by` varchar(100) NOT NULL DEFAULT 'Admin',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=337 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pts_mst_role_privilege`
--

INSERT INTO `pts_mst_role_privilege` (`id`, `module_type`, `module_id`, `role_id`, `access_type`, `created_by`, `created_on`) VALUES
(1, 'log', 16, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(2, 'log', 17, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(3, 'log', 30, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(4, 'log', 18, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(5, 'log', 19, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(6, 'log', 20, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(7, 'log', 21, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(8, 'log', 22, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(9, 'log', 23, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(10, 'log', 24, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(11, 'log', 25, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(12, 'log', 26, 1, 'NO', 'Admin', '2020-06-04 17:12:19'),
(13, 'log', 27, 1, 'NO', 'Admin', '2020-06-04 17:12:19'),
(14, 'log', 28, 1, 'NO', 'Admin', '2020-06-04 17:12:19'),
(15, 'log', 29, 1, 'YES', 'Admin', '2020-06-04 17:12:19'),
(16, 'log', 31, 1, 'NO', 'Admin', '2020-06-04 17:12:19'),
(17, 'log', 16, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(18, 'log', 17, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(19, 'log', 30, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(20, 'log', 18, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(21, 'log', 19, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(22, 'log', 20, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(23, 'log', 21, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(24, 'log', 22, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(25, 'log', 23, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(26, 'log', 24, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(27, 'log', 25, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(28, 'log', 26, 2, 'NO', 'Admin', '2020-06-04 17:12:19'),
(29, 'log', 27, 2, 'NO', 'Admin', '2020-06-04 17:12:19'),
(30, 'log', 28, 2, 'NO', 'Admin', '2020-06-04 17:12:19'),
(31, 'log', 29, 2, 'YES', 'Admin', '2020-06-04 17:12:19'),
(32, 'log', 31, 2, 'NO', 'Admin', '2020-06-04 17:12:19'),
(33, 'log', 16, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(34, 'log', 17, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(35, 'log', 30, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(36, 'log', 18, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(37, 'log', 19, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(38, 'log', 20, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(39, 'log', 21, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(40, 'log', 22, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(41, 'log', 23, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(42, 'log', 24, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(43, 'log', 25, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(44, 'log', 26, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(45, 'log', 27, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(46, 'log', 28, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(47, 'log', 29, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(48, 'log', 31, 5, 'NO', 'Admin', '2020-06-04 17:12:19'),
(49, 'log', 16, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(50, 'log', 17, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(51, 'log', 30, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(52, 'log', 18, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(53, 'log', 19, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(54, 'log', 20, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(55, 'log', 21, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(56, 'log', 22, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(57, 'log', 23, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(58, 'log', 24, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(59, 'log', 25, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(60, 'log', 26, 4, 'NO', 'Admin', '2020-06-04 17:12:19'),
(61, 'log', 27, 4, 'NO', 'Admin', '2020-06-04 17:12:19'),
(62, 'log', 28, 4, 'NO', 'Admin', '2020-06-04 17:12:19'),
(63, 'log', 29, 4, 'YES', 'Admin', '2020-06-04 17:12:19'),
(64, 'log', 31, 4, 'NO', 'Admin', '2020-06-04 17:12:19'),
(65, 'report', 2, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(66, 'report', 1, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(67, 'report', 3, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(68, 'report', 4, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(69, 'report', 5, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(70, 'report', 6, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(71, 'report', 7, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(72, 'report', 8, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(73, 'report', 9, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(74, 'report', 10, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(75, 'report', 11, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(76, 'report', 12, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(77, 'report', 13, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(78, 'report', 14, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(79, 'report', 15, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(80, 'report', 16, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(81, 'report', 17, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(82, 'report', 18, 1, 'YES', 'Admin', '2020-06-09 13:04:30'),
(83, 'report', 1, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(84, 'report', 2, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(85, 'report', 3, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(86, 'report', 4, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(87, 'report', 5, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(88, 'report', 6, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(89, 'report', 7, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(90, 'report', 8, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(91, 'report', 9, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(92, 'report', 10, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(93, 'report', 11, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(94, 'report', 12, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(95, 'report', 13, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(96, 'report', 14, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(97, 'report', 15, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(98, 'report', 16, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(99, 'report', 17, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(100, 'report', 18, 2, 'YES', 'Admin', '2020-06-09 13:04:30'),
(101, 'report', 1, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(102, 'report', 2, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(103, 'report', 3, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(104, 'report', 4, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(105, 'report', 5, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(106, 'report', 6, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(107, 'report', 7, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(108, 'report', 8, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(109, 'report', 9, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(110, 'report', 10, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(111, 'report', 11, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(112, 'report', 12, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(113, 'report', 13, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(114, 'report', 14, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(115, 'report', 15, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(116, 'report', 16, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(117, 'report', 17, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(118, 'report', 18, 5, 'YES', 'Admin', '2020-06-09 13:04:30'),
(119, 'report', 1, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(120, 'report', 2, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(121, 'report', 3, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(122, 'report', 4, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(123, 'report', 5, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(124, 'report', 6, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(125, 'report', 7, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(126, 'report', 8, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(127, 'report', 9, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(128, 'report', 10, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(129, 'report', 11, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(130, 'report', 12, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(131, 'report', 13, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(132, 'report', 14, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(133, 'report', 15, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(134, 'report', 16, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(135, 'report', 17, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(136, 'report', 18, 4, 'YES', 'Admin', '2020-06-09 13:04:30'),
(137, 'log', 16, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(138, 'log', 17, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(139, 'log', 30, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(140, 'log', 18, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(141, 'log', 19, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(142, 'log', 20, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(143, 'log', 21, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(144, 'log', 22, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(145, 'log', 23, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(146, 'log', 24, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(147, 'log', 25, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(148, 'log', 26, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(149, 'log', 27, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(150, 'log', 28, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(151, 'log', 29, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(152, 'log', 31, 6, 'NO', 'Admin', '2020-06-04 17:12:19'),
(153, 'log', 16, 12, 'YES', 'Admin', '2020-06-04 17:12:19'),
(154, 'log', 17, 12, 'YES', 'Admin', '2020-06-04 17:12:19'),
(155, 'log', 30, 12, 'YES', 'Admin', '2020-06-04 17:12:19'),
(156, 'log', 18, 12, 'YES', 'Admin', '2020-06-04 17:12:19'),
(157, 'log', 19, 12, 'NO', 'Admin', '2020-06-04 17:12:19'),
(158, 'log', 20, 12, 'NO', 'Admin', '2020-06-04 17:12:19'),
(159, 'log', 21, 12, 'NO', 'Admin', '2020-06-04 17:12:19'),
(160, 'log', 22, 12, 'NO', 'Admin', '2020-06-04 17:12:19'),
(161, 'log', 23, 12, 'YES', 'Admin', '2020-06-04 17:12:19'),
(162, 'log', 24, 12, 'NO', 'Admin', '2020-06-04 17:12:19'),
(163, 'log', 25, 12, 'NO', 'Admin', '2020-06-04 17:12:19'),
(164, 'log', 26, 12, 'YES', 'Admin', '2020-06-04 17:12:19'),
(165, 'log', 27, 12, 'YES', 'Admin', '2020-06-04 17:12:19'),
(166, 'log', 28, 12, 'YES', 'Admin', '2020-06-04 17:12:19'),
(167, 'log', 29, 12, 'YES', 'Admin', '2020-06-04 17:12:19'),
(168, 'log', 31, 12, 'YES', 'Admin', '2020-06-04 17:12:19'),
(169, 'report', 2, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(170, 'report', 1, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(171, 'report', 3, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(172, 'report', 4, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(173, 'report', 5, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(174, 'report', 6, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(175, 'report', 7, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(176, 'report', 8, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(177, 'report', 9, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(178, 'report', 10, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(179, 'report', 11, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(180, 'report', 12, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(181, 'report', 13, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(182, 'report', 14, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(183, 'report', 15, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(184, 'report', 16, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(185, 'report', 17, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(186, 'report', 18, 12, 'YES', 'Admin', '2020-06-09 13:04:30'),
(187, 'report', 2, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(188, 'report', 1, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(189, 'report', 3, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(190, 'report', 4, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(191, 'report', 5, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(192, 'report', 6, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(193, 'report', 7, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(194, 'report', 8, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(195, 'report', 9, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(196, 'report', 10, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(197, 'report', 11, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(198, 'report', 12, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(199, 'report', 13, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(200, 'report', 14, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(201, 'report', 15, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(202, 'report', 16, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(203, 'report', 17, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(204, 'report', 18, 6, 'NO', 'Admin', '2020-06-09 13:04:30'),
(205, 'master', 1, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(206, 'master', 2, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(207, 'master', 3, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(208, 'master', 4, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(209, 'master', 5, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(210, 'master', 6, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(211, 'master', 7, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(212, 'master', 8, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(213, 'master', 9, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(214, 'master', 10, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(215, 'master', 11, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(216, 'master', 12, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(217, 'master', 13, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(218, 'master', 14, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(219, 'master', 15, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(220, 'master', 16, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(221, 'master', 17, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(222, 'master', 18, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(223, 'master', 19, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(224, 'master', 20, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(225, 'master', 21, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(226, 'master', 22, 1, 'NO', 'Admin', '2020-06-09 13:04:30'),
(227, 'master', 1, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(228, 'master', 2, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(229, 'master', 3, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(230, 'master', 4, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(231, 'master', 5, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(232, 'master', 6, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(233, 'master', 7, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(234, 'master', 8, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(235, 'master', 9, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(236, 'master', 10, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(237, 'master', 11, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(238, 'master', 12, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(239, 'master', 13, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(240, 'master', 14, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(241, 'master', 15, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(242, 'master', 16, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(243, 'master', 17, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(244, 'master', 18, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(245, 'master', 19, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(246, 'master', 20, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(247, 'master', 21, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(248, 'master', 22, 2, 'NO', 'Admin', '2020-06-09 13:04:30'),
(249, 'master', 1, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(250, 'master', 2, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(251, 'master', 3, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(252, 'master', 4, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(253, 'master', 5, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(254, 'master', 6, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(255, 'master', 7, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(256, 'master', 8, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(257, 'master', 9, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(258, 'master', 10, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(259, 'master', 11, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(260, 'master', 12, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(261, 'master', 13, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(262, 'master', 14, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(263, 'master', 15, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(264, 'master', 16, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(265, 'master', 17, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(266, 'master', 18, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(267, 'master', 19, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(268, 'master', 20, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(269, 'master', 21, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(270, 'master', 22, 4, 'NO', 'Admin', '2020-06-09 13:04:30'),
(271, 'master', 1, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(272, 'master', 2, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(273, 'master', 3, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(274, 'master', 4, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(275, 'master', 5, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(276, 'master', 6, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(277, 'master', 7, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(278, 'master', 8, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(279, 'master', 9, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(280, 'master', 10, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(281, 'master', 11, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(282, 'master', 12, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(283, 'master', 13, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(284, 'master', 14, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(285, 'master', 15, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(286, 'master', 16, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(287, 'master', 17, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(288, 'master', 18, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(289, 'master', 19, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(290, 'master', 20, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(291, 'master', 21, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(292, 'master', 22, 5, 'NO', 'Admin', '2020-06-09 13:04:30'),
(293, 'master', 1, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(294, 'master', 2, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(295, 'master', 3, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(296, 'master', 4, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(297, 'master', 5, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(298, 'master', 6, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(299, 'master', 7, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(300, 'master', 8, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(301, 'master', 9, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(302, 'master', 10, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(303, 'master', 11, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(304, 'master', 12, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(305, 'master', 13, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(306, 'master', 14, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(307, 'master', 15, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(308, 'master', 16, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(309, 'master', 17, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(310, 'master', 18, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(311, 'master', 19, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(312, 'master', 20, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(313, 'master', 21, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(314, 'master', 22, 12, 'NO', 'Admin', '2020-06-09 13:04:30'),
(315, 'master', 1, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(316, 'master', 2, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(317, 'master', 3, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(318, 'master', 4, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(319, 'master', 5, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(320, 'master', 6, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(321, 'master', 7, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(322, 'master', 8, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(323, 'master', 9, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(324, 'master', 10, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(325, 'master', 11, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(326, 'master', 12, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(327, 'master', 13, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(328, 'master', 14, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(329, 'master', 15, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(330, 'master', 16, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(331, 'master', 17, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(332, 'master', 18, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(333, 'master', 19, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(334, 'master', 20, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(335, 'master', 21, 6, 'YES', 'Admin', '2020-06-09 13:04:30'),
(336, 'master', 22, 6, 'YES', 'Admin', '2020-06-09 13:04:30');

DROP TABLE IF EXISTS `pts_trn_log_file_details`;
CREATE TABLE IF NOT EXISTS `pts_trn_log_file_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_code` varchar(200) NOT NULL,
  `process_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `act_id` int(11) NOT NULL,
  `file_name` varchar(250) NOT NULL,
  `remark` varchar(250) NOT NULL,
  `role_id` int(11) NOT NULL,
  `uploaded_by` varchar(200) NOT NULL,
  `uploaded_by_id` int(11) NOT NULL,
  `uploaded_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

