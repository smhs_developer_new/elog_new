CREATE TABLE `getstart_stoped_data` (
`status` int(11)
,`area_code` varchar(50)
,`room_code` varchar(50)
,`status_name` varchar(250)
,`product_description` varchar(200)
,`document_no` varchar(50)
,`batch_no` varchar(50)
,`lot_no` varchar(50)
,`created_on` timestamp
,`operation_type` varchar(200)
,`created_by` varchar(250)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `getstatusrecord`
-- (See below for the actual view)
--
CREATE TABLE `getstatusrecord` (
`warid` int(11)
,`header_id` int(11)
,`status` int(11)
,`area_code` varchar(50)
,`room_code` varchar(50)
,`status_name` varchar(250)
,`product_description` varchar(200)
,`document_no` varchar(50)
,`batch_no` varchar(50)
,`lot_no` varchar(50)
,`created_on` timestamp
,`operation_type` varchar(200)
,`doneby` varchar(200)
,`actionby` varchar(200)
,`roleid` varchar(50)
,`actiontime` datetime
,`type` varchar(50)
);

-- --------------------------------------------------------

--
-- Table structure for table `log_tableaction`
--

CREATE TABLE `log_tableaction` (
  `id` int(11) NOT NULL,
  `action` varchar(200) DEFAULT NULL,
  `tablename` varchar(500) DEFAULT NULL,
  `tableid` int(11) DEFAULT NULL,
  `created_by` varchar(500) DEFAULT NULL,
  `created_on` varchar(500) DEFAULT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_tablechanges`
--

CREATE TABLE `log_tablechanges` (
  `id` int(11) NOT NULL,
  `log_id` int(11) DEFAULT NULL,
  `fieldname` varchar(500) DEFAULT NULL,
  `oldvalue` text,
  `newvalue` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mst_accessory_criticalparts`
--

CREATE TABLE `mst_accessory_criticalparts` (
  `id` bigint(20) NOT NULL,
  `part_code` varchar(50) DEFAULT NULL,
  `part_name` varchar(200) DEFAULT NULL,
  `part_description` varchar(2000) DEFAULT NULL,
  `part_type` varchar(200) DEFAULT NULL,
  `part_remark` varchar(1000) DEFAULT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_actions`
--

CREATE TABLE `mst_actions` (
  `id` int(11) NOT NULL,
  `actions` varchar(300) NOT NULL,
  `created_by` varchar(300) NOT NULL DEFAULT 'Admin',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(300) NOT NULL DEFAULT 'Admin',
  `modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_activity`
--

CREATE TABLE `mst_activity` (
  `id` bigint(20) NOT NULL,
  `activity_code` varchar(50) DEFAULT NULL,
  `activity_name` varchar(500) DEFAULT NULL,
  `activitytype_id` bigint(20) DEFAULT NULL,
  `activity_type` varchar(500) DEFAULT NULL,
  `activityfunc_tocall` varchar(256) NOT NULL,
  `activity_icon` varchar(500) DEFAULT NULL,
  `activity_nature` varchar(2000) DEFAULT NULL,
  `activity_duration` varchar(200) DEFAULT NULL,
  `activity_remark` varchar(2000) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_activity`
--
DELIMITER $$
CREATE TRIGGER `au_activityactivity` AFTER UPDATE ON `mst_activity` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_activity',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.activity_code != NEW.activity_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_code', OLD.activity_code, NEW.activity_code);
	  END IF;
	  
	  IF(OLD.activity_name != NEW.activity_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_name', OLD.activity_name, NEW.activity_name);
	  END IF;
	  
	  IF(OLD.activitytype_id != NEW.activitytype_id)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activitytype_id', OLD.activitytype_id, NEW.activitytype_id);
	  END IF;
	  
	 IF(OLD.activity_type != NEW.activity_type)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_type', OLD.activity_type, NEW.activity_type);
	  END IF; 
	  
	  IF(OLD.activityfunc_tocall != NEW.activityfunc_tocall)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activityfunc_tocall', OLD.activityfunc_tocall, NEW.activityfunc_tocall);
	  END IF;
	  IF(OLD.activity_icon != NEW.activity_icon)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_icon', OLD.activity_icon, NEW.activity_icon);
	  END IF;
	  IF(OLD.activity_nature != NEW.activity_nature)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_nature', OLD.activity_nature, NEW.activity_nature);
	  END IF;
	  IF(OLD.activity_duration != NEW.activity_duration)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_duration', OLD.activity_duration, NEW.activity_duration);
	  END IF;
             
          IF(OLD.activity_remark != NEW.activity_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_remark', OLD.activity_remark, NEW.activity_remark);
	  END IF;
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_code', OLD.area_code, NEW.area_code);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_activity', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_activitytype`
--

CREATE TABLE `mst_activitytype` (
  `id` bigint(20) NOT NULL,
  `activitytype_name` varchar(500) DEFAULT NULL,
  `activitytype_icon` varchar(500) DEFAULT NULL,
  `activityfunction_tocall` varchar(500) DEFAULT NULL,
  `activity_remarks` text,
  `activity_blockcolor` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(250) DEFAULT NULL,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Activity Type Master Table';

--
-- Triggers `mst_activitytype`
--
DELIMITER $$
CREATE TRIGGER `au_activitytypeactivity` AFTER UPDATE ON `mst_activitytype` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_activitytype',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.activitytype_name != NEW.activitytype_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activitytype_name', OLD.activitytype_name, NEW.activitytype_name);
	  END IF;
	  IF(OLD.activitytype_icon != NEW.activitytype_icon)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activitytype_icon', OLD.activitytype_icon, NEW.activitytype_icon);
	  END IF;
	  
	  IF(OLD.activityfunction_tocall != NEW.activityfunction_tocall)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activityfunction_tocall', OLD.activityfunction_tocall, NEW.activityfunction_tocall);
	  END IF;
	  IF(OLD.activity_remarks != NEW.activity_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_remarks', OLD.activity_remarks, NEW.activity_remarks);
	  END IF;
	  IF(OLD.activity_blockcolor != NEW.activity_blockcolor)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_blockcolor', OLD.activity_blockcolor, NEW.activity_blockcolor);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_activitytype', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_area`
--

CREATE TABLE `mst_area` (
  `id` int(11) NOT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `area_name` varchar(250) DEFAULT NULL,
  `area_address` varchar(4000) DEFAULT NULL,
  `area_remarks` varchar(2000) DEFAULT NULL,
  `area_contact` varchar(20) DEFAULT NULL,
  `area_sop_no` varchar(20) NOT NULL,
  `block_code` varchar(50) DEFAULT NULL,
  `department_code` varchar(200) NOT NULL,
  `number_of_rooms` int(11) NOT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_area`
--
DELIMITER $$
CREATE TRIGGER `au_areaactivity` AFTER UPDATE ON `mst_area` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_area',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_code', OLD.area_code, NEW.area_code);
	  END IF;
	  
	  IF(OLD.area_name != NEW.area_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_name', OLD.area_name, NEW.area_name);
	  END IF;
	  
	  IF(OLD.area_address != NEW.area_address)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_address', OLD.area_address, NEW.area_address);
	  END IF;
	  
	 IF(OLD.area_contact != NEW.area_contact)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_contact', OLD.area_contact, NEW.area_contact);
	  END IF; 
	  
	  
	  IF(OLD.area_remarks != NEW.area_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_remarks', OLD.area_remarks, NEW.area_remarks);
	  END IF; 
	  
	  IF(OLD.area_sop_no != NEW.area_sop_no)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_sop_no', OLD.area_sop_no, NEW.area_sop_no);
	  END IF;
	  IF(OLD.number_of_rooms != NEW.number_of_rooms)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'number_of_rooms', OLD.number_of_rooms, NEW.number_of_rooms);
	  END IF;
	  IF(OLD.block_code != NEW.block_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_code', OLD.block_code, NEW.block_code);
	  END IF; 
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_area', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_block`
--

CREATE TABLE `mst_block` (
  `id` bigint(20) NOT NULL,
  `block_code` varchar(50) DEFAULT NULL,
  `block_name` varchar(2000) DEFAULT NULL,
  `block_contact` varchar(20) DEFAULT NULL,
  `block_remarks` varchar(2000) DEFAULT NULL,
  `number_of_areas` int(11) DEFAULT NULL,
  `plant_code` varchar(50) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_block`
--
DELIMITER $$
CREATE TRIGGER `au_blockactivity` AFTER UPDATE ON `mst_block` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_block',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	 IF(OLD.block_code != NEW.block_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_code', OLD.block_code, NEW.block_code);
	  END IF;
	  
	  IF(OLD.block_name != NEW.block_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_name', OLD.block_name, NEW.block_name);
	  END IF; 
	  
	 IF(OLD.block_contact != NEW.block_contact)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_contact', OLD.block_contact, NEW.block_contact);
	  END IF; 
	  
	  
	  IF(OLD.block_remarks != NEW.block_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_remarks', OLD.block_remarks, NEW.block_remarks);
	  END IF; 
	  
	  
	  IF(OLD.number_of_areas != NEW.number_of_areas)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'number_of_areas', OLD.number_of_areas, NEW.number_of_areas);
	  END IF;
	  IF(OLD.plant_code != NEW.plant_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_code', OLD.plant_code, NEW.plant_code);
	  END IF; 
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_block', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_department`
--

CREATE TABLE `mst_department` (
  `id` bigint(20) NOT NULL,
  `department_code` varchar(50) DEFAULT NULL,
  `department_name` varchar(500) DEFAULT NULL,
  `department_address` varchar(2000) DEFAULT NULL,
  `department_contact` varchar(20) DEFAULT NULL,
  `department_remark` varchar(2000) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `block_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_department`
--
DELIMITER $$
CREATE TRIGGER `au_departmentactivity` AFTER UPDATE ON `mst_department` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_department',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	 IF(OLD.department_code != NEW.department_code)
	  THEN
	  
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'department_code', OLD.department_code, NEW.department_code);
	  END IF;
	  IF(OLD.department_name != NEW.department_name)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'department_name', OLD.department_name, NEW.department_name);
	  END IF; 
	  IF(OLD.department_address != NEW.department_address)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'department_address', OLD.department_address, NEW.department_address);
	  END IF; 
	  IF(OLD.department_contact != NEW.department_contact)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'department_contact', OLD.department_contact, NEW.department_contact);
	  END IF; 
	  IF(OLD.department_remark != NEW.department_remark)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'department_remark', OLD.department_remark, NEW.department_remark);
	  END IF; 
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_code', OLD.area_code, NEW.area_code);
	  END IF; 
	  IF(OLD.block_code != NEW.block_code)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_code', OLD.block_code, NEW.block_code);
	  END IF; 
	  END IF;
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_department', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_document`
--

CREATE TABLE `mst_document` (
  `id` bigint(20) NOT NULL,
  `doc_id` varchar(50) DEFAULT NULL,
  `remarks` varchar(2000) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_document`
--
DELIMITER $$
CREATE TRIGGER `au_documentactivity` AFTER UPDATE ON `mst_document` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_document',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	 IF(OLD.doc_id != NEW.doc_id)
	  THEN
	  
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'doc_id', OLD.doc_id, NEW.doc_id);
	  END IF;
	  IF(OLD.remarks != NEW.remarks)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'remarks', OLD.remarks, NEW.remarks);
	  END IF; 
	  END IF;
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_document', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_drainpoint`
--

CREATE TABLE `mst_drainpoint` (
  `id` bigint(20) NOT NULL,
  `drainpoint_code` varchar(50) DEFAULT NULL,
  `drainpoint_name` varchar(500) DEFAULT NULL,
  `drainpoint_remark` varchar(2000) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `solution_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_drainpoint`
--
DELIMITER $$
CREATE TRIGGER `au_drainpointactivity` AFTER UPDATE ON `mst_drainpoint` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_drainpoint',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.drainpoint_code != NEW.drainpoint_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'drainpoint_code', OLD.drainpoint_code, NEW.drainpoint_code);
	  END IF;
	  
	  IF(OLD.drainpoint_name != NEW.drainpoint_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'drainpoint_name', OLD.drainpoint_name, NEW.drainpoint_name);
	  END IF;
	  
	  IF(OLD.drainpoint_remark != NEW.drainpoint_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'drainpoint_remark', OLD.drainpoint_remark, NEW.drainpoint_remark);
	  END IF; 
	  
	  
	  IF(OLD.room_code != NEW.room_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_code', OLD.room_code, NEW.room_code);
	  END IF; 
          
	  IF(OLD.solution_code != NEW.solution_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'solution_code', OLD.solution_code, NEW.solution_code);
	  END IF;	 
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_drainpoint', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_employee`
--

CREATE TABLE `mst_employee` (
  `id` bigint(20) NOT NULL,
  `emp_code` varchar(50) DEFAULT NULL,
  `emp_name` varchar(250) DEFAULT NULL,
  `email2` varchar(200) DEFAULT NULL,
  `emp_address` varchar(2000) DEFAULT NULL,
  `emp_contact` bigint(20) DEFAULT NULL,
  `emp_email` varchar(500) DEFAULT NULL,
  `emp_password` varchar(500) DEFAULT NULL,
  `designation_code` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `role_code` varchar(50) DEFAULT NULL,
  `block_code` varchar(2000) DEFAULT NULL,
  `Sub_block_code` varchar(2000) DEFAULT NULL,
  `area_code` varchar(2000) DEFAULT NULL,
  `emp_remark` varchar(2000) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `is_blocked` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_equipment`
--

CREATE TABLE `mst_equipment` (
  `id` bigint(20) NOT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `equipment_name` varchar(250) DEFAULT NULL,
  `equipment_icon` varchar(500) DEFAULT NULL,
  `equipment_type` varchar(500) DEFAULT NULL,
  `equipment_remarks` varchar(2000) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `block_code` varchar(50) DEFAULT NULL,
  `drain_point_code` varchar(50) DEFAULT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_inused` bit(1) NOT NULL DEFAULT b'0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_equipment`
--
DELIMITER $$
CREATE TRIGGER `au_equipmentactivity` AFTER UPDATE ON `mst_equipment` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_equipment',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.equipment_code != NEW.equipment_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'equipment_code', OLD.equipment_code, NEW.equipment_code);
	  END IF;
	  
	  IF(OLD.equipment_name != NEW.equipment_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'equipment_name', OLD.equipment_name, NEW.equipment_name);
	  END IF;
	  
	  IF(OLD.equipment_icon != NEW.equipment_icon)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'equipment_icon', OLD.equipment_icon, NEW.equipment_icon);
	  END IF;
	  
	 IF(OLD.equipment_type != NEW.equipment_type)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'equipment_type', OLD.equipment_type, NEW.equipment_type);
	  END IF; 
	  
	  
	  IF(OLD.equipment_remarks != NEW.equipment_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'equipment_remarks', OLD.equipment_remarks, NEW.equipment_remarks);
	  END IF; 
	  
	  
	  IF(OLD.room_code != NEW.room_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_code', OLD.room_code, NEW.room_code);
	  END IF; 
          
	  IF(OLD.drain_point_code != NEW.drain_point_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'drain_point_code', OLD.drain_point_code, NEW.drain_point_code);
	  END IF; 
	  
	  IF(OLD.sop_code != NEW.sop_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_code', OLD.sop_code, NEW.sop_code);
	  END IF; 
	  
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_equipment', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_equipment-old`
--

CREATE TABLE `mst_equipment-old` (
  `id` bigint(20) NOT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `equipment_name` varchar(250) DEFAULT NULL,
  `equipment_icon` varchar(500) DEFAULT NULL,
  `equipment_type` varchar(500) DEFAULT NULL,
  `equipment_remarks` varchar(2000) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `drain_point_code` varchar(50) DEFAULT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_holiday`
--

CREATE TABLE `mst_holiday` (
  `id` bigint(20) NOT NULL,
  `holiday_code` varchar(50) DEFAULT NULL,
  `holiday_name` varchar(250) DEFAULT NULL,
  `holiday_date` datetime DEFAULT NULL,
  `holiday_type` varchar(500) DEFAULT NULL,
  `holiday_remark` varchar(2000) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_holiday`
--
DELIMITER $$
CREATE TRIGGER `au_holidayactivity` AFTER UPDATE ON `mst_holiday` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_holiday',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.holiday_code != NEW.holiday_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'holiday_code', OLD.holiday_code, NEW.holiday_code);
	  END IF;
	  
	  IF(OLD.holiday_name != NEW.holiday_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'holiday_name', OLD.holiday_name, NEW.holiday_name);
	  END IF;
	  
	  IF(OLD.holiday_date != NEW.holiday_date)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'holiday_date', OLD.holiday_date, NEW.holiday_date);
	  END IF;
	  
	 IF(OLD.holiday_type != NEW.holiday_type)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'holiday_type', OLD.holiday_type, NEW.holiday_type);
	  END IF; 
	  
	  
	  IF(OLD.holiday_remark != NEW.holiday_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'holiday_remark', OLD.holiday_remark, NEW.holiday_remark);
	  END IF; 
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_holiday', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_inj_cleaning`
--

CREATE TABLE `mst_inj_cleaning` (
  `id` int(20) NOT NULL,
  `description` varchar(300) NOT NULL,
  `type` varchar(100) NOT NULL,
  `seq` int(10) NOT NULL,
  `priority` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_inj_solution`
--

CREATE TABLE `mst_inj_solution` (
  `id` bigint(20) NOT NULL,
  `solution_code` varchar(50) DEFAULT NULL,
  `solution_name` varchar(500) DEFAULT NULL,
  `Block_Code` varchar(50) DEFAULT NULL,
  `Precautions` varchar(1000) DEFAULT NULL,
  `Short_Name` varchar(200) DEFAULT NULL,
  `sol_qty` varchar(200) DEFAULT NULL,
  `water_qty` varchar(200) DEFAULT NULL,
  `wfi_qs` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_messages`
--

CREATE TABLE `mst_messages` (
  `id` int(11) NOT NULL,
  `msg-header` varchar(50) NOT NULL,
  `message` varchar(500) DEFAULT NULL,
  `created_by` varchar(200) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_plant`
--

CREATE TABLE `mst_plant` (
  `id` bigint(20) NOT NULL,
  `plant_code` varchar(50) DEFAULT NULL,
  `plant_name` varchar(250) DEFAULT NULL,
  `plant_address` varchar(2000) DEFAULT NULL,
  `plant_remarks` varchar(4000) DEFAULT NULL,
  `plant_contact` bigint(20) DEFAULT NULL,
  `number_of_blocks` int(11) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_plant`
--
DELIMITER $$
CREATE TRIGGER `au_plantactivity` AFTER UPDATE ON `mst_plant` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_plant',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	 IF(OLD.plant_code != NEW.plant_code)
	  THEN
	  
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_code', OLD.plant_code, NEW.plant_code);
	  END IF;
	  IF(OLD.plant_name != NEW.plant_name)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_name', OLD.plant_name, NEW.plant_name);
	  END IF; 
	  
	  IF(OLD.plant_address != NEW.plant_address)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_address', OLD.plant_address, NEW.plant_address);
	  END IF; 
	  
	  IF(OLD.plant_contact != NEW.plant_contact)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_contact', OLD.plant_contact, NEW.plant_contact);
	  END IF; 
	  
	  
	  IF(OLD.plant_remarks != NEW.plant_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_remarks', OLD.plant_remarks, NEW.plant_remarks);
	  END IF; 
	  
	  
	  IF(OLD.number_of_blocks != NEW.number_of_blocks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'number_of_blocks', OLD.number_of_blocks, NEW.number_of_blocks);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_plant', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_procedure`
--

CREATE TABLE `mst_procedure` (
  `id` bigint(20) NOT NULL,
  `procedure_code` varchar(50) DEFAULT NULL,
  `procedure_name` varchar(1000) DEFAULT NULL,
  `solution_code` varchar(50) DEFAULT NULL,
  `is_active` bit(1) DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_product`
--

CREATE TABLE `mst_product` (
  `id` bigint(20) NOT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `product_name` varchar(500) DEFAULT NULL,
  `product_market` varchar(500) DEFAULT NULL,
  `product_remark` varchar(2000) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_product`
--
DELIMITER $$
CREATE TRIGGER `au_productactivity` AFTER UPDATE ON `mst_product` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_product',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.product_code != NEW.product_code)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'product_code', OLD.product_code, NEW.product_code);
	  END IF;
	  IF(OLD.product_name != NEW.product_name)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'product_name', OLD.product_name, NEW.product_name);
	  END IF;
	  
	  IF(OLD.product_market != NEW.product_market)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'product_market', OLD.product_market, NEW.product_market);
	  END IF;
	  IF(OLD.product_remark != NEW.product_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'product_remark', OLD.product_remark, NEW.product_remark);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_product', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_product_process`
--

CREATE TABLE `mst_product_process` (
  `id` bigint(20) NOT NULL,
  `process_code` varchar(50) DEFAULT NULL,
  `process_name` varchar(500) DEFAULT NULL,
  `process_type` varchar(500) DEFAULT NULL,
  `process_duration` varchar(200) DEFAULT NULL,
  `process_remark` varchar(2000) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_role`
--

CREATE TABLE `mst_role` (
  `id` bigint(20) NOT NULL,
  `role_level` int(11) DEFAULT NULL,
  `role_code` varchar(50) DEFAULT NULL,
  `role_type` varchar(500) DEFAULT NULL,
  `role_description` varchar(2000) DEFAULT NULL,
  `role_remark` varchar(2000) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_role`
--
DELIMITER $$
CREATE TRIGGER `au_roleactivity` AFTER UPDATE ON `mst_role` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_role',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.role_code != NEW.role_code)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'role_code', OLD.role_code, NEW.role_code);
	  END IF;
	  IF(OLD.role_type != NEW.role_type)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'role_type', OLD.role_type, NEW.role_type);
	  END IF;
	  
	  IF(OLD.role_description != NEW.role_description)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'role_description', OLD.role_description, NEW.role_description);
	  END IF;
	  IF(OLD.role_remark != NEW.role_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'role_remark', OLD.role_remark, NEW.role_remark);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_role', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_roleid_workflow_mapping`
--

CREATE TABLE `mst_roleid_workflow_mapping` (
  `id` bigint(20) NOT NULL,
  `status_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(500) NOT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_roleid_workflow_mapping`
--
DELIMITER $$
CREATE TRIGGER `au_roleworkflowactivity` AFTER UPDATE ON `mst_roleid_workflow_mapping` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_roleid_workflow_mapping',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.status_id != NEW.status_id)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'status_id', OLD.status_id, NEW.status_id);
	  END IF;
	  IF(OLD.role_id != NEW.role_id)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'role_id', OLD.role_id, NEW.role_id);
	  END IF;	  
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_roleid_workflow_mapping', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_room`
--

CREATE TABLE `mst_room` (
  `id` bigint(20) NOT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `room_name` varchar(250) DEFAULT NULL,
  `room_address` varchar(4000) DEFAULT NULL,
  `room_remarks` varchar(2000) DEFAULT NULL,
  `room_contact` bigint(20) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `in_use` int(1) DEFAULT '0',
  `inuse_activity` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_room`
--
DELIMITER $$
CREATE TRIGGER `au_roomactivity` AFTER UPDATE ON `mst_room` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_room',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.room_code != NEW.room_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_code', OLD.room_code, NEW.room_code);
	  END IF;
	  
	  IF(OLD.room_name != NEW.room_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_name', OLD.room_name, NEW.room_name);
	  END IF;
	  
	  IF(OLD.room_address != NEW.room_address)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_address', OLD.room_address, NEW.room_address);
	  END IF;
	  
	 IF(OLD.room_contact != NEW.room_contact)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_contact', OLD.room_contact, NEW.room_contact);
	  END IF; 
	  
	  
	  IF(OLD.room_remarks != NEW.room_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_remarks', OLD.room_remarks, NEW.room_remarks);
	  END IF; 
	  
	  
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_code', OLD.area_code, NEW.area_code);
	  END IF; 
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_room', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_shift`
--

CREATE TABLE `mst_shift` (
  `id` int(11) NOT NULL,
  `shift_code` varchar(50) DEFAULT NULL,
  `shift_starttime` datetime DEFAULT NULL,
  `shift_endtime` datetime DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mst_solution`
--

CREATE TABLE `mst_solution` (
  `id` bigint(20) NOT NULL,
  `solution_code` varchar(50) DEFAULT NULL,
  `solution_name` varchar(500) DEFAULT NULL,
  `Block_Code` varchar(50) DEFAULT NULL,
  `Precautions` varchar(1000) DEFAULT NULL,
  `Short_Name` varchar(200) DEFAULT NULL,
  `sol_qty` varchar(200) DEFAULT NULL,
  `water_qty` varchar(200) DEFAULT NULL,
  `tot_qty` varchar(200) DEFAULT NULL,
  `UoM` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_solution`
--
DELIMITER $$
CREATE TRIGGER `au_solutionactivity` AFTER UPDATE ON `mst_solution` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_solution',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.solution_code != NEW.solution_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'solution_code', OLD.solution_code, NEW.solution_code);
	  END IF;
	  
	  IF(OLD.solution_name != NEW.solution_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'solution_name', OLD.solution_name, NEW.solution_name);
	  END IF;
	  
	  IF(OLD.Block_Code != NEW.Block_Code)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'Block_Code', OLD.Block_Code, NEW.Block_Code);
	  END IF;
	  
	 IF(OLD.Precautions != NEW.Precautions)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'Precautions', OLD.Precautions, NEW.Precautions);
	  END IF; 
	  
	  IF(OLD.Short_Name != NEW.Short_Name)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'Short_Name', OLD.Short_Name, NEW.Short_Name);
	  END IF;
	  IF(OLD.sol_qty != NEW.sol_qty)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sol_qty', OLD.sol_qty, NEW.sol_qty);
	  END IF;
	  IF(OLD.water_qty != NEW.water_qty)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'water_qty', OLD.water_qty, NEW.water_qty);
	  END IF;
	  IF(OLD.tot_qty != NEW.tot_qty)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'tot_qty', OLD.tot_qty, NEW.tot_qty);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_solution', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_sop`
--

CREATE TABLE `mst_sop` (
  `id` bigint(20) NOT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `sop_name` varchar(500) DEFAULT NULL,
  `sop_type` varchar(500) DEFAULT NULL,
  `sop_frequency` varchar(500) DEFAULT NULL,
  `sop_remark` varchar(2000) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `activity_code` varchar(50) DEFAULT NULL,
  `filepath` varchar(1000) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_sop`
--
DELIMITER $$
CREATE TRIGGER `au_sopactivity` AFTER UPDATE ON `mst_sop` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_sop',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.sop_code != NEW.sop_code)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_code', OLD.sop_code, NEW.sop_code);
	  END IF;
	  IF(OLD.sop_name != NEW.sop_name)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_name', OLD.sop_name, NEW.sop_name);
	  END IF;
	  
	  IF(OLD.sop_type != NEW.sop_type)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_type', OLD.sop_type, NEW.sop_type);
	  END IF;
	  IF(OLD.sop_frequency != NEW.sop_frequency)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_frequency', OLD.sop_frequency, NEW.sop_frequency);
	  END IF;
	  IF(OLD.sop_remark != NEW.sop_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_remark', OLD.sop_remark, NEW.sop_remark);
	  END IF;
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_code', OLD.area_code, NEW.area_code);
	  END IF;
	  IF(OLD.activity_code != NEW.activity_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_code', OLD.activity_code, NEW.activity_code);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_sop', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `mst_status`
--

CREATE TABLE `mst_status` (
  `id` bigint(20) NOT NULL,
  `status_name` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(250) DEFAULT NULL,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `mst_status`
--
DELIMITER $$
CREATE TRIGGER `au_statusactivity` AFTER UPDATE ON `mst_status` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_status',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.status_name != NEW.status_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'status_name', OLD.status_name, NEW.status_name);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_status', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `operationheader_audit_log`
--

CREATE TABLE `operationheader_audit_log` (
  `id` int(11) NOT NULL,
  `tablename` varchar(250) NOT NULL,
  `tableid` int(11) NOT NULL,
  `action` varchar(250) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `operationsqnlog`
-- (See below for the actual view)
--
CREATE TABLE `operationsqnlog` (
`id` bigint(20)
,`document_no` varchar(50)
,`lot_no` varchar(50)
,`area_code` varchar(50)
,`room_code` varchar(50)
,`batch_no` varchar(50)
,`product_description` varchar(200)
,`operation_type` varchar(200)
,`start_time` varchar(50)
,`end_time` varchar(50)
,`started_by` varchar(250)
,`ended_by` varchar(250)
,`names` text
,`approvedby` text
,`approvedon` text
,`equipments` text
);

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_activity`
--

CREATE TABLE `pts_mst_activity` (
  `id` int(10) NOT NULL,
  `activity_name` varchar(80) DEFAULT NULL,
  `activity_url` varchar(100) DEFAULT NULL,
  `is_need_approval` enum('yes','no') DEFAULT 'yes',
  `workflowtype` varchar(200) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `type` varchar(50) DEFAULT NULL,
  `room_id` int(10) DEFAULT NULL,
  `created_date` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_balance_calibration`
--

CREATE TABLE `pts_mst_balance_calibration` (
  `id` int(11) NOT NULL,
  `balance_no` varchar(100) NOT NULL,
  `capacity` varchar(100) NOT NULL,
  `least_count` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_balance_calibration_standard`
--

CREATE TABLE `pts_mst_balance_calibration_standard` (
  `id` int(11) NOT NULL,
  `balance_calibration_id` int(11) NOT NULL,
  `measurement_unit` varchar(20) NOT NULL,
  `standard_value` float NOT NULL,
  `min_value` float NOT NULL,
  `max_value` float NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_balance_frequency_data`
--

CREATE TABLE `pts_mst_balance_frequency_data` (
  `id` int(11) NOT NULL,
  `balance_no` varchar(100) NOT NULL,
  `frequency_id` int(11) NOT NULL,
  `measurement_unit` varchar(20) NOT NULL,
  `standard_value` varchar(20) NOT NULL,
  `min_value` varchar(20) NOT NULL,
  `max_value` varchar(20) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_by` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_document`
--

CREATE TABLE `pts_mst_document` (
  `id` bigint(20) NOT NULL,
  `prefix` varchar(50) DEFAULT NULL,
  `docno` bigint(50) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_filter`
--

CREATE TABLE `pts_mst_filter` (
  `id` int(10) NOT NULL,
  `filter_name` varchar(100) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `filter_desc` varchar(150) DEFAULT NULL,
  `status` enum('on','off') DEFAULT 'on',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_frequency`
--

CREATE TABLE `pts_mst_frequency` (
  `id` int(10) NOT NULL,
  `frequency_name` varchar(100) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_id_standard_weight`
--

CREATE TABLE `pts_mst_id_standard_weight` (
  `id` int(11) NOT NULL,
  `id_no_statndard_weight` varchar(100) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `weight` float NOT NULL,
  `measurement_unit` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_instrument`
--

CREATE TABLE `pts_mst_instrument` (
  `id` int(11) NOT NULL,
  `block_code` varchar(20) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `equipment_name` varchar(100) NOT NULL,
  `equipment_code` varchar(100) NOT NULL,
  `equipment_desc` varchar(200) DEFAULT 'NA',
  `equipment_type` enum('Instrument','Apparatus','Samplingrod') NOT NULL DEFAULT 'Instrument',
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_line_log`
--

CREATE TABLE `pts_mst_line_log` (
  `id` int(11) NOT NULL,
  `block_code` varchar(50) DEFAULT NULL,
  `area_code` varchar(50) NOT NULL,
  `room_code` varchar(100) NOT NULL,
  `line_log_code` varchar(100) NOT NULL,
  `line_log_name` varchar(100) NOT NULL,
  `equipment_code` varchar(100) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_material_relocation`
--

CREATE TABLE `pts_mst_material_relocation` (
  `id` int(10) NOT NULL,
  `material_name` varchar(100) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `material_code` varchar(100) NOT NULL,
  `material_desc` varchar(150) DEFAULT NULL,
  `material_type` varchar(150) DEFAULT NULL,
  `material_batch_no` varchar(100) DEFAULT NULL,
  `material_relocation_type` varchar(100) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_role_level`
--

CREATE TABLE `pts_mst_role_level` (
  `id` int(11) NOT NULL,
  `roleid` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_role_workassign_to_mult_roles`
--

CREATE TABLE `pts_mst_role_workassign_to_mult_roles` (
  `id` int(11) NOT NULL,
  `roleid` int(11) DEFAULT NULL,
  `workassign_to_roleid` int(11) DEFAULT NULL,
  `prty_btn_text` enum('on','off') NOT NULL DEFAULT 'off',
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_stages`
--

CREATE TABLE `pts_mst_stages` (
  `id` int(11) NOT NULL,
  `stage_name` varchar(100) NOT NULL,
  `stage_desc` varchar(100) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_sys_id`
--

CREATE TABLE `pts_mst_sys_id` (
  `id` int(10) NOT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `room_name` varchar(60) DEFAULT NULL,
  `room_code` varchar(20) DEFAULT NULL,
  `area_id` varchar(60) DEFAULT NULL,
  `room_id` int(10) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL,
  `device_id` varchar(30) DEFAULT NULL,
  `logger_id` varchar(50) DEFAULT NULL,
  `gauge_id` varchar(50) DEFAULT NULL,
  `room_type` enum('room','corridor') NOT NULL DEFAULT 'room'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pts_mst_tablet_tooling_log`
--

CREATE TABLE `pts_mst_tablet_tooling_log` (
  `id` int(11) NOT NULL,
  `product_code` varchar(100) NOT NULL,
  `supplier` varchar(100) NOT NULL,
  `upper_punch_qty` float NOT NULL,
  `lower_punch_qty` float NOT NULL,
  `year` int(11) NOT NULL,
  `dimension` varchar(100) NOT NULL,
  `shape` varchar(100) NOT NULL,
  `machine` varchar(100) NOT NULL,
  `die_quantity` float NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_activity_approval_log`
--

CREATE TABLE `pts_trn_activity_approval_log` (
  `id` int(10) NOT NULL,
  `usr_activity_log_id` int(11) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `approval_status` enum('approve','reject','N/A') DEFAULT 'N/A',
  `workflow_type` varchar(50) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `activity_remarks` varchar(240) DEFAULT NULL,
  `role_id` int(10) DEFAULT NULL,
  `activity_time` int(10) DEFAULT NULL,
  `reason` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_activity_log`
--

CREATE TABLE `pts_trn_activity_log` (
  `id` int(10) NOT NULL,
  `usr_activity_log_id` int(11) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `activity_status` varchar(50) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `remarks` varchar(240) DEFAULT NULL,
  `role_id` int(10) DEFAULT NULL,
  `activity_time` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_balance_calibration`
--

CREATE TABLE `pts_trn_balance_calibration` (
  `id` int(11) NOT NULL,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(100) NOT NULL,
  `balance_no` varchar(100) DEFAULT NULL,
  `frequency_id` int(11) DEFAULT NULL,
  `standerd_wt` float DEFAULT NULL,
  `id_no_of_st_wt` varchar(50) DEFAULT NULL,
  `oberved_wt` float DEFAULT NULL,
  `diff` float DEFAULT NULL,
  `sprit_level_status` enum('ok','notok','NA') DEFAULT 'NA',
  `time_status` enum('ok','notok','NA') DEFAULT 'NA',
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_batchin`
--

CREATE TABLE `pts_trn_batchin` (
  `id` bigint(20) NOT NULL,
  `room_code` varchar(100) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_desc` varchar(300) NOT NULL,
  `emp_code` varchar(50) DEFAULT NULL,
  `emp_name` varchar(250) DEFAULT NULL,
  `emp_email` varchar(500) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `batch_in_time` int(11) DEFAULT NULL,
  `batch_out_time` int(11) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_emp_roomin`
--

CREATE TABLE `pts_trn_emp_roomin` (
  `id` bigint(20) NOT NULL,
  `room_code` varchar(100) NOT NULL,
  `emp_code` varchar(50) DEFAULT NULL,
  `emp_name` varchar(250) DEFAULT NULL,
  `emp_email` varchar(500) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `session_id` varchar(200) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `room_in_time` int(11) DEFAULT NULL,
  `room_out_time` int(11) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_env_cond_diff`
--

CREATE TABLE `pts_trn_env_cond_diff` (
  `id` int(11) NOT NULL,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) NOT NULL,
  `data_log_id` varchar(50) DEFAULT NULL,
  `magnelic_id` varchar(50) DEFAULT NULL,
  `pressure_diff` enum('ok','notok','NA') NOT NULL DEFAULT 'NA',
  `reading` float DEFAULT NULL,
  `temp` float DEFAULT NULL,
  `temp_min` float DEFAULT NULL,
  `temp_max` float DEFAULT NULL,
  `rh` float DEFAULT NULL,
  `rh_min` float DEFAULT NULL,
  `rh_max` float DEFAULT NULL,
  `temp_from` varchar(50) DEFAULT NULL,
  `temp_to` varchar(50) DEFAULT NULL,
  `rh_from` varchar(50) DEFAULT NULL,
  `rh_to` varchar(50) DEFAULT NULL,
  `globe_pressure_diff` varchar(50) DEFAULT NULL,
  `from_pressure` varchar(50) DEFAULT NULL,
  `to_pressure` varchar(50) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(200) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_equip_appratus_log`
--

CREATE TABLE `pts_trn_equip_appratus_log` (
  `id` int(11) NOT NULL,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(100) NOT NULL,
  `equip_appratus_no` varchar(200) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `stage` varchar(250) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  `cleaning_verification` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_instrument_log_register`
--

CREATE TABLE `pts_trn_instrument_log_register` (
  `id` int(11) NOT NULL,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(100) NOT NULL,
  `instrument_code` varchar(50) DEFAULT NULL,
  `stage` varchar(100) DEFAULT NULL,
  `test` varchar(100) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `result_left` enum('ok','notok','NA') NOT NULL DEFAULT 'NA',
  `result_right` enum('ok','notok','NA') NOT NULL DEFAULT 'NA',
  `remark` varchar(500) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_laf_pressure_diff`
--

CREATE TABLE `pts_trn_laf_pressure_diff` (
  `id` int(11) NOT NULL,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(100) NOT NULL,
  `booth_no` varchar(200) DEFAULT NULL,
  `m1` varchar(100) DEFAULT NULL,
  `m2` varchar(100) DEFAULT NULL,
  `g1` varchar(100) DEFAULT NULL,
  `g2` varchar(100) DEFAULT NULL,
  `g3` varchar(100) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_material_retreival_relocation`
--

CREATE TABLE `pts_trn_material_retreival_relocation` (
  `id` int(11) NOT NULL,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) NOT NULL,
  `material_code` varchar(100) DEFAULT NULL,
  `material_condition` varchar(10) DEFAULT NULL,
  `material_batch_no` varchar(50) NOT NULL,
  `material_type` varchar(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_pre_filter_cleaning`
--

CREATE TABLE `pts_trn_pre_filter_cleaning` (
  `id` int(11) NOT NULL,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(100) NOT NULL,
  `selection_status` enum('from','to') NOT NULL DEFAULT 'from',
  `product_code` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `pre_filter_cleaning` enum('yes','no','NA') NOT NULL DEFAULT 'NA',
  `outer_surface_cleaning` enum('yes','no','NA') NOT NULL DEFAULT 'NA',
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_return_air_filter`
--

CREATE TABLE `pts_trn_return_air_filter` (
  `id` int(11) NOT NULL,
  `doc_no` varchar(100) NOT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `product_code` varchar(100) NOT NULL,
  `batch_no` varchar(100) NOT NULL,
  `frequency_id` int(11) NOT NULL,
  `filter` varchar(500) NOT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(100) NOT NULL,
  `done_by_role_id` int(11) NOT NULL,
  `done_by_email` varchar(100) NOT NULL,
  `done_by_remark` varchar(200) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_room_log_activity`
--

CREATE TABLE `pts_trn_room_log_activity` (
  `id` int(10) NOT NULL,
  `activity_name` varchar(80) DEFAULT NULL,
  `activity_url` varchar(100) NOT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `room_id` int(10) DEFAULT NULL,
  `mst_act_id` int(11) NOT NULL,
  `created_date` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_swab_sample_record`
--

CREATE TABLE `pts_trn_swab_sample_record` (
  `id` int(11) NOT NULL,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(100) NOT NULL,
  `ar_numer` varchar(50) DEFAULT NULL,
  `equipment_id` varchar(50) DEFAULT NULL,
  `equipment_desc` varchar(1000) DEFAULT NULL,
  `pre_product_code` varchar(50) DEFAULT NULL,
  `pre_barch_no` varchar(50) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_tablet_tooling`
--

CREATE TABLE `pts_trn_tablet_tooling` (
  `id` int(11) NOT NULL,
  `mst_matrial_id` int(11) DEFAULT NULL,
  `doc_no` varchar(20) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `act_type` varchar(50) DEFAULT NULL,
  `U` varchar(50) DEFAULT NULL,
  `L` varchar(50) DEFAULT NULL,
  `D` varchar(50) DEFAULT NULL,
  `tablet_tooling_from` varchar(50) DEFAULT NULL,
  `tablet_tooling_to` varchar(50) DEFAULT NULL,
  `damaged` varchar(50) DEFAULT NULL,
  `b_no` varchar(50) DEFAULT NULL,
  `tab_qty` varchar(50) DEFAULT NULL,
  `cum_qty` varchar(50) DEFAULT NULL,
  `cum_qty_punch` varchar(50) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `done_by_user_id` varchar(50) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` varchar(50) DEFAULT NULL,
  `done_by_email` varchar(50) DEFAULT NULL,
  `done_by_remark` varchar(50) DEFAULT NULL,
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_user_activity_log`
--

CREATE TABLE `pts_trn_user_activity_log` (
  `id` int(10) NOT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `roomlogactivity` int(10) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `room_id` int(10) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `product_code` varchar(100) NOT NULL,
  `batch_no` varchar(100) NOT NULL,
  `equip_code` varchar(500) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `activity_remarks` varchar(240) DEFAULT NULL,
  `doc_id` varchar(120) DEFAULT NULL,
  `activity_start` int(10) DEFAULT NULL,
  `activity_pause` int(10) DEFAULT NULL,
  `activity_stop` int(10) DEFAULT NULL,
  `workflowstatus` int(10) DEFAULT NULL,
  `workflownextstep` int(10) DEFAULT NULL,
  `pending_apprvl_for_name` varchar(100) DEFAULT NULL,
  `pending_apprvl_for_code` int(11) DEFAULT NULL,
  `is_qa_approve` enum('yes','no') DEFAULT 'no',
  `sampling_rod_id` int(11) DEFAULT NULL,
  `dies_no` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_vaccum_cleaner`
--

CREATE TABLE `pts_trn_vaccum_cleaner` (
  `id` int(11) NOT NULL,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `vaccum_cleaner_code` varchar(500) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_no` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pts_trn_workflowsteps`
--

CREATE TABLE `pts_trn_workflowsteps` (
  `id` bigint(20) NOT NULL,
  `status_id` bigint(20) NOT NULL,
  `activity_id` bigint(20) NOT NULL COMMENT 'Store Activity code from MST_PTS_Activity',
  `workflowtype` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  `next_step` bigint(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(500) NOT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_accessorycleaning`
--

CREATE TABLE `trn_accessorycleaning` (
  `id` bigint(20) NOT NULL,
  `document_no` varchar(50) NOT NULL,
  `area_code` varchar(200) DEFAULT NULL,
  `room_code` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `department_name` varchar(50) NOT NULL,
  `product_name` mediumtext NOT NULL,
  `accessorylist` mediumtext,
  `remarks` mediumtext NOT NULL,
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `stop_time` datetime DEFAULT NULL,
  `performed_by` varchar(250) DEFAULT NULL,
  `performed_name` varchar(500) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `checked_name` varchar(500) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `role_id` int(11) NOT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_activityrole`
--

CREATE TABLE `trn_activityrole` (
  `id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `activity_typeid` int(20) NOT NULL,
  `activity_id` bigint(20) NOT NULL COMMENT 'Store Code from mst_activitytivitre Ac',
  `block_code` varchar(200) DEFAULT NULL,
  `area_code` varchar(200) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(500) NOT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_batch`
--

CREATE TABLE `trn_batch` (
  `batch_code` varchar(50) NOT NULL,
  `batch_description` varchar(50) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `product_description` varchar(200) DEFAULT NULL,
  `UOM` varchar(20) DEFAULT NULL,
  `batch_size` int(11) DEFAULT NULL,
  `market` varchar(30) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='temporary table for prototype';

-- --------------------------------------------------------

--
-- Table structure for table `trn_cleaningandsanitization`
--

CREATE TABLE `trn_cleaningandsanitization` (
  `id` int(11) NOT NULL,
  `equipcode` varchar(200) DEFAULT NULL,
  `sop_code` varchar(200) DEFAULT NULL,
  `documentno` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `lotno` varchar(200) DEFAULT NULL,
  `strarttime` time DEFAULT NULL,
  `startby` varchar(200) DEFAULT NULL,
  `endtime` time DEFAULT NULL,
  `endby` varchar(200) DEFAULT NULL,
  `checkedby_code` varchar(200) DEFAULT NULL,
  `checkedby_name` varchar(200) DEFAULT NULL,
  `checkedon` datetime DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(200) DEFAULT NULL,
  `created_byname` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_dailycleaning`
--

CREATE TABLE `trn_dailycleaning` (
  `id` int(11) NOT NULL,
  `document_no` varchar(50) NOT NULL,
  `area_code` varchar(100) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `sanitizationused` mediumtext,
  `available_quantity` varchar(10) DEFAULT NULL,
  `expiry_date_solution` datetime DEFAULT NULL,
  `required_to_clean` varchar(10) DEFAULT NULL,
  `roomno` varchar(300) DEFAULT NULL,
  `wastebin` int(1) NOT NULL,
  `floorcovering` int(1) DEFAULT NULL,
  `drain_points` mediumtext,
  `checked_by` varchar(250) DEFAULT NULL,
  `checked_name` varchar(500) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_time` datetime DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_dpmonitoring`
--

CREATE TABLE `trn_dpmonitoring` (
  `id` int(11) NOT NULL,
  `mat_trans_tbl_id` int(11) DEFAULT NULL,
  `equip_code` varchar(50) DEFAULT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `documentno` varchar(50) DEFAULT NULL,
  `date` datetime NOT NULL,
  `time` time NOT NULL,
  `dpvalue` varchar(200) DEFAULT NULL,
  `ensuredby` varchar(200) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_drainpointdetails`
--

CREATE TABLE `trn_drainpointdetails` (
  `id` int(11) NOT NULL,
  `document_no` varchar(50) NOT NULL,
  `area_code` varchar(100) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `sanitizationused` varchar(300) NOT NULL,
  `available_quantity` varchar(10) DEFAULT NULL,
  `expiry_date_solution` datetime DEFAULT NULL,
  `required_to_clean` varchar(10) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `drain_points` int(11) DEFAULT NULL,
  `identification_drainpoints` mediumtext,
  `check_remark` varchar(2000) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `checked_name` varchar(500) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_time` datetime DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_injequipmentdetails`
--

CREATE TABLE `trn_injequipmentdetails` (
  `id` int(11) NOT NULL,
  `lot_no` varchar(1000) DEFAULT NULL,
  `sop_no` varchar(1000) DEFAULT NULL,
  `equipmentname` varchar(1000) DEFAULT NULL,
  `code_no` varchar(1000) DEFAULT NULL,
  `cleaning_status` varchar(50) DEFAULT NULL,
  `checked_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `checked_by` varchar(1000) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_injfiltrationdetails`
--

CREATE TABLE `trn_injfiltrationdetails` (
  `id` int(11) NOT NULL,
  `form_id` int(11) DEFAULT NULL,
  `lot_no` varchar(1000) DEFAULT NULL,
  `pre_limit` varchar(500) DEFAULT NULL,
  `collection_fromtime` time DEFAULT NULL,
  `collection_totime` time DEFAULT NULL,
  `post_limit` varchar(500) DEFAULT NULL,
  `checked_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `checked_by` varchar(500) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_injpar64details`
--

CREATE TABLE `trn_injpar64details` (
  `id` int(11) NOT NULL,
  `equipmentcode` varchar(1000) DEFAULT NULL,
  `sop_no` varchar(500) DEFAULT NULL,
  `cleaning_start` datetime DEFAULT NULL,
  `cleaning_stop` datetime DEFAULT NULL,
  `clean_by` varchar(500) DEFAULT NULL,
  `cleanstop_by` varchar(500) DEFAULT NULL,
  `clean_remarks` varchar(1000) DEFAULT NULL,
  `clean_checkby` varchar(500) DEFAULT NULL,
  `op_productname` varchar(1000) DEFAULT NULL,
  `op_lotno` varchar(1000) DEFAULT NULL,
  `op_output` varchar(500) DEFAULT NULL,
  `op_start` datetime DEFAULT NULL,
  `op_stop` datetime DEFAULT NULL,
  `op_by` varchar(500) DEFAULT NULL,
  `opstop_by` varchar(500) DEFAULT NULL,
  `op_remarks` varchar(1000) DEFAULT NULL,
  `op_checkby` varchar(500) DEFAULT NULL,
  `break_fromtime` datetime DEFAULT NULL,
  `break_totime` datetime DEFAULT NULL,
  `break_remarks` varchar(1000) DEFAULT NULL,
  `break_checkby` varchar(500) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_injsolutiondetails`
--

CREATE TABLE `trn_injsolutiondetails` (
  `id` int(11) NOT NULL,
  `form_id` int(11) DEFAULT NULL,
  `doc_id` varchar(50) DEFAULT NULL,
  `solutionname` varchar(300) NOT NULL,
  `required_quantity` varchar(50) DEFAULT NULL,
  `standard_water_qty` varchar(50) DEFAULT NULL,
  `Standard_hydrogen_qty` varchar(1000) DEFAULT NULL,
  `wfi_makeup_volume` varchar(1000) DEFAULT NULL,
  `actual_water_qty` varchar(500) DEFAULT NULL,
  `arn_water` varchar(500) DEFAULT NULL,
  `uom_water` varchar(50) DEFAULT NULL,
  `actual_hydrogen_qty` varchar(500) DEFAULT NULL,
  `arn_hydrogen` varchar(500) DEFAULT NULL,
  `uom_hydrogen` varchar(50) DEFAULT NULL,
  `actual_wfi_makeup_volume` varchar(500) DEFAULT NULL,
  `arn_wfi` varchar(500) DEFAULT NULL,
  `uom_wfi` varchar(50) DEFAULT NULL,
  `expiry_datatime` datetime DEFAULT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stop_by` varchar(500) DEFAULT NULL,
  `stop_on` datetime DEFAULT NULL,
  `is_active` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_injsterilizationdetails`
--

CREATE TABLE `trn_injsterilizationdetails` (
  `id` int(11) NOT NULL,
  `form_id` int(11) DEFAULT NULL,
  `equipmentname` varchar(1000) DEFAULT NULL,
  `code_no` varchar(1000) DEFAULT NULL,
  `sterilizer_no` varchar(1000) DEFAULT NULL,
  `run_no` int(11) DEFAULT NULL,
  `ref_graph_no` varchar(1000) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `from_time` time DEFAULT NULL,
  `to_time` time DEFAULT NULL,
  `checked_by` varchar(1000) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_inj_roomcleaningheader`
--

CREATE TABLE `trn_inj_roomcleaningheader` (
  `id` bigint(20) NOT NULL,
  `date` date DEFAULT NULL,
  `solution_code` varchar(200) DEFAULT NULL,
  `room_code` varchar(20) DEFAULT NULL,
  `cleaning_activity_type` varchar(20) DEFAULT NULL,
  `lot_no` varchar(20) DEFAULT NULL,
  `starttime` time DEFAULT NULL,
  `endtime` time DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `checkedby_code` varchar(200) DEFAULT NULL,
  `checkedby_name` varchar(200) DEFAULT NULL,
  `checkedon` time DEFAULT NULL,
  `createdby_name` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(200) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_inprocesscontainercleaning`
--

CREATE TABLE `trn_inprocesscontainercleaning` (
  `id` int(11) NOT NULL,
  `document_no` varchar(50) NOT NULL,
  `area_code` varchar(100) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `batch_no` varchar(100) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `sop_no` varchar(100) DEFAULT NULL,
  `equipment_code` mediumtext,
  `remarks` mediumtext,
  `checked_by` varchar(250) DEFAULT NULL,
  `checked_name` varchar(500) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_time` datetime DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_material_transfer_dtl`
--

CREATE TABLE `trn_material_transfer_dtl` (
  `id` int(11) NOT NULL,
  `mat_eqip_dtl` varchar(2000) DEFAULT NULL,
  `mat_eqip_rec_transfer_FT` time DEFAULT NULL,
  `mat_eqip_rec_transfer_TT` time DEFAULT NULL,
  `sanitization_agent` varchar(200) DEFAULT NULL,
  `performedby` varchar(200) DEFAULT NULL,
  `checkedby_code` varchar(200) DEFAULT NULL,
  `checkedby_name` varchar(200) DEFAULT NULL,
  `checkedon` date DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_operationequipmentdetaillog`
--

CREATE TABLE `trn_operationequipmentdetaillog` (
  `id` int(11) NOT NULL,
  `equipmentheader_table_id` int(11) DEFAULT NULL,
  `header_id` int(11) DEFAULT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `equipment_type` varchar(50) DEFAULT NULL,
  `strattime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `memo_number` varchar(250) DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `action_type` varchar(250) DEFAULT NULL,
  `performed_by` varchar(250) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_operationequipmentheaderlog`
--

CREATE TABLE `trn_operationequipmentheaderlog` (
  `id` int(11) NOT NULL,
  `header_id` int(11) DEFAULT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_operationequipmentlog`
--

CREATE TABLE `trn_operationequipmentlog` (
  `id` int(11) NOT NULL,
  `header_id` int(11) DEFAULT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `equipment_type` varchar(50) DEFAULT NULL,
  `sop_code` varchar(250) DEFAULT NULL,
  `brkdwn_strattime` datetime DEFAULT NULL,
  `brkdwn_endtime` datetime DEFAULT NULL,
  `brkdwn_checked_by` varchar(250) DEFAULT NULL,
  `memo_number` varchar(250) DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `performed_by` varchar(250) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_operationlogdetail`
--

CREATE TABLE `trn_operationlogdetail` (
  `id` bigint(20) NOT NULL,
  `header_id` varchar(50) DEFAULT NULL,
  `start_time` varchar(50) DEFAULT NULL,
  `end_time` varchar(50) DEFAULT NULL,
  `checked_by_time` datetime DEFAULT NULL,
  `performed_by` varchar(250) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_operationlogheader`
--

CREATE TABLE `trn_operationlogheader` (
  `id` bigint(20) NOT NULL,
  `document_no` varchar(50) NOT NULL,
  `lot_no` varchar(50) DEFAULT NULL,
  `area_code` varchar(50) NOT NULL,
  `room_code` varchar(50) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `activity_code` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `next_step` int(11) DEFAULT NULL,
  `operation_type` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `trn_operationlogheader`
--
DELIMITER $$
CREATE TRIGGER `trn_operationheader_insert_audit_log` AFTER INSERT ON `trn_operationlogheader` FOR EACH ROW Begin
INSERT INTO operationheader_audit_log (id,tablename,tableid,action,createdon,createdby)
	  VALUES(null,'operationlogheader',New.id,'inserted',NOW(),New.created_by);
End
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trn_operationheader_update_audit_log` AFTER UPDATE ON `trn_operationlogheader` FOR EACH ROW Begin
INSERT INTO operationheader_audit_log (id,tablename,tableid,action,createdon,createdby)
	  VALUES(null,'operationlogheader',New.id,'updated',NOW(),New.created_by);
End
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `trn_operation_workflow_approval_records`
--

CREATE TABLE `trn_operation_workflow_approval_records` (
  `id` int(11) NOT NULL,
  `header_id` int(11) DEFAULT NULL,
  `doneby` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `roleid` varchar(50) DEFAULT NULL,
  `time` datetime NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `remark` varchar(2000) DEFAULT NULL,
  `filepath` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_portabledetail`
--

CREATE TABLE `trn_portabledetail` (
  `id` bigint(20) NOT NULL,
  `document_no` varchar(50) DEFAULT NULL,
  `department_name` varchar(50) DEFAULT NULL,
  `batch_code` varchar(100) DEFAULT NULL,
  `area_code` varchar(100) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `performed_by` varchar(250) DEFAULT NULL,
  `performed_name` varchar(500) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `checked_name` varchar(500) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `role_id` int(11) NOT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `checked_on` datetime DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `stoptime` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_portableequipmentdetail`
--

CREATE TABLE `trn_portableequipmentdetail` (
  `id` bigint(20) NOT NULL,
  `document_portable_id` varchar(50) DEFAULT NULL,
  `document_no` varchar(50) DEFAULT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `equipment_type` varchar(300) DEFAULT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `changepart` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_roomcleaning_detail`
--

CREATE TABLE `trn_roomcleaning_detail` (
  `id` bigint(20) NOT NULL,
  `header_id` bigint(20) DEFAULT NULL,
  `cleaning_activity_code` varchar(200) DEFAULT NULL,
  `status` varchar(200) DEFAULT NULL,
  `created_by` varchar(200) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_solutiondetails`
--

CREATE TABLE `trn_solutiondetails` (
  `id` int(11) NOT NULL,
  `doc_id` varchar(50) NOT NULL,
  `area_code` varchar(200) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `solutionname` varchar(300) NOT NULL,
  `required_quantity` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `qcar_no` varchar(50) DEFAULT NULL,
  `standard_solution_qty` varchar(50) DEFAULT NULL,
  `purified_water` varchar(1000) DEFAULT NULL,
  `makeup_volume` varchar(1000) DEFAULT NULL,
  `actual_solution_qty` varchar(500) DEFAULT NULL,
  `actual_purified_water` varchar(500) DEFAULT NULL,
  `solution_valid_upto` varchar(500) DEFAULT NULL,
  `expiry_datatime` datetime DEFAULT NULL,
  `check_remark` varchar(2000) DEFAULT NULL,
  `solution_destroy_checked_by` varchar(250) DEFAULT NULL,
  `solution_destroy_checked_name` varchar(500) DEFAULT NULL,
  `checked_on` datetime DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `role_id` int(11) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text,
  `is_destroy` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_solutionlog`
--

CREATE TABLE `trn_solutionlog` (
  `id` int(11) NOT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `qcar_no` varchar(50) DEFAULT NULL,
  `standard_solution_qty` varchar(50) DEFAULT NULL,
  `purified_water` varchar(1000) DEFAULT NULL,
  `makeup_volume` varchar(1000) DEFAULT NULL,
  `actual_solution_qty` varchar(500) DEFAULT NULL,
  `actual_purified_water` varchar(500) DEFAULT NULL,
  `solution_valid_upto` varchar(500) DEFAULT NULL,
  `check_remark` varchar(2000) DEFAULT NULL,
  `solution_destroy_checked_by` varchar(250) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_solutionstock`
--

CREATE TABLE `trn_solutionstock` (
  `id` int(11) NOT NULL,
  `document_no` varchar(250) DEFAULT NULL,
  `solutionname` varchar(250) DEFAULT NULL,
  `available_qty` int(11) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `is_active` int(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trn_weeklydrainpointdetails`
--

CREATE TABLE `trn_weeklydrainpointdetails` (
  `id` int(11) NOT NULL,
  `document_no` varchar(50) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `sanitizationused` varchar(300) NOT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `drain_points` int(11) DEFAULT NULL,
  `identification_drainpoints` mediumtext,
  `check_remark` varchar(2000) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_time` datetime DEFAULT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trn_workflowsteps`
--

CREATE TABLE `trn_workflowsteps` (
  `id` bigint(20) NOT NULL,
  `status_id` bigint(20) NOT NULL,
  `activity_id` bigint(20) NOT NULL COMMENT 'Store Activity code from mst_activity',
  `role_id` bigint(20) NOT NULL,
  `alert_roleid` bigint(20) DEFAULT NULL,
  `alert_message` text,
  `next_step` bigint(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(500) NOT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure for view `getstart_stoped_data`
--
DROP TABLE IF EXISTS `getstart_stoped_data`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `getstart_stoped_data`  AS  select `oph`.`status` AS `status`,`oph`.`area_code` AS `area_code`,`oph`.`room_code` AS `room_code`,`s`.`status_name` AS `status_name`,`b`.`product_description` AS `product_description`,`oph`.`document_no` AS `document_no`,`oph`.`batch_no` AS `batch_no`,`oph`.`lot_no` AS `lot_no`,`oph`.`created_on` AS `created_on`,`oph`.`operation_type` AS `operation_type`,(select `mst_employee`.`emp_name` from `mst_employee` where (`mst_employee`.`emp_code` = `oph`.`created_by`)) AS `created_by` from ((`trn_operationlogheader` `oph` join `mst_status` `s` on((`s`.`id` = `oph`.`status`))) join `trn_batch` `b` on((`oph`.`batch_no` = `b`.`batch_code`))) order by `oph`.`created_on` desc ;

-- --------------------------------------------------------

--
-- Structure for view `getstatusrecord`
--
DROP TABLE IF EXISTS `getstatusrecord`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `getstatusrecord`  AS  select `war`.`id` AS `warid`,`war`.`header_id` AS `header_id`,`war`.`status` AS `status`,`oph`.`area_code` AS `area_code`,`oph`.`room_code` AS `room_code`,`s`.`status_name` AS `status_name`,`b`.`product_description` AS `product_description`,`oph`.`document_no` AS `document_no`,`oph`.`batch_no` AS `batch_no`,`oph`.`lot_no` AS `lot_no`,`oph`.`created_on` AS `created_on`,`oph`.`operation_type` AS `operation_type`,`war`.`doneby` AS `doneby`,`war`.`name` AS `actionby`,`war`.`roleid` AS `roleid`,`war`.`time` AS `actiontime`,`war`.`type` AS `type` from (((`trn_operationlogheader` `oph` join `trn_operation_workflow_approval_records` `war` on((`oph`.`id` = `war`.`header_id`))) join `mst_status` `s` on((`s`.`id` = `war`.`status`))) join `trn_batch` `b` on((`oph`.`batch_no` = `b`.`batch_code`))) order by `oph`.`created_on` desc ;

-- --------------------------------------------------------

--
-- Structure for view `operationsqnlog`
--
DROP TABLE IF EXISTS `operationsqnlog`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `operationsqnlog`  AS  select `a`.`id` AS `id`,`a`.`document_no` AS `document_no`,`a`.`lot_no` AS `lot_no`,`a`.`area_code` AS `area_code`,`a`.`room_code` AS `room_code`,`a`.`batch_no` AS `batch_no`,`e`.`product_description` AS `product_description`,`a`.`operation_type` AS `operation_type`,min(`b`.`start_time`) AS `start_time`,max(`b`.`end_time`) AS `end_time`,(select `mst_employee`.`emp_name` from `mst_employee` where (`mst_employee`.`emp_name` = min(`b`.`performed_by`))) AS `started_by`,(select `mst_employee`.`emp_name` from `mst_employee` where (`mst_employee`.`emp_name` = max(`b`.`performed_by`))) AS `ended_by`,group_concat(distinct concat(convert(`b`.`performed_by` using utf8)) separator ',') AS `names`,group_concat(distinct concat(convert(`ar`.`name` using utf8)) separator ',') AS `approvedby`,group_concat(distinct concat(convert(`ar`.`time` using utf8)) separator ',') AS `approvedon`,group_concat(distinct concat(convert(`c`.`equipment_code` using utf8),'###',`d`.`equipment_name`,'###',`d`.`equipment_type`,'###',convert(`c`.`sop_code` using utf8)) separator ',') AS `equipments` from ((((((`trn_operationlogheader` `a` join `trn_operationlogdetail` `b` on((`a`.`id` = `b`.`header_id`))) join `trn_operationequipmentheaderlog` `c` on((`a`.`id` = `c`.`header_id`))) join `mst_equipment` `d` on((convert(`c`.`equipment_code` using utf8) = `d`.`equipment_code`))) join `mst_sop` `sop` on((convert(`c`.`sop_code` using utf8) = `sop`.`sop_code`))) join `trn_batch` `e` on((`a`.`batch_no` = `e`.`batch_code`))) join `trn_operation_workflow_approval_records` `ar` on((`a`.`id` = `ar`.`header_id`))) where (`ar`.`type` = 'Apprved') group by `a`.`id` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `log_tableaction`
--
ALTER TABLE `log_tableaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_tablechanges`
--
ALTER TABLE `log_tablechanges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_accessory_criticalparts`
--
ALTER TABLE `mst_accessory_criticalparts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_activity`
--
ALTER TABLE `mst_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_activitytype`
--
ALTER TABLE `mst_activitytype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_area`
--
ALTER TABLE `mst_area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_block`
--
ALTER TABLE `mst_block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_department`
--
ALTER TABLE `mst_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_document`
--
ALTER TABLE `mst_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_drainpoint`
--
ALTER TABLE `mst_drainpoint`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_employee`
--
ALTER TABLE `mst_employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_equipment`
--
ALTER TABLE `mst_equipment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_equipment-old`
--
ALTER TABLE `mst_equipment-old`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_holiday`
--
ALTER TABLE `mst_holiday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_inj_cleaning`
--
ALTER TABLE `mst_inj_cleaning`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_inj_solution`
--
ALTER TABLE `mst_inj_solution`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_messages`
--
ALTER TABLE `mst_messages`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `mst_plant`
--
ALTER TABLE `mst_plant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_procedure`
--
ALTER TABLE `mst_procedure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_product`
--
ALTER TABLE `mst_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_product_process`
--
ALTER TABLE `mst_product_process`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_role`
--
ALTER TABLE `mst_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_roleid_workflow_mapping`
--
ALTER TABLE `mst_roleid_workflow_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_room`
--
ALTER TABLE `mst_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_shift`
--
ALTER TABLE `mst_shift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_solution`
--
ALTER TABLE `mst_solution`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_sop`
--
ALTER TABLE `mst_sop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_status`
--
ALTER TABLE `mst_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `operationheader_audit_log`
--
ALTER TABLE `operationheader_audit_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_mst_activity`
--
ALTER TABLE `pts_mst_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_mst_balance_calibration`
--
ALTER TABLE `pts_mst_balance_calibration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_mst_balance_calibration_standard`
--
ALTER TABLE `pts_mst_balance_calibration_standard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_mst_balance_frequency_data`
--
ALTER TABLE `pts_mst_balance_frequency_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_mst_document`
--
ALTER TABLE `pts_mst_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_mst_filter`
--
ALTER TABLE `pts_mst_filter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_mst_frequency`
--
ALTER TABLE `pts_mst_frequency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_mst_id_standard_weight`
--
ALTER TABLE `pts_mst_id_standard_weight`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_mst_instrument`
--
ALTER TABLE `pts_mst_instrument`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_mst_line_log`
--
ALTER TABLE `pts_mst_line_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_mst_role_level`
--
ALTER TABLE `pts_mst_role_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_mst_role_workassign_to_mult_roles`
--
ALTER TABLE `pts_mst_role_workassign_to_mult_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_mst_stages`
--
ALTER TABLE `pts_mst_stages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_mst_sys_id`
--
ALTER TABLE `pts_mst_sys_id`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_mst_tablet_tooling_log`
--
ALTER TABLE `pts_mst_tablet_tooling_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_activity_approval_log`
--
ALTER TABLE `pts_trn_activity_approval_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_activity_log`
--
ALTER TABLE `pts_trn_activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_balance_calibration`
--
ALTER TABLE `pts_trn_balance_calibration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_batchin`
--
ALTER TABLE `pts_trn_batchin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_emp_roomin`
--
ALTER TABLE `pts_trn_emp_roomin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_env_cond_diff`
--
ALTER TABLE `pts_trn_env_cond_diff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_equip_appratus_log`
--
ALTER TABLE `pts_trn_equip_appratus_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_instrument_log_register`
--
ALTER TABLE `pts_trn_instrument_log_register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_laf_pressure_diff`
--
ALTER TABLE `pts_trn_laf_pressure_diff`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_material_retreival_relocation`
--
ALTER TABLE `pts_trn_material_retreival_relocation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_pre_filter_cleaning`
--
ALTER TABLE `pts_trn_pre_filter_cleaning`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_return_air_filter`
--
ALTER TABLE `pts_trn_return_air_filter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_room_log_activity`
--
ALTER TABLE `pts_trn_room_log_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_swab_sample_record`
--
ALTER TABLE `pts_trn_swab_sample_record`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_tablet_tooling`
--
ALTER TABLE `pts_trn_tablet_tooling`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_user_activity_log`
--
ALTER TABLE `pts_trn_user_activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_vaccum_cleaner`
--
ALTER TABLE `pts_trn_vaccum_cleaner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pts_trn_workflowsteps`
--
ALTER TABLE `pts_trn_workflowsteps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_accessorycleaning`
--
ALTER TABLE `trn_accessorycleaning`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_activityrole`
--
ALTER TABLE `trn_activityrole`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_batch`
--
ALTER TABLE `trn_batch`
  ADD PRIMARY KEY (`batch_code`);

--
-- Indexes for table `trn_cleaningandsanitization`
--
ALTER TABLE `trn_cleaningandsanitization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_dailycleaning`
--
ALTER TABLE `trn_dailycleaning`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_dpmonitoring`
--
ALTER TABLE `trn_dpmonitoring`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_drainpointdetails`
--
ALTER TABLE `trn_drainpointdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_injequipmentdetails`
--
ALTER TABLE `trn_injequipmentdetails`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `trn_injfiltrationdetails`
--
ALTER TABLE `trn_injfiltrationdetails`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `trn_injpar64details`
--
ALTER TABLE `trn_injpar64details`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `trn_injsolutiondetails`
--
ALTER TABLE `trn_injsolutiondetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_injsterilizationdetails`
--
ALTER TABLE `trn_injsterilizationdetails`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `trn_inj_roomcleaningheader`
--
ALTER TABLE `trn_inj_roomcleaningheader`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_inprocesscontainercleaning`
--
ALTER TABLE `trn_inprocesscontainercleaning`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_material_transfer_dtl`
--
ALTER TABLE `trn_material_transfer_dtl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_operationequipmentdetaillog`
--
ALTER TABLE `trn_operationequipmentdetaillog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_operationequipmentheaderlog`
--
ALTER TABLE `trn_operationequipmentheaderlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_operationequipmentlog`
--
ALTER TABLE `trn_operationequipmentlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_operationlogdetail`
--
ALTER TABLE `trn_operationlogdetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_operationlogheader`
--
ALTER TABLE `trn_operationlogheader`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_operation_workflow_approval_records`
--
ALTER TABLE `trn_operation_workflow_approval_records`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_portabledetail`
--
ALTER TABLE `trn_portabledetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_portableequipmentdetail`
--
ALTER TABLE `trn_portableequipmentdetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_roomcleaning_detail`
--
ALTER TABLE `trn_roomcleaning_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_solutiondetails`
--
ALTER TABLE `trn_solutiondetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_solutionlog`
--
ALTER TABLE `trn_solutionlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_solutionstock`
--
ALTER TABLE `trn_solutionstock`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `trn_weeklydrainpointdetails`
--
ALTER TABLE `trn_weeklydrainpointdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trn_workflowsteps`
--
ALTER TABLE `trn_workflowsteps`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `log_tableaction`
--
ALTER TABLE `log_tableaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=992;
--
-- AUTO_INCREMENT for table `log_tablechanges`
--
ALTER TABLE `log_tablechanges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=358;
--
-- AUTO_INCREMENT for table `mst_accessory_criticalparts`
--
ALTER TABLE `mst_accessory_criticalparts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_activity`
--
ALTER TABLE `mst_activity`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2609;
--
-- AUTO_INCREMENT for table `mst_activitytype`
--
ALTER TABLE `mst_activitytype`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `mst_area`
--
ALTER TABLE `mst_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `mst_block`
--
ALTER TABLE `mst_block`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `mst_department`
--
ALTER TABLE `mst_department`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `mst_document`
--
ALTER TABLE `mst_document`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_drainpoint`
--
ALTER TABLE `mst_drainpoint`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=567;
--
-- AUTO_INCREMENT for table `mst_employee`
--
ALTER TABLE `mst_employee`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `mst_equipment`
--
ALTER TABLE `mst_equipment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;
--
-- AUTO_INCREMENT for table `mst_equipment-old`
--
ALTER TABLE `mst_equipment-old`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_holiday`
--
ALTER TABLE `mst_holiday`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_messages`
--
ALTER TABLE `mst_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `mst_plant`
--
ALTER TABLE `mst_plant`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mst_procedure`
--
ALTER TABLE `mst_procedure`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_product`
--
ALTER TABLE `mst_product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=737;
--
-- AUTO_INCREMENT for table `mst_product_process`
--
ALTER TABLE `mst_product_process`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_role`
--
ALTER TABLE `mst_role`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `mst_roleid_workflow_mapping`
--
ALTER TABLE `mst_roleid_workflow_mapping`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `mst_room`
--
ALTER TABLE `mst_room`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `mst_shift`
--
ALTER TABLE `mst_shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mst_solution`
--
ALTER TABLE `mst_solution`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `mst_sop`
--
ALTER TABLE `mst_sop`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=743;
--
-- AUTO_INCREMENT for table `mst_status`
--
ALTER TABLE `mst_status`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `operationheader_audit_log`
--
ALTER TABLE `operationheader_audit_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=794;
--
-- AUTO_INCREMENT for table `pts_mst_activity`
--
ALTER TABLE `pts_mst_activity`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `pts_mst_balance_calibration`
--
ALTER TABLE `pts_mst_balance_calibration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pts_mst_balance_calibration_standard`
--
ALTER TABLE `pts_mst_balance_calibration_standard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `pts_mst_balance_frequency_data`
--
ALTER TABLE `pts_mst_balance_frequency_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pts_mst_document`
--
ALTER TABLE `pts_mst_document`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `pts_mst_filter`
--
ALTER TABLE `pts_mst_filter`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pts_mst_frequency`
--
ALTER TABLE `pts_mst_frequency`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `pts_mst_id_standard_weight`
--
ALTER TABLE `pts_mst_id_standard_weight`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `pts_mst_instrument`
--
ALTER TABLE `pts_mst_instrument`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pts_mst_line_log`
--
ALTER TABLE `pts_mst_line_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pts_mst_role_level`
--
ALTER TABLE `pts_mst_role_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pts_mst_role_workassign_to_mult_roles`
--
ALTER TABLE `pts_mst_role_workassign_to_mult_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pts_mst_stages`
--
ALTER TABLE `pts_mst_stages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pts_mst_sys_id`
--
ALTER TABLE `pts_mst_sys_id`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `pts_mst_tablet_tooling_log`
--
ALTER TABLE `pts_mst_tablet_tooling_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pts_trn_activity_approval_log`
--
ALTER TABLE `pts_trn_activity_approval_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `pts_trn_activity_log`
--
ALTER TABLE `pts_trn_activity_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pts_trn_balance_calibration`
--
ALTER TABLE `pts_trn_balance_calibration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `pts_trn_batchin`
--
ALTER TABLE `pts_trn_batchin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pts_trn_emp_roomin`
--
ALTER TABLE `pts_trn_emp_roomin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pts_trn_env_cond_diff`
--
ALTER TABLE `pts_trn_env_cond_diff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pts_trn_equip_appratus_log`
--
ALTER TABLE `pts_trn_equip_appratus_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pts_trn_instrument_log_register`
--
ALTER TABLE `pts_trn_instrument_log_register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pts_trn_laf_pressure_diff`
--
ALTER TABLE `pts_trn_laf_pressure_diff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pts_trn_material_retreival_relocation`
--
ALTER TABLE `pts_trn_material_retreival_relocation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pts_trn_pre_filter_cleaning`
--
ALTER TABLE `pts_trn_pre_filter_cleaning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `pts_trn_return_air_filter`
--
ALTER TABLE `pts_trn_return_air_filter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `pts_trn_room_log_activity`
--
ALTER TABLE `pts_trn_room_log_activity`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `pts_trn_swab_sample_record`
--
ALTER TABLE `pts_trn_swab_sample_record`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pts_trn_tablet_tooling`
--
ALTER TABLE `pts_trn_tablet_tooling`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `pts_trn_user_activity_log`
--
ALTER TABLE `pts_trn_user_activity_log`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `pts_trn_vaccum_cleaner`
--
ALTER TABLE `pts_trn_vaccum_cleaner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `pts_trn_workflowsteps`
--
ALTER TABLE `pts_trn_workflowsteps`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `trn_accessorycleaning`
--
ALTER TABLE `trn_accessorycleaning`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;
--
-- AUTO_INCREMENT for table `trn_activityrole`
--
ALTER TABLE `trn_activityrole`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1047;
--
-- AUTO_INCREMENT for table `trn_cleaningandsanitization`
--
ALTER TABLE `trn_cleaningandsanitization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trn_dailycleaning`
--
ALTER TABLE `trn_dailycleaning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `trn_dpmonitoring`
--
ALTER TABLE `trn_dpmonitoring`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `trn_drainpointdetails`
--
ALTER TABLE `trn_drainpointdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT for table `trn_injequipmentdetails`
--
ALTER TABLE `trn_injequipmentdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `trn_injfiltrationdetails`
--
ALTER TABLE `trn_injfiltrationdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `trn_injpar64details`
--
ALTER TABLE `trn_injpar64details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trn_injsolutiondetails`
--
ALTER TABLE `trn_injsolutiondetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trn_injsterilizationdetails`
--
ALTER TABLE `trn_injsterilizationdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trn_inj_roomcleaningheader`
--
ALTER TABLE `trn_inj_roomcleaningheader`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `trn_inprocesscontainercleaning`
--
ALTER TABLE `trn_inprocesscontainercleaning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
--
-- AUTO_INCREMENT for table `trn_material_transfer_dtl`
--
ALTER TABLE `trn_material_transfer_dtl`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trn_operationequipmentdetaillog`
--
ALTER TABLE `trn_operationequipmentdetaillog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trn_operationequipmentheaderlog`
--
ALTER TABLE `trn_operationequipmentheaderlog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=558;

ALTER TABLE `pts_mst_tablet_tooling_log` CHANGE `upper_punch_qty` `upper_punch_qty` VARCHAR(100) NOT NULL, CHANGE `lower_punch_qty` `lower_punch_qty` VARCHAR(100) NOT NULL; 

ALTER TABLE `pts_trn_instrument_log_register` CHANGE `test` `test` VARCHAR(600) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL;

ALTER TABLE `pts_mst_stages` CHANGE `stage_desc` `stage_desc` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL; 

ALTER TABLE `pts_trn_user_activity_log` CHANGE `sampling_rod_id` `sampling_rod_id` VARCHAR(100) NULL DEFAULT NULL; 

CREATE TABLE `pts_mst_report_header` (
  `id` int(11) NOT NULL,
  `form_no` varchar(100) NOT NULL,
  `version_no` varchar(100) NOT NULL,
  `effective_date` varchar(100) NOT NULL,
  `document_no` varchar(100) NOT NULL,
  `status` enum('active','inactive','default') NOT NULL DEFAULT 'active',
  `created_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pts_mst_report_header`
--
ALTER TABLE `pts_mst_report_header`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pts_mst_report_header`
--
ALTER TABLE `pts_mst_report_header`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

  ALTER TABLE `pts_mst_line_log` ADD `is_inused` BIT(0) NOT NULL AFTER `status`;
ALTER TABLE `pts_mst_instrument` ADD `is_inused` BIT(0) NOT NULL AFTER `status`;

ALTER TABLE `pts_trn_env_cond_diff` CHANGE `from_pressure` `from_pressure` FLOAT NULL DEFAULT NULL, CHANGE `to_pressure` `to_pressure` FLOAT NULL DEFAULT NULL;
ALTER TABLE `pts_trn_env_cond_diff` CHANGE `globe_pressure_diff` `globe_pressure_diff` ENUM('PASCAL','MM WC','NA') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'NA';