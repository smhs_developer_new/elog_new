$(document).ready(function(){
    checkNetworkOut();
});
function checkNetworkOut(){
    checkAndSendReport();
    setTimeout(checkNetworkOut, 1000);//reruns after 1 seconds
}
function checkAndSendReport(){
    if (navigator.onLine == true) {
        var d = new Date();
        
        var up_time = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
        var down_time = localStorage.getItem("down_time");
        var current_url = localStorage.getItem("current_url");
        var msg = localStorage.getItem("msg");
//        alert(msg);
        if (msg== "Network Error") {
            var formData = {url: current_url, msg: msg, up_time: up_time, down_time: down_time, record_type: 'master'};
            $.ajax({
                url: $('#base').val()+"/Networkcontroller/captureNetworkStatus",
                type: "POST",
                data: formData,
                success: function (data, textStatus, jqXHR)
                {
                    localStorage.setItem("current_url", "");
                    localStorage.setItem("msg", "");
                    localStorage.setItem("down_time", "");
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    console.log("Something went wrong.Please try again");
                }
            });
        }

    } else {
        alert("Network Error");
        var current_url = window.location.href;
         var d = new Date();
        var down_time = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
        var down = localStorage.getItem("down_time");
        var msg=localStorage.getItem("msg");
        
          if (msg!='Network Error') {
            localStorage.setItem("msg", "Network Error");
            localStorage.setItem("down_time", down_time);
            localStorage.setItem("current_url", current_url);
        }
}
}