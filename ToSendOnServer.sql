-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 172.29.54.196    Database: elog_new_v2_3april_wrkng
-- ------------------------------------------------------
-- Server version	5.7.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping routines for database 'elog_new_v2_3april_wrkng'
--
/*!50003 DROP PROCEDURE IF EXISTS `getAuditdata` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAuditdata`(IN `username` varchar(255) CHARSET utf8, IN `startdate` datetime, IN `enddate`  datetime, IN `pageNo` INT,IN `noOfRecords` INT)
    DETERMINISTIC
    COMMENT 'SP - To Get Audit List'
BEGIN
	DECLARE LowerBound INT;
	DECLARE UpperBound INT;
 
	SET LowerBound = ((pageNo*noOfRecords)-noOfRecords);
	SET UpperBound = noOfRecords;
(SELECT 
    `uniontable`.`doc_id` AS `doc_id`,
    `uniontable`.`logname` AS `logname`,
    `uniontable`.`actname` AS `actname`,
    `uniontable`.`approval_status` AS `approval_status`,
    `uniontable`.`workflow_type` AS `workflow_type`,
    `uniontable`.`user_name` AS `user_name`,
    `uniontable`.`datetime` AS `datetime`,
    `uniontable`.`activity_start` AS `activity_start`,
    `uniontable`.`room_code` AS `room_code`,
    `uniontable`.`activity_remarks` AS `activity_remarks`,
    `uniontable`.`room_name` AS `room_name`,
    `uniontable`.`batch_no` AS `batch_no`,
    `uniontable`.`product_code` AS `product_code`,
    `uniontable`.`product_name` AS `product_name`,
    `uniontable`.`batch_in_time` AS `batch_in_time`,
    `uniontable`.`batch_out_time` AS `batch_out_time`,
    `uniontable`.`room_in_time` AS `room_in_time`,
    `uniontable`.`room_out_time` AS `room_out_time`,
    `uniontable`.`table_extra_field` AS `table_extra_field`,
    `uniontable`.`record_type` AS `record_type`,
    `uniontable`.`command_type` AS `command_type`,
    `uniontable`.`table_name` AS `table_name`,
    `uniontable`.`history_id` AS `history_id`,
    `uniontable`.`action_performed` AS `action_performed`,
    `uniontable`.`previous_data` AS `previous_data`,
    `uniontable`.`master_previous_data` AS `master_previous_data`
FROM
    (SELECT 
        `pts_trn_user_activity_log`.`doc_id` AS `doc_id`,
            `pts_trn_room_log_activity`.`activity_name` AS `logname`,
            `pts_mst_activity`.`activity_name` AS `actname`,
            `pts_trn_activity_approval_log`.`approval_status` AS `approval_status`,
            `pts_trn_activity_approval_log`.`workflow_type` AS `workflow_type`,
            `pts_trn_activity_approval_log`.`user_name` AS `user_name`,
            DATE_FORMAT(FROM_UNIXTIME(`pts_trn_activity_approval_log`.`activity_time`), '%Y-%m-%d %H:%i:%s') AS `datetime`,
            DATE_FORMAT(FROM_UNIXTIME(`pts_trn_activity_approval_log`.`activity_time`), '%Y-%m-%d') AS `activity_start`,
            `pts_trn_user_activity_log`.`room_code` AS `room_code`,
            `pts_trn_activity_approval_log`.`activity_remarks` AS `activity_remarks`,
            `mst_room`.`room_name` AS `room_name`,
            `pts_trn_user_activity_log`.`batch_no` AS `batch_no`,
            `pts_trn_user_activity_log`.`product_code` AS `product_code`,
            `mst_product`.`product_name` AS `product_name`,
            0 AS `batch_in_time`,
            0 AS `batch_out_time`,
            0 AS `room_in_time`,
            0 AS `room_out_time`,
            `pts_elog_history`.`table_extra_field` AS `table_extra_field`,
            `pts_elog_history`.`record_type` AS `record_type`,
            `pts_elog_history`.`command_type` AS `command_type`,
            `pts_elog_history`.`table_name` AS `table_name`,
            `pts_elog_history`.`id` AS `history_id`,
            0 AS `action_performed`,
            0 AS `previous_data`,
            0 AS `master_previous_data`
    FROM
        (((((((`pts_trn_activity_approval_log`
    LEFT JOIN `pts_trn_user_activity_log` ON (`pts_trn_activity_approval_log`.`usr_activity_log_id` = `pts_trn_user_activity_log`.`id`))
    LEFT JOIN `pts_mst_activity` ON (`pts_mst_activity`.`id` = `pts_trn_user_activity_log`.`activity_id`))
    LEFT JOIN `pts_trn_room_log_activity` ON (`pts_trn_room_log_activity`.`id` = `pts_trn_user_activity_log`.`roomlogactivity`))
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_user_activity_log`.`room_code`))
    LEFT JOIN `pts_mst_sys_id` ON (`pts_mst_sys_id`.`room_code` = `mst_room`.`room_code`))
    LEFT JOIN `mst_product` ON (`mst_product`.`product_code` = `pts_trn_user_activity_log`.`product_code`))
    LEFT JOIN `pts_elog_history` ON (`pts_elog_history`.`log_user_activity_id` = `pts_trn_activity_approval_log`.`usr_activity_log_id`
        AND `pts_elog_history`.`command_type` = 'insert'
        AND `pts_trn_activity_approval_log`.`workflow_type` = 'start'))
    WHERE
    CASE WHEN `username` != '' THEN
     `pts_trn_activity_approval_log`.`user_name` = `username` 
     else true end
      AND DATE_FORMAT(FROM_UNIXTIME(`pts_trn_activity_approval_log`.`activity_time`), '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')
            
            UNION 
            SELECT 
        `pts_trn_user_activity_log`.`doc_id` AS `doc_id`,
            `pts_elog_history`.`activity_name` AS `logname`,
            `pts_elog_history`.`type` AS `actname`,
            0 AS `0`,
            0 AS `0`,
            `pts_elog_history`.`created_by_user` AS `user_name`,
            DATE_FORMAT(`pts_elog_history`.`created_on`, '%Y-%m-%d %H:%i:%s') AS `datetime`,
            DATE_FORMAT(`pts_elog_history`.`created_on`, '%Y-%m-%d') AS `activity_start`,
            `pts_trn_user_activity_log`.`room_code` AS `room_code`,
            `pts_trn_activity_approval_log`.`activity_remarks` AS `activity_remarks`,
            `mst_room`.`room_name` AS `room_name`,
            `pts_trn_user_activity_log`.`batch_no` AS `batch_no`,
            `pts_trn_user_activity_log`.`product_code` AS `product_code`,
            `mst_product`.`product_name` AS `product_name`,
            0 AS `batch_in_time`,
            0 AS `batch_out_time`,
            0 AS `room_in_time`,
            0 AS `room_out_time`,
            `pts_elog_history`.`table_extra_field` AS `table_extra_field`,
            `pts_elog_history`.`record_type` AS `record_type`,
            `pts_elog_history`.`command_type` AS `command_type`,
            `pts_elog_history`.`table_name` AS `table_name`,
            `pts_elog_history`.`id` AS `history_id`,
            0 AS `0`,
            (SELECT 
                    `peh1`.`table_extra_field` AS `previous_data`
                FROM
                    `pts_elog_history` `peh1`
                WHERE
                    `peh1`.`log_user_activity_id` = `pts_elog_history`.`log_user_activity_id`
                        AND `peh1`.`record_type` = 'log'
                        AND `peh1`.`id` < `pts_elog_history`.`id`
                ORDER BY `peh1`.`id` DESC
                LIMIT 1) AS `previous_data`,
            (SELECT 
                    `peh1`.`table_extra_field` AS `master_previous_data`
                FROM
                    `pts_elog_history` `peh1`
                WHERE
                    `peh1`.`primary_id` = `pts_elog_history`.`primary_id`
                        AND `peh1`.`table_name` = `pts_elog_history`.`table_name`
                        AND `peh1`.`record_type` = 'master'
                        AND `peh1`.`id` < `pts_elog_history`.`id`
                ORDER BY `peh1`.`id` DESC
                LIMIT 1) AS `master_previous_data`
    FROM
        (((((`pts_elog_history`
    LEFT JOIN `pts_trn_user_activity_log` ON (`pts_elog_history`.`log_user_activity_id` = `pts_trn_user_activity_log`.`id`))
    LEFT JOIN `pts_trn_activity_approval_log` ON (`pts_elog_history`.`log_user_activity_id` = `pts_trn_activity_approval_log`.`usr_activity_log_id`))
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_user_activity_log`.`room_code`))
    LEFT JOIN `mst_product` ON (`mst_product`.`product_code` = `pts_trn_user_activity_log`.`product_code`))
    LEFT JOIN `mst_employee` ON (`mst_employee`.`id` = `pts_elog_history`.`created_by`))
    WHERE
        (`pts_elog_history`.`command_type` <> 'insert'
            OR `pts_elog_history`.`record_type` <> 'log')
            AND
            CASE WHEN `username` != '' THEN
             `pts_elog_history`.`created_by_user` = `username`
            ELSE true end
            AND DATE_FORMAT(`pts_elog_history`.`created_on`, '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')
            
    GROUP BY `pts_elog_history`.`id`) `uniontable`) UNION SELECT 
    0 AS `0`,
    'Batch In' AS `logname`,
    0 AS `actname`,
    0 AS `My_exp_0`,
    0 AS `My_exp_1_0`,
    `pts_trn_batchin`.`emp_name` AS `emp_name`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_in_time`),
            '%Y-%m-%d %H:%i:%s') AS `datetime`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_in_time`),
            '%Y-%m-%d') AS `activity_start`,
    `mst_room`.`room_code` AS `room_code`,
    0 AS `My_exp_2_0`,
    `mst_room`.`room_name` AS `room_name`,
    0 AS `My_exp_3_0`,
    0 AS `My_exp_4_0`,
    0 AS `My_exp_5_0`,
    `pts_trn_batchin`.`batch_in_time` AS `batch_in_time`,
    0 AS `batch_out_time`,
    0 AS `room_in_time`,
    0 AS `room_out_time`,
    0 AS `table_extra_field`,
    0 AS `record_type`,
    0 AS `command_type`,
    0 AS `My_exp_6_0`,
    0 AS `My_exp_7_0`,
    0 AS `My_exp_8_0`,
    0 AS `My_exp_9_0`,
    0 AS `My_exp_10_0`
FROM
    (`pts_trn_batchin`
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_batchin`.`room_code`)) 
    where
     CASE WHEN `username` != '' THEN
      `pts_trn_batchin`.`emp_name` = `username`
      else true end
   AND DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_in_time`), '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')
           
            
UNION SELECT 
    0 AS `0`,
    'Batch Out' AS `logname`,
    0 AS `actname`,
    0 AS `0`,
    0 AS `0`,
    `pts_trn_batchin`.`emp_name` AS `emp_name`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_out_time`),
            '%Y-%m-%d %H:%i:%s') AS `datetime`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_out_time`),
            '%Y-%m-%d') AS `activity_start`,
    `mst_room`.`room_code` AS `room_code`,
    0 AS `0`,
    `mst_room`.`room_name` AS `room_name`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `batch_in_time`,
    `pts_trn_batchin`.`batch_out_time` AS `batch_out_time`,
    0 AS `room_in_time`,
    0 AS `room_out_time`,
    0 AS `table_extra_field`,
    0 AS `record_type`,
    0 AS `command_type`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`
FROM
    (`pts_trn_batchin`
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_batchin`.`room_code`)) 
     where
      CASE WHEN `username` != '' THEN
       `pts_trn_batchin`.`emp_name` = `username` 
       else true end
   AND DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_out_time`), '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')
           
UNION SELECT 
    0 AS `0`,
    'Room In' AS `logname`,
    0 AS `actname`,
    0 AS `0`,
    0 AS `0`,
    `pts_trn_emp_roomin`.`emp_name` AS `emp_name`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_in_time`),
            '%Y-%m-%d %H:%i:%s') AS `datetime`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_in_time`),
            '%Y-%m-%d') AS `activity_start`,
    `mst_room`.`room_code` AS `room_code`,
    0 AS `0`,
    `mst_room`.`room_name` AS `room_name`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `batch_in_time`,
    0 AS `batch_out_time`,
    `pts_trn_emp_roomin`.`room_in_time` AS `room_in_time`,
    0 AS `room_out_time`,
    0 AS `table_extra_field`,
    0 AS `record_type`,
    0 AS `command_type`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`
FROM
    (`pts_trn_emp_roomin`
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_emp_roomin`.`room_code`)) 
    where
     CASE WHEN `username` != '' THEN
     `pts_trn_emp_roomin`.`emp_name` = `username`
     else true end
    AND DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_in_time`), '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')
            
UNION SELECT 
    0 AS `0`,
    'Room Out' AS `logname`,
    0 AS `actname`,
    0 AS `0`,
    0 AS `0`,
    `pts_trn_emp_roomin`.`emp_name` AS `emp_name`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_out_time`),
            '%Y-%m-%d %H:%i:%s') AS `datetime`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_out_time`),
            '%Y-%m-%d') AS `activity_start`,
    `mst_room`.`room_code` AS `room_code`,
    0 AS `0`,
    `mst_room`.`room_name` AS `room_name`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `batch_in_time`,
    0 AS `batch_out_time`,
    0 AS `room_in_time`,
    `pts_trn_emp_roomin`.`room_out_time` AS `room_out_time`,
    0 AS `table_extra_field`,
    0 AS `record_type`,
    0 AS `command_type`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`
FROM
    (`pts_trn_emp_roomin`
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_emp_roomin`.`room_code`))
     where
      CASE WHEN `username` != '' THEN
      `pts_trn_emp_roomin`.`emp_name` = `username`
      else true end
    AND DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_out_time`), '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d') LIMIT LowerBound,UpperBound;
    
    
    -- Count  --- ---- -------- -------
 select COUNT(*) AS `RecordCount`  from
   ( (SELECT 
    `uniontable`.`doc_id` AS `doc_id`,
    `uniontable`.`logname` AS `logname`,
    `uniontable`.`actname` AS `actname`,
    `uniontable`.`approval_status` AS `approval_status`,
    `uniontable`.`workflow_type` AS `workflow_type`,
    `uniontable`.`user_name` AS `user_name`,
    `uniontable`.`datetime` AS `datetime`,
    `uniontable`.`activity_start` AS `activity_start`,
    `uniontable`.`room_code` AS `room_code`,
    `uniontable`.`activity_remarks` AS `activity_remarks`,
    `uniontable`.`room_name` AS `room_name`,
    `uniontable`.`batch_no` AS `batch_no`,
    `uniontable`.`product_code` AS `product_code`,
    `uniontable`.`product_name` AS `product_name`,
    `uniontable`.`batch_in_time` AS `batch_in_time`,
    `uniontable`.`batch_out_time` AS `batch_out_time`,
    `uniontable`.`room_in_time` AS `room_in_time`,
    `uniontable`.`room_out_time` AS `room_out_time`,
    `uniontable`.`table_extra_field` AS `table_extra_field`,
    `uniontable`.`record_type` AS `record_type`,
    `uniontable`.`command_type` AS `command_type`,
    `uniontable`.`table_name` AS `table_name`,
    `uniontable`.`history_id` AS `history_id`,
    `uniontable`.`action_performed` AS `action_performed`,
    `uniontable`.`previous_data` AS `previous_data`,
    `uniontable`.`master_previous_data` AS `master_previous_data`
FROM
    (SELECT 
        `pts_trn_user_activity_log`.`doc_id` AS `doc_id`,
            `pts_trn_room_log_activity`.`activity_name` AS `logname`,
            `pts_mst_activity`.`activity_name` AS `actname`,
            `pts_trn_activity_approval_log`.`approval_status` AS `approval_status`,
            `pts_trn_activity_approval_log`.`workflow_type` AS `workflow_type`,
            `pts_trn_activity_approval_log`.`user_name` AS `user_name`,
            DATE_FORMAT(FROM_UNIXTIME(`pts_trn_activity_approval_log`.`activity_time`), '%Y-%m-%d %H:%i:%s') AS `datetime`,
            DATE_FORMAT(FROM_UNIXTIME(`pts_trn_activity_approval_log`.`activity_time`), '%Y-%m-%d') AS `activity_start`,
            `pts_trn_user_activity_log`.`room_code` AS `room_code`,
            `pts_trn_activity_approval_log`.`activity_remarks` AS `activity_remarks`,
            `mst_room`.`room_name` AS `room_name`,
            `pts_trn_user_activity_log`.`batch_no` AS `batch_no`,
            `pts_trn_user_activity_log`.`product_code` AS `product_code`,
            `mst_product`.`product_name` AS `product_name`,
            0 AS `batch_in_time`,
            0 AS `batch_out_time`,
            0 AS `room_in_time`,
            0 AS `room_out_time`,
            `pts_elog_history`.`table_extra_field` AS `table_extra_field`,
            `pts_elog_history`.`record_type` AS `record_type`,
            `pts_elog_history`.`command_type` AS `command_type`,
            `pts_elog_history`.`table_name` AS `table_name`,
            `pts_elog_history`.`id` AS `history_id`,
            0 AS `action_performed`,
            0 AS `previous_data`,
            0 AS `master_previous_data`
    FROM
        (((((((`pts_trn_activity_approval_log`
    LEFT JOIN `pts_trn_user_activity_log` ON (`pts_trn_activity_approval_log`.`usr_activity_log_id` = `pts_trn_user_activity_log`.`id`))
    LEFT JOIN `pts_mst_activity` ON (`pts_mst_activity`.`id` = `pts_trn_user_activity_log`.`activity_id`))
    LEFT JOIN `pts_trn_room_log_activity` ON (`pts_trn_room_log_activity`.`id` = `pts_trn_user_activity_log`.`roomlogactivity`))
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_user_activity_log`.`room_code`))
    LEFT JOIN `pts_mst_sys_id` ON (`pts_mst_sys_id`.`room_code` = `mst_room`.`room_code`))
    LEFT JOIN `mst_product` ON (`mst_product`.`product_code` = `pts_trn_user_activity_log`.`product_code`))
    LEFT JOIN `pts_elog_history` ON (`pts_elog_history`.`log_user_activity_id` = `pts_trn_activity_approval_log`.`usr_activity_log_id`
        AND `pts_elog_history`.`command_type` = 'insert'
        AND `pts_trn_activity_approval_log`.`workflow_type` = 'start'))
    WHERE
    CASE WHEN `username` != '' THEN
     `pts_trn_activity_approval_log`.`user_name` = `username` 
     else true end
      AND DATE_FORMAT(FROM_UNIXTIME(`pts_trn_activity_approval_log`.`activity_time`), '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')
            
            UNION 
            SELECT 
        `pts_trn_user_activity_log`.`doc_id` AS `doc_id`,
            `pts_elog_history`.`activity_name` AS `logname`,
            `pts_elog_history`.`type` AS `actname`,
            0 AS `0`,
            0 AS `0`,
            `pts_elog_history`.`created_by_user` AS `user_name`,
            DATE_FORMAT(`pts_elog_history`.`created_on`, '%Y-%m-%d %H:%i:%s') AS `datetime`,
            DATE_FORMAT(`pts_elog_history`.`created_on`, '%Y-%m-%d') AS `activity_start`,
            `pts_trn_user_activity_log`.`room_code` AS `room_code`,
            `pts_trn_activity_approval_log`.`activity_remarks` AS `activity_remarks`,
            `mst_room`.`room_name` AS `room_name`,
            `pts_trn_user_activity_log`.`batch_no` AS `batch_no`,
            `pts_trn_user_activity_log`.`product_code` AS `product_code`,
            `mst_product`.`product_name` AS `product_name`,
            0 AS `batch_in_time`,
            0 AS `batch_out_time`,
            0 AS `room_in_time`,
            0 AS `room_out_time`,
            `pts_elog_history`.`table_extra_field` AS `table_extra_field`,
            `pts_elog_history`.`record_type` AS `record_type`,
            `pts_elog_history`.`command_type` AS `command_type`,
            `pts_elog_history`.`table_name` AS `table_name`,
            `pts_elog_history`.`id` AS `history_id`,
            0 AS `0`,
            (SELECT 
                    `peh1`.`table_extra_field` AS `previous_data`
                FROM
                    `pts_elog_history` `peh1`
                WHERE
                    `peh1`.`log_user_activity_id` = `pts_elog_history`.`log_user_activity_id`
                        AND `peh1`.`record_type` = 'log'
                        AND `peh1`.`id` < `pts_elog_history`.`id`
                ORDER BY `peh1`.`id` DESC
                LIMIT 1) AS `previous_data`,
            (SELECT 
                    `peh1`.`table_extra_field` AS `master_previous_data`
                FROM
                    `pts_elog_history` `peh1`
                WHERE
                    `peh1`.`primary_id` = `pts_elog_history`.`primary_id`
                        AND `peh1`.`table_name` = `pts_elog_history`.`table_name`
                        AND `peh1`.`record_type` = 'master'
                        AND `peh1`.`id` < `pts_elog_history`.`id`
                ORDER BY `peh1`.`id` DESC
                LIMIT 1) AS `master_previous_data`
    FROM
        (((((`pts_elog_history`
    LEFT JOIN `pts_trn_user_activity_log` ON (`pts_elog_history`.`log_user_activity_id` = `pts_trn_user_activity_log`.`id`))
    LEFT JOIN `pts_trn_activity_approval_log` ON (`pts_elog_history`.`log_user_activity_id` = `pts_trn_activity_approval_log`.`usr_activity_log_id`))
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_user_activity_log`.`room_code`))
    LEFT JOIN `mst_product` ON (`mst_product`.`product_code` = `pts_trn_user_activity_log`.`product_code`))
    LEFT JOIN `mst_employee` ON (`mst_employee`.`id` = `pts_elog_history`.`created_by`))
    WHERE
        (`pts_elog_history`.`command_type` <> 'insert'
            OR `pts_elog_history`.`record_type` <> 'log')
            AND
            CASE WHEN `username` != '' THEN
             `pts_elog_history`.`created_by_user` = `username`
            ELSE true end
            AND DATE_FORMAT(`pts_elog_history`.`created_on`, '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')
            
    GROUP BY `pts_elog_history`.`id`) `uniontable`) UNION SELECT 
    0 AS `0`,
    'Batch In' AS `logname`,
    0 AS `actname`,
    0 AS `My_exp_0`,
    0 AS `My_exp_1_0`,
    `pts_trn_batchin`.`emp_name` AS `emp_name`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_in_time`),
            '%Y-%m-%d %H:%i:%s') AS `datetime`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_in_time`),
            '%Y-%m-%d') AS `activity_start`,
    `mst_room`.`room_code` AS `room_code`,
    0 AS `My_exp_2_0`,
    `mst_room`.`room_name` AS `room_name`,
    0 AS `My_exp_3_0`,
    0 AS `My_exp_4_0`,
    0 AS `My_exp_5_0`,
    `pts_trn_batchin`.`batch_in_time` AS `batch_in_time`,
    0 AS `batch_out_time`,
    0 AS `room_in_time`,
    0 AS `room_out_time`,
    0 AS `table_extra_field`,
    0 AS `record_type`,
    0 AS `command_type`,
    0 AS `My_exp_6_0`,
    0 AS `My_exp_7_0`,
    0 AS `My_exp_8_0`,
    0 AS `My_exp_9_0`,
    0 AS `My_exp_10_0`
FROM
    (`pts_trn_batchin`
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_batchin`.`room_code`)) 
    where
     CASE WHEN `username` != '' THEN
      `pts_trn_batchin`.`emp_name` = `username` 
      else true end
   AND DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_in_time`), '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')
           
            
UNION SELECT 
    0 AS `0`,
    'Batch Out' AS `logname`,
    0 AS `actname`,
    0 AS `0`,
    0 AS `0`,
    `pts_trn_batchin`.`emp_name` AS `emp_name`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_out_time`),
            '%Y-%m-%d %H:%i:%s') AS `datetime`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_out_time`),
            '%Y-%m-%d') AS `activity_start`,
    `mst_room`.`room_code` AS `room_code`,
    0 AS `0`,
    `mst_room`.`room_name` AS `room_name`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `batch_in_time`,
    `pts_trn_batchin`.`batch_out_time` AS `batch_out_time`,
    0 AS `room_in_time`,
    0 AS `room_out_time`,
    0 AS `table_extra_field`,
    0 AS `record_type`,
    0 AS `command_type`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`
FROM
    (`pts_trn_batchin`
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_batchin`.`room_code`)) 
     where
      CASE WHEN `username` != '' THEN
       `pts_trn_batchin`.`emp_name` = `username` 
       else true end
   AND DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_out_time`), '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')
           
UNION SELECT 
    0 AS `0`,
    'Room In' AS `logname`,
    0 AS `actname`,
    0 AS `0`,
    0 AS `0`,
    `pts_trn_emp_roomin`.`emp_name` AS `emp_name`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_in_time`),
            '%Y-%m-%d %H:%i:%s') AS `datetime`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_in_time`),
            '%Y-%m-%d') AS `activity_start`,
    `mst_room`.`room_code` AS `room_code`,
    0 AS `0`,
    `mst_room`.`room_name` AS `room_name`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `batch_in_time`,
    0 AS `batch_out_time`,
    `pts_trn_emp_roomin`.`room_in_time` AS `room_in_time`,
    0 AS `room_out_time`,
    0 AS `table_extra_field`,
    0 AS `record_type`,
    0 AS `command_type`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`
FROM
    (`pts_trn_emp_roomin`
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_emp_roomin`.`room_code`)) 
    where
     CASE WHEN `username` != '' THEN
     `pts_trn_emp_roomin`.`emp_name` = `username`
     else true end
    AND DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_in_time`), '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')
            
UNION SELECT 
    0 AS `0`,
    'Room Out' AS `logname`,
    0 AS `actname`,
    0 AS `0`,
    0 AS `0`,
    `pts_trn_emp_roomin`.`emp_name` AS `emp_name`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_out_time`),
            '%Y-%m-%d %H:%i:%s') AS `datetime`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_out_time`),
            '%Y-%m-%d') AS `activity_start`,
    `mst_room`.`room_code` AS `room_code`,
    0 AS `0`,
    `mst_room`.`room_name` AS `room_name`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `batch_in_time`,
    0 AS `batch_out_time`,
    0 AS `room_in_time`,
    `pts_trn_emp_roomin`.`room_out_time` AS `room_out_time`,
    0 AS `table_extra_field`,
    0 AS `record_type`,
    0 AS `command_type`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`
FROM
    (`pts_trn_emp_roomin`
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_emp_roomin`.`room_code`))
     where
      CASE WHEN `username` != '' THEN
      `pts_trn_emp_roomin`.`emp_name` = `username`
      else true end
    AND DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_out_time`), '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')) as test;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getAuditdataPrint` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `getAuditdataPrint`(IN `username` varchar(255) CHARSET utf8, IN `startdate` datetime, IN `enddate`  datetime, IN `pageNo` INT,IN `noOfRecords` INT)
    DETERMINISTIC
    COMMENT 'SP - To Get Audit List'
BEGIN
	DECLARE LowerBound INT;
	DECLARE UpperBound INT;
 
	SET LowerBound = ((pageNo*noOfRecords)-noOfRecords);
	SET UpperBound = noOfRecords;
(SELECT 
    `uniontable`.`doc_id` AS `doc_id`,
    `uniontable`.`logname` AS `logname`,
    `uniontable`.`actname` AS `actname`,
    `uniontable`.`approval_status` AS `approval_status`,
    `uniontable`.`workflow_type` AS `workflow_type`,
    `uniontable`.`user_name` AS `user_name`,
    `uniontable`.`datetime` AS `datetime`,
    `uniontable`.`activity_start` AS `activity_start`,
    `uniontable`.`room_code` AS `room_code`,
    `uniontable`.`activity_remarks` AS `activity_remarks`,
    `uniontable`.`room_name` AS `room_name`,
    `uniontable`.`batch_no` AS `batch_no`,
    `uniontable`.`product_code` AS `product_code`,
    `uniontable`.`product_name` AS `product_name`,
    `uniontable`.`batch_in_time` AS `batch_in_time`,
    `uniontable`.`batch_out_time` AS `batch_out_time`,
    `uniontable`.`room_in_time` AS `room_in_time`,
    `uniontable`.`room_out_time` AS `room_out_time`,
    `uniontable`.`table_extra_field` AS `table_extra_field`,
    `uniontable`.`record_type` AS `record_type`,
    `uniontable`.`command_type` AS `command_type`,
    `uniontable`.`table_name` AS `table_name`,
    `uniontable`.`history_id` AS `history_id`,
    `uniontable`.`action_performed` AS `action_performed`,
    `uniontable`.`previous_data` AS `previous_data`,
    `uniontable`.`master_previous_data` AS `master_previous_data`
FROM
    (SELECT 
        `pts_trn_user_activity_log`.`doc_id` AS `doc_id`,
            `pts_trn_room_log_activity`.`activity_name` AS `logname`,
            `pts_mst_activity`.`activity_name` AS `actname`,
            `pts_trn_activity_approval_log`.`approval_status` AS `approval_status`,
            `pts_trn_activity_approval_log`.`workflow_type` AS `workflow_type`,
            `pts_trn_activity_approval_log`.`user_name` AS `user_name`,
            DATE_FORMAT(FROM_UNIXTIME(`pts_trn_activity_approval_log`.`activity_time`), '%Y-%m-%d %H:%i:%s') AS `datetime`,
            DATE_FORMAT(FROM_UNIXTIME(`pts_trn_activity_approval_log`.`activity_time`), '%Y-%m-%d') AS `activity_start`,
            `pts_trn_user_activity_log`.`room_code` AS `room_code`,
            `pts_trn_activity_approval_log`.`activity_remarks` AS `activity_remarks`,
            `mst_room`.`room_name` AS `room_name`,
            `pts_trn_user_activity_log`.`batch_no` AS `batch_no`,
            `pts_trn_user_activity_log`.`product_code` AS `product_code`,
            `mst_product`.`product_name` AS `product_name`,
            0 AS `batch_in_time`,
            0 AS `batch_out_time`,
            0 AS `room_in_time`,
            0 AS `room_out_time`,
            `pts_elog_history`.`table_extra_field` AS `table_extra_field`,
            `pts_elog_history`.`record_type` AS `record_type`,
            `pts_elog_history`.`command_type` AS `command_type`,
            `pts_elog_history`.`table_name` AS `table_name`,
            `pts_elog_history`.`id` AS `history_id`,
            0 AS `action_performed`,
            0 AS `previous_data`,
            0 AS `master_previous_data`
    FROM
        (((((((`pts_trn_activity_approval_log`
    LEFT JOIN `pts_trn_user_activity_log` ON (`pts_trn_activity_approval_log`.`usr_activity_log_id` = `pts_trn_user_activity_log`.`id`))
    LEFT JOIN `pts_mst_activity` ON (`pts_mst_activity`.`id` = `pts_trn_user_activity_log`.`activity_id`))
    LEFT JOIN `pts_trn_room_log_activity` ON (`pts_trn_room_log_activity`.`id` = `pts_trn_user_activity_log`.`roomlogactivity`))
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_user_activity_log`.`room_code`))
    LEFT JOIN `pts_mst_sys_id` ON (`pts_mst_sys_id`.`room_code` = `mst_room`.`room_code`))
    LEFT JOIN `mst_product` ON (`mst_product`.`product_code` = `pts_trn_user_activity_log`.`product_code`))
    LEFT JOIN `pts_elog_history` ON (`pts_elog_history`.`log_user_activity_id` = `pts_trn_activity_approval_log`.`usr_activity_log_id`
        AND `pts_elog_history`.`command_type` = 'insert'
        AND `pts_trn_activity_approval_log`.`workflow_type` = 'start'))
    WHERE
    CASE WHEN `username` != '' THEN
     `pts_trn_activity_approval_log`.`user_name` = `username` 
     else true end
      AND DATE_FORMAT(FROM_UNIXTIME(`pts_trn_activity_approval_log`.`activity_time`), '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')
            
            UNION 
            SELECT 
        `pts_trn_user_activity_log`.`doc_id` AS `doc_id`,
            `pts_elog_history`.`activity_name` AS `logname`,
            `pts_elog_history`.`type` AS `actname`,
            0 AS `0`,
            0 AS `0`,
            `pts_elog_history`.`created_by_user` AS `user_name`,
            DATE_FORMAT(`pts_elog_history`.`created_on`, '%Y-%m-%d %H:%i:%s') AS `datetime`,
            DATE_FORMAT(`pts_elog_history`.`created_on`, '%Y-%m-%d') AS `activity_start`,
            `pts_trn_user_activity_log`.`room_code` AS `room_code`,
            `pts_trn_activity_approval_log`.`activity_remarks` AS `activity_remarks`,
            `mst_room`.`room_name` AS `room_name`,
            `pts_trn_user_activity_log`.`batch_no` AS `batch_no`,
            `pts_trn_user_activity_log`.`product_code` AS `product_code`,
            `mst_product`.`product_name` AS `product_name`,
            0 AS `batch_in_time`,
            0 AS `batch_out_time`,
            0 AS `room_in_time`,
            0 AS `room_out_time`,
            `pts_elog_history`.`table_extra_field` AS `table_extra_field`,
            `pts_elog_history`.`record_type` AS `record_type`,
            `pts_elog_history`.`command_type` AS `command_type`,
            `pts_elog_history`.`table_name` AS `table_name`,
            `pts_elog_history`.`id` AS `history_id`,
            0 AS `0`,
            (SELECT 
                    `peh1`.`table_extra_field` AS `previous_data`
                FROM
                    `pts_elog_history` `peh1`
                WHERE
                    `peh1`.`log_user_activity_id` = `pts_elog_history`.`log_user_activity_id`
                        AND `peh1`.`record_type` = 'log'
                        AND `peh1`.`id` < `pts_elog_history`.`id`
                ORDER BY `peh1`.`id` DESC
                LIMIT 1) AS `previous_data`,
            (SELECT 
                    `peh1`.`table_extra_field` AS `master_previous_data`
                FROM
                    `pts_elog_history` `peh1`
                WHERE
                    `peh1`.`primary_id` = `pts_elog_history`.`primary_id`
                        AND `peh1`.`table_name` = `pts_elog_history`.`table_name`
                        AND `peh1`.`record_type` = 'master'
                        AND `peh1`.`id` < `pts_elog_history`.`id`
                ORDER BY `peh1`.`id` DESC
                LIMIT 1) AS `master_previous_data`
    FROM
        (((((`pts_elog_history`
    LEFT JOIN `pts_trn_user_activity_log` ON (`pts_elog_history`.`log_user_activity_id` = `pts_trn_user_activity_log`.`id`))
    LEFT JOIN `pts_trn_activity_approval_log` ON (`pts_elog_history`.`log_user_activity_id` = `pts_trn_activity_approval_log`.`usr_activity_log_id`))
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_user_activity_log`.`room_code`))
    LEFT JOIN `mst_product` ON (`mst_product`.`product_code` = `pts_trn_user_activity_log`.`product_code`))
    LEFT JOIN `mst_employee` ON (`mst_employee`.`id` = `pts_elog_history`.`created_by`))
    WHERE
        (`pts_elog_history`.`command_type` <> 'insert'
            OR `pts_elog_history`.`record_type` <> 'log')
            AND
            CASE WHEN `username` != '' THEN
             `pts_elog_history`.`created_by_user` = `username`
            ELSE true end
            AND DATE_FORMAT(`pts_elog_history`.`created_on`, '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')
            
    GROUP BY `pts_elog_history`.`id`) `uniontable`) UNION SELECT 
    0 AS `0`,
    'Batch In' AS `logname`,
    0 AS `actname`,
    0 AS `My_exp_0`,
    0 AS `My_exp_1_0`,
    `pts_trn_batchin`.`emp_name` AS `emp_name`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_in_time`),
            '%Y-%m-%d %H:%i:%s') AS `datetime`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_in_time`),
            '%Y-%m-%d') AS `activity_start`,
    `mst_room`.`room_code` AS `room_code`,
    0 AS `My_exp_2_0`,
    `mst_room`.`room_name` AS `room_name`,
    0 AS `My_exp_3_0`,
    0 AS `My_exp_4_0`,
    0 AS `My_exp_5_0`,
    `pts_trn_batchin`.`batch_in_time` AS `batch_in_time`,
    0 AS `batch_out_time`,
    0 AS `room_in_time`,
    0 AS `room_out_time`,
    0 AS `table_extra_field`,
    0 AS `record_type`,
    0 AS `command_type`,
    0 AS `My_exp_6_0`,
    0 AS `My_exp_7_0`,
    0 AS `My_exp_8_0`,
    0 AS `My_exp_9_0`,
    0 AS `My_exp_10_0`
FROM
    (`pts_trn_batchin`
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_batchin`.`room_code`)) 
    where
     CASE WHEN `username` != '' THEN
      `pts_trn_batchin`.`emp_name` = `username` 
      else true end
   AND DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_in_time`), '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')
           
            
UNION SELECT 
    0 AS `0`,
    'Batch Out' AS `logname`,
    0 AS `actname`,
    0 AS `0`,
    0 AS `0`,
    `pts_trn_batchin`.`emp_name` AS `emp_name`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_out_time`),
            '%Y-%m-%d %H:%i:%s') AS `datetime`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_out_time`),
            '%Y-%m-%d') AS `activity_start`,
    `mst_room`.`room_code` AS `room_code`,
    0 AS `0`,
    `mst_room`.`room_name` AS `room_name`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `batch_in_time`,
    `pts_trn_batchin`.`batch_out_time` AS `batch_out_time`,
    0 AS `room_in_time`,
    0 AS `room_out_time`,
    0 AS `table_extra_field`,
    0 AS `record_type`,
    0 AS `command_type`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`
FROM
    (`pts_trn_batchin`
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_batchin`.`room_code`)) 
     where
      CASE WHEN `username` != '' THEN
       `pts_trn_batchin`.`emp_name` = `username` 
       else true end
   AND DATE_FORMAT(FROM_UNIXTIME(`pts_trn_batchin`.`batch_out_time`), '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')
           
UNION SELECT 
    0 AS `0`,
    'Room In' AS `logname`,
    0 AS `actname`,
    0 AS `0`,
    0 AS `0`,
    `pts_trn_emp_roomin`.`emp_name` AS `emp_name`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_in_time`),
            '%Y-%m-%d %H:%i:%s') AS `datetime`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_in_time`),
            '%Y-%m-%d') AS `activity_start`,
    `mst_room`.`room_code` AS `room_code`,
    0 AS `0`,
    `mst_room`.`room_name` AS `room_name`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `batch_in_time`,
    0 AS `batch_out_time`,
    `pts_trn_emp_roomin`.`room_in_time` AS `room_in_time`,
    0 AS `room_out_time`,
    0 AS `table_extra_field`,
    0 AS `record_type`,
    0 AS `command_type`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`
FROM
    (`pts_trn_emp_roomin`
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_emp_roomin`.`room_code`)) 
    where
     CASE WHEN `username` != '' THEN
     `pts_trn_emp_roomin`.`emp_name` = `username`
     else true end
    AND DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_in_time`), '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d')
            
UNION SELECT 
    0 AS `0`,
    'Room Out' AS `logname`,
    0 AS `actname`,
    0 AS `0`,
    0 AS `0`,
    `pts_trn_emp_roomin`.`emp_name` AS `emp_name`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_out_time`),
            '%Y-%m-%d %H:%i:%s') AS `datetime`,
    DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_out_time`),
            '%Y-%m-%d') AS `activity_start`,
    `mst_room`.`room_code` AS `room_code`,
    0 AS `0`,
    `mst_room`.`room_name` AS `room_name`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `batch_in_time`,
    0 AS `batch_out_time`,
    0 AS `room_in_time`,
    `pts_trn_emp_roomin`.`room_out_time` AS `room_out_time`,
    0 AS `table_extra_field`,
    0 AS `record_type`,
    0 AS `command_type`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`,
    0 AS `0`
FROM
    (`pts_trn_emp_roomin`
    LEFT JOIN `mst_room` ON (`mst_room`.`room_code` = `pts_trn_emp_roomin`.`room_code`))
     where
      CASE WHEN `username` != '' THEN
      `pts_trn_emp_roomin`.`emp_name` = `username`
      else true end
    AND DATE_FORMAT(FROM_UNIXTIME(`pts_trn_emp_roomin`.`room_out_time`), '%Y-%m-%d') BETWEEN DATE_FORMAT(`startdate`, '%Y-%m-%d') AND DATE_FORMAT(`enddate`, '%Y-%m-%d');
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-14 17:04:40
