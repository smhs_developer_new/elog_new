var page_session_start_date_time = new Date();
var page_session_start_time = page_session_start_date_time.getTime();
$(document).ready(function(){
    checkSessionOut();//reruns after 10 seconds
});
$(document.body).keydown( function(e){
    page_session_start_date_time = new Date();
    page_session_start_time = page_session_start_date_time.getTime();
});
$(document.body).click(function(){
    page_session_start_date_time = new Date();
    page_session_start_time = page_session_start_date_time.getTime();
});
function checkSessionOut(){
    var current_date_time = new Date();//milliseceonds
    var current_time = current_date_time.getTime();//milliseceonds
    var time_difference = current_time - page_session_start_time;
    if(time_difference>=300000){
        document.location.href = $('#base').val()+'/User/autoLogout';
    }
    setTimeout(checkSessionOut, 1000);//reruns after 1 seconds
}