-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: elog_new_v3
-- ------------------------------------------------------
-- Server version	5.7.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary table structure for view `getstart_stoped_data`
--

DROP TABLE IF EXISTS `getstart_stoped_data`;
/*!50001 DROP VIEW IF EXISTS `getstart_stoped_data`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `getstart_stoped_data` AS SELECT 
 1 AS `status`,
 1 AS `area_code`,
 1 AS `room_code`,
 1 AS `status_name`,
 1 AS `product_description`,
 1 AS `document_no`,
 1 AS `batch_no`,
 1 AS `lot_no`,
 1 AS `created_on`,
 1 AS `operation_type`,
 1 AS `created_by`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `getstatusrecord`
--

DROP TABLE IF EXISTS `getstatusrecord`;
/*!50001 DROP VIEW IF EXISTS `getstatusrecord`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `getstatusrecord` AS SELECT 
 1 AS `warid`,
 1 AS `header_id`,
 1 AS `status`,
 1 AS `area_code`,
 1 AS `room_code`,
 1 AS `status_name`,
 1 AS `product_description`,
 1 AS `document_no`,
 1 AS `batch_no`,
 1 AS `lot_no`,
 1 AS `created_on`,
 1 AS `operation_type`,
 1 AS `doneby`,
 1 AS `actionby`,
 1 AS `roleid`,
 1 AS `actiontime`,
 1 AS `type`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `log_table`
--

DROP TABLE IF EXISTS `log_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(500) DEFAULT NULL,
  `activity` varchar(500) DEFAULT NULL,
  `oldval` varchar(500) DEFAULT NULL,
  `newval` varchar(500) DEFAULT NULL,
  `created_by` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_table`
--

LOCK TABLES `log_table` WRITE;
/*!40000 ALTER TABLE `log_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_tableaction`
--

DROP TABLE IF EXISTS `log_tableaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_tableaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(200) DEFAULT NULL,
  `tablename` varchar(500) DEFAULT NULL,
  `tableid` int(11) DEFAULT NULL,
  `created_by` varchar(500) DEFAULT NULL,
  `created_on` varchar(500) DEFAULT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_tableaction`
--

LOCK TABLES `log_tableaction` WRITE;
/*!40000 ALTER TABLE `log_tableaction` DISABLE KEYS */;
INSERT INTO `log_tableaction` VALUES (1,'Updated','mst_department',1,'Superadmin','2020-04-07 12:18:00',NULL,NULL),(2,'Updated','mst_area',2,'Superadmin','2020-04-07 12:18:47','Superadmin','2020-04-07 15:56:56');
/*!40000 ALTER TABLE `log_tableaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `log_tablechanges`
--

DROP TABLE IF EXISTS `log_tablechanges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_tablechanges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log_id` int(11) DEFAULT NULL,
  `fieldname` varchar(500) DEFAULT NULL,
  `oldvalue` text,
  `newvalue` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_tablechanges`
--

LOCK TABLES `log_tablechanges` WRITE;
/*!40000 ALTER TABLE `log_tablechanges` DISABLE KEYS */;
INSERT INTO `log_tablechanges` VALUES (1,2,'area_remarks','','Added Number of Rooms'),(2,2,'number_of_rooms','1','3');
/*!40000 ALTER TABLE `log_tablechanges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_accessory_criticalparts`
--

DROP TABLE IF EXISTS `mst_accessory_criticalparts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_accessory_criticalparts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `part_code` varchar(50) DEFAULT NULL,
  `part_name` varchar(200) DEFAULT NULL,
  `part_description` varchar(2000) DEFAULT NULL,
  `part_type` varchar(200) DEFAULT NULL,
  `part_remark` varchar(1000) DEFAULT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_accessory_criticalparts`
--

LOCK TABLES `mst_accessory_criticalparts` WRITE;
/*!40000 ALTER TABLE `mst_accessory_criticalparts` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_accessory_criticalparts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_actions`
--

DROP TABLE IF EXISTS `mst_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_actions` (
  `id` int(11) NOT NULL,
  `actions` varchar(300) NOT NULL,
  `created_by` varchar(300) NOT NULL DEFAULT 'Admin',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` varchar(300) NOT NULL DEFAULT 'Admin',
  `modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_active` bit(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_actions`
--

LOCK TABLES `mst_actions` WRITE;
/*!40000 ALTER TABLE `mst_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_activity`
--

DROP TABLE IF EXISTS `mst_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_activity` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activity_code` varchar(50) DEFAULT NULL,
  `activity_name` varchar(500) DEFAULT NULL,
  `activitytype_id` bigint(20) DEFAULT NULL,
  `activity_type` varchar(500) DEFAULT NULL,
  `activityfunc_tocall` varchar(256) NOT NULL,
  `activity_icon` varchar(500) DEFAULT NULL,
  `activity_nature` varchar(2000) DEFAULT NULL,
  `activity_duration` varchar(200) DEFAULT NULL,
  `activity_remark` varchar(2000) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2610 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_activity`
--

LOCK TABLES `mst_activity` WRITE;
/*!40000 ALTER TABLE `mst_activity` DISABLE KEYS */;
INSERT INTO `mst_activity` VALUES (107,'99','First Time Plant Set-up',15,'First Time Plant Set-up','No function','031f688cddc9b0171bdb1387b9ac4f77.png',NULL,NULL,'First time plant set up','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary '\0'),(114,'1002','Create Block ',15,'Create Plant data','Admin/mstblock','919e0e83e4a2fb5d37bbd8b936daf838.png',NULL,NULL,'Post Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(115,'1003','Create Area ',15,'Create Plant data','Admin/mstArea','89ab2a5fd8cec0255b5650c3257fcbe6.png',NULL,NULL,'Post Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(116,'1004','Create Room ',15,'Create Plant data','Admin/mstRoom','976b96687d66eab0ca8584933801215b.png',NULL,NULL,'Post Loaded','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-09 16:44:07',_binary ''),(119,'1007','Create Activity Area Mapping',15,'Create Plant data','Admin/activityMapping','aaf0dfaf83223fb1f8d5d280e1236516.png',NULL,NULL,'Post Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(123,'1008','Create Role Activity Mapping',15,'Create Plant data','Activity/activityMapping','6f66cc3fff5499b0bfc5fab1fc2fe33c.png',NULL,NULL,'Post Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(124,'1013.5','Create Employee ',15,'Create Plant data','Admin/AddEmployee','e5ab0be6756f03bb83256edd7d8a9990.png',NULL,NULL,'Post Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(125,'1004.6','Create Equipment',15,'Create Plant data','Admin/mstEquipment','3191f7ca1a6d601a9f6992c5a1f4ed32.png',NULL,NULL,'Post Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(126,'1003.7','Create Drainpoint',15,'Create Plant data','Admin/mstDrainpoint','701dc7a7e72d0724edf82cbdd809dde6.png',NULL,NULL,'Post Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(127,'1004.5','Create SOP ',15,'Create Plant data','Admin/AddSop','02a26fe96bdb87d3819f80ee65b25e38.png',NULL,NULL,'Post Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(128,'1013','Create Workflow ',15,'Create Plant data','user/workflow','aad754af7cd288b8e226ddc1f931353b.png',NULL,NULL,'Post Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(129,'1014','Add & Edit Plant Data',16,'Create Master Data','admin/mstPlantview','8c45bcdc81c95e0672529e93fe6ada70.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-09 16:07:43',_binary ''),(130,'1015','Add & Edit Block Data',16,'Create Master Data','Admin/mstBlock','ed9ba8473586a551464a5a2ebb657d6f.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-09 16:07:28',_binary ''),(131,'1016','Add & Edit Area Data',16,'Create Master Data','Admin/mstArea','27bbab95e65db3bf146672052ef990ef.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-09 16:07:01',_binary ''),(132,'1017','Add & Edit Room Data',16,'Create Master Data','Admin/mstRoom','a0915a7cbabc2a40933c7039af94c85b.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-09 16:06:42',_binary ''),(133,'1018','Add & Edit Roleid Workflow Mapping Data',16,'Create Master Data','Admin/AddRolestatusMapping','661ecc3079b9f54993992288df45c4f4.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-09 16:06:27',_binary '\0'),(134,'1019','Add & Edit Activity Type Data',16,'Create Master Data','Admin/AddCategory','bb0ac4d47751578513150e6ab72a2a63.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-09 16:06:08',_binary ''),(135,'1020','Add & Edit Activity Data',16,'Create Master Data','Admin/AddActivity','b277c4eb7d721bd94e2cbed15a1d92af.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-09 16:05:45',_binary ''),(136,'1021','Add & Edit Activity Role Data',16,'Create Master Data','Activity/activityMapping','957643a7da768636a49f79e02eb01509.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-09 16:05:30',_binary ''),(137,'1022','Add & Edit Employee Data',16,'Create Master Data','Admin/AddEmployee','895e3bae54587fd1e81770f0fffa1315.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-09 16:05:09',_binary ''),(138,'1023','Add & Edit Equipment Data',16,'Create Master Data','Admin/mstEquipment','011f9a40ff8efad397534ef772e91d39.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-09 16:04:53',_binary ''),(139,'1024','Add & Edit Drainpoint Data',16,'Create Master Data','Admin/mstDrainpoint','4360c30a96c59c55efd63582920c7ad3.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-09 16:04:28',_binary ''),(140,'1025','Add & Edit SOP Data',16,'Create Master Data','Admin/AddSop','6bae0d82adbf7306e6f468092dbeb6d2.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-09 16:04:10',_binary ''),(141,'1026','Add & Edit Workflow Data',16,'Create Master Data','user/workflow','60ea8f6fc6d969b611f7e086bf751125.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(142,'1027','View & Edit Role Status Data',16,'Create Master Data','Admin/Add_Status','b27e4418a992a248a9dda0f13a49ba46.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-09 16:12:52',_binary '\0'),(143,'1028','View & Edit Document Data',16,'Create Master Data','Function to call','3fe03625cecc4fafe159a6f8f76aa3f0.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-09 16:12:30',_binary '\0'),(144,'1029','View & Edit Sub-block',16,'Create Master Data','Admin/mstDepartment','e2850b9bc54e2bccb86b2612a64b37fa.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(145,'1030','View & Edit Role Data',16,'Create Master Data','Admin/Addrole','ba5bb5bd5979c3cba2ab5958176d4455.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(146,'1031','View & Edit Product Data',16,'Create Master Data','Admin/AddProduct','edc7eb582bace89a031cb69f58f42628.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(147,'1032','View & Edit Message Data',16,'Create Master Data','Admin/AddMsg','41969f327bae40558e01252dd5bee098.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary '\0'),(148,'1033','View & Edit Solution Data',16,'Create Master Data','Admin/AddSolution','a8a76d856f144a7dc0212bd27648e25f.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(149,'1034','View & Edit Batch Data',16,'Create Master Data','Function to call','0070e271877672b8452f0be1a65f8f63.png',NULL,NULL,'Pre-Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary '\0'),(150,'129','Create Master Data',16,'Create Master Data','Function to call',NULL,NULL,NULL,NULL,'A-000',NULL,'2020-01-15 05:23:15',NULL,NULL,_binary '\0'),(151,'39','QA Approval Activity',21,'QA Approvals','User/AS','8630c3fc2b1e936a3cb331edb1ad335d.png',NULL,NULL,'QA Approvals Activity','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-10 13:59:04',_binary ''),(152,'1001','Create Plant ',15,'Create Plant data','Admin/mstPlant','908d2d5b2f19cc5b694b94687ff4e29a.png',NULL,NULL,'Create plant data','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(153,'1004.8','Create Solution ',15,'Create Plant data','Admin/AddSolution','fe8d14400db960d23d216136bc3c9caf.png',NULL,NULL,'Testing','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-10 16:47:00',_binary ''),(247,'1002.5','Create Sub-Block ',15,'Create Sub-Block data','Admin/mstDepartment','919e0e83e4a2fb5d37bbd8b936daf838.png',NULL,NULL,'Post Loaded','A-000','Superadmin','2020-01-15 05:23:15',NULL,NULL,_binary ''),(265,'37','Reports',17,'Reports','','d37a87bbe5a683fd7c1b8a0a86bc8e99.png',NULL,NULL,'First time plant set up','A-000','Superadmin','2020-01-15 05:23:15','Superadmin',NULL,_binary ''),(269,'1','Operation',14,'Production','','d23c2919d26a15f67aecc997ed530d80.png',NULL,NULL,'First time plant set up','A-000','Superadmin','2020-01-15 05:23:15','Superadmin',NULL,_binary ''),(270,'2222','Type-AA',14,'Production','test','61f16e92dd1765ed93d856c0d9b3cecd.png',NULL,NULL,'First time plant set up test','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2020-03-13 08:17:30',_binary ''),(271,'3','Type-B',14,'Production','','286bd3f88d1ee62c1eded74cce670299.png',NULL,NULL,'First time plant set up','A-000','Superadmin','2020-01-15 05:23:15','Superadmin',NULL,_binary ''),(272,'4','In-process container cleaning',14,'Production','','1e667541028aaf93884c2e5c45aef9f3.png',NULL,NULL,'First time plant set up','A-000','Superadmin','2020-01-15 05:23:15','Superadmin',NULL,_binary ''),(273,'5','Portable Equipment Cleaning',14,'Production','','781d3b0796eb3a6fb9f39ed048697d41.png',NULL,NULL,'First time plant set up','A-000','Superadmin','2020-01-15 05:23:15','Superadmin',NULL,_binary ''),(274,'26','Accessory Cleaning',14,'Production','','722f5f4f9e31c7fe3144124e3e2678f3.png',NULL,NULL,'First time plant set up','A-000','Superadmin','2020-01-15 05:23:15','Superadmin',NULL,_binary ''),(275,'27','Solution Preparation',20,'Sanitization','solutionPreparationview','8511c88168a76868441ddad90ac9a320.png',NULL,NULL,'First time plant set up','A-000','Superadmin','2020-01-15 05:23:15','Superadmin',NULL,_binary ''),(276,'29','Drain Points Cleaning',20,'Sanitization','drainPointview','9a7637245bf0d6826e1f6bf64b4bdf7d.png',NULL,NULL,'First time plant set up','A-000','Superadmin','2020-01-15 05:23:15','Superadmin',NULL,_binary ''),(277,'30','Daily Cleaning',20,'Sanitization','dailyCleaningview','970eade053d5f7435765eb46f097f1c2.png',NULL,NULL,'First time plant set up','A-000','Superadmin','2020-01-15 05:23:15','Superadmin',NULL,_binary ''),(278,'1005','Create Product',15,'Create Plant data','Admin/AddProduct','fe8d14400db960d23d216136bc3c9caf.png',NULL,NULL,'Testing','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-10-10 16:47:00',_binary ''),(574,'38','Approvals',18,'Approvals','No function','d37a87bbe5a683fd7c1b8a0a86bc8e99.png',NULL,NULL,'First time plant set up testing','A-000','Superadmin','2020-01-15 05:23:15','Superadmin','2019-11-10 15:20:53',_binary ''),(2557,'1','Operation',14,'Production','','d23c2919d26a15f67aecc997ed530d80.png',NULL,NULL,'First time plant set up','A-001','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2558,'1','Operation',14,'Production','','d23c2919d26a15f67aecc997ed530d80.png',NULL,NULL,'First time plant set up','A-002','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2559,'1','Operation',14,'Production','','d23c2919d26a15f67aecc997ed530d80.png',NULL,NULL,'First time plant set up','A-003','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2560,'1','Operation',14,'Production','','d23c2919d26a15f67aecc997ed530d80.png',NULL,NULL,'First time plant set up','A-004','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2561,'1','Operation',14,'Production','','d23c2919d26a15f67aecc997ed530d80.png',NULL,NULL,'First time plant set up','A-111','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2562,'1','Operation',14,'Production','','d23c2919d26a15f67aecc997ed530d80.png',NULL,NULL,'First time plant set up','A-112','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2563,'1','Operation',14,'Production','','d23c2919d26a15f67aecc997ed530d80.png',NULL,NULL,'First time plant set up','A-113','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2564,'1','Operation',14,'Production','','d23c2919d26a15f67aecc997ed530d80.png',NULL,NULL,'First time plant set up','A-114','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2565,'1','Operation',14,'Production','','d23c2919d26a15f67aecc997ed530d80.png',NULL,NULL,'First time plant set up','A-115','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2566,'1','Operation',14,'Production','','d23c2919d26a15f67aecc997ed530d80.png',NULL,NULL,'First time plant set up','A-116','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2567,'1','Operation',14,'Production','','d23c2919d26a15f67aecc997ed530d80.png',NULL,NULL,'First time plant set up','A-117','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2568,'1','Operation',14,'Production','','d23c2919d26a15f67aecc997ed530d80.png',NULL,NULL,'First time plant set up','A-118','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2569,'2','Type-A',14,'Production','','61f16e92dd1765ed93d856c0d9b3cecd.png',NULL,NULL,'First time plant set up','A-001','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2570,'2','Type-A',14,'Production','','61f16e92dd1765ed93d856c0d9b3cecd.png',NULL,NULL,'First time plant set up','A-002','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2571,'2','Type-A',14,'Production','','61f16e92dd1765ed93d856c0d9b3cecd.png',NULL,NULL,'First time plant set up','A-003','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2572,'2','Type-A',14,'Production','','61f16e92dd1765ed93d856c0d9b3cecd.png',NULL,NULL,'First time plant set up','A-004','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2573,'2','Type-A',14,'Production','','61f16e92dd1765ed93d856c0d9b3cecd.png',NULL,NULL,'First time plant set up','A-111','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2574,'2','Type-A',14,'Production','','61f16e92dd1765ed93d856c0d9b3cecd.png',NULL,NULL,'First time plant set up','A-112','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2575,'2','Type-A',14,'Production','','61f16e92dd1765ed93d856c0d9b3cecd.png',NULL,NULL,'First time plant set up','A-113','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2576,'2','Type-A',14,'Production','','61f16e92dd1765ed93d856c0d9b3cecd.png',NULL,NULL,'First time plant set up','A-114','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2577,'2','Type-A',14,'Production','','61f16e92dd1765ed93d856c0d9b3cecd.png',NULL,NULL,'First time plant set up','A-115','Superadmin','2020-01-15 06:12:06','Superadmin',NULL,_binary ''),(2578,'2','Type-A',14,'Production','','61f16e92dd1765ed93d856c0d9b3cecd.png',NULL,NULL,'First time plant set up','A-116','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2579,'2','Type-A',14,'Production','','61f16e92dd1765ed93d856c0d9b3cecd.png',NULL,NULL,'First time plant set up','A-117','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2580,'2','Type-A',14,'Production','','61f16e92dd1765ed93d856c0d9b3cecd.png',NULL,NULL,'First time plant set up','A-118','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2581,'26','Accessory Cleaning',14,'Production','','722f5f4f9e31c7fe3144124e3e2678f3.png',NULL,NULL,'First time plant set up','A-001','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2582,'26','Accessory Cleaning',14,'Production','','722f5f4f9e31c7fe3144124e3e2678f3.png',NULL,NULL,'First time plant set up','A-002','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2583,'26','Accessory Cleaning',14,'Production','','722f5f4f9e31c7fe3144124e3e2678f3.png',NULL,NULL,'First time plant set up','A-003','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2584,'26','Accessory Cleaning',14,'Production','','722f5f4f9e31c7fe3144124e3e2678f3.png',NULL,NULL,'First time plant set up','A-004','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2585,'26','Accessory Cleaning',14,'Production','','722f5f4f9e31c7fe3144124e3e2678f3.png',NULL,NULL,'First time plant set up','A-111','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2586,'26','Accessory Cleaning',14,'Production','','722f5f4f9e31c7fe3144124e3e2678f3.png',NULL,NULL,'First time plant set up','A-112','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2587,'26','Accessory Cleaning',14,'Production','','722f5f4f9e31c7fe3144124e3e2678f3.png',NULL,NULL,'First time plant set up','A-113','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2588,'26','Accessory Cleaning',14,'Production','','722f5f4f9e31c7fe3144124e3e2678f3.png',NULL,NULL,'First time plant set up','A-114','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2589,'26','Accessory Cleaning',14,'Production','','722f5f4f9e31c7fe3144124e3e2678f3.png',NULL,NULL,'First time plant set up','A-115','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2590,'26','Accessory Cleaning',14,'Production','','722f5f4f9e31c7fe3144124e3e2678f3.png',NULL,NULL,'First time plant set up','A-116','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2591,'26','Accessory Cleaning',14,'Production','','722f5f4f9e31c7fe3144124e3e2678f3.png',NULL,NULL,'First time plant set up','A-117','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2592,'26','Accessory Cleaning',14,'Production','','722f5f4f9e31c7fe3144124e3e2678f3.png',NULL,NULL,'First time plant set up','A-118','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2593,'27','Solution Preparation',20,'Sanitization','solutionPreparationview','8511c88168a76868441ddad90ac9a320.png',NULL,NULL,'First time plant set up','A-001','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2594,'27','Solution Preparation',20,'Sanitization','solutionPreparationview','8511c88168a76868441ddad90ac9a320.png',NULL,NULL,'First time plant set up','A-002','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2595,'29','Drain Points Cleaning',20,'Sanitization','drainPointview','9a7637245bf0d6826e1f6bf64b4bdf7d.png',NULL,NULL,'First time plant set up','A-001','Superadmin','2020-01-15 06:12:07','Superadmin',NULL,_binary ''),(2596,'29','Drain Points Cleaning',20,'Sanitization','drainPointview','9a7637245bf0d6826e1f6bf64b4bdf7d.png',NULL,NULL,'First time plant set up','A-002','Superadmin','2020-01-15 06:12:08','Superadmin',NULL,_binary ''),(2597,'3','Type-B',14,'Production','','286bd3f88d1ee62c1eded74cce670299.png',NULL,NULL,'First time plant set up','A-001','Superadmin','2020-01-15 06:12:08','Superadmin',NULL,_binary ''),(2598,'3','Type-B',14,'Production','','286bd3f88d1ee62c1eded74cce670299.png',NULL,NULL,'First time plant set up','A-002','Superadmin','2020-01-15 06:12:08','Superadmin',NULL,_binary ''),(2599,'30','Daily Cleaning',20,'Sanitization','dailyCleaningview','970eade053d5f7435765eb46f097f1c2.png',NULL,NULL,'First time plant set up','A-001','Superadmin','2020-01-15 06:12:08','Superadmin',NULL,_binary ''),(2600,'30','Daily Cleaning',20,'Sanitization','dailyCleaningview','970eade053d5f7435765eb46f097f1c2.png',NULL,NULL,'First time plant set up','A-002','Superadmin','2020-01-15 06:12:08','Superadmin',NULL,_binary ''),(2601,'37','Reports',17,'Reports','','d37a87bbe5a683fd7c1b8a0a86bc8e99.png',NULL,NULL,'First time plant set up','A-001','Superadmin','2020-01-15 06:12:08','Superadmin',NULL,_binary ''),(2602,'37','Reports',17,'Reports','','d37a87bbe5a683fd7c1b8a0a86bc8e99.png',NULL,NULL,'First time plant set up','A-002','Superadmin','2020-01-15 06:12:08','Superadmin',NULL,_binary ''),(2603,'38','Approvals',18,'Approvals','No function','d37a87bbe5a683fd7c1b8a0a86bc8e99.png',NULL,NULL,'First time plant set up testing','A-001','Superadmin','2020-01-15 06:12:08','Superadmin',NULL,_binary ''),(2604,'38','Approvals',18,'Approvals','No function','d37a87bbe5a683fd7c1b8a0a86bc8e99.png',NULL,NULL,'First time plant set up testing','A-002','Superadmin','2020-01-15 06:12:08','Superadmin',NULL,_binary ''),(2605,'4','In-process container cleaning',14,'Production','','1e667541028aaf93884c2e5c45aef9f3.png',NULL,NULL,'First time plant set up','A-001','Superadmin','2020-01-15 06:12:08','Superadmin',NULL,_binary ''),(2606,'4','In-process container cleaning',14,'Production','','1e667541028aaf93884c2e5c45aef9f3.png',NULL,NULL,'First time plant set up','A-002','Superadmin','2020-01-15 06:12:08','Superadmin',NULL,_binary ''),(2607,'5','Portable Equipment Cleaning',14,'Production','','781d3b0796eb3a6fb9f39ed048697d41.png',NULL,NULL,'First time plant set up','A-001','Superadmin','2020-01-15 06:12:08','Superadmin',NULL,_binary ''),(2608,'5','Portable Equipment Cleaning',14,'Production','','781d3b0796eb3a6fb9f39ed048697d41.png',NULL,NULL,'First time plant set up','A-002','Superadmin','2020-01-15 06:12:08','Superadmin',NULL,_binary ''),(2609,'act0099','ActName0099',14,'Production','test','c269ab713ff64808bc05af9c6651ced6.png',NULL,NULL,'remark test','A-000','Superadmin','2020-03-13 12:59:30',NULL,NULL,_binary '');
/*!40000 ALTER TABLE `mst_activity` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_activityactivity` AFTER UPDATE ON `mst_activity` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_activity',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.activity_code != NEW.activity_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_code', OLD.activity_code, NEW.activity_code);
	  END IF;
	  
	  IF(OLD.activity_name != NEW.activity_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_name', OLD.activity_name, NEW.activity_name);
	  END IF;
	  
	  IF(OLD.activitytype_id != NEW.activitytype_id)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activitytype_id', OLD.activitytype_id, NEW.activitytype_id);
	  END IF;
	  
	 IF(OLD.activity_type != NEW.activity_type)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_type', OLD.activity_type, NEW.activity_type);
	  END IF; 
	  
	  IF(OLD.activityfunc_tocall != NEW.activityfunc_tocall)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activityfunc_tocall', OLD.activityfunc_tocall, NEW.activityfunc_tocall);
	  END IF;
	  IF(OLD.activity_icon != NEW.activity_icon)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_icon', OLD.activity_icon, NEW.activity_icon);
	  END IF;
	  IF(OLD.activity_nature != NEW.activity_nature)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_nature', OLD.activity_nature, NEW.activity_nature);
	  END IF;
	  IF(OLD.activity_duration != NEW.activity_duration)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_duration', OLD.activity_duration, NEW.activity_duration);
	  END IF;
             
          IF(OLD.activity_remark != NEW.activity_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_remark', OLD.activity_remark, NEW.activity_remark);
	  END IF;
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_code', OLD.area_code, NEW.area_code);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_activity', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_activitytype`
--

DROP TABLE IF EXISTS `mst_activitytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_activitytype` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `activitytype_name` varchar(500) DEFAULT NULL,
  `activitytype_icon` varchar(500) DEFAULT NULL,
  `activityfunction_tocall` varchar(500) DEFAULT NULL,
  `activity_remarks` text,
  `activity_blockcolor` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(250) DEFAULT NULL,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='Activity Type Master Table';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_activitytype`
--

LOCK TABLES `mst_activitytype` WRITE;
/*!40000 ALTER TABLE `mst_activitytype` DISABLE KEYS */;
INSERT INTO `mst_activitytype` VALUES (14,'Production','aff22bbcb7c3b4edfe07c87d577cb01d.png','Production/area','First time plant set up','#ff6f61','2020-01-15 05:23:16','Superadmin',NULL,NULL,_binary ''),(15,'First time Plant Set-up','7150947c9217d597e9a8c41c201407b8.png','Admin/Activity/15','First time plant set up','#ff6f61','2020-01-15 05:23:16','Superadmin',NULL,NULL,_binary ''),(16,'Create Master Data','0d140305421992f5859a9a51de63e750.png','Admin/Activity/16','Testing again','#ff6f61','2020-01-15 05:23:16','Superadmin',NULL,NULL,_binary ''),(17,'Reports','d5ba01aff184809802df09d3b4594e91.png','Report/Reports','First time plant set up','#ff6f61','2020-01-15 05:23:16','Superadmin',NULL,NULL,_binary ''),(18,'Approvals','d1af133d417cbfbc1c04665d33ae4e15.png','User/AS','First time plant set up','#88b04b','2020-01-15 05:23:16','Superadmin',NULL,NULL,_binary ''),(20,'Sanitization','3e4c5dadd4a2172cd759bc508c864b68.png','Sanitization/area','First time plant set up','#88b04b','2020-01-15 05:23:16','Superadmin',NULL,NULL,_binary ''),(21,'QA Approvals','38bdb0a3a3f3fd33190ed28e6460f1d2.png','User/AS','QA Approvals','#ff6f61','2020-01-15 05:23:16','Superadmin','Superadmin','2019-10-11 06:50:31',_binary ''),(22,'Aseptic','a6338a29acb1179868af3b1dfa11c0fb.png','injectable/activities',' Injectibles set up','#ff6f61','2020-01-15 05:23:16','Superadmin',NULL,NULL,_binary ''),(23,'Clean Area','b613e685eb3d2202f002ae31d34ce400.png','injectable/activities','injectibles set up','#ff6f61','2020-01-15 05:23:16','Superadmin',NULL,NULL,_binary ''),(24,'Grade D','2b10e1060b0888c76fb7e2e6c8caccb2.png','injectable/activities','injectibles set up','#ff6f61','2020-01-15 05:23:16','Superadmin',NULL,NULL,_binary ''),(25,'Non-Classified','c5eed8274e9c230f6805a56801f13e2d.png','injectable/activities',' Injectibles set up','#ff6f61','2020-01-15 05:23:16','Superadmin',NULL,NULL,_binary '');
/*!40000 ALTER TABLE `mst_activitytype` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_activitytypeactivity` AFTER UPDATE ON `mst_activitytype` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_activitytype',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.activitytype_name != NEW.activitytype_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activitytype_name', OLD.activitytype_name, NEW.activitytype_name);
	  END IF;
	  IF(OLD.activitytype_icon != NEW.activitytype_icon)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activitytype_icon', OLD.activitytype_icon, NEW.activitytype_icon);
	  END IF;
	  
	  IF(OLD.activityfunction_tocall != NEW.activityfunction_tocall)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activityfunction_tocall', OLD.activityfunction_tocall, NEW.activityfunction_tocall);
	  END IF;
	  IF(OLD.activity_remarks != NEW.activity_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_remarks', OLD.activity_remarks, NEW.activity_remarks);
	  END IF;
	  IF(OLD.activity_blockcolor != NEW.activity_blockcolor)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_blockcolor', OLD.activity_blockcolor, NEW.activity_blockcolor);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_activitytype', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_area`
--

DROP TABLE IF EXISTS `mst_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area_code` varchar(50) DEFAULT NULL,
  `area_name` varchar(250) DEFAULT NULL,
  `area_address` varchar(4000) DEFAULT NULL,
  `area_remarks` varchar(2000) DEFAULT NULL,
  `area_contact` varchar(20) DEFAULT NULL,
  `area_sop_no` varchar(20) NOT NULL,
  `block_code` varchar(50) DEFAULT NULL,
  `department_code` varchar(200) NOT NULL,
  `number_of_rooms` int(11) NOT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_area`
--

LOCK TABLES `mst_area` WRITE;
/*!40000 ALTER TABLE `mst_area` DISABLE KEYS */;
INSERT INTO `mst_area` VALUES (1,'WC0001','Wurster Coating Area 1',NULL,'','','SOP/Area/001','PBG','PBG',1,_binary '','Superadmin','2020-04-07 06:48:25',NULL,NULL),(2,'PR0001','Packaging Area 1',NULL,'Added Number of Rooms','','SOP/Area/001','PBG','PBG',3,_binary '','Superadmin','2020-04-07 06:48:47','Superadmin','2020-04-07 15:56:56'),(3,'DR0001','Dispensing Area 1',NULL,'','','SOP/Area/001','PBG','PBG',1,_binary '','Superadmin','2020-04-07 06:49:14',NULL,NULL),(4,'IPQA','IPQA PBG Area',NULL,'','','SOP/Area/001','PBG','PBG',1,_binary '','Superadmin','2020-04-07 06:49:39',NULL,NULL);
/*!40000 ALTER TABLE `mst_area` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_areaactivity` AFTER UPDATE ON `mst_area` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_area',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_code', OLD.area_code, NEW.area_code);
	  END IF;
	  
	  IF(OLD.area_name != NEW.area_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_name', OLD.area_name, NEW.area_name);
	  END IF;
	  
	  IF(OLD.area_address != NEW.area_address)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_address', OLD.area_address, NEW.area_address);
	  END IF;
	  
	 IF(OLD.area_contact != NEW.area_contact)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_contact', OLD.area_contact, NEW.area_contact);
	  END IF; 
	  
	  
	  IF(OLD.area_remarks != NEW.area_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_remarks', OLD.area_remarks, NEW.area_remarks);
	  END IF; 
	  
	  IF(OLD.area_sop_no != NEW.area_sop_no)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_sop_no', OLD.area_sop_no, NEW.area_sop_no);
	  END IF;
	  IF(OLD.number_of_rooms != NEW.number_of_rooms)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'number_of_rooms', OLD.number_of_rooms, NEW.number_of_rooms);
	  END IF;
	  IF(OLD.block_code != NEW.block_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_code', OLD.block_code, NEW.block_code);
	  END IF; 
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_area', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_block`
--

DROP TABLE IF EXISTS `mst_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_block` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `block_code` varchar(50) DEFAULT NULL,
  `block_name` varchar(2000) DEFAULT NULL,
  `block_contact` varchar(20) DEFAULT NULL,
  `block_remarks` varchar(2000) DEFAULT NULL,
  `number_of_areas` int(11) DEFAULT NULL,
  `plant_code` varchar(50) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `block_code` (`block_code`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_block`
--

LOCK TABLES `mst_block` WRITE;
/*!40000 ALTER TABLE `mst_block` DISABLE KEYS */;
INSERT INTO `mst_block` VALUES (1,'PBG','Parenteral Block G','','',1,'PL001',1,'Superadmin','2020-04-07 06:47:06',NULL,NULL);
/*!40000 ALTER TABLE `mst_block` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_blockactivity` AFTER UPDATE ON `mst_block` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_block',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	 IF(OLD.block_code != NEW.block_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_code', OLD.block_code, NEW.block_code);
	  END IF;
	  
	  IF(OLD.block_name != NEW.block_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_name', OLD.block_name, NEW.block_name);
	  END IF; 
	  
	 IF(OLD.block_contact != NEW.block_contact)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_contact', OLD.block_contact, NEW.block_contact);
	  END IF; 
	  
	  
	  IF(OLD.block_remarks != NEW.block_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_remarks', OLD.block_remarks, NEW.block_remarks);
	  END IF; 
	  
	  
	  IF(OLD.number_of_areas != NEW.number_of_areas)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'number_of_areas', OLD.number_of_areas, NEW.number_of_areas);
	  END IF;
	  IF(OLD.plant_code != NEW.plant_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_code', OLD.plant_code, NEW.plant_code);
	  END IF; 
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_block', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_department`
--

DROP TABLE IF EXISTS `mst_department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_department` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `department_code` varchar(50) DEFAULT NULL,
  `department_name` varchar(500) DEFAULT NULL,
  `department_address` varchar(2000) DEFAULT NULL,
  `department_contact` varchar(20) DEFAULT NULL,
  `department_remark` varchar(2000) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `block_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_department`
--

LOCK TABLES `mst_department` WRITE;
/*!40000 ALTER TABLE `mst_department` DISABLE KEYS */;
INSERT INTO `mst_department` VALUES (1,'PBG','Parenteral Block G',NULL,'','','WC0001','PBG','Superadmin','2020-04-07 06:48:00',NULL,NULL,_binary ''),(2,'PBG','Parenteral Block G',NULL,'','','PR0001','PBG','Superadmin','2020-04-07 06:48:47',NULL,NULL,_binary ''),(3,'PBG','Parenteral Block G',NULL,'','','DR0001','PBG','Superadmin','2020-04-07 06:49:14',NULL,NULL,_binary ''),(4,'PBG','Parenteral Block G',NULL,'','','IPQA','PBG','Superadmin','2020-04-07 06:49:39',NULL,NULL,_binary '');
/*!40000 ALTER TABLE `mst_department` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_departmentactivity` AFTER UPDATE ON `mst_department` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_department',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	 IF(OLD.department_code != NEW.department_code)
	  THEN
	  
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'department_code', OLD.department_code, NEW.department_code);
	  END IF;
	  IF(OLD.department_name != NEW.department_name)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'department_name', OLD.department_name, NEW.department_name);
	  END IF; 
	  IF(OLD.department_address != NEW.department_address)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'department_address', OLD.department_address, NEW.department_address);
	  END IF; 
	  IF(OLD.department_contact != NEW.department_contact)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'department_contact', OLD.department_contact, NEW.department_contact);
	  END IF; 
	  IF(OLD.department_remark != NEW.department_remark)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'department_remark', OLD.department_remark, NEW.department_remark);
	  END IF; 
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_code', OLD.area_code, NEW.area_code);
	  END IF; 
	  IF(OLD.block_code != NEW.block_code)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'block_code', OLD.block_code, NEW.block_code);
	  END IF; 
	  END IF;
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_department', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_document`
--

DROP TABLE IF EXISTS `mst_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_document` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `doc_id` varchar(50) DEFAULT NULL,
  `remarks` varchar(2000) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_document`
--

LOCK TABLES `mst_document` WRITE;
/*!40000 ALTER TABLE `mst_document` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_document` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_documentactivity` AFTER UPDATE ON `mst_document` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_document',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	 IF(OLD.doc_id != NEW.doc_id)
	  THEN
	  
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'doc_id', OLD.doc_id, NEW.doc_id);
	  END IF;
	  IF(OLD.remarks != NEW.remarks)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'remarks', OLD.remarks, NEW.remarks);
	  END IF; 
	  END IF;
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_document', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_drainpoint`
--

DROP TABLE IF EXISTS `mst_drainpoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_drainpoint` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `drainpoint_code` varchar(50) DEFAULT NULL,
  `drainpoint_name` varchar(500) DEFAULT NULL,
  `drainpoint_remark` varchar(2000) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `solution_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_drainpoint`
--

LOCK TABLES `mst_drainpoint` WRITE;
/*!40000 ALTER TABLE `mst_drainpoint` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_drainpoint` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_drainpointactivity` AFTER UPDATE ON `mst_drainpoint` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_drainpoint',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.drainpoint_code != NEW.drainpoint_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'drainpoint_code', OLD.drainpoint_code, NEW.drainpoint_code);
	  END IF;
	  
	  IF(OLD.drainpoint_name != NEW.drainpoint_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'drainpoint_name', OLD.drainpoint_name, NEW.drainpoint_name);
	  END IF;
	  
	  IF(OLD.drainpoint_remark != NEW.drainpoint_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'drainpoint_remark', OLD.drainpoint_remark, NEW.drainpoint_remark);
	  END IF; 
	  
	  
	  IF(OLD.room_code != NEW.room_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_code', OLD.room_code, NEW.room_code);
	  END IF; 
          
	  IF(OLD.solution_code != NEW.solution_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'solution_code', OLD.solution_code, NEW.solution_code);
	  END IF;	 
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_drainpoint', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_employee`
--

DROP TABLE IF EXISTS `mst_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_employee` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `emp_code` varchar(50) DEFAULT NULL,
  `emp_name` varchar(250) DEFAULT NULL,
  `email2` varchar(200) DEFAULT NULL,
  `emp_address` varchar(2000) DEFAULT NULL,
  `emp_contact` bigint(20) DEFAULT NULL,
  `emp_email` varchar(500) DEFAULT NULL,
  `emp_password` varchar(500) DEFAULT NULL,
  `designation_code` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `role_code` varchar(50) DEFAULT NULL,
  `block_code` varchar(2000) DEFAULT NULL,
  `Sub_block_code` varchar(2000) DEFAULT NULL,
  `area_code` varchar(2000) DEFAULT NULL,
  `emp_remark` varchar(2000) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `is_blocked` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `emp_code` (`emp_code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_employee`
--

LOCK TABLES `mst_employee` WRITE;
/*!40000 ALTER TABLE `mst_employee` DISABLE KEYS */;
INSERT INTO `mst_employee` VALUES (1,'Superadmin','Super Admin','sa@sunpharma.in',NULL,9,'sa','329a424d6a0bb26caaea10cc033b64b4c1ab45a6874196554cc16d705949e033','superadmin',6,'Role6','Block-000',NULL,'A-033','This is pre-loaded data','Admin','2020-01-15 05:23:22',NULL,NULL,_binary '',_binary '\0'),(2,'0002','Prashant','prashant@smhs.motherson.com','',0,'prashant','af24ad3aad1be6e5e3edcd122145eb2f68670ba8ddd079237541dbdb26ce2c37','PM',1,NULL,'PBG','PBG','IPQA,DR0001,PR0001,WC0001','','Superadmin','2020-04-06 09:22:18',NULL,NULL,_binary '',_binary '\0'),(3,'0003','Anurag','prashant@smhs.motherson.com','',0,'anurag','af24ad3aad1be6e5e3edcd122145eb2f68670ba8ddd079237541dbdb26ce2c37','PM',2,NULL,'PBG','PBG','IPQA,DR0001,PR0001,WC0001','','Superadmin','2020-04-06 09:22:50','Superadmin','2020-04-06 14:53:12',_binary '',_binary '\0'),(4,'0004','Poonam','prashant@smhs.motherson.com','',0,'Poonam','af24ad3aad1be6e5e3edcd122145eb2f68670ba8ddd079237541dbdb26ce2c37','PM',1,NULL,'PBG','PBG','IPQA,DR0001,PR0001,WC0001','','Superadmin','2020-04-06 09:23:47','Superadmin','2020-04-07 13:02:45',_binary '',_binary '\0'),(5,'0005','aashwani','prashant@smhs.motherson.com','',0,'aashwani','af24ad3aad1be6e5e3edcd122145eb2f68670ba8ddd079237541dbdb26ce2c37','PM',2,NULL,'PBG','PBG','IPQA,DR0001,PR0001,WC0001','','Superadmin','2020-04-06 09:24:30',NULL,NULL,_binary '',_binary '\0'),(6,'0006','Jaweed','','',0,'Jaweed','af24ad3aad1be6e5e3edcd122145eb2f68670ba8ddd079237541dbdb26ce2c37','PM',5,NULL,'PBG','PBG','IPQA,DR0001,PR0001,WC0001','','Superadmin','2020-04-06 09:25:08',NULL,NULL,_binary '',_binary '\0'),(7,'EMMP001','EMMPName2','emp@gmail.com','Delhi',9988744562,'Empname','da6a6c2e07eea34ac458cf419358cdcb379baa51aed5e02715374523bd0826ef','test2',1,NULL,'PBG','PBG','tesst,test,test,tesar1,tesar1,Area123,testarea1,test003,IPQA,DR0001,PR0001,WC0001','ok','Superadmin','2020-04-06 14:22:26','Superadmin','2020-04-06 19:53:40',_binary '\0',_binary '\0');
/*!40000 ALTER TABLE `mst_employee` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `emp_insert_audit_log` AFTER INSERT ON `mst_employee` FOR EACH ROW BEGIN
INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','Employee','Insert',NEW.created_on, NEW.created_by, 'N/A', 'N/A');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `emp_audit_log` AFTER UPDATE ON `mst_employee` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  IF(OLD.emp_code != NEW.emp_code)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','emp_code','update',NEW.created_on, NEW.created_by, OLD.emp_code, NEW.emp_code);
	  END IF;
	  
	  IF(OLD.emp_name != NEW.emp_name)
	  THEN	
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','emp_name','update',NEW.created_on, NEW.created_by, OLD.emp_name, NEW.emp_name);
	  END IF;
	  
	  IF(OLD.email2 != NEW.email2)
	  THEN	 
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','email2','update',NEW.created_on, NEW.created_by, OLD.email2, NEW.email2);
	  END IF;
	  
	  IF(OLD.emp_address != NEW.emp_address)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','emp_address','update',NEW.created_on, NEW.created_by, OLD.emp_address, NEW.emp_address);
	  END IF; 
	  
	  
	  IF(OLD.emp_contact != NEW.emp_contact)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','emp_contact','update',NEW.created_on, NEW.created_by, OLD.emp_contact, NEW.emp_contact);
	  END IF; 
	  
	  IF(OLD.emp_email != NEW.emp_email)
	  THEN	 
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','emp_email','update',NEW.created_on, NEW.created_by, OLD.emp_email, NEW.emp_email);
	  END IF;
	  
	  IF(OLD.emp_password != NEW.emp_password)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','emp_password','update',NEW.created_on, NEW.created_by, OLD.emp_password, NEW.emp_password);	 
	  END IF;
	  
	  IF(OLD.designation_code != NEW.designation_code)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','designation_code','update',NEW.created_on, NEW.created_by, OLD.designation_code, NEW.designation_code);	 	 
	  END IF; 
	  
	  IF(OLD.role_id != NEW.role_id)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','role_id','update',NEW.created_on, NEW.created_by, OLD.role_id, NEW.role_id);	 	 
	  END IF; 
	  
	  IF(OLD.role_code != NEW.role_code)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','role_code','update',NEW.created_on, NEW.created_by, OLD.role_code, NEW.role_code);	
	  END IF; 
	  
	  IF(OLD.block_code != NEW.block_code)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','block_code','update',NEW.created_on, NEW.created_by, OLD.block_code, NEW.block_code);
	  END IF; 
	  
	  IF(OLD.Sub_block_code != NEW.Sub_block_code)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','Sub_block_code','update',NEW.created_on, NEW.created_by, OLD.Sub_block_code, NEW.Sub_block_code);
	  END IF;
	  
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','area_code','update',NEW.created_on, NEW.created_by, OLD.area_code, NEW.area_code);
	  END IF; 
	  
	  IF(OLD.emp_remark != NEW.emp_remark)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','emp_remark','update',NEW.created_on, NEW.created_by, OLD.emp_remark, NEW.emp_remark);
	  END IF; 
	  
	  IF(OLD.is_blocked != NEW.is_blocked)
	  THEN
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','is_blocked','update',NEW.created_on, NEW.created_by, OLD.is_blocked, NEW.is_blocked);	  
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	
	  INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Employee Master','is_active','update',NEW.created_on, NEW.created_by, OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_equipment`
--

DROP TABLE IF EXISTS `mst_equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_equipment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `equipment_code` varchar(50) DEFAULT NULL,
  `equipment_name` varchar(250) DEFAULT NULL,
  `equipment_icon` varchar(500) DEFAULT NULL,
  `equipment_type` varchar(500) DEFAULT NULL,
  `equipment_remarks` varchar(2000) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `block_code` varchar(50) DEFAULT NULL,
  `drain_point_code` varchar(50) DEFAULT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_inused` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `equipment_code` (`equipment_code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_equipment`
--

LOCK TABLES `mst_equipment` WRITE;
/*!40000 ALTER TABLE `mst_equipment` DISABLE KEYS */;
INSERT INTO `mst_equipment` VALUES (1,'EQWC0001','Wurster Coating - 1','default.png','Fixed','','WC0001','PBG','','',_binary '','Superadmin','2020-04-07 07:25:00',NULL,NULL,_binary '\0'),(2,'EQWC0002','Wurster Coating - 2','default.png','Fixed','','WC0001','PBG','','',_binary '','Superadmin','2020-04-07 07:25:33',NULL,NULL,_binary '\0'),(3,'EQSP0001','Solution Preparation Tank 1','default.png','Portable','','','PBG','','',_binary '','Superadmin','2020-04-07 07:26:17',NULL,NULL,_binary '\0'),(4,'EQVC0001','Vacuum Cleaner - 1','default.png','Vaccum','','','PBG','','',_binary '','Superadmin','2020-04-07 07:26:45',NULL,NULL,_binary '\0'),(5,'EQPL0001','EQ 1 Packaging Line 1','default.png','Fixed','','PR0001','PBG','','',_binary '','Superadmin','2020-04-07 07:28:02',NULL,NULL,_binary '\0'),(6,'EQPL0002','EQ 2 Packaging Line 1','default.png','Fixed','','PR0001','PBG','','',_binary '','Superadmin','2020-04-07 07:29:14',NULL,NULL,_binary '\0'),(7,'EQPL0003','EQ 3 Packaging Line 1','default.png','Fixed','','PR0001','PBG','','',_binary '','Superadmin','2020-04-07 07:29:47',NULL,NULL,_binary '\0');
/*!40000 ALTER TABLE `mst_equipment` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_equipmentactivity` AFTER UPDATE ON `mst_equipment` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_equipment',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.equipment_code != NEW.equipment_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'equipment_code', OLD.equipment_code, NEW.equipment_code);
	  END IF;
	  
	  IF(OLD.equipment_name != NEW.equipment_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'equipment_name', OLD.equipment_name, NEW.equipment_name);
	  END IF;
	  
	  IF(OLD.equipment_icon != NEW.equipment_icon)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'equipment_icon', OLD.equipment_icon, NEW.equipment_icon);
	  END IF;
	  
	 IF(OLD.equipment_type != NEW.equipment_type)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'equipment_type', OLD.equipment_type, NEW.equipment_type);
	  END IF; 
	  
	  
	  IF(OLD.equipment_remarks != NEW.equipment_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'equipment_remarks', OLD.equipment_remarks, NEW.equipment_remarks);
	  END IF; 
	  
	  
	  IF(OLD.room_code != NEW.room_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_code', OLD.room_code, NEW.room_code);
	  END IF; 
          
	  IF(OLD.drain_point_code != NEW.drain_point_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'drain_point_code', OLD.drain_point_code, NEW.drain_point_code);
	  END IF; 
	  
	  IF(OLD.sop_code != NEW.sop_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_code', OLD.sop_code, NEW.sop_code);
	  END IF; 
	  
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_equipment', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_equipment-old`
--

DROP TABLE IF EXISTS `mst_equipment-old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_equipment-old` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `equipment_code` varchar(50) DEFAULT NULL,
  `equipment_name` varchar(250) DEFAULT NULL,
  `equipment_icon` varchar(500) DEFAULT NULL,
  `equipment_type` varchar(500) DEFAULT NULL,
  `equipment_remarks` varchar(2000) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `drain_point_code` varchar(50) DEFAULT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_equipment-old`
--

LOCK TABLES `mst_equipment-old` WRITE;
/*!40000 ALTER TABLE `mst_equipment-old` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_equipment-old` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_holiday`
--

DROP TABLE IF EXISTS `mst_holiday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_holiday` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `holiday_code` varchar(50) DEFAULT NULL,
  `holiday_name` varchar(250) DEFAULT NULL,
  `holiday_date` datetime DEFAULT NULL,
  `holiday_type` varchar(500) DEFAULT NULL,
  `holiday_remark` varchar(2000) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_holiday`
--

LOCK TABLES `mst_holiday` WRITE;
/*!40000 ALTER TABLE `mst_holiday` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_holiday` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_holidayactivity` AFTER UPDATE ON `mst_holiday` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_holiday',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.holiday_code != NEW.holiday_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'holiday_code', OLD.holiday_code, NEW.holiday_code);
	  END IF;
	  
	  IF(OLD.holiday_name != NEW.holiday_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'holiday_name', OLD.holiday_name, NEW.holiday_name);
	  END IF;
	  
	  IF(OLD.holiday_date != NEW.holiday_date)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'holiday_date', OLD.holiday_date, NEW.holiday_date);
	  END IF;
	  
	 IF(OLD.holiday_type != NEW.holiday_type)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'holiday_type', OLD.holiday_type, NEW.holiday_type);
	  END IF; 
	  
	  
	  IF(OLD.holiday_remark != NEW.holiday_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'holiday_remark', OLD.holiday_remark, NEW.holiday_remark);
	  END IF; 
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_holiday', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_inj_cleaning`
--

DROP TABLE IF EXISTS `mst_inj_cleaning`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_inj_cleaning` (
  `id` int(20) NOT NULL,
  `description` varchar(300) NOT NULL,
  `type` varchar(100) NOT NULL,
  `seq` int(10) NOT NULL,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_inj_cleaning`
--

LOCK TABLES `mst_inj_cleaning` WRITE;
/*!40000 ALTER TABLE `mst_inj_cleaning` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_inj_cleaning` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_inj_solution`
--

DROP TABLE IF EXISTS `mst_inj_solution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_inj_solution` (
  `id` bigint(20) NOT NULL,
  `solution_code` varchar(50) DEFAULT NULL,
  `solution_name` varchar(500) DEFAULT NULL,
  `Block_Code` varchar(50) DEFAULT NULL,
  `Precautions` varchar(1000) DEFAULT NULL,
  `Short_Name` varchar(200) DEFAULT NULL,
  `sol_qty` varchar(200) DEFAULT NULL,
  `water_qty` varchar(200) DEFAULT NULL,
  `wfi_qs` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_inj_solution`
--

LOCK TABLES `mst_inj_solution` WRITE;
/*!40000 ALTER TABLE `mst_inj_solution` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_inj_solution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_messages`
--

DROP TABLE IF EXISTS `mst_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msg-header` varchar(50) NOT NULL,
  `message` varchar(500) DEFAULT NULL,
  `created_by` varchar(200) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_messages`
--

LOCK TABLES `mst_messages` WRITE;
/*!40000 ALTER TABLE `mst_messages` DISABLE KEYS */;
INSERT INTO `mst_messages` VALUES (1,'Start','Batch operation is started successfully.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(3,'Checked By','Batch operation is successfully taken over by you.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(4,'Stop','Batch operation is successfully completed.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(5,'Start','Type A cleaning is successfully started.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(6,'Checked By','Type-B Cleaning is successfully taken over by you.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(7,'Stop','Type-B cleaning is successfully completed.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(8,'Checked By','Type-A Cleaning is successfully taken over by you.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(9,'Stop','Type-A cleaning is successfully completed.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(10,'Start','Portable equipments cleaning is successfully started.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(11,'Stop','Portable equipments cleaning is successfully completed.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(12,'Checked By','Portable equipments cleaning is successfully taken over by you.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(13,'Start','In-process Container cleaning is successfully started.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(14,'Stop','In-process container cleaning is successfully completed.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(15,'Checked By','In-process container cleaning is successfully taken over by you.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(16,'Stop','Daily cleaning is successfully completed.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(17,'Start','Daily cleaning is successfully started.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(18,'Checked By','Daily cleaning is successfully taken oevr by you.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(19,'Submission','Solution Preparation form is successfully submitted.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(20,'Checked By','Solution Preparation is successfully taken over by you.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(21,'Destroy Solution','Remaining solution is successfully destroyed ',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(22,'Start','Drain points cleaning is successfully started.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(23,'Stop','Drain points cleaning is successfully completed.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(24,'Checked By','Drain points cleaning is successfully taken over by you.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(25,'Start','Accessory cleaning is successfully started.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(26,'Stop','Accessory cleaning is successfully completed.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(27,'Checked By','Accessory cleaning is successfully taken over by you.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(28,'Warning','This will finish the current task. Do you really want to finish it?',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(29,'Exception','The operation for this lot is already finished successfully.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(30,'Error','User id or paasword is incorrect. ',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(31,'Request','You must change your password using the Profile option in the system. Remember the password. Next time when you login use this password.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(32,'Error','You cannot finish an operation when there is breakdown. First close the breakdown.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(33,'Error','You cannot log a breakdown without taking over.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(34,'Sequential Log for Area and Equipments','There is no data in the database tables.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(35,'Sequential Log for Area and Equipments','The cleaning time has exceeded. Please do cleaning again.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(36,'Block Master Maintenance','Missing Block data',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(37,'Block Master Maintenance','Block data updated',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(38,'Block Master Maintenance','Block data added',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(39,'Block Master Maintenance','Block data deleted',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(40,'Role master','Role data deleted',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(41,'Role master','Role data added',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(42,'Role master','Role data deleted',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(43,'Role master','Missing role data',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(44,'Start','Type B cleaning is successfully started.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(45,'Exception handing','This is a dummy message 9991','Admin','2020-01-15 05:23:27','Admin','2019-10-05 11:30:29',_binary '\0'),(46,'Activity Master','Activity Successfully Saved',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(47,'Activity Master','Activity Successfully Deleted',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(48,'Blank data','Opps Please Contact Administrator.We are facing issued with Data validity Or Blank Data in following Table(s)',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(49,'ERR_solutionPreparation','No valid solutions are defined in the Solution Master table. Please contact administrator.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(50,'ERR_drainPoint','Drain Points have not been defined for this room. Please contact the administrator.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(51,'ERR_drainPointsolstock','No valid solution is available for cleaning. Either the solutions do not exist or have expired.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(55,'ERR_dailyCleaning','Either there is no solution in Solution Stock or it has crossed its expiry date. Please create a new solution.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(56,'ERR_accessoryCleaning','No Accessories avaialable',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(57,'ERR_inprocessContainercleaning','No Equipments avaialable',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(58,'ERR_docid','Document number does not exist in Document master. Please contact administrator.','Admin','2020-01-15 05:23:27',NULL,NULL,_binary ''),(59,'ERR_InEquipmentNotFoundByRoomcode','Either equipments or SOPs for equipment do not exist in master tables. Please contact administrator. ',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(60,'ERR_AreaNotfoundByBlockcode','This block does not have any areas. Please contact area administrator.',NULL,'2020-01-15 05:23:27',NULL,NULL,_binary ''),(61,'ERR_ActivityNotFound','This activity is not defined for this area. Please contact the administrator.','Admin','2020-01-15 05:23:27',NULL,NULL,_binary '');
/*!40000 ALTER TABLE `mst_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_plant`
--

DROP TABLE IF EXISTS `mst_plant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_plant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `plant_code` varchar(50) DEFAULT NULL,
  `plant_name` varchar(250) DEFAULT NULL,
  `plant_address` varchar(2000) DEFAULT NULL,
  `plant_remarks` varchar(4000) DEFAULT NULL,
  `plant_contact` bigint(20) DEFAULT NULL,
  `number_of_blocks` int(11) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `plant_code` (`plant_code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_plant`
--

LOCK TABLES `mst_plant` WRITE;
/*!40000 ALTER TABLE `mst_plant` DISABLE KEYS */;
INSERT INTO `mst_plant` VALUES (4,'PL001','SPIL Paonta Sahib','Paonta Sahib','Added by Sanchayita for testing',9999089900,10,_binary '','Admin','2020-01-15 05:23:28','Superadmin','2020-04-03 17:13:59'),(6,'Pl002','Plant 2','ghaziabad','test',9999999999,6,_binary '\0','Superadmin','2020-04-06 14:22:13','Superadmin','2020-04-06 19:52:28');
/*!40000 ALTER TABLE `mst_plant` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_plantactivity` AFTER UPDATE ON `mst_plant` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_plant',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	 IF(OLD.plant_code != NEW.plant_code)
	  THEN
	  
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_code', OLD.plant_code, NEW.plant_code);
	  END IF;
	  IF(OLD.plant_name != NEW.plant_name)
	  THEN
	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_name', OLD.plant_name, NEW.plant_name);
	  END IF; 
	  
	  IF(OLD.plant_address != NEW.plant_address)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_address', OLD.plant_address, NEW.plant_address);
	  END IF; 
	  
	  IF(OLD.plant_contact != NEW.plant_contact)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_contact', OLD.plant_contact, NEW.plant_contact);
	  END IF; 
	  
	  
	  IF(OLD.plant_remarks != NEW.plant_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'plant_remarks', OLD.plant_remarks, NEW.plant_remarks);
	  END IF; 
	  
	  
	  IF(OLD.number_of_blocks != NEW.number_of_blocks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'number_of_blocks', OLD.number_of_blocks, NEW.number_of_blocks);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_plant', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_procedure`
--

DROP TABLE IF EXISTS `mst_procedure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_procedure` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `procedure_code` varchar(50) DEFAULT NULL,
  `procedure_name` varchar(1000) DEFAULT NULL,
  `solution_code` varchar(50) DEFAULT NULL,
  `is_active` bit(1) DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_procedure`
--

LOCK TABLES `mst_procedure` WRITE;
/*!40000 ALTER TABLE `mst_procedure` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_procedure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_product`
--

DROP TABLE IF EXISTS `mst_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(50) DEFAULT NULL,
  `product_name` varchar(500) DEFAULT NULL,
  `product_market` varchar(500) DEFAULT NULL,
  `product_remark` varchar(2000) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_code` (`product_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_product`
--

LOCK TABLES `mst_product` WRITE;
/*!40000 ALTER TABLE `mst_product` DISABLE KEYS */;
INSERT INTO `mst_product` VALUES (1,'8040471','Ketanov MD tablets 10 mg','usa','','Superadmin','2020-04-07 06:52:23',NULL,NULL,_binary ''),(2,'8040413','Gliclazide MR Tabs 30 mg-BRZ','India','','Superadmin','2020-04-07 06:52:57',NULL,NULL,_binary ''),(3,'8040411','Gliclazide MR Tabs 30/60 mg CB-BRZ','USA','','Superadmin','2020-04-07 06:53:27',NULL,NULL,_binary '');
/*!40000 ALTER TABLE `mst_product` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_productactivity` AFTER UPDATE ON `mst_product` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_product',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.product_code != NEW.product_code)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'product_code', OLD.product_code, NEW.product_code);
	  END IF;
	  IF(OLD.product_name != NEW.product_name)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'product_name', OLD.product_name, NEW.product_name);
	  END IF;
	  
	  IF(OLD.product_market != NEW.product_market)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'product_market', OLD.product_market, NEW.product_market);
	  END IF;
	  IF(OLD.product_remark != NEW.product_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'product_remark', OLD.product_remark, NEW.product_remark);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_product', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_product_process`
--

DROP TABLE IF EXISTS `mst_product_process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_product_process` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `process_code` varchar(50) DEFAULT NULL,
  `process_name` varchar(500) DEFAULT NULL,
  `process_type` varchar(500) DEFAULT NULL,
  `process_duration` varchar(200) DEFAULT NULL,
  `process_remark` varchar(2000) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_product_process`
--

LOCK TABLES `mst_product_process` WRITE;
/*!40000 ALTER TABLE `mst_product_process` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_product_process` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_role`
--

DROP TABLE IF EXISTS `mst_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_level` int(11) DEFAULT NULL,
  `role_code` varchar(50) DEFAULT NULL,
  `role_type` varchar(500) DEFAULT NULL,
  `role_description` varchar(2000) DEFAULT NULL,
  `role_remark` varchar(2000) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_code` (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_role`
--

LOCK TABLES `mst_role` WRITE;
/*!40000 ALTER TABLE `mst_role` DISABLE KEYS */;
INSERT INTO `mst_role` VALUES (1,1,'Role1','','Operator','','Admin','2020-01-15 05:23:31',NULL,'2019-09-04 11:00:00',1),(2,2,'Role2','','Area Supervisor','','Admin','2020-01-15 05:23:31',NULL,'2019-09-04 11:00:00',1),(3,5,'Role3','','Quatity Control','','Admin','2020-01-15 05:23:31',NULL,'2019-09-04 11:00:00',1),(4,3,'Role4','','Production Manager','','Admin','2020-01-15 05:23:31',NULL,'2019-09-04 11:00:00',1),(5,6,'Role5','','Quality Assurance','','Admin','2020-01-15 05:23:31',NULL,'2019-09-04 11:00:00',1),(6,NULL,'Role6',NULL,'Super Admin',NULL,'Admin','2020-01-15 05:23:31',NULL,'2019-09-04 11:00:00',2),(11,4,'Role7',NULL,'BlockHead',NULL,'Admin','2020-01-31 06:53:08',NULL,NULL,1);
/*!40000 ALTER TABLE `mst_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_roleactivity` AFTER UPDATE ON `mst_role` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_role',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.role_code != NEW.role_code)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'role_code', OLD.role_code, NEW.role_code);
	  END IF;
	  IF(OLD.role_type != NEW.role_type)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'role_type', OLD.role_type, NEW.role_type);
	  END IF;
	  
	  IF(OLD.role_description != NEW.role_description)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'role_description', OLD.role_description, NEW.role_description);
	  END IF;
	  IF(OLD.role_remark != NEW.role_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'role_remark', OLD.role_remark, NEW.role_remark);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_role', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_roleid_workflow_mapping`
--

DROP TABLE IF EXISTS `mst_roleid_workflow_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_roleid_workflow_mapping` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(500) NOT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_roleid_workflow_mapping`
--

LOCK TABLES `mst_roleid_workflow_mapping` WRITE;
/*!40000 ALTER TABLE `mst_roleid_workflow_mapping` DISABLE KEYS */;
INSERT INTO `mst_roleid_workflow_mapping` VALUES (23,1,1,'2020-01-14 23:53:32','Superadmin','Superadmin','2019-10-10 22:35:47',_binary '\0'),(24,2,1,'2020-01-14 23:53:32','Superadmin',NULL,NULL,_binary ''),(25,3,2,'2020-01-14 23:53:32','Superadmin',NULL,NULL,_binary ''),(26,5,3,'2020-01-14 23:53:32','Superadmin',NULL,NULL,_binary ''),(27,4,4,'2020-01-14 23:53:32','Superadmin',NULL,NULL,_binary ''),(28,6,5,'2020-01-14 23:53:32','Superadmin',NULL,NULL,_binary ''),(29,7,11,'2020-01-31 05:40:03','SuperAdmin',NULL,NULL,_binary '');
/*!40000 ALTER TABLE `mst_roleid_workflow_mapping` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_roleworkflowactivity` AFTER UPDATE ON `mst_roleid_workflow_mapping` FOR EACH ROW BEGIN
IF(New.is_active =1)
THEN
  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
  VALUES(NULL,'Updated','mst_roleid_workflow_mapping',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
  SET @last_id = (SELECT LAST_INSERT_ID());
  
  IF(OLD.status_id != NEW.status_id)
  THEN  
  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
  VALUES(NULL,@last_id,'status_id', OLD.status_id, NEW.status_id);
  END IF;
  IF(OLD.role_id != NEW.role_id)
  THEN
  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
  VALUES(NULL,@last_id,'role_id', OLD.role_id, NEW.role_id);
  END IF;  
  END IF;
  
  
  IF(New.is_active =0)
THEN
  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
  VALUES(NULL,'Deleted','mst_roleid_workflow_mapping', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
  SET @last_id = (SELECT LAST_INSERT_ID());
  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
  END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_room`
--

DROP TABLE IF EXISTS `mst_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_room` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `room_code` varchar(50) DEFAULT NULL,
  `room_name` varchar(250) DEFAULT NULL,
  `room_address` varchar(4000) DEFAULT NULL,
  `room_remarks` varchar(2000) DEFAULT NULL,
  `room_contact` bigint(20) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `in_use` int(1) DEFAULT '0',
  `inuse_activity` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `room_code` (`room_code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_room`
--

LOCK TABLES `mst_room` WRITE;
/*!40000 ALTER TABLE `mst_room` DISABLE KEYS */;
INSERT INTO `mst_room` VALUES (1,'WC0001','Wurster Coating Room 1',NULL,'',NULL,'WC0001',_binary '','Superadmin','2020-04-07 06:49:55',NULL,NULL,0,NULL),(2,'DR0001','Dispensing Room 1',NULL,'',NULL,'DR0001',_binary '','Superadmin','2020-04-07 06:50:12',NULL,NULL,0,NULL),(3,'PR0001','Packaging Room 1',NULL,'',NULL,'PR0001',_binary '','Superadmin','2020-04-07 06:50:24',NULL,NULL,0,NULL),(4,'IPQA','IPQA PBG Room',NULL,'',NULL,'IPQA',_binary '','Superadmin','2020-04-07 06:51:35',NULL,NULL,0,NULL),(5,'CD0001','Cold Room',NULL,'Added Cold room under Packaging area',NULL,'PR0001',_binary '','Superadmin','2020-04-07 10:27:46',NULL,NULL,0,NULL),(6,'TL0001','Tooling Room',NULL,'Added Tooling Room in Packaging area',NULL,'PR0001',_binary '','Superadmin','2020-04-07 10:28:39',NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `mst_room` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_roomactivity` AFTER UPDATE ON `mst_room` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_room',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.room_code != NEW.room_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_code', OLD.room_code, NEW.room_code);
	  END IF;
	  
	  IF(OLD.room_name != NEW.room_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_name', OLD.room_name, NEW.room_name);
	  END IF;
	  
	  IF(OLD.room_address != NEW.room_address)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_address', OLD.room_address, NEW.room_address);
	  END IF;
	  
	 IF(OLD.room_contact != NEW.room_contact)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_contact', OLD.room_contact, NEW.room_contact);
	  END IF; 
	  
	  
	  IF(OLD.room_remarks != NEW.room_remarks)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'room_remarks', OLD.room_remarks, NEW.room_remarks);
	  END IF; 
	  
	  
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_code', OLD.area_code, NEW.area_code);
	  END IF; 
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_room', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_shift`
--

DROP TABLE IF EXISTS `mst_shift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shift_code` varchar(50) DEFAULT NULL,
  `shift_starttime` datetime DEFAULT NULL,
  `shift_endtime` datetime DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_shift`
--

LOCK TABLES `mst_shift` WRITE;
/*!40000 ALTER TABLE `mst_shift` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_shift` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_solution`
--

DROP TABLE IF EXISTS `mst_solution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_solution` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `solution_code` varchar(50) DEFAULT NULL,
  `solution_name` varchar(500) DEFAULT NULL,
  `Block_Code` varchar(50) DEFAULT NULL,
  `Precautions` varchar(1000) DEFAULT NULL,
  `Short_Name` varchar(200) DEFAULT NULL,
  `sol_qty` varchar(200) DEFAULT NULL,
  `water_qty` varchar(200) DEFAULT NULL,
  `tot_qty` varchar(200) DEFAULT NULL,
  `UoM` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_solution`
--

LOCK TABLES `mst_solution` WRITE;
/*!40000 ALTER TABLE `mst_solution` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_solution` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_solutionactivity` AFTER UPDATE ON `mst_solution` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_solution',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.solution_code != NEW.solution_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'solution_code', OLD.solution_code, NEW.solution_code);
	  END IF;
	  
	  IF(OLD.solution_name != NEW.solution_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'solution_name', OLD.solution_name, NEW.solution_name);
	  END IF;
	  
	  IF(OLD.Block_Code != NEW.Block_Code)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'Block_Code', OLD.Block_Code, NEW.Block_Code);
	  END IF;
	  
	 IF(OLD.Precautions != NEW.Precautions)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'Precautions', OLD.Precautions, NEW.Precautions);
	  END IF; 
	  
	  IF(OLD.Short_Name != NEW.Short_Name)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'Short_Name', OLD.Short_Name, NEW.Short_Name);
	  END IF;
	  IF(OLD.sol_qty != NEW.sol_qty)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sol_qty', OLD.sol_qty, NEW.sol_qty);
	  END IF;
	  IF(OLD.water_qty != NEW.water_qty)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'water_qty', OLD.water_qty, NEW.water_qty);
	  END IF;
	  IF(OLD.tot_qty != NEW.tot_qty)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'tot_qty', OLD.tot_qty, NEW.tot_qty);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_solution', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_sop`
--

DROP TABLE IF EXISTS `mst_sop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_sop` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sop_code` varchar(50) DEFAULT NULL,
  `sop_name` varchar(500) DEFAULT NULL,
  `sop_type` varchar(500) DEFAULT NULL,
  `sop_frequency` varchar(500) DEFAULT NULL,
  `sop_remark` varchar(2000) DEFAULT NULL,
  `area_code` varchar(50) DEFAULT NULL,
  `activity_code` varchar(50) DEFAULT NULL,
  `filepath` varchar(1000) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_sop`
--

LOCK TABLES `mst_sop` WRITE;
/*!40000 ALTER TABLE `mst_sop` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_sop` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_sopactivity` AFTER UPDATE ON `mst_sop` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_sop',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.sop_code != NEW.sop_code)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_code', OLD.sop_code, NEW.sop_code);
	  END IF;
	  IF(OLD.sop_name != NEW.sop_name)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_name', OLD.sop_name, NEW.sop_name);
	  END IF;
	  
	  IF(OLD.sop_type != NEW.sop_type)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_type', OLD.sop_type, NEW.sop_type);
	  END IF;
	  IF(OLD.sop_frequency != NEW.sop_frequency)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_frequency', OLD.sop_frequency, NEW.sop_frequency);
	  END IF;
	  IF(OLD.sop_remark != NEW.sop_remark)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'sop_remark', OLD.sop_remark, NEW.sop_remark);
	  END IF;
	  IF(OLD.area_code != NEW.area_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'area_code', OLD.area_code, NEW.area_code);
	  END IF;
	  IF(OLD.activity_code != NEW.activity_code)
	  THEN
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'activity_code', OLD.activity_code, NEW.activity_code);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_sop', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `mst_status`
--

DROP TABLE IF EXISTS `mst_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_status` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(250) DEFAULT NULL,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_status`
--

LOCK TABLES `mst_status` WRITE;
/*!40000 ALTER TABLE `mst_status` DISABLE KEYS */;
INSERT INTO `mst_status` VALUES (1,'START','2020-01-15 05:23:39','ADMIN','ADMIN','2019-08-30 18:33:18',_binary ''),(2,'STOP','2020-01-15 05:23:39','ADMIN','ADMIN','2019-08-30 18:36:48',_binary ''),(3,'Approved by AS','2020-01-15 05:23:39','ADMIN','ADMIN','2019-08-30 18:36:48',_binary ''),(4,'Approved by PM','2020-01-15 05:23:39','ADMIN','ADMIN','2019-08-30 18:36:48',_binary ''),(5,'Approved by QC','2020-01-15 05:23:39','ADMIN','ADMIN','2019-08-30 18:36:48',_binary ''),(6,'Approved by QA','2020-01-15 05:23:39','ADMIN','ADMIN','2019-08-30 18:36:48',_binary '');
/*!40000 ALTER TABLE `mst_status` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `au_statusactivity` AFTER UPDATE ON `mst_status` FOR EACH ROW BEGIN
	IF(New.is_active =1)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Updated','mst_status',NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  
	  IF(OLD.status_name != NEW.status_name)
	  THEN	 
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'status_name', OLD.status_name, NEW.status_name);
	  END IF;
	  END IF;
	  
	  
	  IF(New.is_active =0)
	THEN
	  INSERT INTO log_tableaction(id,`action`,tablename,tableid,created_by,created_on,modified_by,modified_on)
	  VALUES(NULL,'Deleted','mst_status', NEW.id, NEW.created_by, NEW.created_on, NEW.modified_by, NEW.modified_on);
	  SET @last_id = (SELECT LAST_INSERT_ID());
	  INSERT INTO log_tablechanges(id,log_id,fieldname,oldvalue,newvalue)
	  VALUES(NULL,@last_id,'is_active', OLD.is_active, NEW.is_active);
	  END IF;
	END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `operationheader_audit_log`
--

DROP TABLE IF EXISTS `operationheader_audit_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operationheader_audit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tablename` varchar(250) NOT NULL,
  `tableid` int(11) NOT NULL,
  `action` varchar(250) NOT NULL,
  `createdon` datetime NOT NULL,
  `createdby` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `operationheader_audit_log`
--

LOCK TABLES `operationheader_audit_log` WRITE;
/*!40000 ALTER TABLE `operationheader_audit_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `operationheader_audit_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `operationsqnlog`
--

DROP TABLE IF EXISTS `operationsqnlog`;
/*!50001 DROP VIEW IF EXISTS `operationsqnlog`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `operationsqnlog` AS SELECT 
 1 AS `id`,
 1 AS `document_no`,
 1 AS `lot_no`,
 1 AS `area_code`,
 1 AS `room_code`,
 1 AS `batch_no`,
 1 AS `product_description`,
 1 AS `operation_type`,
 1 AS `start_time`,
 1 AS `end_time`,
 1 AS `started_by`,
 1 AS `ended_by`,
 1 AS `names`,
 1 AS `approvedby`,
 1 AS `approvedon`,
 1 AS `equipments`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `pts_audit_log`
--

DROP TABLE IF EXISTS `pts_audit_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_audit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `terminalID` varchar(500) DEFAULT NULL,
  `processname_master` varchar(500) DEFAULT NULL,
  `activity_sub_task_actions` varchar(500) DEFAULT NULL,
  `action_performed` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(500) DEFAULT NULL,
  `oldval` varchar(500) DEFAULT NULL,
  `newval` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_audit_log`
--

LOCK TABLES `pts_audit_log` WRITE;
/*!40000 ALTER TABLE `pts_audit_log` DISABLE KEYS */;
INSERT INTO `pts_audit_log` VALUES (1,'N/A','Employee Master','emp_name','update','2020-04-06 09:23:47','Superadmin','Pooname','Poonam');
/*!40000 ALTER TABLE `pts_audit_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_log_tableaction`
--

DROP TABLE IF EXISTS `pts_log_tableaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_log_tableaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(200) DEFAULT NULL,
  `tablename` varchar(500) DEFAULT NULL,
  `tableid` int(11) DEFAULT NULL,
  `created_by` varchar(500) DEFAULT NULL,
  `created_on` varchar(500) DEFAULT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_log_tableaction`
--

LOCK TABLES `pts_log_tableaction` WRITE;
/*!40000 ALTER TABLE `pts_log_tableaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_log_tableaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_log_tablechanges`
--

DROP TABLE IF EXISTS `pts_log_tablechanges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_log_tablechanges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log_id` int(11) DEFAULT NULL,
  `fieldname` varchar(500) DEFAULT NULL,
  `oldvalue` text,
  `newvalue` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_log_tablechanges`
--

LOCK TABLES `pts_log_tablechanges` WRITE;
/*!40000 ALTER TABLE `pts_log_tablechanges` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_log_tablechanges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_activity`
--

DROP TABLE IF EXISTS `pts_mst_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_activity` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(80) DEFAULT NULL,
  `activity_url` varchar(100) DEFAULT NULL,
  `is_need_approval` enum('yes','no') DEFAULT 'yes',
  `workflowtype` varchar(200) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `type` varchar(50) DEFAULT NULL,
  `room_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `activity_name` (`activity_name`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_activity`
--

LOCK TABLES `pts_mst_activity` WRITE;
/*!40000 ALTER TABLE `pts_mst_activity` DISABLE KEYS */;
INSERT INTO `pts_mst_activity` VALUES ('2020-03-17 08:41:45','','0000-00-00 00:00:00','',1,'Type A Cleaning',NULL,'yes','onstop','active','home',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',2,'Type B Cleaning',NULL,'yes','onstop','active','home',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',3,'Type C Cleaning',NULL,'yes','onstop','active','home',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',4,'Type D Cleaning',NULL,'yes','onstop','active','home',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',5,'Maintenance',NULL,'yes','onstart','active','home',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',6,'Production',NULL,'yes','onstart','active','home',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',7,'Sampling',NULL,'yes','onstart','active','home',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',8,'Issuance',NULL,'yes',NULL,'active','tabletooling',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',9,'Cleaning Before Use',NULL,'yes',NULL,'active','tabletooling',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',10,'Inspection / Verification Before Use',NULL,'yes',NULL,'active','tabletooling',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',11,'Usage',NULL,'yes',NULL,'active','tabletooling',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',12,'Cleaning After Use',NULL,'yes',NULL,'active','tabletooling',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',13,'Inspection After Use',NULL,'yes',NULL,'active','tabletooling',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',14,'No. of Punch/Die Returned to Storage Cabinet',NULL,'yes',NULL,'active','tabletooling',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',16,'Process Log','processlog_equipment','yes',NULL,'active','log',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',17,'Portable Equipment Log','process_log_portable_equipment','yes',NULL,'active','log',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',18,'Return Air Filter Cleaning Record','air_filter_cleaning_record','yes',NULL,'active','log',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',19,'Material Retreival and Relocation Record for Cold Room','material_retreival_relocation_record','yes',NULL,'active','log',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',20,'Environmental Condition/Pressure Differential Record','environmental_condition_record','yes',NULL,'active','log',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',21,'Vaccum Cleaner Logbook','vaccum_cleaner_logbook','yes',NULL,'active','log',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',22,'LAF Pressure Differential Record Sheet','laf_pressure_record','yes',NULL,'active','log',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',23,'Tablet Tooling Log Card','tablet_tooling_log_card','yes',NULL,'active','log',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',24,'Balance Calibration Record','balance_calibration_record','yes',NULL,'active','log',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',25,'Vertical Sampler and Dies Cleaning Usages Log','vertical_sampler_dies_cleaning','yes',NULL,'active','log',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',26,'Instrument Log Register','instrument_log_register','no',NULL,'active','log',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',27,'Equipment/Apparatus Log Register (Friabilator)','equipment_apparatus_log_register','no',NULL,'active','log',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',28,'Swab Sample Record','swab_sample_record','yes',NULL,'active','log',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',29,'Pre-Filter Cleaning Record of LAF','pre_filter_cleaning_record','yes',NULL,'active','log',NULL),('2020-03-17 08:41:45','','0000-00-00 00:00:00','',30,'Line Log','line_log','yes',NULL,'active','log',NULL),('2020-03-17 03:11:45','','2020-04-12 11:12:01','',31,'Equipment Apparatus Leaktest','sf_equipment_apparatus_leaktest','no',NULL,'active','log',NULL);
/*!40000 ALTER TABLE `pts_mst_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_balance_calibration`
--

DROP TABLE IF EXISTS `pts_mst_balance_calibration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_balance_calibration` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL,
  `room_code` varchar(100) NOT NULL,
  `balance_no` varchar(100) NOT NULL,
  `capacity` varchar(100) NOT NULL,
  `least_count` varchar(100) NOT NULL,
  `acceptance_limit` varchar(50) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`),
  UNIQUE KEY `balance_no` (`balance_no`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_balance_calibration`
--

LOCK TABLES `pts_mst_balance_calibration` WRITE;
/*!40000 ALTER TABLE `pts_mst_balance_calibration` DISABLE KEYS */;
INSERT INTO `pts_mst_balance_calibration` VALUES ('2020-04-07 06:58:10','','0000-00-00 00:00:00','',1,0,'','PBGWB0001','600','0.05','','active');
/*!40000 ALTER TABLE `pts_mst_balance_calibration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_balance_calibration_standard`
--

DROP TABLE IF EXISTS `pts_mst_balance_calibration_standard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_balance_calibration_standard` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `balance_calibration_id` int(11) NOT NULL,
  `measurement_unit` varchar(20) NOT NULL,
  `id_standard` float NOT NULL,
  `frequency_id` int(11) NOT NULL,
  `standard_value` float NOT NULL,
  `min_value` float NOT NULL,
  `max_value` float NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_balance_calibration_standard`
--

LOCK TABLES `pts_mst_balance_calibration_standard` WRITE;
/*!40000 ALTER TABLE `pts_mst_balance_calibration_standard` DISABLE KEYS */;
INSERT INTO `pts_mst_balance_calibration_standard` VALUES ('2020-04-07 06:58:10','','0000-00-00 00:00:00','',1,1,'KG',0,0,1,0.9,1.1,'active'),('2020-04-07 06:58:10','','0000-00-00 00:00:00','',2,1,'KG',0,0,150,149.9,150.1,'active'),('2020-04-07 06:58:10','','0000-00-00 00:00:00','',3,1,'KG',0,0,300,299.9,300.1,'active'),('2020-04-07 06:58:10','','0000-00-00 00:00:00','',4,1,'KG',0,0,480,479.9,480.1,'active');
/*!40000 ALTER TABLE `pts_mst_balance_calibration_standard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_balance_frequency_data`
--

DROP TABLE IF EXISTS `pts_mst_balance_frequency_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_balance_frequency_data` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `balance_no` varchar(100) NOT NULL,
  `frequency_id` int(11) NOT NULL,
  `id_standard` varchar(20) DEFAULT NULL,
  `measurement_unit` varchar(20) NOT NULL,
  `standard_value` varchar(20) NOT NULL,
  `min_value` varchar(20) NOT NULL,
  `max_value` varchar(20) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_balance_frequency_data`
--

LOCK TABLES `pts_mst_balance_frequency_data` WRITE;
/*!40000 ALTER TABLE `pts_mst_balance_frequency_data` DISABLE KEYS */;
INSERT INTO `pts_mst_balance_frequency_data` VALUES ('2020-04-07 09:32:01','','0000-00-00 00:00:00','',1,'PBGWB0001',1,'0','KG','1','0.9','1.1','active'),('2020-04-07 09:32:01','','0000-00-00 00:00:00','',2,'PBGWB0001',1,'0','KG','150','149.9','150.1','active'),('2020-04-07 09:32:17','','0000-00-00 00:00:00','',3,'PBGWB0001',2,'0','KG','1','0.9','1.1','active'),('2020-04-07 09:32:17','','0000-00-00 00:00:00','',4,'PBGWB0001',2,'0','KG','150','149.9','150.1','active'),('2020-04-07 09:32:17','','0000-00-00 00:00:00','',5,'PBGWB0001',2,'0','KG','480','479.9','480.1','active'),('2020-04-07 09:32:34','','0000-00-00 00:00:00','',6,'PBGWB0001',4,'0','KG','1','0.9','1.1','active'),('2020-04-07 09:32:34','','0000-00-00 00:00:00','',7,'PBGWB0001',4,'0','KG','150','149.9','150.1','active'),('2020-04-07 09:32:34','','0000-00-00 00:00:00','',8,'PBGWB0001',4,'0','KG','300','299.9','300.1','active'),('2020-04-07 09:32:34','','0000-00-00 00:00:00','',9,'PBGWB0001',4,'0','KG','480','479.9','480.1','active');
/*!40000 ALTER TABLE `pts_mst_balance_frequency_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_document`
--

DROP TABLE IF EXISTS `pts_mst_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_document` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `prefix` varchar(50) DEFAULT NULL,
  `docno` bigint(50) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `docno` (`docno`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_document`
--

LOCK TABLES `pts_mst_document` WRITE;
/*!40000 ALTER TABLE `pts_mst_document` DISABLE KEYS */;
INSERT INTO `pts_mst_document` VALUES ('2020-03-17 08:41:45','','0000-00-00 00:00:00','',13,'DOC',2387,_binary '');
/*!40000 ALTER TABLE `pts_mst_document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_filter`
--

DROP TABLE IF EXISTS `pts_mst_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_filter` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `filter_name` varchar(100) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `filter_desc` varchar(150) DEFAULT NULL,
  `status` enum('on','off') DEFAULT 'on',
  `is_approved` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_filter`
--

LOCK TABLES `pts_mst_filter` WRITE;
/*!40000 ALTER TABLE `pts_mst_filter` DISABLE KEYS */;
INSERT INTO `pts_mst_filter` VALUES ('2020-04-07 07:34:12','','0000-00-00 00:00:00','',1,'F1','WC0001',NULL,'on',_binary '\0'),('2020-04-07 07:34:12','','0000-00-00 00:00:00','',2,'F2','WC0001',NULL,'on',_binary '\0'),('2020-04-07 07:34:12','','0000-00-00 00:00:00','',3,'F3','WC0001',NULL,'on',_binary '\0'),('2020-04-07 07:34:12','','0000-00-00 00:00:00','',4,'F4','WC0001',NULL,'on',_binary '\0'),('2020-04-07 07:34:29','','0000-00-00 00:00:00','',5,'F1','DR0001',NULL,'on',_binary '\0'),('2020-04-07 07:34:29','','0000-00-00 00:00:00','',6,'F2','DR0001',NULL,'on',_binary '\0'),('2020-04-07 07:35:04','','0000-00-00 00:00:00','',7,'F1','PR0001',NULL,'on',_binary '\0'),('2020-04-07 07:35:04','','0000-00-00 00:00:00','',8,'F2','PR0001',NULL,'on',_binary '\0'),('2020-04-07 07:35:04','','0000-00-00 00:00:00','',9,'F3','PR0001',NULL,'on',_binary '\0'),('2020-04-07 07:35:04','','0000-00-00 00:00:00','',10,'F4','PR0001',NULL,'on',_binary '\0'),('2020-04-07 07:35:04','','0000-00-00 00:00:00','',11,'F5','PR0001',NULL,'on',_binary '\0');
/*!40000 ALTER TABLE `pts_mst_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_frequency`
--

DROP TABLE IF EXISTS `pts_mst_frequency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_frequency` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `frequency_name` varchar(100) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`),
  UNIQUE KEY `frequency_name` (`frequency_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_frequency`
--

LOCK TABLES `pts_mst_frequency` WRITE;
/*!40000 ALTER TABLE `pts_mst_frequency` DISABLE KEYS */;
INSERT INTO `pts_mst_frequency` VALUES ('2020-03-17 03:11:46',NULL,NULL,NULL,1,'Daily','active'),('2020-03-17 03:11:46',NULL,NULL,NULL,2,'Weekly','active'),('2020-03-17 03:11:46',NULL,NULL,NULL,3,'Fortnightly','active'),('2020-03-17 03:11:46',NULL,NULL,NULL,4,'Monthly','active');
/*!40000 ALTER TABLE `pts_mst_frequency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_id_standard_weight`
--

DROP TABLE IF EXISTS `pts_mst_id_standard_weight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_id_standard_weight` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_no_statndard_weight` varchar(100) NOT NULL,
  `weight` float NOT NULL,
  `measurement_unit` varchar(20) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_id_standard_weight`
--

LOCK TABLES `pts_mst_id_standard_weight` WRITE;
/*!40000 ALTER TABLE `pts_mst_id_standard_weight` DISABLE KEYS */;
INSERT INTO `pts_mst_id_standard_weight` VALUES ('2020-04-07 09:33:05','','0000-00-00 00:00:00','',3,'PBG_1_01',1,'KG','active'),('2020-04-07 09:33:05','','0000-00-00 00:00:00','',4,'PBG_1_02',1,'KG','active'),('2020-04-07 09:33:57','','0000-00-00 00:00:00','',5,'PBG_100_01',100,'KG','active'),('2020-04-07 09:33:57','','0000-00-00 00:00:00','',6,'PBG_100_02',100,'KG','active'),('2020-04-07 09:33:57','','0000-00-00 00:00:00','',7,'PBG_100_03',100,'KG','active'),('2020-04-07 09:33:57','','0000-00-00 00:00:00','',8,'PBG_100_04',100,'KG','active'),('2020-04-07 09:33:57','','0000-00-00 00:00:00','',9,'PBG_100_05',100,'KG','active'),('2020-04-07 09:34:37','','0000-00-00 00:00:00','',10,'PBG_80_01',80,'KG','active'),('2020-04-07 09:34:37','','0000-00-00 00:00:00','',11,'PBG_80_02',80,'KG','active'),('2020-04-07 09:35:26','','0000-00-00 00:00:00','',12,'PBG_50_01',50,'KG','active'),('2020-04-07 09:35:26','','0000-00-00 00:00:00','',13,'PBG_50_02',50,'KG','active'),('2020-04-07 09:35:26','','0000-00-00 00:00:00','',14,'PBG_50_03',50,'KG','active');
/*!40000 ALTER TABLE `pts_mst_id_standard_weight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_instrument`
--

DROP TABLE IF EXISTS `pts_mst_instrument`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_instrument` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_code` varchar(20) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `equipment_name` varchar(100) NOT NULL,
  `equipment_code` varchar(100) NOT NULL,
  `equipment_desc` varchar(200) DEFAULT NULL,
  `equipment_type` enum('Instrument','Apparatus','Samplingrod') NOT NULL DEFAULT 'Instrument',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`),
  UNIQUE KEY `equipment_code` (`equipment_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_instrument`
--

LOCK TABLES `pts_mst_instrument` WRITE;
/*!40000 ALTER TABLE `pts_mst_instrument` DISABLE KEYS */;
INSERT INTO `pts_mst_instrument` VALUES ('2020-04-07 06:54:56','','0000-00-00 00:00:00','',1,'PBG',NULL,'Instrument - 1 PBG','EQIN0001','undefined','Instrument','active'),('2020-04-07 06:55:27','','0000-00-00 00:00:00','',2,'PBG',NULL,'Apparatus - 1 PBG','EQAP0001','','Apparatus','active'),('2020-04-07 06:56:12','','0000-00-00 00:00:00','',3,'PBG',NULL,'Sampling Rod - 1 PBG','EQSR0001','','Samplingrod','active');
/*!40000 ALTER TABLE `pts_mst_instrument` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_line_log`
--

DROP TABLE IF EXISTS `pts_mst_line_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_line_log` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_code` varchar(50) DEFAULT NULL,
  `area_code` varchar(50) NOT NULL,
  `room_code` varchar(100) NOT NULL,
  `line_log_code` varchar(100) NOT NULL,
  `line_log_name` varchar(100) NOT NULL,
  `equipment_code` varchar(500) NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_line_log`
--

LOCK TABLES `pts_mst_line_log` WRITE;
/*!40000 ALTER TABLE `pts_mst_line_log` DISABLE KEYS */;
INSERT INTO `pts_mst_line_log` VALUES ('2020-04-07 07:31:02','','0000-00-00 00:00:00','',1,'PBG','PR0001','PR0001','PBGPL0001','Packaging Line 1','EQPL0001,EQPL0002,EQPL0003','active');
/*!40000 ALTER TABLE `pts_mst_line_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_material_relocation`
--

DROP TABLE IF EXISTS `pts_mst_material_relocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_material_relocation` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `material_name` varchar(100) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `material_code` varchar(100) NOT NULL,
  `material_desc` varchar(150) DEFAULT NULL,
  `material_type` varchar(150) DEFAULT NULL,
  `material_batch_no` varchar(100) DEFAULT NULL,
  `material_relocation_type` varchar(100) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`),
  UNIQUE KEY `material_code` (`material_code`),
  UNIQUE KEY `material_code_2` (`material_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_material_relocation`
--

LOCK TABLES `pts_mst_material_relocation` WRITE;
/*!40000 ALTER TABLE `pts_mst_material_relocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_mst_material_relocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_report_header`
--

DROP TABLE IF EXISTS `pts_mst_report_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_report_header` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_no` varchar(100) NOT NULL,
  `version_no` varchar(100) NOT NULL,
  `effective_date` varchar(100) NOT NULL,
  `document_no` varchar(100) NOT NULL,
  `status` enum('active','inactive','default') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_report_header`
--

LOCK TABLES `pts_mst_report_header` WRITE;
/*!40000 ALTER TABLE `pts_mst_report_header` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_mst_report_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_role_level`
--

DROP TABLE IF EXISTS `pts_mst_role_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_role_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleid` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(250) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `roleid` (`roleid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_role_level`
--

LOCK TABLES `pts_mst_role_level` WRITE;
/*!40000 ALTER TABLE `pts_mst_role_level` DISABLE KEYS */;
INSERT INTO `pts_mst_role_level` VALUES (1,1,1,'2020-01-31 06:55:33','SuperAdmin',NULL,NULL,_binary ''),(2,2,2,'2020-01-31 06:55:33','SuperAdmin',NULL,NULL,_binary ''),(3,4,3,'2020-01-31 06:55:33','SuperAdmin',NULL,NULL,_binary ''),(4,11,4,'2020-01-31 06:55:33','SuperAdmin',NULL,NULL,_binary ''),(5,3,5,'2020-01-31 06:55:33','SuperAdmin',NULL,NULL,_binary ''),(6,5,6,'2020-01-31 06:55:33','SuperAdmin',NULL,NULL,_binary '');
/*!40000 ALTER TABLE `pts_mst_role_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_role_workassign_to_mult_roles`
--

DROP TABLE IF EXISTS `pts_mst_role_workassign_to_mult_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_role_workassign_to_mult_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleid` int(11) DEFAULT NULL,
  `workassign_to_roleid` int(11) DEFAULT NULL,
  `prty_btn_text` enum('on','off') NOT NULL DEFAULT 'off',
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_role_workassign_to_mult_roles`
--

LOCK TABLES `pts_mst_role_workassign_to_mult_roles` WRITE;
/*!40000 ALTER TABLE `pts_mst_role_workassign_to_mult_roles` DISABLE KEYS */;
INSERT INTO `pts_mst_role_workassign_to_mult_roles` VALUES (1,1,1,'off',_binary '','2020-04-07 06:53:53','Super Admin',NULL,NULL),(2,2,2,'off',_binary '','2020-04-07 06:54:04','Super Admin',NULL,NULL),(3,5,5,'off',_binary '','2020-04-07 06:54:15','Super Admin',NULL,NULL);
/*!40000 ALTER TABLE `pts_mst_role_workassign_to_mult_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_stages`
--

DROP TABLE IF EXISTS `pts_mst_stages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_stages` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log` varchar(50) DEFAULT NULL,
  `field_name` varchar(100) DEFAULT NULL,
  `field_value` varchar(100) DEFAULT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_stages`
--

LOCK TABLES `pts_mst_stages` WRITE;
/*!40000 ALTER TABLE `pts_mst_stages` DISABLE KEYS */;
INSERT INTO `pts_mst_stages` VALUES ('2020-04-07 07:43:12','Super Admin','0000-00-00 00:00:00','',1,'Instrument Log Register','ResultLeft','OK','active'),('2020-04-07 07:43:25','Super Admin','0000-00-00 00:00:00','',2,'Instrument Log Register','ResultLeft','Not OK','active'),('2020-04-07 07:43:40','Super Admin','0000-00-00 00:00:00','',3,'Instrument Log Register','ResultLeft','NA','active'),('2020-04-07 07:43:54','Super Admin','0000-00-00 00:00:00','',4,'Instrument Log Register','ResultRight','OK','active'),('2020-04-07 07:44:09','Super Admin','0000-00-00 00:00:00','',5,'Instrument Log Register','ResultRight','Not OK','active'),('2020-04-07 07:44:23','Super Admin','0000-00-00 00:00:00','',6,'Instrument Log Register','ResultRight','NA','active'),('2020-04-07 07:44:42','Super Admin','0000-00-00 00:00:00','',7,'Vertical Sampler and Dies Cleaning Usages Log','Activity','Cleaning','active'),('2020-04-07 07:44:53','Super Admin','0000-00-00 00:00:00','',8,'Vertical Sampler and Dies Cleaning Usages Log','Activity','Sampling','active'),('2020-04-07 07:45:08','Super Admin','0000-00-00 00:00:00','',9,'Vertical Sampler and Dies Cleaning Usages Log','Activity','Maintenance','active'),('2020-04-07 07:45:43','Super Admin','0000-00-00 00:00:00','',10,'Equipment/Apparatus Log Register (Friabilator)','Stage','Compression','active'),('2020-04-07 07:45:58','Super Admin','0000-00-00 00:00:00','',11,'Equipment/Apparatus Log Register (Friabilator)','cleaning_verification','OK','active'),('2020-04-07 07:46:19','Super Admin','0000-00-00 00:00:00','',12,'Equipment/Apparatus Log Register (Friabilator)','cleaning_verification','Not OK','active');
/*!40000 ALTER TABLE `pts_mst_stages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_stop_reason`
--

DROP TABLE IF EXISTS `pts_mst_stop_reason`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_stop_reason` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `stop_reason` varchar(500) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stop_reason` (`stop_reason`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_stop_reason`
--

LOCK TABLES `pts_mst_stop_reason` WRITE;
/*!40000 ALTER TABLE `pts_mst_stop_reason` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_mst_stop_reason` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_sys_id`
--

DROP TABLE IF EXISTS `pts_mst_sys_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_sys_id` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `status` enum('active','inactive') DEFAULT 'active',
  `room_name` varchar(60) DEFAULT NULL,
  `room_code` varchar(20) DEFAULT NULL,
  `area_id` varchar(60) DEFAULT NULL,
  `room_id` int(10) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL,
  `device_id` varchar(30) DEFAULT NULL,
  `logger_id` varchar(50) DEFAULT NULL,
  `gauge_id` varchar(50) DEFAULT NULL,
  `room_type` enum('room','corridor','IPQA') NOT NULL DEFAULT 'room',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_sys_id`
--

LOCK TABLES `pts_mst_sys_id` WRITE;
/*!40000 ALTER TABLE `pts_mst_sys_id` DISABLE KEYS */;
INSERT INTO `pts_mst_sys_id` VALUES ('2020-04-07 07:34:12','','0000-00-00 00:00:00','',1,'active','Wurster Coating Room 1','WC0001','WC0001',1,1586244852,'10.240.240.240','DL0001','MG0001','room'),('2020-04-07 07:34:29','','0000-00-00 00:00:00','',2,'active','Dispensing Room 1','DR0001','DR0001',2,1586244869,'10.240.240.240','','','room'),('2020-04-07 07:35:04','','0000-00-00 00:00:00','',3,'active','Packaging Room 1','PR0001','PR0001',3,1586244904,'10.240.240.240','DL0002','MG0002','room'),('2020-04-07 07:35:17','','0000-00-00 00:00:00','',4,'active','IPQA PBG Room','IPQA','IPQA',4,1586244917,'10.240.240.240','','','room'),('2020-04-07 10:47:01','','0000-00-00 00:00:00','',5,'active','Cold Room','CD0001','PR0001',5,1586256421,'10.240.240.240','','','room'),('2020-04-07 10:47:28','','0000-00-00 00:00:00','',6,'active','Tooling Room','TL0001','PR0001',6,1586256448,'10.240.240.240','','','room');
/*!40000 ALTER TABLE `pts_mst_sys_id` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_tablet_tooling_log`
--

DROP TABLE IF EXISTS `pts_mst_tablet_tooling_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_tablet_tooling_log` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(100) NOT NULL,
  `supplier` varchar(100) NOT NULL,
  `upper_punch_qty` float NOT NULL,
  `lower_punch_qty` float NOT NULL,
  `year` int(11) NOT NULL,
  `dimension` varchar(100) NOT NULL,
  `shape` varchar(100) NOT NULL,
  `machine` varchar(100) NOT NULL,
  `die_quantity` float NOT NULL,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_tablet_tooling_log`
--

LOCK TABLES `pts_mst_tablet_tooling_log` WRITE;
/*!40000 ALTER TABLE `pts_mst_tablet_tooling_log` DISABLE KEYS */;
INSERT INTO `pts_mst_tablet_tooling_log` VALUES ('2020-04-07 07:48:13','','0000-00-00 00:00:00','',1,'8040471','PD Ind. Ltd',50,10,2015,'10.00 * 20.00 MM','Rectangular','CTX',9999,'active'),('2020-04-07 07:48:13','','0000-00-00 00:00:00','',2,'8040471','PD Ind. Ltd',100,40,2020,'10.00 * 20.00 MM','Oval','CTX',9999,'active');
/*!40000 ALTER TABLE `pts_mst_tablet_tooling_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_vertical_sampler`
--

DROP TABLE IF EXISTS `pts_mst_vertical_sampler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_vertical_sampler` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sample_rod_id` varchar(100) NOT NULL,
  `sample_rod_desc` varchar(100) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sample_rod_id` (`sample_rod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_vertical_sampler`
--

LOCK TABLES `pts_mst_vertical_sampler` WRITE;
/*!40000 ALTER TABLE `pts_mst_vertical_sampler` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_mst_vertical_sampler` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_mst_vertical_sampler_activity`
--

DROP TABLE IF EXISTS `pts_mst_vertical_sampler_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_mst_vertical_sampler_activity` (
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified_on` datetime NOT NULL,
  `modified_by` varchar(50) NOT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(80) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_mst_vertical_sampler_activity`
--

LOCK TABLES `pts_mst_vertical_sampler_activity` WRITE;
/*!40000 ALTER TABLE `pts_mst_vertical_sampler_activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_mst_vertical_sampler_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_activity_approval_log`
--

DROP TABLE IF EXISTS `pts_trn_activity_approval_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_activity_approval_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `usr_activity_log_id` int(11) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `approval_status` enum('approve','reject','N/A') DEFAULT 'N/A',
  `workflow_type` varchar(50) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `activity_remarks` varchar(240) DEFAULT NULL,
  `role_id` int(10) DEFAULT NULL,
  `activity_time` int(10) DEFAULT NULL,
  `reason` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `created_on` (`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_activity_approval_log`
--

LOCK TABLES `pts_trn_activity_approval_log` WRITE;
/*!40000 ALTER TABLE `pts_trn_activity_approval_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_activity_approval_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_activity_log`
--

DROP TABLE IF EXISTS `pts_trn_activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_activity_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `usr_activity_log_id` int(11) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `activity_status` varchar(50) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `remarks` varchar(240) DEFAULT NULL,
  `role_id` int(10) DEFAULT NULL,
  `activity_time` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_activity_log`
--

LOCK TABLES `pts_trn_activity_log` WRITE;
/*!40000 ALTER TABLE `pts_trn_activity_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_balance_calibration`
--

DROP TABLE IF EXISTS `pts_trn_balance_calibration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_balance_calibration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `balance_id` int(11) DEFAULT NULL,
  `balance_no` varchar(50) DEFAULT NULL,
  `frequency_id` int(11) DEFAULT NULL,
  `standerd_wt` float DEFAULT NULL,
  `id_no_of_st_wt` varchar(50) DEFAULT NULL,
  `oberved_wt` float DEFAULT NULL,
  `diff` float DEFAULT NULL,
  `sprit_level_status` enum('ok','notok','NA') DEFAULT 'NA',
  `time_status` enum('ok','notok','NA') DEFAULT 'NA',
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bal_calb_index` (`room_code`,`balance_no`,`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_balance_calibration`
--

LOCK TABLES `pts_trn_balance_calibration` WRITE;
/*!40000 ALTER TABLE `pts_trn_balance_calibration` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_balance_calibration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_batchin`
--

DROP TABLE IF EXISTS `pts_trn_batchin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_batchin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `room_code` varchar(20) DEFAULT NULL,
  `batch_no` varchar(50) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_desc` varchar(300) NOT NULL,
  `emp_code` varchar(50) DEFAULT NULL,
  `emp_name` varchar(250) DEFAULT NULL,
  `emp_email` varchar(500) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `batch_in_time` int(11) DEFAULT NULL,
  `batch_out_time` int(11) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_batchin`
--

LOCK TABLES `pts_trn_batchin` WRITE;
/*!40000 ALTER TABLE `pts_trn_batchin` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_batchin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_emp_roomin`
--

DROP TABLE IF EXISTS `pts_trn_emp_roomin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_emp_roomin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `room_code` varchar(20) DEFAULT NULL,
  `emp_code` varchar(50) DEFAULT NULL,
  `emp_name` varchar(250) DEFAULT NULL,
  `emp_email` varchar(500) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  `session_id` varchar(200) DEFAULT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `room_in_time` int(11) DEFAULT NULL,
  `room_out_time` int(11) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_emp_roomin`
--

LOCK TABLES `pts_trn_emp_roomin` WRITE;
/*!40000 ALTER TABLE `pts_trn_emp_roomin` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_emp_roomin` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `emp_roomin_audit_log` AFTER INSERT ON `pts_trn_emp_roomin` FOR EACH ROW BEGIN
INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Room In','Employee','Insert',NEW.created_on, NEW.created_by, 'N/A', 'N/A');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `roomout` AFTER UPDATE ON `pts_trn_emp_roomin` FOR EACH ROW BEGIN
INSERT INTO pts_audit_log(id,terminalID,processname_master,activity_sub_task_actions,action_performed,	created_on,created_by,oldval,newval)
	  VALUES(NULL,'N/A','Room In','Employee','Insert',NEW.created_on, NEW.created_by, 'N/A', 'N/A');
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `pts_trn_env_cond_diff`
--

DROP TABLE IF EXISTS `pts_trn_env_cond_diff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_env_cond_diff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) NOT NULL,
  `data_log_id` varchar(50) DEFAULT NULL,
  `magnelic_id` varchar(50) DEFAULT NULL,
  `pressure_diff` enum('ok','notok','NA') NOT NULL DEFAULT 'NA',
  `reading` float DEFAULT NULL,
  `temp` float DEFAULT NULL,
  `temp_min` float DEFAULT NULL,
  `temp_max` float DEFAULT NULL,
  `rh` float DEFAULT NULL,
  `rh_min` float DEFAULT NULL,
  `rh_max` float DEFAULT NULL,
  `temp_from` varchar(50) DEFAULT NULL,
  `temp_to` varchar(50) DEFAULT NULL,
  `rh_from` varchar(50) DEFAULT NULL,
  `rh_to` varchar(50) DEFAULT NULL,
  `globe_pressure_diff` varchar(50) DEFAULT NULL,
  `from_pressure` varchar(50) DEFAULT NULL,
  `to_pressure` varchar(50) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(200) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_env_cond_diff`
--

LOCK TABLES `pts_trn_env_cond_diff` WRITE;
/*!40000 ALTER TABLE `pts_trn_env_cond_diff` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_env_cond_diff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_equip_appratus_leaktest`
--

DROP TABLE IF EXISTS `pts_trn_equip_appratus_leaktest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_equip_appratus_leaktest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `equip_appratus_no` varchar(200) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `stage` varchar(250) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_equip_appratus_leaktest`
--

LOCK TABLES `pts_trn_equip_appratus_leaktest` WRITE;
/*!40000 ALTER TABLE `pts_trn_equip_appratus_leaktest` DISABLE KEYS */;
INSERT INTO `pts_trn_equip_appratus_leaktest` VALUES (1,'DOC2553','TAA-78','App01','0011001A','1456','Test1',33,'sid',1,'sid@sunpharma.in','ss','2020-04-04 23:39:54','active','no'),(2,'DOC2562','TAA-78','App01','PL001','998877','Test1',33,'sid',1,'sid@sunpharma.in','sssss','2020-04-05 00:08:32','active','no'),(3,'DOC2566','TAA-78','App01','PL001','77','Test1',33,'sid',1,'sid@sunpharma.in','s','2020-04-05 00:14:02','active','no'),(4,'DOC2568','TAA-78','App01','0011001A','78','Test1',33,'sid',1,'sid@sunpharma.in','s','2020-04-05 01:33:07','active','no'),(5,'DOC2574','TAA-78','App01','0011002A','12','Test1',33,'sid',1,'sid@sunpharma.in','sssss','2020-04-05 01:40:24','active','no'),(6,'DOC2309','IPQA','EQAP0001','NA','NA','Compression',4,'Pooname',1,'prashant@smhs.motherson.com','ok','2020-04-06 15:32:42','active','yes'),(7,'DOC2376','WC0001','EQIN0001','8040711','JKT1234','Compression',2,'Prashant',1,'prashant@smhs.motherson.com','ok','2020-04-07 11:39:07','active','no');
/*!40000 ALTER TABLE `pts_trn_equip_appratus_leaktest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_equip_appratus_log`
--

DROP TABLE IF EXISTS `pts_trn_equip_appratus_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_equip_appratus_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `equip_appratus_no` varchar(200) DEFAULT NULL,
  `cleaning_verification` varchar(40) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `stage` varchar(250) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`),
  KEY `equip_aprts_lof_index` (`room_code`,`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_equip_appratus_log`
--

LOCK TABLES `pts_trn_equip_appratus_log` WRITE;
/*!40000 ALTER TABLE `pts_trn_equip_appratus_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_equip_appratus_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_instrument_log_register`
--

DROP TABLE IF EXISTS `pts_trn_instrument_log_register`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_instrument_log_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `instrument_code` varchar(50) DEFAULT NULL,
  `stage` varchar(100) DEFAULT NULL,
  `test` varchar(100) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `result_left` varchar(100) NOT NULL DEFAULT 'NA',
  `result_right` varchar(100) NOT NULL DEFAULT 'NA',
  `remark` varchar(500) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`),
  KEY `intrmt_log_reg_index` (`room_code`,`instrument_code`,`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_instrument_log_register`
--

LOCK TABLES `pts_trn_instrument_log_register` WRITE;
/*!40000 ALTER TABLE `pts_trn_instrument_log_register` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_instrument_log_register` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_laf_pressure_diff`
--

DROP TABLE IF EXISTS `pts_trn_laf_pressure_diff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_laf_pressure_diff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(20) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `booth_no` varchar(200) DEFAULT NULL,
  `m1` varchar(100) DEFAULT NULL,
  `m2` varchar(100) DEFAULT NULL,
  `g1` varchar(100) DEFAULT NULL,
  `g2` varchar(100) DEFAULT NULL,
  `g3` varchar(100) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`),
  KEY `laf_presr_diff_index` (`room_code`,`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_laf_pressure_diff`
--

LOCK TABLES `pts_trn_laf_pressure_diff` WRITE;
/*!40000 ALTER TABLE `pts_trn_laf_pressure_diff` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_laf_pressure_diff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_material_retreival_relocation`
--

DROP TABLE IF EXISTS `pts_trn_material_retreival_relocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_material_retreival_relocation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) NOT NULL,
  `material_code` varchar(50) DEFAULT NULL,
  `material_condition` varchar(10) DEFAULT NULL,
  `material_batch_no` varchar(50) NOT NULL,
  `material_type` varchar(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`),
  KEY `mat_rtrl_rloc_index` (`room_code`,`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_material_retreival_relocation`
--

LOCK TABLES `pts_trn_material_retreival_relocation` WRITE;
/*!40000 ALTER TABLE `pts_trn_material_retreival_relocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_material_retreival_relocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_pre_filter_cleaning`
--

DROP TABLE IF EXISTS `pts_trn_pre_filter_cleaning`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_pre_filter_cleaning` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) NOT NULL,
  `selection_status` enum('from','to') NOT NULL DEFAULT 'from',
  `product_code` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `pre_filter_cleaning` enum('yes','no','NA') NOT NULL DEFAULT 'NA',
  `outer_surface_cleaning` enum('yes','no','NA') NOT NULL DEFAULT 'NA',
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`),
  KEY `pre_filtr_rcode_index` (`room_code`,`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_pre_filter_cleaning`
--

LOCK TABLES `pts_trn_pre_filter_cleaning` WRITE;
/*!40000 ALTER TABLE `pts_trn_pre_filter_cleaning` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_pre_filter_cleaning` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_return_air_filter`
--

DROP TABLE IF EXISTS `pts_trn_return_air_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_return_air_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(100) NOT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `product_code` varchar(100) NOT NULL,
  `batch_no` varchar(100) NOT NULL,
  `frequency_id` int(11) NOT NULL,
  `filter` varchar(500) NOT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(100) DEFAULT NULL,
  `done_by_role_id` int(11) NOT NULL,
  `done_by_email` varchar(100) NOT NULL,
  `done_by_remark` varchar(200) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_in_workflow` enum('yes','no') DEFAULT 'no',
  `status` enum('active','inactive') DEFAULT 'active',
  PRIMARY KEY (`id`),
  KEY `rtn_air_filtr_index` (`room_code`,`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_return_air_filter`
--

LOCK TABLES `pts_trn_return_air_filter` WRITE;
/*!40000 ALTER TABLE `pts_trn_return_air_filter` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_return_air_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_room_log_activity`
--

DROP TABLE IF EXISTS `pts_trn_room_log_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_room_log_activity` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `activity_name` varchar(80) DEFAULT NULL,
  `activity_url` varchar(100) NOT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `room_id` int(10) DEFAULT NULL,
  `mst_act_id` int(11) NOT NULL,
  `created_date` int(10) DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_room_log_activity`
--

LOCK TABLES `pts_trn_room_log_activity` WRITE;
/*!40000 ALTER TABLE `pts_trn_room_log_activity` DISABLE KEYS */;
INSERT INTO `pts_trn_room_log_activity` VALUES (1,'Process Log','processlog_equipment','inactive',1,16,1586180254,NULL,'2020-04-06 19:08:03','Superadmin'),(2,'Process Log','processlog_equipment','active',1,16,1586180877,NULL,NULL,NULL),(3,'Portable Equipment Log','process_log_portable_equipment','active',1,17,1586183048,NULL,NULL,NULL),(4,'Return Air Filter Cleaning Record','air_filter_cleaning_record','active',1,18,1586183058,NULL,NULL,NULL),(5,'Material Retreival and Relocation Record for Cold Room','material_retreival_relocation_record','inactive',1,19,1586183070,NULL,'2020-04-07 13:11:09','Superadmin'),(6,'Environmental Condition/Pressure Differential Record','environmental_condition_record','active',1,20,1586183082,NULL,NULL,NULL),(7,'Vaccum Cleaner Logbook','vaccum_cleaner_logbook','active',1,21,1586183097,NULL,NULL,NULL),(8,'LAF Pressure Differential Record Sheet','laf_pressure_record','active',2,22,1586255681,NULL,NULL,NULL),(9,'Tablet Tooling Log Card','tablet_tooling_log_card','active',6,23,1586256543,NULL,NULL,NULL),(10,'Balance Calibration Record','balance_calibration_record','active',2,24,1586255697,NULL,NULL,NULL),(11,'Vertical Sampler and Dies Cleaning Usages Log','vertical_sampler_dies_cleaning','inactive',1,25,1586183189,NULL,'2020-04-07 13:09:22','Superadmin'),(12,'Instrument Log Register','instrument_log_register','inactive',1,26,1586183202,NULL,'2020-04-07 13:09:27','Superadmin'),(13,'Equipment/Apparatus Log Register (Friabilator)','equipment_apparatus_log_register','inactive',1,27,1586183228,NULL,'2020-04-07 13:09:17','Superadmin'),(14,'Swab Sample Record','swab_sample_record','inactive',1,28,1586183241,NULL,'2020-04-07 13:09:32','Superadmin'),(15,'Pre-Filter Cleaning Record of LAF','pre_filter_cleaning_record','inactive',1,29,1586183257,NULL,'2020-04-07 13:09:38','Superadmin'),(16,'Line Log','line_log','inactive',1,30,1586183272,NULL,'2020-04-07 13:09:42','Superadmin'),(17,'Equipment Apparatus Leaktest','sf_equipment_apparatus_leaktest','inactive',1,31,1586183284,NULL,'2020-04-07 13:09:10','Superadmin'),(18,'Portable Equipment Log','process_log_portable_equipment','inactive',4,17,1586183472,NULL,'2020-04-07 13:09:54','Superadmin'),(19,'Equipment/Apparatus Log Register (Friabilator)','equipment_apparatus_log_register','active',4,27,1586245213,NULL,NULL,NULL),(20,'Vertical Sampler and Dies Cleaning Usages Log','vertical_sampler_dies_cleaning','active',4,25,1586245229,NULL,NULL,NULL),(21,'Instrument Log Register','instrument_log_register','active',4,26,1586245249,NULL,NULL,NULL),(22,'Pre-Filter Cleaning Record of LAF','pre_filter_cleaning_record','active',4,29,1586245264,NULL,NULL,NULL),(23,'Material Retreival and Relocation Record for Cold Room','material_retreival_relocation_record','active',5,19,1586256523,NULL,NULL,NULL),(24,'Swab Sample Record','swab_sample_record','active',4,28,1586245311,NULL,NULL,NULL),(25,'Equipment Apparatus Leaktest','sf_equipment_apparatus_leaktest','active',4,31,1586245336,NULL,NULL,NULL),(26,'Line Log','line_log','active',3,30,1586245351,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pts_trn_room_log_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_swab_sample_record`
--

DROP TABLE IF EXISTS `pts_trn_swab_sample_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_swab_sample_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `ar_numer` varchar(50) DEFAULT NULL,
  `equipment_id` varchar(50) DEFAULT NULL,
  `equipment_desc` varchar(1000) DEFAULT NULL,
  `pre_product_code` varchar(50) DEFAULT NULL,
  `pre_barch_no` varchar(50) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`),
  KEY `swb_smpl_rcode_index` (`room_code`,`equipment_id`,`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_swab_sample_record`
--

LOCK TABLES `pts_trn_swab_sample_record` WRITE;
/*!40000 ALTER TABLE `pts_trn_swab_sample_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_swab_sample_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_tablet_tooling`
--

DROP TABLE IF EXISTS `pts_trn_tablet_tooling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_tablet_tooling` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_matrial_id` int(11) DEFAULT NULL,
  `doc_no` varchar(20) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `product_code` varchar(200) DEFAULT NULL,
  `act_type` varchar(50) DEFAULT NULL,
  `U` varchar(50) DEFAULT NULL,
  `L` varchar(50) DEFAULT NULL,
  `D` varchar(50) DEFAULT NULL,
  `tablet_tooling_from` varchar(50) DEFAULT NULL,
  `tablet_tooling_to` varchar(50) DEFAULT NULL,
  `damaged` varchar(50) DEFAULT NULL,
  `b_no` varchar(50) DEFAULT NULL,
  `tab_qty` varchar(50) DEFAULT NULL,
  `cum_qty` varchar(50) DEFAULT NULL,
  `cum_qty_punch` varchar(50) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `done_by_user_id` varchar(50) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` varchar(50) DEFAULT NULL,
  `done_by_email` varchar(50) DEFAULT NULL,
  `done_by_remark` varchar(50) DEFAULT NULL,
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`),
  KEY `tblt_tl_index` (`mst_matrial_id`,`room_code`,`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_tablet_tooling`
--

LOCK TABLES `pts_trn_tablet_tooling` WRITE;
/*!40000 ALTER TABLE `pts_trn_tablet_tooling` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_tablet_tooling` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_user_activity_log`
--

DROP TABLE IF EXISTS `pts_trn_user_activity_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_user_activity_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) DEFAULT NULL,
  `roomlogactivity` int(10) DEFAULT NULL,
  `status` enum('active','inactive') DEFAULT 'active',
  `room_id` int(10) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `product_code` varchar(100) NOT NULL,
  `batch_no` varchar(100) NOT NULL,
  `equip_code` varchar(500) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `activity_remarks` varchar(240) DEFAULT NULL,
  `doc_id` varchar(120) DEFAULT NULL,
  `activity_start` int(10) DEFAULT NULL,
  `activity_pause` int(10) DEFAULT NULL,
  `activity_stop` int(10) DEFAULT NULL,
  `workflowstatus` int(10) DEFAULT NULL,
  `workflownextstep` int(10) DEFAULT NULL,
  `pending_apprvl_for_name` varchar(100) DEFAULT NULL,
  `pending_apprvl_for_code` int(11) DEFAULT NULL,
  `is_qa_approve` enum('yes','no') DEFAULT 'no',
  `sampling_rod_id` varchar(100) DEFAULT NULL,
  `dies_no` varchar(100) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_user_activity_log`
--

LOCK TABLES `pts_trn_user_activity_log` WRITE;
/*!40000 ALTER TABLE `pts_trn_user_activity_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_user_activity_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_vaccum_cleaner`
--

DROP TABLE IF EXISTS `pts_trn_vaccum_cleaner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_vaccum_cleaner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doc_no` varchar(50) DEFAULT NULL,
  `room_code` varchar(50) DEFAULT NULL,
  `vaccum_cleaner_code` varchar(500) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `done_by_user_id` int(11) DEFAULT NULL,
  `done_by_user_name` varchar(50) DEFAULT NULL,
  `done_by_role_id` int(11) DEFAULT NULL,
  `done_by_email` varchar(200) DEFAULT NULL,
  `done_by_remark` varchar(500) DEFAULT NULL,
  `created_no` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `is_in_workflow` enum('yes','no') NOT NULL DEFAULT 'no',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_vaccum_cleaner`
--

LOCK TABLES `pts_trn_vaccum_cleaner` WRITE;
/*!40000 ALTER TABLE `pts_trn_vaccum_cleaner` DISABLE KEYS */;
/*!40000 ALTER TABLE `pts_trn_vaccum_cleaner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pts_trn_workflowsteps`
--

DROP TABLE IF EXISTS `pts_trn_workflowsteps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pts_trn_workflowsteps` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `status_id` bigint(20) NOT NULL,
  `activity_id` bigint(20) NOT NULL COMMENT 'Store Activity code from MST_PTS_Activity',
  `workflowtype` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  `next_step` bigint(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(500) NOT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pts_trn_workflowsteps`
--

LOCK TABLES `pts_trn_workflowsteps` WRITE;
/*!40000 ALTER TABLE `pts_trn_workflowsteps` DISABLE KEYS */;
INSERT INTO `pts_trn_workflowsteps` VALUES (1,2,1,'start',1,3,'2020-04-07 07:55:18','Superadmin',NULL,NULL,_binary ''),(2,3,1,'start',2,0,'2020-04-07 07:55:18','Superadmin',NULL,NULL,_binary ''),(3,2,1,'stop',1,3,'2020-04-07 07:55:38','Superadmin',NULL,NULL,_binary ''),(4,3,1,'stop',2,-1,'2020-04-07 07:55:38','Superadmin',NULL,NULL,_binary ''),(5,2,18,'stop',1,3,'2020-04-07 07:56:15','Superadmin',NULL,NULL,_binary ''),(6,3,18,'stop',2,6,'2020-04-07 07:56:15','Superadmin',NULL,NULL,_binary ''),(7,6,18,'stop',5,-1,'2020-04-07 07:56:15','Superadmin',NULL,NULL,_binary ''),(8,2,6,'start',1,3,'2020-04-07 07:56:33','Superadmin',NULL,NULL,_binary ''),(9,3,6,'start',2,0,'2020-04-07 07:56:33','Superadmin',NULL,NULL,_binary ''),(10,2,6,'stop',1,3,'2020-04-07 07:56:52','Superadmin',NULL,NULL,_binary ''),(11,3,6,'stop',2,-1,'2020-04-07 07:56:52','Superadmin',NULL,NULL,_binary '');
/*!40000 ALTER TABLE `pts_trn_workflowsteps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_accessorycleaning`
--

DROP TABLE IF EXISTS `trn_accessorycleaning`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_accessorycleaning` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `document_no` varchar(50) NOT NULL,
  `area_code` varchar(200) DEFAULT NULL,
  `room_code` varchar(200) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `department_name` varchar(50) NOT NULL,
  `product_name` mediumtext NOT NULL,
  `accessorylist` mediumtext,
  `remarks` mediumtext NOT NULL,
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `stop_time` datetime DEFAULT NULL,
  `performed_by` varchar(250) DEFAULT NULL,
  `performed_name` varchar(500) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `checked_name` varchar(500) NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `role_id` int(11) NOT NULL,
  `created_by` varchar(250) DEFAULT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_accessorycleaning`
--

LOCK TABLES `trn_accessorycleaning` WRITE;
/*!40000 ALTER TABLE `trn_accessorycleaning` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_accessorycleaning` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_activityrole`
--

DROP TABLE IF EXISTS `trn_activityrole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_activityrole` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) NOT NULL,
  `activity_typeid` int(20) NOT NULL,
  `activity_id` bigint(20) NOT NULL COMMENT 'Store Code from mst_activitytivitre Ac',
  `block_code` varchar(200) DEFAULT NULL,
  `area_code` varchar(200) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(500) NOT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_activityrole`
--

LOCK TABLES `trn_activityrole` WRITE;
/*!40000 ALTER TABLE `trn_activityrole` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_activityrole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_batch`
--

DROP TABLE IF EXISTS `trn_batch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_batch` (
  `batch_code` varchar(50) NOT NULL,
  `batch_description` varchar(50) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `product_description` varchar(200) DEFAULT NULL,
  `UOM` varchar(20) DEFAULT NULL,
  `batch_size` int(11) DEFAULT NULL,
  `market` varchar(30) DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`batch_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='temporary table for prototype';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_batch`
--

LOCK TABLES `trn_batch` WRITE;
/*!40000 ALTER TABLE `trn_batch` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_batch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_cleaningandsanitization`
--

DROP TABLE IF EXISTS `trn_cleaningandsanitization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_cleaningandsanitization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipcode` varchar(200) DEFAULT NULL,
  `sop_code` varchar(200) DEFAULT NULL,
  `documentno` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `lotno` varchar(200) DEFAULT NULL,
  `strarttime` time DEFAULT NULL,
  `startby` varchar(200) DEFAULT NULL,
  `endtime` time DEFAULT NULL,
  `endby` varchar(200) DEFAULT NULL,
  `checkedby_code` varchar(200) DEFAULT NULL,
  `checkedby_name` varchar(200) DEFAULT NULL,
  `checkedon` datetime DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(200) DEFAULT NULL,
  `created_byname` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_cleaningandsanitization`
--

LOCK TABLES `trn_cleaningandsanitization` WRITE;
/*!40000 ALTER TABLE `trn_cleaningandsanitization` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_cleaningandsanitization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_dailycleaning`
--

DROP TABLE IF EXISTS `trn_dailycleaning`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_dailycleaning` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_no` varchar(50) NOT NULL,
  `area_code` varchar(100) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `sanitizationused` mediumtext,
  `available_quantity` varchar(10) DEFAULT NULL,
  `expiry_date_solution` datetime DEFAULT NULL,
  `required_to_clean` varchar(10) DEFAULT NULL,
  `roomno` varchar(300) DEFAULT NULL,
  `wastebin` int(1) NOT NULL,
  `floorcovering` int(1) DEFAULT NULL,
  `drain_points` mediumtext,
  `checked_by` varchar(250) DEFAULT NULL,
  `checked_name` varchar(500) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_time` datetime DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_dailycleaning`
--

LOCK TABLES `trn_dailycleaning` WRITE;
/*!40000 ALTER TABLE `trn_dailycleaning` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_dailycleaning` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_dpmonitoring`
--

DROP TABLE IF EXISTS `trn_dpmonitoring`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_dpmonitoring` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mat_trans_tbl_id` int(11) DEFAULT NULL,
  `equip_code` varchar(50) DEFAULT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `documentno` varchar(50) DEFAULT NULL,
  `date` datetime NOT NULL,
  `time` time NOT NULL,
  `dpvalue` varchar(200) DEFAULT NULL,
  `ensuredby` varchar(200) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_dpmonitoring`
--

LOCK TABLES `trn_dpmonitoring` WRITE;
/*!40000 ALTER TABLE `trn_dpmonitoring` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_dpmonitoring` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_drainpointdetails`
--

DROP TABLE IF EXISTS `trn_drainpointdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_drainpointdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_no` varchar(50) NOT NULL,
  `area_code` varchar(100) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `sanitizationused` varchar(300) NOT NULL,
  `available_quantity` varchar(10) DEFAULT NULL,
  `expiry_date_solution` datetime DEFAULT NULL,
  `required_to_clean` varchar(10) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `drain_points` int(11) DEFAULT NULL,
  `identification_drainpoints` mediumtext,
  `check_remark` varchar(2000) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `checked_name` varchar(500) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_time` datetime DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_drainpointdetails`
--

LOCK TABLES `trn_drainpointdetails` WRITE;
/*!40000 ALTER TABLE `trn_drainpointdetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_drainpointdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_inj_roomcleaningheader`
--

DROP TABLE IF EXISTS `trn_inj_roomcleaningheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_inj_roomcleaningheader` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `solution_code` varchar(200) DEFAULT NULL,
  `room_code` varchar(20) DEFAULT NULL,
  `cleaning_activity_type` varchar(20) DEFAULT NULL,
  `lot_no` varchar(20) DEFAULT NULL,
  `starttime` time DEFAULT NULL,
  `endtime` time DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  `checkedby_code` varchar(200) DEFAULT NULL,
  `checkedby_name` varchar(200) DEFAULT NULL,
  `checkedon` time DEFAULT NULL,
  `createdby_name` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(200) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_inj_roomcleaningheader`
--

LOCK TABLES `trn_inj_roomcleaningheader` WRITE;
/*!40000 ALTER TABLE `trn_inj_roomcleaningheader` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_inj_roomcleaningheader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_injequipmentdetails`
--

DROP TABLE IF EXISTS `trn_injequipmentdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_injequipmentdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lot_no` varchar(1000) DEFAULT NULL,
  `sop_no` varchar(1000) DEFAULT NULL,
  `equipmentname` varchar(1000) DEFAULT NULL,
  `code_no` varchar(1000) DEFAULT NULL,
  `cleaning_status` varchar(50) DEFAULT NULL,
  `checked_on` datetime DEFAULT CURRENT_TIMESTAMP,
  `checked_by` varchar(1000) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_injequipmentdetails`
--

LOCK TABLES `trn_injequipmentdetails` WRITE;
/*!40000 ALTER TABLE `trn_injequipmentdetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_injequipmentdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_injfiltrationdetails`
--

DROP TABLE IF EXISTS `trn_injfiltrationdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_injfiltrationdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) DEFAULT NULL,
  `lot_no` varchar(1000) DEFAULT NULL,
  `pre_limit` varchar(500) DEFAULT NULL,
  `collection_fromtime` time DEFAULT NULL,
  `collection_totime` time DEFAULT NULL,
  `post_limit` varchar(500) DEFAULT NULL,
  `checked_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `checked_by` varchar(500) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_injfiltrationdetails`
--

LOCK TABLES `trn_injfiltrationdetails` WRITE;
/*!40000 ALTER TABLE `trn_injfiltrationdetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_injfiltrationdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_injpar64details`
--

DROP TABLE IF EXISTS `trn_injpar64details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_injpar64details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipmentcode` varchar(1000) DEFAULT NULL,
  `sop_no` varchar(500) DEFAULT NULL,
  `cleaning_start` datetime DEFAULT NULL,
  `cleaning_stop` datetime DEFAULT NULL,
  `clean_by` varchar(500) DEFAULT NULL,
  `cleanstop_by` varchar(500) DEFAULT NULL,
  `clean_remarks` varchar(1000) DEFAULT NULL,
  `clean_checkby` varchar(500) DEFAULT NULL,
  `op_productname` varchar(1000) DEFAULT NULL,
  `op_lotno` varchar(1000) DEFAULT NULL,
  `op_output` varchar(500) DEFAULT NULL,
  `op_start` datetime DEFAULT NULL,
  `op_stop` datetime DEFAULT NULL,
  `op_by` varchar(500) DEFAULT NULL,
  `opstop_by` varchar(500) DEFAULT NULL,
  `op_remarks` varchar(1000) DEFAULT NULL,
  `op_checkby` varchar(500) DEFAULT NULL,
  `break_fromtime` datetime DEFAULT NULL,
  `break_totime` datetime DEFAULT NULL,
  `break_remarks` varchar(1000) DEFAULT NULL,
  `break_checkby` varchar(500) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_injpar64details`
--

LOCK TABLES `trn_injpar64details` WRITE;
/*!40000 ALTER TABLE `trn_injpar64details` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_injpar64details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_injsolutiondetails`
--

DROP TABLE IF EXISTS `trn_injsolutiondetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_injsolutiondetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) DEFAULT NULL,
  `doc_id` varchar(50) DEFAULT NULL,
  `solutionname` varchar(300) NOT NULL,
  `required_quantity` varchar(50) DEFAULT NULL,
  `standard_water_qty` varchar(50) DEFAULT NULL,
  `Standard_hydrogen_qty` varchar(1000) DEFAULT NULL,
  `wfi_makeup_volume` varchar(1000) DEFAULT NULL,
  `actual_water_qty` varchar(500) DEFAULT NULL,
  `arn_water` varchar(500) DEFAULT NULL,
  `uom_water` varchar(50) DEFAULT NULL,
  `actual_hydrogen_qty` varchar(500) DEFAULT NULL,
  `arn_hydrogen` varchar(500) DEFAULT NULL,
  `uom_hydrogen` varchar(50) DEFAULT NULL,
  `actual_wfi_makeup_volume` varchar(500) DEFAULT NULL,
  `arn_wfi` varchar(500) DEFAULT NULL,
  `uom_wfi` varchar(50) DEFAULT NULL,
  `expiry_datatime` datetime DEFAULT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stop_by` varchar(500) DEFAULT NULL,
  `stop_on` datetime DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_injsolutiondetails`
--

LOCK TABLES `trn_injsolutiondetails` WRITE;
/*!40000 ALTER TABLE `trn_injsolutiondetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_injsolutiondetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_injsterilizationdetails`
--

DROP TABLE IF EXISTS `trn_injsterilizationdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_injsterilizationdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) DEFAULT NULL,
  `equipmentname` varchar(1000) DEFAULT NULL,
  `code_no` varchar(1000) DEFAULT NULL,
  `sterilizer_no` varchar(1000) DEFAULT NULL,
  `run_no` int(11) DEFAULT NULL,
  `ref_graph_no` varchar(1000) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `from_time` time DEFAULT NULL,
  `to_time` time DEFAULT NULL,
  `checked_by` varchar(1000) DEFAULT NULL,
  `is_active` int(11) DEFAULT '1',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_injsterilizationdetails`
--

LOCK TABLES `trn_injsterilizationdetails` WRITE;
/*!40000 ALTER TABLE `trn_injsterilizationdetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_injsterilizationdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_inprocesscontainercleaning`
--

DROP TABLE IF EXISTS `trn_inprocesscontainercleaning`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_inprocesscontainercleaning` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_no` varchar(50) NOT NULL,
  `area_code` varchar(100) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `batch_no` varchar(100) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `sop_no` varchar(100) DEFAULT NULL,
  `equipment_code` mediumtext,
  `remarks` mediumtext,
  `checked_by` varchar(250) DEFAULT NULL,
  `checked_name` varchar(500) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_time` datetime DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_inprocesscontainercleaning`
--

LOCK TABLES `trn_inprocesscontainercleaning` WRITE;
/*!40000 ALTER TABLE `trn_inprocesscontainercleaning` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_inprocesscontainercleaning` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_material_transfer_dtl`
--

DROP TABLE IF EXISTS `trn_material_transfer_dtl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_material_transfer_dtl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mat_eqip_dtl` varchar(2000) DEFAULT NULL,
  `mat_eqip_rec_transfer_FT` time DEFAULT NULL,
  `mat_eqip_rec_transfer_TT` time DEFAULT NULL,
  `sanitization_agent` varchar(200) DEFAULT NULL,
  `performedby` varchar(200) DEFAULT NULL,
  `checkedby_code` varchar(200) DEFAULT NULL,
  `checkedby_name` varchar(200) DEFAULT NULL,
  `checkedon` date DEFAULT NULL,
  `remark` varchar(1000) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_material_transfer_dtl`
--

LOCK TABLES `trn_material_transfer_dtl` WRITE;
/*!40000 ALTER TABLE `trn_material_transfer_dtl` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_material_transfer_dtl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_operation_workflow_approval_records`
--

DROP TABLE IF EXISTS `trn_operation_workflow_approval_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_operation_workflow_approval_records` (
  `id` int(11) NOT NULL,
  `header_id` int(11) DEFAULT NULL,
  `doneby` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `roleid` varchar(50) DEFAULT NULL,
  `time` datetime NOT NULL,
  `type` varchar(50) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `remark` varchar(2000) DEFAULT NULL,
  `filepath` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_operation_workflow_approval_records`
--

LOCK TABLES `trn_operation_workflow_approval_records` WRITE;
/*!40000 ALTER TABLE `trn_operation_workflow_approval_records` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_operation_workflow_approval_records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_operationequipmentdetaillog`
--

DROP TABLE IF EXISTS `trn_operationequipmentdetaillog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_operationequipmentdetaillog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipmentheader_table_id` int(11) DEFAULT NULL,
  `header_id` int(11) DEFAULT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `equipment_type` varchar(50) DEFAULT NULL,
  `strattime` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  `memo_number` varchar(250) DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `action_type` varchar(250) DEFAULT NULL,
  `performed_by` varchar(250) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_operationequipmentdetaillog`
--

LOCK TABLES `trn_operationequipmentdetaillog` WRITE;
/*!40000 ALTER TABLE `trn_operationequipmentdetaillog` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_operationequipmentdetaillog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_operationequipmentheaderlog`
--

DROP TABLE IF EXISTS `trn_operationequipmentheaderlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_operationequipmentheaderlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `header_id` int(11) DEFAULT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_operationequipmentheaderlog`
--

LOCK TABLES `trn_operationequipmentheaderlog` WRITE;
/*!40000 ALTER TABLE `trn_operationequipmentheaderlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_operationequipmentheaderlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_operationequipmentlog`
--

DROP TABLE IF EXISTS `trn_operationequipmentlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_operationequipmentlog` (
  `id` int(11) NOT NULL,
  `header_id` int(11) DEFAULT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `equipment_type` varchar(50) DEFAULT NULL,
  `sop_code` varchar(250) DEFAULT NULL,
  `brkdwn_strattime` datetime DEFAULT NULL,
  `brkdwn_endtime` datetime DEFAULT NULL,
  `brkdwn_checked_by` varchar(250) DEFAULT NULL,
  `memo_number` varchar(250) DEFAULT NULL,
  `remark` varchar(2000) DEFAULT NULL,
  `performed_by` varchar(250) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `isactive` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_operationequipmentlog`
--

LOCK TABLES `trn_operationequipmentlog` WRITE;
/*!40000 ALTER TABLE `trn_operationequipmentlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_operationequipmentlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_operationlogdetail`
--

DROP TABLE IF EXISTS `trn_operationlogdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_operationlogdetail` (
  `id` bigint(20) NOT NULL,
  `header_id` varchar(50) DEFAULT NULL,
  `start_time` varchar(50) DEFAULT NULL,
  `end_time` varchar(50) DEFAULT NULL,
  `checked_by_time` datetime DEFAULT NULL,
  `performed_by` varchar(250) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(50) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_operationlogdetail`
--

LOCK TABLES `trn_operationlogdetail` WRITE;
/*!40000 ALTER TABLE `trn_operationlogdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_operationlogdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_operationlogheader`
--

DROP TABLE IF EXISTS `trn_operationlogheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_operationlogheader` (
  `id` bigint(20) NOT NULL,
  `document_no` varchar(50) NOT NULL,
  `lot_no` varchar(50) DEFAULT NULL,
  `area_code` varchar(50) NOT NULL,
  `room_code` varchar(50) NOT NULL,
  `batch_no` varchar(50) NOT NULL,
  `activity_code` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `next_step` int(11) DEFAULT NULL,
  `operation_type` varchar(200) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_operationlogheader`
--

LOCK TABLES `trn_operationlogheader` WRITE;
/*!40000 ALTER TABLE `trn_operationlogheader` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_operationlogheader` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trn_operationheader_insert_audit_log` AFTER INSERT ON `trn_operationlogheader` FOR EACH ROW Begin
INSERT INTO operationheader_audit_log (id,tablename,tableid,action,createdon,createdby)
	  VALUES(null,'operationlogheader',New.id,'inserted',NOW(),New.created_by);
End */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `trn_operationheader_update_audit_log` AFTER UPDATE ON `trn_operationlogheader` FOR EACH ROW Begin
INSERT INTO operationheader_audit_log (id,tablename,tableid,action,createdon,createdby)
	  VALUES(null,'operationlogheader',New.id,'updated',NOW(),New.created_by);
End */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `trn_portabledetail`
--

DROP TABLE IF EXISTS `trn_portabledetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_portabledetail` (
  `id` bigint(20) NOT NULL,
  `document_no` varchar(50) DEFAULT NULL,
  `department_name` varchar(50) DEFAULT NULL,
  `batch_code` varchar(100) DEFAULT NULL,
  `area_code` varchar(100) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `performed_by` varchar(250) DEFAULT NULL,
  `performed_name` varchar(500) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `checked_name` varchar(500) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `role_id` int(11) NOT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `checked_on` datetime DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `stoptime` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_portabledetail`
--

LOCK TABLES `trn_portabledetail` WRITE;
/*!40000 ALTER TABLE `trn_portabledetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_portabledetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_portableequipmentdetail`
--

DROP TABLE IF EXISTS `trn_portableequipmentdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_portableequipmentdetail` (
  `id` bigint(20) NOT NULL,
  `document_portable_id` varchar(50) DEFAULT NULL,
  `document_no` varchar(50) DEFAULT NULL,
  `equipment_code` varchar(50) DEFAULT NULL,
  `equipment_type` varchar(300) DEFAULT NULL,
  `sop_code` varchar(50) DEFAULT NULL,
  `changepart` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_portableequipmentdetail`
--

LOCK TABLES `trn_portableequipmentdetail` WRITE;
/*!40000 ALTER TABLE `trn_portableequipmentdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_portableequipmentdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_roomcleaning_detail`
--

DROP TABLE IF EXISTS `trn_roomcleaning_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_roomcleaning_detail` (
  `id` bigint(20) NOT NULL,
  `header_id` bigint(20) DEFAULT NULL,
  `cleaning_activity_code` varchar(200) DEFAULT NULL,
  `status` varchar(200) DEFAULT NULL,
  `created_by` varchar(200) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(200) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_roomcleaning_detail`
--

LOCK TABLES `trn_roomcleaning_detail` WRITE;
/*!40000 ALTER TABLE `trn_roomcleaning_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_roomcleaning_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_solutiondetails`
--

DROP TABLE IF EXISTS `trn_solutiondetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_solutiondetails` (
  `id` int(11) NOT NULL,
  `doc_id` varchar(50) NOT NULL,
  `area_code` varchar(200) DEFAULT NULL,
  `room_code` varchar(100) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `next_step` int(1) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `solutionname` varchar(300) NOT NULL,
  `required_quantity` varchar(50) DEFAULT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `qcar_no` varchar(50) DEFAULT NULL,
  `standard_solution_qty` varchar(50) DEFAULT NULL,
  `purified_water` varchar(1000) DEFAULT NULL,
  `makeup_volume` varchar(1000) DEFAULT NULL,
  `actual_solution_qty` varchar(500) DEFAULT NULL,
  `actual_purified_water` varchar(500) DEFAULT NULL,
  `solution_valid_upto` varchar(500) DEFAULT NULL,
  `expiry_datatime` datetime DEFAULT NULL,
  `check_remark` varchar(2000) DEFAULT NULL,
  `solution_destroy_checked_by` varchar(250) DEFAULT NULL,
  `solution_destroy_checked_name` varchar(500) DEFAULT NULL,
  `checked_on` datetime DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `role_id` int(11) NOT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_name` varchar(500) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_name` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `rejection_remarks` text NOT NULL,
  `qc_filename` varchar(500) DEFAULT NULL,
  `qc_approveremarks` text,
  `is_destroy` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_solutiondetails`
--

LOCK TABLES `trn_solutiondetails` WRITE;
/*!40000 ALTER TABLE `trn_solutiondetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_solutiondetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_solutionlog`
--

DROP TABLE IF EXISTS `trn_solutionlog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_solutionlog` (
  `id` int(11) NOT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `qcar_no` varchar(50) DEFAULT NULL,
  `standard_solution_qty` varchar(50) DEFAULT NULL,
  `purified_water` varchar(1000) DEFAULT NULL,
  `makeup_volume` varchar(1000) DEFAULT NULL,
  `actual_solution_qty` varchar(500) DEFAULT NULL,
  `actual_purified_water` varchar(500) DEFAULT NULL,
  `solution_valid_upto` varchar(500) DEFAULT NULL,
  `check_remark` varchar(2000) DEFAULT NULL,
  `solution_destroy_checked_by` varchar(250) DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_solutionlog`
--

LOCK TABLES `trn_solutionlog` WRITE;
/*!40000 ALTER TABLE `trn_solutionlog` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_solutionlog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_solutionstock`
--

DROP TABLE IF EXISTS `trn_solutionstock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_solutionstock` (
  `id` int(11) NOT NULL,
  `document_no` varchar(250) DEFAULT NULL,
  `solutionname` varchar(250) DEFAULT NULL,
  `available_qty` int(11) DEFAULT NULL,
  `expiry_date` datetime DEFAULT NULL,
  `is_active` int(1) DEFAULT '0',
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_solutionstock`
--

LOCK TABLES `trn_solutionstock` WRITE;
/*!40000 ALTER TABLE `trn_solutionstock` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_solutionstock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_weeklydrainpointdetails`
--

DROP TABLE IF EXISTS `trn_weeklydrainpointdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_weeklydrainpointdetails` (
  `id` int(11) NOT NULL,
  `document_no` varchar(50) NOT NULL,
  `department_name` varchar(100) NOT NULL,
  `sanitizationused` varchar(300) NOT NULL,
  `batch_no` varchar(50) DEFAULT NULL,
  `drain_points` int(11) DEFAULT NULL,
  `identification_drainpoints` mediumtext,
  `check_remark` varchar(2000) DEFAULT NULL,
  `checked_by` varchar(250) DEFAULT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1',
  `start_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `end_time` datetime DEFAULT NULL,
  `created_by` varchar(250) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` varchar(250) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_weeklydrainpointdetails`
--

LOCK TABLES `trn_weeklydrainpointdetails` WRITE;
/*!40000 ALTER TABLE `trn_weeklydrainpointdetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_weeklydrainpointdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trn_workflowsteps`
--

DROP TABLE IF EXISTS `trn_workflowsteps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trn_workflowsteps` (
  `id` bigint(20) NOT NULL,
  `status_id` bigint(20) NOT NULL,
  `activity_id` bigint(20) NOT NULL COMMENT 'Store Activity code from mst_activity',
  `role_id` bigint(20) NOT NULL,
  `alert_roleid` bigint(20) DEFAULT NULL,
  `alert_message` text,
  `next_step` bigint(20) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(500) NOT NULL,
  `modified_by` varchar(500) DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `is_active` bit(1) NOT NULL DEFAULT b'1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trn_workflowsteps`
--

LOCK TABLES `trn_workflowsteps` WRITE;
/*!40000 ALTER TABLE `trn_workflowsteps` DISABLE KEYS */;
/*!40000 ALTER TABLE `trn_workflowsteps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `getstart_stoped_data`
--

/*!50001 DROP VIEW IF EXISTS `getstart_stoped_data`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `getstart_stoped_data` AS select `oph`.`status` AS `status`,`oph`.`area_code` AS `area_code`,`oph`.`room_code` AS `room_code`,`s`.`status_name` AS `status_name`,`b`.`product_description` AS `product_description`,`oph`.`document_no` AS `document_no`,`oph`.`batch_no` AS `batch_no`,`oph`.`lot_no` AS `lot_no`,`oph`.`created_on` AS `created_on`,`oph`.`operation_type` AS `operation_type`,(select `mst_employee`.`emp_name` from `mst_employee` where (`mst_employee`.`emp_code` = `oph`.`created_by`)) AS `created_by` from ((`trn_operationlogheader` `oph` join `mst_status` `s` on((`s`.`id` = `oph`.`status`))) join `trn_batch` `b` on((`oph`.`batch_no` = `b`.`batch_code`))) order by `oph`.`created_on` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `getstatusrecord`
--

/*!50001 DROP VIEW IF EXISTS `getstatusrecord`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `getstatusrecord` AS select `war`.`id` AS `warid`,`war`.`header_id` AS `header_id`,`war`.`status` AS `status`,`oph`.`area_code` AS `area_code`,`oph`.`room_code` AS `room_code`,`s`.`status_name` AS `status_name`,`b`.`product_description` AS `product_description`,`oph`.`document_no` AS `document_no`,`oph`.`batch_no` AS `batch_no`,`oph`.`lot_no` AS `lot_no`,`oph`.`created_on` AS `created_on`,`oph`.`operation_type` AS `operation_type`,`war`.`doneby` AS `doneby`,`war`.`name` AS `actionby`,`war`.`roleid` AS `roleid`,`war`.`time` AS `actiontime`,`war`.`type` AS `type` from (((`trn_operationlogheader` `oph` join `trn_operation_workflow_approval_records` `war` on((`oph`.`id` = `war`.`header_id`))) join `mst_status` `s` on((`s`.`id` = `war`.`status`))) join `trn_batch` `b` on((`oph`.`batch_no` = `b`.`batch_code`))) order by `oph`.`created_on` desc */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `operationsqnlog`
--

/*!50001 DROP VIEW IF EXISTS `operationsqnlog`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `operationsqnlog` AS select `a`.`id` AS `id`,`a`.`document_no` AS `document_no`,`a`.`lot_no` AS `lot_no`,`a`.`area_code` AS `area_code`,`a`.`room_code` AS `room_code`,`a`.`batch_no` AS `batch_no`,`e`.`product_description` AS `product_description`,`a`.`operation_type` AS `operation_type`,min(`b`.`start_time`) AS `start_time`,max(`b`.`end_time`) AS `end_time`,(select `mst_employee`.`emp_name` from `mst_employee` where (`mst_employee`.`emp_name` = min(`b`.`performed_by`))) AS `started_by`,(select `mst_employee`.`emp_name` from `mst_employee` where (`mst_employee`.`emp_name` = max(`b`.`performed_by`))) AS `ended_by`,group_concat(distinct concat(convert(`b`.`performed_by` using utf8)) separator ',') AS `names`,group_concat(distinct concat(convert(`ar`.`name` using utf8)) separator ',') AS `approvedby`,group_concat(distinct concat(convert(`ar`.`time` using utf8)) separator ',') AS `approvedon`,group_concat(distinct concat(convert(`c`.`equipment_code` using utf8),'###',`d`.`equipment_name`,'###',`d`.`equipment_type`,'###',convert(`c`.`sop_code` using utf8)) separator ',') AS `equipments` from ((((((`trn_operationlogheader` `a` join `trn_operationlogdetail` `b` on((`a`.`id` = `b`.`header_id`))) join `trn_operationequipmentheaderlog` `c` on((`a`.`id` = `c`.`header_id`))) join `mst_equipment` `d` on((convert(`c`.`equipment_code` using utf8) = `d`.`equipment_code`))) join `mst_sop` `sop` on((convert(`c`.`sop_code` using utf8) = `sop`.`sop_code`))) join `trn_batch` `e` on((`a`.`batch_no` = `e`.`batch_code`))) join `trn_operation_workflow_approval_records` `ar` on((`a`.`id` = `ar`.`header_id`))) where (`ar`.`type` = 'Apprved') group by `a`.`id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-07 17:35:49
