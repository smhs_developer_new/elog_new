<center>
   <div class="wrapper">
      <div class="abs-center wd-xl" style="position: fixed;">
         <!-- START card-->
         <div class="text-center mb-12">
            <div class="mb-3"><em class="fa fa-wrench fa-5x text-muted"></em></div>
            <div class="text-lg mb-12">Error In Database</div>
            
            <p><?=$message?></p>
         </div>
         <!--<ul class="list-inline text-center text-sm mb-4" hidden>
            <li class="list-inline-item"><a class="text-muted" href="dashboard.html">Go to App</a></li>
            <li class="text-muted list-inline-item">|</li>
            <li class="list-inline-item"><a class="text-muted" href="login.html">Login</a></li>
            <li class="text-muted list-inline-item">|</li>
            <li class="list-inline-item"><a class="text-muted" href="register.html">Register</a></li>
         </ul>
         <div class="p-3 text-center"><span class="mr-2">&copy;</span><span>2019</span><span class="mr-2"> -</span><span>SMHS</span></div>-->
      </div>
   </div>
</center>
   <!-- =============== VENDOR SCRIPTS ===============-->
   <!-- MODERNIZR-->
   <script src="<?= base_url()?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API-->
   <script src="<?= base_url()?>vendor/js-storage/js.storage.js"></script><!-- i18next-->
   <script src="<?= base_url()?>vendor/i18next/i18next.js"></script>
   <script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- JQUERY-->
   <script src="<?= base_url()?>vendor/jquery/dist/jquery.js"></script><!-- BOOTSTRAP-->
   <script src="<?= base_url()?>vendor/popper.js/dist/umd/popper.js"></script>
   <script src="<?= base_url()?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- PARSLEY-->
   <script src="<?= base_url()?>vendor/parsleyjs/dist/parsley.js"></script><!-- =============== APP SCRIPTS ===============-->
   <script src="js/app.js"></script>
</body>

</html>