




  <!-- Main section-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Role Activity Mapping</div>
        <!--<form class="search-form col-sm-5 pl-0">
          <em class="icon-magnifier"></em>
          <input class="form-control" type="email" placeholder="Enter room code or number on name">
        </form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto hide-on-mob">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Activity/activityMapping">Activity Mapping</a></li>
          </ol>
          <div class="humburger" id="myTopnav"> <em class="fa-2x icon-menu mr-2" id="icon"></em>
            <div class="main-menu"> <a href="Home2.html">Home</a> <a href="#">Products</a> <a href="#">XYZ</a> <a href="#">Features</a> </div>
          </div>
        </div>
      </div>
      <div class="card2 card card-default mt-4" hidden>
        <div class="card-body">
          <div class="row">
            <div class="col-6 offset-6 text-right">
              <label class="pr-3">Select Role</label>
              <div class="btn-group">
                <button class="btn btn-secondary" type="button"><em class="f18 mr-2 fas fa-plus"></em></button>
                <button class="btn dropdown-toggle btn-secondary" type="button" data-toggle="dropdown" aria-expanded="false"> <span class="sr-only">secondary</span></button>
                <div class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, 33px, 0px);"> <a class="dropdown-item" href="#" id="optr1">Operator</a> <a class="dropdown-item" href="#" id="area1">Area</a> <a class="dropdown-item" href="#" id="prodct1">Production</a> <a class="dropdown-item" href="#" id="quty1">Quality</a> <a class="dropdown-item" href="#" id="qtlcheck">Quality Check</a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <form id="activity-map">
      <div class="card2 card card-default mt-4">
        <div class="card-body">

<div class="form-row mt-3">
<div class="col-lg-4 mb-3"><label for="block">Block <span style="color: red">*</span></label>
<select class="form-control" id="block" name="block" required>
  <option value="" disabled selected>Select Block</option>
  <?php
  foreach ($blocks as $block) {
	
		$ss = $block['block_code']== $blockselected?"Selected":"" ;
	  
	
    ?>
    <option value="<?= $block['block_code']?>" <?=$ss?>><?= $block["block_code"]?></option>
    <?php
  }
  ?>
</select>  
</div>


<!--<div class="col-lg-4 mb-3"><label for="subblock">Sub Block <span style="color: red">*</span></label>
<select class="form-control" id="subblock" name="subblock" required>
 <option value="" disabled selected>Select Sub Block</option>
<?php
if(isset($blockselected))
 {
	 foreach ($subblocks as $subblock) {
	 $ss = $subblock['department_code']== $subblockselected?"Selected":"" ;
?>
    <option value="<?= $subblock['department_code']?>" <?=$ss?>><?= $subblock["department_code"]?></option>
<?php
  }}
 ?>


</select>  
</div>


<div class="col-lg-4 mb-3"><label for="area">Area <span style="color: red">*</span></label>
<select class="form-control" id="area" name="area" required>
<option value="" disabled selected>Select Area</option>
  <?php
if(isset($subblockselected))
 {
	 foreach ($areas as $area) {
	 $ss = $area['area_code']== $areaselected?"Selected":"" ;
?>
    <option value="<?= $area['area_code']?>" <?=$ss?>><?= $area['area_code']?></option>
<?php
  }}
 ?>
</select>

</div>                                 
</div> -->
<div id = "divToReload_WithDAta">
 
<?php
//print_r($selected["rid"]);
$this->load->model('Activitymodel');
//print_r($activitySelect);
?>

          <?php 
          $i=1;
          foreach($role as $as){
           ?>
          <div class="row mt-3">
            <input type="text" name="roles[]" value="<?php echo $as['id']; ?>" hidden>
            <div class="col-2 f16"><?php echo $as['role_description']; ?></div>
            <div class="col-10">
              <div class="form-group row">
                <div class="col-md-12">
                  <select class="nselect form-control" multiple="multiple" name="activity<?= $i?>[]" style="width: 100%">
                    <?php
                    //foreach ($select as $s) {
                    $activitySelect = $this->Activitymodel->sA($as['id']);
                      

                        
                      
                    foreach ($activity as $act) {
                      //$ss = "selected";
                     
                    
                       $ss = "";
                       foreach ($activitySelect["ndata"] as $kv) {
                      if($kv["activity_typeid"]==$act['activitytype_id'] && $kv["activity_id"]==$act['activity_code'] && $act["area_code"]!='A-000'){
                      $ss = "selected";
                     }
                     }

                    ?>
                    <option value="<?php echo $act['activity_code']."##".$act['activitytype_id']."##".$act['area_code'] ?>" <?= $ss?>><?php echo $act['activity_name']."|".$act['activity_type']; ?></option>
                    <?php
                    
                    }
                     //}
                    //}
                    //}
                    
                    ?>
                  </select>
                </div>
              </div>
            </div>
         <!-- <div class="col-1"><em class="fa-2x mr-2 far fa-window-close pointer redd" id="clse"></em></div> -->
          </div>
                <?php 
                $i++;
                } 
                ?>

        
        </div>
      </div>
      
      <div class="card2 card card-default mt-5">
        <div class="card-body">
          <div class="row">
            <div class="col-12 offset-5">
              <button class="btn btn-square btn-primary mr-3" type="submit">Map</button>
              <!--<button class="btn btn-square btn-danger" type="button">Cancel</button>-->
            </div>
          </div>
        </div>
      </div>

      </form>


    </div>
	</div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>

<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 
<script src="<?= base_url()?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?= base_url()?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?= base_url()?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?= base_url()?>vendor/i18next/i18next.js"></script> 
<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?= base_url()?>vendor/jquery/dist/jquery.js"></script>
<script src="<?= base_url()?>vendor/jquery/dist/jquery.redirect.js"></script>

<script src="<?= base_url()?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?= base_url()?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<script src="<?= base_url()?>vendor/chosen-js/chosen.jquery.js"></script><!-- SLIDER CTRL--> 

<!-- =============== APP SCRIPTS ===============--> 
<script src="<?= base_url()?>js/app.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>
<script src="jquery.redirect.js"></script>
<script type="text/javascript" src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>
      <link rel="stylesheet" type="text/css" href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css">

<script type="text/javascript">
$(document).ready(function(){

$("#block1").change(function(){
var block = $(this).val();
//alert(b);
//subblock

$.ajax({
    url: "<?= base_url()?>Activity/getSubblocks",
    type: 'POST',
    data: {block:block},
    success: function(res) {
        console.log(res);
        //alert(res);
        var op = "<option value='' selected disabled>Select Sub Block</option>";
        $.each(res, function (index, value) {
        //console.log(value.department_code);
        op = op.concat("<option value='"+value.department_code+"'>"+value.department_code+"</option>");
        });
        $("#subblock").html(op);
		var op = "<option value='' selected disabled>Select Area</option>";
		$("#area").html(op);
		$("#area").attr("disabled",true);
    }
});

});


$("#subblock1").change(function(){
var subblock = $(this).val();
$.ajax({
    url: "<?= base_url()?>Activity/getAreas",
    type: 'POST',
    data: {subblock:subblock},
    success: function(res) {
        //console.log(res);
        //alert(res);
        var op = "<option value='' selected disabled>Select Area</option>";
        $.each(res, function (index, value) {
        //console.log(value.department_code);
        op = op.concat("<option value='"+value.area_code+"'>"+value.area_code+"</option>");
        });
        $("#area").html(op);
		$("#area").attr("disabled",false);
    }
});
});


$("#block").change(function(){

$.redirect("<?= base_url()?>Activity/activityMapping",
  {
    //area:$(this).val(),
    block:$("#block").val(),
    //subblock:$("#subblock").val()
  }, "POST","_self");

});


$('.nselect').select2({
    placeholder: 'Select Activity'
});

//$(".chosen-select").chosen().val();


//$("select").trigger('chosen:updated');

  $('#activity-map').submit(function(){

  //$("select").trigger('chosen:updated');
  //$('option').prop('selected', true); 
    $.ajax({
      type: 'POST',
      url: '<?= base_url()?>Activity/activityMap',
      data: $(this).serialize() // getting filed value in serialize form
    })
    .done(function(details){ // if getting done then call.
    console.log(details);
      if(details.status==1){
        $("#myb").click();
        $("#head").html(details.msgheader);
        $("#message").html(details.msg);
      }
      else{
        alert("No");    
      }
      
    })
    .fail(function(details) { // if fail then getting message
      // just in case posting your form failed
      alert( "Posting failed." );
    });

// to prevent refreshing the whole page page
return false;

});




});  
</script>

<script>
$(".chosen-select").chosen();
$('button').click(function(){
        $(".chosen-select").val('').trigger("chosen:updated");
});
// Show div
$("#optr1").click(function(){
	$("#optr").show();
	});
	$("#area1").click(function(){
	$("#area").show();
	});
	$("#prodct1").click(function(){
	$("#prodtn").show();
	});
	$("#quty1").click(function(){
	$("#qlty").show();
	});
	$("#qtlcheck").click(function(){
	$("#qltycheck").show();
	});
// Hide Div	
$("#clse").click(function(){
	$("#optr").hide();
	});
	$("#clse1").click(function(){
	$("#area").hide();
	});
	$("#clse2").click(function(){
	$("#prodtn").hide();
	});
	$("#clse3").click(function(){
	$("#qlty").hide();
	});
	$("#clse4").click(function(){
	$("#qltycheck").hide();
	});
</script>

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="head"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="message"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <a href="" class="btn btn-primary" id="disclose">Ok</a>
      </div>
    </div>

  </div>
</div>
</body>
</html>