
 <!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container">
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Disinfectant Solution Preparation Record</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Injectable/injectable">Injectable</a></li>
          </ol>
        </div>
      </div>

<form id="equipmentcleaning">
<div class="card card-default">
        <div class="card-body">
			<div class="form-row">
			<div class="col-lg-6 mb-3"><label for="lotno">Lot Number of Solution <span style="color: red">*</span></label><input class="form-control" id="lotno" type="text" placeholder="Lot No" name="lotno" required>
			</div>
			<div class="col-lg-6 mb-3"><label for="sopno">Reference SOP No <span style="color: red">*</span></label><input class="form-control" id="sopno" type="text" placeholder="PAR-022" value="PAR-022" name="sopno" readonly required>
			</div>
	 		</div>
		</div>
</div>

<h4>Equipment Cleaning</h4>

<div class="card card-default">
        <div class="card-body">
			<div class="form-row">
			<div class="col-lg-6 mb-3"><label for="equipmentname">Equipment <span style="color: red">*</span></label><input class="form-control autocomplete" id="equipmentname" type="text" placeholder="Equipment Name" name="equipmentname" required>
			</div>
			<div class="col-lg-2 mb-3"><label for="codeno">Code No <span style="color: red">*</span></label><input class="form-control" id="codeno" type="text" placeholder="Code No" name="codeno" required readonly>
			</div>
			<div class="col-lg-2 mb-3"><label for="cleaningstatus">Cleaning Status <span style="color: red">*</span></label>
				<select class="form-control" name="cleaningstatus" id="cleaningstatus" required>
          <option value="" disabled selected>Select Status</option>
					<option value="Cleaned">Cleaned</option>
					<option value="Un-Cleaned">Un-Cleaned</option>
				</select>
			</div>
			<div class="col-lg-2 mb-3">
				<br>
        <!-- type="submit"  -->
				<button class="btn btn-lg btn-warning" id="validationnow" type="submit" style="width: 100%">Check By</button>
			</div>
	 		</div>
		</div>
</div>
</form>

<form id="preparation">
<h4>Preparation</h4>
<div class="card card-default">
        <div class="card-body">
          
			<div class="form-row">
			<div class="col-lg-4 mb-3"><label for="solutionname">Solution Name <span style="color: red">*</span></label><input class="form-control autocomplete" id="solutionname" type="text" placeholder="e.g. Preparation of 5.0% v/v Hemtop Solution" name="solutionname" required>
			</div>
			<div class="col-lg-4 mb-3"><label for="requiredquantity">Required Quantity (in ml)<span style="color: red">*</span></label><input class="form-control" id="requiredquantity" type="number" min="1" placeholder="Quantity" name="requiredquantity" required>
			</div>
			<div class="col-lg-4 mb-3"><label for="ubdatetime">Used Before Date & Time <span style="color: red">*</span></label>

        <input class="form-control" id="ubdatetime" type="datetime-local" placeholder="date & time" name="ubdatetime">

			</div>
	 		</div>

	 		<div class="form-row">
               <div class="col-md-6">
                  <p><b>Standard Quantity</b></p>
                           <div class="form-group"><label>Water For Injection (ml)<span style="color: red">*</span></label><input class="form-control" type="text" placeholder="8000 ml" id="sqwfi" name="sqwfi" readonly required></div>
                           <div class="form-group"><label>Hydrogen Peroxide (ml)<span style="color: red">*</span></label><input class="form-control" type="text" placeholder="400 ml"  id="sqhp" name="sqhp" readonly required></div>
                           <div class="form-group"><label>WFI Q.S (Volume Makeup Upto) (ml)<span style="color: red">*</span></label><input class="form-control" type="text" placeholder="10000 ml" id="sqqs" name="sqqs" readonly required></div>
                     
               </div>
               
			   <div class="col-md-3">
                   <p><b>Actual Quantity</b></p>
                           <div class="form-group"><label>Arn No <span style="color: red">*</span></label><input class="form-control" type="text" placeholder="Water Arn No" required name="warno" id="warno"></div>
                           <div class="form-group"><label>Arn No <span style="color: red">*</span></label><input class="form-control" type="text" placeholder="Hydrogen Peroxide Arn NO" required name="hparno" id="hparno"></div>
                           <div class="form-group"><label>Arn No <span style="color: red">*</span></label><input class="form-control" type="text" placeholder="WFI Q.S Arn No" required name="wfarno" id="wfarno"></div>
                     
               </div>
			   
			   <div class="col-md-3">
                  <p><b>&nbsp;&nbsp;</b></p>
                           <div class="form-group"><label>Water For Injection <span style="color: red">*</span></label>
                            <div class="row">
                            <div class="col-md-6"><input class="form-control" type="number" placeholder="" min="1" required name="wati" id="wati"></div>
                            <div class="col-md-6">
                              <input type="text" class="form-control" name="wauom" value="ml" required readonly>
                              <!--<select class="form-control" name="wauom" required readonly>
                                <option value="" disabled>Select UoM</option>
                                <option value="ml" selected>ml</option>
                                <option value="l">l</option>
                              </select>-->
                            </div>
                            </div>
                          </div>
                           <div class="form-group"><label>Hydrogen Peroxide <span style="color: red">*</span></label>
                              <div class="row">
                              <div class="col-md-6"><input class="form-control" type="number" placeholder="" min="1" required name="hpq" id="hpq"></div>
                              <div class="col-md-6">
                                <input type="text" class="form-control" name="hpauom" value="ml" required readonly>
                                <!--<select class="form-control" name="hpauom" required readonly>
                                  <option value="" disabled>Select UoM</option>
                                  <option value="ml" selected>ml</option>
                                  <option value="l">l</option>
                                </select>-->
                              </div>
                              </div>
                            
                          </div>
                           <div class="form-group"><label>WFI Q.S (Volume Makeup Upto) <span style="color: red">*</span></label>
                            <div class="row">
                              <div class="col-md-6"><input class="form-control" type="number" placeholder="" min="1" required name="wfiq" id="wfiq"></div>
                              <div class="col-md-6">
                                <input type="text" class="form-control" name="wfauom" value="ml" required readonly>
                                <!--<select class="form-control" name="wfauom" required readonly>
                                  <option value="" disabled>Select UoM</option>
                                  <option value="ml" selected>ml</option>
                                  <option value="l">l</option>
                                </select>-->
                              </div>
                              </div>

                           </div>
                     
               </div>

			   
            </div><!-- END row-->

            <div class="form-row">
               <div class="col-md-12 text-center">
               	<button class="btn btn-lg btn-success" id="validationnow21" type="submit">Start</button> &nbsp; &nbsp;<button class="btn btn-lg btn-danger" id="validationnow22" type="button" disabled>Stop</button>
               </div>
            </div>   

		</div>
</div>				
</form>

<form id="sterilization">
<h4>Sterilization Details</h4>
<div class="card card-default">
        <div class="card-body">
        	<div class="form-row">
			<div class="col-lg-3 mb-3"><label for="equipmentname_steri">Equipment</label><input class="form-control autocomplete" id="equipmentname_steri" type="text" placeholder="Equipment Name" name="equipmentname_steri" required>
			</div>
			<div class="col-lg-3 mb-3"><label for="codeno_steri">Code Number</label><input class="form-control" id="codeno_steri" type="text" placeholder="Code No" name="codeno_steri" required readonly>
			</div>
			<div class="col-lg-3 mb-3"><label for="sterilizerno">Sterilzer Number</label><input class="form-control" id="sterilizerno" type="text" placeholder="Sterilzer No" name="sterilizerno" required>
			</div>
			<div class="col-lg-3 mb-3"><label for="runno">Run Number</label><input class="form-control" id="runno" type="number" placeholder="Run No" name="runno" min="1" required>
			</div>
	 		</div>
            
            <div class="form-row">
			<div class="col-lg-3 mb-3"><label for="graphno">Ref. Graph No </label><input class="form-control" id="graphno" type="text" placeholder="Ref. Graph No" name="graphno">
			</div>
			<div class="col-lg-3 mb-3"><label for="createdon">Sterilization Date</label><input class="form-control" id="createdon" type="date" placeholder="Sterilization Date" name="createdon">
			</div>
			<div class="col-lg-3 mb-3"><label for="fromtime">Sterilization From Time</label><input class="form-control" id="fromtime" type="time" placeholder="From Time" name="fromtime">
			</div>
			<div class="col-lg-3 mb-3"><label for="totime">Sterilization To Time</label><input class="form-control" id="totime" type="time" placeholder="To Time" name="totime">
			</div>
	 		</div>


	 		<div class="form-row">
               <div class="col-md-12 text-center">
               	<button class="btn btn-lg btn-warning" id="validationnow_steri" type="submit">Check By</button>
               </div>
            </div>
       	</div>
</div>
</form>

<form id="filtration">
<h4>Filtration Details</h4>     	 	
<div class="card card-default">
        <div class="card-body">
        	<div class="form-row">
			   <div class="col-lg-4 mb-3 mt-2"><label for="lotno">0.2 micro Polyether Sulfone Capsule Filter Lot Number</label></div>
			   <div class="col-lg-8 mb-3">
			   	<input class="form-control" id="lotno" type="text" placeholder="Number" name="lotno">
			   </div>
			</div>
			<div class="form-row">
			   <div class="col-lg-4 mb-3 mt-2">Pre Filtration Integrity Test Results :
			   </div>
			   <div class="col-lg-8 mb-3">
			   	<div class="row">
			   	<div class="col-lg-4 mt-2"><label for="prelimit">Limit: NLT 38 PSI</label></div><div class="col-lg-8"><input class="form-control" id="prelimit" type="text" placeholder="Number" name="prelimit">
			   </div>
				</div>
				</div>
			</div>
			<div class="form-row">
			   <div class="col-lg-4 mb-3 mt-2">Collect the filtrate of solution
			   </div>
			   <div class="col-lg-8 mb-3">
          <div class="row">
			   	<div class="col-lg-6"><label>From Time</label><input class="form-control" id="fil_fromtime" type="time" placeholder="From Time" name="fil_fromtime"> </div><div class="col-lg-6"><label>To Time</label><input class="form-control" id="fil_totime" type="time" placeholder="To Time" name="fil_totime"></div>
			    </div>
         </div>
			</div>
			<div class="form-row">
			   <div class="col-lg-4 mb-3 mt-2">Post filtration integrity test results :
			   </div>
			   <div class="col-lg-8 mb-3">
			   	<div class="row">
			   	<div class="col-lg-4 mt-2"><label for="postlimit">Limit: NLT 38 PSI</label></div><div class="col-lg-8"><input class="form-control" id="postlimit" type="text" placeholder="Number" name="postlimit">
			   </div>
				</div>
				</div>
			</div>
			<div class="form-row">
               <div class="col-md-12 text-center">
               	<button class="btn btn-lg btn-green" id="validationnow_submit" type="submit">Submit`</button> 
               </div>
            </div>

		</div>
</div>
</form>

<div class="card card-default" id="fooldiv">
        <div class="card-body">
        	<div class="form-row">
<div class="col-md-12 text-center">
               	<button class="btn btn-lg btn-warning" id="validationnow_filtration" type="button">Check By`</button> &nbsp;&nbsp;&nbsp;&nbsp; 
				<!--<button class="btn btn-lg btn-warning" id="validationnow_filtration" type="button" onClick="window.location.reload();"> Reset </button>-->
               </div>
            </div>

		</div>
</div>




        </div>
      </div>
    </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============-->
<!-- MODERNIZR-->

<script src="<?= base_url()?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API-->
<script src="<?= base_url()?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL-->
<script src="<?= base_url()?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next-->
<script src="<?= base_url()?>vendor/i18next/i18next.js"></script>
<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script>
<script src="<?= base_url()?>vendor/jquery/dist/jquery.js"></script>
<script src="<?= base_url()?>vendor/popper.js/dist/umd/popper.js"></script>
<script src="<?= base_url()?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============-->

<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============-->
<!-- =============== APP SCRIPTS ===============-->
<script src="<?= base_url()?>js/app.js"></script>

<!--<script type="text/javascript" src="<?= base_url()?>js/script.js"></script>-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>

<script type="text/javascript">
  
  function submitFunction() {
  alert("Disinfectant Solution Preparation Record Submitted Successfully!!");
}

  
  
  $(document).ready(function(){

    $("#preparation").hide();
    $("#sterilization").hide();
    $("#filtration").hide();
	$("#fooldiv").hide();

    
    $("#equipmentname_steri").keyup(function() {
      var str = $(this).val();
      if(str!="" && str.length>0){
        
      $.ajax({
          url: "<?= base_url()?>Injectable/getEquipments",
          type: 'POST',
          data: {str:str},
          success: function(res) {
              console.log(res);
              availableTags = res; 
        $("#equipmentname_steri").autocomplete({
          source: availableTags
        });
          }
      });

      }
      });

    $("#equipmentname_steri").change(function() {
    var str = $(this).val();

    $.ajax({
        url: "<?= base_url()?>Injectable/getEquipmentsval",
        type: 'POST',
        data: {str:str},
        success: function(res) {
            //console.log(res);
            $("#codeno_steri").val(res.equipment_code);    
        }
    });

    });

    $("#equipmentname").keyup(function() {
      var str = $(this).val();
      if(str!="" && str.length>0){
        
      $.ajax({
          url: "<?= base_url()?>Injectable/getEquipments",
          type: 'POST',
          data: {str:str},
          success: function(res) {
              console.log(res);
              availableTags = res; 
        $("#equipmentname").autocomplete({
          source: availableTags
        });
          }
      });

      }
      });

    $("#equipmentname").change(function() {
    var str = $(this).val();

    $.ajax({
        url: "<?= base_url()?>Injectable/getEquipmentsval",
        type: 'POST',
        data: {str:str},
        success: function(res) {
            //console.log(res);
            $("#codeno").val(res.equipment_code);    
        }
    });

    });

    $('#equipmentcleaning').submit(function () {
          $("#myb").click();
          return false;
    });

    $('#loginform').submit(function () {
          $.ajax({
            url: '<?= base_url()?>Injectable/checkAuth',
            type: 'POST',
            data: $(this).serialize(),
            success: function (res) {
              //console.log(res);
              //alert('form was submitted');
              if(res.status == 1){
                //$("#msg").show();
                //$("#msg").html("Validate User ok..........");
               $("#cnow").click();
               $('#loginform')[0].reset();
               eqnow(res.data.emp_code);
               alert("Checked By successful"); 
              }
              else{
                $('#loginform')[0].reset();
                $("#msg").show();
                $("#msg").html(res.msg);
                $("#msg").css("color","red");
                setTimeout(function() { $("#msg").hide(); }, 5000);
              }
            }
          });
          return false;

    });

    $("#solutionname").keyup(function() {
      var str = $(this).val();
      if(str!="" && str.length>0){
        
      $.ajax({
          url: "<?= base_url()?>Injectable/solutionNames",
          type: 'POST',
          data: {str:str},
          success: function(res) {
              console.log(res);
              availableTags = res; 
        $("#solutionname").autocomplete({
          source: availableTags
        });
          }
      });

      }
      });

    
    $("#solutionname").change(function() {
    var str = $(this).val();
    $.ajax({
        url: "<?= base_url()?>Injectable/getSolutionval",
        type: 'POST',
        data: {str:str},
        success: function(res) {
            //console.log(res);
            //$("#codeno").val(res.equipment_code);
            //sqwfi sqhp sqqs  
            $("#sqwfi").val(res.water_qty);
            $("#sqhp").val(res.sol_qty);
            $("#sqqs").val(res.wfi_qs);
                
        }
    });

    });

    
    $('#preparation').submit(function () {
      $.ajax({
        url: '<?= base_url()?>Injectable/submitPreparation',
        type: 'POST',
        data: $(this).serialize(),
        success: function (res) {
          //console.log(res);
          if(res.status==1){
            $("#validationnow21").attr("disabled",true);
            $("#validationnow22").attr("disabled",false);
            alert("Solution Preparation is started successfully");
          }
        }
      });
      return false;
    });

    $("#validationnow22").click(function(){
      $.ajax({
        url: '<?= base_url()?>Injectable/stopPreparation',
        type: 'GET',
        //data: $(this).serialize(),
        success: function (res) {
          $("#validationnow22").attr("disabled",true);
          $("#sterilization").show();
		  $("#filtration").show();
      alert("Solution Preparation is stopped successfully");
        }
      });
      
    });  


	$('#sterilization').submit(function () {
          $("#myb1").click();
          return false;
    });

   $('#loginform_steri').submit(function () {
          $.ajax({
            url: '<?= base_url()?>Injectable/checkAuth',
            type: 'POST',
            data: $(this).serialize(),
            success: function (res) {
              //console.log(res);
              //alert('form was submitted');
              if(res.status == 1){
                //$("#msg").show();
                //$("#msg").html("Validate User ok..........");
               $("#cnow_steri").click();
               $('#loginform_steri')[0].reset();
               sterinow(res.data.emp_code);
               alert("Checked By successful"); 
              }
              else{
                $('#loginform_steri')[0].reset();
                $("#msg2").show();
                $("#msg2").html(res.msg);
                $("#msg2").css("color","red");
                setTimeout(function() { $("#msg2").hide(); }, 5000);
              }
            }
          });
          return false;

    });

    $('#filtration').submit(function () {
      $.ajax({
        url: '<?= base_url()?>Injectable/filtrationSubmit',
        type: 'POST',
        data: $(this).serialize(),
        success: function (res) {
          //console.log(res);
          if(res.status==1){
            $("#fooldiv").show();
            alert("Filteration Details Submitted successfully");
          }
        }
      });
      return false;
    });



	$('#validationnow_filtration').click(function () {
          $("#myb2").click();
          return false;
    });

   $('#loginform_filtration').submit(function () {
          $.ajax({
            url: '<?= base_url()?>Injectable/checkAuth',
            type: 'POST',
            data: $(this).serialize(),
            success: function (res) {
              //console.log(res);
              //alert('form was submitted');
              if(res.status == 1){
                //$("#msg").show();
                //$("#msg").html("Validate User ok..........");
               $("#cnow_filtration").click();
               $('#loginform_filtration')[0].reset();
               filtrationnow(res.data.emp_code);
               alert("Checked By successful"); 
              }
              else{
                $('#loginform_filtration')[0].reset();
                $("#msg3").show();
                $("#msg3").html(res.msg);
                $("#msg3").css("color","red");
                setTimeout(function() { $("#msg3").hide(); }, 5000);
              }
            }
          });
          return false;

    });

   $("#prelimit").keyup(function(){
    var vv = $(this).val();
    if(vv.length==2 && vv<"38"){
      alert("Value should be equal/more than 38");
      $(this).val("");
    }
    
   });

   $("#postlimit").keyup(function(){
    var vv = $(this).val();
    if(vv.length==2 && vv<"38"){
      alert("Value should be equal/more than 38");
      $(this).val("");
    }
    
   });

   $("#requiredquantity").change(function() {
   var tval = $(this).val();
   if(tval!="0" || tval!=""){
    sqwfi = $("#sqwfi").val();
    sqhp = $("#sqhp").val();
    sqqs = $("#sqqs").val();

    var a1 = (sqwfi/sqqs)*tval;
    var a2 = (sqhp/sqqs)*tval;

    $("#wati").val(a1.toFixed(2));
    $("#hpq").val(a2.toFixed(2));
    $("#wfiq").val(tval);
    
   }
   });

  });


  function eqnow(ucode){
    var data = $('#equipmentcleaning').serializeArray();
    data.push({name: 'ucode', value: ucode});
    $.ajax({
            url: '<?= base_url()?>Injectable/equipmentcleaningSubmit',
            type: 'POST',
            data: data,
            success: function (res) {
              console.log(res);
              $("#validationnow").attr("disabled","true");
              $("#preparation").show();
              //$("#fid").val(res.id); 
            }
           });   
  }

  function sterinow(ucode){
    var data = $('#sterilization').serializeArray();
    data.push({name: 'ucode_steri', value: ucode});
    $.ajax({
            url: '<?= base_url()?>Injectable/setrilizationSubmit',
            type: 'POST',
            data: data,
            success: function (res) {
              console.log(res);
              $("#validationnow_steri").attr("disabled","true");
              //$("#preparation").show();
              //$("#fid").val(res.id); 
            }
           });   
  }

  function filtrationnow(ucode){
    var data = $('#filtration').serializeArray();
    data.push({name: 'ucode_fil', value: ucode});
    $.ajax({
            url: '<?= base_url()?>Injectable/filtrationCheckby',
            type: 'POST',
            data: data,
            success: function (res) {
              console.log(res);
              $("#validationnow_filtration").attr("disabled","true");
              
              //$("#fid").val(res.id); 
            }
           });   
  }

</script>


<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
     <div class="modal-content">
        <div class="modal-header">
           <h4 class="modal-title" id="myModalLabel">Login To Validate</h4>
        </div>
        <div class="modal-body">

          <form id="loginform">
            <div class="form-row">
              <div class="form-group col-md-12"><label>User Id <span style="color: red">*</span></label><input class="form-control" type="text" placeholder="User Id" required name="userid" id="userid"></div>
              <div class="form-group col-md-12"><label>Password <span style="color: red">*</span></label><input class="form-control" type="Password" placeholder="Password" required name="password" id="password"></div>
              <div class="form-group col-md-6">
                <p id="msg"></p>
              </div>
              <div class="form-group col-md-6 text-right">
                <button class="btn btn-success" type="submit">Submit</button> &nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-danger btn-secondary" type="button" data-dismiss="modal" id="cnow">Close</button>
              </div>
            </div>
          </form>
       

        </div>
        <!--<div class="modal-footer" hidden><button class="btn btn-green btn-secondary" type="button" data-dismiss="modal" id="cnow">Close</button></div>-->
     </div>
  </div>
</div>

<!----  STERILIZATION  ---->

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModa2" hidden id="myb1" data-backdrop="static" data-keyboard="false"></button>

<div class="modal fade" id="myModa2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
     <div class="modal-content">
        <div class="modal-header">
           <h4 class="modal-title" id="myModalLabel">Login To Validate</h4>
        </div>
        <div class="modal-body">

          <form id="loginform_steri">
            <div class="form-row">
              <div class="form-group col-md-12"><label>User Id <span style="color: red">*</span></label><input class="form-control" type="text" placeholder="User Id" required name="userid" id="userid"></div>
              <div class="form-group col-md-12"><label>Password <span style="color: red">*</span></label><input class="form-control" type="Password" placeholder="Password" required name="password" id="password"></div>
              <div class="form-group col-md-6">
                <p id="msg2"></p>
              </div>
              <div class="form-group col-md-6 text-right">
                <button class="btn btn-success" type="submit">Submit</button> &nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-danger btn-secondary" type="button" data-dismiss="modal" id="cnow_steri">Close</button>
              </div>
            </div>
          </form>
       

        </div>
        <!--<div class="modal-footer" hidden><button class="btn btn-green btn-secondary" type="button" data-dismiss="modal" id="cnow_steri">Close</button></div>-->
     </div>
  </div>
</div>


<!----  FILTRATION  ---->

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModa3" hidden id="myb2" data-backdrop="static" data-keyboard="false"></button>

<div class="modal fade" id="myModa3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
     <div class="modal-content">
        <div class="modal-header">
           <h4 class="modal-title" id="myModalLabel">Login To Validate</h4>
        </div>
        <div class="modal-body">

          <form id="loginform_filtration">
            <div class="form-row">
              <div class="form-group col-md-12"><label>User Id <span style="color: red">*</span></label><input class="form-control" type="text" placeholder="User Id" required name="userid" id="userid"></div>
              <div class="form-group col-md-12"><label>Password <span style="color: red">*</span></label><input class="form-control" type="Password" placeholder="Password" required name="password" id="password"></div>
              <div class="form-group col-md-6">
                <p id="msg3"></p>
              </div>
              <div class="form-group col-md-6 text-right">
                <button class="btn btn-success" type="submit">Submit</button> &nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-danger btn-secondary" type="button" data-dismiss="modal" id="cnow_filtration">Close</button>
              </div>
            </div>
          </form>
       

        </div>
        <!--<div class="modal-footer" hidden><button class="btn btn-green btn-secondary" type="button" data-dismiss="modal" id="cnow_filtration">Close</button></div>-->
     </div>
  </div>
</div>



</body>
</html>
