<!-- Main section-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-3 pl-0">Select activity</div>
        <form class="search-form col-sm-5 pl-0">
          <!--<em class="icon-magnifier"></em>
          <input class="form-control" type="email" placeholder="Enter room code or number on name">-->
        </form>
        <div class="col-sm-4 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
          </ol>
        </div>
      </div>
          <div class="row mt-14">
          <?php $this->session->set_userdata('room_code', $data['room_code']); ?>
			     <?php foreach($data['activity']->result() as $row) { ?> 
            <div class="col-sm-3 text-center first-bg-color mt-1" id="show-hexagoan"> <a href="<?php echo base_url(); ?><?php echo $row->activityfunc_tocall ?>" class="roomhover">
              <div class="rectangle-box"><img src="<?php echo base_url(); ?>img/<?php echo $row->activity_icon ?>">
                <p><?php echo $row->activity_name ?></p>
              </div>
              </a> </div>
            <?php } ?>			
          </div>          		  
          <!--<div class="row mt30 mb-5">
            <div class="col-sm-12 text-center"> <a href="<?php echo base_url(); ?>#"><img src="<?php echo base_url(); ?>img/down-arrow.png" width="50" class="bounce"></a> </div>
          </div>-->
    </div>
  </section>
<!-- Page footer-->