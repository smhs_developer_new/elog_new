<link rel="stylesheet" href="<?php echo base_url(); ?>css/Bhupeestyle.css"> 
  <!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-8 pl-0">Format for Differential Pressure Monitoring of Pass Box</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-4 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>injectable/activities">Activity</a></li>
            
          </ol>
        </div>
      </div>

            <!--<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">

<a class="btn btn-primary btn-lg" href="<?= base_url()?>Sanitization/dailyCleaningview" > Back To list</a>  
&nbsp;&nbsp;&nbsp;
<a class="btn btn-primary btn-lg" href="<?= base_url()?>Sanitization/dailyCleaning"> Add Daily Cleaning</a>

</div>
</div>
</div>
</div>-->

<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-5">
<span class="listexecutesop"><b>Equipment code : </b>
<select id="equipddl" name="equipddl" class="custom-select custom-select-sm col-sm-8">
  <option value="0">Select Here</option>
 <?php if(count($data['equip']->result())) { ?>
        <?php foreach($data['equip']->result() as $row) { ?>
        <option value="<?php echo $row->equipment_code ?>"><?php echo $row->equipment_code ?> | <?php echo $row->equipment_name ?></option>
        <?php } } ?> 
</select>
</span> 
</div>
<div class="col-sm-4">
<span class="listexecutesop"><b>Reference SOP No. : </b> <span id="sop">PAR-225</span></span>
</div>
<div class="col-sm-3">
<div class="listexecutesop"><b>Format No. : <span id="docid">F/PAR-225/001/04</span></div>
</div>
</div>
</div>
</div>
	  
<div class="card card-default">
  <div class="card-header text-white" style="background-color:#ff6f61;">Differential pressure(DP) Reading (Limit: 5 to 25 mm of water)</div>
  <div class="card-body">
	<fieldset>                      
  <div class="col-md-12">
  <div class="form-row">
  <div class="col-lg-2 mb-3"><label >Date :</label>
  <input class="form-control" type="date" name="dpdate" id="dpdate" required>                                
  </div>
   <div class="col-lg-2 mb-3"><label >Time :</label>
  <input class="form-control" type="time" name="dptime" id="dptime" required>                                
  </div>
   <div class="col-lg-2 mb-3"><label >DP Value :</label>
  <input class="form-control" type="text" name="dpvalue" id="dpvalue" required>                                
  </div>
   <div class="col-lg-4 mb-3"><label >Visually cleaning Ensured By :</label>
  <input class="form-control" type="text" name="dpensuredby" id="dpensuredby" value="<?php echo $this->session->userdata('empname'); ?>" readonly>                                
  </div>
  <div class="col-lg-2 mb-3"><label>&nbsp;</label>
  <input class="form-control btn-success"  id="subbtn" type="submit" value="Submit">                                
  </div>                        
  </div>
  </div>
  </fieldset>
	</div>
</div>
	
  <div class="card card-default" id="divftbl"   >
    <div class="card-header p-5"><center>Differential pressure monitoring records</center></div>
    <table class="table table-dark" id="recordtbl">
    <thead>
    <tr tabindex=0>                        
      <th scope="col">Document No.</th>
      <th scope="col">Equipment code</th>
      <th scope="col">Date</th>
      <th scope="col">Time</th>
      <th scope="col">DP value</th>
      <th scope="col">Ensured By</th>
    </tr>
    </thead>
    <tbody> 
     <?php if(count($data['dpdtl']->result())) { ?>
        <?php foreach($data['dpdtl']->result() as $row) { ?>
        <tr><td><?php echo "PAR-225-".$row->id ?></td><td><?php echo $row->equip_code ?></td><td><?php echo $row->date ?></td><td><?php echo $row->time ?></td><td><?php echo $row->dpvalue ?></td><td><?php echo $row->ensuredby ?></td></tr>
        <?php } } else { ?>  
          <tr><td colspan="5"><center>No record exist.</center></td></tr>
        <?php } ?>                  
    </tbody>
    </table>
    </div><!-- START row-->
  </div>
  </div>
  </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?php echo base_url(); ?>js/app.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="js/script.js"></script>-->
<script type="text/javascript">
  $(document).ready(function () {
  $("#subbtn").click(function(){
      debugger;
    var equipcode = $("#equipddl").val();
    var sop_code = $("#sop").text();
    var docid = $("#docid").text();
    var date = $("#dpdate").val();
    var time = $("#dptime").val();
    var dpvalue = $("#dpvalue").val();
    var ensuredby = $("#dpensuredby").val();

      $.ajax({
      url: "<?= base_url()?>Secondblock/DP_monitoringdtl",
      type: 'POST',
      data: {equipcode:equipcode,sop_code:sop_code,docid:docid,date:date,time:time,dpvalue:dpvalue,ensuredby:ensuredby},
      success: function(res) {
      console.log(res);        
      if(res.status==1)
      {
          alert("successfully saved.");
          window.location.href = "<?= base_url()?>Secondblock/DP_monitoring";
          /*$("#myb").click();
          $("#head").html("DP monitoring");
          $("#message").html("successfully saved.");
          $("#disclose").attr("href","<?= base_url()?>Secondblock/DP_monitoring");*/
      }
      else
      {
        alert("Not saved.");
      }
    }
    });      
  });
});


function getsop()
{
  var area = $("#areaddl").val();
  //$("#area").text(area);

  $.ajax({
    url: "<?= base_url()?>Secondblock/getsop",
    type: 'POST',
    data: {area:area},
    success: function(res) {
        console.log(res);
        $("#sop").text(res);
    }
});

}

</script>

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="head"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="message"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <a href="" class="btn btn-primary" id="disclose">Ok</a>
      </div>
    </div>

  </div>
</div>

</body>
</html>