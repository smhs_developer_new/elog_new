
<!-- Main section-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-8 pl-0">Format for Cleaning and Sanitization Activity for Aseptic Area.</div>
        <!--<form class="search-form col-sm-5 pl-0">
          <em class="icon-magnifier"></em>
          <input class="form-control" type="email" placeholder="Enter room code or number on name">
        </form>-->
        <div class="col-sm-4 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>injectable/activities">Activity</a></li>
            
          </ol>
        </div>
      </div>


      <div class="card2 card card-default mt-4">
        <div class="card-body">
          <div class="row">
            <div class="col-4">
              <span class="listexecutesop"><b>Select Solution : </b>
              <select class="custom-select custom-select-sm" id="solddl">
              <option selected="" value="0">Select here</option>
              <?php if(count($data['sol']->result())) { ?>
              <?php foreach($data['sol']->result() as $row) { ?>
              <option value="<?php echo $row->solutionname ?>"><?php echo $row->solutionname ?></option>
              <?php } } ?>                   
              </select>
              </span>
            </div>
            <div class="col-2">
            <span class="listexecutesop"><b>Select Room : </b>
            <select class="custom-select custom-select-sm" id="roomddl">
            <option selected="" value="0">Select here</option>
            <?php if(count($data['room']->result())) { ?>
            <?php foreach($data['room']->result() as $row) { ?>
            <option value="<?php echo $row->room_code ?>"><?php echo $row->room_code ?></option>
            <?php } } ?>   
            </select>
            </span>
            </div>
            <div class="col-3">
            <span class="listexecutesop"><b>Enter lot no : </b>
            <input class="form-control" type="text" placeholder="Lot Number" id="lotno">
            </span>
            </div>
            <div class="col-3">
            <span class="listexecutesop"><b>Select Cleaning Type : </b>
            <select class="custom-select custom-select-sm" id="cleantype" onchange="getenablebytype();">
            <option selected="" value="0">Select here</option>            
            <?php if(count($data['cleaningtype']->result())) { ?>
            <?php foreach($data['cleaningtype']->result() as $row) { ?>
            <option data-priority="<?php echo $row->priority ?>" value="<?php echo $row->type ?>"><?php echo $row->type ?></option>
            <?php } } ?> 
            </select>
            </span>
            </div>
            
          </div>
        </div>
      <input type="hidden" id="hdd" name="hdd" value="" />
      </div>
      
      <div class="row mt-3">
      <div class="col-12">
       <h4>Sequence of Cleaning and Sanitization activity</h4>
      </div>
      </div>
           
      <div class="card2 card card-default mt-2" id="actdiv">
        <div class="card-body">
        <?php if(count($data['cleaning']->result())) { ?>
        <?php foreach($data['cleaning']->result() as $row) { ?>
         <div class="row mb-2 padd10" id="<?php echo $row->id."-".$row->type ?>">
         <div class="col-11"><?php echo $row->description ?></div> 
         <div class="col-1"><label class="switch"><input type="checkbox" name="check" data-desc="<?php echo $row->description ?>" data-cleaned="" data-priority="<?php echo $row->priority ?>" data-type="<?php echo $row->type ?>" id="<?php echo "chk".$row->id.$row->type ?>" disabled><span></span></label></div></div>
        <?php } } ?>
         
         <div class="row mt-5">
               <div class="col-md-12">
               	<center class="disable-button-color"><button class="btn btn-lg btn-success" id="startbtn" onclick="getstart();" type="button">Start</button> &nbsp; &nbsp;<button class="btn btn-lg btn-danger" id="stopbtn" type="button" onclick="getallchecked();" disabled>Stop</button> &nbsp; &nbsp;<button class="btn btn-lg btn-warning" id="chkbtn" type="button" onclick="getchkbox('<?php echo $row->id ?>')" disabled>Check By</button></center>
               </div>
         </div>
        
        </div>
      </div>
    
    <div class="card card-default disable-button-color" id="divftbl"   >
    <div class="card-header p-5"><center>Cleaning and Sanitization Activity for Aseptic Area Records</center></div>
    <table class="table table-dark" id="recordtbl">
    <tbody> 
     <?php if(count($data['CSAAAdtl']->result())) { ?>
        <?php $i=0; foreach($data['CSAAAdtl']->result() as $row) {  
          if($i==0)
          {
              $id=$row->id;
              $date=$row->date;
              $room=$row->room_code;
              $cleantype=$row->cleaning_activity_type;
              $st=$row->starttime;
              $et=$row->endtime;
              $lotno=$row->lot_no;
              $created_by=$row->created_by;
              $chkedby=$row->checkedby_name;
              $rmk=$row->remark;
          } $i=1; } ?>
        <tr>
          <td>
          <!-- START card tab-->
          <div class="card card-transparent" role="tabpanel">
            <!-- Nav tabs-->
            <ul class="nav nav-tabs nav-fill" role="tablist">              
              <li class="nav-item" role="presentation"><a class="nav-link active bb0" href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><?php echo $date ?> Cleaning Detail</a></li>
              <li class="nav-item" role="presentation"><a class="nav-link bb0" href="#home" aria-controls="home" role="tab" data-toggle="tab">Activities Detail</a></li>
            </ul><!-- Tab panes-->
            <div class="tab-content p-0 bg-white">
              <div class="tab-pane" id="home" role="tabpanel">
                <!-- START list group-->
                <div class="list-group mb-0">
                <?php $i=0; foreach($data['CSAAAdtl']->result() as $row) { ?>
                <a class="list-group-item" href="#"><span class="badge badge-purple float-right"><?php echo $row->status ?></span><em class="fa-fw fa fa-user mr-2"></em><?php echo $row->cleaning_activity_code ?></a>
                <?php } ?>
                </div><!-- END list group-->
                          
              </div>
              <div class="tab-pane active"  id="profile" role="tabpanel">
                <!-- START table responsive-->
                <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                  <thead>
                    <tr tabindex=0>                        
                    <th scope="col">Document No.</th>
                    <th scope="col">Room no.</th>
                    <th scope="col">Cleaning Type</th>
                    <th scope="col">Lot no.</th>
                    <th scope="col">Start Time</th>
                    <th scope="col">End Time</th>
                    <th scope="col">Done by</th>
                    <th scope="col">Checked by</th>
                    <th scope="col">Remark</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                    <td><?php echo "PAR-005-".$id ?></td>
                    <td><?php echo $room ?></td>
                    <td><?php echo $cleantype ?></td>
                    <td><?php echo $lotno ?></td>
                    <td><?php echo $st ?></td>
                    <td><?php echo $et ?></td>
                    <td><?php echo $created_by ?></td>
                    <td><?php echo $chkedby ?></td>
                    <td><?php echo $rmk ?></td>
                    </tr>
                  </tbody>
                </table>
                </div><!-- END table responsive-->
              </div>
              </div>
              </div><!-- END card tab-->
            </td>
        </tr>
        <?php } else { ?>  
          <tr><td><center>No record exist.</center></td></tr>
        <?php } ?>                
    </tbody>
    </table>
    </div><!-- START row--> 

    
    </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?php echo base_url(); ?>js/app.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
  $("#authbtn").click(function(){
    debugger;
    var emailid = $("#emailid").val();
    var pwd = $("#pwd").val();
    var rmk = $("#rmk").val();
    var id  = $("#hdd").val();
    var tblname = "trn_inj_roomcleaningheader";

      $.ajax({
      url: "<?= base_url()?>Secondblock/userauthentication",
      type: 'POST',
      data: {emailid:emailid,pwd:pwd,id:id,tblname:tblname,rmk:rmk},
      success: function(res) {
      console.log(res);        
      if(res.status==1)
      {
          alert(res.msg);
          window.location.href = "<?= base_url()?>Secondblock/CleaningAndSanitizationActivityAsepticArea";
      }
      else
      {
        alert(res.msg);
      }
    }
    });
  });
});

function getenablebytype()
{
  debugger;
  var type = $("#cleantype").val();
  var priority = $("#cleantype option:selected").attr('data-priority');
  $("#actdiv").find('input[name^="check"]').each(function () { 
       $(this).attr("disabled", true);
          $(this).attr("data-status", 'disabled');
          $(this).prop('checked',false);
  });
  $("#actdiv").find('input[name^="check"]').each(function () { 
    for(i = priority ; i>0 ; i--)
    {
        if($(this).attr('data-priority') == i)
        { 
          $(this).attr("disabled", false);
          $(this).attr("data-status", 'enabled');
        }
    }
  });


  /*$("#actdiv").find('input[name^="check"]').each(function () { 
  if($(this).attr('data-type') == type)
  { 
        $(this).attr("disabled", false);
        $(this).attr("data-status", 'enabled');
  }
  else{
    $(this).attr("disabled", true);
    $(this).attr("data-status", 'disabled');
    $(this).prop('checked',false);
  }
  
  });*/
}

function getstart()
{
  debugger;
    var solddl = $("#solddl").val();
    var roomddl = $("#roomddl").val();
    var lotno = $("#lotno").val();
    var cleantype = $("#cleantype").val();

      $.ajax({
      url: "<?= base_url()?>Secondblock/CSAAA",
      type: 'POST',
      data: {solddl:solddl,roomddl:roomddl,cleantype:cleantype,lotno:lotno},
      success: function(res) {
      console.log(res);        
      if(res.status==1)
      {
          alert("Cleaning successfully started.");
          $("#startbtn").attr("disabled", true);
          $("#chkbtn").attr("disabled", true);
          $("#stopbtn").attr("disabled", false);

          $("#solddl").attr("disabled", true);
          $("#roomddl").attr("disabled", true);
          $("#lotno").attr("disabled", true);
          $("#cleantype").attr("disabled", true);

          $("#hdd").val(res.last_id);
          //window.location.href = "<?= base_url()?>Secondblock/Material_transfer_detail";
      }
      else
      {
        alert("Not started.");
      }
    }
    });
}

function getallchecked()
{
  debugger;
  var headerid = $("#hdd").val();
  var type = $("#cleantype").val();
  var chk="1";
  var act = new Array();
    $("#actdiv").find('input[name^="check"]').each(function () { 
      if($(this).attr('data-status') == 'enabled')
      {
        if ($(this).prop('checked')==true){ 
          $(this).attr("data-cleaned", 'done');
        }
        else
        {          
          chk="0";
        }
      }
      else
      {
        $(this).attr("data-cleaned", 'n/a');
      }
    });
    if(chk == "0")
    {
      alert("Please checked all acitivities of " + type +" cleaning.");
    }
    else
    {
      $("#actdiv").find('input[name^="check"]').each(function () {
          act.push($(this).attr("data-desc")+"###"+$(this).attr("data-cleaned"));
      });
    }
  getstop(act,headerid);    
}

function getstop(act,headerid)
{
  debugger;

  $.ajax({
      url: "<?= base_url()?>Secondblock/stopCSAAA",
      type: 'POST',
      data: {act:act,headerid:headerid},
      success: function(res) {
        debugger;
      console.log(res);        
      if(res.status==1)
      {
          alert("Cleaning successfully stopped.");
          $("#startbtn").attr("disabled", true);
          $("#chkbtn").attr("disabled", false);
          $("#stopbtn").attr("disabled", true);

          $("#solddl").attr("disabled", true);
          $("#roomddl").attr("disabled", true);
          $("#lotno").attr("disabled", true);
          $("#cleantype").attr("disabled", true);
          //window.location.href = "<?= base_url()?>Secondblock/Material_transfer_detail";
      }
      else
      {
        alert("Not stopped.");
      }
    }
    });
}
</script>
<script>
var id='';
function getchkbox(obj)
{ 
  debugger;
  $("#myModal").show(1000);
  $("#myModal").attr("data-id", obj);
}
function hidechkbox()
{
  
  $("#myModal").hide(1000);
  var id = $("#myModal").attr("data-id");
  //brkdownover(id);
  
}
</script>
<!--My Modal-->
<div class="modal fade show" id="myModal" data-id="0"  style="display: none; padding-right: 17px;">
  <div class="modal-dialog modal-md">
    <div class="modal-content mt120">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelLarge">Submit your login credential</h4>
        <!--<button class="close" type="button" data-dismiss="modal" aria-label="Close" onclick="hidememo();"><span aria-hidden="true">×</span></button>-->   
      </div>
    
      <div class="modal-body">
    <div class="col-md-12">
                  <!-- START card-->
                  <div class="card card-default">
                     <div class="card-body">
                        <form>
                           <div class="form-group"><label>Email id</label><input name="emailid" id="emailid" class="form-control" type="text" placeholder="Enter Memo Number"></div>
                           <div class="form-group"><label>Password</label><input type="Password" name="pwd" id="pwd" class="form-control" /></div>
                           <div class="form-group"><label>Remark</label><input type="text" name="rmk" id="rmk" class="form-control" /></div>
                        </form>
                     </div>
           
            </div><!-- END card-->
        </div>    
      </div>
    <div class="modal-footer" style="justify-content: center;">       
      <div class="modal-button-container"><button type="button" id="authbtn" name="authbtn" class="btn btn-success">Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" id="closebtn" name="closebtn" onclick="hidechkbox();" class="btn btn-danger">Close</button></div>
    </div>
    </div>
  </div>
</div>
<!--End My Modal-->
</body>
</html>