<link rel="stylesheet" href="<?php echo base_url(); ?>css/Bhupeestyle.css"> 
  <!-- Main section-->
<form action="#" id="mat_dtl" method="post" autocomplete="off" enctype="multipart/form-data">
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-10 pl-0">Format for Differential Pressure Monitoring and Material Transfer Details of Pass Box</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-2 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>injectable/activities">Activity</a></li>
            
          </ol>
        </div>
      </div>

            <!--<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">

<a class="btn btn-primary btn-lg" href="<?= base_url()?>Sanitization/dailyCleaningview" > Back To list</a>  
&nbsp;&nbsp;&nbsp;
<a class="btn btn-primary btn-lg" href="<?= base_url()?>Sanitization/dailyCleaning"> Add Daily Cleaning</a>

</div>
</div>
</div>
</div>-->

<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-5">
<span class="listexecutesop"><b>Equipment code : </b>
<select id="equipddl" name="equipddl" class="custom-select custom-select-sm col-sm-8">
  <option value="0">Select Here</option>
 <?php if(count($data['equip']->result())) { ?>
        <?php foreach($data['equip']->result() as $row) { ?>
        <option value="<?php echo $row->equipment_code ?>"><?php echo $row->equipment_code ?> | <?php echo $row->equipment_name ?></option>
        <?php } } ?> 
</select>
</span> 
</div>
<div class="col-sm-4">
<span class="listexecutesop"><b>Reference SOP No. : </b> <span id="sop">PAR-225</span></span>
</div>
<div class="col-sm-3">
<div class="listexecutesop"><b>Format No. : <span id="docid">F/PAR-225/001/04</span></div>
</div>
</div>
</div>
</div>
	  
                    

  <div class="card card-default">
  <div class="card-header text-white" style="background-color:#ff6f61;">Differential pressure(DP) Reading</div>
  <div class="card-body">
  <div class="col-md-12">
  <div class="row">
  <div class="col-lg-3 mb-3"><label >Date :</label>
  <input class="form-control"  type="date" name="dpdate" id="dpdate" required>                               
  </div>
  <div class="col-lg-3 mb-3"><label >Time :</label>
  <input class="form-control"  type="time" name="dptime" id="dptime" required>                                
  </div>
  <div class="col-lg-3 mb-3"><label >DP Value :</label>
  <input class="form-control"  type="text" name="dpvalue" id="dpvalue" required>                                
  </div>
  <div class="col-lg-3 mb-3"><label >Visually cleaning Ensured By :</label>
  <input class="form-control"  type="text" name="dpensuredby" id="dpensuredby" value="<?php echo $this->session->userdata('empname'); ?>" readonly>
  </div>                        
  </div>
  </div>
  </div>
  </div>


  <div class="card card-default">
  <div class="card-header text-white" style="background-color:#ff6f61;">Material Receive/transfer details</div>
  <div class="card-body">
  <div class="col-md-12">
  <div class="row">
  <div class="col-lg-12 mb-3"><label >Material/Equipment Details :</label>
  <input class="form-control"  type="text" name="matdtl" id="matdtl" required>                                
  </div>
  <div class="col-lg-3 mb-3"><label >Sanitization End time :</label>
  <input class="form-control"  type="time" name="ftime" id="ftime" required>                               
  </div> 
  <div class="col-lg-3 mb-3"><label >Aseptic Area :</label>
  <input class="form-control"  type="time" name="ttime" id="ttime" required>                              
  </div>                       
  <div class="col-lg-3 mb-3"><label >Sanitization Agent :</label>
  <input class="form-control"  type="text" name="Sagent" id="Sagent" required>                              
  </div>
  <div class="col-lg-3 mb-3"><label >Performed by :</label>
  <input class="form-control"  type="text" name="prfmdby" id="prfmdby" value="<?php echo $this->session->userdata('empname'); ?>" readonly>                                
  </div>
  <!--<div class="col-lg-3 mb-3"><label >Checked by :</label>
  <input class="form-control"  type="text" name="chkby" id="chkby" required readonly >                              
  </div>
   <div class="col-lg-3 mb-3"><label >Remark :</label>
  <input class="form-control"  type="text" name="rmk" id="rmk" required>                                
  </div>-->                
  </div>
  </div>
  </div>
  </div>



  <center><input class="btn btn-success mb-3" id="subbtn" type="button" value="Submit"></center>
                               


	
  <div class="card card-default" id="divftbl"   >
    <div class="card-header p-5"><center>Differential pressure monitoring and material transfer records</center></div>
    <table class="table table-dark disable-button-color" id="recordtbl" border="1">
    <thead>
    <tr tabindex=0>                        
      <th scope="col" rowspan="2">Document No.</th>
      <th scope="col" rowspan="2">Equipment code</th>
      <th scope="col" rowspan="2">Date</th>
      <th scope="col" colspan="2">Differential pressure(DP) Reading (Limit: 5 to 25 mm of water)</th>
      <th scope="col" rowspan="2">visually cleaning ensured by</th>
      <th scope="col" rowspan="2">Material Equipment details</th>
      <th scope="col" colspan="2">Material Receive/transfer time</th>
      <th scope="col" rowspan="2">Sanitization Agent</th>
      <th scope="col" rowspan="2">Performed by</th>
      <th scope="col" rowspan="2">Checked by</th>
      <th scope="col" rowspan="2">Remark</th>
      <th scope="col" rowspan="2">Action</th>
    </tr>
    <tr tabindex=0>                        
      <th scope="col">Time</th>
      <th scope="col">DP value</th>
      <th scope="col">From</th>
      <th scope="col">To</th>
    </tr>
    </thead>
    <tbody>
    <?php if(count($data['matdtl']->result())) { ?>
        <?php foreach($data['matdtl']->result() as $row) { ?>
        <tr><td><?php echo "PAR-225-".$row->id ?></td><td><?php echo $row->equip_code ?></td><td><?php echo $row->date ?></td><td><?php echo $row->time ?></td><td><?php echo $row->dpvalue ?></td><td><?php echo $row->ensuredby ?></td><td><?php echo $row->mat_eqip_dtl ?></td><td><?php echo $row->mat_eqip_rec_transfer_FT ?></td><td><?php echo $row->mat_eqip_rec_transfer_TT ?></td><td><?php echo $row->sanitization_agent ?></td><td><?php echo $row->performedby ?></td><td><?php echo $row->checkedby_name ?></td><td><?php echo $row->remark ?></td><td>
          <?php if($row->checkedby_name==null) { ?>
          <input class="btn btn-warning" type="button" id="<?php echo 'chkbtn'.$row->id ?>" name="<?php echo 'chkbtn'.$row->id ?>" value="Check" onclick="getchkbox('<?php echo $row->id ?>')">
        <?php } else { ?>
          <input class="btn btn-warning" type="button" id="<?php echo 'chkbtn'.$row->id ?>" name="<?php echo 'chkbtn'.$row->id ?>" value="Check" onclick="getchkbox('<?php echo $row->id ?>')" disabled>
        <?php } ?>
        </td></tr>
        <?php } } else { ?>  
          <tr><td colspan="13"><center>No record exist.</center></td></tr>
        <?php } ?>                   
    </tbody>
    </table>
    </div><!-- START row-->
  </div>
  </section>
  </form>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?php echo base_url(); ?>js/app.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/script.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>
<script>
$(document).ready(function () {
  $("#subbtn").click(function(){
      debugger;
    var equipcode = $("#equipddl").val();
    var sop_code = $("#sop").text();
    var docid = $("#docid").text();
    var date = $("#dpdate").val();
    var time = $("#dptime").val();
    var dpvalue = $("#dpvalue").val();
    var ensuredby = $("#dpensuredby").val();

    var matdtl = $("#matdtl").val();
    var ftime = $("#ftime").val();
    var ttime = $("#ttime").val();
    var Sagent = $("#Sagent").val();
    var prfmdby = $("#prfmdby").val();
    //var chkby = $("#chkby").val();
    //var rmk = $("#rmk").val();

    /*if($("#areaddl").val()!="" && $("#sop_code").text()!="" && $("#docid").text()!="" && $("#dpdate").val()!="" && $("#dptime").val()!="" && $("#dpvalue").val()!="" && $("#dpensuredby").val()!="" && $("#matdtl").val()!="" && $("#ftime").val()!="" && $("#ttime").val()!="" && $("#Sagent").val()!="" && $("#prfmdby").val()!="" && $("#chkby").val()!="" && $("#rmk").val()!="")
    {*/
      $.ajax({
      url: "<?= base_url()?>Secondblock/Materialtransferdtl",
      type: 'POST',
      data: {equipcode:equipcode,sop_code:sop_code,docid:docid,date:date,time:time,dpvalue:dpvalue,ensuredby:ensuredby,matdtl:matdtl,ftime:ftime,ttime:ttime,Sagent:Sagent,prfmdby:prfmdby},
      success: function(res) {
      console.log(res);        
      if(res.status==1)
      {
          alert("successfully saved.");
          window.location.href = "<?= base_url()?>Secondblock/Material_transfer_detail";
      }
      else
      {
        alert("Not saved.");
      }
    }
    });
  });

  $("#authbtn").click(function(){
    debugger;
    var emailid = $("#emailid").val();
    var pwd = $("#pwd").val();
    var rmk = $("#rmk").val();
    var id = $("#myModal").attr("data-id");
    var tblname = "trn_material_transfer_dtl";

      $.ajax({
      url: "<?= base_url()?>Secondblock/userauthentication",
      type: 'POST',
      data: {emailid:emailid,pwd:pwd,id:id,tblname:tblname,rmk:rmk},
      success: function(res) {
      console.log(res);        
      if(res.status==1)
      {
          alert(res.msg);
          window.location.href = "<?= base_url()?>Secondblock/Material_transfer_detail";
      }
      else
      {
        alert(res.msg);
      }
    }
    });
  });

});

function getsop()
{
  var area = $("#areaddl").val();
  //$("#area").text(area);

  $.ajax({
    url: "<?= base_url()?>Secondblock/getsop",
    type: 'POST',
    data: {area:area},
    success: function(res) {
        console.log(res);
        $("#sop").text(res);
    }
});

}
</script>
<script>
var id='';
function getchkbox(obj)
{ 
  $("#myModal").show(1000);
  $("#myModal").attr("data-id", obj);
}
function hidechkbox()
{
  
  $("#myModal").hide(1000);
  var id = $("#myModal").attr("data-id");
  //brkdownover(id);
  
}
</script>
<!--My Modal-->
<div class="modal fade show" id="myModal" data-id="0"  style="display: none; padding-right: 17px;">
  <div class="modal-dialog modal-md">
    <div class="modal-content mt120">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelLarge">Submit your login credential</h4>
        <!--<button class="close" type="button" data-dismiss="modal" aria-label="Close" onclick="hidememo();"><span aria-hidden="true">×</span></button>-->   
      </div>
    
      <div class="modal-body">
    <div class="col-md-12">
                  <!-- START card-->
                  <div class="card card-default">
                     <div class="card-body">
                        <form>
                           <div class="form-group"><label>Email id</label><input name="emailid" id="emailid" class="form-control" type="text" placeholder="Enter Memo Number"></div>
                           <div class="form-group"><label>Password</label><input type="Password" name="pwd" id="pwd" class="form-control" /></div>
                           <div class="form-group"><label>Remark</label><input type="text" name="rmk" id="rmk" class="form-control" /></div>

                        </form>
                     </div>
           
            </div><!-- END card-->
        </div>    
      </div>
    <div class="modal-footer" style="justify-content: center;">       
      <div class="modal-button-container"><button type="button" id="authbtn" name="authbtn" class="btn btn-success">Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" id="closebtn" name="closebtn" onclick="hidechkbox();" class="btn btn-danger">Close</button></div>
    </div>
    </div>
  </div>
</div>
<!--End My Modal-->
</body>
</html>