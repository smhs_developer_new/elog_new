<link rel="stylesheet" href="<?php echo base_url(); ?>css/Bhupeestyle.css"> 
  <!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-8 pl-0">Format for Cleaning & Sanitization for Dynamic Pass Box</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-4 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>injectable/activities">Activity</a></li>
            
          </ol>
        </div>
      </div>

            <!--<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">

<a class="btn btn-primary btn-lg" href="<?= base_url()?>Sanitization/dailyCleaningview" > Back To list</a>  
&nbsp;&nbsp;&nbsp;
<a class="btn btn-primary btn-lg" href="<?= base_url()?>Sanitization/dailyCleaning"> Add Daily Cleaning</a>

</div>
</div>
</div>
</div>-->

<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-5">
<span class="listexecutesop"><b>Equipment code : </b>
<?php if($data['eqcode']!=null && $data['eqcode']!="") { ?>
<select id="equipddl" name="equipddl" class="custom-select custom-select-sm col-sm-8">
  <option value="<?php echo $data['eqcode'] ?>"><?php echo $data['eqcode'] ?> | <?php echo $data['equipment_name'] ?></option>
</select>
<?php } else { ?>

<select id="equipddl" name="equipddl" class="custom-select custom-select-sm col-sm-8">
  <option value="0">Select Here</option>
 <?php if(count($data['equip']->result())) { ?>
        <?php foreach($data['equip']->result() as $row) { ?>
        <option value="<?php echo $row->equipment_code ?>"><?php echo $row->equipment_code ?> | <?php echo $row->equipment_name ?></option>
        <?php } } ?> 
</select>
<?php } ?>
</span> 
</div>
<div class="col-sm-4">
<span class="listexecutesop"><b>Reference SOP No. : </b> <span id="sop">PAR-225</span></span>
</div>
<div class="col-sm-3">
<div class="listexecutesop"><b>Format No. : <span id="docid">F/PAR-225/002/06</span></div>
</div>
</div>
</div>
</div>
	  
<div class="card card-default">
  <div class="card-body">
	<fieldset>                      
  <div class="col-md-12">
  <div class="form-row">
  <div class="col-lg-6 mb-3"><label >Date :</label>
  <input class="form-control" type="date" name="CSdate" id="CSdate" value="<?php echo $data['date']; ?>" required>                                
  </div>
   <div class="col-lg-6 mb-3"><label >Disinfectant solution/Lot no :</label>
  <input class="form-control" type="text" name="lotno" id="lotno" value="<?php echo $data['lotno']; ?>" required>
  </div>                                                       
  </div>
  </div>
  </fieldset>
	</div>
</div>
<input type="hidden" id="hdd" name="hdd" value="<?php echo $data['id']; ?>" />
<?php if($data['eqcode']!=null && $data['eqcode']!="") { ?>
<center class="disable-button-color"> <input class="btn btn-success mb-3"  id="startbtn" type="button" value="Start" disabled>&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-danger mb-3" type="button" id="stopbtn" name="stopbtn" value="Stop" > </center>
<?php } else { ?>
<center class="disable-button-color"> <input class="btn btn-success mb-3"  id="startbtn" type="button" value="Start">&nbsp;&nbsp;&nbsp;&nbsp;<input class="btn btn-danger mb-3" type="button" id="stopbtn" name="stopbtn" value="Stop" disabled> </center>
<?php } ?>
	
  <div class="card card-default disable-button-color" id="divftbl"   >
    <div class="card-header p-5"><center>Cleaning and Sanitization Records</center></div>
    <table class="table table-dark" id="recordtbl">
    <thead>
    <tr tabindex=0>                        
      <th scope="col">Document No.</th>
      <th scope="col">Date</th>
      <th scope="col">Disinfectant solution/Lot no</th>
      <th scope="col">Sanitization start from</th>
      <th scope="col">Sanitization started by</th>
      <th scope="col">Sanitization completed at</th>
      <th scope="col">Sanitization completed by</th>
      <th scope="col">Checked by</th>
      <th scope="col">Remark</th>
      <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody> 
     <?php if(count($data['CSdtl']->result())) { ?>
        <?php foreach($data['CSdtl']->result() as $row) { ?>
        <tr><td><?php echo "PAR-225-".$row->id ?></td><td><?php echo $row->date ?></td><td><?php echo $row->lotno ?></td><td><?php echo $row->strarttime ?></td><td><?php echo $row->startby ?></td><td><?php echo $row->endtime ?></td><td><?php echo $row->endby ?></td><td><?php echo $row->checkedby_name ?></td><td><?php echo $row->remark ?></td><td>
        <?php if($row->checkedby_name==null) { ?>
        <input class="btn btn-warning" type="button" id="<?php echo 'chkbtn'.$row->id ?>" name="<?php echo 'chkbtn'.$row->id ?>" value="Check" onclick="getchkbox('<?php echo $row->id ?>')">
        <?php } else { ?>
          <input class="btn btn-warning" type="button" id="<?php echo 'chkbtn'.$row->id ?>" name="<?php echo 'chkbtn'.$row->id ?>" value="Check" onclick="getchkbox('<?php echo $row->id ?>')" disabled>
        <?php } ?>
      </td></tr>
        <?php } } else { ?>  
          <tr><td colspan="9"><center>No record exist.</center></td></tr>
        <?php } ?>                
    </tbody>
    </table>
    </div><!-- START row-->
  </div>
  </div>
  </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?php echo base_url(); ?>js/app.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="js/script.js"></script>-->
<script type="text/javascript">
  $(document).ready(function () {
  $("#startbtn").click(function(){
      debugger;
    var equipcode = $("#equipddl").val();
    var sop_code = $("#sop").text();
    var docid = $("#docid").text();
    var CSdate = $("#CSdate").val();
    var lotno = $("#lotno").val();

      $.ajax({
      url: "<?= base_url()?>Secondblock/CSdtl",
      type: 'POST',
      data: {equipcode:equipcode,sop_code:sop_code,docid:docid,CSdate:CSdate,lotno:lotno},
      success: function(res) {
      console.log(res);        
      if(res.status==1)
      {
          alert("successfully saved.");
          window.location.href = "<?= base_url()?>Secondblock/CleaningAndSanitization";
      }
      else
      {
        alert("Not saved.");
      }
    }
    });
  });

  $("#stopbtn").click(function(){
      debugger;
    var id = $("#hdd").val();
      $.ajax({
      url: "<?= base_url()?>Secondblock/stopCSdtl",
      type: 'POST',
      data: {id:id},
      success: function(res) {
      console.log(res);        
      if(res.status==1)
      {
          alert("successfully stopped.");
          window.location.href = "<?= base_url()?>Secondblock/CleaningAndSanitization";
      }
      else
      {
        alert("Not stopped.");
      }
    }
    });
  });

  $("#authbtn").click(function(){
    debugger;
    var emailid = $("#emailid").val();
    var pwd = $("#pwd").val();
    var rmk = $("#rmk").val();
    var id = $("#myModal").attr("data-id");
    var tblname = "trn_cleaningandsanitization";

      $.ajax({
      url: "<?= base_url()?>Secondblock/userauthentication",
      type: 'POST',
      data: {emailid:emailid,pwd:pwd,id:id,tblname:tblname,rmk:rmk},
      success: function(res) {
      console.log(res);        
      if(res.status==1)
      {
          alert(res.msg);
          window.location.href = "<?= base_url()?>Secondblock/CleaningAndSanitization";
      }
      else
      {
        alert(res.msg);
      }
    }
    });
  });

});


function getsop()
{
  var area = $("#areaddl").val();
  //$("#area").text(area);

  $.ajax({
    url: "<?= base_url()?>Secondblock/getsop",
    type: 'POST',
    data: {area:area},
    success: function(res) {
        console.log(res);
        $("#sop").text(res);
    }
});

}

</script>
<script>
var id='';
function getchkbox(obj)
{ 
  debugger;
  $("#myModal").show(1000);
  $("#myModal").attr("data-id", obj);
}
function hidechkbox()
{
  
  $("#myModal").hide(1000);
  var id = $("#myModal").attr("data-id");
  //brkdownover(id);
  
}
</script>
<!--My Modal-->
<div class="modal fade show" id="myModal" data-id="0"  style="display: none; padding-right: 17px;">
  <div class="modal-dialog modal-md">
    <div class="modal-content mt120">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelLarge">Submit your login credential</h4>
        <!--<button class="close" type="button" data-dismiss="modal" aria-label="Close" onclick="hidememo();"><span aria-hidden="true">×</span></button>-->   
      </div>
    
      <div class="modal-body">
    <div class="col-md-12">
                  <!-- START card-->
                  <div class="card card-default">
                     <div class="card-body">
                        <form>
                           <div class="form-group"><label>Email id</label><input name="emailid" id="emailid" class="form-control" type="text" placeholder="Enter Memo Number"></div>
                           <div class="form-group"><label>Password</label><input type="Password" name="pwd" id="pwd" class="form-control" /></div>
                           <div class="form-group"><label>Remark</label><input type="text" name="rmk" id="rmk" class="form-control" /></div>
                        </form>
                     </div>
           
            </div><!-- END card-->
        </div>    
      </div>
    <div class="modal-footer" style="justify-content: center;">       
      <div class="modal-button-container"><button type="button" id="authbtn" name="authbtn" class="btn btn-success">Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" id="closebtn" name="closebtn" onclick="hidechkbox();" class="btn btn-danger">Close</button></div>
    </div>
    </div>
  </div>
</div>
<!--End My Modal-->
</body>
</html>