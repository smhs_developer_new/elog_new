<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid">

          <!-- Page Heading -->
			
          <div class="content-wrapper">
            <div class="content-heading executesop-heading">
              <div class="col-sm-5 pl-0">Equipment Master</div>
              <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
              <div class="col-sm-7 pr-0">
                <ol class="breadcrumb ml-auto">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
                  <li class="breadcrumb-item active"><a href="<?= base_url()?>equipment_view?module_id=<?php echo $_GET['module_id'] ?>">Equipment</a></li>
                  
                </ol>
              </div>
            </div>
            
      
      
      <div class="card card-default">
      <div class="card-body">                     
      <div class="row">
      <div class="col-sm-12 text-right">
      
      <a class="btn btn-primary btn-lg" href="<?= base_url()?>equipment_view?module_id=<?php echo $_GET['module_id'] ?>" > Back To list</a>
      
      </div>
      </div>
      </div>
      </div>
      
      <form id="equipment" enctype='multipart/form-data'>
    <div class="card card-default">
        <div class="card-body">

	  <fieldset>            
      <div class="col-auto">
        <div class="form-row">   
          <div class="col-lg-3 mb-3"><label for="aqty">Equipment Code <span style="color: red">*</span></label><input class="form-control" id="ecode" type="text" maxlength="20" placeholder="Equipment Code" name="ecode" required />
          </div>
          <div class="col-lg-3 mb-3"><label for="edso">Equipment Name <span style="color: red">*</span></label><input class="form-control" id="ename" type="text" maxlength="50" placeholder="Equipment Name" name="ename" required />
          </div>
          <div class="col-lg-3 mb-3"><label for="edso">Equipment Type <span style="color: red">*</span></label>
          <select id="etype" name="etype" onchange="divchng();" class="form-control" required>
          <option value="" selected disabled>Select Equipment Type </option>
					<option value="Fixed">Fixed</option>
					<option value="Portable">Portable</option>
          <option value="Vaccum">Vaccum Cleanner</option>
          <option value="Booth">Dispensing Booth</option>
          </select>
          </div>
          <div class="col-lg-3 mb-3"><label for="validationServer01">Block<span style="color: red">* </span></label>
          <select id="blockcode" name="blockcode" onchange="getarea();" class="form-control" required>
          <option value="0" selected disabled>Select Block</option>
          <?php if($data['block']->num_rows()>0) { ?>
          <?php foreach($data['block']->result() as $row) {  if($row->block_code !="Block-000") {?>
          <option value="<?php echo $row->block_code ?>"><?php echo $row->block_code." | ".$row->block_name ?> </option>
          <?php } } } ?> 
          </select>
          </div>
        </div>
        <div class="form-row" id="fixeq" style="display: none;">
          <div class="col-lg-6 mb-3"><label for="validationServer01">Area<span style="color: red">* </span></label>
          <select id="areacode" onchange="getroom();" name="areacode" class="form-control" required>
                
          </select>
          </div>
          <div class="col-lg-6 mb-3"><label for="validationServer01">Room<span style="color: red">* </span></label>
          <select id="rcode" name="rcode" class="form-control" required>
                
          </select>
          </div>
          </div>

          <div class="form-row">
					<div class="col-lg-6 mb-3"><label for="validationServer01">Drain Point Code </label>
          <!--<select id="scode" name="scode" class="form-control" required>
              <option value="" selected disabled>Select Drain Point </option>
              <?php if($data['sop']->num_rows()>0) { ?>
              <?php foreach($data['sop']->result() as $row) { ?>
              <option value="<?php echo $row->sop_code ?>"><?php echo $row->sop_code ?> | <?php echo $row->sop_name ?> </option>
              <?php } } ?> 
              </select>  autocomplete -->
          <input class="form-control" type="text" placeholder="Drain Point Code" name="dpcode"  id="dpcode">
          </div>
          <div class="col-lg-6 mb-3"><label for="validationServer01">Sop Code</label>
          <!--<select id="scode" name="scode" class="form-control" required>
              <option value="" selected disabled>Select Sop Code </option>
              <?php if($data['sop']->num_rows()>0) { ?>
              <?php foreach($data['sop']->result() as $row) { ?>
              <option value="<?php echo $row->sop_code ?>"><?php echo $row->sop_code ?> | <?php echo $row->sop_name ?> </option>
              <?php } } ?> 
              </select>-->
          <input class="form-control" type="text" placeholder="Sop Code" name="spcode" id="spcode">                      
          </div>                    
          </div>

                        <div class="form-row">
                         <div class="col-lg-6 mb-3"><label for="validationServer01">Remarks </label><textarea class="form-control" aria-label="With textarea" name="eremarks" placeholder="Enter Remarks" ></textarea>
                         </div>
                         <div class="col-lg-6 mb-3"><label for="edso">Equipment Icon <span style="color: red"> (less than 1MB & png/jpg/gif)</span></label><br><input class="" id="eicon" type="file" placeholder="Equipment Type" name="eicon" accept="image/x-png,image/gif,image/jpeg">
                         </div>
                        </div>

                     </fieldset>
					 </div>
					 </div>
					  <div class="card card-default">
                     
                     <div class="card-body" >
                        
                          
<div class="row disable-button-color">
      <div class="col-sm-12 text-center" id="sbtn">
          <?php if ($add) { ?>
                                <button id="sbbtn" class="btn btn-success btn-lg" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
<?php } else { ?>
                                <button class="btn btn-success btn-lg" type="button" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
<?php } ?>
          
      </div>
      </div>

                        
                     </div>
                  </div><!-- END card-->
					 
</form>
      
              </div>
			
			
			
			
			
        </div>
<script type="text/javascript">
$(document).ready(function(){

$("#dpcode").keyup(function() {
var str = $(this).val();
if(str!="" && str.length>=1){
  
$.ajax({
    url: "<?= base_url()?>ponta_sahib/Mastercontroller/getDrainpointlist",
    type: 'POST',
    data: {str:str},
    success: function(res) {
        console.log(res);
        availableTags = res; 
  
  $("#dpcode").autocomplete({
    source: availableTags
  });
    }
}); 
}
});

$("#spcode").keyup(function() {
var str = $(this).val();
if(str!="" && str.length>=1){
  
$.ajax({
    url: "<?= base_url()?>ponta_sahib/Mastercontroller/getSoplist",
    type: 'POST',
    data: {str:str},
    success: function(res) {
        console.log(res);
        availableTags = res; 
  
  $("#spcode").autocomplete({
    source: availableTags
  });
    }
}); 
}
});

 $("#ecode").change(function(){
    var epcode = $(this).val();
    $.ajax({
        url: '<?= base_url()?>ponta_sahib/Mastercontroller/checkEquipmentcode',
        type: 'POST',
        data: {ecode:epcode},
        success: function (res) {
          //console.log(res);
          if(res.status==0){
           // $("#validationnow21").attr("disabled",true);
           // $("#validationnow22").attr("disabled",false);
            alert("Equipment Code Already Exists");
            $("#ecode").val("");
          }
        }
      });
  });


   $('#equipment').submit(function () {
var form_data = new FormData(this);
if($("#eicon").val()!=""){
var file_data = $('#eicon').prop('files')[0];
//console.log(file_data);
var filename = file_data.name;
var extension = filename.substring(filename.lastIndexOf('.') + 1);
var size = file_data.size/1048576;
size = size.toFixed(2);
//console.log(filename+" - "+size.toFixed(2)+" MB - "+extension);

if(size<=1 && (extension == "png" || extension == "jpg" || extension == "gif" || extension == "jpeg")){




$.ajax({
                 url: '<?= base_url()?>ponta_sahib/Mastercontroller/mstEquipmentSubmit',
                 type: "POST",
                 data: form_data,
                 
                 processData: false,
                 contentType: false,
                 cache: false,
         
                 success: function(data) {     
                 //console.log(data);
                 //alert("Image Uploaded");
                 if(data.status==1){
                  alert("Equipment Added successfully");
                  window.location.href = "<?= base_url()?>equipment_view?module_id=<?php echo $_GET['module_id']?>";
                 }
                 }
         
             });

}
else{
  alert("You cann't upload this file.");
  $("#eicon").val(null);
}


}
else{
$.ajax({
                 url: '<?= base_url()?>ponta_sahib/Mastercontroller/mstEquipmentSubmit',
                 type: "POST",
                 data: form_data,
                 
                 processData: false,
                 contentType: false,
                 cache: false,
         
                 success: function(data) {           
                 //console.log(data);
                 //alert("Image Uploaded");
                 if(data.status==1){
                  alert("Equipment Added successfully");
                  window.location.href = "<?= base_url()?>equipment_view?module_id=<?php echo $_GET['module_id']?>";
//				  window.location.reload();
                 }
                 }
         
             });
}
 return false;
    });

});  
</script>
<script type="text/javascript">
function getarea()
{
//  debugger;
  $("#areacode").html('');
  var block = $("#blockcode").val();
  $.ajax({
    url: "<?= base_url()?>ponta_sahib/Mastercontroller/getareabyblockcode",
    type: 'POST',
    data: {block:block},
    success: function(res) {
//      debugger;
        var inv = "<option value='' selected disabled>Select Area</option>";
        $.each(res, function (index, value) {
        inv = inv.concat("<option value='"+value.area_code+"'>"+value.area_code+" | "+value.area_name+"</option>");
        });
        $("#areacode").html(inv);
    }
  });

}
function getroom()
{
//  debugger;
  var area = $("#areacode").val();
  $("#rcode").html('');
  $.ajax({
    url: "<?= base_url()?>Production/getRoomnow",
    type: 'POST',
    data: {area:area},
    success: function(res) {
        //console.log(res);
        var inv = "<option value='' selected disabled>Select Room</option>";
        $.each(res, function (index, value) {
        inv = inv.concat("<option value='"+value.room_code+"'>"+value.room_code+" | "+value.room_name+"</option>");
        });
        $("#rcode").html(inv);

    }
});

}

function divchng()
{
//  debugger;

  $("#blockcode").val('0');

  var etype = $("#etype").val();
  $("#areacode").html('');
  $("#rcode").html('');
  if(etype=="Fixed" || etype=="Booth")
  {
      $("#fixeq").show();
  }
  else
  {
      $("#fixeq").hide(); 
  }
}
</script>