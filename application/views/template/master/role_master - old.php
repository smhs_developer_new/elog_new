<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
    <!-- Page Heading -->

    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row">

                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">    	   
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th colspan="5" class="f18" style="vertical-align:middle">Role Master</th>
                                <th class="text-right">
                                    <?php if ($add) { ?>
                                        <button class="btn btn-success" ng-disabled="recordid > 0"  ng-click="showForm()">Add (+)</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success"  ng-click="showForm()" disabled>Add (+)</button>
                                    <?php } ?>

                                </th>
                            </tr>
                        </thead>
                    </table>  
                </div>
                <div class="col-12">
                    <form name="materialForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-lg-3 mb-3" ng-if="editmodeheader==false"><label>Add new role after :<span style="color: red">*</span></label>
                                            <select class="form-control" tabindex="4" name="prerole" id="prerole" data-placeholder="Select Level"  ng-model="prerole" required>
                                                <option value="">Select Role</option>
                                                <option ng-repeat="dataObj in roleData" value="{{dataObj.id}}">{{dataObj.role_description}}</option>
                                            </select>
                                        </div>   
                                        <div class="col-lg-3 mb-3"><label>Role Name <span style="color: red">*</span></label>
                                            <input type="text" ng-disabled="recordid > -1" class="form-control" placeholder="Role Name" name="role_name" ng-model="role_name" required>
                                        </div>                                    
                                        <div class="col-lg-4 mb-3"><label><b>Remark</b><span style="color: red" ng-show="editmodeheader">* </span></label>
                                            <input type="text" class="form-control" name="remark" maxlength="150" ng-model="remark" ng-required="editmodeheader" >
                                        </div>
                                        <div class="col-lg-2 mb-3" ng-show="recordid > -1"><label><b>Active/Inactive</b></label>
                                            <label class="switch">
                                                <input type="checkbox" style="margin-top: 13px;" ng-model="active_status" ng-click="toggleSelection($event)">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>

                                    </div>


                                </div>
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-sm-12 text-center">
                                            <button class="btn btn-success btn-sm" ng-show="recordid == -1" ng-disabled="materialForm.$invalid"  ng-click="saveRole()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                            <button class="btn btn-success btn-sm" ng-show="recordid > -1" ng-disabled="materialForm.$invalid" ng-click="updateRole()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                            <button ng-click="hideForm()" class="btn btn-danger btn-sm" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Cancel &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="form-row" ng-show="roleData.length == 0">
                <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
            </div>
            <div class="form-row" style="margin-top:10px;" ng-show="roleData.length > 0">   
                <h2 class="f18">Role List</h2>
                <div class="table-responsive noscroll" >
                    <table  class="table custom-table">
                        <thead >
                            <tr>
                                <th>Role Name</th>
                                <th>Role Level</th>
                                <th>Last Modified By</th>
                                <th>Last Modified On</th>
                                <th>Action</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr dir-paginate="dataObj in roleData|itemsPerPage:records_per_page" total-items="total_records" style="background:none;">
                                <td>{{dataObj.role_description}}</td>
                                <td>{{$index}}</td>
                                <td>{{dataObj.modified_by!=null?dataObj.modified_by:dataObj.created_by}}</td>
                                <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on|format| date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td>
                                    <?php if ($edit) { ?>
                                        <a type="button" ng-if="dataObj.role_description != 'Admin' && dataObj.role_description != 'Operator'" ng-click="editRoleMaster(dataObj)"><i class="far fa-edit text-blue pr10"></i></a>
                                        <!--<button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.room_id)">Deactivate</button>-->
                                    <?php } else { ?>

                                        <a type="button" ng-if="dataObj.role_description != 'Admin' && dataObj.role_description != 'Operator'" class="btn btn-sm btn-info" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                        <!--<button class="btn btn-sm btn-info" ng-click="editRoomActivityMaster(dataObj)" disabled>Edit</button>-->
                                        <!--<button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.room_id)" disabled>Deactivate</button>-->
                                    <?php } ?>
                                </td>
                                <td ng-if="dataObj.is_active == '1'"   class="bg-success text-white">Active</td>
                                <td ng-if="dataObj.is_active == '0'" class="bg-danger text-white">Inactive</td>
                            </tr>
                        </tbody>
                    </table>
                    <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getRoleList(newPageNumber)"></dir-pagination-controls>
                </div>

            </div>
        </div>
    </div>	

</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>

<script type="text/javascript">
                                    var app = angular.module("materialApp", ['angular.chosen','angularUtils.directives.dirPagination']);
                                    app.filter('capitalizeWord', function () {

                                        return function (text) {

                                            return (!!text) ? text.charAt(0).toUpperCase() + text.substr(1).toLowerCase() : '';

                                        }

                                    });
                                    app.filter('format', function () {
                                        return function (item) {
                                            var t = item.split(/[- :]/);
                                            var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                            var time = d.getTime();
                                            return time;
                                        };
                                    });
                                    app.controller("materialCtrl", function ($scope, $http, $filter) {
                                        // Get Role List
                                        $scope.editmodeheader = false;
                                        $scope.recordid = -1;
                                        $scope.roleData = [];
                                        $scope.NewroleData = [];
                                        $scope.prerole = "";
                                        $scope.sno=0;
                                        $scope.page = 1;
                                        $scope.total_records = 0;
                                        $scope.records_per_page = 10;
                                        $scope.getRoleList = function (page) {
                                            $scope.page = page;
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList?page='+page,
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.roleData = response.data.role_list;
                                                $scope.NewroleData = $scope.roleData;
                                                $scope.total_records = response.data.total_records;
                                                $scope.records_per_page = response.data.records_per_page;
                                                console.log($scope.NewroleData);
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.getRoleList($scope.page);

                                        // Get Role Levels
                                        // $scope.rolelevelData = [];
                                        // $scope.getRoleLevelList = function () {
                                        //     $http({
                                        //         url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleLevelList',
                                        //         method: "GET",
                                        //         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        //     }).then(function (response) {
                                        //         $scope.rolelevelData = response.data.role_level_list;
                                        //         console.log($scope.rolelevelData);
                                        //     }, function (error) { // optional

                                        //         console.log("Something went wrong.Please try again");
                                        //     });
                                        // }
                                        // $scope.getRoleLevelList();

                                        $scope.showAddForm = false;
                                        $scope.showForm = function () {
                                            $scope.showAddForm = true;
                                        }
                                        
                                        $scope.hideForm = function () {
                                            $scope.editmodeheader = false;
                                            $scope.remark = "";
                                            $scope.status = "";
                                            $scope.recordid = -1;
                                            $scope.role_name = '';
                                            $scope.role_level = '';
                                            $scope.showAddForm = false;
                                        }

                                        $scope.saveRole = function () {
                                            if (confirm("Are you want to Save this Role!")) {
                                                $http({
                                                    url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/saveRole',
                                                    method: "POST",
                                                    data: "role_name=" + $scope.role_name + "&role_level=" + $scope.role_level + "&remark=" + $scope.remark + "&prerole=" + $scope.prerole,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                })
                                                        .then(function (response) {
                                                            alert(response.data.message);
                                                            $scope.editmodeheader = false;
                                                            $scope.prerole = "";
                                                            $scope.remark = "";
                                                            $scope.status = "";
                                                            $scope.recordid = -1;
                                                            $scope.role_name = '';
                                                            $scope.role_level = '';
                                                            $scope.showAddForm = false;
                                                            $scope.getRoleList($scope.page);
                                                        },
                                                                function (response) { // optional
                                                                    $scope.editmodeheader = false;
                                                                    $scope.prerole = "";
                                                                    $scope.remark = "";
                                                                    $scope.status = "";
                                                                    $scope.recordid = -1;
                                                                    $scope.role_name = '';
                                                                    $scope.role_level = '';
                                                                    $scope.showAddForm = false;
                                                                    $scope.getRoleList($scope.page);
                                                                    console.log("Something went wrong.Please try again");
                                                                });
                                            }
                                        }


                                        //Created By : Rahul Chauhan
                                        //Date : 21-07-2020
                                        $scope.status = "";
                                        $scope.active_status = false;
                                        $scope.toggleSelection = function toggleSelection(event) {
                                            if (event.target.checked == true) {
                                                $scope.active_status = true;
                                            } else {
                                                $scope.active_status = false;
                                            }
                                        };
                                        $scope.editRoleMaster = function (dataObj) {
                                            console.log(dataObj);
                                            $scope.editmodeheader = true;
                                            $scope.remark = "";
                                            $scope.recordid = dataObj.id;
                                            //$scope.prerole = $filter('filter')($scope.roleData, {id: (dataObj.id-1)})[0].id;
                                            $scope.role_name = dataObj.role_description;
                                            $scope.role_level = dataObj.role_level;
                                            $scope.showAddForm = true;
                                            if (dataObj.is_active == '1') {
                                                $scope.active_status = true;
                                            } else {
                                                $scope.active_status = false;
                                            }
                                        }

                                        $scope.updateRole = function () {
                                            if (confirm("Are you want to Update this Role!")) {
                                                $http({
                                                    url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/updateRole',
                                                    method: "POST",
                                                    data: "role_name=" + $scope.role_name + "&role_level=" + $scope.role_level + "&remark=" + $scope.remark + "&role_id=" + $scope.recordid + "&status=" + $scope.active_status,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                })
                                                        .then(function (response) {
                                                            alert(response.data.message);
                                                            $scope.editmodeheader = false;
                                                            $scope.remark = "";
                                                            $scope.status = "";
                                                            $scope.recordid = -1;
                                                            $scope.role_name = '';
                                                            $scope.role_level = '';
                                                            $scope.showAddForm = false;
                                                            $scope.getRoleList($scope.page);
                                                        },
                                                                function (response) { // optional
                                                                    $scope.editmodeheader = false;
                                                                    $scope.remark = "";
                                                                    $scope.status = "";
                                                                    $scope.recordid = -1;
                                                                    $scope.role_name = '';
                                                                    $scope.role_level = '';
                                                                    $scope.showAddForm = false;
                                                                    $scope.getRoleList($scope.page);
                                                                    console.log("Something went wrong.Please try again");
                                                                });
                                            }

                                        }

                                    });
</script>