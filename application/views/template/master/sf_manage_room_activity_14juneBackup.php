<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
     $edit = true;
            $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1500);</script>";
    }
}
?>
<div class="container-fluid" ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
    <!-- Page Heading -->

    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row">

                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">    	   
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th colspan="5" style="vertical-align:middle">Activity Details</th>
                                <th>
                                    <?php if ($add) { ?>
                                        <button class="btn btn-success" style="margin-right: -125px;" ng-click="showForm()">Add Room Activity</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success" style="margin-right: -125px;" ng-click="showForm()" disabled>Add Room Activity</button>
                                    <?php } ?>

                                </th>
                            </tr>
                        </thead>
                    </table>  
                </div>
                <div class="col-12">
                    <form name="materialForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row">   
                                        <div class="col-lg-4 mb-3"><label>Room Name <span style="color: red">*</span></label>
                                            <select class="chosen form-control" tabindex="4" name="room_id" data-placeholder="Search Room Code" ng-options="dataObj['room_id'] as (dataObj.room_code +  ' => ' +  dataObj.room_name +  ' => ' +  dataObj.area_id) for dataObj in sysRoomData"  ng-model="room_id" required chosen>
                                                <option value="">Select Room Name</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 mb-3"><label>Activity Name <span style="color: red">*</span></label>
                                            <select class="chzn-select form-control" tabindex="4" ng-change="getActivityInfo(activity_id)"  name="activity_id" data-placeholder="Search Activity" ng-options="dataObj['id'] as dataObj.activity_name for dataObj in activityList"  ng-model="activity_id" required chosen>
                                                <option value="">Select Activity</option>
                                            </select>
                                        </div>

                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-success btn-sm" ng-disabled="materialForm.$invalid"  ng-click="saveRoomActivity()" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="form-row" ng-show="roomActivityData.length == 0">
                <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
                <div class="col-lg-12 text-center" ><h4>(Please Select the above parameters)</h4></div> 
            </div>
            <div class="form-row" style="margin-top:10px;" ng-show="roomActivityData.length > 0">   
                <h2>Room Activity List</h2>
                <div class="table-responsive noscroll" >
                    <table  class="table custom-table">
                        <thead >
                            <tr>
                                <th>Room Name</th>
                                <th>Activity Name</th>
                                <th width="20%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="dataObj in roomActivityData">
                                <td>{{getRoomName(dataObj.room_id)}} ({{getRoomCode(dataObj.room_id)}})</td>
                                <td>{{dataObj.activity_name}}</td>
                                <td>
                                    <?php if ($edit) { ?>
                                        <button class="btn btn-sm btn-info" ng-click="editRoomActivityMaster(dataObj)">Edit</button>
                                        <button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)">Delete</button>
                                    <?php } else { ?>
                                        <button class="btn btn-sm btn-info" ng-click="editRoomActivityMaster(dataObj)" disabled>Edit</button>
                                        <button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)" disabled>Delete</button>
                                    <?php } ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>  
                </div>

            </div>
        </div>
    </div>	

</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>

<script type="text/javascript">
                                        var app = angular.module("materialApp", ['angular.chosen']);
                                        app.directive('chosen', function ($timeout) {

                                            var linker = function (scope, element, attr) {

                                                scope.$watch('sysRoomData', function () {
                                                    $timeout(function () {
                                                        element.trigger('chosen:updated');
                                                    }, 0, false);
                                                }, true);
                                                scope.$watch('activityList', function () {
                                                    $timeout(function () {
                                                        element.trigger('chosen:updated');
                                                    }, 0, false);
                                                }, true);
                                                $timeout(function () {
                                                    element.chosen();
                                                }, 0, false);
                                            };
                                            return {
                                                restrict: 'A',
                                                link: linker
                                            };
                                        });
                                        app.controller("materialCtrl", function ($scope, $http, $filter) {
                                            $scope.sysRoomData = [];
                                            $scope.getDeviceRoomList = function () {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist',
                                                    method: "GET",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    $scope.sysRoomData = response.data.room_list;
                                                }, function (error) { // optional

                                                    console.log("Something went wrong.Please try again");
                                                });
                                            }
                                            $scope.getDeviceRoomList();
                                            // Get Mst Activity List
                                            $scope.activityList = [];
                                            $scope.getMstActivityList = function () {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetMstActivityList',
                                                    method: "GET",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    $scope.activityList = response.data.mst_act_list;
                                                }, function (error) { // optional

                                                    console.log("Something went wrong.Please try again");
                                                });
                                            }
                                            $scope.getMstActivityList();
                                            $scope.showAddForm = false;
                                            $scope.showForm = function () {
                                                $scope.showAddForm = true;
                                            }
                                            $scope.getActivityInfo = function (objId) {
                                                $scope.activity_name = $filter('filter')($scope.activityList, {id: objId})[0].activity_name;
                                                $scope.activity_url = $filter('filter')($scope.activityList, {id: objId})[0].activity_url;
                                            }

                                            $scope.roomActivityData = [];
                                            $scope.getRoomActivityList = function () {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getactivitylistforroom',
                                                    method: "GET",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    $scope.roomActivityData = response.data.activity_list;
                                                }, function (error) { // optional

                                                    console.log("Something went wrong.Please try again");
                                                });
                                            }
                                            $scope.getRoomActivityList();
                                            $scope.saveRoomActivity = function () {
                                                if (confirm("Are you want to Save this Room Activity!")) {
                                                    $http({
                                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Addroomactivity',
                                                        method: "POST",
                                                        data: "activity_name=" + $scope.activity_name + "&activity_url=" + $scope.activity_url + "&room_id=" + $scope.room_id + "&mst_act_id=" + $scope.activity_id + "&recordid=" + $scope.recordid,
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    })
                                                            .then(function (response) {
                                                                alert(response.data.message);
                                                                $scope.activity_name = '';
                                                                $scope.room_id = '';
                                                                $scope.activity_id = "";
                                                                $scope.showAddForm = false;
                                                                $scope.getRoomActivityList();
                                                            },
                                                                    function (response) { // optional
                                                                        $scope.showAddForm = false;
                                                                        $scope.getRoomActivityList();
                                                                        console.log("Something went wrong.Please try again");
                                                                    });
                                                }
                                            }
                                            $scope.getRoomName = function (objId) {
                                                $scope.room_name = $filter('filter')($scope.sysRoomData, {room_id: objId})[0].room_name;
                                                return $scope.room_name;
                                            }
                                            $scope.getRoomCode = function (objId) {
                                                $scope.room_code = $filter('filter')($scope.sysRoomData, {room_id: objId})[0].room_code;
                                                return $scope.room_code;
                                            }

                                            //Created By : Bhupendra Kumar
                                            //Date : 18 March 2020
                                            //Decription : For Deactivate the Selected Record
                                            $scope.CommonDelete = function (id) {
                                                var tblname = "pts_trn_room_log_activity";
                                                var colname = "id";
                                                if (confirm("Are you really want to deactivate this ?")) {
                                                    $http({
                                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/CommonDelete',
                                                        method: "POST",
                                                        data: "id=" + id + "&tblname=" + tblname + "&colname=" + colname,
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    }).then(function (response) {
                                                        alert(response.data.message);
                                                        $scope.getRoomActivityList();
                                                    }, function (error) { // optional
                                                        alert(response.data.message);
                                                        //console.log("Something went wrong.Please try again");
                                                    });
                                                }
                                            }

                                            //Created By : Bhupendra Kumar
                                            //Date : 18 March 2020
                                            //Decription : To get values of selected record
                                            $scope.recordid = "";
                                            $scope.editRoomActivityMaster = function (dataObj) {
                                                console.log(dataObj);
                                                $scope.recordid = dataObj.id;
                                                $scope.room_id = dataObj.room_id;
                                                $scope.getRoomCode(dataObj.room_id);
                                                $scope.getRoomName(dataObj.room_id);
                                                $scope.activity_id = dataObj.mst_act_id;
                                                $scope.getActivityInfo(dataObj.mst_act_id);
                                                $scope.showAddForm = true;
                                            }

                                        });
</script>