<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="userApp" ng-controller="userCtrl" ng-cloak>
    <div class="content-wrapper">
        <div class="card card-default mt-4">
            <div class="card-body">
                <div class="form-row">
                    <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">
                        <table class="table table-dark">
                            <thead>
                                <tr>
                                    <th colspan="5" class="f18" style="vertical-align:middle">User Configuration</th>
                                    <th class="text-right">
                                        <?php if ($add) { ?>
                                            <button class="btn btn-success" ng-disabled="recordid > 0" ng-click="showForm()">Add (<i class="fas fa-plus"></i>)</button>
                                        <?php } else { ?>
                                            <button class="btn btn-success"  ng-click="showForm()"disabled>Add (+)</button>
                                        <?php } ?>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <form name="materialForm" novalidate="" class="ng-pristine ng-valid ng-valid-required">
                            <div class="card card-default" ng-show="showAddForm">
                                <div class="card-body">
                                    <div class="col-auto">
                                        <form>

                                            <fieldset>            
                                                <div class="col-auto">
                                                    <div class="form-row">   
                                                        <div class="col-lg-4 mb-3"><label for="aqty">Emp. Code: <span style="color: red">*</span></label>
                                                            <input class="form-control" maxlength="20" ng-trim="false" ng-disabled="recordid > 0" id="code" type="text" placeholder="Emp. Code" ng-change="checkEmpName(code, 'code')" name="code" ng-model="code" required>
                                                        </div>
                                                        <div class="col-lg-4 mb-3"><label for="edso">Emp. Name <span style="color: red">*</span></label>
                                                            <input class="form-control" maxlength="50" id="empname" type="text" placeholder="Emp. Name" ng-trim="false" ng-change="checkEmpName(empname, 'empname')" name="empname" ng-model="empname" required>
                                                        </div>
                                                        <div class="col-lg-4 mb-3"><label for="edso">Email Id: <span style="color: red">*</span></label>
                                                            <input class="form-control" id="email2" ng-disabled="recordid > 0" type="email" placeholder="Email Id" name="email2" ng-model="email2" required>
                                                        </div>

                                                    </div>


                                                    <div class="form-row">   
                                                        <div class="col-lg-8 mb-3"><label for="">Address </label>

                                                            <input class="form-control" type="text" placeholder="Address" name="add" id="add" maxlength="200" ng-model="add">                      
                                                        </div> 
                                                        <div class="col-lg-4 mb-3"><label for="">Contact No. </label>

                                                            <input class="form-control cc" type="text" maxlength="10" minlength="10" pattern="(.){10,10}" name="cont" id="cont" ng-model="cont" >
                                                            <span class="text-danger" ng-show="cont!='' && cont.length!=10">Please Provide Valid Phone Number</span>
                                                        </div> 				
                                                    </div>      

                                                    <div class="form-row">   
                                                        <div class="col-lg-3 mb-3"><label for="">Active Directory </label>
                                                            <div class="custom-control custom-checkbox mb-3">
                                                                <input type="checkbox" ng-disabled="true" class="custom-control-input" disabled id="activeCheck" name="is_directory" ng-click="checkActiveRadio1()" ng-model="is_directory">
                                                                <label class="custom-control-label" for="activeCheck"></label>
                                                            </div>             
                                                        </div> 
                                                        <div class="col-lg-3 mb-3"><label for="">User Id <span style="color: red">*</span></label>

                                                            <input class="form-control" ng-disabled="recordid > 0" type="text" autocomplete="off" maxlength="20" ng-change="specialAndAlphaNumeric(email)" name="email" id="email" ng-model="email" required>
                                                        </div>
                                                        <div class="col-lg-3 mb-3"><label for="">Generate Passwod <span style="color: red">*</span> </label>
                                                            <div class="custom-control custom-checkbox mb-3">
                                                                <input type="checkbox" class="custom-control-input"  id="customCheck"  name="is_generate" ng-click="checkActiveRadio()" ng-model="is_generate">
                                                                <label class="custom-control-label" for="customCheck"></label>
                                                            </div> 
    <!--<input class="form-control"  id="pwd" autocomplete="off" name="pwd" type="password" ng-model="pwd" required>-->                                        
                                                        </div> 
                                                        <div class="col-lg-3 mb-3"><label for="">Designation </label>

                                                            <input class="form-control" type="text" name="desg" id="desg" ng-model="desg" maxlength="50">                         
                                                        </div> 				
                                                    </div>   

                                                    <div class="form-row">

                                                        <div class="col-lg-6 mb-3">
                                                            <label>Role : <span style="color: red">*</span></label>
                                                            <select class="form-control" name="role" id="role" ng-model="role" required>
                                                                <option value="" selected>Select Here</option>
                                                                <option value="{{dataObj.id}}" ng-repeat="dataObj in roleData">{{dataObj.role_description}}</option>

                                                            </select>
                                                        </div>

                                                        <div class="col-lg-6 mb-3">
                                                            <label>Block : <span style="color: red">*</span></label>
                                                            <select multiple chosen name="block" id="block" data-placeholder="Select Block" class="chzn-select form-control" ng-change="getSubBlockList()" ng-model="block" ng-options="dataObj['block_code'] as (dataObj.block_code +                                          ' | ' +                                          dataObj.block_name) for dataObj in blockData" required>
                                                                <option value="" disabled>Select Block</option>
                                                            </select>

                                                        </div>								
                                                    </div>             

                                                    <!--                                                    <div class="form-row">
                                                                                                            <div class="col-lg-12 mb-3"><label for="validationServer01">Sub Block </label>
                                                                                                                <select multiple ng-disabled="true" id="subblock" chosen name="subblock" class="chzn-select form-control" ng-model="subblock" ng-options="(dataObj['department_code']) as (dataObj.department_code +                      ' | ' +                      dataObj.department_name) for dataObj in departmentData">
                                                                                                                    <option value="">Select Sub Block Code</option>
                                                                                                                </select>
                                                                                                            </div>
                                                    
                                                                                                        </div>-->
                                                    <div class="form-row">
                                                        <div class="col-lg-12 mb-3"><label for="validationServer01">Area</label>

                                                            <select multiple id="area" chosen name="area"id="area" data-placeholder="Select Area" class="chzn-select form-control" ng-change="geRoomList()" ng-model="area" ng-options="(dataObj['area_code']) as (dataObj.area_code +                                          ' | ' +                                          dataObj.area_name) for dataObj in areaList">
                                                                <option value="" disabled>Select Area</option>
                                                            </select>

                                                        </div>

                                                    </div>
                                                    <div class="form-row">
                                                        <div class="col-lg-12 mb-3"><label for="validationServer01">Room </label>

                                                            <select multiple id="room" chosen name="room" id="room" data-placeholder="Select Room" class="chzn-select form-control" ng-model="room" ng-options="(dataObj['room_code']) as (dataObj.room_code +                                          ' | ' +                                          dataObj.room_name) for dataObj in roomList">
                                                                <option value="" disabled>Select Room</option>
                                                            </select>
                                                        </div>

                                                    </div>


                                                    <div class="form-row">
                                                        <div class="col-lg-6 mb-3"><label for="validationServer01">Remark <span style="color: red" ng-show="editmodeheader">* </span></label>
                                                            <textarea class="form-control" aria-label="With textarea" maxlength="100" name="remark" ng-model="remark" ng-required="editmodeheader" placeholder="Enter Remarks"></textarea>
                                                        </div>
                                                        <div class="col-lg-2 mb-3" ng-show="recordid > 0"><label><b>Active/Inactive</b></label>
                                                            <label class="switch">
                                                                <input type="checkbox" style="margin-top: 13px;" ng-model="active_status" ng-click="toggleSelection($event)">
                                                                <span class="slider round"></span>
                                                            </label>

                                                        </div>
                                                        <div class="col-lg-2 mb-3" ng-show="recordid > 0"><label><b>Block/Unblock</b></label>
                                                            <label class="switch">
                                                                <input type="checkbox" style="margin-top: 13px;" id="blockCheck"  name="is_blocked" ng-model="is_blocked" ng-click="toggleSelection1($event)">
                                                                <span class="slider round"></span>
                                                            </label>
                                                            <!--                                                            <div class="custom-control custom-checkbox mb-3">
                                                                                                                            <input type="checkbox" class="custom-control-input"  id="blockCheck"  name="is_blocked" ng-model="is_blocked">
                                                                                                                            <label class="custom-control-label" for="blockCheck"></label>
                                                                                                                        </div> -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>

                                        </form>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-sm-12 text-center">
                                            <button class="btn btn-success btn-sm" ng-show="recordid == ''" ng-disabled="materialForm.$invalid" ng-click="saveUser()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                            <button class="btn btn-success btn-sm" ng-show="recordid > 0" ng-disabled="materialForm.$invalid" ng-click="updateUser()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                            <button class="btn btn-danger btn-sm" ng-click="hideForm()" type="button">&nbsp;&nbsp;&nbsp;&nbsp; Cancel &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-row" style="margin-top:10px;" >
                                    <h2 class="col-md-12 f18">User List</h2>
                                    <div class="table-responsive noscroll" >
                                    <table class="table custom-table">
                                        <thead>
                                            <tr>
                                                <th>Emp. Code</th>
                                                <th>Emp. Name</th>
                                                <th>User Id</th>
                                                <th>Contact</th>
                                                <th>Role</th>
                                                <th>Block</th>
                                                <th>Area</th>
                                                <th>Room</th>
                                                <th>Last Modified By</th>
                                                <th>Last Modified On</th>
                                                <th>Action</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr dir-paginate="dataObj in userList|itemsPerPage:records_per_page" total-items="total_records" style="background:none;" ng-show="dataObj.emp_name != 'Super Admin'">
                                                <td>{{dataObj.emp_code}}</td>
                                                <td>{{dataObj.emp_name}}</td>
                                                <td>{{dataObj.emp_email}}</td>
                                                <td>{{dataObj.emp_contact}}</td> 
                                                <td>{{dataObj.role_id>0?getRoleName(dataObj.role_id).role_description:''}}</td>
                                                <td>{{dataObj.block_code}}</td>
                                                <td>{{dataObj.area_code}}</td>
                                                <td>{{dataObj.room_code}}</td>
                                                <td>{{dataObj.modified_by!=NULL?dataObj.modified_by:dataObj.created_by}}</td>
                                                <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                                <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                                <td>
                                                    <?php if ($edit) { ?>
                                                        <a ng-click="editUser(dataObj)"><i class="far fa-edit text-blue pr10" title="Edit"></i></a>
                                                    <?php } else { ?>
                                                        <a type="button" class="btn btn-sm btn-info" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                                    <?php } ?>
                                                </td>
                                                <td ng-if="dataObj.is_active == '1'"   class="bg-success text-white">Active</td>
                                                <td ng-if="dataObj.is_active == '0'"   class="bg-danger text-white">Inactive</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getUserList(newPageNumber)"></dir-pagination-controls>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script type="text/javascript">
                                                    var app = angular.module("userApp", ['angular.chosen', 'angularUtils.directives.dirPagination']);
                                                    app.directive('chosen', function ($timeout) {

                                                        var linker = function (scope, element, attr) {
                                                            scope.$watch('blockData', function () {
                                                                $timeout(function () {
                                                                    element.trigger('chosen:updated');
                                                                }, 0, false);
                                                            }, true);
                                                            scope.$watch('departmentData', function () {
                                                                $timeout(function () {
                                                                    element.trigger('chosen:updated');
                                                                }, 0, false);
                                                            }, true);
                                                            scope.$watch('areaList', function () {
                                                                $timeout(function () {
                                                                    element.trigger('chosen:updated');
                                                                }, 0, false);
                                                            }, true);
                                                            scope.$watch('roomList', function () {
                                                                $timeout(function () {
                                                                    element.trigger('chosen:updated');
                                                                }, 0, false);
                                                            }, true);
                                                            $timeout(function () {
                                                                element.chosen();
                                                            }, 0, false);
                                                        };
                                                        return {
                                                            restrict: 'A',
                                                            link: linker
                                                        };
                                                    });

                                                    app.directive('onlyDigits', function () {
                                                        return {
                                                            require: 'ngModel',
                                                            restrict: 'A',
                                                            link: function (scope, element, attr, ctrl) {
                                                                function inputValue(val) {
                                                                    if (val) {
                                                                        var digits = val.replace(/[^0-9.]/g, '');

                                                                        if (digits.split('.').length > 2) {
                                                                            digits = digits.substring(0, digits.length - 1);
                                                                        }

                                                                        if (digits !== val) {
                                                                            ctrl.$setViewValue(digits);
                                                                            ctrl.$render();
                                                                        }
                                                                        return parseFloat(digits);
                                                                    }
                                                                    return undefined;
                                                                }
                                                                ctrl.$parsers.push(inputValue);
                                                            }
                                                        };
                                                    });
                                                    app.filter('format', function () {
                                                        return function (item) {
                                                            var t = item.split(/[- :]/);
                                                            var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                                            var time = d.getTime();
                                                            return time;
                                                        };
                                                    });
                                                    app.filter('capitalizeWord', function () {

                                                        return function (text) {

                                                            return (!!text) ? text.charAt(0).toUpperCase() + text.substr(1).toLowerCase() : '';

                                                        }

                                                    });
                                                    app.controller("userCtrl", function ($scope, $http, $filter) {
                                                        $scope.showAddForm = false;
                                                        $scope.showForm = function () {
                                                            $scope.showAddForm = true;
                                                        }
                                                        //Get Role List
                                                        $scope.roleData = [];
                                                        $scope.getRoleList = function () {
                                                            $http({
                                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList?status=active',
                                                                method: "GET",
                                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                            }).then(function (response) {
                                                                $scope.roleData = response.data.role_list;
                                                            }, function (error) { // optional
                                                                $scope.roleData = [];
                                                                console.log("Something went wrong.Please try again");

                                                            });
                                                        }
                                                        $scope.getRoleList();
                                                        //Get Block List
                                                        $scope.blockData = [];
                                                        $scope.getBlockList = function () {
                                                            $http({
                                                                url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getBlockList',
                                                                method: "GET",
                                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                            }).then(function (response) {
                                                                $scope.blockData = response.data.block_list;
                                                            }, function (error) { // optional
                                                                $scope.blockData = [];
                                                                console.log("Something went wrong.Please try again");

                                                            });
                                                        }
                                                        $scope.getBlockList();
                                                        //Get Sub Block List
                                                        $scope.departmentData = [];
                                                        $scope.getSubBlockList = function () {
                                                            $scope.room = [];
                                                            $scope.roomList = [];
                                                            $scope.area = [];
                                                            $scope.areaList = [];
                                                            $http({
                                                                url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getSubBlockList?block_array=' + $scope.block,
                                                                method: "GET",
                                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                            }).then(function (response) {
                                                                $scope.departmentData = response.data.department_list;
                                                                if ($scope.departmentData.length > 0) {
                                                                    angular.forEach($scope.departmentData, function (value, key) {
                                                                        $scope.subblock.push(value.department_code);
                                                                    });
                                                                    $scope.getAreaList();
                                                                } else {
                                                                    $scope.areasubblock = [];
                                                                }


                                                            }, function (error) { // optional
                                                                $scope.departmentData = [];
                                                                console.log("Something went wrong.Please try again");

                                                            });
                                                        }
                                                        //Get Area Master List
                                                        $scope.areaList = [];
                                                        $scope.getAreaList = function () {
                                                            $http({
                                                                url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getAreaList?block_array=' + $scope.block,
                                                                method: "GET",
                                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                            }).then(function (response) {
                                                                $scope.areaList = response.data.area_list;
                                                            }, function (error) { // optional
                                                                $scope.areaList = [];
                                                                console.log("Something went wrong.Please try again");

                                                            });
                                                        }



                                                        //Get Room List
                                                        $scope.roomList = [];
//                                                        $scope.geRoomList = function () {
//                                                            $scope.roomList = [];
//                                                            $http({
//                                                                url: 'ponta_sahib/Mastercontroller/getRoomList?area_array=' + $scope.area,
//                                                                method: "GET",
//                                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
//                                                            }).then(function (response) {
//                                                                $scope.roomList = response.data.room_list;
//                                                            }, function (error) { // optional
//                                                                $scope.roomList = [];
//                                                                console.log("Something went wrong.Please try again");
//
//                                                            });
//                                                        }
                                                        $scope.geRoomList = function () {
                                                            $scope.roomList = [];
                                                            $http({
                                                                url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getRoomList?area_array=' + $scope.area + "&use_for=userconfig",
                                                                method: "GET",
                                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                            }).then(function (response) {
                                                                $scope.roomList = response.data.room_list;
                                                            }, function (error) { // optional
                                                                $scope.roomList = [];
                                                                console.log("Something went wrong.Please try again");

                                                            });
                                                        }



                                                        //Get User List
                                                        $scope.userList = [];
                                                        $scope.page = 1;
                                                        $scope.total_records = 0;
                                                        $scope.records_per_page = 10;
                                                        $scope.getUserList = function (page) {
                                                            $scope.page = page;
                                                            $http({
                                                                url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getUserList?module=master&page=' + page,
                                                                method: "GET",
                                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                            }).then(function (response) {
                                                                $scope.userList = response.data.user_list;
                                                                $scope.total_records = response.data.total_records;
                                                                $scope.records_per_page = response.data.records_per_page;
                                                            }, function (error) { // optional
                                                                $scope.userList = [];
                                                                console.log("Something went wrong.Please try again");

                                                            });
                                                        }
                                                        $scope.getUserList($scope.page);
                                                        $scope.getRoleName = function (objId) {
                                                        if(typeof objId != 'undefined' && objId > 0){
                                                          var role = $filter('filter')($scope.roleData, {id: objId})[0];
                                                        }else{
                                                            var role={};
                                                        }
                                                          return role;   
                                                        }

                                                        $scope.cont = "";
                                                        $scope.remark = "";
                                                        $scope.desg = "";
                                                        $scope.is_generate = true;
                                                        $scope.is_directory = false;
                                                        $scope.subblock = [];
                                                        $scope.area = [];
                                                        $scope.block = [];
                                                        $scope.room = [];
                                                        $scope.checkActiveRadio = function () {
                                                            if ($scope.recordid > 0) {

                                                            } else {
                                                                if ($scope.is_generate) {
                                                                    $scope.is_directory = false;
                                                                } else {
                                                                    $scope.is_directory = true;
                                                                }
                                                            }

                                                        }
                                                        $scope.checkActiveRadio1 = function () {
                                                            if ($scope.recordid > 0) {

                                                            } else {
                                                                if ($scope.is_directory) {
                                                                    $scope.is_generate = false;
                                                                } else {
                                                                    $scope.is_generate = true;
                                                                }
                                                            }

                                                        }
                                                        $scope.hideForm = function () {
                                                            $scope.recordid = "";
                                                            $scope.editmodeheader = false;
                                                            $scope.code = "";
                                                            $scope.empname = "";
                                                            $scope.email2 = "";
                                                            $scope.add = "";
                                                            $scope.cont = "";
                                                            $scope.email = "";
                                                            $scope.pwd = "";
                                                            $scope.desg = "";
                                                            $scope.role = "";
                                                            $scope.block = [];
                                                            $scope.is_generate = true;
                                                            $scope.is_directory = true;
                                                            $scope.subblock = [];
                                                            $scope.area = [];
                                                            $scope.room = [];
                                                            $scope.remark = "";
                                                            $scope.showAddForm = false;
                                                            location.reload();
                                                        }

                                                        $scope.checkEmpName = function (str, model) {
                                                            var firstChar = str.charAt(0);
                                                            if (firstChar <= '9' && firstChar >= '0') {
                                                                alert("It should not start with numeric value");
                                                                if (model == 'code') {
                                                                    $scope.code = "";
                                                                } else if (model == 'empname') {
                                                                    $scope.empname = "";
                                                                } else {
                                                                    $scope.email = "";
                                                                }

                                                            }
                                                            if (str.match("^[a-zA-Z0-9][a-zA-Z0-9 ]*$")) {
                                                            } else {
                                                                if (model == 'code') {
                                                                    $scope.code = "";
                                                                } else if (model == 'empname') {
                                                                    $scope.empname = "";
                                                                } else {
                                                                    $scope.email = "";
                                                                }
                                                                alert('Please fill alphanumeric value');
                                                            }
                                                        }

                                                        $scope.specialAndAlphaNumeric = function(inputtext){
                                                            if(inputtext.match("^[a-zA-Z0-9@%#&._-]*$")){
                                                                return true;
                                                            }else{
                                                                alert('Please fill alphanumeric and special characters only');
                                                                $scope.email = '';
                                                                return false;
                                                            }
                                                        }
                                                        $scope.saveUser = function () {
//                                                            var newPassword = document.getElementById('pwd').value;
//                                                            var upperCount = newPassword.replace(/[^A-Z]/g, "").length;
//                                                            var lowerCount = newPassword.replace(/[^a-z]/g, "").length;
//                                                            var numberCount = newPassword.replace(/[^0-9]/g, "").length;
//                                                            var minNumberofChars = 8;
//                                                            var maxNumberofChars = 16;
//                                                            var regularExpression = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,16}/;
//                                                            var empname = document.getElementById('empname').value;
//                                                            var cont = document.getElementById('cont').value;
//                                                            var userid = document.getElementById('email').value;
//                                                            var name = empname.toUpperCase().split(" ");
//                                                            var firstChar = newPassword.charAt(0);
//                                                            if (firstChar <= '9' && firstChar >= '0') {
//                                                                alert("Password should not Start with Numeric Value");
//                                                                return false;
//                                                            }
//
//                                                            for (var i = 0; i <= name.length; i++) {
//                                                                var tempnew = newPassword.toUpperCase();
//                                                                if (tempnew.indexOf(name[i]) != -1) {
//                                                                    alert("Password should not contain First Name or Surname");
//                                                                    return false;
//                                                                }
//                                                            }
//                                                            if (newPassword.toUpperCase().indexOf(userid.toUpperCase()) != -1) {
//                                                                alert("Password should not contain Userid");
//                                                                return false;
//                                                            }
//                                                            if (newPassword.indexOf(cont) != -1 && cont != '') {
//                                                                alert("Password should not contain Telephone Numbers");
//                                                                return false;
//                                                            }
//                                                            if (newPassword.length < minNumberofChars) {
//                                                                alert("Password should be atleast 8 character");
//                                                                return false;
//                                                            }
//                                                            if (upperCount < 1) {
//                                                                alert("Password should contain at least one upper case letter: (A – Z)");
//                                                                return false;
//                                                            }
//                                                            if (lowerCount < 1) {
//                                                                alert("Password should contain at least one lower case letter: (a – z)");
//                                                                return false;
//                                                            }
//                                                            if (numberCount < 1) {
//                                                                alert("Password should contain at least one number: (0 – 9)");
//                                                                return false;
//                                                            }
//
//                                                            if (!regularExpression.test(newPassword)) {
//                                                                alert("Password should contain at least one Special Characters");
//                                                                return false;
//                                                            }
                                                            if (confirm("Do You Want To Save This Record?")) {
                                                                var pass = makeRandomPass(5);
//                                                                var pass = 'TEST123';
                                                                var pass1 = SHA256(pass);
                                                                $http({
                                                                    url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/AddNewEmployee',
                                                                    method: "POST",
                                                                    data: "code=" + $scope.code + "&empname=" + $scope.empname + "&email2=" + $scope.email2 + "&add=" + $scope.add + "&cont=" + $scope.cont + "&email=" + $scope.email + "&is_generate=" + $scope.is_generate + "&is_directory=" + $scope.is_directory + "&desg=" + $scope.desg + "&block=" + $scope.block + "&area=" + $scope.area + "&room_code=" + $scope.room + "&subblock=" + $scope.subblock + "&role=" + $scope.role + "&remark=" + $scope.remark + "&emp_password=" + pass1 + "&mail_pass=" + pass,
                                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                                })
                                                                        .then(function (response) {
                                                                            if (response.data.result == true) {
                                                                                alert(response.data.message);
                                                                                $scope.recordid = "";
                                                                                $scope.editmodeheader = false;
                                                                                $scope.code = "";
                                                                                $scope.empname = "";
                                                                                $scope.email2 = "";
                                                                                $scope.add = "";
                                                                                $scope.cont = "";
                                                                                $scope.email = "";
                                                                                $scope.pwd = "";
                                                                                $scope.desg = "";
                                                                                $scope.role = "";
                                                                                $scope.block = [];
                                                                                $scope.subblock = [];
                                                                                $scope.area = [];
                                                                                $scope.room = [];
                                                                                $scope.remark = "";
                                                                                $scope.showAddForm = false;
                                                                                location.reload();
                                                                                $scope.getUserList($scope.page);
                                                                            } else {
                                                                                alert(response.data.message);
                                                                            }
                                                                        },
                                                                                function (response) { // optional
                                                                                    alert(response.data.message);
                                                                                    $scope.recordid = "";
                                                                                    $scope.editmodeheader = false;
                                                                                    $scope.code = "";
                                                                                    $scope.empname = "";
                                                                                    $scope.email2 = "";
                                                                                    $scope.add = "";
                                                                                    $scope.cont = "";
                                                                                    $scope.email = "";
                                                                                    $scope.pwd = "";
                                                                                    $scope.desg = "";
                                                                                    $scope.role = "";
                                                                                    $scope.block = [];
                                                                                    $scope.subblock = [];
                                                                                    $scope.area = [];
                                                                                    $scope.room = [];
                                                                                    $scope.remark = "";
                                                                                    $scope.showAddForm = false;
                                                                                    location.reload();
                                                                                    $scope.getUserList($scope.page);
                                                                                    console.log("Something went wrong.Please try again");

                                                                                });
                                                            }

                                                        }
                                                        $scope.toggleSelection = function toggleSelection(event) {
                                                            if (event.target.checked == true) {
                                                                $scope.active_status = true;
                                                            } else {
                                                                $scope.active_status = false;
                                                            }
                                                        };
                                                        $scope.toggleSelection1 = function toggleSelection1(event) {
                                                            if (event.target.checked == true) {
                                                                $scope.is_blocked = true;
                                                            } else {
                                                                $scope.is_blocked = false;
                                                            }
                                                        };
                                                        $scope.is_blocked = false;
                                                        $scope.recordid = "";
                                                        $scope.editPass = "";
                                                        $scope.editUser = function (dataObj) {
                                                            $scope.editUser2(dataObj);
//                                                            $scope.editPass = dataObj.emp_password;

                                                            $http({
                                                                url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/checkBlock',
                                                                method: "POST",
                                                                data: "field_name=block_code&field_value=" + dataObj.block_code + "&table_name=mst_block",
                                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                            }).then(function (response) {
                                                                if (response.data.parent_data.is_active == 0) {
                                                                    $scope.showAddForm = false;
                                                                    alert("Any Block is inactive.First activate Inactive Block.");
                                                                } else {
                                                                    $scope.recordid = dataObj.id;
                                                                    if (dataObj.is_blocked == '0') {
                                                                        $scope.is_blocked = false;
                                                                    } else {
                                                                        $scope.is_blocked = true;
                                                                    }
                                                                    $scope.editmodeheader = true;
                                                                    $scope.code = dataObj.emp_code;
                                                                    $scope.empname = dataObj.emp_name;
                                                                    $scope.email2 = dataObj.email2;
                                                                    $scope.add = dataObj.emp_address;
                                                                    if (dataObj.emp_contact > 0) {
                                                                        $scope.cont = dataObj.emp_contact;
                                                                    } else {
                                                                        $scope.cont = "";
                                                                    }
                                                                    $scope.email = dataObj.emp_email;
//                                                                    $scope.pwd = dataObj.emp_password;
                                                                    $scope.desg = dataObj.designation_code;
                                                                    $scope.role = dataObj.role_id;
                                                                    if (dataObj.block_code != '' || dataObj.block_code != 'NULL') {
                                                                        $scope.block = dataObj.block_code.split(',');
                                                                    } else {
                                                                        $scope.block = [];
                                                                    }
                                                                    $scope.getSubBlockList();
                                                                    $scope.getAreaList();
                                                                    if (dataObj.Sub_block_code != '' || dataObj.Sub_block_code != 'NULL') {
                                                                        $scope.subblock = dataObj.Sub_block_code.split(',');
                                                                    } else {
                                                                        $scope.subblock = [];
                                                                    }
                                                                    if (dataObj.area_code != '' || dataObj.area_code != 'NULL') {
                                                                        $scope.area = dataObj.area_code.split(',');
                                                                    } else {
                                                                        $scope.area = [];
                                                                    }
                                                                    $scope.geRoomList();
                                                                    
                                                                    if (dataObj.room_code != '' || dataObj.room_code != 'NULL') {
                                                                        $scope.room = dataObj.room_code.split(',');
                                                                    } else {
                                                                        $scope.room = [];
                                                                    }
                                                                    $scope.remark = "";
                                                                    $scope.is_generate = false;

                                                                    if (dataObj.is_directory == '1') {
                                                                        $scope.is_directory = true;
                                                                        $('[name="is_generated"]').attr('disabled', true);
                                                                    } else {
                                                                        $scope.is_directory = false;
                                                                    }
                                                                    $scope.showAddForm = true;
                                                                    if (dataObj.is_active == '1') {
                                                                        $scope.active_status = true;
                                                                    } else {
                                                                        $scope.active_status = false;
                                                                    }
                                                                }
                                                            }, function (error) { // optional
                                                            });
                                                        }
                                                        $scope.editUser2 = function (dataObj) {
                                                            
//                                                            $scope.editPass = dataObj.emp_password;

                                                            $http({
                                                                url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/checkBlock',
                                                                method: "POST",
                                                                data: "field_name=block_code&field_value=" + dataObj.block_code + "&table_name=mst_block",
                                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                            }).then(function (response) {
                                                                if (response.data.parent_data.is_active == 0) {
                                                                    $scope.showAddForm = false;
                                                                    alert("Any Block is inactive.First activate Inactive Block.");
                                                                } else {
                                                                    $scope.recordid = dataObj.id;
                                                                    if (dataObj.is_blocked == '0') {
                                                                        $scope.is_blocked = false;
                                                                    } else {
                                                                        $scope.is_blocked = true;
                                                                    }
                                                                    $scope.editmodeheader = true;
                                                                    $scope.code = dataObj.emp_code;
                                                                    $scope.empname = dataObj.emp_name;
                                                                    $scope.email2 = dataObj.email2;
                                                                    $scope.add = dataObj.emp_address;
                                                                    if (dataObj.emp_contact > 0) {
                                                                        $scope.cont = dataObj.emp_contact;
                                                                    } else {
                                                                        $scope.cont = "";
                                                                    }
                                                                    $scope.email = dataObj.emp_email;
//                                                                    $scope.pwd = dataObj.emp_password;
                                                                    $scope.desg = dataObj.designation_code;
                                                                    $scope.role = dataObj.role_id;
                                                                    if (dataObj.block_code != '' || dataObj.block_code != 'NULL') {
                                                                        $scope.block = dataObj.block_code.split(',');
                                                                    } else {
                                                                        $scope.block = [];
                                                                    }
                                                                    $scope.getSubBlockList();
                                                                    $scope.getAreaList();
                                                                    if (dataObj.Sub_block_code != '' || dataObj.Sub_block_code != 'NULL') {
                                                                        $scope.subblock = dataObj.Sub_block_code.split(',');
                                                                    } else {
                                                                        $scope.subblock = [];
                                                                    }
                                                                    if (dataObj.area_code != '' || dataObj.area_code != 'NULL') {
                                                                        $scope.area = dataObj.area_code.split(',');
                                                                    } else {
                                                                        $scope.area = [];
                                                                    }
                                                                    $scope.geRoomList();
                                                                    
                                                                    if (dataObj.room_code != '' || dataObj.room_code != 'NULL') {
                                                                        $scope.room = dataObj.room_code.split(',');
                                                                    } else {
                                                                        $scope.room = [];
                                                                    }
                                                                    $scope.remark = "";
                                                                    $scope.is_generate = false;

                                                                    if (dataObj.is_directory == '1') {
                                                                        $scope.is_directory = true;
                                                                        $('[name="is_generated"]').attr('disabled', true);
                                                                    } else {
                                                                        $scope.is_directory = false;
                                                                    }
                                                                    $scope.showAddForm = true;
                                                                    if (dataObj.is_active == '1') {
                                                                        $scope.active_status = true;
                                                                    } else {
                                                                        $scope.active_status = false;
                                                                    }
                                                                }
                                                            }, function (error) { // optional
                                                            });
                                                        }
                                                        $scope.updateUser = function () {
//                                                            var newPassword = document.getElementById('pwd').value;
//                                                            var upperCount = newPassword.replace(/[^A-Z]/g, "").length;
//                                                            var lowerCount = newPassword.replace(/[^a-z]/g, "").length;
//                                                            var numberCount = newPassword.replace(/[^0-9]/g, "").length;
//                                                            var minNumberofChars = 8;
//                                                            var maxNumberofChars = 16;
//                                                            var regularExpression = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,16}/;
//                                                            var empname = document.getElementById('empname').value;
//                                                            var cont = document.getElementById('cont').value;
//                                                            var userid = document.getElementById('email').value;
//                                                            var name = empname.toUpperCase().split(" ");
//                                                            var firstChar = newPassword.charAt(0);
//                                                            if (newPassword != $scope.editPass) {
//                                                                if (firstChar <= '9' && firstChar >= '0') {
//                                                                    alert("Password should not Start with Numeric Value");
//                                                                    return false;
//                                                                }
//
//                                                                for (var i = 0; i <= name.length; i++) {
//                                                                    var tempnew = newPassword.toUpperCase();
//                                                                    if (tempnew.indexOf(name[i]) != -1) {
//                                                                        alert("Password should not contain First Name or Surname");
//                                                                        return false;
//                                                                    }
//                                                                }
//                                                                if (newPassword.toUpperCase().indexOf(userid.toUpperCase()) != -1) {
//                                                                    alert("Password should not contain Userid");
//                                                                    return false;
//                                                                }
//                                                                if (newPassword.indexOf(cont) != -1 && cont != '') {
//                                                                    alert("Password should not contain Telephone Numbers");
//                                                                    return false;
//                                                                }
//                                                                if (newPassword.length < minNumberofChars) {
//                                                                    alert("Password should be atleast 8 character");
//                                                                    return false;
//                                                                }
//                                                                if (upperCount < 1) {
//                                                                    alert("Password should contain at least one upper case letter: (A – Z)");
//                                                                    return false;
//                                                                }
//                                                                if (lowerCount < 1) {
//                                                                    alert("Password should contain at least one lower case letter: (a – z)");
//                                                                    return false;
//                                                                }
//                                                                if (numberCount < 1) {
//                                                                    alert("Password should contain at least one number: (0 – 9)");
//                                                                    return false;
//                                                                }
//
//                                                                if (!regularExpression.test(newPassword)) {
//                                                                    alert("Password should contain at least one Special Characters");
//                                                                    return false;
//                                                                }
//                                                            }
                                                            if (confirm("Do You Want To Update This Record?")) {
//                                                                if ($scope.pwd == $scope.editPass) {
//                                                                    $scope.pwd = $scope.editPass;
//                                                                } else {
//                                                                    $scope.pwd = SHA256($scope.pwd);
//                                                                }
                                                                var pass = makeRandomPass(5);
//                                                                var pass = 'TEST123';
                                                                var pass1 = SHA256(pass);
                                                                $http({
                                                                    url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/UpdateEmployee',
                                                                    method: "POST",
                                                                    data: "code=" + $scope.code + "&empname=" + $scope.empname + "&email2=" + $scope.email2 + "&add=" + $scope.add + "&cont=" + $scope.cont + "&email=" + $scope.email + "&is_generate=" + $scope.is_generate + "&is_directory=" + $scope.is_directory + "&desg=" + $scope.desg + "&block=" + $scope.block + "&area=" + $scope.area + "&room_code=" + $scope.room + "&subblock=" + $scope.subblock + "&role=" + $scope.role + "&remark=" + $scope.remark + "&hdd=" + $scope.recordid + "&status=" + $scope.active_status + "&is_blocked=" + $scope.is_blocked + "&emp_password=" + pass1 + "&mail_pass=" + pass,
                                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                                })
                                                                        .then(function (response) {
                                                                            if (response.data.result == true) {
                                                                                alert(response.data.message);
                                                                                $scope.recordid = "";
                                                                                $scope.editmodeheader = false;
                                                                                $scope.editPass = "";
                                                                                $scope.code = "";
                                                                                $scope.empname = "";
                                                                                $scope.email2 = "";
                                                                                $scope.add = "";
                                                                                $scope.cont = "";
                                                                                $scope.email = "";
                                                                                $scope.pwd = "";
                                                                                $scope.desg = "";
                                                                                $scope.role = "";
                                                                                $scope.block = [];
                                                                                $scope.subblock = [];
                                                                                $scope.area = [];
                                                                                $scope.room = [];
                                                                                $scope.remark = "";
                                                                                $scope.showAddForm = false;
                                                                                $scope.getUserList($scope.page);
                                                                            } else {
                                                                                alert(response.data.message);
                                                                            }
                                                                        },
                                                                                function (response) { // optional
                                                                                    alert(response.data.message);
                                                                                    $scope.recordid = "";
                                                                                    $scope.editPass = "";
                                                                                    $scope.editmodeheader = false;
                                                                                    $scope.code = "";
                                                                                    $scope.empname = "";
                                                                                    $scope.email2 = "";
                                                                                    $scope.add = "";
                                                                                    $scope.cont = "";
                                                                                    $scope.email = "";
                                                                                    $scope.pwd = "";
                                                                                    $scope.desg = "";
                                                                                    $scope.role = "";
                                                                                    $scope.block = [];
                                                                                    $scope.subblock = [];
                                                                                    $scope.area = [];
                                                                                    $scope.room = [];
                                                                                    $scope.remark = "";
                                                                                    $scope.showAddForm = false;
                                                                                    $scope.getUserList($scope.page);
                                                                                    console.log("Something went wrong.Please try again");

                                                                                });
                                                            }

                                                        }


                                                        function makeRandomPass(length) {
                                                            var result = '';
                                                            var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                                                            var charactersLength = characters.length;
                                                            for (var i = 0; i < length; i++) {
                                                                result += characters.charAt(Math.floor(Math.random() * charactersLength));
                                                            }
                                                            return result;
                                                        }

                                                    });
                                                    $(document).ready(function () {
                                                        $('.cc').bind('keyup paste', function () {
                                                            this.value = this.value.replace(/[^0-9]/g, '');
                                                        });

                                                    });
</script>

<script>
    /**
     *  Secure Hash Algorithm (SHA256)
     *  http://www.webtoolkit.info/
     *  Original code by Angel Marin, Paul Johnston
     **/

    function SHA256(s) {
        var chrsz = 8;
        var hexcase = 0;
        function safe_add(x, y) {
            var lsw = (x & 0xFFFF) + (y & 0xFFFF);
            var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
            return (msw << 16) | (lsw & 0xFFFF);
        }

        function S(X, n) {
            return (X >>> n) | (X << (32 - n));
        }
        function R(X, n) {
            return (X >>> n);
        }
        function Ch(x, y, z) {
            return ((x & y) ^ ((~x) & z));
        }
        function Maj(x, y, z) {
            return ((x & y) ^ (x & z) ^ (y & z));
        }
        function Sigma0256(x) {
            return (S(x, 2) ^ S(x, 13) ^ S(x, 22));
        }
        function Sigma1256(x) {
            return (S(x, 6) ^ S(x, 11) ^ S(x, 25));
        }
        function Gamma0256(x) {
            return (S(x, 7) ^ S(x, 18) ^ R(x, 3));
        }
        function Gamma1256(x) {
            return (S(x, 17) ^ S(x, 19) ^ R(x, 10));
        }

        function core_sha256(m, l) {
            var K = new Array(0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786, 0xFC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA, 0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147, 0x6CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070, 0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2);
            var HASH = new Array(0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19);
            var W = new Array(64);
            var a, b, c, d, e, f, g, h, i, j;
            var T1, T2;

            m[l >> 5] |= 0x80 << (24 - l % 32);
            m[((l + 64 >> 9) << 4) + 15] = l;

            for (var i = 0; i < m.length; i += 16) {
                a = HASH[0];
                b = HASH[1];
                c = HASH[2];
                d = HASH[3];
                e = HASH[4];
                f = HASH[5];
                g = HASH[6];
                h = HASH[7];

                for (var j = 0; j < 64; j++) {
                    if (j < 16)
                        W[j] = m[j + i];
                    else
                        W[j] = safe_add(safe_add(safe_add(Gamma1256(W[j - 2]), W[j - 7]), Gamma0256(W[j - 15])), W[j - 16]);

                    T1 = safe_add(safe_add(safe_add(safe_add(h, Sigma1256(e)), Ch(e, f, g)), K[j]), W[j]);
                    T2 = safe_add(Sigma0256(a), Maj(a, b, c));

                    h = g;
                    g = f;
                    f = e;
                    e = safe_add(d, T1);
                    d = c;
                    c = b;
                    b = a;
                    a = safe_add(T1, T2);
                }

                HASH[0] = safe_add(a, HASH[0]);
                HASH[1] = safe_add(b, HASH[1]);
                HASH[2] = safe_add(c, HASH[2]);
                HASH[3] = safe_add(d, HASH[3]);
                HASH[4] = safe_add(e, HASH[4]);
                HASH[5] = safe_add(f, HASH[5]);
                HASH[6] = safe_add(g, HASH[6]);
                HASH[7] = safe_add(h, HASH[7]);
            }
            return HASH;
        }

        function str2binb(str) {
            var bin = Array();
            var mask = (1 << chrsz) - 1;
            for (var i = 0; i < str.length * chrsz; i += chrsz) {
                bin[i >> 5] |= (str.charCodeAt(i / chrsz) & mask) << (24 - i % 32);
            }
            return bin;
        }

        function Utf8Encode(string) {
            string = string.replace(/\r\n/g, "\n");
            var utftext = "";

            for (var n = 0; n < string.length; n++) {

                var c = string.charCodeAt(n);

                if (c < 128) {
                    utftext += String.fromCharCode(c);
                } else if ((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                } else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }

            }

            return utftext;
        }

        function binb2hex(binarray) {
            var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
            var str = "";
            for (var i = 0; i < binarray.length * 4; i++) {
                str += hex_tab.charAt((binarray[i >> 2] >> ((3 - i % 4) * 8 + 4)) & 0xF) +
                        hex_tab.charAt((binarray[i >> 2] >> ((3 - i % 4) * 8)) & 0xF);
            }
            return str;
        }

        s = Utf8Encode(s);
        return binb2hex(core_sha256(str2binb(s), s.length * chrsz));
    }
</script>
<script>
    function Encrypt(str) {
        if (!str)
            str = "";
        str = (str == "undefined" || str == "null") ? "" : str;
        try {
            var key = 146;
            var pos = 0;
            ostr = '';
            while (pos < str.length) {
                ostr = ostr + String.fromCharCode(str.charCodeAt(pos) ^ key);
                pos += 1;
            }

            return ostr;
        } catch (ex) {
            return '';
        }
    }

    function Decrypt(str) {
        if (!str)
            str = "";
        str = (str == "undefined" || str == "null") ? "" : str;
        try {
            var key = 146;
            var pos = 0;
            ostr = '';
            while (pos < str.length) {
                ostr = ostr + String.fromCharCode(key ^ str.charCodeAt(pos));
                pos += 1;
            }

            return ostr;
        } catch (ex) {
            return '';
        }
    }
</script>
<script type="text/javascript">
    <!--
    
        var keyStr = "ABCDEFGH                IJKLMNOP" +
                "QRST                UVWXYZabcdef" +
                "                ghijklmnopqrstuv" +
                             "wxyz012345678    9        +/" +
                "=";
    
        function encode64(input) {
        input = escape(input);
        var output = "";
        var chr1, chr2, chr3 = "";
        var enc1, enc2, enc3, enc4 = "";
        var i = 0;

        do {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
                    keyStr.charAt(enc1) +
                    keyStr.charAt(enc2) +
                    keyStr.charAt(enc3) +
                    keyStr.charAt(enc4);
            chr1 = chr2 = chr3 = "";
            enc1 = enc2 = enc3 = enc4 = "";
        } while (i < input.length);

        return output;
    }

    function decode64(input) {
        var output = "";
        var chr1, chr2, chr3 = "";
        var enc1, enc2, enc3, enc4 = "";
        var i = 0;

        // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
        var base64test = /[^A-Za-z0-9\+\/\=]/g;
        if (base64test.exec(input)) {
            alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
        }
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        do {
            enc1 = keyStr.indexOf(input.charAt(i++));
            enc2 = keyStr.indexOf(input.charAt(i++));
            enc3 = keyStr.indexOf(input.charAt(i++));
            enc4 = keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

            chr1 = chr2 = chr3 = "";
            enc1 = enc2 = enc3 = enc4 = "";

        } while (i < input.length);

        return unescape(output);
    }

    //--></script>