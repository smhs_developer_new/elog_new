<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid">
    <!-- Page Heading -->

    <div class="content-wrapper">
        <div class="content-heading executesop-heading">
            <div class="col-sm-5 pl-0">Create Drain Point</div>
            <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
            <div class="col-sm-7 pr-0">
                <ol class="breadcrumb ml-auto">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
                    <li class="breadcrumb-item active"><a href="<?= base_url() ?>drain_point_view?module_id=<?php echo $_GET['module_id'] ?>">Drain point</a></li>

                </ol>
            </div>
        </div>



        <div class="card card-default">
            <div class="card-body">                     
                <div class="row">
                    <div class="col-sm-12 text-right">

                        <a class="btn btn-primary btn-lg" href="<?= base_url() ?>drain_point_view?module_id=<?php echo $_GET['module_id'] ?>" > Back To list</a>  
                        &nbsp;&nbsp;&nbsp;
                        <!-- <?php if ($add) { ?><a class="btn btn-primary btn-lg" href="<?= base_url() ?>batch_drain_point"> Excel Upload</a>
                        <?php } else { ?>
                            <button class="btn btn-primary btn-lg"  disabled> Excel Upload</button>
                        <?php } ?> -->
                        

                    </div>
                </div>
            </div>
        </div>

        <form id="drainpoint">
            <div class="card card-default">
                <div class="card-body">

                    <fieldset>

                        <div class="col-auto">
                            <!-- <div class="form-row">
                                <div class="col-lg-12 mb-3"><label for="validationServer01">Sanitization Used <span style="color: red">*</span></label><input class="form-control autocomplete" type="text" placeholder="Sanitization Used" name="solutionname" id="solutionname" required> 
                                </div>
                             </div>   -->
                            <div class="form-row">   
                                <div class="col-lg-4 mb-3"><label for="aqty">Darin Point Code <span style="color: red">*</span></label><input class="form-control" maxlength="20" id="dpcode" type="text" placeholder="Drain point Code" name="dpcode" required >
                                </div>
                                <div class="col-lg-4 mb-3"><label for="edso">Drain point Name <span style="color: red">*</span></label><input class="form-control" maxlength="50" id="dpname" type="text" placeholder="Drain Point Name" name="dpname" required >
                                </div>
                                <div class="col-lg-4 mb-3"><label for="validationServer01">Remarks </label><textarea class="form-control" aria-label="With textarea" maxlength="100" name="dpremarks" placeholder="Enter Remarks" ></textarea>

                                </div>

                            </div>

                            <div class="form-row">


<!--<div class="col-lg-6 mb-3"><label for="validationServer01">Room Code <span style="color: red">* </span></label>
<select id="rcode" name="rcode" class="form-control" required>
<option value="" selected disabled>Select Room Code </option>
                                <?php if ($data['rcode']->num_rows() > 0) { ?>
                                    <?php foreach ($data['rcode']->result() as $row) { ?>
        <option value="<?php echo $row->room_code ?>"><?php echo $row->room_code ?> </option>
                                    <?php }
                                } ?> 
</select>

</div> 
<!--  <div class="col-lg-4 mb-3"><label for="validationServer01">Remarks <span style="color: red">*</span></label><textarea class="form-control" aria-label="With textarea" name="rremarks" placeholder="Enter Remarks" required></textarea>
   
</div>-->

                                <div class="col-lg-6 mb-3"><label for="validationServer01">Solution Code </label>
                                    <select id="scode" name="scode" class="form-control">
                                        <option value="" selected disabled>Select Solution Code </option>
                                        <?php if ($data['scode']->num_rows() > 0) { ?>
                                            <?php foreach ($data['scode']->result() as $row) { ?>
                                                <option value="<?php echo $row->solution_code ?>"><?php echo $row->solution_code ?> | <?php echo $row->solution_name ?> </option>
    <?php }
} ?> 
                                    </select>

                                </div>






                            </div>
                    </fieldset>
                </div>
            </div>
            <div class="card card-default">

                <div class="card-body" >


                    <div class="row disable-button-color">
                        <div class="col-sm-12 text-center" id="sbtn">
                                                                <?php if ($add) { ?>
                                <button id="sbbtn" class="btn btn-success btn-lg" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
<?php } else { ?>
                                <button class="btn btn-success btn-lg" type="button" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
<?php } ?>
                        </div>
                    </div>


                </div>
            </div><!-- END card-->

        </form>

    </div>




</div>

<script type="text/javascript">
    $(document).ready(function () {

        $("#dpcode").change(function () {
            var dpcode = $(this).val();
            $.ajax({
                url: '<?= base_url() ?>ponta_sahib/Mastercontroller/checkDraincode',
                type: 'POST',
                data: {dpcode: dpcode},
                success: function (res) {
                    if (res.status == 0) {
                        alert("Drain Point Code Already Exists");
                        $("#dpcode").val("");
                        return false;
                    }
                }
            });
        });

        $('#drainpoint').submit(function () {
            $.ajax({
                url: '<?= base_url() ?>ponta_sahib/Mastercontroller/mstDrainpointSubmit',
                type: 'POST',
                data: $(this).serialize(),
                success: function (res) {
                    if (res.status == 2) {
                        alert("Record already exists at this drainpoint code...!");
                        window.location.href = "<?= base_url() ?>drain_point_view?module_id=<?php echo $_GET['module_id']?>";
                    } else if (res.status == 2) {
                        alert("Drain Point Added successfully");
                        window.location.href = "<?= base_url() ?>drain_point_view?module_id=<?php echo $_GET['module_id']?>";
                    }
                }
            });
            return false;
        });


    });
</script>