<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="smhs" content="">

    <title>E-Logbook</title>

    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url(); ?>assets/css/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>css/fontfamily.css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php echo base_url(); ?>css/sb-admin-2.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/chosen.min.css"> 

    <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
    <!-- Bootstrap core JavaScript-->



    <?php
//    $diff = time() - $_SESSION['__ci_last_regenerate'];
//
//    if ($diff > 300) {
//        redirect(base_url() . 'User/logout');
//    } else {
//        $_SESSION['__ci_last_regenerate'] = time(); //set new timestamp
//    }
    ?>

</head>