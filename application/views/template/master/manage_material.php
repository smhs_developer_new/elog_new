<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Bootstrap Admin App">
        <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
        <link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>favicon.ico">
        <title>E-Logbook</title>
        <!-- =============== VENDOR STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/brands.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/regular.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/solid.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/fontawesome.css">
        <!-- SIMPLE LINE ICONS-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/simple-line-icons/css/simple-line-icons.css">
        <!-- ANIMATE.CSS-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/animate.css/animate.css">
        <!-- WHIRL (spinners)-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/whirl/dist/whirl.css">
        <!-- =============== PAGE VENDOR STYLES ===============-->
        <!-- =============== BOOTSTRAP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css" id="bscss">
        <!-- =============== APP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/app.css" id="maincss">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/Ponta_style.css">



        <!-- TAGS INPUT-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.css"><!-- SLIDER CTRL-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-slider/dist/css/bootstrap-slider.css"><!-- CHOSEN-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/chosen-js/chosen.css"><!-- DATETIMEPICKER-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.css"><!-- COLORPICKER-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css"><!-- SELECT2-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/select2/dist/css/select2.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.css"><!-- WYSIWYG-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-wysiwyg/css/style.css"><!-- =============== BOOTSTRAP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />
        <style>
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
        </style>
    </head>

    <body ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
        <div class="wrapper">
            <!-- top navbar-->
            <header class="topnavbar-wrapper"> 
                <!-- START Top Navbar-->
                <nav class="navbar topnavbar"> 
                    <!-- START navbar header-->
                    <div class="navbar-header"><a class="navbar-brand" href="<?php echo base_url(); ?>home">
                            <div class="brand-logo font-weight-bold text-white">SMHS - eLog System</div>
                            <div class="brand-logo-collapsed font-weight-bold text-white">SMHS</div>
                        </a></div>
                    <!-- END navbar header--> 
                    <!-- START Left navbar-->
                    <ul class="navbar-nav mr-auto flex-row">
                        <li class="nav-item"> 
                            <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link d-none d-md-block d-lg-block d-xl-block" href="#" data-trigger-resize="" data-toggle-state="aside-collapsed"><em class="fas fa-bars"></em></a><!-- Button to show/hide the sidebar on mobile. Visible on mobile only.--><a class="nav-link sidebar-toggle d-md-none" href="#" data-toggle-state="aside-toggled" data-no-persist="true"><em class="fas fa-bars"></em></a></li>
                        <!-- START User avatar toggle-->
                        <li class="nav-item d-none d-md-block"> 
                            <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link" id="user-block-toggle" href="#user-block" data-toggle="collapse"><em class="icon-user"></em></a></li>
                        <!-- END User avatar toggle--> 
                    </ul>
                    <!-- END Left navbar--> 
                    <!-- START Right Navbar-->
                    <ul class="navbar-nav flex-row">
                        <!-- Fullscreen (only desktops)-->
                        <li class="nav-item d-none d-md-block"><a class="nav-link" href="<?php echo base_url(); ?>" data-toggle-fullscreen=""><em class="fas fa-expand"></em></a></li>
                        <!-- START Offsidebar button-->
                        <li class="nav-item"><a href="<?php echo base_url(); ?>#" class="nav-link"></a></li>


                        <!-- END Offsidebar menu-->
                    </ul>
                    <!-- END Right Navbar--> 

                </nav>
                <!-- END Top Navbar--> 
            </header>
            <!-- sidebar-->
            <aside class="aside-container"> 
                <!-- START Sidebar (left)-->
                <div class="aside-inner">
                    <nav class="sidebar" data-sidebar-anyclick-close=""> 
                        <!-- START sidebar nav-->
                        <ul class="sidebar-nav">
                            <!-- START user info-->
                            <li class="has-user-block">
                                <div class="collapse" id="user-block">
                                    <div class="item user-block"> 
                                        <!-- User picture-->
                                        <div class="user-block-picture">
                                            <div class="user-block-status"><img class="img-thumbnail rounded-circle" src="<?php echo base_url(); ?>img/user/02.jpg" alt="Avatar" width="60" height="60">
                                                <div class="circle bg-success circle-lg"></div>
                                            </div>
                                        </div>
                                        <!-- Name and Job-->
                                        <div class="user-block-info"><span class="user-block-name">Hello, <?php echo $this->session->userdata('empname') ?></span><span class="user-block-role"><?php if ($this->session->userdata('empname')) { ?><a href="<?php echo base_url() ?>user/logout" >Logout </a><?php } ?></span></div>
                                    </div>
                                </div>
                            </li>
                            <!-- END user info--> 
                            <!-- Iterates over all sidebar items-->
                            <li class="nav-heading text-bold"><span data-localize="sidebar.heading.HEADER">Main Navigation</span>
                            <li>
                                <a href="<?php echo base_url(); ?>User/home" title="Home"><em class="fas fa-home"></em><span data-localize="sidebar.nav.Plant master">Home</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>manage_material" title="Material master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Material master">Manage Material</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>manage_room" title="Room master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Room master">Manage Room</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>manage_room_activity" title="Room Activity master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Room Activity master">Manage Room Activity</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>balance_master" title="Balance master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Balance master">Manage Balance Data</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>balance_frequency_master" title="Balance Frequency master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Balance Frequency Master">Balance Frequency Data</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>tablet_tooling_master" title="Tablet Tooling master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Tooling master">Tablet Tooling</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>manage_id_standard_weight" title="Id Standard Weight master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Id Standard Weight master">Manage Id Standard Weight</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>work_assign_asper_rolebase" title="Work Assign Master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Work Assign Master">Work Assign Master</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>workflow" title="Work Flow"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Work Flow">Work Flow</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>line_log_master" title="Line Log Master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Line Log Master">Line Log Master</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>instrument_master" title="Instrument Master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Instrument Master">Instrument Master</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>stage_master" title="Stage Master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Stage Master">Stage Master</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>report_header" title="Report Header Master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Header Master">Header Master</span></a>
                            </li>
                        </ul>
                        <!-- END sidebar nav--> 
                    </nav>
                </div>
                <!-- END Sidebar (left)--> 
            </aside>
            <!-- Main section-->
          <!--<section>-->
            <!-- Page content-->
            <section class="section-container"> 
                <!-- Page content-->
                <div class="content-wrapper">
                    <div class="content-heading executesop-heading">
                        <div class="col-sm-5 pl-0">Material Details</div>
                    </div>
                    <div class="card card-default">
                        <div class="card-body">                     
                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <button class="btn btn-primary btn-lg" ng-click="showForm()"> Add Material</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form name="materialForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row">   
                                        <div class="col-lg-4 mb-3"><label>Room Code <span style="color: red">*</span></label>
                                            <select class="chzn-select form-control" tabindex="4"  name="room_code" data-placeholder="Search Room Code" ng-options="dataObj['room_code'] as dataObj.room_code for dataObj in roomData"  ng-model="room_code" required chosen>
                                                <option value="">Select Room Code</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 mb-3"><label>Name <span style="color: red">*</span></label>
                                            <input class="form-control" maxlength="50" type="text" placeholder="Material Name" name="material_name" ng-model="material_name" required>
                                        </div>
                                         <div class="col-lg-4 mb-3"><label>Code <span style="color: red">*</span></label>
                                            <input class="form-control" type="text" maxlength="20" placeholder="Material Code" name="material_code" ng-model="material_code" required>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-lg-4 mb-3"><label>Description <span style="color: red">*</span></label>
                                            <textarea class="form-control" maxlength="1000" aria-label="With textarea" name="material_desc" placeholder="Material Description" ng-model="material_desc" required></textarea>
                                        </div>
                                        <div class="col-lg-4 mb-3"><label>Type <span style="color: red">*</span></label>
                                            <input class="form-control cc" id="pcontact" type="text" placeholder="Material Type" name="material_type" ng-model="material_type" required>
                                        </div>
                                        <div class="col-lg-4 mb-3"><label>Batch No.<span style="color: red">*</span></label>
                                            <input class="form-control cc" type="text" placeholder="Material Batch No." name="material_batch_no" ng-model="material_batch_no" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-4 mb-3"><label>Relocation Type <span style="color: red">*</span></label>
                                            <input class="form-control cc" type="text" placeholder="Material Relocation Type" name="material_relocation_type" ng-model="material_relocation_type" required>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-success btn-lg" ng-disabled="materialForm.$invalid"  ng-click="saveMaterial()" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        <button id="reset" class="btn btn-danger btn-lg" type="reset">&nbsp;&nbsp;&nbsp;&nbsp;RESET &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="row" ng-show="materialData.length > 0">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10"><h2>Material List</h2></div>
                                    
                            </div>
                            <div class="row" ng-show="materialData.length== 0">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10"><h2>No Material Found</h2></div>
                                    
                            </div>
                            <table class="table table-dark" ng-show="materialData.length > 0">
                                <thead>
                                    <tr>
                                        <th>Room Code</th>
                                        <th>Name</th>
                                        <th>Code</th>
                                        <th>Description</th>
                                        <th>Type</th>
                                        <th>Batch No.</th>
                                        <th>Relocation Type</th>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="dataObj in materialData">
                                        <td>{{dataObj.room_code}}</td>
                                        <td>{{dataObj.material_name}}</td>
                                        <td>{{dataObj.material_code}}</td>
                                        <td>{{dataObj.material_desc}}</td>
                                        <td>{{dataObj.material_type}}</td>
                                        <td>{{dataObj.material_batch_no}}</td>
                                        <td>{{dataObj.material_relocation_type}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
                    </div>
                </div>
            </section>
        </div>
        <!--</section>-->
        <!-- Page footer-->
        <!-- Page footer-->

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>js/angular.min.js"></script>
    <script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
    <script type="text/javascript">
                                            var app = angular.module("materialApp", ['angular.chosen']);
                                            app.directive('chosen', function ($timeout) {

                                                var linker = function (scope, element, attr) {

                                                    scope.$watch('roomData', function () {
                                                        $timeout(function () {
                                                            element.trigger('chosen:updated');
                                                        }, 0, false);
                                                    }, true);
                                                    $timeout(function () {
                                                        element.chosen();
                                                    }, 0, false);
                                                };
                                                return {
                                                    restrict: 'A',
                                                    link: linker
                                                };
                                            });
                                            app.controller("materialCtrl", function ($scope, $http, $filter) {
                                                $scope.device_id = "59.144.179.216";
                                                $scope.roomData = [];
                                                $scope.getRoomList = function () {
                                                    $http({
                                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist?system_id=' + $scope.device_id,
                                                        method: "GET",
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    }).then(function (response) {
                                                        $scope.roomData = response.data.room_list;
                                                    }, function (error) { // optional

                                                        console.log("Something went wrong.Please try again");

                                                    });
                                                }
                                                $scope.getRoomList();
                                                $scope.showAddForm = false;
                                                $scope.showForm = function () {
                                                    $scope.showAddForm = true;
                                                }
                                                $scope.materialData = [];
                                                $scope.getMaterialList = function () {
                                                    $http({
                                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getmaterialrelocation',
                                                        method: "GET",
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    }).then(function (response) {
                                                        $scope.materialData = response.data.material_relocation;
                                                    }, function (error) { // optional

                                                        console.log("Something went wrong.Please try again");

                                                    });
                                                }
                                                $scope.getMaterialList();

                                                $scope.saveMaterial = function () {
                                                    if (confirm("Do You Want To Save This Record?")) {
                                                        $http({
                                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Addmaterialrelocation',
                                                            method: "POST",
                                                            data: "material_name=" + $scope.material_name + "&material_desc=" + $scope.material_desc + "&room_code=" + $scope.room_code + "&material_type=" + $scope.material_type + "&material_batch_no=" + $scope.material_batch_no + "&material_relocation_type=" + $scope.material_relocation_type+"&material_code="+$scope.material_code,
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        })
                                                                .then(function (response) {
                                                                    angular.element('#reset').trigger('click');
                                                                    $scope.room_code="";
                                                                    $scope.showAddForm = false;
                                                                    $scope.getMaterialList();
                                                                },
                                                                        function (response) { // optional
                                                                            $scope.showAddForm = false;
                                                                            $scope.getMaterialList();
                                                                            console.log("Something went wrong.Please try again");

                                                                        });
                                                    }
                                                }

                                            });
    </script>
</body>
</html>