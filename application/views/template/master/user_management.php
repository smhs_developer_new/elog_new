<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="usermgmtApp" ng-controller="usermgmtCtrl" ng-cloak>

    <!-- Page Heading -->
    <div class="content-heading executesop-heading mb-4">
        <div class="col-sm-12 font-weight-bold pl-0 f18">Roles and Responsibility</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->

    </div>
    <div class="card card-default">
        <div class="card-body">
            <div class="form-row">
                <div class="col-lg-6 mb-3">
                    <label>Select User ID & type of  Access :</label>
                    <select class="chzn-select form-control" tabindex="4" name="user_name" data-placeholder="Search User"  ng-model="user_id" ng-change="selectUser()" required chosen>
                        <option value="">Select User Name</option>
                        <option value="{{dataObj.id}}" ng-hide="dataObj.emp_name == 'Super Admin'" ng-repeat="dataObj in userListData">{{dataObj.emp_email}} / {{dataObj.emp_name}}</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-4 mb-4">
                    <div class="radio-bg">
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" ng-disabled="user_id == ''" id="master"  name="defaultExampleRadios" ng-click="setDefaultValue('master')" value="master" ng-model="module_name">
                            <label class="custom-control-label lh26 black-text" for="master">Master</label>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 mb-4">
                    <div class="radio-bg">
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" ng-disabled="user_id == ''" id="report" ng-model="module_name" ng-click="setDefaultValue('report')" value="report" name="defaultExampleRadios">
                            <label class="custom-control-label lh26 black-text" for="report">Report</label>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 mb-4">
                    <div class="radio-bg">
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" ng-disabled="user_id == ''" id="log" ng-model="module_name" ng-click="setDefaultValue('log')" value="log" name="defaultExampleRadios">
                            <label class="custom-control-label lh26 black-text" for="log">Log</label>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div class="card card-default" ng-show="activity_name != '' && user_id != ''">
        <div class="card-body">
            <div class="card" ng-show="activity_name == 'master'">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link f15" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <strong> Master </strong>
                        </button>
                    </h2>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="row" ng-repeat="dataObj in masterListData">
                            <div class="col-lg-3 mb-3">
                                {{dataObj.master_name}}
                            </div>
                            <div class="col-lg-9 mb-9">
                                <div class="form-row">
                                    <div class="col-lg-4 mb-4">

                                        <div class="custom-control custom-radio">
                                            <input type="checkbox" class="custom-control-input" id="create{{$index}}"  ng-model="dataObj.is_create" value="create" name="create{{$index}}">
                                            <label class="custom-control-label lh26 black-text" for="create{{$index}}">Create</label>

                                        </div>
                                    </div>

                                    <div class="col-lg-4 mb-4">

                                        <div class="custom-control custom-radio">
                                            <input type="checkbox" class="custom-control-input" id="update{{$index}}"  ng-model="dataObj.is_edit" ng-click="isChecked(dataObj, $index)" value="update" name="update{{$index}}">
                                            <label class="custom-control-label lh26 black-text" for="update{{$index}}">Edit & Update</label>

                                        </div>

                                    </div>
                                    <div class="col-lg-4 mb-4">

                                        <div class="custom-control custom-radio">
                                            <input type="checkbox" class="custom-control-input" id="view{{$index}}"ng-disabled="dataObj.is_edit"  ng-model="dataObj.is_view" value="view" name="view{{$index}}">
                                            <label class="custom-control-label lh26 black-text" for="view{{$index}}">View</label>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row mt20">
                            <div class="col-lg-4"></div>
                            <div class="mt-3 col-lg-4 text-center">
                            <?php if ($add || $edit) { ?>
                                <button class="btn btn-primary ml15 btn-icon-split" ng-click="saveMasterPrivilege(masterListData, 'master')">
                                    <span class="text">Submit</span>
                                </button>
                            <?php } else { ?>
                                <button id="sbbtn" class="btn btn-success btn-sm" ng-click="saveRole()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card" ng-show="activity_name == 'report'">
                <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed f15" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <strong> Report </strong>
                        </button>
                    </h2>
                </div>
                <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="row" ng-repeat="dataObj in reportList">
                            <div class="col-lg-3 mb-3">
                                {{dataObj.report_name}}
                            </div>
                            <div class="col-lg-9 mb-9">

                                <div class="form-row">
                                    <div class="col-lg-4 mb-4">

                                        <div class="custom-control custom-radio">
                                            <input type="checkbox" class="custom-control-input" id="reportview{{$index}}" value="view" ng-click="setActive(dataObj, $index)"  ng-model="dataObj.is_view" name="reportview{{$index}}">
                                            <label class="custom-control-label lh26 black-text" for="reportview{{$index}}">View</label>

                                        </div>
                                    </div>

                                    <div class="col-lg-4 mb-4">

                                        <div class="custom-control custom-radio">
                                            <input type="checkbox" class="custom-control-input" id="reportprint{{$index}}" ng-model="dataObj.is_edit" ng-click="setActiveprint(dataObj, $index)" value="edit" name="reportprint{{$index}}">
                                            <label class="custom-control-label lh26 black-text" for="reportprint{{$index}}">View & Print</label>

                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="row mt20">
                            <div class="col-lg-4"></div>
                            <div class="mt-3 col-lg-4 text-center">
                            <?php if ($add || $edit) { ?>
                                <button class="btn btn-primary ml15 btn-icon-split" ng-click="saveMasterPrivilege(reportList, 'report')">
                                    <span class="text">Submit</button>
                            <?php } else { ?>
                                <button id="sbbtn" class="btn btn-success btn-lg" type="submit" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                            <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card" ng-show="activity_name == 'log' && user_room != ''">
                <div class="card-header" id="headingfour">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed f15" type="button" data-toggle="collapse" data-target="#headingfour" aria-expanded="false" aria-controls="collapseThree">
                            <strong> Log </strong>
                        </button>
                    </h2>
                </div>
                <!--                                <div id="headingfour" class="collapse show" aria-labelledby="headingfour" data-parent="#accordionExample">
                                                    <div class="card-body">
                                                        <div class="row" ng-repeat="dataObj in activityList">
                                                            <div class="col-lg-3 mb-3">
                                                                {{dataObj.activity_name}}
                                                            </div>
                                                            <div class="col-lg-9 mb-9">
                                                                <div class="form-row">
                                                                    <div class="col-lg-4 mb-4">
                                
                                                                        <div class="custom-control custom-radio">
                                                                            <input type="checkbox" class="custom-control-input" id="logcreate{{$index}}" value="create" ng-model="dataObj.is_create"  name="logcreate{{$index}}">
                                                                            <label class="custom-control-label lh26 black-text" for="logcreate{{$index}}">Create</label>
                                
                                                                        </div>
                                                                    </div>
                                
                                                                    <div class="col-lg-4 mb-4">
                                
                                                                        <div class="custom-control custom-radio">
                                                                            <input type="checkbox" class="custom-control-input" id="logupdate{{$index}}" value="update"  ng-model="dataObj.is_edit" ng-click="isCheckedLog(dataObj, $index)" name="logupdate{{$index}}">
                                                                            <label class="custom-control-label lh26 black-text" for="logupdate{{$index}}">Edit & Update</label>
                                
                                                                        </div>
                                
                                                                    </div>
                                                                    <div class="col-lg-4 mb-4">
                                
                                                                        <div class="custom-control custom-radio">
                                                                            <input type="checkbox" class="custom-control-input" id="logview{{$index}}" value="view" ng-disabled="dataObj.is_edit" ng-model="dataObj.is_view" name="logview{{$index}}">
                                                                            <label class="custom-control-label lh26 black-text" for="logview{{$index}}">View</label>
                                                                        </div>
                                
                                                                    </div>
                                
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row text-center mb0 dspblk mt20">
                                                            <hr class="sidebar-divider">
                                                                                        <a href="#" class="btn btn-danger btn-icon-split">
                                                                                            <span class="text">Cancel</span>
                                                                                        </a>
                                                            <button class="btn btn-primary ml15 btn-icon-split" ng-click="saveMasterPrivilege(activityList, 'log')">
                                                                <span class="text">Save</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>-->
                <div id="headingfour" class="collapse show" aria-labelledby="headingfour" data-parent="#accordionExample">
                    <div class="card-body">
                        <form name="logForm" id="logForm" novalidate>
                            <div class="form-row">
                                <div class="col-lg-4 mb-6">
                                    <label>Select Room:</label>
                                    <select class="chzn-select form-control" tabindex="4" name="room_id" data-placeholder="Search Room Code" ng-change="getProcessLogActivity(room_id)" ng-options="dataObj.room_id as (dataObj.room_code +                  ' => ' +                  dataObj.room_name +                  ' => ' +                  dataObj.area_id) for dataObj in sysRoomData"  ng-model="room_id" required chosen>
                                        <option value="">Select Room Name</option>
                                    </select>
                                </div>

                                <div class="col-lg-4 mb-6"   ng-if="editlog">
                                    <label>Select Log :</label>
                                    <select class="chzn-select form-control" ng-change="getActivityInfo(activity_id)"  name="activity_id" data-placeholder="Search Log" ng-options="dataObj['mst_act_id'] as dataObj.activity_name for dataObj in processActivityList"  ng-model="activity_id" required chosen>
                                        <option value="">Select Log</option>
                                    </select>

                                </div>
                                <div class="col-lg-4 mb-6" ng-if="!editlog"  >
                                    <label>Select Log :</label>
                                    <select  class="chzn-select form-control" ng-change="getActivityInfo(activity_id)"  name="activity_id" data-placeholder="Search Log" ng-options="dataObj['mst_act_id'] as dataObj.activity_name for dataObj in processActivityListadd"  ng-model="activity_id" required chosen>
                                        <option value="">Select Log</option>
                                    </select>
                                </div>

                                <div class="col-lg-4 mb-6" ng-show="showtype">
                                    <label>Select Activity :</label>
                                    <select multiple class="chzn-select form-control" data-placeholder="Search Activity"   name="activity_type"  ng-model="activity_type" chosen>
                                        <!--                                        <option ng-repeat="dataObj in masterActivityData" ng-if="dataObj.id != 3" value="{{dataObj.activity_name}}">{{dataObj.activity_name}}</option>-->
                                        <option ng-repeat="dataObj in masterActivityData" value="{{dataObj.activity_name}}">{{dataObj.activity_name}}</option>

                                    </select>
                                </div>

                            </div>
                            <div class="row mt20">

                                <!--                            <a href="#" class="btn btn-danger btn-icon-split">
                                                                <span class="text">Cancel</span>
                                                            </a>-->
                                <div class="col-lg-4"></div>
                                <div class="mt-3 col-lg-4 text-center">
                                    <?php if ($add || $edit) { ?>
                                        <button type="button" class="btn btn-primary ml15 btn-icon-split" ng-disabled="logForm.$invalid" ng-click="saveLogsPrivilege(activity_type)">
                                            <span class="text">Submit</button>
                                        <button type="button" class="btn btn-danger" ng-click="cancilData()">Cancel</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success btn-lg" type="button" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;<button id="sbbtn" class="btn btn-success btn-lg" type="submit" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        <button type="button" class="btn btn-danger" ng-click="cancilData()" disabled>Cancel</button>
                                    <?php } ?>
                                </div>


                            </div>
                        </form>
                        <div class="form-row scroll-table" style="margin-top:10px;">
                            <table class="table custom-table" ng-show="user_log_data.length > 0">
                                <thead>
                                    <tr>
                                        <th>Room code</th>
                                        <th>Log Name</th>
                                        <th>Activity Name</th>
                                        <th>Action</th>
                                        <th>Status(Active/Inactive)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="dataObj in user_log_data">
                                        <td>{{dataObj.room_code}}</td>
                                        <td>{{dataObj.activity_name}}</td>
                                        <td>{{dataObj.type_id!=""?dataObj.type_id:""}}</td>

                                        <?php if ($edit) { ?>
                                            <td><a type="button" ng-click="editUserLog(dataObj)"><i class="far fa-edit text-blue pr10"></i></a></td>

                                            <td><label class="switch" style="margin-left: 30px;">
                                                    <input type="checkbox" name="{{$index}}" style="margin-top: 13px;" ng-model="dataObj.active_status" ng-click="toggleSelection($event, dataObj.log_id)">
                                                    <span class="slider round"></span>
                                                </label></td>
                                            <!--<button class="btn btn-success" ng-click="removeUserLog(dataObj.log_id)">Deactivate</button></td>-->
                                        <?php } else { ?>
                                            <td><a type="button" class="btn btn-sm btn-info" disabled><i class="far fa-edit text-blue pr10"></i></a></td>
                                            <td><label class="switch" style="margin-left: 30px;">
                                                    <input type="checkbox"name="{{$index}}" style="margin-top: 13px;" ng-model="dataObj.active_status" ng-click="toggleSelection($event, dataObj.log_id)">
                                                    <span class="slider round"></span>
                                                </label></td>
                                            <!--<button class="btn btn-success" ng-click="removeUserLog(dataObj.log_id)" disabled>Deactivate</button></td>-->
                                        <?php } ?>

                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<!--<script data-require="jquery@*" data-semver="2.2.0" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>-->
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>js/lodash.min.js"></script>
<script src="<?php echo base_url() ?>js/angularjs-dropdown-multiselect.min.js"></script>
<script src="<?php echo base_url() ?>js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>js/kendo.all.min.js"></script>
<script src="<?php echo base_url() ?>js/moment-with-locales.js"></script>
<script type="text/javascript">
                                                        var url_base_path = '<?php echo base_url() ?>';
                                                        var app = angular.module("usermgmtApp", ['angular.chosen']);
                                                        app.directive('chosen', function ($timeout) {
                                                            var linker = function (scope, element, attr) {
                                                                scope.$watch('sysRoomData', function () {
                                                                    $timeout(function () {
                                                                        element.trigger('chosen:updated');
                                                                    }, 0, false);
                                                                }, true);
                                                                scope.$watch('processActivityList', function () {
                                                                    $timeout(function () {
                                                                        element.trigger('chosen:updated');
                                                                    }, 0, false);
                                                                }, true);
                                                                scope.$watch('masterActivityData', function () {
                                                                    $timeout(function () {
                                                                        element.trigger('chosen:updated');
                                                                    }, 0, false);
                                                                }, true);
                                                                scope.$watch('userListData', function () {
                                                                    $timeout(function () {
                                                                        element.trigger('chosen:updated');
                                                                    }, 0, false);
                                                                }, true);
                                                                $timeout(function () {
                                                                    element.chosen();
                                                                }, 0, false);
                                                            };
                                                            return {
                                                                restrict: 'A',
                                                                link: linker
                                                            };
                                                        });
                                                        app.controller("usermgmtCtrl", function ($scope, $http, $filter) {
//                                        $scope.device_id = "59.144.179.216";
//                                        $scope.device_id = "10.240.240.240";

                                                            $scope.getServerList = function () {
                                                                $http({
                                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetServerList',
                                                                    method: "GET",
                                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                                }).then(function (response) {
                                                                    $scope.serverData = response.data.ip_list;
                                                                    $scope.device_id = $scope.serverData[0].server_ip;
                                                                    $scope.getRoomList();
                                                                }, function (error) { // optional
                                                                    console.log("Something went wrong.Please try again");
                                                                });
                                                            }
//                                    $scope.getServerList();
                                                            $scope.showAllBlocks = false;
                                                            $scope.sysRoomData = [];
                                                            $scope.activity_type = [];
                                                            $scope.getRoomList = function () {
                                                                $http({
                                                                    //url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist?room_array=' + $scope.user_room,
                                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist',
                                                                    method: "GET",
                                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                                }).then(function (response) {
                                                                    $scope.sysRoomData = response.data.room_list;
                                                                }, function (error) { // optional
                                                                    console.log("Something went wrong.Please try again");
                                                                });
                                                            }
                                                            $scope.processActivityListadd = [];
                                                            $scope.processActivityList = [];
                                                            $scope.room_code = "";

                                                            $scope.getProcessLogActivity = function (room_id) {
                                                                $scope.processActivityListadd = [];
                                                                $scope.processActivityList = [];
                                                                $scope.activity_id = "";
                                                                $scope.room_code = $filter('filter')($scope.sysRoomData, {room_id: room_id})[0].room_code;
                                                                $http({
                                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getactivitylistforroom?room_id=' + room_id,
                                                                    method: "GET",
                                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                                }).then(function (response) {
                                                                    $scope.processActivityList = response.data.activity_list;
                                                                    var temp = [];
//                                                                    console.log($scope.user_log_data);
                                                                    angular.forEach($scope.processActivityList, function (value, key) {
                                                                        var filter = $filter('filter')($scope.user_log_data, {module_id: value.mst_act_id, "room_code": value.room_code, "user_id": $scope.user_id});
                                                                        if (filter.length == 0 && value.room_code == $scope.room_code) {
                                                                            temp.push(value);
                                                                        } else {

                                                                        }
                                                                    });
                                                                    $scope.processActivityListadd = temp;
//                                                                    console.log($scope.processActivityListadd);
                                                                }, function (error) { // optional

                                                                    console.log("Something went wrong.Please try again");
                                                                });
                                                            }
                                                            $scope.masterActivityData = [];
                                                            $scope.getMasterActivityList = function () {
                                                                $http({
                                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getmasteractivitylist?activity_type=home',
                                                                    method: "GET",
                                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                                }).then(function (response) {
                                                                    $scope.masterActivityData = response.data.master_activity_list;
                                                                }, function (error) { // optional
                                                                    console.log("Something went wrong.Please try again");
                                                                });
                                                            }
                                                            $scope.getMasterActivityList();
                                                            $scope.activity_id = "";
                                                            $scope.current_module = "";
                                                            $scope.showtype = false;
                                                            $scope.getActivityInfo = function (current) {
                                                                $scope.current_module = current;
                                                                if (current == 16 || current == 17 || current == 30) {
                                                                    $scope.showtype = true;
                                                                } else {
                                                                    $scope.activity_type = [];
                                                                    $scope.showtype = false;
                                                                }
                                                            }
                                                            $scope.userListData = [];
                                                            $scope.getUsersList = function () {
                                                                $http({
                                                                    url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/getUsersList',
                                                                    method: "GET",
                                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                                }).then(function (response) {
                                                                    $scope.userListData = response.data.user_data;
                                                                }, function (error) { // optional
                                                                    toaster.pop('error', "error", "Something went wrong.Please try again");
                                                                });
                                                            }
                                                            $scope.getUsersList();
                                                            $scope.user_id = "";
                                                            $scope.activity_name = "";
                                                            $scope.user_room = "";
                                                            $scope.setDefaultValue = function (val) {
                                                                $scope.activity_name = val;
                                                                $scope.getUserModuleData();
                                                                $scope.room_id = "";
                                                                $scope.activity_id = "";
                                                                $scope.activity_type = [];
                                                                $scope.showtype = false;
                                                                if (val == 'log') {
                                                                    if ($scope.user_room != "") {
                                                                        $scope.getRoomList();
                                                                    } else {
                                                                        alert("there is no room assign to this user.");
                                                                    }
                                                                }
                                                            }
                                                            $scope.selectUser = function () {
                                                                $scope.user_room = $filter('filter')($scope.userListData, {id: $scope.user_id})[0].room_code;
                                                                $scope.module_name = "";
                                                                $scope.activity_name = "";
                                                                $scope.user_log_data = [];
                                                                $scope.room_id = "";
                                                                $scope.activity_id = "";
                                                                $scope.activity_type = [];
                                                                $scope.showtype = false;
                                                                $scope.editlog = false;

                                                            }

                                                            //Get All Master Data 
                                                            $scope.masterListData = [];
                                                            $scope.getMasterData = function () {
                                                                $http({
                                                                    url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/getMasterData',
                                                                    method: "GET",
                                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                                }).then(function (response) {
                                                                    $scope.masterListData = response.data.master_list;
                                                                }, function (error) { // optional
                                                                    toaster.pop('error', "error", "Something went wrong.Please try again");
                                                                });
                                                            }
                                                            //$scope.getMasterData();

                                                            //Get All Transaction Logs Data
                                                            $scope.activityList = [];
                                                            $scope.getMstActivityList = function () {
                                                                $http({
                                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetMstActivityList',
                                                                    method: "GET",
                                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                                }).then(function (response) {
                                                                    $scope.activityList = response.data.mst_act_list;
                                                                }, function (error) { // optional

                                                                    console.log("Something went wrong.Please try again");
                                                                });
                                                            }
                                                            //$scope.getMstActivityList();
                                                            //Get All Transaction Logs Data
                                                            $scope.reportList = [];
                                                            $scope.getReportList = function () {
                                                                $http({
                                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getReportList',
                                                                    method: "GET",
                                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                                }).then(function (response) {
                                                                    $scope.reportList = response.data.report_list;
                                                                }, function (error) { // optional

                                                                    console.log("Something went wrong.Please try again");
                                                                });
                                                            }
                                                            //$scope.getReportList();

                                                            //Save Master Privilege
                                                            $scope.isChecked = function (dataObj, index) {
                                                                if ($scope.masterListData[index].is_edit) {
                                                                    $scope.masterListData[index].is_view = true;
                                                                } else {
                                                                    $scope.masterListData[index].is_view = false;
                                                                }
                                                            }
                                                            $scope.isCheckedLog = function (dataObj, index) {
                                                                if ($scope.activityList[index].is_edit) {
                                                                    $scope.activityList[index].is_view = true;
                                                                } else {
                                                                    $scope.activityList[index].is_view = false;
                                                                }
                                                            }

                                                            $scope.setActive = function (dataObj, index) {
                                                                if (dataObj.is_view) {
                                                                    $scope.reportList[index]['is_view'] = true;
                                                                    $scope.reportList[index]['is_edit'] = false;
                                                                } else {
                                                                    $scope.reportList[index]['is_view'] = false;
                                                                }
                                                            }
                                                            $scope.setActiveprint = function (dataObj, index) {

                                                                if (dataObj.is_edit) {
                                                                    $scope.reportList[index]['is_edit'] = true;
                                                                    $scope.reportList[index]['is_view'] = false;
                                                                } else {
                                                                    $scope.reportList[index]['is_edit'] = false;
                                                                }

                                                            }
                                                            $scope.saveMasterPrivilege = function (dataArray, log_type) {
                                                                //console.log(dataArray);return false;
                                                                if (confirm("Do You Want To Save This Record?")) {
                                                                    $http({
                                                                        url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/saveUserPrivilege',
                                                                        method: "POST",
                                                                        data: "module_type=" + log_type + "&user_id=" + $scope.user_id + "&log_data=" + encodeURIComponent(angular.toJson(dataArray)),
                                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                                    }).then(function (response) {
                                                                        alert(response.data.message);
                                                                    }, function (error) { // optional
                                                                        toaster.pop('error', "error", "Something went wrong.Please try again");
                                                                    });
                                                                }
                                                            }

                                                            $scope.cancilData = function () {
                                                                $scope.editlog = false;
                                                                $scope.room_id = "";
                                                                $scope.activity_id = "";
                                                                $scope.activity_type = [];
                                                                $scope.showtype = false;
                                                                $scope.editlog = false;
                                                            }
                                                            $scope.user_previlege_list = [];
                                                            $scope.getUserModuleData = function () {
                                                                $http({
                                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getUserModuleData?user_id=' + $scope.user_id + "&module_type=" + $scope.activity_name,
                                                                    method: "GET",
                                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                                }).then(function (response) {
                                                                    $scope.user_previlege_list = response.data.user_previlege_list;
                                                                    if ($scope.user_previlege_list.length > 0) {
                                                                        if ($scope.activity_name == 'log') {
                                                                            $scope.getUserLogData();
                                                                        } else if ($scope.activity_name == 'master') {
                                                                            $scope.masterListData = $scope.user_previlege_list;
                                                                        } else {
                                                                            $scope.reportList = $scope.user_previlege_list;
                                                                        }
                                                                    } else {
                                                                        if ($scope.activity_name == 'log') {
                                                                            $scope.getMstActivityList();
                                                                        } else if ($scope.activity_name == 'master') {
                                                                            $scope.getMasterData();
                                                                        } else {
                                                                            $scope.getReportList();
                                                                        }
                                                                    }
                                                                }, function (error) {
                                                                    console.log("Something went wrong.Please try again");
                                                                });
                                                            }

                                                            //Code for Transactional Logs Privilege Data
                                                            $scope.saveLogsPrivilege = function (activity_type) {
                                                                if (confirm("Do You Want To Save This Record?")) {
                                                                    if ($scope.showtype == false) {
                                                                        activity_type = [];
                                                                    }
                                                                    $http({
                                                                        url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/saveLogsPrivilege',
                                                                        method: "POST",
                                                                        data: "module_type=log&user_id=" + $scope.user_id + "&room_code=" + $scope.room_code + "&module_id=" + $scope.current_module + "&type_id=" + encodeURIComponent(angular.toJson(activity_type)),
                                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                                    }).then(function (response) {
                                                                        alert(response.data.message);
                                                                        $scope.processActivityListadd = [];
                                                                        $scope.processActivityList = [];
                                                                        $scope.room_id = "";
                                                                        $scope.activity_id = "";
                                                                        $scope.activity_type = [];
                                                                        $scope.getUserLogData();
                                                                        $scope.showtype = false;
                                                                        $scope.editlog = false;
                                                                    }, function (error) { // optional
                                                                        toaster.pop('error', "error", "Something went wrong.Please try again");
                                                                    });
                                                                }
                                                            }

                                                            //Function to get User Log privilege data

                                                            $scope.user_log_data = [];
                                                            $scope.getUserLogData = function () {

                                                                $http({
                                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getUserLogData?user_id=' + $scope.user_id + "&module_type=" + $scope.activity_name,
                                                                    method: "GET",
                                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                                }).then(function (response) {
                                                                    $scope.user_log_data = response.data.user_previlege_list;
                                                                }, function (error) {
                                                                    console.log("Something went wrong.Please try again");
                                                                });
                                                            }

                                                            $scope.editUserLog = function (dataObj) {
                                                                $scope.editlog = true;
                                                                $scope.room_code = dataObj.room_code;
                                                                $scope.room_id = $filter('filter')($scope.sysRoomData, {room_code: dataObj.room_code})[0].room_id;
                                                                $scope.getProcessLogActivity($scope.room_id);
                                                                $scope.activity_id = dataObj.module_id;
                                                                $scope.current_module = dataObj.module_id;
                                                                if (dataObj.module_id == '16' || dataObj.module_id == '17' || dataObj.module_id == '30') {
                                                                    $scope.showtype = true;
                                                                    $scope.activity_type = dataObj.type_id.split(',');
                                                                } else {
                                                                    $scope.showtype = false;
                                                                    $scope.activity_type = [];
                                                                }
                                                            }
                                                            $scope.toggleSelection = function toggleSelection(event, objId) {
                                                                if (event.target.checked == true) {
                                                                    $scope.active_status = true;
                                                                } else {
                                                                    $scope.active_status = false;
                                                                }
                                                                $scope.removeUserLog(objId);
                                                            };
                                                            $scope.removeUserLog = function (objId) {
                                                                if ($scope.active_status == true) {
                                                                    var str = "Activate";
                                                                } else {
                                                                    var str = "Deactivate";
                                                                }
                                                                if (confirm("Do You Want To " + str + " this Record?")) {
                                                                    $http({
                                                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/removeUserLog',
                                                                        method: "POST",
                                                                        data: "id=" + objId + "&status=" + $scope.active_status,
                                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                                    }).then(function (response) {
                                                                        alert(response.data.message);
                                                                        $scope.getUserLogData();
                                                                    }, function (error) { // optional
                                                                        alert(response.data.message);
                                                                        //console.log("Something went wrong.Please try again");
                                                                    });
                                                                }
                                                            }
                                                        });
</script>