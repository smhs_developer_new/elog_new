<div class="container-fluid">
          <!-- Page Heading -->
			
          <div class="content-wrapper">
            <div class="content-heading executesop-heading">
              <div class="col-sm-5 pl-0">Bulk Upload</div>
              <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
              <div class="col-sm-7 pr-0">
                <ol class="breadcrumb ml-auto">
                   <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
                  <li class="breadcrumb-item active"><a href="<?= base_url()?>equipment_view">Equipment</a></li>
                  
                </ol>
              </div>
            </div>
            
      
      
      <div class="card card-default">
      <div class="card-body">                     
      <div class="row">
      <div class="col-sm-12 text-right">
      
     <a class="btn btn-primary btn-lg" href="<?= base_url()?>equipment_view" > Back To list</a>  
        &nbsp;&nbsp;&nbsp;
       <a class="btn btn-primary btn-lg" href="<?= base_url()?>create_equipment"> Add New Equipment</a>
      
      </div>
      </div>
      </div>
      </div>
      
      <form enctype="multipart/form-data" id="batcheq">
    <div class="card card-default">
        <div class="card-body">

	  <fieldset>
	  <div class="col-auto">
      <div class="form-row">
	  <div class="col-lg-6 mb-3"><label for="edso">Upload File <span style="color: red">Allowed Format : xls, xlsx</span></label><br><input class="" id="uploadFile" type="file" placeholder="" name="uploadFile" accept=".xlsx, .xls" required>
      </div>
	  <div class="col-lg-6 mb-3"><label for="edso"><a class="btn btn-primary btn-lg" href="<?= base_url()?>samplexls/equipment.xlsx">Click Here To Download Sample File</a></label><br>
      </div>
	  </fieldset>
	  </div>
	  </div>
	
	<!--Upload excel file : 
    <input type="file" name="uploadFile" value="" /><br><br>
	<input type="hidden" name ="tablename" value="userdetails">-->


    <!--<input type="submit" name="submit" value="Upload" />-->
<div class="card card-default">
                     
                     <div class="card-body" >
                        
                          
<div class="row disable-button-color">
      <div class="col-sm-12 text-center" id="sbtn"><button id="sbbtn" class="btn btn-success btn-lg" type="submit" name="submit">&nbsp;&nbsp;&nbsp;&nbsp;Upload File &nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      </div>

                        
                     </div>
                  </div><!-- END card-->



</form>
      
              </div>
			
			
			
        </div>
          
          <script type="text/javascript">
$(document).ready(function(){

 $('#batcheq').submit(function () {

	 var file_data = $('#uploadFile').prop('files')[0];
     var filename = file_data.name;
	 var extension = filename.substring(filename.lastIndexOf('.') + 1);
     //console.log( "The file extension is " + extension );
	 if(extension == "xls" || extension == "xlsx"){
	  $.ajax({
        url: '<?= base_url()?>ponta_sahib/Mastercontroller/uploadEquipmentExcel',
        type: 'POST',
        //data: $(this).serialize(),
		//data: form_data,
		data:new FormData(this),
		processData: false,
        contentType: false,
        cache: false,
	    async:false,
        success: function (res) {
          //console.log(data);
          if(res.status==1){
           // $("#validationnow21").attr("disabled",true);
           // $("#validationnow22").attr("disabled",false);
           
			alert("File Uploaded And Processed successfully");
            window.location.href = "<?= base_url()?>equipment_view?module_id=<?php echo $_GET['module_id']?>";
          }
		  else{
			  alert("Error Occured In File, Please check And Retry Again");
		  }
        }
      });
	  }
else{
 alert("You cann't upload this file.");
  $("#uploadFile").val(null);
}
      return false;
    });


});  
</script>