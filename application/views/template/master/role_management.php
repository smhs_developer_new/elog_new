<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="usermgmtApp" ng-controller="usermgmtCtrl" ng-cloak>

    <!-- Page Heading -->
    <div class="content-heading executesop-heading mb-4">
        <div class="col-sm-12 font-weight-bold pl-0">Roles Base Responsibility</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->

    </div>
    <div class="card card-default">
        <div class="card-body">
            <div class="form-row">
                <div class="col-lg-6 mb-3">
                    <label>Select Role & type of  Access :</label>
                    <select class="chzn-select form-control" tabindex="4" name="role_name" data-placeholder="Search Role"  ng-model="role_id" ng-change="selectRole()" required chosen>
                        <option value="">Select Role Name</option>
                        <option value="{{dataObj.id}}" ng-hide="dataObj.id == '-1'" ng-repeat="dataObj in roleData">{{dataObj.role_description}}</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="col-lg-4 mb-4">
                    <div class="radio-bg">
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="master" ng-disabled="role_id == ''"  name="defaultExampleRadios" ng-click="setDefaultValue('master')" value="master" ng-model="module_name">
                            <label class="custom-control-label lh26 black-text" for="master">Master</label>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 mb-4">
                    <div class="radio-bg">
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="report" ng-disabled="role_id == ''" ng-model="module_name" ng-click="setDefaultValue('report')" value="report" name="defaultExampleRadios">
                            <label class="custom-control-label lh26 black-text" for="report">Report</label>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4 mb-4">
                    <div class="radio-bg">
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="log" ng-disabled="role_id == ''" ng-model="module_name" ng-click="setDefaultValue('log')" value="log" name="defaultExampleRadios">
                            <label class="custom-control-label lh26 black-text" for="log">Log</label>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div class="card card-default" ng-show="activity_name != '' && user_id != ''">
        <div class="card-body">
            <div class="card" ng-show="activity_name == 'master'">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link f15" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <strong> Master </strong>
                        </button>
                    </h2>
                </div>

                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="row" ng-repeat="dataObj in masterListData">
                            <div class="col-lg-3 mb-3">
                                {{dataObj.master_name}}
                            </div>
                            <div class="col-lg-9 mb-9">
                                <div class="form-row">
                                    <div class="col-lg-4 mb-4">

                                        <div class="custom-control custom-radio">
                                            <input type="checkbox" class="custom-control-input" id="create{{$index}}"  ng-model="dataObj.is_create" value="create" name="create{{$index}}">
                                            <label class="custom-control-label lh26 black-text" for="create{{$index}}">Create</label>

                                        </div>
                                    </div>

                                    <div class="col-lg-4 mb-4">

                                        <div class="custom-control custom-radio">
                                            <input type="checkbox" class="custom-control-input" id="update{{$index}}"  ng-model="dataObj.is_edit" ng-click="isChecked(dataObj, $index)" value="update" name="update{{$index}}">
                                            <label class="custom-control-label lh26 black-text" for="update{{$index}}">Edit & Update</label>

                                        </div>

                                    </div>
                                    <div class="col-lg-4 mb-4">

                                        <div class="custom-control custom-radio">
                                            <input type="checkbox" class="custom-control-input" id="view{{$index}}"ng-disabled="dataObj.is_edit"  ng-model="dataObj.is_view" value="view" name="view{{$index}}">
                                            <label class="custom-control-label lh26 black-text" for="view{{$index}}">View</label>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row mt20">
                            <div class="col-lg-4"></div>
                            <div class="mt-3 col-lg-4 text-center">
                                <?php if ($add || $edit) { ?>
                                    <button class="btn btn-primary ml15 btn-icon-split" ng-click="saveMasterPrivilege(masterListData, 'master')">
                                    <span class="text">Submit</button>
                                <?php } else { ?>
                                    <button class="btn btn-success btn-lg" type="button" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;<button id="sbbtn" class="btn btn-success btn-lg" type="submit" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                <?php } ?>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

            <div class="card" ng-show="activity_name == 'report'">
                <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed f15" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <strong> Report </strong>
                        </button>
                    </h2>
                </div>
                <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="row" ng-repeat="dataObj in reportList">
                            <div class="col-lg-3 mb-3">
                                {{dataObj.report_name}}
                            </div>
                            <div class="col-lg-9 mb-9">

                                <div class="form-row">
                                    <div class="col-lg-4 mb-4">

                                        <div class="custom-control custom-radio">
                                            <input type="checkbox" class="custom-control-input" ng-click="setActive(dataObj, $index)" id="reportview{{$index}}" value="view"  ng-model="dataObj.is_view" name="reportview{{$index}}">
                                            <label class="custom-control-label lh26 black-text" for="reportview{{$index}}">View</label>

                                        </div>
                                    </div>

                                    <div class="col-lg-4 mb-4">

                                        <div class="custom-control custom-radio">
                                            <input type="checkbox" class="custom-control-input" ng-click="setActiveprint(dataObj, $index)" id="reportprint{{$index}}" ng-model="dataObj.is_edit" value="edit" name="reportprint{{$index}}">
                                            <label class="custom-control-label lh26 black-text" for="reportprint{{$index}}">View & Print</label>

                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="row mt20">
                            <div class="col-lg-4"></div>
                            <div class="mt-3 col-lg-4 text-center">
                                <?php if ($add || $edit) { ?>
                                    <button class="btn btn-primary ml15 btn-icon-split" ng-click="saveMasterPrivilege(reportList, 'report')">
                                    <span class="text">Submit</button>
                                <?php } else { ?>
                                    <button class="btn btn-success btn-lg" type="button" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;<button id="sbbtn" class="btn btn-success btn-lg" type="submit" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                <?php } ?>
                            </div>


                        </div>
                    </div>
                </div>
            </div>

            <div class="card" ng-show="activity_name == 'log'">
                <div class="card-header" id="headingfour">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed f15" type="button" data-toggle="collapse" data-target="#headingfour" aria-expanded="false" aria-controls="collapseThree">
                            <strong> Log </strong>
                        </button>
                    </h2>
                </div>
                <div id="headingfour" class="collapse show" aria-labelledby="headingfour" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="form-row scroll-table" style="margin-top:10px;">
                            <table class="table custom-table">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" name="select_all" ng-model="is_all" ng-click="selectAll()">Select All</th>
                                        <th>Log Name</th>
                                        <th>Activity Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="dataObj in activityList">
                                        <td><input type="checkbox" ng-model="dataObj.is_create" ng-click="isLogChecked(dataObj, $index)" value="{{dataObj.is_create}}" name="{{dataObj.id}}" id="{{dataObj.id}}" ></td>
                                        <td>{{dataObj.activity_name}}</td>
                                        <td ng-if="dataObj.id == 16 || dataObj.id == 17 || dataObj.id == 30">{{typeActivity}}</td>
                                        <td ng-if="dataObj.id != 16 && dataObj.id != 17 && dataObj.id != 30"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="row mt20">
                            <div class="col-lg-4"></div>
                            <div class="mt-3 col-lg-4 text-center">
                                <?php if ($add || $edit) { ?>
                                    <button type="button" class="btn btn-primary ml15 btn-icon-split" ng-disabled="logForm.$invalid" ng-click="saveLogsPrivilege(activity_type)">
                                        <span class="text">Submit</button>
                                <?php } else { ?>
                                    <button class="btn btn-success btn-lg" type="button" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;<button id="sbbtn" class="btn btn-success btn-lg" type="submit" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                <?php } ?>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- /.container-fluid -->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>js/lodash.min.js"></script>
<script src="<?php echo base_url() ?>js/angularjs-dropdown-multiselect.min.js"></script>
<script src="<?php echo base_url() ?>js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>js/kendo.all.min.js"></script>
<script src="<?php echo base_url() ?>js/moment-with-locales.js"></script>
<script type="text/javascript">
                                        var url_base_path = '<?php echo base_url() ?>';
                                        var app = angular.module("usermgmtApp", ['angular.chosen']);
                                        app.directive('chosen', function ($timeout) {
                                            var linker = function (scope, element, attr) {
                                                scope.$watch('roleData', function () {
                                                    $timeout(function () {
                                                        element.trigger('chosen:updated');
                                                    }, 0, false);
                                                }, true);
                                                $timeout(function () {
                                                    element.chosen();
                                                }, 0, false);

                                            };
                                            return {
                                                restrict: 'A',
                                                link: linker
                                            };
                                        });
                                        app.controller("usermgmtCtrl", function ($scope, $http, $filter) {
                                            //Get Role List
                                            $scope.roleData = [];
                                            $scope.getRoleList = function () {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList?status=active',
                                                    method: "GET",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    $scope.roleData = response.data.role_list;
                                                }, function (error) { // optional
                                                    $scope.roleData = [];
                                                    console.log("Something went wrong.Please try again");

                                                });
                                            }
                                            $scope.getRoleList();
                                            $scope.showAllBlocks = false;
                                            $scope.activity_type = [];
                                            $scope.masterActivityData = [];
                                            $scope.typeActivity = "";
                                            $scope.getMasterActivityList = function () {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getmasteractivitylist?activity_type=home',
                                                    method: "GET",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    $scope.masterActivityData = response.data.master_activity_list;
                                                    angular.forEach($scope.masterActivityData, function (value, key) {
                                                        if ($scope.typeActivity != '') {
                                                            $scope.typeActivity = $scope.typeActivity + ',' + value.activity_name;
                                                        } else {
                                                            $scope.typeActivity = value.activity_name;
                                                        }
                                                    });
                                                }, function (error) { // optional
                                                    console.log("Something went wrong.Please try again");

                                                });
                                            }
                                            $scope.getMasterActivityList();
                                            $scope.activity_id = "";
                                            $scope.current_module = "";
                                            $scope.showtype = false;
                                            $scope.getActivityInfo = function (current) {
                                                $scope.current_module = current;
                                                if (current == 16 || current == 17 || current == 30) {
                                                    $scope.showtype = true;
                                                } else {
                                                    $scope.activity_type = [];
                                                    $scope.showtype = false;
                                                }
                                            }
                                            $scope.userListData = [];
                                            $scope.getUsersList = function () {
                                                $http({
                                                    url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/getUsersList',
                                                    method: "GET",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    $scope.userListData = response.data.user_data;
                                                }, function (error) { // optional
                                                    toaster.pop('error', "error", "Something went wrong.Please try again");
                                                });
                                            }
                                            $scope.getUsersList();
                                            $scope.role_id = "";
                                            $scope.activity_name = "";
                                            $scope.setDefaultValue = function (val) {
                                                $scope.activity_name = val;

                                                $scope.getUserModuleData();
                                                $scope.room_id = "";
                                                $scope.activity_id = "";
                                                $scope.activity_type = [];
                                                $scope.showtype = false;
                                            }
                                            $scope.selectRole = function () {
                                                $scope.module_name = "";
                                                $scope.activity_name = "";
                                                $scope.user_log_data = [];
                                                $scope.activity_id = "";
                                                $scope.activity_type = [];
                                                $scope.showtype = false;

                                            }

                                            //Get All Master Data 
                                            $scope.masterListData = [];
                                            $scope.getMasterData = function () {
                                                $http({
                                                    url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/getMasterData',
                                                    method: "GET",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    $scope.masterListData = response.data.master_list;
                                                }, function (error) { // optional
                                                    toaster.pop('error', "error", "Something went wrong.Please try again");
                                                });
                                            }
                                            //$scope.getMasterData();

                                            //Get All Transaction Logs Data
                                            $scope.activityList = [];
                                            $scope.getMstActivityList = function () {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetMstActivityList',
                                                    method: "GET",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    $scope.activityList = response.data.mst_act_list;
                                                    angular.forEach($scope.activityList, function (value, key) {
                                                        $scope.activityList[key]['module_id'] = value['id'];
                                                        $scope.activityList[key]['is_create'] = false;
                                                        $scope.activityList[key]['is_edit'] = false;
                                                        $scope.activityList[key]['is_view'] = false;
                                                    });
                                                }, function (error) { // optional

                                                    console.log("Something went wrong.Please try again");
                                                });
                                            }
                                            //Get All Transaction Logs Data
                                            $scope.reportList = [];
                                            $scope.getReportList = function () {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getReportList',
                                                    method: "GET",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    $scope.reportList = response.data.report_list;
                                                }, function (error) { // optional

                                                    console.log("Something went wrong.Please try again");
                                                });
                                            }
                                            //$scope.getReportList();

                                            //Save Master Privilege
                                            $scope.isChecked = function (dataObj, index) {
                                                if ($scope.masterListData[index].is_edit) {
                                                    $scope.masterListData[index].is_view = true;
                                                } else {
                                                    $scope.masterListData[index].is_view = false;
                                                }
                                            }
                                            $scope.isCheckedLog = function (dataObj, index) {
                                                if ($scope.activityList[index].is_edit) {
                                                    $scope.activityList[index].is_view = true;
                                                } else {
                                                    $scope.activityList[index].is_view = false;
                                                }
                                            }

                                            $scope.setActive = function (dataObj, index) {
                                                if (dataObj.is_view) {
                                                    $scope.reportList[index]['is_view'] = true;
                                                    $scope.reportList[index]['is_edit'] = false;
                                                } else {
                                                    $scope.reportList[index]['is_view'] = false;
                                                }
                                            }
                                            $scope.setActiveprint = function (dataObj, index) {

                                                if (dataObj.is_edit) {
                                                    $scope.reportList[index]['is_edit'] = true;
                                                    $scope.reportList[index]['is_view'] = false;
                                                } else {
                                                    $scope.reportList[index]['is_edit'] = false;
                                                }

                                            }
                                            $scope.saveMasterPrivilege = function (dataArray, log_type) {
                                                if (confirm("Do You Want To Update This Record?")) {
                                                    $http({
                                                        url: url_base_path + 'ponta_sahib/Mastercontroller/saveRolePrivilege',
                                                        method: "POST",
                                                        data: "module_type=" + log_type + "&role_id=" + $scope.role_id + "&log_data=" + encodeURIComponent(angular.toJson(dataArray)),
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    }).then(function (response) {
                                                        alert(response.data.message);

                                                    }, function (error) { // optional
                                                        toaster.pop('error', "error", "Something went wrong.Please try again");
                                                    });
                                                }
                                            }
                                            $scope.role_previlege_list = [];
                                            $scope.getUserModuleData = function () {
                                                $http({
                                                    url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getRoleModuleData?role_id=' + $scope.role_id + "&module_type=" + $scope.activity_name,
                                                    method: "GET",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    $scope.role_previlege_list = response.data.role_previlege_list;
                                                    if ($scope.role_previlege_list.length > 0) {
                                                        if ($scope.activity_name == 'log') {
                                                            $scope.getRoleLogData();
                                                        } else if ($scope.activity_name == 'master') {
                                                            $scope.masterListData = $scope.role_previlege_list;
                                                        } else {
                                                            $scope.reportList = $scope.role_previlege_list;
                                                        }
                                                    } else {
                                                        if ($scope.activity_name == 'log') {
                                                            $scope.getMstActivityList();
                                                        } else if ($scope.activity_name == 'master') {
                                                            $scope.getMasterData();
                                                        } else {
                                                            $scope.getReportList();
                                                        }
                                                    }
                                                }, function (error) {
                                                    console.log("Something went wrong.Please try again");
                                                });
                                            }

                                            //Code for Transactional Logs Privilege Data
                                            //Save Master Privilege
//                                        $scope.is_all=false;
                                            $scope.selectAll = function () {
                                                if ($scope.is_all) {
                                                    angular.forEach($scope.activityList, function (value, key) {
                                                        $scope.activityList[key]['is_create'] = true;
                                                        $scope.activityList[key]['is_edit'] = true;
                                                        $scope.activityList[key]['is_view'] = true;
                                                    });
                                                } else {
                                                    angular.forEach($scope.activityList, function (value, key) {
                                                        $scope.activityList[key]['is_create'] = false;
                                                        $scope.activityList[key]['is_edit'] = false;
                                                        $scope.activityList[key]['is_view'] = false;
                                                    });
                                                }

                                            }
                                            $scope.isLogChecked = function (dataObj, index) {
                                                if (dataObj.is_create) {
                                                    $scope.activityList[index]['is_create'] = true;
                                                    $scope.activityList[index]['is_edit'] = true;
                                                    $scope.activityList[index]['is_view'] = true;
                                                } else {
                                                    $scope.activityList[index]['is_create'] = false;
                                                    $scope.activityList[index]['is_edit'] = false;
                                                    $scope.activityList[index]['is_view'] = false;
                                                }

                                            }
                                            $scope.saveLogsPrivilege = function (activity_type) {
                                                if (confirm("Do You Want To Update This Record?")) {
                                                    if ($scope.showtype == false) {
                                                        activity_type = [];
                                                    }
                                                    $http({
                                                        url: url_base_path + 'ponta_sahib/Mastercontroller/saveRoleLogsPrivilege',
                                                        method: "POST",
                                                        data: "module_type=log&role_id=" + $scope.role_id + "&log_data=" + encodeURIComponent(angular.toJson($scope.activityList)),
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    }).then(function (response) {
                                                        alert(response.data.message);
                                                        $scope.getRoleLogData();
                                                    }, function (error) { // optional
                                                        toaster.pop('error', "error", "Something went wrong.Please try again");
                                                    });
                                                }
                                            }

                                            //Function to get User Log privilege data
                                            $scope.getRoleLogData = function () {
                                                $http({
                                                    url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getRoleModuleData?role_id=' + $scope.role_id + "&module_type=" + $scope.activity_name,
                                                    method: "GET",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    $scope.activityList = response.data.role_previlege_list;
                                                }, function (error) {
                                                    console.log("Something went wrong.Please try again");
                                                });
                                            }

                                        });
</script>