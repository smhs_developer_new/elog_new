<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid">
          <!-- Page Heading -->
			
          <div class="content-wrapper">
            <div class="content-heading executesop-heading">
              <div class="col-sm-5 pl-0">Plant Details</div>
              <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
              <div class="col-sm-7 pr-0">
                <ol class="breadcrumb ml-auto">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
                  <li class="breadcrumb-item active"><a  href="<?= base_url()?>plant_view?module_id=<?php echo $_GET['module_id']?>">Plant</a></li>
                  
                </ol>
              </div>
            </div>
      
                        <div class="card card-default">
      <div class="card-body">                     
      <div class="row">
      <div class="col-sm-12 text-right">
      <?php if ($add) { ?>
      <a class="btn btn-primary btn-lg" href="<?= base_url()?>create_plant?module_id=<?php echo $_GET['module_id']?>"> Add New Plant</a>
       <?php } else { ?>
        <button class="btn btn-primary btn-lg"  disabled>Add New Plant</button>
                        <?php } ?>
      </div>
      </div>
      </div>
      </div>
            
          <div class="card card-default">
            <div class="card-body">
              <table class="table custom-table">
              <thead>
                <tr><th>Plant Code</th><th>Plant Name</th><th>Plant Address</th><th>Plant Contact</th><th>No. Of Blocks</th><th>Created By</th><th>Created on</th><th>View</th></tr>
                </thead>
                <tbody>
                <?php
                  foreach ($data as $li) {
                    ?>
                    <tr><td><?= $li["plant_code"]?></td><td><?= $li["plant_name"]?></td><td><?= $li["plant_address"]?></td><td><?= $li["plant_contact"]?></td><td><?= $li["number_of_blocks"]?></td><td><?= $li["created_by"]?></td><td><?= $li["created_on"]?></td>
                        <td>
                             <?php if ($edit) { ?>
                                         <a href="<?= base_url()?>plant_view_edit/<?= $li['id']?>?module_id=<?php echo $_GET['module_id'];?>" class="btn btn-primary">View</a>
                                <?php } else { ?>
                                    <button disabled class="btn btn-primary">View</button>
                                <?php } ?>
                        </td></tr>
                    <?php
                  }
                ?>
                        </tbody>
              </table>
            </div> 
          </div>     
      
      
      
      
              </div>
			
			
			
			
        </div>