<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid">
          <!-- Page Heading -->
          
          <div class="content-wrapper">
            <div class="content-heading executesop-heading">
              <div class="col-sm-5 pl-0">Product List</div>
              <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
              <div class="col-sm-7 pr-0">
                <ol class="breadcrumb ml-auto">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
                  
                  
                </ol>
              </div>
            </div>
      
      <div class="card card-default">
      <div class="card-body">                     
      <div class="row">
      <div class="col-sm-12 text-right">
      <?php if ($add) { ?>
          <a class="btn btn-primary btn-lg" href="<?= base_url()?>manage_product?module_id=<?php echo $_GET['module_id']?>" > Add New Product</a>
        
                        <?php } else { ?>
        <button class="btn btn-primary btn-lg"  disabled>Add New Product</button> &nbsp;&nbsp;&nbsp;
                        <?php } ?>
      
      
      </div>
      </div>
      </div>
      </div>
            
      <div class="card card-default" id="divftbl">
          <div class="pt10"><center><p>Product List</p></center></div>
          <div class="scroll-table">
          <table class="table custom-table">
          <thead>
          <tr tabindex="0">                        
            <th scope="col">S.No.</th>
            <th scope="col">Product Code</th>
            <th scope="col">Product Name</th>
            <th scope="col">Market</th>
            <th scope="col">Remark</th>
            <th scope="col">Created By</th>
            <th scope="col">Created On</th>
            <th scope="col">Action</th>
          </tr>
          </thead>
          <tbody> 
            <?php if(count($data['prodtl']->result())) { ?>
               <?php $N=0; foreach($data['prodtl']->result() as $row) { $N++; ?>
               <tr><td><?php echo $N ?></td><td><?php echo $row->product_code ?></td><td><?php echo $row->product_name ?></td><td><?php echo $row->product_market ?></td><td><?php echo $row->product_remark ?></td><td><?php echo $row->created_by ?></td><td><?php echo $row->created_on ?></td>
                 <td class="text-left" style="">
                     <?php if ($edit) { ?>
                     <a type="button" href="<?= base_url()?>product_edit/<?= $row->id ?>?module_id=<?php echo $_GET['module_id'];?>" class="btn btn-sm btn-info mr-2 command-edit" data-id="10243">Edit</a>
                   <a type="button" href="#" class="btn btn-sm btn-danger" onclick="CommonDelete('<?php echo $row->id ?>')" data-id="10243">Deactivate</a>
                                <?php } else { ?>
                                    <button disabled class="btn btn-primary">Edit</button>
                                    <button disabled class="btn btn-primary">Deactivate</button>
                                <?php } ?>
                     
                   
                   </td>
               </tr>
               <?php } } else { ?>  
                 <tr><td colspan="5"><center>No record exist.</center></td></tr>
               <?php } ?>                  
           </tbody>
          </table>
          </div>
          </div><!-- START row-->
      
        </div>
			
        </div>

<script type="text/javascript">
  function CommonDelete(id)
  {
    debugger;
    var r = confirm("Do You Want To Deactivate This Record ?");
    if(r == true)
    {
      var id=id;
      var tblname = "mst_product";
      var colname = "id";
      $.ajax({
          url: "<?= base_url()?>ponta_sahib/Mastercontroller/CommonDelete",
          type: 'POST',
          data: {id:id,tblname:tblname,colname:colname},
          success: function(res) {

          if(res>0){
            alert("Record successfully deactivated...");
            window.location.href = "<?= base_url()?>product_view?module_id=<?php echo $_GET['module_id']?>";
          }
          else
          {
            alert("Something went wrong");
          }
         }
      }); 
    }
  }
</script>