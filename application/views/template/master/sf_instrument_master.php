<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
    <!-- Page Heading -->

    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row">

                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">    	   
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th colspan="5" class="f18" style="vertical-align:middle">Instrument Master </th>
                                <th class="text-right">
                                    <?php if ($add) { ?>
                                        <button class="btn btn-success" ng-click="showForm()" ng-disabled="editmode">Add (+)</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success" ng-click="showForm()" disabled>Add (+)</button>
                                    <?php } ?>

                                </th>
                            </tr>
                        </thead>
                    </table>  
                </div>
                <div class="col-12">
                    <form name="balanceForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-lg-4 mb-3"><label>Instrument Code <span style="color: red">*</span></label>
                                            <input type="text" only-alphanum ng-disabled="recordid > 0" class="form-control" maxlength="20" ng-change="CommonCheckIsExist(equipment_code)" name="equipment_code" ng-model="equipment_code" required>
                                        </div>
                                        <div class="col-lg-4 mb-3"><label>Instrument Name <span style="color: red">*</span></label>
                                            <input type="text" only-alphanum class="form-control" maxlength="50" name="equipment_name" ng-model="equipment_name" required>
                                        </div>
                                        <div class="col-lg-4 mb-3"><label>Instrument Description <span style="color: red"></span></label>
                                            <textarea class="form-control" only-alphanum name="equipment_desc" maxlength="200" ng-model="equipment_desc"></textarea>
                                        </div>


                                    </div>
                                </div>
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-lg-4 mb-3"><label>Instrument Type <span style="color: red">*</span></label>
                                            <select class="form-control" name="equipment_type" ng-model="equipment_type" required>
                                                <option value="Instrument">Instrument</option>
                                                <option value="Apparatus">Apparatus</option>
                                                <option value="Samplingrod">Sampling Rod</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 mb-3"><label>Block Code <span style="color: red">*</span></label>
                                            <select class="form-control" tabindex="4" id="room"  name="block_code" ng-change="getAreaList()" ng-options="dataObj['block_code'] as (dataObj.block_code +  ' => ' +  dataObj.block_name) for dataObj in blockList"  ng-model="block_code" required>
                                                <option value="">Select Block Name</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 mb-3">
                                            <label for="validationServer01">Remarks <span style="color: red" ng-show="editmode">*</span></label>
                                            <textarea class="form-control" aria-label="With textarea" maxlength="100" name="bremarks" ng-model="remark"  ng-required="editmode"></textarea>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-lg-2 mb-3" ng-show="editmode" style="padding: 30px 10px 10px 10px;"><label><b>Active/Inactive</b></label>
                                            <label class="switch">
                                                <input type="checkbox" style="margin-top: 13px;" ng-model="active_status" ng-checked="active_status == 'active'" ng-click="toggleSelection($event)">
                                                <span class="slider round"></span>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 text-center">
                                            <button class="btn btn-sm btn-success" ng-disabled="balanceForm.$invalid"  ng-click="saveInstrument()" type="submit">Submit</button>
                                            <button ng-click="resetForm();hideForm();" class="btn btn-sm btn-danger"  type="reset">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="form-row" ng-show="instrumentList.length == 0">
                <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
                <div class="col-lg-12 text-center" ><h4>(Please Select the above parameters)</h4></div> 
            </div>
            <div class="form-row" style="margin-top:10px;" ng-show="instrumentList.length > 0">   
                <h2 class="f18">Instrument List</h2>
                <table  class="table custom-table">
                    <thead >
                        <tr>
                            <th>Instrument Code</th>
                            <th>Instrument Name</th>
                            <th>Instrument Type</th>
                            <th>Block Code</th>
                            <!-- <th>Instrument Desc</th> -->
                            <th>Last Modified By</th>
                            <th>Last Modified On</th>
                            <th>Action</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr dir-paginate="dataObj in instrumentList|itemsPerPage:records_per_page" total-items="total_records" style="background:none;">
                            <td>{{dataObj.equipment_code}}</td>
                            <td>{{dataObj.equipment_name}}</td>
                            <td>{{dataObj.equipment_type}}</td>
                            <td>{{dataObj.block_code}}</td>
                            <!-- <td>{{dataObj.equipment_desc}}</td> -->
                            <td>{{dataObj.modified_by!=null && dataObj.modified_by!=''?dataObj.modified_by:dataObj.created_by}}</td>
                            <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                            <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                            <td>
                                <?php if ($edit) { ?>
                                    <!-- <button class="btn btn-sm btn-info" ng-click="editInstrumentMaster(dataObj)">Edit</button> -->
                                    <!-- <button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)">Deactivate</button> -->
                                    <a type="button" ng-click="editInstrumentMaster(dataObj)"><i class="far fa-edit text-blue pr10"></i></a>
                                <?php } else { ?>
                                    <!-- <button class="btn btn-sm btn-info" ng-click="editInstrumentMaster(dataObj)" disabled>Edit</button> -->
                                    <!--<button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)" disabled>Deactivate</button> -->
                                    <a type="button" ng-click="editInstrumentMaster(dataObj)" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                <?php } ?>

                            </td>
                            <td ng-if="dataObj.status == 'inactive'" class="bg-danger text-white">Inactive</td>
                            <td ng-if="dataObj.status == 'active'" class="bg-success text-white">Active</td>
                        </tr>
                    </tbody>
                </table>
                <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getInstrumentList(newPageNumber)"></dir-pagination-controls>
            </div>
        </div>
    </div>	

</div>

<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script type="text/javascript">
                                var app = angular.module("materialApp", ['angular.chosen', 'angularUtils.directives.dirPagination']);
                                app.directive('chosen', function ($timeout) {

                                    var linker = function (scope, element, attr) {

                                        scope.$watch('blockList', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);

                                        $timeout(function () {
                                            element.chosen();
                                        }, 0, false);
                                    };
                                    return {
                                        restrict: 'A',
                                        link: linker
                                    };
                                });

                                app.directive('onlyAlphanum', function () {
                                    return {
                                        require: 'ngModel',
                                        link: function (scope, element, attr, ngModelCtrl) {
                                            function fromUser(text) {
                                                var transformedInput = text.replace(/[^0-9a-zA-Z\-\.\s]/g, '');
                                                if (transformedInput !== text) {
                                                    ngModelCtrl.$setViewValue(transformedInput);
                                                    ngModelCtrl.$render();
                                                }
                                                return transformedInput; // or return Number(transformedInput)
                                            }
                                            ngModelCtrl.$parsers.push(fromUser);
                                        }
                                    };
                                });
                                app.filter('format', function () {
                                    return function (item) {
                                        var t = item.split(/[- :]/);
                                        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                        var time = d.getTime();
                                        return time;
                                    };
                                });
                                app.controller("materialCtrl", function ($scope, $http, $filter) {
                                    $scope.blockList = [];
                                    $scope.getBlockList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetBlockList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.blockList = response.data.block_data;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");
                                        });
                                    }

                                    $scope.getBlockList();
                                    $scope.equipment_type = "Instrument";
                                    $scope.showAddForm = false;
                                    $scope.editmode = false;
                                    $scope.remark = '';
                                    $scope.showForm = function () {
                                        $scope.showAddForm = true;
                                    }
                                    $scope.hideForm = function () {
                                        $scope.showAddForm = false;
                                        $scope.editmode = false;

                                        $scope.recordid = '';
                                        delete $scope.block_code;
                                        $scope.equipment_name = '';
                                        $scope.equipment_code = '';
                                        $scope.equipment_desc = '';
                                        $scope.equipment_type = 'Instrument';
                                        $scope.remark = '';
                                        $scope.active_status = '';

                                    }
                                    $scope.saveInstrument = function () {
                                        var active_status = ($scope.active_status === undefined || $scope.active_status == '') ? 'active' : $scope.active_status;
                                        var msg = 'Save';
                                        if ($scope.recordid > 0) {
                                            var msg = 'Update';
                                        }

                                        if (confirm("Do You Want To " + msg + " This Record?")) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/saveInstrument',
                                                method: "POST",
                                                data: "block_code=" + $scope.block_code + "&equipment_name=" + $scope.equipment_name + "&equipment_code=" + $scope.equipment_code + "&equipment_desc=" + $scope.equipment_desc + "&equipment_type=" + $scope.equipment_type + "&recordid=" + $scope.recordid + "&status=" + active_status + "&remark=" + $scope.remark,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            })
                                                    .then(function (response) {
                                                        alert(response.data.message);
                                                        $scope.editmode = false;
                                                        $scope.showAddForm = false;
                                                        $scope.block_code = "";
                                                        $scope.equipment_name = "";
                                                        $scope.equipment_code = "";
                                                        $scope.equipment_desc = "";
                                                        $scope.remark = "";
                                                        $scope.active_status = "";
                                                        $scope.recordid = "";
                                                        $scope.getInstrumentList($scope.page);
                                                    },
                                                            function (response) { // optional
                                                                $scope.showAddForm = false;
                                                                $scope.block_code = "";
                                                                $scope.equipment_name = "";
                                                                $scope.equipment_code = "";
                                                                $scope.equipment_desc = "";
                                                                $scope.remark = "";
                                                                $scope.active_status = "";
                                                                $scope.recordid = "";
                                                                $scope.getInstrumentList($scope.page);
                                                                console.log("Something went wrong.Please try again");

                                                            });
                                        }
                                    }
                                    $scope.instrumentList = [];
                                    $scope.page = 1;
                                    $scope.total_records = 0;
                                    $scope.records_per_page = 10;
                                    $scope.getInstrumentList = function (page) {
                                        $scope.page = page;
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetInstrumentList?page=' + page,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.instrumentList = response.data.instrument_data;
                                            $scope.total_records = response.data.total_records;
                                            $scope.records_per_page = response.data.records_per_page;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getInstrumentList($scope.page);

                                    //Created By : Bhupendra Kumar
                                    //Date : 18 March 2020
                                    //Decription : For Deactivate the Selected Record
                                    $scope.CommonDelete = function (id) {
                                        var tblname = "pts_mst_instrument";
                                        var colname = "id";
                                        if (confirm("Do You Want To Deactivate This Record ?")) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/CommonDelete',
                                                method: "POST",
                                                data: "id=" + id + "&tblname=" + tblname + "&colname=" + colname,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                alert(response.data.message);
                                                $scope.getInstrumentList($scope.page);
                                            }, function (error) { // optional
                                                alert(response.data.message);
                                                //console.log("Something went wrong.Please try again");
                                            });
                                        }
                                    }

                                    //Created By : Bhupendra Kumar
                                    //Date : 18 March 2020
                                    //Decription : To get values of selected record
                                    $scope.recordid = "";
                                    $scope.editInstrumentMaster = function (dataObj) {
<?php if ($edit == false) { ?>
                                            return false;
<?php } ?>
                                        $scope.editmode = true;
                                        $scope.recordid = dataObj.id;
                                        $scope.block_code = dataObj.block_code;
                                        $scope.equipment_name = dataObj.equipment_name;
                                        $scope.equipment_code = dataObj.equipment_code;
                                        $scope.equipment_desc = dataObj.equipment_desc;
                                        $scope.equipment_type = dataObj.equipment_type;
                                        $scope.remark = "";
                                        $scope.active_status = dataObj.status;
                                        $scope.showAddForm = true;
                                        $("html, body").animate({scrollTop: 0}, "slow");
                                    }

                                    //created By : Bhupendra kumar
                                    //Date : 6th April 2020
                                    //Description : to check the record is already exist or not on given parameter
                                    $scope.CommonCheckIsExist = function (code) {
                                        var tblname = "pts_mst_instrument";
                                        var col = "equipment_code";
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/chkcode',
                                            method: "POST",
                                            data: "col=" + col + "&tblname=" + tblname + "&code=" + code,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (res) {
                                            if (res.data.status == 1) {
                                                alert("Record already exists at this equipment_code.");
                                                $scope.equipment_code = "";
                                            }
                                        }, function (error) { // optional
                                        });
                                    }
                                    $scope.toggleSelection = function (event) {
                                        if (event.target.checked == true) {
                                            $scope.active_status = 'active';
                                        } else {
                                            $scope.active_status = 'inactive';
                                        }
                                    };
                                });
</script>