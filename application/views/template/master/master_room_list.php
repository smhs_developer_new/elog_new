<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<div class="container-fluid" ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
    <!-- Page Heading -->

    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row" style="margin-top:10px;" ng-show="sysRoomData.length > 0">   
                <h2 class="f18">Room Configuration List</h2>
                <div class="table-responsive noscroll" >
                    <table  class="table custom-table">
                        <thead >
                            <tr>
                                <th>Room Code</th>
                                <th>Room Name</th>
                                <th>Room Type</th>
                                <th>Room Area</th>
                                <th>Device IP</th>
                                <th>Data Logger ID</th>
                                <th>Magnehlic Gauge ID</th>
                                <th>Filter Desc</th>
                                <th>Last Modified By</th>
                                <th>Last Modified On</th>
                                <th>Action</th>
                                <th>Status</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="dataObj in sysRoomData">
                                <td>{{dataObj.room_code}}</td>
                                <td>{{dataObj.room_name}}</td>
                                <td>{{dataObj.room_type}}</td>
                                <td>{{dataObj.area_id}}</td>
                                <td title="{{dataObj.device_id}}">{{ dataObj.device_id | limitTo: 8 }}{{dataObj.device_id.length > 8 ? '...' : ''}}</td>
                                <td title="{{dataObj.logger_id}}">{{ dataObj.logger_id | limitTo: 8 }}{{dataObj.logger_id.length > 8 ? '...' : ''}}</td>
                                <td title="{{dataObj.gauge_id}}">{{ dataObj.gauge_id | limitTo: 8 }}{{dataObj.gauge_id.length > 8 ? '...' : ''}}</td>
                                <td>{{dataObj.filter}}</td>
                                <td>{{dataObj.modified_by!=""?dataObj.modified_by:dataObj.created_by}}</td>
                                <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on | format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on | format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td>
                                    <?php if ($edit) { ?>
                                        <!--<button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)">Deactivate</button>-->
                                        <a type="button" ng-click="editRoomMaster(dataObj)"><i class="far fa-edit text-blue pr10"></i></a>
                                    <?php } else { ?>
                                        <!--<button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)"disabled>Deactivate</button>-->
                                        <a type="button" class="btn btn-sm btn-info" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                        <!--<button class="btn btn-sm btn-info" ng-click="editRoomMaster(dataObj)" disabled>Edit</button>-->
                                    <?php } ?>
                                </td>
                                <td ng-if="dataObj.status == 'active'"   class="bg-success text-white">{{dataObj.status| capitalizeWord}}</td>
                                <td ng-if="dataObj.status == 'inactive'" class="bg-danger text-white">{{dataObj.status| capitalizeWord}}</td>
                            </tr>
                        </tbody>
                    </table>  
                </div>

            </div>
        </div>
    </div>  

</div>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/ngIpAddress.min.js"></script>

<script type="text/javascript">
                                    var app = angular.module("materialApp", ['angular.chosen', 'ng-ip-address']);
                                    app.directive('chosen', function ($timeout) {

                                        var linker = function (scope, element, attr) {
                                            scope.$watch('masterRoomList', function () {
                                                $timeout(function () {
                                                    element.trigger('chosen:updated');
                                                }, 0, false);
                                            }, true);
                                            $timeout(function () {
                                                element.chosen();
                                            }, 0, false);
                                        };
                                        return {
                                            restrict: 'A',
                                            link: linker
                                        };
                                    });

                                    app.directive('onlyDigits', function () {
                                        return {
                                            require: 'ngModel',
                                            restrict: 'A',
                                            link: function (scope, element, attr, ctrl) {
                                                function inputValue(val) {
                                                    if (val) {
                                                        var digits = val.replace(/[^0-9.]/g, '');

                                                        if (digits.split('.').length > 2) {
                                                            digits = digits.substring(0, digits.length - 1);
                                                        }

                                                        if (digits !== val) {
                                                            ctrl.$setViewValue(digits);
                                                            ctrl.$render();
                                                        }
                                                        return parseFloat(digits);
                                                    }
                                                    return undefined;
                                                }
                                                ctrl.$parsers.push(inputValue);
                                            }
                                        };
                                    });
                                    app.filter('format', function () {
                                        return function (item) {
                                            var t = item.split(/[- :]/);
                                            var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                            var time = d.getTime();
                                            return time;
                                        };
                                    });
                                    app.filter('capitalizeWord', function () {

                                        return function (text) {

                                            return (!!text) ? text.charAt(0).toUpperCase() + text.substr(1).toLowerCase() : '';

                                        }

                                    });
                                    app.controller("materialCtrl", function ($scope, $http, $filter) {
                                        $scope.masterRoomList = [];
                                        $scope.room_type = "room";
                                        $scope.getMasterRoomList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Roomlist',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.masterRoomList = response.data.room_data;
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");

                                            });
                                        }
                                        $scope.getMasterRoomList();
                                        $scope.showAddForm = false;
                                        $scope.showForm = function () {
                                            $scope.showAddForm = true;
                                        }
                                        $scope.hideForm = function () {
                                            $scope.remark = "Null";
                                            $scope.recordid = "";
                                            $scope.status = "";
                                            $scope.filter_count = "";
                                            $scope.flterData = [];
                                            $scope.filter_name = "";
                                            $scope.loggerData = [{"logger_id": ""}];
                                            $scope.gaugeData = [{"gauge_id": ""}];
                                            $scope.room_id = "";
                                            $scope.system_id = "";
                                            $scope.showAddForm = false;
                                        }
                                        $scope.getRoomInfo = function (objId) {
                                            $scope.room_name = $filter('filter')($scope.masterRoomList, {id: objId})[0].room_name;
                                            $scope.room_code = $filter('filter')($scope.masterRoomList, {id: objId})[0].room_code;
                                            $scope.area_id = $filter('filter')($scope.masterRoomList, {id: objId})[0].area_code;
                                        }
                                        $scope.sysRoomData = [];
                                        $scope.getDeviceRoomList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist?module=master',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.sysRoomData = response.data.room_list;
                                                console.log(response.data.room_list);
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");

                                            });
                                        }
                                        $scope.getDeviceRoomList();
                                        $scope.loggerData = [{"logger_id": ""}];
                                        $scope.AddLogger = function (index) {
                                            const obj = {};
                                            for (const key of Object.keys($scope.loggerData[index])) {
                                                obj[key] = null;
                                            }
                                            $scope.loggerData.push(obj);

                                        }
                                        $scope.RemoveLogger = function (index) {
                                            //Remove the item from Array using Index.
                                            $scope.loggerData.splice(index, 1);
                                        }
                                        $scope.showAddButtonLogger = function (index) {
                                            return index === $scope.loggerData.length - 1;
                                        };
                                        $scope.gaugeData = [{"gauge_id": ""}];
                                        $scope.AddGauge = function (index) {
                                            const obj = {};
                                            for (const key of Object.keys($scope.gaugeData[index])) {
                                                obj[key] = null;
                                            }
                                            $scope.gaugeData.push(obj);
                                        }
                                        $scope.RemoveGauge = function (index) {
                                            //Remove the item from Array using Index.
                                            $scope.gaugeData.splice(index, 1);
                                        }
                                        $scope.showAddButtonGauge = function (index) {
                                            return index === $scope.gaugeData.length - 1;
                                        };
                                        //Check Filter count
                                        $scope.flterData = [];
                                        $scope.filter_name = "";
                                        $scope.checkFilterCount = function () {
                                            $scope.flterData = [];
                                            $scope.filter_name = "";
                                            if (parseInt($scope.filter_count) < 1 || parseInt($scope.filter_count) > 13) {
                                                $scope.filter_count = 1;

                                            }
                                            if (parseInt($scope.filter_count) >= 1) {
                                                for (var i = 1; i <= parseInt($scope.filter_count); i++) {
                                                    $scope.flterData.push("F" + i)

                                                }
                                            }
                                            $scope.filter_name = $scope.flterData.toString();

                                        }

                                        $scope.saveDeviceRoom = function () {
                                            var logger = [];
                                            angular.forEach($scope.loggerData, function (value, key) {
                                                if (value.logger_id != "") {
                                                    logger.push(value.logger_id);
                                                }

                                            });
                                            var gauge = [];
                                            angular.forEach($scope.gaugeData, function (value, key) {
                                                if (value.gauge_id != "") {
                                                    gauge.push(value.gauge_id);
                                                }

                                            });
                                            var valid_ip = $scope.ValidateIPaddress($scope.system_id);
                                            if (valid_ip) {
                                                if (confirm("Do You Want To Save This Record?")) {
                                                    $http({
                                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Assignip',
                                                        method: "POST",
                                                        data: "room_name=" + $scope.room_name + "&room_code=" + $scope.room_code + "&area_id=" + $scope.area_id + "&room_id=" + $scope.room_id + "&system_id=" + $scope.system_id + "&logger_id=" + logger.toString() + "&gauge_id=" + gauge.toString() + "&room_type=" + $scope.room_type + "&filter_data=" + encodeURIComponent(angular.toJson($scope.flterData)) + "&recordid=" + $scope.recordid+"&status="+$scope.active_status+"&remark="+$scope.remark,
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    })
                                                            .then(function (response) {
                                                                if (response.data.result == true) {
                                                                    $scope.room_id = '';
                                                                    alert(response.data.message);
                                                                    angular.element('#reset').trigger('click');
                                                                    $scope.flterData = [];
                                                                    $scope.status = "";
                                                                    $scope.filter_name = "";
                                                                    $scope.loggerData = [{"logger_id": ""}];
                                                                    $scope.gaugeData = [{"gauge_id": ""}];
                                                                    $scope.room_id = "";
                                                                    $scope.system_id = "";
                                                                    $scope.filter_count = "";
                                                                    $scope.showAddForm = false;
                                                                    $scope.recordid = "";
                                                                    $scope.getDeviceRoomList();
                                                                } else {
                                                                    alert(response.data.message);
                                                                }
                                                            },
                                                                    function (response) { // optional
                                                                    $scope.room_id = '';
                                                                    alert(response.data.message);
                                                                    $scope.flterData = [];
                                                                    $scope.status = "";
                                                                    $scope.filter_name = "";
                                                                    $scope.loggerData = [{"logger_id": ""}];
                                                                    $scope.gaugeData = [{"gauge_id": ""}];
                                                                    $scope.room_id = "";
                                                                    $scope.system_id = "";
                                                                    $scope.filter_count = "";
                                                                    $scope.showAddForm = false;
                                                                    $scope.recordid = "";
                                                                        $scope.getDeviceRoomList();
                                                                        console.log("Something went wrong.Please try again");

                                                                    });
                                                }
                                            }

                                        }

                                        //Created By : Bhupendra Kumar
                                        //Date : 17 March 2020
                                        //Decription : For Deactivate the Selected Record
                                        $scope.CommonDelete = function (id) {
                                            var tblname = "pts_mst_sys_id";
                                            var colname = "id";
                                            if (confirm("Do You Want To Deactivate This Record?")) {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/CommonDelete',
                                                    method: "POST",
                                                    data: "id=" + id + "&tblname=" + tblname + "&colname=" + colname,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    alert(response.data.message);
                                                    $scope.getDeviceRoomList();
                                                }, function (error) { // optional
                                                    alert(response.data.message);
                                                    //console.log("Something went wrong.Please try again");
                                                });
                                            }
                                        }
                                        $scope.recordid = "";
                                        $scope.status = "";
                                        $scope.active_status = false;
                                        $scope.remark = "Null";
                                        $scope.toggleSelection = function toggleSelection(event) {
                                            if (event.target.checked == true) {
                                                $scope.active_status = true;
                                            } else {
                                                $scope.active_status = false;
                                            }
                                        };
                                        $scope.editRoomMaster = function (dataObj) {
                                            $scope.remark="";
                                            $scope.recordid = dataObj.id;
                                            $scope.loggerData = [];
                                            $scope.gaugeData = [];
                                            $scope.flterData = [];
                                            var obj = {};
                                            $scope.room_name = dataObj.room_name;
                                            $scope.room_code = dataObj.room_code;
                                            $scope.area_id = dataObj.area_id;
                                            $scope.room_id = dataObj.room_id;
                                            $scope.system_id = dataObj.device_id;
                                            $scope.room_type = dataObj.room_type;
                                            if (dataObj.status == 'active') {
                                                $scope.active_status = true;
                                            } else {
                                                $scope.active_status = false;
                                            }

                                            var logidarray = dataObj.logger_id.split(',');
                                            for (var i = 0; i < logidarray.length; i++) {
                                                obj = {logger_id: logidarray[i]}
                                                $scope.loggerData.push(obj);
                                            }

                                            var gaugeidarray = dataObj.gauge_id.split(',');
                                            for (var i = 0; i < gaugeidarray.length; i++) {
                                                obj = {gauge_id: gaugeidarray[i]}
                                                $scope.gaugeData.push(obj);
                                            }
                                            $scope.filter_name = dataObj.filter;
                                            var filterarray = dataObj.filter.split(',');
                                            $scope.filter_count = filterarray.length;
                                            for (var i = 0; i < filterarray.length; i++) {
                                                $scope.flterData.push(filterarray[i]);
                                            }
                                            $scope.showAddForm = true;
                                        }

                                        $scope.ValidateIPaddress = function (ipaddress)
                                        {
                                            if (/^([0-9]{1,3})[.]([0-9]{1,3})[.]([0-9]{1,3})[.]([0-9]{1,3})$/.test(ipaddress))
                                            {
                                                return (true)
                                            }
                                            alert("You have entered an invalid Device IP!")
                                            return (false)
                                        }

                                    });
</script>