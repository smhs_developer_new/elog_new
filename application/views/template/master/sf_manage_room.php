<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
    <!-- Page Heading -->

    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row">

                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">         
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th colspan="5" class="f18" style="vertical-align:middle">Room Configuration</th>
                                <th class="text-right">
                                    <?php if ($add) { ?>
                                        <button class="btn btn-success" ng-disabled="recordid > 0" ng-click="showForm()">Add (+)</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success"  ng-click="showForm()"disabled>Add (+)</button>
                                    <?php } ?>
                                </th>
                            </tr>
                        </thead>
                    </table>  
                </div>
                <div class="col-12">
                    <form name="materialForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row">   
                                        <div ng-show="recordid > 0" class="col-lg-4 mb-3"><label><b>Room Name </b><span style="color: red">*</span></label>
                                            <select  ng-disabled="true" class="form-control" tabindex="4" ng-change="getRoomInfo(room_id)"  name="room_id" data-placeholder="Search Room Code" ng-options="dataObj['id'] as (dataObj.room_code +               ' => ' +               dataObj.room_name +               ' => ' +               dataObj.area_code) for dataObj in masterRoomList"  ng-model="room_id" required chosen>
                                                <option value="" readonly>Select Room Name</option>
                                            </select>

                                        </div>
                                        <div ng-show="recordid == ''" class="col-lg-4 mb-3"><label><b>Room Name </b><span style="color: red">*</span></label>
                                            <select class="form-control" tabindex="4" ng-change="getRoomInfo(room_id)"  name="room_id" data-placeholder="Search Room Code" ng-options="dataObj['id'] as (dataObj.room_code +               ' => ' +               dataObj.room_name +               ' => ' +               dataObj.area_code) for dataObj in masterRoomList"  ng-model="room_id" required chosen>
                                                <option value="">Select Room Name</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 mb-3"><label><b>Device IP </b><span style="color: red">*</span>(xxx.xxx.xxx.xxx) Range (0-255)</label>
                                            <input class="form-control" type="text"  placeholder="Device IP" name="system_id" ng-model="system_id" onchange="ValidateIPaddressLocal(this.value)" ng-ip-address required>
                                        </div>
                                        <div class="col-lg-4 mb-3"><label><b>Room Type </b><span style="color: red">*</span></label>
                                            <select class="form-control" name="room_type" ng-model="room_type">
                                                <option value="room">Room</option>
                                                <option value="corridor">Corridor</option>
                                                <option value="IPQA">IPQA</option>
                                            </select>
                                        </div>

                                    </div>


                                </div>
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-sm-4">
                                            <label><b>Data Logger Id</b></label>
                                            <div class="form-row"  ng-repeat="dataObj in loggerData"> 
                                                <div class="col-lg-7 mb-3">
                                                    <input type="text" maxlength="20" class="form-control" name="supplier" ng-model="dataObj.logger_id">
                                                </div>
                                                <div class="col-lg-5 mb-2" style="display:none;">
                                                    <label><span style="color: red"></span></label>
                                                    <button class="btn btn-xs btn-danger" ng-if="loggerData.length > 1" ng-click="RemoveLogger($index)" title="Remove"><i class="fa fa-times" aria-hidden="true"></i></button><!-- end ngIf: reportField[0].length > 1 -->
                                                    &nbsp;
                                                    <button class="btn btn-xs btn-success"  ng-show="showAddButtonLogger($index)" ng-click="AddLogger($index)" title="Add"><i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label><b>Magnehelic Gauge ID</b></label>
                                            <div class="form-row"  ng-repeat="dataObj in gaugeData"> 
                                                <div class="col-lg-7 mb-3">
                                                    <input type="text" maxlength="20" class="form-control" name="supplier" ng-model="dataObj.gauge_id">
                                                </div>
                                                <div class="col-lg-5 mb-2" style="display:none;">
                                                    <label><span style="color: red"></span></label>
                                                    <button class="btn btn-xs btn-danger" ng-if="gaugeData.length > 1" ng-click="RemoveGauge($index)" title="Remove"><i class="fa fa-times" aria-hidden="true"></i></button><!-- end ngIf: reportField[0].length > 1 -->
                                                    &nbsp;
                                                    <button class="btn btn-xs btn-success" ng-show="showAddButtonGauge($index)" ng-click="AddGauge($index)" title="Add"><i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4"><label><b title="Max Limit 13">Filter Count </b></label>
                                            <input class="form-control" only-digits ng-keyup="checkFilterCount()"  type="text" placeholder="Filter Count" name="filter_count" ng-model="filter_count" min="1" max="13">
                                            <p class="mb-0" ng-show="filter_name != ''">{{filter_name}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <div class="form-row"> 

                                        <div class="col-lg-4 mb-3" ><label><b>Remark</b><span ng-show="recordid > 0" style="color: red">*</span></label>
                                            <textarea class="form-control" name="remark" maxlength="150" ng-model="remark" ng-required="recordid > 0"></textarea>
                                        </div>
                                        <div class="col-lg-2 mb-3" ng-show="recordid > 0"><label><b>Active/Inactive</b></label>
                                            <label class="switch">
                                                <input type="checkbox" style="margin-top: 13px;" ng-model="active_status" ng-click="toggleSelection($event)">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <div class="row" style="margin-top:10px;">
                                        <div class="col-sm-12 text-center">
                                            <button class="btn btn-success btn-sm" ng-disabled="materialForm.$invalid"  ng-click="saveDeviceRoom()" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                            <!--<button id="reset" class="btn btn-danger btn-sm" type="reset">&nbsp;&nbsp;&nbsp;&nbsp;RESET &nbsp;&nbsp;&nbsp;&nbsp;</button>-->
                                            <button ng-click="hideForm()" class="btn btn-danger btn-sm" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Cancel &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="form-row" ng-show="sysRoomData.length == 0">
                <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
                <div class="col-lg-12 text-center" ><h4>(Please Select the above parameters)</h4></div> 
            </div>
            <div class="form-row" style="margin-top:10px;" ng-show="sysRoomData.length > 0">   
                <h2 class="f18">Room Configuration List</h2>
                <div class="table-responsive noscroll" >
                    <table  class="table custom-table">
                        <thead >
                            <tr>
                                <th>Room Code</th>
                                <th>Room Name</th>
                                <th>Room Type</th>
                                <th>Room Area</th>
                                <th>Device IP</th>
                                <th>Data Logger ID</th>
                                <th>Magnehelic Gauge ID</th>
                                <th>Filter Desc</th>
                                <th>Last Modified By</th>
                                <th>Last Modified On</th>
                                <th>Action</th>
                                <th>Status</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr dir-paginate="dataObj in sysRoomData|itemsPerPage:records_per_page" total-items="total_records" style="background:none;">
                                <td>{{dataObj.room_code}}</td>
                                <td>{{dataObj.room_name}}</td>
                                <td>{{dataObj.room_type}}</td>
                                <td>{{dataObj.area_id}}</td>
                                <td title="{{dataObj.device_id}}">{{ dataObj.device_id | limitTo: 8 }}{{dataObj.device_id.length > 8 ? '...' : ''}}</td>
                                <td title="{{dataObj.logger_id}}">{{ dataObj.logger_id | limitTo: 8 }}{{dataObj.logger_id.length > 8 ? '...' : ''}}</td>
                                <td title="{{dataObj.gauge_id}}">{{ dataObj.gauge_id | limitTo: 8 }}{{dataObj.gauge_id.length > 8 ? '...' : ''}}</td>
                                <td>{{dataObj.filter}}</td>
                                <td>{{dataObj.modified_by!=""?dataObj.modified_by:dataObj.created_by}}</td>
                                <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td>
                                    <?php if ($edit) { ?>
                                        <!--<button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)">Deactivate</button>-->
                                        <a type="button" ng-click="editRoomMaster(dataObj)"><i class="far fa-edit text-blue pr10"></i></a>
                                    <?php } else { ?>
                                        <!--<button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)"disabled>Deactivate</button>-->
                                        <a type="button" class="btn btn-sm btn-info" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                        <!--<button class="btn btn-sm btn-info" ng-click="editRoomMaster(dataObj)" disabled>Edit</button>-->
                                    <?php } ?>
                                </td>
                                <td ng-if="dataObj.status == 'active'"   class="bg-success text-white">{{dataObj.status| capitalizeWord}}</td>
                                <td ng-if="dataObj.status == 'inactive'" class="bg-danger text-white">{{dataObj.status| capitalizeWord}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getDeviceRoomList(newPageNumber)"></dir-pagination-controls>
                </div>

            </div>
        </div>
    </div>  

</div>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="<?php echo base_url() ?>assets/js/ngIpAddress.min.js"></script>

<script type="text/javascript">
                                    var app = angular.module("materialApp", ['angular.chosen', 'ng-ip-address', 'angularUtils.directives.dirPagination']);
                                    app.directive('chosen', function ($timeout) {

                                        var linker = function (scope, element, attr) {
                                            scope.$watch('masterRoomList', function () {
                                                $timeout(function () {
                                                    element.trigger('chosen:updated');
                                                }, 0, false);
                                            }, true);
                                            $timeout(function () {
                                                element.chosen();
                                            }, 0, false);
                                        };
                                        return {
                                            restrict: 'A',
                                            link: linker
                                        };
                                    });

                                    app.directive('onlyDigits', function () {
                                        return {
                                            require: 'ngModel',
                                            restrict: 'A',
                                            link: function (scope, element, attr, ctrl) {
                                                function inputValue(val) {
                                                    if (val) {
                                                        var digits = val.replace(/[^0-9.]/g, '');

                                                        if (digits.split('.').length > 2) {
                                                            digits = digits.substring(0, digits.length - 1);
                                                        }

                                                        if (digits !== val) {
                                                            ctrl.$setViewValue(digits);
                                                            ctrl.$render();
                                                        }
                                                        return parseFloat(digits);
                                                    }
                                                    return true;
                                                }
                                                ctrl.$parsers.push(inputValue);
                                            }
                                        };
                                    });
                                    app.filter('format', function () {
                                        return function (item) {
                                            var t = item.split(/[- :]/);
                                            var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                            var time = d.getTime();
                                            return time;
                                        };
                                    });
                                    app.filter('capitalizeWord', function () {

                                        return function (text) {

                                            return (!!text) ? text.charAt(0).toUpperCase() + text.substr(1).toLowerCase() : '';

                                        }

                                    });
                                    app.controller("materialCtrl", function ($scope, $http, $filter) {
                                        $scope.masterRoomList = [];
                                        $scope.room_type = "room";
                                        $scope.sysRoomData = [];
                                        $scope.page = 1;
                                        $scope.total_records = 0;
                                        $scope.records_per_page = 10;
                                        $scope.getMasterRoomList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Roomlist',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.masterRoomList = response.data.room_data;
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");

                                            });
                                        }
                                        $scope.getMasterRoomList();
                                        $scope.showAddForm = false;
                                        $scope.showForm = function () {
                                            $scope.showAddForm = true;
                                        }
                                        $scope.filter_count = null;
                                        $scope.remark = "";
                                        $scope.hideForm = function () {
                                            $scope.remark = "";
                                            $scope.recordid = "";
                                            $scope.status = "";
                                            $scope.filter_count = null;
                                            $scope.flterData = [];
                                            $scope.filter_name = "";
                                            $scope.loggerData = [{"logger_id": ""}];
                                            $scope.gaugeData = [{"gauge_id": ""}];
                                            $scope.room_id = "";
                                            $scope.system_id = "";
                                            $scope.showAddForm = false;
                                        }
                                        $scope.getRoomInfo = function (objId) {
                                            $scope.room_name = $filter('filter')($scope.masterRoomList, {id: objId})[0].room_name;
                                            $scope.room_code = $filter('filter')($scope.masterRoomList, {id: objId})[0].room_code;
                                            $scope.area_id = $filter('filter')($scope.masterRoomList, {id: objId})[0].area_code;
                                        }
                                        $scope.sysRoomData = [];
                                        $scope.getDeviceRoomList = function (page) {
                                            $scope.page = page;
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist?module=master&page=' + page,
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.sysRoomData = response.data.room_list;
                                                $scope.total_records = response.data.total_records;
                                                $scope.records_per_page = response.data.records_per_page;
                                                //console.log(response.data.room_list);
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");

                                            });
                                        }
                                        $scope.getDeviceRoomList($scope.page);
                                        $scope.loggerData = [{"logger_id": ""}];
                                        $scope.AddLogger = function (index) {
                                            const obj = {};
                                            for (const key of Object.keys($scope.loggerData[index])) {
                                                obj[key] = null;
                                            }
                                            $scope.loggerData.push(obj);

                                        }
                                        $scope.RemoveLogger = function (index) {
                                            //Remove the item from Array using Index.
                                            $scope.loggerData.splice(index, 1);
                                        }
                                        $scope.showAddButtonLogger = function (index) {
                                            return index === $scope.loggerData.length - 1;
                                        };
                                        $scope.gaugeData = [{"gauge_id": ""}];
                                        $scope.AddGauge = function (index) {
                                            const obj = {};
                                            for (const key of Object.keys($scope.gaugeData[index])) {
                                                obj[key] = null;
                                            }
                                            $scope.gaugeData.push(obj);
                                        }
                                        $scope.RemoveGauge = function (index) {
                                            //Remove the item from Array using Index.
                                            $scope.gaugeData.splice(index, 1);
                                        }
                                        $scope.showAddButtonGauge = function (index) {
                                            return index === $scope.gaugeData.length - 1;
                                        };
                                        //Check Filter count
                                        $scope.flterData = [];
                                        $scope.filter_name = "";
                                        $scope.checkFilterCount = function () {
                                            $scope.flterData = [];
                                            $scope.filter_name = "";
                                            if (parseInt($scope.filter_count) < 1 || parseInt($scope.filter_count) > 13) {
                                                $scope.filter_count = null;

                                            }
                                            if (parseInt($scope.filter_count) >= 1) {
                                                for (var i = 1; i <= parseInt($scope.filter_count); i++) {
                                                    $scope.flterData.push("F" + i)

                                                }
                                            }
                                            $scope.filter_name = $scope.flterData.toString();

                                        }

                                        $scope.saveDeviceRoom = function () {
                                            var logger = [];
                                            angular.forEach($scope.loggerData, function (value, key) {
                                                if (value.logger_id != "") {
                                                    logger.push(value.logger_id);
                                                }

                                            });
                                            var gauge = [];
                                            angular.forEach($scope.gaugeData, function (value, key) {
                                                if (value.gauge_id != "") {
                                                    gauge.push(value.gauge_id);
                                                }

                                            });
                                            var valid_ip = $scope.ValidateIPaddress($scope.system_id);
                                            if (valid_ip) {
                                                var msg = 'Save';
                                                if ($scope.recordid > 0) {
                                                    var msg = 'Update';
                                                }

                                                if (confirm("Do You Want To " + msg + " This Record?")) {
                                                    $http({
                                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Assignip',
                                                        method: "POST",
                                                        data: "room_name=" + $scope.room_name + "&room_code=" + $scope.room_code + "&area_id=" + $scope.area_id + "&room_id=" + $scope.room_id + "&system_id=" + $scope.system_id + "&logger_id=" + logger.toString() + "&gauge_id=" + gauge.toString() + "&room_type=" + $scope.room_type + "&filter_data=" + encodeURIComponent(angular.toJson($scope.flterData)) + "&recordid=" + $scope.recordid + "&status=" + $scope.active_status + "&remark=" + $scope.remark,
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    })
                                                            .then(function (response) {
                                                                if (response.data.result == true) {
                                                                    $scope.room_id = '';
                                                                    alert(response.data.message);
                                                                    angular.element('#reset').trigger('click');
                                                                    $scope.flterData = [];
                                                                    $scope.status = "";
                                                                    $scope.filter_name = "";
                                                                    $scope.loggerData = [{"logger_id": ""}];
                                                                    $scope.gaugeData = [{"gauge_id": ""}];
                                                                    $scope.room_id = "";
                                                                    $scope.system_id = "";
                                                                    $scope.filter_count = "";
                                                                    $scope.showAddForm = false;
                                                                    $scope.recordid = "";
                                                                    $scope.remark = "";
                                                                    $scope.getDeviceRoomList($scope.page);
                                                                } else {
                                                                    alert(response.data.message);
                                                                }
                                                            },
                                                                    function (response) { // optional
                                                                        $scope.room_id = '';
                                                                        alert(response.data.message);
                                                                        $scope.flterData = [];
                                                                        $scope.status = "";
                                                                        $scope.filter_name = "";
                                                                        $scope.loggerData = [{"logger_id": ""}];
                                                                        $scope.gaugeData = [{"gauge_id": ""}];
                                                                        $scope.room_id = "";
                                                                        $scope.system_id = "";
                                                                        $scope.filter_count = null;
                                                                        $scope.showAddForm = false;
                                                                        $scope.recordid = "";
                                                                        $scope.remark = "";
                                                                        $scope.getDeviceRoomList($scope.page);
                                                                        console.log("Something went wrong.Please try again");

                                                                    });
                                                }
                                            }

                                        }

                                        //Created By : Bhupendra Kumar
                                        //Date : 17 March 2020
                                        //Decription : For Deactivate the Selected Record
                                        $scope.CommonDelete = function (id) {
                                            var tblname = "pts_mst_sys_id";
                                            var colname = "id";
                                            if (confirm("Do You Want To Deactivate This Record ?")) {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/CommonDelete',
                                                    method: "POST",
                                                    data: "id=" + id + "&tblname=" + tblname + "&colname=" + colname,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    alert(response.data.message);
                                                    $scope.getDeviceRoomList($scope.page);
                                                }, function (error) { // optional
                                                    alert(response.data.message);
                                                    //console.log("Something went wrong.Please try again");
                                                });
                                            }
                                        }
                                        $scope.recordid = "";
                                        $scope.status = "";
                                        $scope.active_status = false;
                                        $scope.remark = "";
                                        $scope.toggleSelection = function toggleSelection(event) {
                                            if (event.target.checked == true) {
                                                $scope.active_status = true;
                                            } else {
                                                $scope.active_status = false;
                                            }
                                        };
                                        $scope.editRoomMaster = function (dataObj) {
                                            $scope.remark = "";
                                            $scope.recordid = dataObj.id;
                                            $scope.loggerData = [];
                                            $scope.gaugeData = [];
                                            $scope.flterData = [];
                                            var obj = {};
                                            $scope.room_name = dataObj.room_name;
                                            $scope.room_code = dataObj.room_code;
                                            $scope.area_id = dataObj.area_id;
                                            $scope.room_id = dataObj.room_id;
                                            $scope.system_id = dataObj.device_id;
                                            $scope.room_type = dataObj.room_type;
                                            if (dataObj.status == 'active') {
                                                $scope.active_status = true;
                                            } else {
                                                $scope.active_status = false;
                                            }

                                            var logidarray = dataObj.logger_id.split(',');
                                            for (var i = 0; i < logidarray.length; i++) {
                                                obj = {logger_id: logidarray[i]}
                                                $scope.loggerData.push(obj);
                                            }

                                            var gaugeidarray = dataObj.gauge_id.split(',');
                                            for (var i = 0; i < gaugeidarray.length; i++) {
                                                obj = {gauge_id: gaugeidarray[i]}
                                                $scope.gaugeData.push(obj);
                                            }
                                            $scope.filter_name = dataObj.filter;
                                            if (dataObj.filter == '') {
                                                $scope.filter_count = null;
                                            } else {
                                                var filterarray = dataObj.filter.split(',');
                                                $scope.filter_count = filterarray.length;

                                                for (var i = 0; i < filterarray.length; i++) {
                                                    $scope.flterData.push(filterarray[i]);
                                                }
                                            }
                                            $scope.showAddForm = true;
                                        }

                                        $scope.ValidateIPaddress = function (ipaddress)
                                        {
                                            if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)) {
                                                return (true)
                                            }
                                            alert("You have entered an invalid IP address!")
                                            return (false)
                                        }

                                    });
                                    function ValidateIPaddressLocal(ipaddress) {
                                        var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
                                        if (ipaddress.match(ipformat)) {
                                        } else {
                                            alert("You have entered an invalid IP address!");
                                            return (false)
                                        }
                                    }
</script>