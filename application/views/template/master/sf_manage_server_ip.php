<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
    <!-- Page Heading -->

    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row">

                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">    	   
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th colspan="5" class="f18" style="vertical-align:middle">Server Configuration Master </th>
                                <th class="text-right">
                                    <?php if ($add) { ?>
                                        <button class="btn btn-success" ng-click="showForm()">Add Server Ip</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success" ng-click="showForm()" disabled>Add Server Ip</button>
                                    <?php } ?>
                                </th>
                            </tr>
                        </thead>
                    </table>  
                </div>
                <div class="col-12">
                    <form name="materialForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row">   
                                        <div class="col-lg-4 mb-3"><label><b>IP Address</b></label>
                                            <input type="text" class="form-control" name="ip_address" ng-model="ip_address">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-success btn-sm" ng-disabled="materialForm.$invalid || ip_address== '' "  ng-click="saveServer()" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="form-row" ng-show="serverData.length == 0">
                <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
                <div class="col-lg-12 text-center" ><h4>(Please Select the above parameters)</h4></div> 
            </div>
            <div class="form-row" style="margin-top:10px;" ng-show="serverData.length > 0">   
                <h2 class="f18">Server Configuration List</h2>
                <table  class="table custom-table">
                    <thead >
                        <tr>
                            <th>Sr. No.</th>
                            <th>Server Ip</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr dir-paginate="dataObj in serverData|itemsPerPage:records_per_page" total-items="total_records" style="background:none;">
                            <td>{{$index+1}}</td>
                            <td>{{dataObj.server_ip}}</td>
                            <td>
                                 <?php if ($edit) { ?>
                                    <button type="button" class="btn btn-sm btn-info" ng-click="editServer(dataObj)">Edit</button>&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-sm btn-danger" ng-click="deleteServer(dataObj.id)">Deactivate</button>
                                <?php } else { ?>
                                    <button type="button" class="btn btn-sm btn-info" ng-click="editServer(dataObj)" disabled>Edit</button>&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-sm btn-danger" ng-click="deleteServer(dataObj.id)" disabled>Deactivate</button>
                                <?php } ?>
                                
                            </td>
                        </tr>
                    </tbody>
                </table>
                <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getServerList(newPageNumber)"></dir-pagination-controls>
            </div>
        </div>
    </div>	

</div>

<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>

<script type="text/javascript">
                                var app = angular.module("materialApp", ['angular.chosen','angularUtils.directives.dirPagination']);
                                app.controller("materialCtrl", function ($scope, $http, $filter) {
                                    $scope.serverData = [];
                                    $scope.page = 1;
                                    $scope.total_records = 0;
                                    $scope.records_per_page = 10;
                                    $scope.getServerList = function (page) {
                                        $scope.page = page;
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetServerList?page='+page,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.serverData = response.data.ip_list;
                                            $scope.total_records = response.data.total_records;
                                            $scope.records_per_page = response.data.records_per_page;
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getServerList($scope.page);
                                    $scope.showAddForm = false;
                                    $scope.showForm = function () {
                                        $scope.showAddForm = true;
                                    }
                                    $scope.ip_address = '';
                                    $scope.ipId = 0;
                                    $scope.editServer = function (dataObj) {
                                        $scope.showAddForm = true;
                                        $scope.ip_address = dataObj.server_ip;
                                        $scope.ipId = dataObj.id;
                                    }
                                    $scope.saveServer = function () {
                                        if (confirm("Do You Want To Save This Record?")) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/saveServer',
                                                method: "POST",
                                                data: "ip_address=" + $scope.ip_address + "&ip_id=" + $scope.ipId,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            })
                                                    .then(function (response) {
                                                        alert(response.data.message);
                                                        $scope.ip_address = '';
                                                        $scope.ipId = 0;
                                                        $scope.showAddForm = false;
                                                       $scope.getServerList($scope.page);
                                                    },
                                                            function (response) { // optional
                                                                $scope.showAddForm = false;
                                                                $scope.ipId = 0;
                                                                $scope.getServerList($scope.page);
                                                                console.log("Something went wrong.Please try again");
                                                            });
                                        }
                                    }

                                    $scope.deleteServer = function (objId) {
                                        if (confirm("Do You Want To Deactivate This Record")) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/deleteServer',
                                                method: "POST",
                                                data: "id=" + objId,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            })
                                                    .then(function (response) {
                                                        $scope.getServerList($scope.page);
                                                        alert(response.data.message);
                                                    },
                                                            function (response) { // optional
                                                                $scope.getServerList($scope.page);
                                                                console.log("Something went wrong.Please try again");
                                                            });
                                        }
                                    }

                                });
</script>