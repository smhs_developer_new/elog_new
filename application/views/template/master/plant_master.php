<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="plantApp" ng-controller="plantCtrl" ng-cloak>
    <!-- Page Heading -->
    <div class="content-wrapper">
        <div class="card card-default mt-4">
            <div class="card-body">
                <div class="form-row">
                    <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">
                        <table class="table table-dark">
                            <thead>
                                <tr>
                                    <th colspan="5" class="f18" style="vertical-align:middle">Plant Master</th>
                                    <th class="text-right">
                                        <?php if ($add) { ?>
                                            <button class="btn btn-success" ng-disabled="recordid > 0" ng-click="showForm()">Add (<i class="fas fa-plus"></i>)</button>
                                        <?php } else { ?>
                                            <button class="btn btn-success"  ng-click="showForm()"disabled>Add (+)</button>
                                        <?php } ?>

                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <form name="materialForm" novalidate="" class="ng-pristine ng-valid ng-valid-required">
                            <div class="card card-default" ng-show="showAddForm">
                                <div class="card-body">
                                    <div class="col-auto">
                                        <form>

                                            <fieldset>

                                                <div class="col-auto">
                                                    <!-- <div class="form-row">
                                                        <div class="col-lg-12 mb-3"><label for="validationServer01">Sanitization Used <span style="color: red">*</span></label><input class="form-control autocomplete" type="text" placeholder="Sanitization Used" name="solutionname" id="solutionname" required> 
                                                        </div>
                                                     </div>   -->
                                                    <div class="form-row">   
                                                        <div class="col-lg-4 mb-3"><label for="aqty">Plant Code <span style="color: red">*</span></label>
                                                            <input ng-disabled="recordid > 0" class="form-control" id="pcode" maxlength="20" type="text" placeholder="Plant Code" name="pcode" ng-model="pcode" required>
                                                        </div>
                                                        <div class="col-lg-4 mb-3"><label for="edso">Plant Name <span style="color: red">*</span></label>
                                                            <input class="form-control" id="pname" type="text" maxlength="50" placeholder="Plant Name" ng-model="pname" required>
                                                        </div>
                                                        <div class="col-lg-4 mb-3"><label for="dpoints"> No of Blocks <span style="color: red">*</span></label>
                                                            <input class="form-control cc" id="pblock" type="text" maxlength="2" placeholder="No. Blocks" name="pblock" ng-model="pblock" required>

                                                        </div>


                                                    </div>

                                                    <div class="form-row">
                                                        <div class="col-lg-4 mb-3"><label for="rtc">Plant Address <span style="color: red">*</span></label>
                                                            <textarea class="form-control" maxlength="500" aria-label="With textarea" name="paddress" placeholder="Plant Address" ng-model="paddress" required></textarea>
                                                        </div>
                                                        <div class="col-lg-4 mb-3"><label for="validationServer01">Plant Contact <span style="color: red">*</span></label>
                                                            <input class="form-control cc" id="pcontact" type="text" only-digit placeholder="Plant Contact" name="pcontact" maxlength="10" minlength="10" pattern="(.){10,10}" title="Enter 10 digits phone number"ng-model="pcontact" required>

                                                        </div>

                                                        <div class="col-lg-4 mb-3"><label for="validationServer01">Remarks <span style="color: red" ng-show="editmodeheader">*</span></label>
                                                            <textarea class="form-control" aria-label="With textarea" name="premarks" maxlength="100" placeholder="Enter Remarks" ng-model="premarks" ng-required="editmodeheader"></textarea>

                                                        </div>

                                                    </div>
                                                    <div class="form-row" style="display:none">
                                                        <div class="col-lg-2 mb-3" ng-show="recordid > 0"><label><b>Active/Inactive</b></label>
                                                            <label class="switch">
                                                                <input type="checkbox" style="margin-top: 13px;" ng-model="active_status" ng-click="toggleSelection($event)">
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </div>

                                                    </div>

                                                </div>
                                            </fieldset>

                                        </form>
                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-sm-12 text-center">
                                            <button class="btn btn-success btn-sm" ng-show="recordid == ''" ng-disabled="materialForm.$invalid" ng-click="savePlant()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                            <button class="btn btn-success btn-sm" ng-show="recordid > 0" ng-disabled="materialForm.$invalid" ng-click="updatePlant()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                            <button class="btn btn-danger btn-sm" ng-click="hideForm()" type="button">&nbsp;&nbsp;&nbsp;&nbsp; Cancel &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <div class="form-row" style="margin-top:10px;" ng-show="plantData.length == 0">
                                <div class="col-lg-12 text-center f18" ><h2>No Data Found</h2></div>
                            </div>
                            <div class="form-row" style="margin-top:10px; padding:5px;" ng-show="plantData.length > 0">
                                <h2 class="f18">Plant Master List</h2>
                                <table class="table custom-table f1">
                                    <thead>
                                        <tr>
                                            <th>Plant Code</th>
                                            <th>Plant Name</th>
                                            <th>No. Of Blocks</th>
                                            <th>Last Modified By</th>
                                            <th>Last Modified On</th>
                                            <th>Action</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- ngRepeat: dataObj in roomActivityData -->
                                        <tr dir-paginate="dataObj in plantData|itemsPerPage:records_per_page" total-items="total_records" style="background:none;">
                                            <td>{{dataObj.plant_code}}</td>
                                            <td>{{dataObj.plant_name}}</td>
                                            <td>{{dataObj.number_of_blocks}}</td>
                                            <td>{{dataObj.modified_by!=NULL?dataObj.modified_by:dataObj.created_by}}</td>
                                            <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                            <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                            <td>
                                                <?php if ($edit) { ?>
                                                    <a ng-click="editPlant(dataObj)"><i class="far fa-edit text-blue pr10" title="Edit"></i></a>
                                                <?php } else { ?>
                                                    <a type="button" class="btn btn-sm btn-info" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                                <?php } ?>

                                            </td>
                                            <td ng-if="dataObj.is_active == '1'"   class="bg-success text-white">Active</td>
                                            <td ng-if="dataObj.is_active == '0'"   class="bg-danger text-white">Inactive</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getPlantList(newPageNumber)"></dir-pagination-controls>
                            </div>

                    </div>
                </div>
            </div>
        </div>
        <script src="<?php echo base_url(); ?>js/angular.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
        <script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
        <script src="<?php echo base_url() ?>assets/js/ngIpAddress.min.js"></script>
        <script type="text/javascript">
                                                var app = angular.module("plantApp", ['angular.chosen', 'ng-ip-address','angularUtils.directives.dirPagination']);
                                                app.directive('onlyDigits', function () {
                                                    return {
                                                        require: 'ngModel',
                                                        restrict: 'A',
                                                        link: function (scope, element, attr, ctrl) {
                                                            function inputValue(val) {
                                                                if (val) {
                                                                    var digits = val.replace(/[^0-9.]/g, '');

                                                                    if (digits.split('.').length > 2) {
                                                                        digits = digits.substring(0, digits.length - 1);
                                                                    }

                                                                    if (digits !== val) {
                                                                        ctrl.$setViewValue(digits);
                                                                        ctrl.$render();
                                                                    }
                                                                    return parseFloat(digits);
                                                                }
                                                                return undefined;
                                                            }
                                                            ctrl.$parsers.push(inputValue);
                                                        }
                                                    };
                                                });
                                                app.filter('format', function () {
                                                    return function (item) {
                                                        var t = item.split(/[- :]/);
                                                        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                                        var time = d.getTime();
                                                        return time;
                                                    };
                                                });
                                                app.filter('capitalizeWord', function () {

                                                    return function (text) {

                                                        return (!!text) ? text.charAt(0).toUpperCase() + text.substr(1).toLowerCase() : '';

                                                    }

                                                });
                                                app.controller("plantCtrl", function ($scope, $http, $filter) {
                                                    $scope.showAddForm = false;
                                                    $scope.showForm = function () {
                                                        $scope.showAddForm = true;
                                                    }
                                                    $scope.premarks = "";
                                                    $scope.hideForm = function () {
                                                        $scope.recordid = "";
                                                        $scope.editmodeheader = false;
                                                        $scope.premarks = "";
                                                        $scope.pcode = "";
                                                        $scope.pname = "";
                                                        $scope.pblock = "";
                                                        $scope.paddress = "";
                                                        $scope.pcontact = "";
                                                        $scope.showAddForm = false;
                                                    }
                                                    //Get Plant List
                                                    $scope.plantData = [];
                                                    $scope.page = 1;
                                                    $scope.total_records = 0;
                                                    $scope.records_per_page = 10;
                                                    $scope.getPlantList = function (page) {
                                                        $scope.page = page;
                                                        $http({
                                                            url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getPlantList?module=master&page='+page,
                                                            method: "GET",
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        }).then(function (response) {
                                                            $scope.plantData = response.data.plant_list;
                                                            $scope.total_records = response.data.total_records;
                                                            $scope.records_per_page = response.data.records_per_page;
                                                            //console.log($scope.plantData);
                                                        }, function (error) { // optional
                                                            $scope.plantData = [];
                                                            //console.log("Something went wrong.Please try again");

                                                        });
                                                    }
                                                    $scope.getPlantList($scope.page);

                                                    $scope.savePlant = function () {
                                                        if (confirm("Do You Want To Save This Record?")) {
                                                            $http({
                                                                url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/mstPlantSubmit',
                                                                method: "POST",
                                                                data: "pcode=" + $scope.pcode + "&pname=" + $scope.pname + "&paddress=" + $scope.paddress + "&premarks=" + $scope.premarks + "&pcontact=" + $scope.pcontact + "&pblock=" + $scope.pblock,
                                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                            })
                                                                    .then(function (response) {
                                                                        if (response.data.result == true) {
                                                                            alert(response.data.message);
                                                                            $scope.editmodeheader = false;
                                                                            $scope.recordid = "";
                                                                            $scope.premarks = "";
                                                                            $scope.pcode = "";
                                                                            $scope.pname = "";
                                                                            $scope.pblock = "";
                                                                            $scope.paddress = "";
                                                                            $scope.pcontact = "";
                                                                            $scope.showAddForm = false;
                                                                            $scope.getPlantList($scope.page);
                                                                        } else {
                                                                            alert(response.data.message);
                                                                        }
                                                                    },
                                                                            function (response) { // optional
                                                                                alert(response.data.message);
                                                                                $scope.editmodeheader = false;
                                                                                $scope.recordid = "";
                                                                                $scope.premarks = "";
                                                                                $scope.pcode = "";
                                                                                $scope.pname = "";
                                                                                $scope.pblock = "";
                                                                                $scope.paddress = "";
                                                                                $scope.pcontact = "";
                                                                                $scope.showAddForm = false;
                                                                                $scope.getPlantList($scope.page);
                                                                                console.log("Something went wrong.Please try again");

                                                                            });
                                                        }

                                                    }
                                                    $scope.toggleSelection = function toggleSelection(event) {
                                                        if (event.target.checked == true) {
                                                            $scope.active_status = true;
                                                        } else {
                                                            $scope.active_status = false;
                                                        }
                                                    };
                                                    $scope.recordid = "";
                                                    $scope.editPlant = function (dataObj) {
                                                        
                                                        $scope.editmodeheader = true;
                                                        $scope.recordid = dataObj.id;
                                                        $scope.premarks = "";
                                                        $scope.pcode = dataObj.plant_code;
                                                        $scope.pname = dataObj.plant_name;
                                                        $scope.pblock = dataObj.number_of_blocks;
                                                        $scope.paddress = dataObj.plant_address;
                                                        $scope.pcontact = dataObj.plant_contact;
                                                        if (dataObj.is_active == '1') {
                                                            $scope.active_status = true;
                                                        } else {
                                                            $scope.active_status = false;
                                                        }
                                                        $scope.active_status = true;
                                                        $scope.showAddForm = true;
                                                    }
                                                    $scope.updatePlant = function () {
                                                        if (confirm("Do You Want To Update This Record?")) {
                                                            $http({
                                                                url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/mstPlantUpdate',
                                                                method: "POST",
                                                                data: "pcode=" + $scope.pcode + "&pname=" + $scope.pname + "&paddress=" + $scope.paddress + "&premarks=" + $scope.premarks + "&pcontact=" + $scope.pcontact + "&pblock=" + $scope.pblock + "&plantid=" + $scope.recordid + "&status=" + $scope.active_status,
                                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                            })
                                                                    .then(function (response) {
                                                                        if (response.data.result == true) {
                                                                            alert(response.data.message);
                                                                            $scope.editmodeheader = false;
                                                                            $scope.recordid = "";
                                                                            $scope.premarks = "";
                                                                            $scope.pcode = "";
                                                                            $scope.pname = "";
                                                                            $scope.pblock = "";
                                                                            $scope.paddress = "";
                                                                            $scope.pcontact = "";
                                                                            $scope.showAddForm = false;
                                                                            $scope.getPlantList($scope.page);
                                                                        } else {
                                                                            alert(response.data.message);
                                                                        }
                                                                    },
                                                                            function (response) { // optional
                                                                                alert(response.data.message);
                                                                                $scope.editmodeheader = false;
                                                                                $scope.recordid = "";
                                                                                $scope.premarks = "";
                                                                                $scope.pcode = "";
                                                                                $scope.pname = "";
                                                                                $scope.pblock = "";
                                                                                $scope.paddress = "";
                                                                                $scope.pcontact = "";
                                                                                $scope.showAddForm = false;
                                                                                $scope.getPlantList($scope.page);
                                                                                console.log("Something went wrong.Please try again");

                                                                            });
                                                        }

                                                    }


                                                });
                                                $(document).ready(function () {
                                                    $('.cc').bind('keyup paste', function () {
                                                        this.value = this.value.replace(/[^0-9]/g, '');
                                                    });

                                                });
        </script>