<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid">
    <!-- Page Heading -->

    <div class="content-wrapper">
        <div class="content-heading executesop-heading">
            <div class="col-sm-5 pl-0">Sub Block Master</div>
            <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
            <div class="col-sm-7 pr-0">
                <ol class="breadcrumb ml-auto">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
                    <li class="breadcrumb-item active"><a href="<?= base_url() ?>sub_block_view?module_id=<?php echo $_GET['module_id'] ?>">Sub Block</a></li>

                </ol>
            </div>
        </div>



        <div class="card card-default">
            <div class="card-body">                     
                <div class="row">
                    <div class="col-sm-12 text-right">

                        <a class="btn btn-primary btn-lg" href="<?= base_url() ?>sub_block_view?module_id=<?php echo $_GET['module_id'] ?>" > Back To list</a>  
                        &nbsp;&nbsp;&nbsp;
                        <?php if ($add) { ?><a class="btn btn-primary btn-lg" href="<?= base_url() ?>create_sub_block?module_id=<?php echo $_GET['module_id']; ?>"> Add New Sub Block</a>
                        <?php } else { ?>
                            <button class="btn btn-primary btn-lg"  disabled> Add New Sub Block</button>
                        <?php } ?>


                    </div>
                </div>
            </div>
        </div>

        <form id="department">
            <div class="card card-default">
                <div class="card-body">

                    <fieldset>

                        <div class="col-auto">
                            <!-- <div class="form-row">
                                <div class="col-lg-12 mb-3"><label for="validationServer01">Sanitization Used <span style="color: red">*</span></label><input class="form-control autocomplete" type="text" placeholder="Sanitization Used" name="solutionname" id="solutionname" required> 
                                </div>
                             </div>   -->
                            <div class="form-row">   
                                <div class="col-lg-4 mb-3"><label for="aqty">Sub Block Code <span style="color: red">*</span></label><input class="form-control" id="dcode" maxlength="20" type="text" placeholder="Sub Block Code" name="dcode" required >
                                </div>
                                <div class="col-lg-4 mb-3"><label for="edso">Sub Block Name <span style="color: red">*</span></label><input class="form-control" id="dname" maxlength="50" type="text" placeholder="Sub Block Name" name="dname" required >
                                </div>
                                <div class="col-lg-4 mb-3"><label for="rtc">Sub Block contact </label><input class="form-control cc" id="dcontact" type="text" placeholder="Sub Block Contact" name="dcontact" maxlength="10" minlength="10" pattern="(.){10,10}" title="Enter 10 digits phone number">
                                </div>

                            </div>

                            <div class="form-row">


<!--<div class="col-lg-4 mb-3"><label for="rtc">Sub Block Address <span style="color: red">*</span></label><textarea class="form-control" aria-label="With textarea" name="daddress" placeholder="Department Address" required></textarea>
</div>-->


                                <div class="col-lg-4 mb-3"><label for="validationServer01">Block Code <span style="color: red">* <?php $cc = $data['bcode']->num_rows() == 0 ? "Please Add Block First" : "";
                        echo $cc;
                        ?></span></label>
                                    <select id="bcode" name="bcode" class="form-control" required>
                                        <option value="" selected disabled>Select Block Code </option>
                                        <?php if ($data['bcode']->num_rows() > 0) { ?>
                                            <?php
                                            foreach ($data['bcode']->result() as $row) {
                                                if ($row->block_code != 'Block-000') {
                                                    ?>
                                                    <option value="<?php echo $row->block_code ?>"><?php echo $row->block_code ?> | <?php echo $row->block_name ?> </option>
                                                <?php
                                                }
                                            }
                                        }
                                        ?> 
                                    </select>

                                </div>

                <!--<div class="col-lg-4 mb-3"><label for="validationServer01">Area Code <span style="color: red">*</span> <span style="color: red" id="emsg"></span></label>
                  <select id="acode" name="acode" class="form-control" required>
                  <option value="" selected disabled>Select Area Code </option>
                  </select>
                </div>-->

                                <div class="col-lg-4 mb-3"><label for="validationServer01">Remarks </label><textarea class="form-control" aria-label="With textarea" name="dremarks" maxlength="100" placeholder="Enter Remarks" ></textarea>
                                </div>


                            </div>
                            <!--<div class="form-row">
                            <div class="col-lg-12 mb-3"><label for="validationServer01">Remarks <span style="color: red">*</span></label><textarea class="form-control" aria-label="With textarea" name="dremarks" placeholder="Enter Remarks" required></textarea></div> 
                            </div>-->

                    </fieldset>
                </div>
            </div>
            <div class="card card-default">
                <div class="card-body" >           
                    <div class="row disable-button-color">
                        <div class="col-sm-12 text-center" id="sbtn">
                            <?php if ($add) { ?>
                                <button id="sbbtn" class="btn btn-success btn-lg" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
<?php } else { ?>
                                <button class="btn btn-success btn-lg" type="button" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
<?php } ?>

                        </div>
                    </div>
                </div>
            </div><!-- END card-->

        </form>

    </div>





</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.cc').bind('keyup paste', function () {
            this.value = this.value.replace(/[^0-9]/g, '');
        });

        $("#bcode").change(function () {
            var bcode = $(this).val();
            $.ajax({
                url: '<?= base_url() ?>ponta_sahib/Mastercontroller/checkBlocklimit',
                type: 'POST',
                data: {bcode: bcode},
                success: function (res) {
                    console.log(res);
                    if (res.status == 0) {
                        // $("#validationnow21").attr("disabled",true);
                        // $("#validationnow22").attr("disabled",false);
                        alert("The number of sub blocks for this block has reached its maximum value. You cannot create any more sub block.");
                        $("#sbbtn").attr('disabled', true);
                    } else {
                        $("#sbbtn").attr('disabled', false);
                    }
                }
            });
        });



        $("#dcode").change(function () {
            var dcode = $(this).val();
            $.ajax({
                url: '<?= base_url() ?>ponta_sahib/Mastercontroller/checkDepartmentcode',
                type: 'POST',
                data: {dcode: dcode},
                success: function (res) {
                    //console.log(res);
                    if (res.status == 0) {
                        // $("#validationnow21").attr("disabled",true);
                        // $("#validationnow22").attr("disabled",false);
                        alert("Sub Block Code Already Existed");
                        $("#dcode").val("");
                    }
                }
            });
        });

        $('#department').submit(function () {
            $.ajax({
                url: '<?= base_url() ?>ponta_sahib/Mastercontroller/mstDepartmentSubmit',
                type: 'POST',
                data: $('#department').serialize(),
                success: function (res) {
                    //console.log(res);
                    if (res.status == 1) {
                        // $("#validationnow21").attr("disabled",true);
                        // $("#validationnow22").attr("disabled",false);
                        alert("Sub Block Added successfully");
                        window.location.href = "<?= base_url() ?>sub_block_view?module_id=<?php echo $_GET['module_id'] ?>";
//			window.location.reload();
                                            } else {
                                                alert(res.msg);
                                            }
                                        }
                                    });
                                    return false;
                                });


                            });
</script>