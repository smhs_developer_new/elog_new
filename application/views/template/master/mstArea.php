<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid">
          <!-- Page Heading -->
			
          <div class="content-wrapper">
            <div class="content-heading executesop-heading">
              <div class="col-sm-5 pl-0">Area Master</div>
              <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
              <div class="col-sm-7 pr-0">
                <ol class="breadcrumb ml-auto">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
                  <li class="breadcrumb-item active"><a href="<?= base_url()?>area_view?module_id=<?php echo $_GET['module_id'] ?>">Area</a></li>
                  
                </ol>
              </div>
            </div>
            
      
      
      <div class="card card-default">
      <div class="card-body">                     
      <div class="row">
      <div class="col-sm-12 text-right">
      
      <a class="btn btn-primary btn-lg" href="<?= base_url()?>area_view?module_id=<?php echo $_GET['module_id'] ?>"> Back To list</a>  
      &nbsp;&nbsp;&nbsp;
      <?php if ($add) { ?><a class="btn btn-primary btn-lg" href="<?= base_url()?>manage_area?module_id=<?php echo $_GET['module_id'] ?>"> Add New Area</a>
                        <?php } else { ?>
                            <button class="btn btn-primary btn-lg"  disabled> Add New Area</button>
                        <?php } ?>
      
      
      </div>
      </div>
      </div>
      </div>
      
      <form id="area">
    <div class="card card-default">
        <div class="card-body">

	  <fieldset>
                        
                           <div class="col-auto">
                             <!-- <div class="form-row">
                                 <div class="col-lg-12 mb-3"><label for="validationServer01">Sanitization Used <span style="color: red">*</span></label><input class="form-control autocomplete" type="text" placeholder="Sanitization Used" name="solutionname" id="solutionname" required> 
                                 </div>
                              </div>   -->
                              <div class="form-row">   
                                 <div class="col-lg-4 mb-3"><label for="aqty">Area Code <span style="color: red">*</span></label><input class="form-control" id="acode" maxlength="10" type="text" placeholder="Area Code" name="acode" required >
                                 </div>
                                 <div class="col-lg-4 mb-3"><label for="edso">Area Name <span style="color: red">*</span></label><input class="form-control" id="aname" maxlength="20" type="text" placeholder="Area Name" name="aname" required >
                                 </div>
                                 <div class="col-lg-4 mb-3"><label for="rtc">Area contact </label><input class="form-control cc" id="acontact" type="text" placeholder="Area Contact" name="acontact"   maxlength="10" minlength="10" pattern="(.){10,10}" title="Enter 10 digits phone number">
                                 </div>
                                 
                              </div>

                              <div class="form-row">
                                                                  
                                 
                                 <div class="col-lg-4 mb-3"><label for="validationServer01">Sub Block Code <span style="color: red">* <?php $cc = $data['acode']->num_rows()==0 ? "Please Add Block First":""; echo $cc;?></span></label>
                  <select id="bcode" name="bcode" class="form-control" required>
  <option value="" selected disabled>Select Sub Block Code </option>
 <?php if($data['acode']->num_rows()>0) { ?>
        <?php foreach($data['acode']->result() as $row) { ?>
        <option value="<?php echo $row->department_code."##".$row->block_code ?>"><?php echo $row->department_code ?> | <?php echo $row->department_name ?> </option>
        <?php } } ?> 
</select>
                                 
                              </div>

								 <div class="col-lg-4 mb-3"><label for="rtc">Area SOP No <span style="color: red">*</span></label><input class="form-control" id="asop" type="text" placeholder="Area Contact" name="asop" required>
                                 </div>

								  <div class="col-lg-4 mb-3"><label for="rtc">No. Room <span style="color: red">*</span></label><input class="form-control cc" id="aroom" type="text" placeholder="" name="aroom" required>
                                 </div>
								 </div>
								  
								  
								  <div class="form-row">

                                 <div class="col-lg-4 mb-3"><label for="validationServer01">Remarks </label><textarea class="form-control" aria-label="With textarea" name="aremarks" placeholder="Enter Remarks" ></textarea>
                                    
                                 </div>

								 <!--<div class="col-lg-4 mb-3"><label for="validationServer01">Block Code <span style="color: red">* <?php $cc = $data['acode']->num_rows()==0 ? "Please Add Block First":""; echo $cc;?></span></label>
                  <select id="bcode" name="bcode" class="form-control" required>
  <option value="" selected disabled>Select Sub Block Code </option>
 <?php if($data['acode']->num_rows()>0) { ?>
        <?php foreach($data['acode']->result() as $row) { ?>
        <option value="<?php echo $row->department_code."##".$row->block_code ?>"><?php echo $row->department_code ?> | <?php echo $row->department_name ?> </option>
        <?php } } ?> 
</select>
                                 
                              </div>-->

							  

                              
                              
                              
                        </div>
                     </fieldset>
					 </div>
					 </div>
					  <div class="card card-default">
                     
                     <div class="card-body" >
                        
                          
<div class="row disable-button-color">
      <div class="col-sm-12 text-center" id="sbtn">
                                    <?php if ($add) { ?>
                                <button id="sbbtn" class="btn btn-success btn-lg" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
<?php } else { ?>
                                <button class="btn btn-success btn-lg" type="button" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
<?php } ?>
          
      </div>

      </div>

                        
                     </div>
                  </div><!-- END card-->
					 
</form>
      
              </div>
			
			
			
			
			
        </div>
          
<script type="text/javascript">
$(document).ready(function(){
  $('.cc').bind('keyup paste', function(){
        this.value = this.value.replace(/[^0-9]/g, '');
  });

  $("#bcode").change(function(){
    var bcode = $(this).val();
    $.ajax({
        url: '<?= base_url()?>ponta_sahib/Mastercontroller/checkBlocklimit',
        type: 'POST',
        data: {bcode:bcode},
        success: function (res) {
          console.log(res);
          if(res.status==0){
           // $("#validationnow21").attr("disabled",true);
           // $("#validationnow22").attr("disabled",false);
            //alert("The number of areas for this block has reached its maximum value. You cannot create any more areas.");
            $("#sbbtn").attr('disabled', false);
          }
          else{
            $("#sbbtn").attr('disabled', false);
          }
        }
      });
  });

  $("#acode").change(function(){
    var acode = $(this).val();
    $.ajax({
        url: '<?= base_url()?>ponta_sahib/Mastercontroller/checkAreacode',
        type: 'POST',
        data: {acode:acode},
        success: function (res) {
          //console.log(res);
          if(res.status==0){
           // $("#validationnow21").attr("disabled",true);
           // $("#validationnow22").attr("disabled",false);
            alert("Area Code Already Exists");
            $("#acode").val("");
          }
        }
      });
  });

   $('#area').submit(function () {
      $.ajax({
        url: '<?= base_url()?>ponta_sahib/Mastercontroller/mstAreaSubmit',
        type: 'POST',
        data: $(this).serialize(),
        success: function (res) {
          //console.log(res);
          if(res.status==1){
           // $("#validationnow21").attr("disabled",true);
           // $("#validationnow22").attr("disabled",false);
            alert("Area Added successfully");
            window.location.href = "<?= base_url()?>area_view?module_id=<?php echo $_GET['module_id']?>";
          }else{
              alert(res.msg);
          }
        }
      });
      return false;
    });


});  
</script>