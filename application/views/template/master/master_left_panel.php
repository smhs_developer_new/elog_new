<!-- Sidebar -->
<?php
$reportData=$this->db->get_where("pts_mst_report",array("status"=>"active"))->result_array();
$masterData=$this->db->get_where("pts_mst_master",array("status"=>"active"))->result_array();
$url=$_SERVER['REQUEST_URI'];
$end = array_slice(explode('/', $url), -1)[0];
?>

<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">



    <!-- Sidebar - Brand -->

    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url(); ?>User/home">

        <div class="sidebar-brand-text mx-3">SMHS eLogBook</div>

    </a>



    <!-- Divider -->

    <hr class="sidebar-divider">



    <div class="sidebar-heading">

        Main Navigation

    </div>



    <!-- Nav Item - Dashboard -->

    <li class="nav-item">

        <a class="nav-link" href="<?php echo base_url(); ?>User/home">

            <i class="fas fa-home"></i>

            <span>Home</span></a>

    </li>



    <!-- Nav Item - Pages Collapse Menu -->

    <li class="nav-item">

        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">

            <i class="fas fa-fw fa-cog"></i>

            <span>Master</span>

        </a>

        <div id="collapseTwo" class="collapse <?php if($end!='home'){ echo 'show';}?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="max-height: 300px;overflow: auto;">

            <!-- <div class="submenu-bg  py-2 collapse-inner rounded scrollbar" id="style-8"> -->
                <div class="submenu-bg  py-2 collapse-inner rounded">
                 <?php
                if (!empty($masterData)) {
                        foreach ($masterData as $key => $value) {  ?>
                            <a class="collapse-item" href="<?php echo base_url(); ?><?php echo $value['master_url'] ?>?module_id=<?php echo $value['id']?>"><?php echo $value['master_name'] ?></a>
                      <?php  }
                    }
                
                ?>

            </div>

        </div>

    </li>



    <!-- Nav Item - Utilities Collapse Menu -->



    <!-- Nav Item - Pages Collapse Menu -->

    <li class="nav-item">

        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">

            <i class="fas fa-fw fa-wrench"></i>

            <span>Report</span>

        </a>

        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar" style="max-height: 300px;overflow: auto;">

            <!-- <div class="submenu-bg py-2 collapse-inner rounded scrollbar" id="style-8"> -->
                <div class="submenu-bg py-2 collapse-inner rounded">
                <?php
                if (!empty($reportData)) {
                        foreach ($reportData as $key => $value) {  ?>
                            
                            <a class="collapse-item" href="<?php echo base_url(); ?><?php echo $value['report_url'] ?>?module_id=<?php echo $value['id']?>&mst_act_id=<?php echo $value['mst_act_id']?>"><?php echo $value['report_name'] ?></a>
                      <?php  }
                    }
                
                ?>
            </div>

        </div>

    </li>



   <!--  <li class="nav-item">

        <a class="nav-link" href="#">

            <i class="fas fa-home"></i>

            <span>First time Plant Set-up</span></a>

    </li>



    <li class="nav-item">

        <a class="nav-link" href="#">

            <i class="fas fa-home"></i>

            <span>Create Master Data</span></a>

    </li> -->



    <!-- Divider -->

    <hr class="sidebar-divider d-none d-md-block">



    <!-- Sidebar Toggler (Sidebar) -->

    <div class="text-center d-none d-md-inline">

        <button class="rounded-circle border-0" id="sidebarToggle"></button>

    </div>



</ul>

