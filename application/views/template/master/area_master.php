<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="areaApp" ng-controller="areaCtrl" ng-cloak>
    <!-- Page Heading -->
    <div class="content-wrapper">
        <div class="card card-default mt-4">
            <div class="card-body">
                    <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">
                        <table class="table table-dark">
                            <thead>
                                <tr>
                                    <th class="f18" colspan="5" style="vertical-align:middle">Area Master</th>
                                    <th class="text-right f20">
                                        <?php if ($add) { ?>
                                            <button class="btn btn-success" ng-disabled="recordid > 0" ng-click="showForm()">Add (<i class="fas fa-plus"></i>)</button>
                                        <?php } else { ?>
                                            <button class="btn btn-success"  ng-click="showForm()"disabled>Add (+)</button>
                                        <?php } ?>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <form name="materialForm" novalidate="" class="ng-pristine ng-valid ng-valid-required">
                            <div class="card card-default" ng-show="showAddForm">
                                <div class="card-body">
                                    <div class="col-auto">
                                        <fieldset>
                                            <div class="col-auto">
                                                <div class="form-row">
                                                    <div class="col-lg-4 mb-3">
                                                        <label for="aqty">Area Code <span style="color: red">*</span>
                                                        </label>
                                                        <input class="form-control" maxlength="20" id="acode" type="text" placeholder="Area Code" name="acode" ng-model="acode" ng-disabled="recordid > 0" required>
                                                    </div>
                                                    <div class="col-lg-4 mb-3">
                                                        <label for="edso">Area Name <span style="color: red">*</span>
                                                        </label>
                                                        <input class="form-control" maxlength="50" id="aname" type="text" placeholder="Area Name" ng-model="aname" required>
                                                    </div>
                                                    <div class="col-lg-4 mb-3">
                                                        <label for="rtc">No. of Room <span style="color: red">*</span>
                                                        </label>
                                                        <input class="form-control cc" id="aroom" type="text" placeholder="No. of Room" name="aroom" ng-model="aroom" maxlength="2" required>
                                                    </div>

                                                </div>
                                                <div class="form-row">
                                                    <div class="col-lg-4 mb-3">
                                                        <label for="validationServer01">Sub Block Code <span style="color: red">* </span>
                                                        </label>
                                                        <select id="bcode" chosen name="bcode" ng-change="checkBlocklimit()" class="chzn-select form-control" ng-model="bcode" ng-options="(dataObj['department_code']+'##'+dataObj['block_code']) as (dataObj.department_code +        ' | ' +        dataObj.department_name) for dataObj in departmentData" required>
                                                            <option value="">Select Block Code</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4 mb-3">
                                                        <label for="rtc">Area SOP No <!--<span style="color: red">*</span>-->
                                                        </label>
                                                        <input class="form-control" id="asop" type="text" placeholder="Area SOP No" name="asop" maxlength="20" ng-model="asop" >
                                                    </div>

                                                    <div class="col-lg-4 mb-3">
                                                        <label for="rtc">Area contact</label>
                                                        <input class="form-control cc" id="acontact" type="text" placeholder="Area Contact" name="acontact" maxlength="10" minlength="10" pattern="(.){10,10}" title="Enter 10 digits phone number" ng-model="acontact">
                                                    </div>

                                                </div>
                                                <div class="form-row">
                                                    <div class="col-lg-4 mb-3">
                                                        <label for="validationServer01">Remarks<span style="color: red" ng-show="editmodeheader">* </span></label>
                                                        <textarea class="form-control" aria-label="With textarea" name="aremarks" placeholder="Enter Remarks" maxlength="100" ng-model="aremarks" ng-required="editmodeheader"></textarea>
                                                    </div>
                                                    <div class="col-lg-2 mb-3" ng-show="recordid > 0"><label><b>Active/Inactive</b></label>
                                                        <label class="switch">
                                                            <input type="checkbox" class="toggle-task" style="margin-top: 13px;" ng-model="active_status" ng-click="toggleSelection($event)">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                                <div class="row mb-4">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-success btn-sm" ng-show="recordid == ''" ng-disabled="materialForm.$invalid || !checkblock" ng-click="saveArea()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        <button class="btn btn-success btn-sm" ng-show="recordid > 0" ng-disabled="materialForm.$invalid || !checkblock" ng-click="updateArea()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        <button class="btn btn-danger btn-sm" ng-click="hideForm()" type="button">&nbsp;&nbsp;&nbsp;&nbsp; Cancel &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                    <div class="form-row" style="margin-top:10px;" ng-show="areaList.length == 0">
                        <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
                    </div>
                    <div class="form-row" style="margin-top:10px; padding:10px;" ng-show="areaList.length > 0">
                        <h2 class="col-md-12 f18">Area List</h2>
                        <table class="table custom-table">
                            <thead>
                                <tr>
                                    <th>Area Code</th>
                                    <th>Area Name</th>
                                    <th>No. Of Rooms</th>
                                    <th>Block Code</th>
                                    <th>Sub Block Code</th>
                                    <th>Last Modified By</th>
                                    <th>Last Modified On</th>
                                    <th>Action</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr dir-paginate="dataObj in areaList|itemsPerPage:records_per_page" total-items="total_records" style="background:none;" ng-hide="dataObj.block_code == 'Block-000'">
                                    <td>{{dataObj.area_code}}</td>
                                    <td>{{dataObj.area_name}}</td>
                                    <td>{{dataObj.number_of_rooms}}</td>
                                    <td>{{dataObj.block_code}}</td>
                                    <td>{{dataObj.department_code}}</td>
                                    <td>{{dataObj.modified_by!=NULL?dataObj.modified_by:dataObj.created_by}}</td>
                                    <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                    <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                    <td>
                                        <?php if ($edit) { ?>
                                            <a ng-click="editArea(dataObj)"><i class="far fa-edit text-blue pr10" title="Edit"></i></a>
                                        <?php } else { ?>
                                            <a type="button" class="btn btn-sm btn-info" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                        <?php } ?>
                                    </td>
                                    <td ng-if="dataObj.is_active == '1'"   class="bg-success text-white">Active</td>
                                    <td ng-if="dataObj.is_active == '0'"   class="bg-danger text-white">Inactive</td>
                                </tr>
                            </tbody>
                        </table>
                        <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getAreaList(newPageNumber)"></dir-pagination-controls>
                    </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script type="text/javascript">
                                        var app = angular.module("areaApp", ['angular.chosen','angularUtils.directives.dirPagination']);
                                        app.directive('chosen', function ($timeout) {

                                            var linker = function (scope, element, attr) {
                                                scope.$watch('departmentData', function () {
                                                    $timeout(function () {
                                                        element.trigger('chosen:updated');
                                                    }, 0, false);
                                                }, true);
                                                $timeout(function () {
                                                    element.chosen();
                                                }, 0, false);
                                            };
                                            return {
                                                restrict: 'A',
                                                link: linker
                                            };
                                        });

                                        app.directive('onlyDigits', function () {
                                            return {
                                                require: 'ngModel',
                                                restrict: 'A',
                                                link: function (scope, element, attr, ctrl) {
                                                    function inputValue(val) {
                                                        if (val) {
                                                            var digits = val.replace(/[^0-9.]/g, '');

                                                            if (digits.split('.').length > 2) {
                                                                digits = digits.substring(0, digits.length - 1);
                                                            }

                                                            if (digits !== val) {
                                                                ctrl.$setViewValue(digits);
                                                                ctrl.$render();
                                                            }
                                                            return parseFloat(digits);
                                                        }
                                                        return undefined;
                                                    }
                                                    ctrl.$parsers.push(inputValue);
                                                }
                                            };
                                        });
                                        app.filter('format', function () {
                                            return function (item) {
                                                var t = item.split(/[- :]/);
                                                var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                                var time = d.getTime();
                                                return time;
                                            };
                                        });
                                        app.filter('capitalizeWord', function () {

                                            return function (text) {

                                                return (!!text) ? text.charAt(0).toUpperCase() + text.substr(1).toLowerCase() : '';

                                            }

                                        });
                                        app.controller("areaCtrl", function ($scope, $http, $filter) {
                                            $scope.showAddForm = false;
                                            $scope.showForm = function () {
                                                $scope.showAddForm = true;
                                            }
                                            //Get Sub Block List
                                            $scope.departmentData = [];
                                            $scope.getSubBlockList = function () {
                                                $http({
                                                    url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getSubBlockList',
                                                    method: "GET",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    $scope.departmentData = response.data.department_list;
                                                }, function (error) { // optional
                                                    $scope.departmentData = [];
                                                    console.log("Something went wrong.Please try again");

                                                });
                                            }
                                            $scope.getSubBlockList();
                                            //Get Area Master List
                                            $scope.areaList = [];
                                            $scope.page = 1;
                                            $scope.total_records = 0;
                                            $scope.records_per_page = 10;
                                            $scope.getAreaList = function (page) {
                                                $scope.page = page;
                                                $http({
                                                    url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getAreaList?module=master&page='+page,
                                                    method: "GET",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    $scope.areaList = response.data.area_list;
                                                    $scope.total_records = response.data.total_records;
                                                    $scope.records_per_page = response.data.records_per_page;
                                                }, function (error) { // optional
                                                    $scope.areaList = [];
                                                    console.log("Something went wrong.Please try again");

                                                });
                                            }
                                            $scope.getAreaList($scope.page);

                                            $scope.aremarks = "";
                                            $scope.acontact = "";
                                            $scope.hideForm = function () {
                                                $scope.recordid = "";
                                                $scope.editmodeheader = false;
                                                $scope.acode = "";
                                                $scope.aname = "";
                                                $scope.bcode = "";
                                                $scope.acontact = "";
                                                $scope.aremarks = "";
                                                $scope.asop = "";
                                                $scope.aroom = "";
                                                $scope.showAddForm = false;
                                            }
                                            $scope.checkblock = true;
                                            $scope.checkBlocklimit = function () {
                                                $http({
                                                    url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/checkBlocklimit',
                                                    method: "POST",
                                                    data: "bcode=" + $scope.bcode,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    if (response.data.status == false) {
                                                        $scope.checkblock = true;
                                                       // alert("The number of sub blocks for this block has reached its maximum value. You cannot create any more sub block.");

                                                    } else {
                                                        $scope.checkblock = true;
                                                    }
                                                }, function (error) { // optional
                                                });
                                            }

                                            $scope.saveArea = function () {
                                                if (confirm("Do You Want To Save This Record?")) {
                                                    $http({
                                                        url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/mstAreaSubmit',
                                                        method: "POST",
                                                        data: "acode=" + $scope.acode + "&aname=" + $scope.aname + "&acontact=" + $scope.acontact + "&aremarks=" + $scope.aremarks + "&asop=" + $scope.asop + "&aroom=" + $scope.aroom + "&bcode=" + $scope.bcode,
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    })
                                                            .then(function (response) {
                                                                if (response.data.result == true) {
                                                                    alert(response.data.message);
                                                                    $scope.recordid = "";
                                                                    $scope.editmodeheader = false;
                                                                    $scope.acode = "";
                                                                    $scope.aname = "";
                                                                    $scope.bcode = "";
                                                                    $scope.acontact = "";
                                                                    $scope.aremarks = "";
                                                                    $scope.asop = "";
                                                                    $scope.aroom = "";
                                                                    $scope.showAddForm = false;
                                                                    $scope.getAreaList($scope.page);
                                                                } else {
                                                                    alert(response.data.message);
                                                                }
                                                            },
                                                                    function (response) { // optional
                                                                        alert(response.data.message);
                                                                        $scope.recordid = "";
                                                                        $scope.editmodeheader = false;
                                                                        $scope.acode = "";
                                                                        $scope.aname = "";
                                                                        $scope.bcode = "";
                                                                        $scope.acontact = "";
                                                                        $scope.aremarks = "";
                                                                        $scope.asop = "";
                                                                        $scope.aroom = "";
                                                                        $scope.showAddForm = false;
                                                                        $scope.getAreaList($scope.page);
                                                                        console.log("Something went wrong.Please try again");

                                                                    });
                                                }

                                            }
                                            $scope.toggleSelection = function toggleSelection(event) {
                                                if (event.target.checked == true) {
                                                    $scope.active_status = true;
                                                } else {
                                                    $scope.active_status = false;
                                                }
                                            };
                                            $scope.recordid = "";
                                            $scope.editArea = function (dataObj) {
                                                $http({
                                                    url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/checkParentStatus',
                                                    method: "POST",
                                                    data: "field_name=department_code&field_value=" + dataObj.department_code + "&table_name=mst_department",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    if (response.data.parent_data[0].is_active == 0) {
                                                        $scope.showAddForm = false;
                                                        alert("Sub Block is inactive.First activate this Sub Block.");
                                                    } else {
                                                        $scope.showAddForm = true;
                                                        $scope.editmodeheader = true;
                                                        $scope.recordid = dataObj.id;
                                                        $scope.aremarks = "";
                                                        $scope.acode = dataObj.area_code;
                                                        $scope.aname = dataObj.area_name;
                                                        $scope.bcode = dataObj.department_code + '##' + dataObj.block_code;
                                                        $scope.acontact = dataObj.area_contact;
                                                        $scope.asop = dataObj.area_sop_no;
                                                        $scope.aroom = dataObj.number_of_rooms;
                                                        if (dataObj.is_active == '1') {
                                                            $scope.active_status = true;
                                                        } else {
                                                            $scope.active_status = false;
                                                        }
                                                    }
                                                }, function (error) { // optional
                                                });

                                            }
                                            $scope.updateArea = function () {
                                                if (confirm('Do You Want To Update This Record?')) {
                                                    $http({
                                                        url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/mstAreaupdate',
                                                        method: "POST",
                                                        data: "acode=" + $scope.acode + "&aname=" + $scope.aname + "&acontact=" + $scope.acontact + "&aremarks=" + $scope.aremarks + "&asop=" + $scope.asop + "&aroom=" + $scope.aroom + "&bcode=" + $scope.bcode + "&areaid=" + $scope.recordid + "&status=" + $scope.active_status,
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    })
                                                            .then(function (response) {
                                                                if (response.data.result == true) {
                                                                    alert(response.data.message);
                                                                    $scope.recordid = "";
                                                                    $scope.editmodeheader = false;
                                                                    $scope.acode = "";
                                                                    $scope.aname = "";
                                                                    $scope.bcode = "";
                                                                    $scope.acontact = "";
                                                                    $scope.aremarks = "";
                                                                    $scope.asop = "";
                                                                    $scope.aroom = "";
                                                                    $scope.showAddForm = false;
                                                                    $scope.getAreaList($scope.page);
                                                                } else {
                                                                    alert(response.data.message);
                                                                }
                                                            },
                                                                    function (response) { // optional
                                                                        alert(response.data.message);
                                                                        $scope.recordid = "";
                                                                        $scope.editmodeheader = false;
                                                                        $scope.acode = "";
                                                                        $scope.aname = "";
                                                                        $scope.bcode = "";
                                                                        $scope.acontact = "";
                                                                        $scope.aremarks = "";
                                                                        $scope.asop = "";
                                                                        $scope.aroom = "";
                                                                        $scope.showAddForm = false;
                                                                        $scope.getAreaList($scope.page);
                                                                        console.log("Something went wrong.Please try again");

                                                                    });
                                                }

                                            }


                                        });
                                        $(document).ready(function () {
                                            $('.cc').bind('keyup paste', function () {
                                                this.value = this.value.replace(/[^0-9]/g, '');
                                            });

                                        });
</script>