<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="equipApp" ng-controller="equipCtrl" ng-cloak>
    <!-- Page Heading -->
    <div class="content-wrapper">
        <div class="card card-default mt-4">
            <div class="card-body">
                <div class="form-row">
                    <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">
                        <table class="table table-dark">
                            <thead>
                                <tr>
                                    <th colspan="5" style="vertical-align:middle" class="f18">Equipment Master</th>
                                    <th class="text-right">
                                        <?php if ($add) { ?>
                                            <button class="btn btn-success" ng-disabled="recordid > 0" ng-click="showForm()">Add (<i class="fas fa-plus"></i>)</button>
                                        <?php } else { ?>
                                            <button class="btn btn-success"  ng-click="showForm()"disabled>Add (+)</button>
                                        <?php } ?>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <form id="fileUpload" name="fileUpload" novalidate enctype="multipart/form-data" method="post">
                            <!--<form name="materialForm" novalidate="" class="ng-pristine ng-valid ng-valid-required">-->
                            <div class="card card-default" ng-show="showAddForm">
                                <div class="card-body">
                                    <div class="col-auto">

                                        <fieldset>            
                                            <div class="col-auto">
                                                <div class="form-row">   
                                                    <div class="col-lg-3 mb-3"><label for="aqty">Equipment Code <span style="color: red">*</span></label>
                                                        <input ng-disabled="recordid > 0" class="form-control" id="ecode" maxlength="20" type="text" placeholder="Equipment Code" name="ecode" ng-model="ecode" required>
                                                    </div>
                                                    <div class="col-lg-3 mb-3"><label for="edso">Equipment Name <span style="color: red">*</span></label>
                                                        <input class="form-control" id="ename" maxlength="50" type="text" placeholder="Equipment Name" name="ename" ng-model="ename" required>
                                                    </div>
                                                    <div class="col-lg-3 mb-3"><label for="edso">Equipment Type <span style="color: red">*</span></label>
                                                        <select id="etype" ng-change="checkEquipmentType()" name="etype" class="form-control" ng-model="etype" required>
                                                            <option value="" selected="" disabled="">Select Equipment Type </option>
                                                            <option value="Fixed">Fixed</option>
                                                            <option value="Portable">Portable</option>
                                                            <option value="Vacuum">Vacuum Cleaner</option>
                                                            <option value="Booth">Dispensing Booth</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 mb-3"><label for="validationServer01">Select Block Code<span style="color: red">* </span></label>
                                                        <select id="blockcode" chosen name="blockcode" class="chzn-select form-control" ng-model="blockcode" ng-change="getAreaList()" ng-options="dataObj['block_code'] as (dataObj.block_code +                      ' | ' +                      dataObj.block_name) for dataObj in blockData" required>
                                                            <option value="">Select Block Code</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-row" ng-show="etype == 'Fixed' || etype == 'Booth'">
                                                    <div class="col-lg-6 mb-3">
                                                        <label for="validationServer01">Area<span style="color: red">* </span></label>
                                                        <select id="areacode" chosen name="areacode" class="chzn-select form-control" ng-required="etype == 'Fixed' || etype == 'Booth'" ng-model="areacode" ng-change="getRoomList()" ng-options="(dataObj['area_code']) as (dataObj.area_code +                   ' | ' +                   dataObj.area_name) for dataObj in areaList" ng-required="etype == 'Fixed' || etype == 'Booth'">
                                                            <option value="">Select Area Code</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-6 mb-3">
                                                        <label for="validationServer01">Room<span style="color: red">* </span></label>
                                                        <select id="areacode" chosen name="rcode" class="chzn-select form-control" ng-required="etype == 'Fixed' || etype == 'Booth'" ng-model="rcode" ng-options="(dataObj['room_code']) as (dataObj.room_code +                   ' | ' +                   dataObj.room_name) for dataObj in roomList" ng-required="etype == 'Fixed' || etype == 'Booth'">
                                                            <option value="">Select Room Code</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-row">   
                                                    <div class="col-lg-6 mb-3"><label for="validationServer01">Sop No. </label>

                                                        <input class="form-control" type="text" placeholder="Sop No." name="spcode" id="spcode" ng-model="spcode">                      
                                                    </div>
                                                    <div class="col-lg-6 mb-3">
                                                        <label for="edso">Equipment Icon <span style="color: red; font-size: 13px;"> (less than 1MB &amp; png/jpg/gif)</span></label><br>
                                                        <input id="file_id" type="file" (change)="onSelectFile($event)" accept="image/*" name="file" file-upload/>
                                                        <!--<input class="f13" id="eicon" type="file" placeholder="Equipment Type" name="eicon" accept="image/x-png,image/gif,image/jpeg">-->
                                                    </div>
                                                </div>                


                                                <div class="form-row">
                                                    <div class="col-lg-6 mb-3"><label for="validationServer01">Remarks <span style="color: red" ng-show="editmodeheader">* </span></label>
                                                        <textarea class="form-control" aria-label="With textarea" name="eremarks" placeholder="Enter Remarks" maxlength="100" ng-model="eremarks" ng-required="editmodeheader"></textarea>
                                                    </div>
                                                    <div class="col-lg-2 mb-3" ng-show="recordid > 0"><label><b>Active/Inactive</b></label>
                                                        <label class="switch">
                                                            <input type="checkbox" style="margin-top: 13px;" ng-model="active_status" ng-click="toggleSelection($event)">
                                                            <span class="slider round"></span>
                                                        </label>

                                                    </div>

                                                </div>

                                            </div></fieldset>

                                    </div>
                                    <div class="row mb-4">
                                        <div class="col-sm-12 text-center">
                                            <button class="btn btn-success btn-sm" ng-show="recordid == ''" ng-disabled="fileUpload.$invalid" ng-click="saveEquipment()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                            <button class="btn btn-success btn-sm" ng-show="recordid > 0" ng-disabled="fileUpload.$invalid" ng-click="updateEquipment()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                            <button class="btn btn-danger btn-sm" ng-click="hideForm()" type="button">&nbsp;&nbsp;&nbsp;&nbsp; Cancel &nbsp;&nbsp;&nbsp;&nbsp;</button>                                        </div>
                                    </div>

                                </div>
                            </div>
                        </form>
                        <div class="col-md-12">
                            <div class="form-row" style="margin-top:10px;" ng-show="equipmentData.length > 0">
                                <h2 class="col-md-12 f18">Equipment List</h2>
                                <table class="table custom-table">
                                    <thead>
                                        <tr>
                                            <th>Equipment Code</th>
                                            <th>Equipment Name</th>
                                            <th>Equipment Type</th>
                                            <th>Block Code</th>
                                            <th>Room Code</th>
                                            <th>Equipment Icon</th>
                                            <th>Last Modified By</th>
                                            <th>Last Modified On</th>
                                            <th>Action</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr dir-paginate="dataObj in equipmentData|itemsPerPage:records_per_page" total-items="total_records" style="background:none;">
                                            <td>{{dataObj.equipment_code}}</td>
                                            <td>{{dataObj.equipment_name}}</td>
                                            <td>{{dataObj.equipment_type}}</td>
                                            <td>{{dataObj.block_code}}</td>
                                            <td>{{dataObj.room_code}}</td> 
                                            <td><img src="<?php echo base_url()?>img/icons/{{dataObj.equipment_icon}}" style="height: 50px;width: 50px;"></td> 
                                            <td>{{dataObj.modified_by!=NULL?dataObj.modified_by:dataObj.created_by}}</td>
                                            <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                            <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                            <td>
                                                <?php if ($edit) { ?>
                                                    <a ng-click="editEquipment(dataObj)"><i class="far fa-edit text-blue pr10" title="Edit"></i></a>
                                                <?php } else { ?>
                                                    <a type="button" class="btn btn-sm btn-info" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                                <?php } ?> 
                                            </td>
                                            <td ng-if="dataObj.is_active == '1'"   class="bg-success text-white">Active</td>
                                            <td ng-if="dataObj.is_active == '0'"   class="bg-danger text-white">Inactive</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getEquipmentList(newPageNumber)"></dir-pagination-controls>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script type="text/javascript">
                                                var app = angular.module("equipApp", ['angular.chosen','angularUtils.directives.dirPagination']);
                                                app.directive('fileUpload', function () {
                                                    return {
                                                        scope: true, //create a new scope
                                                        link: function (scope, el, attrs) {
                                                            el.bind('change', function (event) {
                                                                var files = event.target.files;
                                                                //iterate files since 'multiple' may be specified on the element
                                                                for (var i = 0; i < files.length; i++) {
                                                                    //emit event upward
                                                                    scope.$emit("fileSelected", {
                                                                        file: files[i]
                                                                    });
                                                                    //               console.log(file);
                                                                }
                                                            });
                                                        }
                                                    };
                                                });
                                                app.directive('chosen', function ($timeout) {

                                                    var linker = function (scope, element, attr) {
                                                        scope.$watch('blockData', function () {
                                                            $timeout(function () {
                                                                element.trigger('chosen:updated');
                                                            }, 0, false);
                                                        }, true);
                                                        scope.$watch('areaList', function () {
                                                            $timeout(function () {
                                                                element.trigger('chosen:updated');
                                                            }, 0, false);
                                                        }, true);
                                                        scope.$watch('roomList', function () {
                                                            $timeout(function () {
                                                                element.trigger('chosen:updated');
                                                            }, 0, false);
                                                        }, true);
                                                        $timeout(function () {
                                                            element.chosen();
                                                        }, 0, false);
                                                    };
                                                    return {
                                                        restrict: 'A',
                                                        link: linker
                                                    };
                                                });

                                                app.directive('onlyDigits', function () {
                                                    return {
                                                        require: 'ngModel',
                                                        restrict: 'A',
                                                        link: function (scope, element, attr, ctrl) {
                                                            function inputValue(val) {
                                                                if (val) {
                                                                    var digits = val.replace(/[^0-9.]/g, '');

                                                                    if (digits.split('.').length > 2) {
                                                                        digits = digits.substring(0, digits.length - 1);
                                                                    }

                                                                    if (digits !== val) {
                                                                        ctrl.$setViewValue(digits);
                                                                        ctrl.$render();
                                                                    }
                                                                    return parseFloat(digits);
                                                                }
                                                                return undefined;
                                                            }
                                                            ctrl.$parsers.push(inputValue);
                                                        }
                                                    };
                                                });
                                                app.filter('format', function () {
                                                    return function (item) {
                                                        var t = item.split(/[- :]/);
                                                        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                                        var time = d.getTime();
                                                        return time;
                                                    };
                                                });
                                                app.filter('capitalizeWord', function () {

                                                    return function (text) {

                                                        return (!!text) ? text.charAt(0).toUpperCase() + text.substr(1).toLowerCase() : '';

                                                    }

                                                });
                                                app.controller("equipCtrl", function ($scope, $http, $filter) {
                                                    $scope.showAddForm = false;
                                                    $scope.showForm = function () {
                                                        $scope.showAddForm = true;
                                                    }
                                                    //Get Block List
                                                    $scope.blockData = [];
                                                    $scope.getBlockList = function () {
                                                        $http({
                                                            url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getBlockList',
                                                            method: "GET",
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        }).then(function (response) {
                                                            $scope.blockData = response.data.block_list;
//                                                            console.log($scope.blockData);
                                                        }, function (error) { // optional
                                                            $scope.blockData = [];
                                                            console.log("Something went wrong.Please try again");

                                                        });
                                                    }
                                                    $scope.getBlockList();
                                                    $scope.checkEquipmentType = function () {
                                                        if ($scope.etype != 'Fixed' || $scope.etype != 'Booth') {
                                                            $scope.areacode = "";
                                                            $scope.rcode = "";
                                                        }
                                                    }
                                                    //Get Area List
                                                    $scope.areaList = [];
                                                    $scope.getAreaList = function () {
                                                        $http({
                                                            url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getAreaList?block_code=' + $scope.blockcode,
                                                            method: "GET",
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        }).then(function (response) {
                                                            $scope.areaList = response.data.area_list;
//                                                            console.log($scope.areaList);
                                                        }, function (error) { // optional
                                                            $scope.areaList = [];
                                                            console.log("Something went wrong.Please try again");

                                                        });
                                                    }

                                                    //Get Room Master List
                                                    $scope.roomList = [];
                                                    $scope.getRoomList = function () {
                                                        $http({
                                                            url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getRoomList?area_code=' + $scope.areacode,
                                                            method: "GET",
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        }).then(function (response) {
                                                            $scope.roomList = response.data.room_list;
//                                                            console.log($scope.roomList);
                                                        }, function (error) { // optional
                                                            $scope.roomList = [];
                                                            console.log("Something went wrong.Please try again");

                                                        });
                                                    }

                                                    //Get Equipment List
                                                    $scope.equipmentData = [];
                                                    $scope.page = 1;
                                                    $scope.total_records = 0;
                                                    $scope.records_per_page = 10;
                                                    $scope.getEquipmentList = function (page) {
                                                        $scope.page = page;
                                                        $http({
                                                            url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getEquipmentList?module=master&page='+page,
                                                            method: "GET",
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        }).then(function (response) {
                                                            $scope.equipmentData = response.data.equipment_list;
                                                            $scope.total_records = response.data.total_records;
                                                            $scope.records_per_page = response.data.records_per_page;
//                                                            console.log($scope.equipmentData);
                                                        }, function (error) { // optional
                                                            $scope.equipmentData = [];
                                                            console.log("Something went wrong.Please try again");

                                                        });
                                                    }
                                                    $scope.getEquipmentList($scope.page);

                                                    $scope.eremarks = "";
                                                    $scope.etype = "";
                                                    $scope.areacode = "";
                                                    $scope.rcode = "";
                                                    $scope.spcode = "";
                                                    $scope.files = [];
                                                    $scope.hideForm = function () {
                                                        $scope.files = [];
                                                        $scope.recordid = "";
                                                        $scope.editmodeheader = false;
                                                        $scope.ecode = "";
                                                        $scope.ename = "";
                                                        $scope.etype = "";
                                                        $scope.blockcode = "";
                                                        $scope.spcode = "";
                                                        $scope.areacode = "";
                                                        $scope.rcode = "";
                                                        $scope.eremarks = "";
                                                        $scope.showAddForm = false;
                                                    }
                                                    $scope.$on("fileSelected", function (event, args) {
                                                        var item = args;
                                                        var full_path = item.file['name'];
                                                        item['file_ext'] = full_path.split(".")[1];

                                                        $scope.files.push(item);

                                                        var reader = new FileReader();

                                                        reader.addEventListener("load", function () {
                                                            $scope.$apply(function () {
                                                                item.src = reader.result;
                                                            });
                                                        }, false);

                                                        if (item.file) {
                                                            reader.readAsDataURL(item.file);
                                                        }
                                                    });

                                                    $scope.saveEquipment = function () {
                                                        if (confirm("Do You Want To Save This Record?")) {
                                                            var myForm = document.getElementById('fileUpload');
                                                            var formData = new FormData(myForm);
                                                            formData.append('ecode', $scope.ecode);
                                                            formData.append('ename', $scope.ename);
                                                            formData.append('blockcode', $scope.blockcode);
                                                            formData.append('spcode', $scope.spcode);
                                                            formData.append('areacode', $scope.areacode);
                                                            formData.append('rcode', $scope.rcode);
                                                            formData.append('eremarks', $scope.eremarks);
                                                            angular.forEach($scope.files, function (value, key) {
                                                                var file = value.file;
                                                                formData.append('file[]', file);
                                                            });
                                                            var request = {
                                                                method: 'POST',
                                                                url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/mstEquipmentSubmit',
                                                                data: formData,
                                                                headers: {
                                                                    'Content-Type': undefined
                                                                }
                                                            };

                                                            // SEND THE FILES.
                                                            $http(request).then(function (response) {
                                                                $("#file_id").val("");
                                                                alert(response.data.message);
                                                                $scope.files = [];
                                                                $scope.recordid = "";
                                                                $scope.editmodeheader = false;
                                                                $scope.ecode = "";
                                                                $scope.ename = "";
                                                                $scope.etype = "";
                                                                $scope.blockcode = "";
                                                                $scope.spcode = "";
                                                                $scope.areacode = "";
                                                                $scope.rcode = "";
                                                                $scope.eremarks = "";
                                                                $scope.showAddForm = false;
                                                                $scope.getEquipmentList($scope.page);
                                                            }, function (error) {
                                                                $("#file_id").val("");
                                                                alert(response.data.message);
                                                                $scope.files = [];
                                                                $scope.recordid = "";
                                                                $scope.editmodeheader = false;
                                                                $scope.ecode = "";
                                                                $scope.ename = "";
                                                                $scope.etype = "";
                                                                $scope.blockcode = "";
                                                                $scope.spcode = "";
                                                                $scope.areacode = "";
                                                                $scope.rcode = "";
                                                                $scope.eremarks = "";
                                                                $scope.showAddForm = false;
                                                                $scope.getEquipmentList($scope.page);
                                                                console.log(error);
                                                            });




                                                        }

                                                    }
                                                    $scope.toggleSelection = function toggleSelection(event) {
                                                        if (event.target.checked == true) {
                                                            $scope.active_status = true;
                                                        } else {
                                                            $scope.active_status = false;
                                                        }
                                                    };
                                                    $scope.recordid = "";
                                                    $scope.editEquipment = function (dataObj) {
                                                        $http({
                                                            url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/checkParentStatus',
                                                            method: "POST",
                                                            data: "field_name=block_code&field_value=" + dataObj.block_code + "&table_name=mst_block",
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        }).then(function (response) {
                                                            if (response.data.parent_data[0].is_active == 0) {
                                                                $scope.showAddForm = false;
                                                                alert("Block is inactive.First activate this Block.");
                                                            } else {
                                                                $scope.showAddForm = true;
                                                                $scope.editmodeheader = true;
                                                                $scope.recordid = dataObj.id;
                                                                $scope.ecode = dataObj.equipment_code;
                                                                $scope.ename = dataObj.equipment_name;
                                                                $scope.etype = dataObj.equipment_type;
                                                                $scope.blockcode = dataObj.block_code;
                                                                $scope.areacode = dataObj.area_code;
                                                                $scope.rcode = dataObj.room_code;
                                                                if ($scope.etype == 'Fixed' || $scope.etype == 'Booth') {
                                                                    $scope.getAreaList();
                                                                    $scope.getRoomList();
                                                                }
                                                                $scope.spcode = dataObj.sop_code;

                                                                $scope.eremarks = "";
                                                                if (dataObj.is_active == '1') {
                                                                    $scope.active_status = true;
                                                                } else {
                                                                    $scope.active_status = false;
                                                                }
                                                            }
                                                        }, function (error) { // optional
                                                        });
                                                    }
                                                    $scope.updateEquipment = function () {
                                                        if (confirm("Do You Want To Update This Record?")) {
                                                            var myForm = document.getElementById('fileUpload');
                                                            var formData = new FormData(myForm);
                                                            formData.append('ecode', $scope.ecode);
                                                            formData.append('ename', $scope.ename);
                                                            formData.append('blockcode', $scope.blockcode);
                                                            formData.append('spcode', $scope.spcode);
                                                            formData.append('areacode', $scope.areacode);
                                                            formData.append('rcode', $scope.rcode);
                                                            formData.append('eremarks', $scope.eremarks);
                                                            formData.append('equipmentid', $scope.recordid);
                                                            formData.append('status', $scope.active_status);
                                                            angular.forEach($scope.files, function (value, key) {
                                                                var file = value.file;
                                                                formData.append('file[]', file);
                                                            });
                                                            var request = {
                                                                method: 'POST',
                                                                url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/mstEquipmentUpdate',
                                                                data: formData,
                                                                headers: {
                                                                    'Content-Type': undefined
                                                                }
                                                            };

                                                            // SEND THE FILES.
                                                            $http(request).then(function (response) {
                                                                $("#file_id").val("");
                                                                alert(response.data.message);
                                                                $scope.files = [];
                                                                $scope.recordid = "";
                                                                $scope.editmodeheader = false;
                                                                $scope.ecode = "";
                                                                $scope.ename = "";
                                                                $scope.etype = "";
                                                                $scope.blockcode = "";
                                                                $scope.spcode = "";
                                                                $scope.areacode = "";
                                                                $scope.rcode = "";
                                                                $scope.eremarks = "";
                                                                $scope.showAddForm = false;
                                                                $scope.getEquipmentList($scope.page);
                                                            }, function (error) {
                                                                $("#file_id").val("");
                                                                alert(response.data.message);
                                                                $scope.files = [];
                                                                $scope.recordid = "";
                                                                $scope.editmodeheader = false;
                                                                $scope.ecode = "";
                                                                $scope.ename = "";
                                                                $scope.etype = "";
                                                                $scope.blockcode = "";
                                                                $scope.spcode = "";
                                                                $scope.areacode = "";
                                                                $scope.rcode = "";
                                                                $scope.eremarks = "";
                                                                $scope.showAddForm = false;
                                                                $scope.getEquipmentList($scope.page);
                                                                console.log(error);
                                                            });

                                                        }

                                                    }


                                                });
                                                $(document).ready(function () {
                                                    $('.cc').bind('keyup paste', function () {
                                                        this.value = this.value.replace(/[^0-9]/g, '');
                                                    });

                                                });
</script>