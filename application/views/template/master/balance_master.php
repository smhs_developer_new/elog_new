<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Bootstrap Admin App">
        <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
        <link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>favicon.ico">
        <title>E-Logbook</title>
        <!-- =============== VENDOR STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/brands.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/regular.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/solid.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/fontawesome.css">
        <!-- SIMPLE LINE ICONS-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/simple-line-icons/css/simple-line-icons.css">
        <!-- ANIMATE.CSS-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/animate.css/animate.css">
        <!-- WHIRL (spinners)-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/whirl/dist/whirl.css">
        <!-- =============== PAGE VENDOR STYLES ===============-->
        <!-- =============== BOOTSTRAP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css" id="bscss">
        <!-- =============== APP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/app.css" id="maincss">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/Ponta_style.css">



        <!-- TAGS INPUT-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.css"><!-- SLIDER CTRL-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-slider/dist/css/bootstrap-slider.css"><!-- CHOSEN-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/chosen-js/chosen.css"><!-- DATETIMEPICKER-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.css"><!-- COLORPICKER-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css"><!-- SELECT2-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/select2/dist/css/select2.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.css"><!-- WYSIWYG-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-wysiwyg/css/style.css"><!-- =============== BOOTSTRAP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />
        <style>
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
        </style>
    </head>

    <body ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
        <div class="wrapper">
            <!-- top navbar-->
            <header class="topnavbar-wrapper"> 
                <!-- START Top Navbar-->
                <nav class="navbar topnavbar"> 
                    <!-- START navbar header-->
                    <div class="navbar-header"><a class="navbar-brand" href="<?php echo base_url(); ?>home">
                            <div class="brand-logo font-weight-bold text-white">SMHS - eLog System</div>
                            <div class="brand-logo-collapsed font-weight-bold text-white">SMHS</div>
                        </a></div>
                    <!-- END navbar header--> 
                    <!-- START Left navbar-->
                    <ul class="navbar-nav mr-auto flex-row">
                        <li class="nav-item"> 
                            <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link d-none d-md-block d-lg-block d-xl-block" href="#" data-trigger-resize="" data-toggle-state="aside-collapsed"><em class="fas fa-bars"></em></a><!-- Button to show/hide the sidebar on mobile. Visible on mobile only.--><a class="nav-link sidebar-toggle d-md-none" href="#" data-toggle-state="aside-toggled" data-no-persist="true"><em class="fas fa-bars"></em></a></li>
                        <!-- START User avatar toggle-->
                        <li class="nav-item d-none d-md-block"> 
                            <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link" id="user-block-toggle" href="#user-block" data-toggle="collapse"><em class="icon-user"></em></a></li>
                        <!-- END User avatar toggle--> 
                    </ul>
                    <!-- END Left navbar--> 
                    <!-- START Right Navbar-->
                    <ul class="navbar-nav flex-row">
                        <!-- Fullscreen (only desktops)-->
                        <li class="nav-item d-none d-md-block"><a class="nav-link" href="<?php echo base_url(); ?>" data-toggle-fullscreen=""><em class="fas fa-expand"></em></a></li>
                        <!-- START Offsidebar button-->
                        <li class="nav-item"><a href="<?php echo base_url(); ?>#" class="nav-link"></a></li>


                        <!-- END Offsidebar menu-->
                    </ul>
                    <!-- END Right Navbar--> 

                </nav>
                <!-- END Top Navbar--> 
            </header>
            <!-- sidebar-->
            <aside class="aside-container"> 
                <!-- START Sidebar (left)-->
                <div class="aside-inner">
                    <nav class="sidebar" data-sidebar-anyclick-close=""> 
                        <!-- START sidebar nav-->
                        <ul class="sidebar-nav">
                            <!-- START user info-->
                            <li class="has-user-block">
                                <div class="collapse" id="user-block">
                                    <div class="item user-block"> 
                                        <!-- User picture-->
                                        <div class="user-block-picture">
                                            <div class="user-block-status"><img class="img-thumbnail rounded-circle" src="<?php echo base_url(); ?>img/user/02.jpg" alt="Avatar" width="60" height="60">
                                                <div class="circle bg-success circle-lg"></div>
                                            </div>
                                        </div>
                                        <!-- Name and Job-->
                                        <div class="user-block-info"><span class="user-block-name">Hello, <?php echo $this->session->userdata('empname') ?></span><span class="user-block-role"><?php if ($this->session->userdata('empname')) { ?><a href="<?php echo base_url() ?>user/logout" >Logout </a><?php } ?></span></div>
                                    </div>
                                </div>
                            </li>
                            <!-- END user info--> 
                            <!-- Iterates over all sidebar items-->
                            <li class="nav-heading text-bold"><span data-localize="sidebar.heading.HEADER">Main Navigation</span>
                            <li>
                                <a href="<?php echo base_url(); ?>User/home" title="Home"><em class="fas fa-home"></em><span data-localize="sidebar.nav.Plant master">Home</span></a>
                            </li>
<!--                            <li>
                                <a href="<?php echo base_url(); ?>manage_material" title="Material master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Material master">Manage Material</span></a>
                            </li>-->
                            <li>
                                <a href="<?php echo base_url(); ?>manage_room" title="Room master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Room master">Manage Room</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>manage_room_activity" title="Room Activity master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Room Activity master">Manage Room Activity</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>balance_master" title="Balance master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Balance master">Manage Balance Data</span></a>
                            </li>
                             <li>
                                <a href="<?php echo base_url(); ?>balance_frequency_master" title="Balance Frequency master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Balance Frequency Master">Balance Frequency Data</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>tablet_tooling_master" title="Tablet Tooling master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Tooling master">Tablet Tooling</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>manage_id_standard_weight" title="Id Standard Weight master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Id Standard Weight master">Manage Id Standard Weight</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>work_assign_asper_rolebase" title="Work Assign Master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Work Assign Master">Work Assign Master</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>workflow" title="Work Flow"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Work Flow">Work Flow</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>line_log_master" title="Line Log Master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Line Log Master">Line Log Master</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>instrument_master" title="Instrument Master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Instrument Master">Instrument Master</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>stage_master" title="Stage Master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Stage Master">Stage Master</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>report_header" title="Report Header Master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Header Master">Header Master</span></a>
                            </li>
                        </ul>
                        <!-- END sidebar nav--> 
                    </nav>
                </div>
                <!-- END Sidebar (left)--> 
            </aside>
            <!-- Main section-->
          <!--<section>-->
            <!-- Page content-->
            <section class="section-container"> 
                <!-- Page content-->
                <div class="content-wrapper">
                    <div class="content-heading executesop-heading">
                        <div class="col-sm-5 pl-0">Balance Details</div>
                    </div>
                    <div class="card card-default">
                        <div class="card-body">                     
                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <button class="btn btn-primary btn-lg" ng-click="showForm()"> Add Balance Data</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <form name="balanceForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-lg-3 mb-3"><label>Balance No <span style="color: red">*</span></label>
                                            <input type="text" maxlength="20" class="form-control" name="balance_no" ng-model="balance_no" required>
                                        </div>
                                        <div class="col-lg-3 mb-3"><label>Capacity <span style="color: red">*</span></label>
                                            <input type="text" class="form-control" name="capacity" only-digits ng-model="capacity" required>
                                        </div>
                                        <div class="col-lg-2 mb-2"><label>UOM(KG/GM)</label>
                                            <select class="form-control" name="UOM" ng-model="UOM">
                                                <option value="KG">KG</option>
                                                <option value="GM">GM</option>
                                            </select>
                                            
                                        </div>
                                        <div class="col-lg-2 mb-2"><label>Least Count <span style="color: red">*</span></label>
                                            <input type="text" class="form-control" name="least_count" only-digits ng-model="least_count" required>
                                        </div>
                                        <div class="col-lg-2 mb-2"><label>Acceptance Limit <span style="color: red">*</span></label>
                                            <input type="text" class="form-control" name="accep_limit" ng-model="accep_limit" required>
                                        </div>
                                        </div>
                                    <div class="form-row" ng-repeat="dataObj in standardData"> 
<!--                                        <div class="col-lg-2 mb-3"><label>Select Frequency<span style="color: red">*</span></label>
                                            <select class="form-control" name="frequency_id" ng-model="dataObj.frequency_id" required>
                                                <option value="">select Frequency</option>
                                                <option value="{{dtaObj.id}}" ng-repeat="dtaObj in frequencyData">{{dtaObj.frequency_name}}</option>
                                            </select>
                                        </div>-->
                                        <div class="col-lg-2 mb-3"><label>Measurement(KG/GM)</label>
                                            <select class="form-control" name="measurement_unit" ng-model="dataObj.measurement_unit">
                                                <option value="KG">KG</option>
                                                <option value="GM">GM</option>
                                            </select>
                                            
                                        </div>
                                        <div class="col-lg-2 mb-3"><label>Standard WT.(KG/GM)<span style="color: red">*</span></label>
                                            <input type="text" class="form-control" name="standard_value" ng-model="dataObj.standard_value" only-digits required>
                                        </div>
                                        
                                        <div class="col-lg-2     mb-3"><label>Upper Limit WT.(KG/GM)<span style="color: red">*</span></label>
                                            <input type="text" class="form-control" name="max_value" ng-model="dataObj.max_value" only-digits required>
                                        </div>
                                        <div class="col-lg-2 mb-3"><label>Lower Limit WT.(KG/GM) <span style="color: red">*</span></label>
                                            <input type="text" class="form-control" name="min_value" ng-model="dataObj.min_value" only-digits required>
                                        </div>
                                        <div class="col-lg-1 mb-2" style="margin-top: 35px;">
                                            <label><span style="color: red"></span></label>
                                            <button class="btn btn-xs btn-danger" ng-if="standardData.length > 1" ng-click="Remove($index)" title="Remove"><span class="glyphicon glyphicon-remove"></span></button><!-- end ngIf: reportField[0].length > 1 -->
                                            &nbsp;
                                            <button class="btn btn-xs btn-success" ng-show="showAddButton($index)" ng-click="Add($index)" title="Add"><span class="glyphicon glyphicon-plus"></span></button>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-sm btn-success" ng-disabled="balanceForm.$invalid"  ng-click="saveRoomBalance()" type="submit">Submit</button>
                                        <button ng-click="resetForm()" class="btn btn-sm btn-danger"  type="reset">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="row" ng-show="balanceCalibrationData.length > 0">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10"><h2>Room Balance Calibration List</h2></div>

                            </div>
                            <div class="row" ng-show="balanceCalibrationData.length == 0">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10"><h2>No Room Balance Calibration Found</h2></div>

                            </div>
                            <table class="table table-dark" ng-show="balanceCalibrationData.length > 0">
                                <thead>
                                    <tr>
                                        <th>Balance No</th>
                                        <th>Capacity</th>
                                        <th>Least Count</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="dataObj in balanceCalibrationData">
                                        <td>{{dataObj.balance_no}}</td>
                                        <td>{{dataObj.capacity}}</td>
                                        <td>{{dataObj.least_count}}</td>
                                        <td><button class="btn btn-sm btn-info" ng-click="editBalanceMaster(dataObj)">Edit</button>
                                        <button class="btn btn-success" ng-click="CommonDelete(dataObj.id)">Delete</button></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div> 
                    </div>
                </div>
            </section>
        </div>
        <!--</section>-->
        <!-- Page footer-->
        <!-- Page footer-->

    </div>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
    <script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>js/angular.min.js"></script>
    <script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
    <script type="text/javascript">
                                            var app = angular.module("materialApp", ['angular.chosen']);
                                            app.directive('chosen', function ($timeout) {

                                                var linker = function (scope, element, attr) {

                                                    scope.$watch('sysRoomData', function () {
                                                        $timeout(function () {
                                                            element.trigger('chosen:updated');
                                                        }, 0, false);
                                                    }, true);
                                                    scope.$watch('activityList', function () {
                                                        $timeout(function () {
                                                            element.trigger('chosen:updated');
                                                        }, 0, false);
                                                    }, true);
                                                    $timeout(function () {
                                                        element.chosen();
                                                    }, 0, false);
                                                };
                                                return {
                                                    restrict: 'A',
                                                    link: linker
                                                };
                                            });
                                            app.directive('onlyDigits', function () {
                                                return {
                                                    require: 'ngModel',
                                                    restrict: 'A',
                                                    link: function (scope, element, attr, ctrl) {
                                                        function inputValue(val) {
                                                            if (val) {
                                                                var digits = val.replace(/[^0-9.]/g, '');

                                                                if (digits.split('.').length > 2) {
                                                                    digits = digits.substring(0, digits.length - 1);
                                                                }

                                                                if (digits !== val) {
                                                                    ctrl.$setViewValue(digits);
                                                                    ctrl.$render();
                                                                }
                                                                return parseFloat(digits);
                                                            }
                                                            return undefined;
                                                        }
                                                        ctrl.$parsers.push(inputValue);
                                                    }
                                                };
                                            });
                                            app.controller("materialCtrl", function ($scope, $http, $filter) {
                                                $scope.sysRoomData = [];
                                                $scope.getDeviceRoomList = function () {
                                                    $http({
                                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist',
                                                        method: "GET",
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    }).then(function (response) {
                                                        $scope.sysRoomData = response.data.room_list;
                                                    }, function (error) { // optional

                                                        console.log("Something went wrong.Please try again");

                                                    });
                                                }
                                                $scope.getDeviceRoomList();

                                                $scope.showAddForm = false;
                                                $scope.showForm = function () {
                                                    $scope.showAddForm = true;
                                                }
                                                $scope.getRoomCode = function (objId) {
                                                    $scope.room_code = $filter('filter')($scope.sysRoomData, {room_id: $scope.room_id})[0].room_code;
                                                }



                                                $scope.balanceCalibrationData = [];
                                                $scope.getBalanceCalibrationList = function () {
                                                    $http({
                                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetBalanceCalibrationList',
                                                        method: "GET",
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    }).then(function (response) {
                                                        $scope.balanceCalibrationData = response.data.balance_calibration_list;
                                                        console.log($scope.balanceCalibrationData);
                                                    }, function (error) { // optional

                                                        console.log("Something went wrong.Please try again");

                                                    });
                                                }
                                                $scope.getBalanceCalibrationList();
                                                $scope.standardData = [{"measurement_unit":"KG","standard_value": "", "max_value": "", "min_value": ""}];
                                                $scope.Add = function (index) {
                                                    const obj = {};
                                                    for (const key of Object.keys($scope.standardData[index])) {
                                                        obj[key] = null;
                                                    }
                                                    $scope.standardData.push(obj);

                                                };
                                                $scope.Remove = function (index) {
                                                    //Remove the item from Array using Index.
                                                    $scope.standardData.splice(index, 1);
                                                }
                                                $scope.showAddButton = function (index) {
                                                    return index === $scope.standardData.length - 1;
                                                };
                                                $scope.frequencyData = [];
                                                $scope.getFrequencyList = function () {
                                                    $http({
                                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetFrequencyList',
                                                        method: "GET",
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    }).then(function (response) {
                                                        $scope.frequencyData = response.data.frequency_list;
                                                    }, function (error) { // optional

                                                        console.log("Something went wrong.Please try again");

                                                    });
                                                }
                                                $scope.getFrequencyList();

                                                $scope.saveRoomBalance = function () {
                                                    if (confirm("Do You Want To Save This Room Balance Activity?")) {
                                                        $http({
                                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/AddRoomBalanceCalibration',
                                                            method: "POST",
                                                            $scope.capacity = $scope.capacity + $scope.UOM;
                                                        
                                                            data: "capacity=" + $scope.capacity + "&least_count=" + $scope.least_count + "&balance_no=" + $scope.balance_no + "&accep_limit=" + $scope.accep_limit + "&standard_data=" + encodeURIComponent(angular.toJson($scope.standardData)) + "&balance_id=" + $scope.balance_id,
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        })
                                                        .then(function (response) {
                                                            alert(response.data.message);
                                                            $scope.capacity = '';
                                                            $scope.least_count = '';
                                                            $scope.balance_no = '';
                                                            $scope.balance_id = "";
                                                            $scope.accep_limit = "";
                                                            $scope.standardData = [{"measurement_unit":"KG","standard_value": "", "max_value": "", "min_value": ""}];
                                                                $scope.showAddForm = false;
                                                                $scope.getBalanceCalibrationList();
                                                            },
                                                                function (response) { // optional
                                                                    $scope.showAddForm = false;
                                                                    $scope.balance_id = "";
                                                                    $scope.getBalanceCalibrationList();
                                                                    console.log("Something went wrong.Please try again");

                                                                });
                                                    }
                                                }
                                                $scope.balance_id = "";
                                                $scope.editBalanceMaster = function (dataObj) {
                                                    console.log(dataObj);
                                                    $scope.balance_id = dataObj.id;
                                                    $scope.balance_no = dataObj.balance_no;
                                                    $scope.capacity = dataObj.capacity;
                                                    $scope.least_count = dataObj.least_count;
                                                    if (dataObj.standard_data.length > 0) {
                                                        $scope.standardData = dataObj.standard_data;
                                                    }
                                                    $scope.showAddForm = true;
                                                }

                                                $scope.resetForm = function () {
                                                    $scope.standardData = [{"measurement_unit":"KG","standard_value": "", "max_value": "", "min_value": ""}];
                                                }

                                                //Created By : Bhupendra Kumar
                                                //Date : 18 March 2020
                                                //Decription : For Deactivate the Selected Record
                                                $scope.CommonDelete = function (id) {
                                                    var tblname = "pts_mst_balance_calibration";
                                                    var colname = "id";
                                                    if (confirm("Do You Want To Deactivate This Record ?")) {
                                                    $http({
                                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/CommonDelete',
                                                        method: "POST",
                                                        data: "id=" + id + "&tblname=" + tblname + "&colname=" + colname,
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    }).then(function (response) {
                                                        alert(response.data.message);
                                                        $scope.getBalanceCalibrationList();
                                                    }, function (error) { // optional
                                                        alert(response.data.message);
                                                        //console.log("Something went wrong.Please try again");
                                                    });
                                                }
                                                }
                                            });
    </script>
</body>
</html>