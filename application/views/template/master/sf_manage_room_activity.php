<style>
    [ng\:cloak],
    [ng-cloak],
    [data-ng-cloak],
    [x-ng-cloak],
    .ng-cloak,
    .x-ng-cloak {
        display: none !important;
    }
</style>
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
    <!-- Page Heading -->

    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row">

                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th colspan="5" class="f18" style="vertical-align:middle">Room Log Mapping</th>
                                <th class="text-right">
                                    <?php if ($add) { ?>
                                        <button class="btn btn-success" ng-disabled="recordid > 0" ng-click="showForm()">Add (+)</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success" ng-click="showForm()" disabled>Add (+)</button>
                                    <?php } ?>

                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="col-12">
                    <form name="materialForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-lg-4 mb-3"><label>Room Name <span style="color: red">*</span></label>
                                            <select ng-disabled="recordid > 0" class="form-control" tabindex="4" name="room_id" data-placeholder="Search Room Code" ng-options="dataObj['room_id'] as (dataObj.room_code +       ' => ' +       dataObj.room_name +       ' => ' +       dataObj.area_id) for dataObj in roomIds" ng-model="room_id" required chosen>
                                                <option value="" readonly>Select Room Name</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 mb-3"><label>Log Name <span style="color: red">*</span></label>
                                            <select multiple class="chzn-select form-control" tabindex="4" data-placeholder="Search Activity" ng-change="getActivityInfo(activity_id)" name="activity_id" ng-model="activity_id" chosen>
                                                <option ng-repeat="dataObj in activityList" value="{{dataObj.id}}">{{dataObj.activity_name}}</option>

                                            </select>

                                            <!-- <select class="chzn-select form-control" tabindex="4" multiple ng-change="getActivityInfo(activity_id)"  name="activity_id" data-placeholder="Search Activity" ng-options="dataObj['id'] as dataObj.activity_name for dataObj in activityList"  ng-model="activity_id" required chosen>
                                                <option value="">Select Activity</option>
                                            </select> -->
                                        </div>

                                    </div>


                                </div>
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-lg-4 mb-3"><label><b>Remark</b><span ng-show="recordid > 0" style="color: red">*</span></label>
                                            <textarea class="form-control" name="remark" maxlength="150" ng-model="remark" ng-required="recordid > 0"></textarea>
                                        </div>
                                        <div class="col-lg-2 mb-3" ng-show="recordid > 0"><label><b>Active/Inactive</b></label>
                                            <label class="switch">
                                                <input type="checkbox" style="margin-top: 13px;" ng-model="active_status" ng-click="toggleSelection($event)">
                                                <span class="slider round"></span>
                                            </label>
                                        </div>

                                    </div>


                                </div>
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-sm-12 text-center">
                                            <button class="btn btn-success btn-sm" ng-disabled="materialForm.$invalid" ng-click="saveRoomActivities()" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                            <button ng-click="hideForm()" class="btn btn-danger btn-sm" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Cancel &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="form-row" ng-show="roomActivityData.length == 0">
                <div class="col-lg-12 text-center">
                    <h2>No Data Found</h2>
                </div>
                <div class="col-lg-12 text-center">
                    <h4>(Please Select the above parameters)</h4>
                </div>
            </div>
            <div class="form-row" style="margin-top:10px;" ng-show="roomActivityData.length > 0">
                <h2 class="f18">Room Activity Mapping List</h2>
                <div class="table-responsive noscroll">
                    <table class="table custom-table">
                        <thead>
                            <tr>
                                <th>Room Name</th>
                                <th>Log Name</th>
                                <th>Last Modified By</th>
                                <th>Last Modified On</th>
                                <th>Action</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr dir-paginate="dataObj in roomActivityData|itemsPerPage:records_per_page" total-items="total_records" style="background:none;">
                                <td>{{dataObj.room_name}} ({{dataObj.room_code}})</td>
                                <td>{{dataObj.logs}}</td>
                                <td>{{dataObj.modified_by!=null?dataObj.modified_by:dataObj.created_by}}</td>
                                <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on * 1000| date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td>
                                    <?php if ($edit) { ?>
                                        <a type="button" ng-click="editRoomActivityMaster(dataObj)"><i class="far fa-edit text-blue pr10"></i></a>
                                        <!--<button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.room_id)">Deactivate</button>-->
                                    <?php } else { ?>

                                        <a type="button" class="btn btn-sm btn-info" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                        <!--<button class="btn btn-sm btn-info" ng-click="editRoomActivityMaster(dataObj)" disabled>Edit</button>-->
                                        <!--<button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.room_id)" disabled>Deactivate</button>-->
                                    <?php } ?>
                                </td>
                                <td ng-if="dataObj.status == 'active'" class="bg-success text-white">{{dataObj.status| capitalizeWord}}</td>
                                <td ng-if="dataObj.status == 'inactive'" class="bg-danger text-white">{{dataObj.status| capitalizeWord}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getRoomActivityList(newPageNumber)"></dir-pagination-controls>
                </div>

            </div>
        </div>
    </div>

</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script>

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script type="text/javascript">
                                    var app = angular.module("materialApp", ['angular.chosen', 'angularUtils.directives.dirPagination']);
                                    app.directive('chosen', function ($timeout) {

                                        var linker = function (scope, element, attr) {

                                            scope.$watch('sysRoomData', function () {
                                                $timeout(function () {
                                                    element.trigger('chosen:updated');
                                                }, 0, false);
                                            }, true);
                                            scope.$watch('activityList', function () {
                                                $timeout(function () {
                                                    element.trigger('chosen:updated');
                                                }, 0, false);
                                            }, true);
                                            $timeout(function () {
                                                element.chosen();
                                            }, 0, false);
                                        };
                                        return {
                                            restrict: 'A',
                                            link: linker
                                        };
                                    });
                                    app.filter('capitalizeWord', function () {

                                        return function (text) {

                                            return (!!text) ? text.charAt(0).toUpperCase() + text.substr(1).toLowerCase() : '';

                                        }

                                    });
                                    app.filter('format', function () {
                                        return function (item) {
                                            var t = item.split(/[- :]/);
                                            var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                            var time = d.getTime();
                                            return time;
                                        };
                                    });
                                    app.controller("materialCtrl", function ($scope, $http, $filter) {
                                        $scope.sysRoomData = [];
                                        $scope.getDeviceRoomList = function () {
                                            $scope.SysRoomDataId = [];
                                            $scope.roomIds = [];
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist',
                                                method: "GET",
                                                headers: {
                                                    'Content-Type': 'application/x-www-form-urlencoded'
                                                },
                                            }).then(function (response) {
                                                $scope.sysRoomData = response.data.room_list;
                                                $scope.roomIds = response.data.room_list;
                                                angular.forEach($scope.sysRoomData, function (value, key) {
                                                    $scope.SysRoomDataId.push(value['room_id']);
                                                });
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.getDeviceRoomList();
                                        // Get Mst Activity List
                                        $scope.activityList = [];
                                        $scope.getMstActivityList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetMstActivityList',
                                                method: "GET",
                                                headers: {
                                                    'Content-Type': 'application/x-www-form-urlencoded'
                                                },
                                            }).then(function (response) {
                                                $scope.activityList = response.data.mst_act_list;
                                                //console.log($scope.activityList);
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.getMstActivityList();
                                        $scope.showAddForm = false;
                                        $scope.showForm = function () {
                                            $scope.showAddForm = true;
                                            angular.forEach($scope.roomActivityData, function (value, key) {
                                                var pos = $.inArray((value['room_id']).toString(), $scope.SysRoomDataId);
                                                if (pos != -1) {
                                                    $scope.roomIds.splice(pos, 1);
                                                    $scope.SysRoomDataId.splice(pos, 1);
                                                }
                                            });
                                        }
                                        $scope.recordid = "";
                                        $scope.remark = "";
                                        $scope.hideForm = function () {
                                            $scope.remark = "";
                                            $scope.status = "";
                                            $scope.recordid = "";
                                            $scope.activity_name = '';
                                            $scope.room_id = '';
                                            $scope.activity_id = "";
                                            $scope.showAddForm = false;
                                        }

                                        $scope.activity_id = [];
                                        $scope.roomactivity = [];
                                        $scope.getActivityInfo = function (objId) {
                                            $scope.roomactivity = [];
                                            // $scope.acid=objId[objId.length-1];
                                            // $scope.activity_name = $filter('filter')($scope.activityList, {id: objId})[0].activity_name;
                                            // $scope.activity_url = $filter('filter')($scope.activityList, {id: objId})[0].activity_url;
                                            angular.forEach(objId, function (value, key) {
                                                $scope.activity_name = $filter('filter')($scope.activityList, {
                                                    id: value
                                                })[0].activity_name;
                                                $scope.activity_url = $filter('filter')($scope.activityList, {
                                                    id: value
                                                })[0].activity_url;
                                                $scope.roomactivity.push({
                                                    "activity_name": $scope.activity_name,
                                                    "activity_url": $scope.activity_url,
                                                    "mst_act_id": value,
                                                    "room_id": $scope.room_id
                                                });
                                            });
                                            //console.log($scope.roomactivity);
                                        }

                                        $scope.roomActivityData = [];
                                        $scope.page = 1;
                                        $scope.total_records = 0;
                                        $scope.records_per_page = 10;
                                        $scope.getRoomActivityList = function (page) {
                                            $scope.page = page;
                                            $scope.roomActivityData = [];
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getactivitylistforroomwithgroupby?module=master&page=' + page,
                                                method: "GET",
                                                headers: {
                                                    'Content-Type': 'application/x-www-form-urlencoded'
                                                },
                                            }).then(function (response) {
                                                $scope.total_records = response.data.total_records;
                                                $scope.records_per_page = response.data.records_per_page;
                                                angular.forEach(response.data.activity_list, function (value, key) {
                                                    $scope.getAllLogs(value["room_id"]);
                                                });
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.getRoomActivityList($scope.page);
                                        $scope.saveRoomActivity = function () {
                                            if (confirm('Do You Want To Save This Record?')) {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Addroomactivity',
                                                    method: "POST",
                                                    data: "activity_name=" + $scope.activity_name + "&activity_url=" + $scope.activity_url + "&room_id=" + $scope.room_id + "&mst_act_id=" + $scope.activity_id + "&recordid=" + $scope.recordid,
                                                    headers: {
                                                        'Content-Type': 'application/x-www-form-urlencoded'
                                                    },
                                                })
                                                        .then(function (response) {
                                                            alert(response.data.message);
                                                            $scope.activity_name = '';
                                                            $scope.room_id = '';
                                                            $scope.activity_id = "";
                                                            $scope.recordid = "";
                                                            $scope.showAddForm = false;
                                                            $scope.getRoomActivityList($scope.page);
                                                        },
                                                                function (response) { // optional
                                                                    $scope.showAddForm = false;
                                                                    $scope.getRoomActivityList($scope.page);
                                                                    console.log("Something went wrong.Please try again");
                                                                });
                                            }
                                        }

                                        $scope.saveRoomActivities = function () {
                                            var msg = 'Save';
                                            if ($scope.recordid > 0) {
                                                var msg = 'Update';
                                            }

                                            if (confirm("Do You Want To " + msg + " This Record?")) {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Saveroomactivities',
                                                    method: "POST",
                                                    data: "Roomactivities=" + encodeURIComponent(angular.toJson($scope.roomactivity)) + "&recordid=" + $scope.recordid + "&status=" + $scope.active_status + "&remark=" + $scope.remark,
                                                    headers: {
                                                        'Content-Type': 'application/x-www-form-urlencoded'
                                                    },
                                                })
                                                        .then(function (response) {
                                                            alert(response.data.message);
                                                            $scope.activity_name = '';
                                                            $scope.room_id = '';
                                                            $scope.recordid = "";
                                                            $scope.activity_id = "";
                                                            $scope.remark = "";
                                                            $scope.showAddForm = false;
                                                            $scope.getRoomActivityList($scope.page);
                                                        },
                                                                function (response) { // optional
                                                                    $scope.activity_name = '';
                                                                    $scope.room_id = '';
                                                                    $scope.recordid = "";
                                                                    $scope.activity_id = "";
                                                                    $scope.remark = "";
                                                                    $scope.showAddForm = false;
                                                                    $scope.getRoomActivityList($scope.page);
                                                                    console.log("Something went wrong.Please try again");
                                                                });
                                            }
                                        }
                                        $scope.getRoomName = function (objId) {
                                            $scope.room_name = $filter('filter')($scope.sysRoomData, {
                                                room_id: objId
                                            })[0].room_name;

                                            return $scope.room_name;
                                        }
                                        $scope.getRoomCode = function (objId) {
                                            $scope.room_code = $filter('filter')($scope.sysRoomData, {
                                                room_id: objId
                                            })[0].room_code;
                                            return $scope.room_code;
                                        }
                                        $scope.getAllLogs = function (objId) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getactivitylistofselectedroom?module=master',
                                                method: "POST",
                                                data: "room_id=" + objId,
                                                headers: {
                                                    'Content-Type': 'application/x-www-form-urlencoded'
                                                },
                                            }).then(function (response) {
                                                $scope.logs = "";
                                                $scope.urls = "";
                                                $scope.logid = "";

                                                angular.forEach(response.data.log_list, function (value, key) {
                                                    if (key == 0) {
                                                        $scope.logs = value["activity_name"];
                                                        $scope.urls = value["activity_url"];
                                                        $scope.logid = value["mst_act_id"];
                                                        $scope.staus = value["status"];
                                                        $scope.created_by = value["created_by"];
                                                        $scope.created_on = value["created_date"];
                                                        $scope.modified_on = value["modified_on"];
                                                        $scope.modified_by = value["modified_by"];
                                                    } else {
                                                        $scope.logs = $scope.logs + " , " + value["activity_name"];
                                                        $scope.urls = $scope.urls + " , " + value["activity_url"];
                                                        $scope.logid = $scope.logid + "," + value["mst_act_id"];
                                                    }
                                                });

                                                var room_name = $scope.getRoomName(objId);
                                                var room_code = $scope.getRoomCode(objId);
                                                $scope.roomActivityData.push({
                                                    "room_id": objId,
                                                    "room_name": room_name,
                                                    "room_code": room_code,
                                                    "logs": $scope.logs,
                                                    "urls": $scope.urls,
                                                    "logid": $scope.logid,
                                                    "status": $scope.staus,
                                                    "created_by": $scope.created_by,
                                                    "created_on": $scope.created_on,
                                                    "modified_on": $scope.modified_on,
                                                    "modified_by": $scope.modified_by
                                                });
                                                var pos = $.inArray((objId).toString(), $scope.SysRoomDataId);
                                                if (pos != -1) {
                                                    $scope.roomIds.splice(pos, 1);
                                                    $scope.SysRoomDataId.splice(pos, 1);
                                                }
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");
                                            });
                                        }

                                        //Created By : Bhupendra Kumar
                                        //Date : 18 March 2020
                                        //Decription : For Deactivate the Selected Record
                                        $scope.CommonDelete = function (id) {
                                            var tblname = "pts_trn_room_log_activity";
                                            var colname = "room_id";
                                            if (confirm("Do You Want To Deactivate This Record ?")) {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/CommonDelete',
                                                    method: "POST",
                                                    data: "id=" + id + "&tblname=" + tblname + "&colname=" + colname,
                                                    headers: {
                                                        'Content-Type': 'application/x-www-form-urlencoded'
                                                    },
                                                }).then(function (response) {
                                                    alert(response.data.message);
                                                    $scope.getRoomActivityList($scope.page);
                                                }, function (error) { // optional
                                                    alert(response.data.message);
                                                    //console.log("Something went wrong.Please try again");
                                                });
                                            }
                                        }

                                        //Created By : Bhupendra Kumar
                                        //Date : 18 March 2020
                                        //Decription : To get values of selected record
                                        $scope.recordid = "";
                                        $scope.status = "";
                                        $scope.active_status = false;
                                        $scope.remark = "";
                                        $scope.toggleSelection = function toggleSelection(event) {
                                            if (event.target.checked == true) {
                                                $scope.active_status = true;
                                            } else {
                                                $scope.active_status = false;
                                            }
                                        };
                                       $scope.editRoomActivityMaster = function (dataObj) {
                                            $scope.getDeviceRoomList();
                                            $scope.SysRoomDataId = [];
                                            $scope.roomIds = [];
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist',
                                                method: "GET",
                                                headers: {
                                                    'Content-Type': 'application/x-www-form-urlencoded'
                                                },
                                            }).then(function (response) {
                                                $scope.sysRoomData = response.data.room_list;
                                                $scope.roomIds = response.data.room_list;
                                                angular.forEach($scope.sysRoomData, function (value, key) {
                                                    $scope.SysRoomDataId.push(value['room_id']);
                                                });
                                                $scope.remark = "";
                                                $scope.roomactivity = [];
                                                $scope.activity_id = [];
                                                $scope.recordid = dataObj.room_id;
                                                $scope.room_id = dataObj.room_id;
                                                //$scope.getRoomCode(dataObj.room_id);
                                                // $scope.getRoomName(dataObj.room_id);
                                                $scope.activity_id = dataObj.logid.split(",");
                                                var Logs = dataObj.logs.split(",");
                                                var Urls = dataObj.urls.split(",");
                                                angular.forEach($scope.activity_id, function (value, key) {
                                                    $scope.roomactivity.push({
                                                        "activity_name": Logs[key],
                                                        "activity_url": Urls[key],
                                                        "mst_act_id": value,
                                                        "room_id": $scope.room_id
                                                    });
                                                });
                                                $scope.showAddForm = true;
                                                if (dataObj.status == 'active') {
                                                    $scope.active_status = true;
                                                } else {
                                                    $scope.active_status = false;
                                                }
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");
                                            });


                                        }

                                    });
</script>