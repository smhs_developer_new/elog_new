<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="roomApp" ng-controller="roomCtrl" ng-cloak>
    <!-- Page Heading -->
    <div class="content-wrapper">
        <div class="card card-default mt-4">
            <div class="card-body">
                <div class="form-row">
                    <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">
                        <table class="table table-dark">
                            <thead>
                                <tr>
                                    <th colspan="5" class="f18" style="vertical-align:middle">Room Master</th>
                                    <th class="text-right">
                                        <?php if ($add) { ?>
                                            <button class="btn btn-success" ng-disabled="recordid > 0" ng-click="showForm()">Add (<i class="fas fa-plus"></i>)</button>
                                        <?php } else { ?>
                                            <button class="btn btn-success"  ng-click="showForm()"disabled>Add (+)</button>
                                        <?php } ?>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="col-md-12">
                        <form name="materialForm" novalidate="" class="ng-pristine ng-valid ng-valid-required">
                            <div class="card card-default" ng-show="showAddForm">
                                <div class="card-body">
                                    <div class="col-auto">
                                        <form id="area">
                                            <fieldset>
                                                <div class="form-row">  
                                                    <div class="col-lg-6 mb-3"><label for="aqty">Room Code <span style="color: red">*</span></label><input class="form-control" ng-disabled="recordid>0" maxlength="20" id="rcode" type="text" placeholder="Room Code" name="rcode" ng-model="rcode" required>
                                                    </div>
                                                    <div class="col-lg-6 mb-3"><label for="edso">Room Name <span style="color: red">*</span></label><input class="form-control" id="rname" maxlength="50" type="text" placeholder="Room Name" name="rname" ng-model="rname" required>
                                                    </div>                                              
                                                </div>

                                                <div class="form-row">

                                                    <div class="col-lg-5 mb-3">
                                                        <label for="validationServer01">Area Code <span style="color: red">* </span></label>
                                                        <select id="acode" chosen name="acode" ng-change="checkArealimit()" class="chzn-select form-control" ng-model="acode" ng-options="(dataObj['area_code']) as (dataObj.area_code +             ' | ' +             dataObj.area_name) for dataObj in areaList" required>
                                                            <option value="">Select Area Code</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-lg-5 mb-3"><label for="validationServer01">Remarks <span style="color: red" ng-show="editmodeheader">* </span></label>
                                                        <textarea class="form-control" aria-label="With textarea" maxlength="100" name="rremarks" placeholder="Enter Remarks" ng-model="rremarks" ng-required="editmodeheader"></textarea>

                                                    </div>
                                                    <div class="col-lg-2 mb-3" ng-show="recordid > 0"><label>Active/Inactive</label>
                                                        <label class="switch">
                                                            <input type="checkbox" class="toggle-task" style="margin-top: 13px;" ng-model="active_status" ng-click="toggleSelection($event)">
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </fieldset>
                                    </div>
                                    </form>
                                </div>
                                <div class="row mb-4">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-success btn-sm" ng-show="recordid == ''" ng-disabled="materialForm.$invalid || !checkblock" ng-click="saveRoom()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        <button class="btn btn-success btn-sm" ng-show="recordid > 0" ng-disabled="materialForm.$invalid || !checkblock" ng-click="updateRoom()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        <button class="btn btn-danger btn-sm" ng-click="hideForm()" type="button">&nbsp;&nbsp;&nbsp;&nbsp; Cancel &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-row" style="margin-top:10px; padding:5px;" ng-show="roomList.length > 0">
                        <h2 class="col-md-12 f18">Room List</h2>
                        <table class="table custom-table">
                            <thead>
                                <tr>
                                    <th>Room Code</th>
                                    <th>Room Name</th>
                                    <th>Area Code</th>
                                    <th>Last Modified By</th>
                                    <th>Last Modified On</th>
                                    <th>Action</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr dir-paginate="dataObj in roomList|itemsPerPage:records_per_page" total-items="total_records" style="background:none;">
                                    <td>{{dataObj.room_code}}</td>
                                    <td>{{dataObj.room_name}}</td>
                                    <td>{{dataObj.area_code}}</td>
                                    <td>{{dataObj.modified_by!=NULL?dataObj.modified_by:dataObj.created_by}}</td>
                                    <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                    <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                    <td>
                                        <?php if ($edit) { ?>
                                            <a ng-click="editRoomMaster(dataObj)"><i class="far fa-edit text-blue pr10" title="Edit"></i></a>
                                        <?php } else { ?>
                                            <a type="button" class="btn btn-sm btn-info" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                        <?php } ?>
                                    </td>
                                    <td ng-if="dataObj.is_active == '1'"   class="bg-success text-white">Active</td>
                                    <td ng-if="dataObj.is_active == '0'"   class="bg-danger text-white">Inactive</td>
                                </tr>
                            </tbody>
                        </table>
                        <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="geRoomList(newPageNumber)"></dir-pagination-controls>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script type="text/javascript">
                                        var app = angular.module("roomApp", ['angular.chosen','angularUtils.directives.dirPagination']);
                                        app.directive('chosen', function ($timeout) {

                                            var linker = function (scope, element, attr) {
                                                scope.$watch('areaList', function () {
                                                    $timeout(function () {
                                                        element.trigger('chosen:updated');
                                                    }, 0, false);
                                                }, true);
                                                $timeout(function () {
                                                    element.chosen();
                                                }, 0, false);
                                            };
                                            return {
                                                restrict: 'A',
                                                link: linker
                                            };
                                        });

                                        app.directive('onlyDigits', function () {
                                            return {
                                                require: 'ngModel',
                                                restrict: 'A',
                                                link: function (scope, element, attr, ctrl) {
                                                    function inputValue(val) {
                                                        if (val) {
                                                            var digits = val.replace(/[^0-9.]/g, '');

                                                            if (digits.split('.').length > 2) {
                                                                digits = digits.substring(0, digits.length - 1);
                                                            }

                                                            if (digits !== val) {
                                                                ctrl.$setViewValue(digits);
                                                                ctrl.$render();
                                                            }
                                                            return parseFloat(digits);
                                                        }
                                                        return undefined;
                                                    }
                                                    ctrl.$parsers.push(inputValue);
                                                }
                                            };
                                        });
                                        app.filter('format', function () {
                                            return function (item) {
                                                var t = item.split(/[- :]/);
                                                var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                                var time = d.getTime();
                                                return time;
                                            };
                                        });
                                        app.filter('capitalizeWord', function () {

                                            return function (text) {

                                                return (!!text) ? text.charAt(0).toUpperCase() + text.substr(1).toLowerCase() : '';

                                            }

                                        });
                                        app.controller("roomCtrl", function ($scope, $http, $filter) {
                                            $scope.showAddForm = false;
                                            $scope.showForm = function () {
                                                $scope.showAddForm = true;
                                            }
                                            //Get Area Master List
                                            $scope.areaList = [];
                                            $scope.getAreaList = function () {
                                                $http({
                                                    url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getAreaList',
                                                    method: "GET",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    $scope.areaList = response.data.area_list;
                                                    console.log($scope.areaList);
                                                }, function (error) { // optional
                                                    $scope.areaList = [];
                                                    console.log("Something went wrong.Please try again");

                                                });
                                            }
                                            $scope.getAreaList();
                                            //Get Room Master List
                                            $scope.roomList = [];
                                            $scope.page = 1;
                                            $scope.total_records = 0;
                                            $scope.records_per_page = 10;
                                            $scope.geRoomList = function (page) {
                                                $scope.page = page;
                                                $http({
                                                    url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getRoomList?module=master&page='+page,
                                                    method: "GET",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    $scope.roomList = response.data.room_list;
                                                    $scope.total_records = response.data.total_records;
                                                    $scope.records_per_page = response.data.records_per_page;
                                                    console.log($scope.roomList);
                                                }, function (error) { // optional
                                                    $scope.roomList = [];
                                                    console.log("Something went wrong.Please try again");

                                                });
                                            }
                                            $scope.geRoomList($scope.page);

                                            $scope.rremarks = "";
                                            $scope.hideForm = function () {
                                                $scope.recordid = "";
                                                $scope.editmodeheader = false;
                                                $scope.rcode = "";
                                                $scope.rname = "";
                                                $scope.acode = "";
                                                $scope.rremarks = "";
                                                $scope.showAddForm = false;
                                            }
                                            $scope.checkblock = true;
                                            $scope.checkArealimit = function () {
                                                $http({
                                                    url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/checkArealimit',
                                                    method: "POST",
                                                    data: "acode=" + $scope.acode,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    if (response.data.status == false) {
                                                        $scope.checkblock = false;
                                                        alert("The number of rooms for this area has reached its maximum value. You cannot create any more rooms.");
                                                    } else {
                                                        $scope.checkblock = true;
                                                    }
                                                }, function (error) { // optional
                                                });
                                            }

                                            $scope.saveRoom = function () {
                                                if (confirm("Do You Want To Save This Record?")) {
                                                    $http({
                                                        url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/mstRoomSubmit',
                                                        method: "POST",
                                                        data: "rcode=" + $scope.rcode + "&rname=" + $scope.rname + "&acode=" + $scope.acode + "&rremarks=" + $scope.rremarks,
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    })
                                                            .then(function (response) {
                                                                if (response.data.result == true) {
                                                                    alert(response.data.message);
                                                                    $scope.recordid = "";
                                                                    $scope.editmodeheader = false;
                                                                    $scope.rcode = "";
                                                                    $scope.rname = "";
                                                                    $scope.acode = "";
                                                                    $scope.rremarks = "";
                                                                    $scope.showAddForm = false;
                                                                    $scope.geRoomList($scope.page);
                                                                } else {
                                                                    alert(response.data.message);
                                                                }
                                                            },
                                                                    function (response) { // optional
                                                                        alert(response.data.message);
                                                                        $scope.recordid = "";
                                                                        $scope.editmodeheader = false;
                                                                        $scope.rcode = "";
                                                                        $scope.rname = "";
                                                                        $scope.acode = "";
                                                                        $scope.rremarks = "";
                                                                        $scope.showAddForm = false;
                                                                        $scope.geRoomList($scope.page);
                                                                        console.log("Something went wrong.Please try again");

                                                                    });
                                                }

                                            }
                                            $scope.toggleSelection = function toggleSelection(event) {
                                                if (event.target.checked == true) {
                                                    $scope.active_status = true;
                                                } else {
                                                    $scope.active_status = false;
                                                }
                                            };
                                            $scope.recordid = "";
                                            $scope.editRoomMaster = function (dataObj) {
                                                $http({
                                                    url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/checkParentStatus',
                                                    method: "POST",
                                                    data: "field_name=area_code&field_value=" + dataObj.area_code + "&table_name=mst_area",
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    if (response.data.parent_data[0].is_active == 0) {
                                                        $scope.showAddForm = false;
                                                        alert("Area is inactive.First activate this Area.");
                                                    } else {
                                                        $scope.showAddForm = true;
                                                        $scope.recordid = dataObj.id;
                                                        $scope.editmodeheader = true;
                                                        $scope.rremarks = "";
                                                        $scope.acode = dataObj.area_code;
                                                        $scope.rname = dataObj.room_name;
                                                        $scope.rcode = dataObj.room_code;
                                                        if (dataObj.is_active == '1') {
                                                            $scope.active_status = true;
                                                        } else {
                                                            $scope.active_status = false;
                                                        }
                                                    }
                                                }, function (error) { // optional
                                                });

                                            }
                                            $scope.updateRoom = function () {
                                                if (confirm("Do You Want To Update This Record?")) {
                                                    $http({
                                                        url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/mstRoomupdate',
                                                        method: "POST",
                                                        data: "rcode=" + $scope.rcode + "&rname=" + $scope.rname + "&acode=" + $scope.acode + "&rremarks=" + $scope.rremarks + "&roomid=" + $scope.recordid + "&status=" + $scope.active_status,
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    })
                                                            .then(function (response) {
                                                                if (response.data.result == true) {
                                                                    alert(response.data.message);
                                                                    $scope.recordid = "";
                                                                    $scope.editmodeheader = false;
                                                                    $scope.rcode = "";
                                                                    $scope.rname = "";
                                                                    $scope.acode = "";
                                                                    $scope.rremarks = "";
                                                                    $scope.showAddForm = false;
                                                                    $scope.geRoomList($scope.page);
                                                                } else {
                                                                    alert(response.data.message);
                                                                }
                                                            },
                                                                    function (response) { // optional
                                                                        alert(response.data.message);
                                                                        $scope.recordid = "";
                                                                        $scope.editmodeheader = false;
                                                                        $scope.rcode = "";
                                                                        $scope.rname = "";
                                                                        $scope.acode = "";
                                                                        $scope.rremarks = "";
                                                                        $scope.showAddForm = false;
                                                                        $scope.geRoomList($scope.page);
                                                                        console.log("Something went wrong.Please try again");

                                                                    });
                                                }

                                            }


                                        });
                                        $(document).ready(function () {
                                            $('.cc').bind('keyup paste', function () {
                                                this.value = this.value.replace(/[^0-9]/g, '');
                                            });

                                        });
</script>
