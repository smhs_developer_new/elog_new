<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
<div class="container-fluid" ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
    <!-- Page Heading -->
    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row">

                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">    	   
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th colspan="5" class="f18" style="vertical-align:middle">Report Header Master</th>
                                <th class="text-right">
                                    <?php if ($add) { ?>
                                        <button class="btn btn-success" ng-click="showForm()" ng-disabled="editmodeheader"> Add (+)</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success" ng-click="showForm()" disabled> Add (+)</button>
                                    <?php } ?>

                                </th>
                            </tr>
                        </thead>
                    </table>  
                </div>
                <div class="col-12">
                    <form name="materialForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row"> 
                                        <!-- <div class="col-lg-4 mb-3"><label>Report Name <span style="color: red">*</span></label>
                                            <select class="chosen form-control" tabindex="4" name="header_report_id" data-placeholder="Search Report" ng-options="dataObj['id'] as (dataObj.report_name) for dataObj in reportList"  ng-model="header_report_id" required ng-disabled="editmodeheader">
                                                <option value="">Select Report Name</option>
                                            </select>
                                        </div> -->
                                        <div class="col-lg-3 mb-3"><label>Log Name <span style="color: red">*</span></label>
                                            <select class="chosen form-control" tabindex="4" name="header_Log_id" data-placeholder="Search Log Name" ng-options="dataObj['id'] as (dataObj.activity_name) for dataObj in LogList" ng-change="getHeaderList(dataObj.id)"  ng-model="header_Log_id" required ng-disabled="editmodeheader">
                                                <option value="">Select Log Name</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 mb-3"><label><b>Form No </b><span style="color: red">*</span></label>
                                            <input type="text" class="form-control" name="form_no" ng-model="form_no" required ng-disabled="editmodeheader">
                                        </div>
                                        <div class="col-lg-3 mb-3"><label><b>Document Reference No</b><span style="color: red">*</span></label>
                                            <input type="text" class="form-control" name="document_no" ng-model="document_no" required ng-disabled="editmodeheader">
                                        </div>
                                        <div class="col-lg-3 mb-3"><label>Footer Name </label>
                                            <input type="text" class="form-control" name="footer_name" ng-model="footer_name" >
                                        </div>                                        

                                    </div>


                                </div>
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-lg-3 mb-3"><label><b>Version No</b><span style="color: red">*</span></label>
                                            <input style="display:none;" type="text" class="form-control" name="version_no" ng-model="version_no" required ng-disabled="editmodeheader" readonly>
                                            <input type="text" class="form-control" numeric-only name="user_version_no" ng-model="user_version_no" required ng-disabled="editmodeheader">
                                        </div>
                                        <div class="col-lg-3 mb-3"><label><b>Effective Date </b><span style="color: red">*</span></label>
                                            <div class="input-group"
                                                 moment-picker="effective_date"
                                                 format="DD-MMM-YYYY"   start-view="month" locale="en" today="true">

                                                <input class="form-control" readonly id="effective_date" name="effective_date" 
                                                       placeholder="Select effective Date"
                                                       ng-model="effective_date"
                                                       ng-model-options="{
                                                                           updateOn: 'blur'
                                                                       }" autocomplete="off" required ng-disabled="editmodeheader">
                                            </div>

                                        </div>
                                        <div class="col-lg-3 mb-3"><label><b>Retired  Date </b></label>
                                            <div class="input-group"
                                                 moment-picker="retired_date"
                                                 format="DD-MMM-YYYY"   start-view="month" min-date="effective_date"  locale="en" today="true">

                                                <input class="form-control " readonly id="retired_date" name="retired_date"
                                                       placeholder="Select Retired Date"
                                                       ng-model="retired_date"
                                                       ng-model-options="{
                                                                           updateOn: 'blur'
                                                                       }" autocomplete="off">
                                            </div>

                                        </div>
                                        <div class="col-lg-3 mb-3"><label><b>Alert On before days</b></label>
                                            <input type="text" class="form-control" name="alertondate" ng-model="alertondate" numeric-only ng-disabled="retired_date == undefined || retired_date == '' || retired_date == null" ng-blur="getalerton()">
                                            <!-- <div class="input-group"
                                                 moment-picker="alertondate"
                                                 format="DD-MMM-YYYY"   start-view="month" min-date="effective_date" max-date="retired_date"  locale="en" today="true">

                                                <input class="form-control " readonly id="alertondate" name="alertondate" 
                                                       placeholder="Select Alert On Date"
                                                       ng-model="alertondate" ng-disabled="retired_date == undefined || retired_date == '' || retired_date == null"
                                                       ng-model-options="{
                                                                           updateOn: 'blur'
                                                                       }" autocomplete="off">
                                            </div> -->
                                        </div>                                                                                
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-6 mb-3"  ng-show="editmodeheader">
                                            <label for="validationServer01">Remarks <span style="color: red" ng-show="editmodeheader">*</span></label>
                                            <textarea class="form-control" aria-label="With textarea" maxlength="100" name="bremarks" ng-model="remark"  ng-required="editmodeheader"></textarea>

                                        </div>
                                        <div class="col-lg-6 mb-3" ng-show="editmodeheader"><label><b>Active/Inactive</b></label>
                                            <label class="switch">
                                                <input type="checkbox" style="margin-top: 13px;" ng-model="active_status" ng-checked="active_status == 'active'" ng-click="toggleSelection($event)">
                                                <span class="slider round"></span>

                                        </div>                              
                                    </div>                                  

                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-success btn-sm" ng-disabled="materialForm.$invalid"  ng-click="saveHeader()" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        <button ng-click="resetForm();hideFormHeader();" class="btn btn-sm btn-danger"  type="reset">&nbsp;&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;&nbsp;</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="form-row" ng-show="headerData.length == 0">
                <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
            </div>
            <div class="form-row" style="margin-top:10px;" ng-show="headerData.length > 0">   
                <h2 class="f18">Report Header List</h2>
                <div class="table-responsive noscroll" >
                    <table  class="table custom-table">
                        <thead >
                            <tr>
                                <th>Header Name</th>
                                <th>Form No.</th>
                                <th>Version No.</th>
                                <th>Effective Date</th>
                                <th>Retired Date</th>
                                <th>Document No.</th>
                                <th>Last Modified By</th>
                                <th>Last Modified On</th>
                                <th>Action</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="dataObj in headerData">
                                <td>{{dataObj.activity_name}}</td>
                                <td>{{dataObj.form_no}}</td>
                                <td>{{dataObj.user_version_no}}</td>
                                <td>{{dataObj.effective_date| date:'dd-MMM-yyyy'}}</td>
                                <td ng-if="dataObj.retired_date != NULL">{{dataObj.retired_date| date:'dd-MMM-yyyy'}}</td>
                                <td ng-if="dataObj.retired_date == NULL">NA</td>
                                <td>{{dataObj.document_no}}</td>
                                <td>{{dataObj.modified_by!=null?dataObj.modified_by:dataObj.created_by}}</td>
                                <td ng-if="dataObj.modified_on != null">{{dataObj.modified_on| format | date:'dd-MMM-yyyy'}}</td>
                                <td ng-if="dataObj.modified_on == null">{{dataObj.created_on| format | date:'dd-MMM-yyyy'}}</td>
                                <td >
                                    <?php if ($edit) { ?>
                                        <a type="button" ng-click="editHeader(dataObj)"><i class="far fa-edit text-blue pr10"></i></a>
                                    <?php } else { ?>
                                        <a type="button" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                    <?php } ?>
                                </td>
                                <td ng-if="dataObj.status == 'inactive'" class="bg-danger text-white">Inactive</td>
                                <td ng-if="dataObj.status == 'active'" class="bg-success text-white">Active</td>
                            </tr>
                        </tbody>
                    </table>  
                </div>

            </div>
        </div>
    </div>
    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row">

                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">    	   
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th colspan="5" class="f18" style="vertical-align:middle">Report Footer Master</th>
                                <th class="text-right">
                                    <?php if ($add) { ?>
                                        <button class="btn btn-success" ng-click="showFooterForm()" ng-disabled="editmodefooter"> Add (+)</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success" ng-click="showFooterForm()" disabled> Add (+)</button>
                                    <?php } ?>

                                </th>
                            </tr>
                        </thead>
                    </table>  
                </div>
                <div class="col-12">
                    <form name="footerForm" novalidate>
                        <div class="card card-default" ng-show="showAddFooterForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row">   
                                        <div class="col-lg-4 mb-3"><label>Report Name <span style="color: red">*</span></label>
                                            <select class="form-control" name="report_id"   ng-model="report_id" required ng-disabled="editmodefooter">
                                                <option value="">Select Report Name</option>
                                                <option ng-repeat="dataObj in reportList" ng-hide="dataObj.id == 1 || dataObj.id == 18 || dataObj.id == 19 || dataObj.id == 20 || dataObj.id == 21 || dataObj.id == 22" value="{{dataObj.id}}">{{dataObj.report_name}}</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 mb-3"><label>Footer Name <span style="color: red">*</span></label>
                                            <input type="text" class="form-control" name="footer_name" ng-model="footer_name" required>
                                        </div>
                                        <div class="col-lg-4 mb-3" ng-show="editmodefooter">
                                            <label for="validationServer01">Remarks <span style="color: red" ng-show="editmodefooter">*</span></label>
                                            <textarea class="form-control" aria-label="With textarea" maxlength="100"  name="bremarks" ng-model="remark_footer"  ng-required="editmodefooter"></textarea>

                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="col-lg-2 mb-3" ng-show="editmodefooter" style="padding: 30px 10px 10px 10px;"><label><b>Active/Inactive</b></label>
                                            <label class="switch">
                                                <input type="checkbox" style="margin-top: 13px;" ng-model="active_status_footer" ng-checked="active_status_footer == 'active'" ng-click="toggleSelectionFooter($event)">
                                                <span class="slider round"></span>

                                        </div>                              
                                    </div> 

                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-success btn-sm" ng-disabled="footerForm.$invalid"  ng-click="saveFooter()" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        <button ng-click="resetForm();hideFormFooter();" class="btn btn-sm btn-danger"  type="reset">&nbsp;&nbsp;&nbsp;&nbsp;Cancel&nbsp;&nbsp;&nbsp;&nbsp;</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="form-row" ng-show="footerData.length == 0">
                <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
            </div>
            <div class="form-row" style="margin-top:10px;" ng-show="footerData.length > 0">   
                <h2 class="f18">Report Footer List</h2>
                <div class="table-responsive noscroll" >
                    <table  class="table custom-table">
                        <thead >
                            <tr>
                                <th>Report Name</th>
                                <th>Report Footer</th>
                                <th>Last Modified By</th>
                                <th>Last Modified On</th>
                                <th>Action</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="dataObj in footerData">
                                <td>{{dataObj.report_name}}</td>
                                <td>{{dataObj.footer_name}}</td>
                                <td>{{dataObj.emp_name!=null?dataObj.emp_name:dataObj.created_name}}</td>

                                <td ng-if="dataObj.updated_on != null">{{dataObj.updated_on| format | date:'dd-MMM-yyyy'}}</td>
                                <td ng-if="dataObj.updated_on == null">{{dataObj.created_on| format | date:'dd-MMM-yyyy'}}</td>
                                <td >
                                    <?php if ($edit) { ?>
                                        <a type="button" ng-click="editFooter(dataObj)"><i class="far fa-edit text-blue pr10"></i></a>
                                    <?php } else { ?>
                                        <a type="button" ng-click="editFooter(dataObj)"><i class="far fa-edit text-blue pr10"></i></a>
                                    <?php } ?>
                                </td>
                                <td ng-if="dataObj.status == 'inactive'" class="bg-danger text-white">Inactive</td>
                                <td ng-if="dataObj.status == 'active'" class="bg-success text-white">Active</td>
                            </tr>
                        </tbody>
                    </table>  
                </div>

            </div>
        </div>
    </div>

</div>

<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment-with-locales.js"></script>
<script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>

<script type="text/javascript">
                                    var app = angular.module("materialApp", ['angular.chosen', 'moment-picker']);
                                    app.directive('chosen', function ($timeout) {

                                        var linker = function (scope, element, attr) {
                                            scope.$watch('reportList', function () {
                                                $timeout(function () {
                                                    element.trigger('chosen:updated');
                                                }, 0, false);
                                            }, true);
                                            $timeout(function () {
                                                element.chosen();
                                            }, 0, false);
                                        };
                                        return {
                                            restrict: 'A',
                                            link: linker
                                        };
                                    });
                                    app.directive('numericOnly', function () {
                                        return {
                                            require: '?ngModel',
                                            link: function (scope, element, attrs, ngModelCtrl) {
                                                if (!ngModelCtrl) {
                                                    return;
                                                }
                                                ngModelCtrl.$parsers.push(function (val) {
                                                    if (angular.isUndefined(val)) {
                                                        var val = '';
                                                    }

                                                    var clean = val.replace(/[^-0-9\\.]/g, '');
                                                    var negativeCheck = clean.split('-');
                                                    var decimalCheck = clean.split('.');
                                                    if (!angular.isUndefined(negativeCheck[1])) {
                                                        negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                                                        clean = negativeCheck[0] + '-' + negativeCheck[1];
                                                        if (negativeCheck[0].length > 0) {
                                                            clean = negativeCheck[0];
                                                        }

                                                    }

                                                    if (!angular.isUndefined(decimalCheck[1])) {
                                                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                                                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                                                    }

                                                    if (val !== clean) {
                                                        ngModelCtrl.$setViewValue(clean);
                                                        ngModelCtrl.$render();
                                                    }
                                                    return clean;
                                                });

                                                element.bind('keypress', function (event) {
                                                    if (event.keyCode === 32) {
                                                        event.preventDefault();
                                                    }
                                                });
                                            }
                                        };
                                    });
                                    app.filter('format', function () {
                                        return function (item) {
                                            var t = item.split(/[- :]/);
                                            var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                            var time = d.getTime();
                                            return time;
                                        };
                                    });
                                    app.controller("materialCtrl", function ($scope, $http, $filter) {
                                        $scope.preRetiredDate = "";
                                        $scope.preDate = "";
                                        $scope.retired_date = "";
                                        $scope.alertondate = "";
                                        $scope.headerData = [];
                                        $scope.getHeaderList = function () {
                                            $scope.headerData = [];
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getHeaderList?header_id=' + $scope.header_Log_id,
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.headerData = response.data.header_list;
                                                console.log(response.data.header_list);
                                                console.log($scope.headerData);
                                                if ($scope.headerData.length > 0 && $scope.header_Log_id > 0)
                                                {
                                                    $scope.preRetiredDate = $scope.headerData[0]["retired_date"];
                                                    var version = $scope.headerData[0]["version_no"];
                                                    var val = version.split(".");
                                                    $scope.version_no = (parseInt(val[0]) + 1) + ".0";
                                                } else
                                                {
                                                    $scope.version_no = "1.0";
                                                }
                                                //$scope.preRetiredDate =   $filter('date')($scope.preDate, "dd-MMM-yyyy");
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.getHeaderList();

                                        $scope.headerDataforpreRetiredate = [];
                                        $scope.headerDataforpreRetiredate = function () {
                                            $scope.headerDataforpreRetiredate = [];
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getHeaderList?header_id=' + $scope.header_Log_id,
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.headerDataforpreRetiredate = response.data.header_list;
                                                $scope.preRetiredDate = $scope.headerDataforpreRetiredate[0]["retired_date"];
                                                //$scope.preRetiredDate =   $filter('date')($scope.preDate, "dd-MMM-yyyy");
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");
                                            });
                                        }

                                        $scope.showAddForm = false;
                                        $scope.showForm = function () {
                                            $scope.showAddForm = true;
                                        }
                                        $scope.alertonfunc = function () {
                                            if ($scope.retired_date == "" || $scope.retired_date == undefined || $scope.retired_date == null)
                                            {
                                                alert("You can not be set the alert date without retired date");
                                                $scope.alertondate = "";
                                                return false;
                                            } else
                                            {
                                                if ($scope.alertondate > $scope.retired_date)
                                                {
                                                    alert("Alert date should be less then the retired date");
                                                    $scope.alertondate = "";
                                                    return false;
                                                }
                                            }
                                        }
                                        $scope.getalerton = function () {
                                            if ($scope.retired_date != "") {
                                                var date2 = new Date($scope.retired_date);
                                                var date1 = new Date($scope.effective_date);
                                                var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                                                $scope.dayDifference = Math.ceil(timeDiff / (1000 * 3600 * 24));
                                                if ($scope.alertondate >= $scope.dayDifference)
                                                {
                                                    alert("alert days should be less then from between days your date selection...!");
                                                    $scope.alertondate = "";
                                                    return false;
                                                }
                                            }
                                        }
                                        $scope.checkdate = function () {
                                            debugger;
                                            if ($scope.effective_date > $scope.preRetiredDate)
                                            {
                                                return true;
                                            } else {
                                                alert("Effective date should be greater then the last Retired date");
                                                $scope.effective_date = "";
                                                return false;
                                            }
                                        }
                                        $scope.remark = '';
                                        $scope.user_version_no = "";
                                        $scope.hideFormHeader = function () {
                                            $scope.showAddForm = false;
                                            $scope.editmodeheader = false;

                                            $scope.headerId = 0;
                                            $scope.form_no = '';
                                            $scope.version_no = '';
                                            $scope.user_version_no = "";
                                            $scope.effective_date = "";
                                            $scope.document_no = "";
                                            $scope.remark = '';
                                            $scope.footer_name = '';
                                            $scope.active_status = '';
                                            $scope.header_Log_id = "";
                                            $scope.getHeaderList();

                                        }
                                        $scope.form_no = '';
                                        $scope.version_no = '';
                                        $scope.effective_date = "";
                                        $scope.document_no = "";
                                        $scope.headerId = 0;
                                        $scope.remark = "";
                                        $scope.editHeader = function (dataObj) {
<?php if ($edit == false) { ?>
                                                return false;
<?php } ?>
                                            $scope.editmodeheader = true;
                                            $scope.showAddForm = true;
                                            $scope.header_Log_id = dataObj.header_id;
                                            //$scope.headerDataforpreRetiredate();
                                            $scope.form_no = dataObj.form_no;
                                            $scope.version_no = dataObj.version_no;
                                            $scope.user_version_no = dataObj.user_version_no;
                                            $scope.effective_date = dataObj.effective_date;
                                            $scope.footer_name = dataObj.footer_name;
                                            $scope.effective_date = $filter('date')($scope.effective_date, "dd-MMM-yyyy");
                                            if (dataObj.retired_date != null) {
                                                $scope.retired_date = dataObj.retired_date;
                                                $scope.retired_date = $filter('date')($scope.retired_date, "dd-MMM-yyyy");
                                            }
                                            $scope.alertondate = dataObj.alertondate;
                                            //$scope.alertondate = $filter('date')($scope.alertondate, "dd-MMM-yyyy");
                                            $scope.document_no = dataObj.document_no;
                                            $scope.headerId = dataObj.id;
                                            $scope.remark = "";
                                            $scope.active_status = dataObj.status;

                                        }
                                        $scope.saveHeader = function () {
                                            // if(($scope.retired_date == "" || $scope.retired_date == undefined || $scope.retired_date == null) && ($scope.alertondate == ""))
                                            // {
                                            if ((moment(moment($scope.effective_date, 'DD-MMM-YYYY')).format('YYYY-MM-DD') > $scope.preRetiredDate) || ($scope.headerId > 0 && $scope.headerId != '') || $scope.preRetiredDate==null)
                                            {
                                                if ($scope.headerId > 0)
                                                {
                                                    var active_status = ($scope.active_status === undefined) ? 'inactive' : $scope.active_status;
                                                } else
                                                {
                                                    var active_status = 'active';
                                                }

                                                if (confirm("Do You Want To Save This Record?")) {
                                                    $http({
                                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/saveHeader',
                                                        method: "POST",
                                                        data: "header_Log_id=" + $scope.header_Log_id + "&form_no=" + $scope.form_no + "&version_no=" + $scope.version_no + "&effective_date=" + $scope.effective_date + "&retired_date=" + $scope.retired_date + "&document_no=" + $scope.document_no + "&header_id=" + $scope.headerId + "&status=" + active_status + "&remark=" + $scope.remark + "&alertondate=" + $scope.alertondate + "&user_version_no=" + $scope.user_version_no+"&footer_name="+$scope.footer_name,
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    })
                                                            .then(function (response) {
                                                                alert(response.data.message);
                                                                $scope.header_Log_id = "";
                                                                $scope.preRetiredDate = "";
                                                                $scope.form_no = '';
                                                                $scope.version_no = '';
                                                                $scope.user_version_no = '';
                                                                $scope.effective_date = "";
                                                                $scope.retired_date = "";
                                                                $scope.alertondate = "";
                                                                $scope.document_no = "";
                                                                $scope.headerId = 0;
                                                                $scope.showAddForm = false;
                                                                $scope.remark = "";
                                                                $scope.active_status = "";
                                                                $scope.editmodeheader = false;
                                                                $scope.footer_name = '';
                                                                $scope.getHeaderList();
                                                            },
                                                                    function (response) { // optional
                                                                        $scope.showAddForm = false;
                                                                        $scope.stageId = 0;
                                                                        $scope.remark = "";
                                                                        $scope.active_status = "";
                                                                        $scope.user_version_no = '';
                                                                        $scope.footer_name = '';
                                                                        $scope.getHeaderList();
                                                                        console.log("Something went wrong.Please try again");
                                                                    });
                                                }
                                            } else {
                                                alert("Effective date should be greater then the last Retired date");
                                                $scope.effective_date = "";
                                                $scope.retired_date = "";
                                                $scope.alertondate = "";
                                                return false;
                                            }
                                            // }
                                            // else
                                            // {
                                            //     alert("You can not be set without Retired date, Please set the retired_date first...!");
                                            //     $scope.alertondate ="";
                                            //     return false;
                                            // }
                                        }

                                        $scope.deleteHeader = function (objId, action) {
                                            if (confirm("Do You Want To " + action + " This Header!")) {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/deleteHeader',
                                                    method: "POST",
                                                    data: "id=" + objId + "&action=" + action,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                })
                                                        .then(function (response) {
                                                            $scope.getHeaderList();
                                                            alert(response.data.message);
                                                        },
                                                                function (response) { // optional
                                                                    $scope.getHeaderList();
                                                                    console.log("Something went wrong.Please try again");
                                                                });
                                            }
                                        }

                                        //Code for Report Footer Section
                                        //Get All Transaction Logs Data
                                        $scope.reportList = [];
                                        $scope.getReportList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getReportList',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.reportList = response.data.report_list;
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.getReportList();

                                        $scope.LogList = [];
                                        $scope.Getactivitylistforroom = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getmasteractivitylist?activity_type=log',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.LogList = response.data.master_activity_list;
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.Getactivitylistforroom();

                                        $scope.showAddFooterForm = false;
                                        $scope.showFooterForm = function () {
                                            $scope.showAddFooterForm = true;
                                        }
                                        $scope.footerData = [];
                                        $scope.getFooterList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getFooterList',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.footerData = response.data.footer_list;
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.getFooterList();
                                        // $scope.report_id = '';
                                        $scope.footer_name = '';
                                        $scope.footerId = 0;
                                        $scope.remark_footer = '';
                                        $scope.hideFormFooter = function () {
                                            $scope.showAddFooterForm = false;
                                            $scope.editmodefooter = false;

                                            if (angular.isDefined($scope.report_id)) {
                                                delete $scope.report_id;
                                            }
                                            $scope.footerId = 0;
                                            $scope.remark_footer = '';
                                            $scope.active_status_footer = '';

                                        }
                                        $scope.saveFooter = function () {
                                            var active_status_footer = ($scope.active_status_footer === undefined) ? 'active' : $scope.active_status_footer
                                            if (confirm("Do You Want To Save This Record?")) {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/saveFooter',
                                                    method: "POST",
                                                    data: "report_id=" + $scope.report_id + "&footer_name=" + $scope.footer_name + "&footer_id=" + $scope.footerId + "&status=active&remark=" + $scope.remark_footer,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                })
                                                        .then(function (response) {
                                                            alert(response.data.message);
                                                            $scope.report_id = '';
                                                            $scope.footer_name = '';
                                                            $scope.footerId = 0;
                                                            $scope.showAddFooterForm = false;
                                                            $scope.remark_footer = '';
                                                            $scope.active_status_footer = '';
                                                            $scope.editmodefooter = false;
                                                            $scope.getFooterList();
                                                        },
                                                                function (response) { // optional
                                                                    $scope.report_id = '';
                                                                    $scope.footer_name = '';
                                                                    $scope.footerId = 0;
                                                                    $scope.showAddFooterForm = false;
                                                                    $scope.remark_footer = '';
                                                                    $scope.active_status_footer = '';
                                                                    $scope.editmodefooter = false;
                                                                    $scope.getFooterList();
                                                                    console.log("Something went wrong.Please try again");
                                                                });
                                            }
                                        }
                                        $scope.editFooter = function (dataObj) {
<?php if ($edit == false) { ?>
                                                return false;
<?php } ?>
                                            $scope.editmodefooter = true;
                                            $scope.showAddFooterForm = true;
                                            $scope.report_id = dataObj.report_id;
                                            $scope.footer_name = dataObj.footer_name;
                                            $scope.footerId = dataObj.id;
                                            $scope.remark_footer = "";
                                            $scope.active_status_footer = dataObj.status;
                                        }
                                        $scope.deleteFooter = function (objId, action) {
                                            if (confirm("Do You Want To " + action + " This Report Footer!")) {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/deleteFooter',
                                                    method: "POST",
                                                    data: "id=" + objId + "&action=" + action,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                })
                                                        .then(function (response) {
                                                            $scope.getFooterList();
                                                            alert(response.data.message);
                                                        },
                                                                function (response) { // optional
                                                                    $scope.getFooterList();
                                                                    console.log("Something went wrong.Please try again");
                                                                });
                                            }
                                        }
                                        $scope.toggleSelection = function (event) {
                                            if (event.target.checked == true) {
                                                $scope.active_status = 'active';
                                            } else {
                                                $scope.active_status = 'inactive';
                                            }
                                        };
                                        $scope.toggleSelectionFooter = function (event) {
                                            if (event.target.checked == true) {
                                                $scope.active_status_footer = 'active';
                                            } else {
                                                $scope.active_status_footer = 'inactive';
                                            }
                                        };
                                    });
</script>