<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Bootstrap Admin App">
        <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
        <link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>favicon.ico">
        <title>E-Logbook</title>
        <!-- =============== VENDOR STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/brands.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/regular.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/solid.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/fontawesome.css">
        <!-- SIMPLE LINE ICONS-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/simple-line-icons/css/simple-line-icons.css">
        <!-- ANIMATE.CSS-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/animate.css/animate.css">
        <!-- WHIRL (spinners)-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/whirl/dist/whirl.css">
        <!-- =============== PAGE VENDOR STYLES ===============-->
        <!-- =============== BOOTSTRAP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css" id="bscss">
        <!-- =============== APP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/app.css" id="maincss">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/Ponta_style.css">



        <!-- TAGS INPUT-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.css"><!-- SLIDER CTRL-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-slider/dist/css/bootstrap-slider.css"><!-- CHOSEN-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/chosen-js/chosen.css"><!-- DATETIMEPICKER-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.css"><!-- COLORPICKER-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css"><!-- SELECT2-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/select2/dist/css/select2.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.css"><!-- WYSIWYG-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-wysiwyg/css/style.css"><!-- =============== BOOTSTRAP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />
        <style>
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
        </style>
    </head>

    <body ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
        <div class="wrapper">
            <!-- top navbar-->
            <header class="topnavbar-wrapper"> 
                <!-- START Top Navbar-->
                <nav class="navbar topnavbar"> 
                    <!-- START navbar header-->
                    <div class="navbar-header"><a class="navbar-brand" href="<?php echo base_url(); ?>home">
                            <div class="brand-logo font-weight-bold text-white">SMHS - eLog System</div>
                            <div class="brand-logo-collapsed font-weight-bold text-white">SMHS</div>
                        </a></div>
                    <!-- END navbar header--> 
                    <!-- START Left navbar-->
                    <ul class="navbar-nav mr-auto flex-row">
                        <li class="nav-item"> 
                            <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link d-none d-md-block d-lg-block d-xl-block" href="#" data-trigger-resize="" data-toggle-state="aside-collapsed"><em class="fas fa-bars"></em></a><!-- Button to show/hide the sidebar on mobile. Visible on mobile only.--><a class="nav-link sidebar-toggle d-md-none" href="#" data-toggle-state="aside-toggled" data-no-persist="true"><em class="fas fa-bars"></em></a></li>
                        <!-- START User avatar toggle-->
                        <li class="nav-item d-none d-md-block"> 
                            <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link" id="user-block-toggle" href="#user-block" data-toggle="collapse"><em class="icon-user"></em></a></li>
                        <!-- END User avatar toggle--> 
                    </ul>
                    <!-- END Left navbar--> 
                    <!-- START Right Navbar-->
                    <ul class="navbar-nav flex-row">
                        <!-- Fullscreen (only desktops)-->
                        <li class="nav-item d-none d-md-block"><a class="nav-link" href="<?php echo base_url(); ?>" data-toggle-fullscreen=""><em class="fas fa-expand"></em></a></li>
                        <!-- START Offsidebar button-->
                        <li class="nav-item"><a href="<?php echo base_url(); ?>#" class="nav-link"></a></li>


                        <!-- END Offsidebar menu-->
                    </ul>
                    <!-- END Right Navbar--> 

                </nav>
                <!-- END Top Navbar--> 
            </header>
            <!-- sidebar-->
            <aside class="aside-container"> 
                <!-- START Sidebar (left)-->
                <div class="aside-inner">
                    <nav class="sidebar" data-sidebar-anyclick-close=""> 
                        <!-- START sidebar nav-->
                        <ul class="sidebar-nav">
                            <!-- START user info-->
                            <li class="has-user-block">
                                <div class="collapse" id="user-block">
                                    <div class="item user-block"> 
                                        <!-- User picture-->
                                        <div class="user-block-picture">
                                            <div class="user-block-status"><img class="img-thumbnail rounded-circle" src="<?php echo base_url(); ?>img/user/02.jpg" alt="Avatar" width="60" height="60">
                                                <div class="circle bg-success circle-lg"></div>
                                            </div>
                                        </div>
                                        <!-- Name and Job-->
                                        <div class="user-block-info"><span class="user-block-name">Hello, <?php echo $this->session->userdata('empname') ?></span><span class="user-block-role"><?php if ($this->session->userdata('empname')) { ?><a href="<?php echo base_url() ?>user/logout" >Logout </a><?php } ?></span></div>
                                    </div>
                                </div>
                            </li>
                            <!-- END user info--> 
                            <!-- Iterates over all sidebar items-->
                            <li class="nav-heading text-bold"><span data-localize="sidebar.heading.HEADER">Main Navigation</span>
                            <li>
                                <a href="<?php echo base_url(); ?>User/home" title="Home"><em class="fas fa-home"></em><span data-localize="sidebar.nav.Plant master">Home</span></a>
                            </li>
                            <!--                            <li>
                                                            <a href="<?php echo base_url(); ?>manage_material" title="Material master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Material master">Manage Material</span></a>
                                                        </li>-->
                            <li>
                                <a href="<?php echo base_url(); ?>manage_room" title="Room master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Room master">Manage Room</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>manage_room_activity" title="Room Activity master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Room Activity master">Manage Room Activity</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>balance_master" title="Balance master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Balance master">Manage Balance Data</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>balance_frequency_master" title="Balance Frequency master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Balance Frequency Master">Balance Frequency Data</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>tablet_tooling_master" title="Tablet Tooling master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Tooling master">Tablet Tooling</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>manage_id_standard_weight" title="Id Standard Weight master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Id Standard Weight master">Manage Id Standard Weight</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>work_assign_asper_rolebase" title="Work Assign Master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Work Assign Master">Work Assign Master</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>workflow" title="Work Flow"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Work Flow">Work Flow</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>line_log_master" title="Line Log Master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Line Log Master">Line Log Master</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>instrument_master" title="Instrument Master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Instrument Master">Instrument Master</span></a>
                            </li>
                            <li>
                                <a href="<?php echo base_url(); ?>stage_master" title="Stage Master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Tablet Stage Master">Stage Master</span></a>
                            </li>
                        </ul>
                        <!-- END sidebar nav--> 
                    </nav>
                </div>
                <!-- END Sidebar (left)--> 
            </aside>
            <!-- Main section-->
          <!--<section>-->
            <!-- Page content-->
            <section class="section-container"> 
                <!-- Page content-->
                <div class="content-wrapper">
                    <div class="content-heading executesop-heading">
                        <div class="col-sm-5 pl-0">Room Filter Details</div>
                    </div>                   
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="row" ng-show="room_filterlist.length > 0">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10"><h2>Room Filter List</h2></div>

                            </div>
                            <div class="row" ng-show="room_filterlist.length == 0">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10"><h2>No Room Found</h2></div>

                            </div>
                             <table class="table table-dark" ng-show="room_filterlist.length > 0">
                                    <thead>
                                        <tr>
                                            <th>Room Code</th>
                                            <th>Filter</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="dataObj in room_filterlist">
                                            <td>{{dataObj.room_code}}</td>
                                            <td>{{dataObj.filter_name}}</td>
                                            <td>{{dataObj.filter_desc}}</td>
                                            <td><button class="btn btn-success" ng-click="getfilterApproved(dataObj.id);">Approval</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!--</section>-->
        <!-- Page footer-->
        <!-- Page footer-->

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>js/angular.min.js"></script>
    <script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
    <script type="text/javascript">
                                            var app = angular.module("materialApp", ['angular.chosen']);
                                            app.directive('chosen', function ($timeout) {

                                                var linker = function (scope, element, attr) {

                                                    scope.$watch('masterRoomList', function () {
                                                        $timeout(function () {
                                                            element.trigger('chosen:updated');
                                                        }, 0, false);
                                                    }, true);
                                                    $timeout(function () {
                                                        element.chosen();
                                                    }, 0, false);
                                                };
                                                return {
                                                    restrict: 'A',
                                                    link: linker
                                                };
                                            });
                                            app.controller("materialCtrl", function ($scope, $http, $filter) {
                                                $scope.room_filterlist = [];
                                                $scope.room_type = "room";
                                                $scope.getroom_filterlist = function () {
                                                    $http({
                                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getroomfilters_forApproval',
                                                        method: "GET",
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    }).then(function (response) {
                                                        $scope.room_filterlist = response.data.room_filterlist;
                                                    }, function (error) { // optional

                                                        console.log("Something went wrong.Please try again");

                                                    });
                                                }
                                                $scope.getroom_filterlist();

                                                $scope.getfilterApproved = function (id) {
                                                    if (confirm("Do You Want To Approve This Filter!")) {
                                                        $http({
                                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getfilterApproved',
                                                            method: "POST",
                                                            data: "id=" + id,
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        }).then(function (response) {
                                                            alert(response.data.message);
                                                            $scope.getroom_filterlist();
                                                        }, function (error) { // optional
                                                        console.log("Something went wrong.Please try again");
                                                    });
                                                    }
                                                }


                                            });
    </script>
</body>
</html>