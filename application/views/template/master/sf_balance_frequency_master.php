<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
    <!-- Page Heading -->

    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row">

                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">    	   
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th colspan="5" style="vertical-align:middle">Balance Calibration Frequency</th>
                                <th class="text-right">
                                    <?php if ($add) { ?>
                                        <button class="btn btn-success" ng-click="showForm()" ng-disabled="editmode">Add (+)</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success" ng-click="showForm()" ng-disabled="editmode">Add (+)</button>
                                    <?php } ?>

                                </th>
                            </tr>
                        </thead>
                    </table>  
                </div>
                <div class="col-12">
                    <form name="balanceForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-lg-3 mb-3"><label>Balance No <span style="color: red">*</span></label>
                                            <select class="form-control"  id="room" ng-change="resetData()"  name="balance_no"  ng-model="balance_no" ng-disabled="editmode" required>
                                                <option value="">Select Balance</option>
                                                <option value="{{dtaObj.balance_no}}" ng-repeat="dtaObj in balanceCalibrationData">{{dtaObj.balance_no}}</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 mb-3"><label>Frequency <span style="color: red">*</span></label>
                                            <select class="form-control" name="frequency_id" ng-change="getStandardData()" ng-model="frequency_id" ng-disabled="editmode" required>
                                                <option value="">Select Frequency</option>
                                                <option value="{{daataObj.id}}" ng-repeat="daataObj in frequencyData" ng-hide="daataObj.frequency_name == 'Type -A' || daataObj.frequency_name == 'Type -B'">{{daataObj.frequency_name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row"  ng-show="standardData.length > 0"> 
                                        <table class="table ">
                                            <thead class="table-dark">
                                                <tr>
                                                    <th></th>
                                                    <th>Measurement(KG/GM)</th>
                                                    <th>Standard WT.(KG/GM)</th>
                                                    <th>Upper Limit WT.(KG/GM)</th>
                                                    <th>Lower Limit WT.(KG/GM)</th>                     
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="dataObj in standardData">
                                                    <td><input type="checkbox" ng-click="getCheckedData(dataObj)" name="ischecked" id="{{dataObj.id}}" ng-model="dataObj.ischecked" ng-checked="dataObj.checked"></td>
                                                    <td>{{dataObj.measurement_unit}}</td>
                                                    <td>{{dataObj.standard_value}}</td>
                                                    <td>{{dataObj.max_value}}</td>
                                                    <td>{{dataObj.min_value}}</td>                      
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-3 mb-3"> 
                                            <label for="validationServer01">Remarks <span style="color: red" ng-show="editmode">*</span></label>
                                            <textarea class="form-control" aria-label="With textarea" maxlength="100" ng-required="recordid > 0" name="remark" ng-model="remark"  placeholder="Enter Remarks" ></textarea>

                                        </div>
                                        <div class="col-lg-2 mb-3" ng-show="editmode" style="padding: 30px 10px 10px 10px;"><label><b>Active/Inactive</b></label>
                                            <label class="switch">
                                                <input type="checkbox" style="margin-top: 13px;" ng-model="active_status" ng-click="toggleSelection($event)">
                                                <span class="slider round"></span>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-sm btn-success" ng-show="recordid > 0"  ng-disabled="formData.length == 0 || remark==''"  ng-click="saveFrequencyBalance()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        <button class="btn btn-sm btn-success" ng-show="recordid==''" ng-disabled="formData.length == 0"  ng-click="saveFrequencyBalance()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        <button ng-click="hideForm()" class="btn btn-danger btn-sm" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Cancel &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="form-row" ng-show="balanceFrequencyList.length == 0">
                <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
                <div class="col-lg-12 text-center" ><h4>(Please Select the above parameters)</h4></div> 
            </div>
            <div class="form-row" style="margin-top:10px;" ng-show="balanceFrequencyList.length > 0">   
                <h2>Balance Calibration Frequency List</h2>
                <div class="table-responsive noscroll">
                    <table  class="table custom-table">
                        <thead class="table-dark">
                            <tr>
                                <th>Balance No</th>
                                <th>Frequency</th>
                                <!-- <th>Measurement(KG/GM)</th>
                                <th>Standard WT.(KG/GM)</th>
                                <th>Upper Limit WT.(KG/GM)</th>
                                <th>Lower Limit WT.(KG/GM)</th> -->
                                <th>Last Modified By</th>
                                <th>Last Modified On</th>
                                <th>Action</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr dir-paginate="dataObj in balanceFrequencyList|itemsPerPage:records_per_page" total-items="total_records" style="background:none;">
                                <td>{{dataObj.balance_no}}</td>
                                <td>{{getFrequencyName(dataObj.frequency_id)}}</td>
                                <!-- <td>{{dataObj.measurement_unit}}</td>
                                <td>{{dataObj.standard_value}}</td>
                                <td>{{dataObj.max_value}}</td>
                                <td>{{dataObj.min_value}}</td> -->
                                <td>{{dataObj.modified_by!=null?dataObj.modified_by:dataObj.created_by}}</td>
                                <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                            <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on|format| date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td>
                                    <?php if ($edit) { ?>
                                        <!-- <button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)">Deactivate</button> -->
                                        <a type="button" ng-click="EditFrequency(dataObj)"><i class="far fa-edit text-blue pr10"></i></a>
                                        <!-- <button class="btn btn-sm btn-success" ng-click="EditFrequency(dataObj)">Edit</button> -->
                                    <?php } else { ?>
                                        <!-- <button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)" disabled>Deactivate</button> -->
                                    <?php } ?>

                                </td>
                                <td ng-if="dataObj.status == 'inactive'" class="bg-danger text-white">Inactive</td>
                                <td ng-if="dataObj.status == 'active'" class="bg-success text-white">Active</td>
                            </tr>
                        </tbody>
                    </table>
                    <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getBalanceFrequencyList(newPageNumber)"></dir-pagination-controls>
                </div>
            </div>
        </div>
    </div>	
</div>

<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="<?php echo base_url() ?>assets/js/ngIpAddress.min.js"></script>
<script type="text/javascript">
                                    var app = angular.module("materialApp", ['angular.chosen', 'angularUtils.directives.dirPagination']);
                                    app.directive('chosen', function ($timeout) {
                                        var linker = function (scope, element, attr) {

                                            scope.$watch('balanceCalibrationData', function () {
                                                $timeout(function () {
                                                    element.trigger('chosen:updated');
                                                }, 0, false);
                                            }, true);
                                            $timeout(function () {
                                                element.chosen();
                                            }, 0, false);
                                        };
                                        return {
                                            restrict: 'A',
                                            link: linker
                                        };
                                    });
                                    app.directive('onlyDigits', function () {
                                        return {
                                            require: 'ngModel',
                                            restrict: 'A',
                                            link: function (scope, element, attr, ctrl) {
                                                function inputValue(val) {
                                                    if (val) {
                                                        var digits = val.replace(/[^0-9.]/g, '');

                                                        if (digits.split('.').length > 2) {
                                                            digits = digits.substring(0, digits.length - 1);
                                                        }

                                                        if (digits !== val) {
                                                            ctrl.$setViewValue(digits);
                                                            ctrl.$render();
                                                        }
                                                        return parseFloat(digits);
                                                    }
                                                    return undefined;
                                                }
                                                ctrl.$parsers.push(inputValue);
                                            }
                                        };
                                    });
                                    app.filter('format', function () {
                                        return function (item) {
                                            var t = item.split(/[- :]/);
                                            var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                            var time = d.getTime();
                                            return time;
                                        };
                                    });
                                    app.controller("materialCtrl", function ($scope, $http, $filter) {
                                        $scope.balanceCalibrationData = [];
                                        $scope.active_status = true;
                                        $scope.getBalanceCalibrationList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetBalanceCalibrationList',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.balanceCalibrationData = response.data.balance_calibration_list;
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.getBalanceCalibrationList();
                                        $scope.frequencyData = [];
                                        $scope.getFrequencyList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetFrequencyList',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.frequencyData = response.data.frequency_list;
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");
                                            });
                                        }

                                        $scope.getFrequencyList();
                                        $scope.showAddForm = false;
                                        $scope.showForm = function () {
                                            $scope.showAddForm = true;
                                            $scope.balance_no = "";
                                            $scope.frequency_id = "";
                                        }
                                        $scope.hideForm = function () {
                                            $scope.recordid="";
                                            $scope.showAddForm = false;
                                            $scope.balance_no = "";
                                            $scope.frequency_id = "";
                                            $scope.remark = "";
                                            if ($scope.editmode) {
                                                location.reload();
                                            }
                                        }
                                        $scope.standardData = [];
                                        $scope.getStandardData = function () {
                                            $scope.standardData = [];
                                            $scope.formData = [];
                                            if ($scope.balance_no != "" && $scope.balance_no != null && $scope.balance_no != undefined) {
                                                $scope.standardData = $filter('filter')($scope.balanceCalibrationData, {balance_no: $scope.balance_no})[0].standard_data;
                                                angular.forEach($scope.standardData, function (value, key) {
                                                    delete $scope.standardData[key].created_on;
                                                    delete $scope.standardData[key].balance_calibration_id;
                                                    delete $scope.standardData[key].status;
                                                    $scope.standardData[key]['ischecked'] = false;
                                                });
                                            } else {
                                                alert("Please select the balance_no");
                                                $scope.frequency_id = "";
                                            }
                                        }
                                        $scope.formData = [];
                                        $scope.resetData = function () {
                                            $scope.frequency_id = "";
                                            $scope.standardData = [];
                                            $scope.formData = [];
                                        }

                                        $scope.getCheckedData = function (dataObj) {
                                            if (dataObj.ischecked) {
                                                var temp = [];
                                                if ($scope.formData.length > 0) {
                                                    var temp = $filter('filter')($scope.formData, {id: dataObj.id});
                                                    if (temp.length > 0) {

                                                    } else {
                                                        $scope.formData.push(dataObj);
                                                    }
                                                } else {
                                                    $scope.formData.push(dataObj);
                                                }
                                            } else {
                                                angular.forEach($scope.formData, function (obj, index) {
                                                    if (obj.id == dataObj.id) {
                                                        $scope.formData.splice(index, 1);
                                                    }
                                                });
                                            }
                                            //console.log($scope.formData);
                                        }
                                         $scope.remark = "";
                                        $scope.recordid = "";
                                        $scope.saveFrequencyBalance = function () {
                                            if ($scope.editmode && $scope.remark.trim() == '') {
                                                alert('Remark is required.');
                                                return false;
                                            }
                                             var msg='Save';
                                            if($scope.recordid > 0){
                                                var msg='Update';
                                            }

                                            if (confirm("Do You Want To "+msg+" This Record?")) {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/SaveFrequencyBalance',
                                                    method: "POST",
                                                    data: "balance_no=" + $scope.balance_no + "&frequency_id=" + $scope.frequency_id + "&remark=" + $scope.remark + "&standard_data=" + encodeURIComponent(angular.toJson($scope.formData)) + "&status=" + $scope.active_status+"&recordid="+$scope.recordid,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    alert(response.data.message);
                                                    $scope.recordid="";
                                                    $scope.balance_no = '';
                                                    $scope.frequency_id = "";
                                                    $scope.remark = "";
                                                    $scope.formData = [];
                                                    $scope.standardData = [];
                                                    $scope.getBalanceFrequencyList($scope.page);
                                                    $scope.showAddForm = false;
                                                }, function (response) { // optional
                                                    $scope.balance_no = '';
                                                    $scope.recordid="";
                                                    $scope.frequency_id = "";
                                                    $scope.remark = "";
                                                    $scope.formData = [];
                                                    $scope.standardData = [];
                                                    $scope.getBalanceFrequencyList($scope.page);
                                                    $scope.showAddForm = false;
                                                    console.log("Something went wrong.Please try again");

                                                }
                                                );
                                            }
                                        }

                                        $scope.resetForm = function () {
                                            $scope.standardData = [{"measurement_unit": "KG", "standard_value": "", "max_value": "", "min_value": ""}];
                                        }
                                        $scope.balanceFrequencyList = [];
                                        $scope.page = 1;
                                        $scope.total_records = 0;
                                        $scope.records_per_page = 10;
                                        $scope.getBalanceFrequencyList = function (page) {
                                            $scope.page = page;
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetBalanceFrequencyList?page=' + page,
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                //console.log(response.data.balance_frequency_list);
                                                $scope.total_records = response.data.total_records;
                                                $scope.records_per_page = response.data.records_per_page;
                                                $scope.balanceFrequencyList = response.data.balance_frequency_list;
                                                $scope.balanceFrequencyListAll = response.data.balance_frequency_list_all;
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.getBalanceFrequencyList($scope.page);
                                        $scope.getFrequencyName = function (objId) {
                                            var frequency = $filter('filter')($scope.frequencyData, {id: objId})[0].frequency_name;
                                            return frequency;
                                        }

                                        //Created By : Bhupendra Kumar
                                        //Date : 25 March 2020
                                        //Decription : For Deactivate the Selected Record
                                        $scope.CommonDelete = function (id) {
                                            var tblname = "pts_mst_balance_frequency_data";
                                            var colname = "id";
                                            if (confirm("Do You Want To Deactivate This Record ?")) {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/CommonDelete',
                                                    method: "POST",
                                                    data: "id=" + id + "&tblname=" + tblname + "&colname=" + colname,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    alert(response.data.message);
                                                    $scope.getBalanceFrequencyList($scope.page);
                                                }, function (error) { // optional
                                                    alert(response.data.message);
                                                    //console.log("Something went wrong.Please try again");
                                                });
                                            }
                                        }

                                        $scope.EditFrequency = function ($dataObj) {
                                            $scope.recordid = $dataObj.id;
                                            $scope.editmode = true;
                                            $scope.showAddForm = true;
                                            $scope.balance_no = $dataObj.balance_no;
                                            $scope.frequency_id = $dataObj.frequency_id;
                                            $scope.remark = "";
                                             if ($dataObj.status == 'active') {
                                                $scope.active_status = true;
                                            } else {
                                                $scope.active_status = false;
                                            }
//                                            $scope.active_status = ($dataObj.status == 'active') ? true : false;
                                            $scope.getStandardData();
                                            $scope.listobeselected = [];
                                            angular.forEach($scope.balanceFrequencyListAll, function (obj, index) {
                                                if (obj.balance_no == $scope.balance_no && obj.frequency_id == $scope.frequency_id) {
                                                    $scope.listobeselected.push({"cb_standard_id": obj.calibration_standard});
                                                }
                                            });


                                            angular.forEach($scope.balanceCalibrationData, function (obj, index) {
                                                if (obj.balance_no == $scope.balance_no) {
                                                    for (let i = 0; i < obj.standard_data.length; i++) {
                                                        obj.standard_data[i].checked = false;
                                                        for (let j = 0; j < $scope.listobeselected.length; j++) {

                                                            if (obj.standard_data[i].id == $scope.listobeselected[j].cb_standard_id) {
                                                                obj.standard_data[i].checked = true;
                                                                obj.standard_data[i].ischecked = true;
                                                                $scope.getCheckedData(obj.standard_data[i]);
                                                            }
                                                        }
                                                    }
                                                }

                                            });
                                        }
                                        $scope.toggleSelection = function (event) {
                                            if (event.target.checked == true) {
                                                $scope.active_status = true;
                                            } else {
                                                $scope.active_status = false;
                                            }
                                        };
                                    });
</script>