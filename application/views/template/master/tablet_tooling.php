<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?> 
<div class="container-fluid" ng-app="tabletApp" ng-controller="tabletCtrl" ng-cloak>
    <!-- Page Heading -->
    <div class="content-wrapper">
        <div class="card card-default mt-4">
            <div class="card-body">
                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th class="f18" colspan="5" style="vertical-align:middle">Tablet Tooling Master</th>
                                <th class="text-right f20">
                                    <?php if ($add) { ?>
                                        <button class="btn btn-success" ng-disabled="recordid > 0" ng-click="showForm()">Add (<i class="fas fa-plus"></i>)</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success"  ng-click="showForm()"disabled>Add (+)</button>
                                    <?php } ?>
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="col-md-12" ng-show="showAddForm">
                    <form name="tabletForm" novalidate>
                        <div class="card card-default" >
                            <div class="card-body">
                                <fieldset>
                                    <div class="col-auto">
                                        <div class="form-row">
                                            <div class="col-lg-4 mb-3">
                                                <label>Product Code<span style="color: red">* </span></label>
                                                <select ng-disabled="recordid > 0" class="form-control" tabindex="4" ng-change="GetTabletToolingpuchsetdata()"  name="product_code" data-placeholder="Search Product No." ng-options="dataObj['product_code'] as (dataObj.product_code) for dataObj in productData"  ng-model="product_code" required chosen>
                                                    <option value="">Select Product code</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4 mb-3">
                                                <label>Product Desc.</label><br>
                                                {{product_desc}}
                                            </div>
                                            <div class="col-lg-4 mb-3">                                                
                                                <label >Punch set<span style="color: red">* </span></label>
                                                <select  class="form-control" name="no_of_subset" ng-change="getProductDetail()" ng-model="no_of_subset" required>
                                                    <option value="">Please Select Punch set</option>
                                                    <option ng-repeat="dataObj in punchsetdata" value="{{dataObj.punch_set}}">{{dataObj.punch_set}}</option>
                                                </select>
                                                <!-- <select  name="no_of_subset" data-placeholder="Search Punch set"  ng-model="no_of_subset" ng-option="dataObj['punch_set'] as (dataObj.punch_set) for dataObj in punchsetdata" required chosen>
                                                    <option value="">Select Punch set</option>
                                                </select> -->
                                            </div>
                                            <div class="col-lg-4 mb-3">
                                                <label>Supplier<span style="color: red">* </span></label>
                                                <input class="form-control" id="" type="text" placeholder="Supplier" name="supplier" ng-model="supplier" readonly>
                                            </div>
                                            <div class="col-lg-4 mb-3">
                                                <label>Year<span style="color: red">* </span></label>
                                                <input class="form-control" id="" type="text" placeholder="Year" ng-model="year" name="year" value="{{year| date:'yyyy'}}" readonly>
                                            </div>
                                            <div class="col-lg-4 mb-3">
                                                <label>Shape<span style="color: red">* </span></label>
                                                <input class="form-control" maxlength="20" only-alphawithspace id="" type="text" placeholder="Shape" name="shape" ng-model="shape" required>
                                            </div>
                                            <div class="col-lg-4 mb-3">
                                                <label>Upper Punch QTY/Embossing<span style="color: red">* </span></label>
                                                <div class="row">
                                                    <div class="col-lg-5">
                                                        <input class="form-control" maxlength="20" only-alphanumwithspace id="" type="text" placeholder="" name="upper_punch" ng-model="upper_punch" required readonly="">
                                                        <!-- <select class="form-control" name="upper_punch" ng-model="upper_punch" required>
                                                            <option value="">Please Select</option>
                                                            <option ng-repeat="dataObj in punchSethData" value="{{dataObj.punch_set_code}}">{{dataObj.punch_set_code}}</option>
                                                        </select> -->
                                                    </div>/
                                                    <div class="col-lg-5">
                                                        <input class="form-control" maxlength="20" only-alphanumwithspace id="" type="text" placeholder="" name="upper_embossing" ng-model="upper_embossing" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-3">
                                                <label>Lower Punch QTY/Embossing<span style="color: red">* </span></label>
                                                <div class="row">
                                                    <div class="col-lg-5">
                                                        <input class="form-control" maxlength="20" id="" type="text" placeholder="" name="lower_punch" ng-model="lower_punch" required readonly>
                                                        <!-- <select class="form-control" name="lower_punch" ng-model="lower_punch" required>
                                                            <option value="">Please Select</option>
                                                            <option ng-repeat="dataObj in punchSethData" value="{{dataObj.punch_set_code}}">{{dataObj.punch_set_code}}</option>
                                                        </select> -->
                                                    </div>/
                                                    <div class="col-lg-5">
                                                        <input class="form-control" maxlength="20" only-alphanumwithspace id="" type="text" placeholder="" name="lower_embossing" ng-model="lower_embossing" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 mb-3">
                                                <label>Die Quantity<span style="color: red">* </span></label>
                                                <input class="form-control" maxlength="20" id="" type="text" placeholder="" name="die_quantity" ng-model="die_quantity" required readonly>
                                                <!-- <select class="form-control" name="die_quantity" ng-model="die_quantity" required>
                                                    <option value="">Please Select</option>
                                                    <option ng-repeat="dataObj in punchSethData" value="{{dataObj.punch_set_code}}">{{dataObj.punch_set_code}}</option>
                                                </select> -->
                                            </div> 
                                            <div class="col-lg-4 mb-3">
                                                <label>Machine<span style="color: red">* </span></label>
                                                <input class="form-control" maxlength="25" only-alphanumwithspace id="" type="text" placeholder="Machine" name="machine" ng-model="machine" required>
                                            </div>
                                            <div class="col-lg-4 mb-3">
                                                <label>Remark<span style="color: red" ng-show="editmodeheader">* </span></label>
                                                <textarea class="form-control" aria-label="With textarea" name="remark" ng-model="remark" placeholder="Remark" ng-required="editmodeheader" maxlength="100"></textarea>
                                            </div>
                                            <div class="col-lg-2 mb-3" ng-show="recordid > 0"><label><b>Active/Inactive</b></label>
                                                <label class="switch">
                                                    <input type="checkbox" class="toggle-task" style="margin-top: 13px;" ng-model="active_status" ng-click="toggleSelection($event)">
                                                    <span class="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-sm-12 text-center">
                                <button class="btn btn-success btn-sm" ng-disabled="tabletForm.$invalid" ng-click="saveTabletTooling()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                <button class="btn btn-danger btn-sm" ng-click="hideForm()" type="button">&nbsp;&nbsp;&nbsp;&nbsp; Cancel &nbsp;&nbsp;&nbsp;&nbsp;</button>
                            </div>
                        </div>
                        <!-- END card-->
                    </form>
                </div>
                <div class="form-row" style="margin-top:10px;" ng-show="tabletList.length == 0">
                    <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
                </div>
                <div class="form-row" style="margin-top:10px; padding:10px;" ng-show="tabletList.length > 0 && editmodeheader == false">
                    <h2 class="col-md-12 f18">Tablet Tooling List</h2>
                    <div class="table-responsive noscroll" >
                        <table class="table custom-table" ng-show="tabletList.length > 0">
                            <thead>
                                <tr>
                                    <th>Product Code</th>
                                    <th>Supplier</th>
                                    <th>Upper Punch-Qty./Embossing</th>
                                    <th>Lower Punch-Qty./Embossing</th>
                                    <th>Year</th>
                                    <th>Punch Set</th>
                                    <th>Shape</th>
                                    <th>Machine</th>
                                    <th>Die- Quantity</th>
                                    <th>Last Modified By</th>
                                    <th>Last Modified On</th>
                                    <th>Action</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr dir-paginate="dataObj in tabletList|itemsPerPage:records_per_page" total-items="total_records">
                                    <td>{{dataObj.product_code}}</td>
                                    <td>{{dataObj.supplier}}</td>
                                    <td>{{dataObj.upper_punch}}/{{dataObj.upper_embossing}}</td>
                                    <td>{{dataObj.lower_punch}}/{{dataObj.lower_embossing}}</td>
                                    <td>{{dataObj.year}}</td>
                                    <td>{{dataObj.no_of_subset}}</td>
                                    <td>{{dataObj.shape}}</td>
                                    <td>{{dataObj.machine}}</td>
                                    <td>{{dataObj.die_quantity}}</td>
                                    <td>{{dataObj.modified_by!=NULL?dataObj.modified_by:dataObj.created_by}}</td>
                                    <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                    <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                    <td>
                                        <?php if ($edit) { ?>
                                            <a ng-click="editTablettoolingMaster(dataObj)"><i class="far fa-edit text-blue pr10" title="Edit"></i></a>
                                        <?php } else { ?>
                                            <a type="button" class="btn btn-sm btn-info" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                        <?php } ?>

                                    </td>
                                    <td ng-if="dataObj.status == 'active'"   class="bg-success text-white">Active</td>
                                    <td ng-if="dataObj.status == 'inactive'"   class="bg-danger text-white">Inactive</td>
                                </tr>
                            </tbody>
                        </table>
                        <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getTabletToolingLogList(newPageNumber)"></dir-pagination-controls>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

    <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>js/angular.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
    <script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
    <script src="<?php echo base_url() ?>js/moment-with-locales.js"></script>
    <script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
    <script type="text/javascript">
                                        var app = angular.module("tabletApp", ['angular.chosen', 'angularUtils.directives.dirPagination','moment-picker']);
                                        app.directive('chosen', function ($timeout) {

                                        var linker = function (scope, element, attr) {
                                        scope.$watch('productData', function () {
                                        $timeout(function () {
                                        element.trigger('chosen:updated');
                                        }, 0, false);
                                        }, true);
                                        $timeout(function () {
                                        element.chosen();
                                        }, 0, false);
                                        };
                                        return {
                                        restrict: 'A',
                                                link: linker
                                        };
                                        });
                                        app.directive('onlyDigits', function () {
                                        return {
                                        require: 'ngModel',
                                                restrict: 'A',
                                                link: function (scope, element, attr, ctrl) {
                                                function inputValue(val) {
                                                if (val) {
                                                var digits = val.replace(/[^0-9.]/g, '');
                                                if (digits.split('.').length > 2) {
                                                digits = digits.substring(0, digits.length - 1);
                                                }

                                                if (digits !== val) {
                                                ctrl.$setViewValue(digits);
                                                ctrl.$render();
                                                }
                                                return parseFloat(digits);
                                                }
                                                return undefined;
                                                }
                                                ctrl.$parsers.push(inputValue);
                                                }
                                        };
                                        });
                                        app.directive('onlyAlphawithspace', function() {
                                        return {
                                        require: 'ngModel',
                                                link: function(scope, element, attr, ngModelCtrl) {
                                                function fromUser(text) {
                                                var transformedInput = text.replace(/[^a-zA-Z\s]/g, '');
//                                                console.log(transformedInput);
                                                if (transformedInput !== text) {
                                                ngModelCtrl.$setViewValue(transformedInput);
                                                ngModelCtrl.$render();
                                                }
                                                return transformedInput; // or return Number(transformedInput)
                                                }
                                                ngModelCtrl.$parsers.push(fromUser);
                                                }
                                        };
                                        });
                                        app.directive('onlyAlphanumwithspace', function() {
                                        return {
                                        require: 'ngModel',
                                                link: function(scope, element, attr, ngModelCtrl) {
                                                function fromUser(text) {
                                                var transformedInput = text.replace(/[^0-9a-zA-Z\s]/g, '');
//                                                console.log(transformedInput);
                                                if (transformedInput !== text) {
                                                ngModelCtrl.$setViewValue(transformedInput);
                                                ngModelCtrl.$render();
                                                }
                                                return transformedInput; // or return Number(transformedInput)
                                                }
                                                ngModelCtrl.$parsers.push(fromUser);
                                                }
                                        };
                                        });
                                        app.filter('format', function () {
                                        return function (item) {
                                        var t = item.split(/[- :]/);
                                        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                        var time = d.getTime();
                                        return time;
                                        };
                                        });
                                        app.filter('capitalizeWord', function () {

                                        return function (text) {

                                        return (!!text) ? text.charAt(0).toUpperCase() + text.substr(1).toLowerCase() : '';
                                        }

                                        });
                                        app.controller("tabletCtrl", function ($scope, $http, $filter) {
                                        $scope.punchsetdata = [];
                                        $scope.showAddForm = false;
                                        $scope.product_desc = "";
                                        $scope.no_of_subset = "";
                                        $scope.supplier = "";
                                        $scope.year = "";
                                        $scope.punchSethData = [];
                                        $scope.punch_subset_data = [];
                                        $scope.showForm = function () {
                                        $scope.showAddForm = true;
                                        }
                                        $scope.productData = [];
                                        $scope.getProductList = function () {
                                        $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetPunchSetAllocationRecord',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                        $scope.productData = response.data.punch_set_data;
//                                        console.log($scope.productData);
                                        }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                        });
                                        }
                                        $scope.getProductList();
                                        $scope.punchsetdata = [];
                                        $scope.GetTabletToolingpuchsetdata = function () {
                                        $scope.punchsetdata = [];
                                        $scope.product_desc = $filter('filter')($scope.productData, {product_code: $scope.product_code})[0].product_name;
                                        $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetTabletToolingpuchsetdata2?product_code=' + $scope.product_code,
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                        $scope.punchsetdata = response.data.punch_set_data;
//                                        console.log($scope.punchsetdata);
                                        }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                        });
                                        }



                                        $scope.page = 1;
                                        $scope.total_records = 0;
                                        $scope.records_per_page = 10;
                                        $scope.tabletList = [];
                                        $scope.getTabletToolingLogList = function (page) {
                                        $scope.page = page;
                                        $http({
                                        url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/GetTabletToolingLogList?module=master&page=' + page,
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                        $scope.tabletList = response.data.tablet_list;
                                        }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                        });
                                        }

                                        $scope.getTabletToolingLogList($scope.page);
                                        $scope.recordid = "";
                                        $scope.remark = "";
                                        $scope.product_desc = "";
                                        //$scope.no_of_subset = "";
                                        $scope.supplier = "";
                                        $scope.year = "";
                                        $scope.shape = "";
                                        $scope.upper_punch = "";
                                        $scope.upper_embossing = "";
                                        $scope.lower_punch = "";
                                        $scope.lower_embossing = "";
                                        $scope.die_quantity = "";
                                        $scope.machine = "";
                                        $scope.active_status = false;
                                        $scope.editmodeheader = false;
                                        $scope.hideForm = function () {
                                        $scope.recordid = "";
                                        $scope.editmodeheader = false;
                                        $scope.remark = "";
                                        $scope.product_desc = "";
                                        $scope.no_of_subset = "";
                                        $scope.supplier = "";
                                        $scope.year = "";
                                        $scope.shape = "";
                                        $scope.upper_punch = "";
                                        $scope.upper_embossing = "";
                                        $scope.lower_punch = "";
                                        $scope.lower_embossing = "";
                                        $scope.die_quantity = "";
                                        $scope.machine = "";
                                        $scope.showAddForm = false;
                                        $scope.punchsetdata = [];
                                        $scope.punchSethData = [];
                                        $scope.punch_subset_data = [];
                                        }

                                        $scope.saveTabletTooling = function () {
                                        var msg = 'Save';
                                        if ($scope.recordid > 0) {
                                        var msg = 'Update';
                                        }

                                        if (confirm("Do You Want To " + msg + " This Record?")) {
                                        $http({
                                        url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/AddTabletTooling',
                                                method: "POST",
                                                data: "product_code=" + $scope.product_code + "&no_of_subset=" + $scope.no_of_subset + "&supplier=" + $scope.supplier + "&year=" + $scope.year + "&shape=" + $scope.shape + "&upper_punch=" + $scope.upper_punch + "&upper_embossing=" + $scope.upper_embossing + "&lower_punch=" + $scope.lower_punch + "&lower_embossing=" + $scope.lower_embossing + "&die_quantity=" + $scope.die_quantity + "&machine=" + $scope.machine + "&remark=" + $scope.remark + "&recordid=" + $scope.recordid + "&status=" + $scope.active_status + "&remark=" + $scope.remark,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        })
                                                .then(function (response) {
                                                alert(response.data.message);
                                                $scope.hideForm();
                                                $scope.getTabletToolingLogList($scope.page);
                                                },
                                                        function (response) { // optional
                                                        alert(response.data.message);
                                                        $scope.hideForm();
                                                        console.log("Something went wrong.Please try again");
                                                        $scope.getTabletToolingLogList($scope.page);
                                                        });
                                        }
                                        }

                                        $scope.editTablettoolingMaster = function (dataObj) {
//                                        console.log(dataObj);
                                        //     $scope.no_of_subset = "";
                                        //     $scope.punchsetdata = [];
                                        //     $scope.punchSethData = [];
                                        // $scope.punch_subset_data = [];
                                        $scope.showAddForm = false;
                                        $scope.recordid = dataObj.id;
                                        $scope.editmodeheader = true;
                                        $scope.product_code = dataObj.product_code;
                                        $scope.GetTabletToolingpuchsetdata();
                                        $scope.no_of_subset = dataObj.no_of_subset;
                                        //alert(dataObj.no_of_subset);
                                        //$scope.getProductDetail(dataObj.no_of_subset);
                                        $scope.remark = "";
                                        $scope.supplier = dataObj.supplier;
                                        $scope.year = dataObj.year;
                                        $scope.shape = dataObj.shape;
                                        $scope.upper_punch = dataObj.upper_punch;
                                        $scope.upper_embossing = dataObj.upper_embossing;
                                        $scope.lower_punch = dataObj.lower_punch;
                                        $scope.lower_embossing = dataObj.lower_embossing;
                                        $scope.die_quantity = dataObj.die_quantity;
                                        $scope.machine = dataObj.machine;
                                        $scope.showAddForm = true;
                                        if (dataObj.status == 'active') {
                                        $scope.active_status = true;
                                        } else {
                                        $scope.active_status = false;
                                        }

                                        }

                                        $scope.toggleSelection = function toggleSelection(event) {
                                        if (event.target.checked == true) {
                                        $scope.active_status = true;
                                        } else {
                                        $scope.active_status = false;
                                        }
                                        };
                                        $scope.getProductDetail = function () {
                                        $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetTabletToolingpuchsubset_for_mst?punch_set=' + $scope.no_of_subset + "&product_code=" + $scope.product_code,
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                        $scope.punchSethData = response.data.punch_subset_data;
                                        $scope.upper_punch = $scope.punchSethData.length;
                                        $scope.lower_punch = $scope.punchSethData.length;
                                        $scope.die_quantity = $scope.punchSethData.length;
                                        $scope.supplier = response.data.punch_subset_data[0]['supplier'];
                                        $scope.year = moment(response.data.punch_subset_data[0]['dom']).format('YYYY');
//                                        console.log($scope.punchSethData);
                                        }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                        });
                                        }

                                        });
                                        $(document).ready(function () {
                                        $('.cc').bind('keyup paste', function () {
                                        this.value = this.value.replace(/[^0-9]/g, '');
                                        });
                                        });
    </script>