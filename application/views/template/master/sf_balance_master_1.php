<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
    <!-- Page Heading -->

    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row">

                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">    	   
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th colspan="5" class="f18" style="vertical-align:middle">Balance Master</th>
                                <th class="text-right">
                                    <?php if ($add) { ?>
                                        <button class="btn btn-success" ng-disabled="balance_id > 0" ng-click="showForm()">Add (+)</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success" ng-click="showForm()" disabled>Add (+)</button>
                                    <?php } ?>

                                </th>
                            </tr>
                        </thead>
                    </table>  
                </div>
                <div class="col-12">
                    <form name="balanceForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row"> 
                                        <div class="col-lg-4 mb-3"><label><b>Select Room Code</b><span style="color: red">*</span></label>
                                            <select ng-disabled="balance_id > 0" class="form-control" tabindex="4" name="room_id" ng-change="getRoomCode(room_id)" data-placeholder="Search Room Code" ng-options="dataObj['room_id'] as (dataObj.room_code +       ' => ' +       dataObj.room_name +       ' => ' +       dataObj.area_id) for dataObj in sysRoomData"  ng-model="room_id" required chosen>
                                                <option value="">Select Room Name</option>
                                            </select>
                                        </div>
                                    </div>


                                </div> 
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="form-row">
                                            <div class="col-lg-3 mb-3"><label>Balance No <span style="color: red">*</span></label>
                                                <input ng-disabled="balance_id > 0" type="text" class="form-control" ng-change="CommonCheckIsExist(balance_no)" name="balance_no" ng-model="balance_no" required>
                                            </div>
                                            <div class="col-lg-3 mb-3"><label>Capacity <span style="color: red">*</span></label>
                                                <input ng-disabled="balance_id > 0" type="text" class="form-control" name="capacity" only-digits ng-model="capacity" required>
                                            </div>
                                            <div class="col-lg-2 mb-2"><label>UOM(KG/GM)</label>
                                                <select ng-disabled="balance_id > 0" class="form-control" name="UOM" ng-model="UOM">
                                                    <option value="KG">KG</option>
                                                    <option value="GM">GM</option>
                                                </select>

                                            </div>
                                            <div class="col-lg-2 mb-2"><label>Least Count <span style="color: red">*</span></label>
                                                <input ng-disabled="balance_id > 0" type="text" class="form-control" name="least_count" only-digits ng-model="least_count" required>
                                            </div>                                        
                                        </div>
                                        <div class="form-row">
                                            <div class="col-lg-12 mb-3"><label><span style="color: red">{{msg}}</span></label>
                                            </div>
                                        </div>
                                        <div class="form-row" ng-repeat="dataObj in standardData"> 
    <!--                                        <div class="col-lg-2 mb-3"><label>Select Frequency<span style="color: red">*</span></label>
                                                <select class="form-control" name="frequency_id" ng-model="dataObj.frequency_id" required>
                                                    <option value="">select Frequency</option>
                                                    <option value="{{dtaObj.id}}" ng-repeat="dtaObj in frequencyData">{{dtaObj.frequency_name}}</option>
                                                </select>
                                            </div>-->

                                            <div class="col-lg-2 mb-3"><label><small>Standard WT.(KG/GM)</small><span style="color: red">*</span></label>
                                                <input type="text" ng-disabled="balance_id > 0" class="form-control" name="standard_value" ng-model="dataObj.standard_value" ng-blur="chkstd_WT_validation($index)" only-digits required>
                                            </div>
                                            <div class="col-lg-2 mb-3"><label><small>Measurement(KG/GM)</small></label>
                                                <select ng-disabled="balance_id > 0" class="form-control" name="measurement_unit" ng-model="dataObj.measurement_unit">
                                                    <option value="KG">KG</option>
                                                    <option value="GM">GM</option>
                                                </select>

                                            </div>

                                            <div class="col-lg-3 mb-3"><label><small>Upper Limit WT.(KG/GM)</small><span style="color: red">*</span></label>
                                                <input type="text" class="form-control" name="max_value" ng-model="dataObj.max_value" ng-blur="chkstd_WT_validation($index)" only-digits required>
                                            </div>
                                            <div class="col-lg-3 mb-3"><label><small>Lower Limit WT.(KG/GM) </small><span style="color: red">*</span></label>
                                                <input type="text" class="form-control" name="min_value" ng-model="dataObj.min_value" ng-blur="chkstd_WT_validation($index)" only-digits required>
                                            </div>
                                            <div class="col-lg-2 " style="margin-top: 25px;">

                                                <button class="btn btn-xs btn-danger" ng-if="standardData.length > 1" ng-disabled="balance_id > 0" ng-click="Remove($index)" title="Remove"><i class="fa fa-times" aria-hidden="true"></i></button><!-- end ngIf: reportField[0].length > 1 -->
                                                &nbsp;
                                                <button class="btn btn-xs btn-success" ng-show="showAddButton($index)" ng-disabled="balance_id > 0" ng-click="Add($index)" title="Add"><i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-lg-4 mb-3" ng-show="balance_id > 0"><label><b>Remark</b><span style="color: red">*</span></label>
                                            <textarea class="form-control" name="remark" maxlength="150" ng-model="remark" required></textarea>
                                        </div>
                                        <div class="col-lg-2 mb-3" ng-show="balance_id > 0"><label><b>Active/Inactive</b></label>
                                            <label class="switch">
                                                <input type="checkbox" style="margin-top: 13px;" ng-model="active_status" ng-click="toggleSelection($event)">
                                                <span class="slider round"></span>

                                        </div>

                                    </div>


                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-sm btn-success" ng-disabled="balanceForm.$invalid || msg != ''"  ng-click="saveRoomBalance()" type="submit">Submit</button>
                                        <button ng-click="resetForm()" class="btn btn-sm btn-danger"  type="button">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="form-row" ng-show="balanceCalibrationData.length == 0">
                <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
                <div class="col-lg-12 text-center" ><h4>(Please Select the above parameters)</h4></div> 
            </div>
            <div class="form-row" style="margin-top:10px;" ng-show="balanceCalibrationData.length > 0">   
                <h2 class="f18">Balance Master List</h2>
                <div class="table-responsive noscroll" >
                    <table  class="table custom-table">
                        <thead >
                            <tr>
                                <th>Room Code</th>
                                <th>Balance No</th>
                                <th>Capacity</th>
                                <th>Measurement Unit</th>
                                <th>Least Count</th>
                                <th>Last Modified By</th>
                                <th>Last Modified On</th>
                                <th>Action</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="dataObj in balanceCalibrationData">
                                <td>{{dataObj.room_code}}</td>
                                <td>{{dataObj.balance_no}}</td>
                                <td>{{getMeasuremenyUnit(dataObj)[0]}}</td>
                                <td>{{getMeasuremenyUnit(dataObj)[1]}}</td>
                                <td>{{dataObj.least_count}}</td>
                                <td>{{dataObj.modified_by!=null?dataObj.modified_by:dataObj.created_by}}</td>
                                <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td>
                                    <?php if ($edit) { ?>
                                        <a type="button" ng-click="editBalanceMaster(dataObj)"><i class="far fa-edit text-blue pr10"></i></a>
                                        <!--<button class="btn btn-sm btn-info" ng-click="editBalanceMaster(dataObj)">Edit</button>-->
                                        <!--<button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)">Deactivate</button>-->
                                    <?php } else { ?>
                                        <a type="button" class="btn btn-sm btn-info" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                        <!--<button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)" disabled>Deactivate</button>-->
                                    <?php } ?>
                                </td>
                                <td ng-if="dataObj.status == 'active'"   class="bg-success text-white">{{dataObj.status| capitalizeWord}}</td>
                                <td ng-if="dataObj.status == 'inactive'" class="bg-danger text-white">{{dataObj.status| capitalizeWord}}</td>
                            </tr>
                        </tbody>
                    </table> 
                    <table ng-show="total_records>0"><tr>
                        <td ng-repeat="dataObj1 in page_array">
                            <div ng-click="getBalanceCalibrationList(dataObj1)">{{dataObj1}}</div>
                        </td></tr>
                    </table>
                </div>

            </div>
        </div>
    </div>	

</div>

<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>

<script type="text/javascript">
                                    var app = angular.module("materialApp", ['angular.chosen']);
                                    app.directive('chosen', function ($timeout) {

                                        var linker = function (scope, element, attr) {

                                            scope.$watch('sysRoomData', function () {
                                                $timeout(function () {
                                                    element.trigger('chosen:updated');
                                                }, 0, false);
                                            }, true);
                                            scope.$watch('activityList', function () {
                                                $timeout(function () {
                                                    element.trigger('chosen:updated');
                                                }, 0, false);
                                            }, true);
                                            $timeout(function () {
                                                element.chosen();
                                            }, 0, false);
                                        };
                                        return {
                                            restrict: 'A',
                                            link: linker
                                        };
                                    });
                                    app.directive('onlyDigits', function () {
                                        return {
                                            require: '?ngModel',
                                            link: function (scope, element, attrs, ngModelCtrl) {
                                                if (!ngModelCtrl) {
                                                    return;
                                                }

                                                ngModelCtrl.$parsers.push(function (val) {
                                                    if (angular.isUndefined(val)) {
                                                        var val = '';
                                                    }

                                                    var clean = val.replace(/[^-0-9\.]/g, '');
                                                    var negativeCheck = clean.split('-');
                                                    var decimalCheck = clean.split('.');
                                                    if (!angular.isUndefined(negativeCheck[1])) {
                                                        negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                                                        clean = negativeCheck[0] + '-' + negativeCheck[1];
                                                        if (negativeCheck[0].length > 0) {
                                                            clean = negativeCheck[0];
                                                        }

                                                    }

                                                    if (!angular.isUndefined(decimalCheck[1])) {
                                                        decimalCheck[1] = decimalCheck[1].slice(0, 4);
                                                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                                                    }

                                                    if (val !== clean) {
                                                        ngModelCtrl.$setViewValue(clean);
                                                        ngModelCtrl.$render();
                                                    }
                                                    return clean;
                                                });

                                                element.bind('keypress', function (event) {
                                                    if (event.keyCode === 32) {
                                                        event.preventDefault();
                                                    }
                                                });
                                            }
                                        };
                                    });
                                    // app.directive('onlyDigits', function () {
                                    //     return {
                                    //         require: 'ngModel',
                                    //         restrict: 'A',
                                    //         link: function (scope, element, attr, ctrl) {
                                    //             function inputValue(val) {
                                    //                 if (val) {
                                    //                     var digits = val.replace(/[^0-9.]/g, '');

                                    //                     if (digits.split('.').length > 2) {
                                    //                         digits = digits.substring(0, digits.length - 1);
                                    //                     }

                                    //                     if (digits !== val) {
                                    //                         ctrl.$setViewValue(digits);
                                    //                         ctrl.$render();
                                    //                     }
                                    //                     return parseFloat(digits);
                                    //                 }
                                    //                 return undefined;
                                    //             }
                                    //             ctrl.$parsers.push(inputValue);
                                    //         }
                                    //     };
                                    // });
                                    app.filter('capitalizeWord', function () {

                                        return function (text) {

                                            return (!!text) ? text.charAt(0).toUpperCase() + text.substr(1).toLowerCase() : '';

                                        }

                                    });
                                    app.filter('format', function () {
                                        return function (item) {
                                            var t = item.split(/[- :]/);
                                            var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                            var time = d.getTime();
                                            return time;
                                        };
                                    });
                                    app.controller("materialCtrl", function ($scope, $http, $filter) {
                                        $scope.sysRoomData = [];
                                        $scope.page = 1;
                                        $scope.total_records = 0;
                                        $scope.records_per_page = 10;
                                        $scope.page_array = [];
                                        $scope.getDeviceRoomList = function (page) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.sysRoomData = response.data.room_list;
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");

                                            });
                                        }
                                        $scope.getDeviceRoomList(0);

                                        $scope.showAddForm = false;
                                        $scope.showForm = function () {
                                            $scope.showAddForm = true;
                                        }
                                        $scope.getRoomCode = function (objId) {
                                            $scope.room_code = $filter('filter')($scope.sysRoomData, {room_id: $scope.room_id})[0].room_code;
                                        }

                                        $scope.page = 1;
                                        $scope.total_records = 0;
                                        $scope.records_per_page = 10;//default assigned 10 so that no division error
                                        $scope.balanceCalibrationData = [];
                                        $scope.getBalanceCalibrationList = function (page) {
                                            $scope.page = page;
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetBalanceCalibrationList?module=master&page='+$scope.page,
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.balanceCalibrationData = response.data.balance_calibration_list;
                                                $scope.total_records = response.data.total_records;
                                                $scope.records_per_page = response.data.records_per_page;
                                                $scope.createPaginateTable($scope.total_records,$scope.records_per_page);
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");

                                            });
                                            //$scope.createPaginateTable($scope.total_records,$scope.records_per_page);
                                        }
                                        $scope.getBalanceCalibrationList(1);
                                        $scope.createPaginateTable = function(total_records,records_per_page){
                                           $scope.page_array =[];
                                           var paginate_total_length = Math.ceil(total_records/records_per_page);
                                           for(var i=0; i<paginate_total_length;i++){
                                               $scope.page_array.push(i+1);
                                           }
                                           return $scope.page_array;
                                        }
                                        //$scope.createPaginateTable($scope.total_records,$scope.records_per_page);
                                        
                                        $scope.standardData = [{"measurement_unit": "KG", "standard_value": "", "max_value": "", "min_value": ""}];
                                        $scope.msg = "";
                                        $scope.Add = function (index) {
                                            if (parseFloat($scope.standardData[index]['min_value']) < parseFloat($scope.standardData[index]['standard_value']) && parseFloat($scope.standardData[index]['standard_value']) < parseFloat($scope.standardData[index]['max_value']))
                                            {
                                                const obj = {"measurement_unit": "KG", "standard_value": "", "max_value": "", "min_value": ""};
                                                // for (const key of Object.keys($scope.standardData[index])) {
                                                // obj[key] = null;
                                                // }
                                                $scope.standardData.push(obj);
                                                console.log($scope.standardData);
                                                $scope.msg = "";

                                            } else
                                            {
                                                $scope.msg = "Standard WT. should be greater than Lower limit and Upper limit should be greater than Standard WT.";
                                                return false
                                            }
                                        };
                                        $scope.Remove = function (index) {
                                            //Remove the item from Array using Index.
                                            $scope.standardData.splice(index, 1);
                                        }
                                        $scope.chkstd_WT_validation = function (index) {
                                            if (parseFloat($scope.standardData[index]['min_value']) < parseFloat($scope.standardData[index]['standard_value']) && parseFloat($scope.standardData[index]['standard_value']) < parseFloat($scope.standardData[index]['max_value']))
                                            {
                                                $scope.msg = "";
                                            } else
                                            {
                                                $scope.msg = "Standard WT. should be greater than Lower limit and Upper limit should be greater than Standard WT.";
                                            }
                                        };
                                        $scope.showAddButton = function (index) {
                                            return index === $scope.standardData.length - 1;
                                        };
                                        $scope.frequencyData = [];
                                        $scope.getFrequencyList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetFrequencyList',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.frequencyData = response.data.frequency_list;
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");

                                            });
                                        }
                                        $scope.getFrequencyList();

                                        $scope.saveRoomBalance = function () {
                                            if (confirm("Do You Want To Save This Record?")) {
                                                if ($scope.UOM == '' || $scope.UOM == undefined || $scope.UOM == null)
                                                {
                                                    alert("Please Select Unit of Measurement...!");
                                                    return false;
                                                } else {
                                                    $scope.capacity = $scope.capacity + $scope.UOM;
                                                }
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/AddRoomBalanceCalibration',
                                                    method: "POST",

                                                    data: "capacity=" + $scope.capacity + "&least_count=" + $scope.least_count + "&balance_no=" + $scope.balance_no + "&standard_data=" + encodeURIComponent(angular.toJson($scope.standardData)) + "&balance_id=" + $scope.balance_id + "&status=" + $scope.active_status + "&remark=" + $scope.remark + "&room_id=" + $scope.room_id + "&room_code=" + $scope.room_code,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                })
                                                        .then(function (response) {
                                                            alert(response.data.message);
                                                            $scope.capacity = '';
                                                            $scope.least_count = '';
                                                            $scope.balance_no = '';
                                                            $scope.balance_id = "";
                                                            $scope.room_id = "";
                                                            $scope.room_code = ""
                                                            $scope.standardData = [{"measurement_unit": "KG", "standard_value": "", "max_value": "", "min_value": ""}];
                                                            $scope.showAddForm = false;
                                                            $scope.getBalanceCalibrationList(1);
                                                        },
                                                                function (response) { // optional
                                                                    $scope.showAddForm = false;
                                                                    $scope.balance_id = "";
                                                                    $scope.getBalanceCalibrationList(1);
                                                                    console.log("Something went wrong.Please try again");

                                                                });
                                            }
                                        }
                                        $scope.balance_id = "";
                                        $scope.status = "";
                                        $scope.active_status = false;
                                        $scope.remark = "Null";
                                        $scope.toggleSelection = function toggleSelection(event) {
                                            if (event.target.checked == true) {
                                                $scope.active_status = true;
                                            } else {
                                                $scope.active_status = false;
                                            }
                                        }
                                        $scope.editBalanceMaster = function (dataObj) {
                                            //console.log(dataObj);
                                            $http({
                                                url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/checkParentStatus',
                                                method: "POST",
                                                data: "field_name=room_id&field_value=" + dataObj.room_id + "&table_name=pts_mst_sys_id",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                if (response.data.parent_data[0].status == 'inactive') {
                                                    $scope.showAddForm = false;
                                                    alert("Room is inactive.First activate this Room through Room Configuration.");
                                                } else {
                                                    $scope.showAddForm = true;
                                                    $scope.room_id = dataObj.room_id;
                                                    $scope.msg = "";
                                                    $scope.remark = "";
                                                    $scope.balance_id = dataObj.id;
                                                    $scope.balance_no = dataObj.balance_no;
                                                    $scope.capacity = dataObj.capacity.substring(0, dataObj.capacity.length - 2);
                                                    $scope.UOM = dataObj.capacity.substring(dataObj.capacity.length - 2);
                                                    $scope.least_count = dataObj.least_count;
                                                    if (dataObj.standard_data.length > 0) {
                                                        $scope.standardData = dataObj.standard_data;
                                                    }
                                                    $scope.getRoomCode(dataObj.room_id);
                                                    if (dataObj.status == 'active') {
                                                        $scope.active_status = true;
                                                    } else {
                                                        $scope.active_status = false;
                                                    }
                                                }
                                            }, function (error) { // optional
                                            });

                                        }

                                        $scope.resetForm = function () {
                                            $scope.remark = "Null";
                                            $scope.status = "";
                                            $scope.recordid = "";
                                            $scope.capacity = '';
                                            $scope.room_id = "";
                                            $scope.room_code = ""
                                            $scope.least_count = '';
                                            $scope.balance_no = '';
                                            $scope.balance_id = "";
                                            $scope.standardData = [{"measurement_unit": "KG", "standard_value": "", "max_value": "", "min_value": ""}];
                                            $scope.showAddForm = false;
                                        }

                                        //Created By : Bhupendra Kumar
                                        //Date : 18 March 2020
                                        //Decription : For Deactivate the Selected Record
                                        $scope.CommonDelete = function (id) {
                                            var tblname = "pts_mst_balance_calibration";
                                            var colname = "id";
                                            if (confirm("Do You Want To Deactivate This Record ?")) {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/CommonDelete',
                                                    method: "POST",
                                                    data: "id=" + id + "&tblname=" + tblname + "&colname=" + colname,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    alert(response.data.message);
                                                    $scope.getBalanceCalibrationList(1);
                                                }, function (error) { // optional
                                                    alert(response.data.message);
                                                    //console.log("Something went wrong.Please try again");
                                                });
                                            }
                                        }

                                        //created By : Bhupendra kumar
                                        //Date : 6th April 2020
                                        //Description : to check the record is already exist or not on given parameter
                                        $scope.CommonCheckIsExist = function (code) {
                                            var tblname = "pts_mst_balance_calibration";
                                            var col = "balance_no";
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/chkcode',
                                                method: "POST",
                                                data: "col=" + col + "&tblname=" + tblname + "&code=" + code,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (res) {
                                                console.log(res);
                                                if (res.data.status == 1) {
                                                    alert("Record already exists at this balance no.");
                                                    $scope.balance_no = "";
                                                }
                                            }, function (error) { // optional
                                            });
                                        }
                                        $scope.getMeasuremenyUnit = function (dataObj) {
                                            var capacity = dataObj.capacity.substring(0, dataObj.capacity.length - 2);
                                            var UOM = dataObj.capacity.substring(dataObj.capacity.length - 2);
                                            var temp = [capacity, UOM];
                                            return temp;
                                        }
                                    });
</script>