<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">  
        <div class="copyright text-center my-auto">
            <span>SMHS. &copy; 2020 - 2021. All Rights Reserved.</span>
        </div>
    </div>
</footer>
<!-- End of Footer --> 

<!-- Page level plugins -->
<!-- Custom scripts for all pages-->

<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 


<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<script type="text/javascript">
                if (navigator.onLine == true) {
                    var d = new Date($.now());
                    var up_time = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                    var down_time = localStorage.getItem("down_time");
                    var current_url = localStorage.getItem("current_url");
                    var msg = localStorage.getItem("msg");
                    if (msg != '') {
                        var formData = {url: current_url, msg: msg, up_time: up_time, down_time: down_time, record_type: 'master'};
                        $.ajax({
                            url: "<?php echo base_url() ?>/Networkcontroller/captureNetworkStatus",
                            type: "POST",
                            data: formData,
                            success: function (data, textStatus, jqXHR)
                            {
                                localStorage.setItem("url", "");
                                localStorage.setItem("msg", "");
                            },
                            error: function (jqXHR, textStatus, errorThrown)
                            {
                                console.log("Something went wrong.Please try again");
                            }
                        });
                    }

                } else {
                    alert("Network Error");
                    var d = new Date($.now());
                    var down_time = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + " " + d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                    localStorage.setItem("msg", "Network Error");
                    localStorage.setItem("down_time", down_time);
                    var current_url = window.location.href;
                    localStorage.setItem("current_url", current_url);

                }
            </script>
