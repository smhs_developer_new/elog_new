<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="productApp" ng-controller="productCtrl" ng-cloak>
    <!-- Page Heading -->
    <div class="content-wrapper">
        <div class="card card-default mt-4">
            <div class="card-body">                     
                <div class="form-row">
                    <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">
                        <table class="table table-dark">
                            <thead>
                                <tr>
                                    <th colspan="5" class="f18" style="vertical-align:middle">Product Master</th>
                                    <th class="text-right">
                                        <?php if ($add) { ?>
                                            <button class="btn btn-success" ng-disabled="recordid > 0" ng-click="showForm()">Add (<i class="fas fa-plus"></i>)</button>
                                        <?php } else { ?>
                                            <button class="btn btn-success"  ng-click="showForm()"disabled>Add (+)</button>
                                        <?php } ?>

                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>

                     <div class="col-md-12">
                        <form name="materialForm" novalidate="" class="ng-pristine ng-valid ng-valid-required">
                            <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                        <div class="col-md-12">
                        <div class="form-row">
                            <div class="col-lg-4 mb-3"><label >Product code :<span style="color: red">*</span></label>
                                <input ng-disabled="recordid > 0" class="form-control" type="text" name="pcode" id="pcode" maxlength="20" ng-model="pcode" required>                                
                            </div>
                            <div class="col-lg-8 mb-3"><label >Product Name :<span style="color: red">*</span></label>
                                <input class="form-control" type="text" name="pname" id="pname" maxlength="50" ng-model="pname" required>                                
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-lg-4 mb-3"><label>Market :<span style="color: red">*</span></label>
                                <input class="form-control"  id="market" name="market" type="text" ng-model="market" required>                                
                            </div>                      
                            <div class="col-lg-8 mb-3"><label>Remark<span style="color: red" ng-show="editmodeheader">*</span> :</label>
                                <input class="form-control"  id="remark" name="remark" ng-model="remark" type="text" ng-required="editmodeheader">                               
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-lg-2 mb-3" ng-show="recordid > 0"><label><b>Active/Inactive</b></label>
                            <label class="switch">
                                <input type="checkbox" style="margin-top: 13px;" ng-model="active_status" ng-click="toggleSelection($event)">
                                <span class="slider round"></span>
                            </label>
                        </div>
                        </div>
                        <div class="row mb-4">
                        <div class="col-sm-12 text-center">
                            <button class="btn btn-success btn-sm" ng-show="recordid == ''" ng-disabled="materialForm.$invalid" ng-click="saveProduct()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                            <button class="btn btn-success btn-sm" ng-show="recordid > 0" ng-disabled="materialForm.$invalid" ng-click="updateProduct()" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                            <button class="btn btn-danger btn-sm" ng-click="hideForm()" type="button">&nbsp;&nbsp;&nbsp;&nbsp; Cancel &nbsp;&nbsp;&nbsp;&nbsp;</button>
                        </div>
                        </div>

                    </div>

                </div>
            </div>
            </form>
            </div>
            <!-- Product Listing -->

                                        <div class="col-md-12">
                                <div class="form-row" style="margin-top:10px; padding:5px;">
                                    <h2 class="col-md-12 f18">Product Master List</h2>
                                    <table class="table custom-table">
                                        <thead>
                                            <tr>
                                                <th>Product Code</th>
                                                <th>Product Name</th>
                                                <th>Market</th>
                                                <th>Remark</th>
                                                <th>Created By</th>
                                                <th>Created On</th>
                                                <th>Action</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!-- ngRepeat: dataObj in roomActivityData -->
                                            <tr dir-paginate="dataObj in productData|itemsPerPage:records_per_page" total-items="total_records" style="background:none;">
                                                <td>{{dataObj.product_code}}</td>
                                                <td>{{dataObj.product_name}}</td>
                                                <td>{{dataObj.product_market}}</td>
                                                <td>{{dataObj.product_remark}}</td>
                                                <td>{{dataObj.modified_by!=NULL?dataObj.modified_by:dataObj.created_by}}</td>
                                                <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                                <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                                <td>
                                                    <?php if ($edit) { ?>
                                                        <a ng-click="editProduct(dataObj)"><i class="far fa-edit text-blue pr10" title="Edit"></i></a>
                                                    <?php } else { ?>
                                                        <a type="button" class="btn btn-sm btn-info" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                                    <?php } ?>
                                                </td>
                                                <td ng-if="dataObj.is_active == '1'"   class="bg-success text-white">Active</td>
                                                <td ng-if="dataObj.is_active == '0'"   class="bg-danger text-white">Inactive</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getproductList(newPageNumber)"></dir-pagination-controls>
                                </div>
                            </div>

            <!-- End of product listing -->


                </div>
            </div>
        </div>

       
    </div>

</div>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/ngIpAddress.min.js"></script>
<script type="text/javascript">
                                                var app = angular.module("productApp", ['angular.chosen','angularUtils.directives.dirPagination']);
                                                    app.directive('chosen', function ($timeout) {
                                                    var linker = function (scope, element, attr) {
                                                    scope.$watch('masterRoomList', function () {
                                                            $timeout(function () {
                                                            element.trigger('chosen:updated');
                                                            }, 0, false);
                                                                }, true);
                                                            $timeout(function () {
                                                            element.chosen();
                                                            }, 0, false);
                                                                };
                                                            return {
                                                                restrict: 'A',
                                                                link: linker
                                                                };
                                                        });
                                                        app.directive('onlyDigits', function () {
                                                        return {
                                                            require: 'ngModel',
                                                            restrict: 'A',
                                                            link: function (scope, element, attr, ctrl) {
                                                                function inputValue(val) {
                                                                    if (val) {
                                                                        var digits = val.replace(/[^0-9.]/g, '');

                                                                        if (digits.split('.').length > 2) {
                                                                            digits = digits.substring(0, digits.length - 1);
                                                                        }

                                                                        if (digits !== val) {
                                                                            ctrl.$setViewValue(digits);
                                                                            ctrl.$render();
                                                                        }
                                                                        return parseFloat(digits);
                                                                    }
                                                                    return undefined;
                                                                }
                                                                ctrl.$parsers.push(inputValue);
                                                            }
                                                        };
                                                    });
                                                    app.filter('format', function () {
                                                        return function (item) {
                                                            var t = item.split(/[- :]/);
                                                            var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                                            var time = d.getTime();
                                                            return time;
                                                        };
                                                    });
                                                    app.filter('capitalizeWord', function () {

                                                        return function (text) {

                                                            return (!!text) ? text.charAt(0).toUpperCase() + text.substr(1).toLowerCase() : '';

                                                        }

                                                    });
                                                    app.controller("productCtrl", function ($scope, $http, $filter) {
                                                        $scope.showAddForm = false;
                                                        $scope.showForm = function () {
                                                            $scope.showAddForm = true;
                                                        }
                                                        $scope.remark = "";
                                                        $scope.hideForm = function () {
                                                            $scope.recordid = "";
                                                            $scope.editmodeheader = false;
                                                            $scope.pcode = "";
                                                            $scope.pname = "";
                                                            $scope.market = "";
                                                            $scope.remark = "";
                                                            $scope.showAddForm = false;
                                                        }

                                                        //Get Plant List
                                                        $scope.productData = [];
                                                        $scope.page = 1;
                                                        $scope.total_records = 0;
                                                        $scope.records_per_page = 10;
                                                        $scope.getproductList = function (page) {
                                                            $scope.page = page;
                                                            $http({
                                                                url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/getproductList?module=master&page='+page,
                                                                method: "GET",
                                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                            }).then(function (response) {
                                                                $scope.productData = response.data.product_list;
                                                                $scope.total_records = response.data.total_records;
                                                                $scope.records_per_page = response.data.records_per_page;
                                                            }, function (error) { // optional
                                                                $scope.productData = [];
                                                                console.log("Something went wrong.Please try again");

                                                            });
                                                        }
                                                        $scope.getproductList($scope.page);

                                    //Save Product data into Database
                                    $scope.saveProduct = function () {
                                    if (confirm("Do you want to Save this Product!")) {
                                        $http({
                                        url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/AddNewProduct',
                                        method: "POST",
                                        data: "pcode=" + $scope.pcode + "&pname=" + $scope.pname + "&market=" + $scope.market + "&remark=" + $scope.remark,
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        })
                                        .then(function (response) {
                                            if (response.data.result == true) {
                                            alert(response.data.message);
                                            $scope.editmodeheader = false;
                                            $scope.recordid = "";
                                           $scope.remark = "";
                                            $scope.pcode = "";
                                            $scope.pname = "";
                                            $scope.market = "";
                                            $scope.showAddForm = false;
                                            $scope.getproductList($scope.page);
                                            } else { alert(response.data.message); }
                                        },
                                        function (response) { // optional
                                            alert(response.data.message);
                                            $scope.editmodeheader = false;
                                            $scope.recordid = "";
                                            $scope.remark = "";
                                            $scope.pcode = "";
                                            $scope.pname = "";
                                            $scope.market = "";
                                            $scope.showAddForm = false;
                                            $scope.getproductList($scope.page);
                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    }

                                    $scope.toggleSelection = function toggleSelection(event) {
                                        if (event.target.checked == true) {
                                            $scope.active_status = true;
                                        } else {
                                            $scope.active_status = false;
                                        }
                                    };
                                    $scope.recordid = "";
                                    $scope.editProduct = function (dataObj) {
                                        $scope.editmodeheader = true;
                                        $scope.recordid = dataObj.id;
                                        $scope.remark = "";
                                        $scope.pcode = dataObj.product_code;
                                        $scope.pname = dataObj.product_name;
                                        $scope.market = dataObj.product_market;
                                        if (dataObj.is_active == '1') {
                                        $scope.active_status = true;
                                        } else {
                                        $scope.active_status = false;
                                        }
                                    $scope.showAddForm = true;
                                    }
                                    $scope.updateProduct = function () {
                                        if (confirm("Do you want to Update this Product!")) {
                                        $http({
                                        url: '<?php echo base_url() ?>ponta_sahib/Mastercontroller/UpdateProduct',
                                        method: "POST",
                                        data: "pcode=" + $scope.pcode + "&pname=" + $scope.pname + "&market=" + $scope.market + "&remark=" + $scope.remark + "&hdd=" + $scope.recordid + "&status=" + $scope.active_status,
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        })
                                        .then(function (response) {
                                        if (response.data.result == true) {
                                        alert(response.data.message);
                                        $scope.editmodeheader = false;
                                        $scope.recordid = "";
                                        $scope.remark = "";
                                        $scope.pcode = "";
                                        $scope.pname = "";
                                        $scope.market = "";
                                        $scope.showAddForm = false;
                                        $scope.getproductList($scope.page);
                                        } else {
                                        alert(response.data.message);
                                        }
                                        },
                                        function (response) { // optional
                                        alert(response.data.message);
                                        $scope.editmodeheader = false;
                                        $scope.recordid = "";
                                        $scope.remark = "";
                                        $scope.pcode = "";
                                        $scope.pname = "";
                                        $scope.market = "";
                                        $scope.showAddForm = false;
                                        $scope.getproductList($scope.page);
                                        console.log("Something went wrong.Please try again");
                                        });
                                        }
                                    }

    });
</script>