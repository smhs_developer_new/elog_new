<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
    <!-- Page Heading -->

    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row">

                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">    	   
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th colspan="5" class="f18" style="vertical-align:middle">Tablet Tooling Master</th>
                                <th class="text-right"> 
                                     <?php if ($add) { ?>
                                        <button class="btn btn-success" ng-click="showForm()">Add Tablet Data</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success" ng-click="showForm()" disabled>Add Tablet Data</button>
                                    <?php } ?>
                                </th>
                            </tr>
                        </thead>
                    </table>  
                </div>
                <div class="col-12">
                    <form name="balanceForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-lg-4 mb-3"></div>
                                        <div class="col-lg-4 mb-3"><label>Product Code <span style="color: red">*</span></label>
                                            <select class="chzn-select form-control" tabindex="4"  name="product_code" ng-change="getProdesc(product_code)" data-placeholder="Search Product No." ng-options="dataObj['product_code'] as dataObj.product_code for dataObj in productData"  ng-model="product_code" required chosen>
                                                <option value="">Select Product No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row" ng-show="productdesc!=''">
                                        <div class="col-lg-4 mb-3"></div>
                                        <div class="col-lg-8 mb-3"><p>Product Description : {{productdesc}}</p></div>
                                    </div>
                                    <div class="form-row" ng-repeat="dataObj in tabletData">
                                        <div class="col-lg-11 mb-3" style="border: 1px solid;">
                                            <div class="form-row" > 
                                                <div class="col-lg-2 mb-3"><label><small>Supplier</small><span style="color: red">*</span></label>
                                                    <input type="text" only-char class="form-control" maxlength="40" name="supplier" ng-model="dataObj.supplier" required>
                                                </div>
                                                <div class="col-lg-3 mb-3"><label title="Only Numeric Value"><small>Upper Punch-Qty./Embossing</small><span style="color: red">*</span></label>
                                                    <input type="text" only-alphanum maxlength="10" class="form-control" name="upper_punch_qty" ng-model="dataObj.upper_punch_qty" required>
                                                </div>
                                                <div class="col-lg-3 mb-3"><label title="Only Numeric Value"><small>Lower Punch-Qty./Embossing</small><span style="color: red">*</span></label>
                                                    <input type="text" only-alphanum maxlength="10" class="form-control" name="lower_punch_qty" ng-model="dataObj.lower_punch_qty" required>
                                                </div>

                                                <div class="col-lg-2 mb-3"><label title="Only Numeric Value"><small>Year</small><span style="color: red">*</span></label>
                                                <select class="form-control" name="year" ng-model="dataObj.year">
                                                <option value="">Select</option>
                                                <option value="2015">2015</option>
                                                <option value="2016">2016</option> 
                                                <option value="2017">2017</option>
                                                <option value="2018">2018</option>
                                                <option value="2019">2019</option>
                                                <option value="2020">2020</option>
                                                <option value="2021">2021</option>
                                                <option value="2022">2022</option>
                                                <option value="2023">2023</option>
                                                <option value="2024">2024</option>
                                                <option value="2025">2025</option>
                                                </select>
                                                <!-- <input type="text" maxlength="4" class="form-control" name="year" ng-model="dataObj.year" only-digits required> -->
                                                </div>
                                            </div>
                                            <div class="form-row"> 
                                                <div class="col-lg-3 mb-3"><label><small>Dimension</small><span style="color: red">*</span></label>
                                                    <input type="text" only-alphanum maxlength="10" class="form-control" name="dimension" ng-model="dataObj.dimension" required>
                                                </div>
                                                <div class="col-lg-3 mb-3"><label><small>Shape</small><span style="color: red">*</span></label>
                                                    <input type="text" only-char maxlength="10" class="form-control" name="shape" ng-model="dataObj.shape" required>
                                                </div>
                                                <div class="col-lg-3 mb-3"><label><small>Machine</small><span style="color: red">*</span></label>
                                                    <input type="text" maxlength="10" class="form-control" name="machine" ng-model="dataObj.machine" required>
                                                </div>
                                                <div class="col-lg-2 mb-3"><label title="Only Numeric Value"><small>Die- Quantity</small><span style="color: red">*</span></label>
                                                    <input type="text" maxlength="4" class="form-control" name="die_quantity" ng-model="dataObj.die_quantity" only-digits required>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-lg-1 mb-3">
                                            <div class="col-lg-1 mb-2" style="margin-top: 35px;">
                                                <label><span style="color: red"></span></label>
                                                <button class="btn btn-xs btn-danger" ng-if="tabletData.length > 1" ng-click="Remove($index)" title="Remove"><i class="fa fa-times" aria-hidden="true"></i></button><!-- end ngIf: reportField[0].length > 1 -->
                                                &nbsp;
                                                <button class="btn btn-xs btn-success" ng-show="showAddButton($index)" ng-click="Add($index)" title="Add"><i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-sm btn-success" ng-disabled="balanceForm.$invalid || product_code=='NA'"  ng-click="saveTabletTooling()" type="submit">Submit</button>
                                        <button ng-click="resetForm()" class="btn btn-sm btn-danger"  type="reset">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="form-row" ng-show="tabletList.length == 0">
                <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
                <div class="col-lg-12 text-center" ><h4>(Please Select the above parameters)</h4></div> 
            </div>
            <div class="form-row" style="margin-top:10px;" ng-show="tabletList.length > 0">   
                <h2 class="f18">Tablet Tooling Card Log List</h2>
                <div class="table-responsive noscroll" >
                    <table  class="table custom-table">
                        <thead >
                            <tr>
                                <th>Product Code</th>
                                <th>Supplier</th>
                                <th>Upper Punch-Qty./Embossing</th>
                                <th>Lower Punch-Qty./Embossing</th>
                                <th>Year</th>
                                <th>Dimension</th>
                                <th>Shape</th>
                                <th>Machine</th>
                                <th>Die- Quantity</th>
                                <th >Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr dir-paginate="dataObj in tabletList|itemsPerPage:records_per_page" total-items="total_records" style="background:none;">
                                <td>{{dataObj.product_code}}</td>
                                <td>{{dataObj.supplier}}</td>
                                <td>{{dataObj.upper_punch_qty}}</td>
                                <td>{{dataObj.lower_punch_qty}}</td>
                                <td>{{dataObj.year}}</td>
                                <td>{{dataObj.dimension}}</td>
                                <td>{{dataObj.shape}}</td>
                                <td>{{dataObj.machine}}</td>
                                <td>{{dataObj.die_quantity}}</td>
                                <td>
                                    <?php if ($edit) { ?>
                                    <button class="btn btn-sm btn-info" ng-click="editTablettoolingMaster(dataObj)">Edit</button>
                                    <button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)">Deactivate</button>
                                <?php } else { ?>
                                    <button class="btn btn-sm btn-info" ng-click="editTablettoolingMaster(dataObj)" disabled>Edit</button>
                                    <button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)" disabled>Deactivate</button>
                                <?php } ?>
                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getTabletToolingLogList(newPageNumber)"></dir-pagination-controls>
                </div>

            </div>
        </div>
    </div>	

</div>

<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment-with-locales.js"></script>
<script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
<script type="text/javascript">
    var app = angular.module("materialApp", ['angular.chosen', 'moment-picker','angularUtils.directives.dirPagination']);
    app.directive('chosen', function ($timeout) {

        var linker = function (scope, element, attr) {

            scope.$watch('productData', function () {
                $timeout(function () {
                    element.trigger('chosen:updated');
                }, 0, false);
            }, true);

            $timeout(function () {
                element.chosen();
            }, 0, false);
        };
        return {
            restrict: 'A',
            link: linker
        };
    });
    app.directive('onlyDigits', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attr, ctrl) {
                function inputValue(val) {
                    if (val) {
                        var digits = val.replace(/[^0-9.]/g, '');

                        if (digits.split('.').length > 2) {
                            digits = digits.substring(0, digits.length - 1);
                        }

                        if (digits !== val) {
                            ctrl.$setViewValue(digits);
                            ctrl.$render();
                        }
                        return parseFloat(digits);
                    }
                    return undefined;
                }
                ctrl.$parsers.push(inputValue);
            }
        };
    });
    app.directive('onlyChar', function() {
  return {
    require: 'ngModel',
    link: function (scope, element, attr, ngModelCtrl) {
      function fromUser(text) {
        var transformedInput = text.replace(/[^A-Za-z ]/g, '');
//        console.log(transformedInput);
        if(transformedInput !== text) {
            ngModelCtrl.$setViewValue(transformedInput);
            ngModelCtrl.$render();
        }
        return transformedInput;
      }
      ngModelCtrl.$parsers.push(fromUser);
    }
  }; 
});
    app.directive('onlyAlphanum', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attr, ngModelCtrl) {
      function fromUser(text) {
        var transformedInput = text.replace(/[^0-9a-zA-Z\-\.\s]/g, '');
//        console.log(transformedInput);
        if (transformedInput !== text) {
          ngModelCtrl.$setViewValue(transformedInput);
          ngModelCtrl.$render();
        }
        return transformedInput; // or return Number(transformedInput)
      }
      ngModelCtrl.$parsers.push(fromUser);
    }
  };
});
    app.controller("materialCtrl", function ($scope, $http, $filter) {
        $scope.productData = [];
        $scope.getProductList = function () {
            $http({
                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).then(function (response) {
                $scope.productData = response.data.product_list;
//                console.log($scope.productData);
            }, function (error) { // optional
                console.log("Something went wrong.Please try again");

            });
        }
        $scope.getProductList();
        $scope.showAddForm = false;
        $scope.showForm = function () {
            $scope.showAddForm = true;
        }

        $scope.tabletData = [{"supplier": "", "upper_punch_qty": "", "lower_punch_qty": "", "year": "", "dimension": "", "shape": "", "machine": "", "die_quantity": ""}];
        $scope.Add = function (index) {
            const obj = {};
            for (const key of Object.keys($scope.tabletData[index])) {
                obj[key] = null;
            }
            $scope.tabletData.push(obj);
//            console.log($scope.tabletData);
        }
        $scope.Remove = function (index) {
            //Remove the item from Array using Index.
            $scope.tabletData.splice(index, 1);
        }
        $scope.showAddButton = function (index) {
            return index === $scope.tabletData.length - 1;
        };
        $scope.productdesc="";
        $scope.getProdesc = function (prono) {
            $scope.productdesc = $filter('filter')($scope.productData, {product_code: prono})[0].product_name;
//            console.log($scope.productdesc);

        };
        $scope.saveTabletTooling = function () {
            if (confirm("Do You Want To Save This Record ?")) {
                $http({
                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/AddTabletToolingLog',
                    method: "POST",
                    data: "product_code=" + $scope.product_code + "&tablet_data=" + encodeURIComponent(angular.toJson($scope.tabletData)) + "&recordid=" + $scope.recordid,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                })
                        .then(function (response) {
                            alert(response.data.message);
                            $scope.product_code = '';
                            $scope.tabletData = [{"supplier": "", "upper_punch_qty": "", "lower_punch_qty": "", "year": "", "dimension": "", "shape": "", "machine": "", "die_quantity": ""}];
                            $scope.showAddForm = false;
                            $scope.getTabletToolingLogList($scope.page);
                        },
                                function (response) { // optional
                                    $scope.product_code = '';
                                    $scope.tabletData = [{"supplier": "", "upper_punch_qty": "", "lower_punch_qty": "", "year": "", "dimension": "", "shape": "", "machine": "", "die_quantity": ""}];
                                    $scope.showAddForm = false;
                                    $scope.getTabletToolingLogList($scope.page);
                                    console.log("Something went wrong.Please try again");

                                });
            }
        }

        $scope.resetForm = function () {
            $scope.product_code = '';
            $scope.tabletData = [{"supplier": "", "upper_punch_qty": "", "lower_punch_qty": "", "year": "", "dimension": "", "shape": "", "machine": "", "die_quantity": ""}];
        }
        $scope.tabletList = [];
        $scope.page = 1;
        $scope.total_records = 0;
        $scope.records_per_page = 10;
        $scope.getTabletToolingLogList = function (page) {
            $scope.page = page;
            $http({
                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetTabletToolingLogList?page='+page,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).then(function (response) {
                $scope.tabletList = response.data.tablet_list;
                $scope.total_records = response.data.total_records;
                $scope.records_per_page = response.data.records_per_page;
            }, function (error) { // optional
                console.log("Something went wrong.Please try again");

            });
        }

        $scope.getTabletToolingLogList($scope.page);

        //Created By : Bhupendra Kumar
        //Date : 17 March 2020
        //Decription : For Deactivate the Selected Record
        $scope.CommonDelete = function (id) {
            var tblname = "pts_mst_tablet_tooling_log";
            var colname = "id";
            if (confirm("Do You Want To Deactivate This Record ?")) {
                $http({
                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/CommonDelete',
                    method: "POST",
                    data: "id=" + id + "&tblname=" + tblname + "&colname=" + colname,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                }).then(function (response) {
                    alert(response.data.message);
                    $scope.getTabletToolingLogList($scope.page);
                }, function (error) { // optional
                    alert(response.data.message);
                    //console.log("Something went wrong.Please try again");
                });
            }
        }

        $scope.editTablettoolingMaster = function (dataObj) {
            $scope.recordid = dataObj.id;
            $scope.tabletData = [];
            $scope.product_code = dataObj.product_code;
            $scope.getProdesc($scope.product_code);
            var obj = {};
            angular.forEach($scope.tabletList, function(value, key) {
                if($scope.product_code == value.product_code){
                    obj = {supplier: value.supplier, upper_punch_qty: value.upper_punch_qty, lower_punch_qty: value.lower_punch_qty, year: value.year, dimension: value.dimension, shape: value.shape, machine: value.machine, die_quantity: value.die_quantity}
                    $scope.tabletData.push(obj);
                }
                
            });
            

            $scope.showAddForm = true;
        }

    });
</script>