<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="content-wrapper">
        <div class="content-heading executesop-heading">
            <div class="col-sm-5 pl-0">Update Master</div>
            <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
            <div class="col-sm-7 pr-0">
                <ol class="breadcrumb ml-auto">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>


                </ol>
            </div>
        </div>

        <div class="card card-default">
            <div class="card-body">                     
                <div class="row">
                    <div class="col-sm-12 text-right">

                        <a class="btn btn-primary btn-lg" href="<?= base_url() ?>product_view?module_id=<?php echo $_GET['module_id']?>" > Back To list</a>  

                    </div>
                </div>
            </div>
        </div>

        <form id="product">  
            <div class="card card-default">
                <div class="card-header text-white blue-bg">Edit Product Details</div>
                <div class="card-body">
                    <?php if (count($data['prodtl']->result())) {
                        $res = $data['prodtl']->row_array();
                        ?>                      
                        <div class="col-md-12">
                            <div class="form-row">
                                <input type="hidden" id="hdd" name="hdd" value="<?php echo $res['id'] ?>">
                                <div class="col-lg-4 mb-3"><label >Product code :<span style="color: red">*</span></label>
                                    <input class="form-control" type="text" name="pcode" id="pcode" maxlength="20" value="<?php echo $res['product_code'] ?>" readonly required>                                
                                </div>
                                <div class="col-lg-8 mb-3"><label >Product Name :<span style="color: red">*</span></label>
                                    <input class="form-control" type="text" name="pname" maxlength="50" id="pname" value="<?php echo $res['product_name'] ?>" required>                                
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-lg-4 mb-3"><label>Market :<span style="color: red">*</span></label>
                                    <input class="form-control"  id="market" name="market" type="text" value="<?php echo $res['product_market'] ?>" required>                                
                                </div>                      
                                <div class="col-lg-8 mb-3"><label>Remark :</label>
                                    <input class="form-control"  id="remark" name="remark" type="text" value="<?php echo $res['product_remark'] ?>">                               
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-lg-12"> 
                                    <center>
                                         <?php if ($edit) { ?>
                                <input class="btn btn-success"  id="subbtn" type="submit" value="Submit">
<?php } else { ?>
                                <button class="btn btn-success btn-lg" type="button" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
<?php } ?>
                                    </center> 
                                </div>              
                            </div>

                        </div>
<?php } else { ?>
                        <div class="col-md-12">
                            <div class="form-row">
                                <div class="col-lg-3 mb-3"><center><label >There is nothing to update</label></center></div>
                            </div>
                        </div>
<?php } ?>
                </div>
            </div>
        </form>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {

        $("#pcode").change(function () {
            debugger;
            var tblname = "mst_product";
            var col = "product_code";
            var code = $(this).val();
            $.ajax({
                url: '<?= base_url() ?>ponta_sahib/Mastercontroller/chkcode',
                type: 'POST',
                data: {tblname: tblname, col: col, code: code},
                success: function (res) {
                    //console.log(res);
                    if (res.status == 1) {
                        // $("#validationnow21").attr("disabled",true);
                        // $("#validationnow22").attr("disabled",false);
                        alert("Record Already Exists at this code.");
                        $("#pcode").val("");
                        $("#pcode").focus();
                    }
                }
            });
        });

        $('#product').submit(function () {
            debugger;
            $.ajax({
                url: '<?= base_url() ?>ponta_sahib/Mastercontroller/UpdateProduct',
                type: 'POST',
                data: $(this).serialize(),
                success: function (res) {
                    debugger;
                    console.log(res);
                    if (res.status == 1)
                    {
                        alert("successfully Updated.");
                        //location.reload(true);
                        window.location.href = "<?= base_url() ?>product_view?module_id=<?php echo $_GET['module_id']?>";
                    } else
                    {
                        alert("Not saved.");
                    }
                }
            });
            return false;
        });

    });
</script>
<script type="text/javascript">
    function getdept()
    {
        var block = $("#block").val();
        $.ajax({
            url: "<?= base_url() ?>ponta_sahib/Mastercontroller/getdept",
            type: 'POST',
            data: {block: block},
            success: function (res) {
                //console.log(res);
                var inv = "<option value='0' selected disabled>Select Department</option>";
                $.each(res, function (index, value) {
                    inv = inv.concat("<option value='" + value.department_code + "'>" + value.department_name + "</option>");
                });
                $("#dept").html(inv);

            }
        });

    }
</script>