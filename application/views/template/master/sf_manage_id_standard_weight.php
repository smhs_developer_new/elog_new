<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
    <!-- Page Heading -->

    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row">

                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">    	   
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th colspan="5" class="f18" style="vertical-align:middle">Weight Standard ID Mapping</th>
                                <th class="text-right">
                                    <?php if ($add) { ?>
                                        <button class="btn btn-success" ng-disabled="recordid > 0" ng-click="showForm()">Add (<i class="fas fa-plus"></i>)</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success"  ng-click="showForm()"disabled>Add (+)</button>
                                    <?php } ?>

                                </th>
                            </tr>
                        </thead>
                    </table>  
                </div>
                <div class="col-12">
                    <form name="materialForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-lg-3 mb-3"><label>Weight <span style="color: red">*</span></label>
                                            <input type="text" ng-disabled="recordid > 0" class="form-control" name="weight" maxlength="5" only-digits ng-model="weight" required>
                                        </div>
                                        <div class="col-lg-3 mb-3"><label>Measurement Unit <span style="color: red">*</span></label>
                                            <select class="form-control" ng-disabled="recordid > 0" name="measurement_unit" ng-model="measurement_unit">
                                                <option value="KG">KG</option>
                                                <option value="GM">GM</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="form-row">
                                        <div class="col-sm-6">
                                            <label>Id No Standard Weight<span style="color: red">*</span></label>
                                            <div class="form-row"  ng-repeat="dataObj in weightData">
                                                <div class="col-lg-6 mb-3">
                                                    <input type="text" ng-disabled="recordid > 0" class="form-control" ng-blur="CommonCheckIsExist(dataObj.id_no_statndard_weight, $index)" maxlength="15" name="supplier" ng-model="dataObj.id_no_statndard_weight" required>
                                                </div>
                                                <div class="col-lg-3 mb-2">
                                                    <label><span style="color: red"></span></label>
                                                    <button ng-disabled="recordid > 0" class="btn btn-xs btn-danger" ng-if="weightData.length > 1" ng-click="Remove($index)" title="Remove"><i class="fa fa-times" aria-hidden="true"></i></button><!-- end ngIf: reportField[0].length > 1 -->
                                                    &nbsp;
                                                    <button ng-disabled="recordid > 0" class="btn btn-xs btn-success" ng-show="showAddButton($index)" ng-click="Add($index)" title="Add"><i class="fa fa-plus-square" aria-hidden="true"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-4 mb-3">
                                            <label for="validationServer01">Remarks <span style="color: red" ng-show="editmodeheader">* </span></label>
                                            <textarea class="form-control" aria-label="With textarea" name="bremarks" placeholder="Enter Remarks" ng-required="editmodeheader" ng-model="remark"></textarea>
                                        </div>
                                        <div class="col-lg-2 mb-3" ng-show="recordid > 0"><label><b>Active/Inactive</b></label>
                                            <label class="switch">
                                                <input type="checkbox" style="margin-top: 13px;" ng-model="active_status" ng-click="toggleSelection($event)">
                                                <span class="slider round"></span>
                                            </label>

                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-success btn-sm" ng-disabled="materialForm.$invalid"  ng-click="saveIdStandard()" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        <button class="btn btn-danger btn-sm" ng-click="hideForm()" type="button">&nbsp;&nbsp;&nbsp;&nbsp; Cancel &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="form-row" ng-show="idStandardData.length == 0">
                <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
                <div class="col-lg-12 text-center" ><h4>(Please Select the above parameters)</h4></div> 
            </div>
            <div class="form-row" style="margin-top:10px;" ng-show="idStandardData.length > 0">   
                <h2 class="f18">Standard Weight ID List</h2>
                <div class="table-responsive noscroll" >
                    <table  class="table custom-table">
                        <thead >
                            <tr>
                                <th>Weight</th>
                                <th>Measurement Unit</th>
                                <th>ID of standard weight</th>
                                <th>Last Modified By</th>
                                <th>Last Modified On</th>
                                <th>Action</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr dir-paginate="dataObj in idStandardData|itemsPerPage:records_per_page" total-items="total_records" style="background:none;">
                                <td>{{dataObj.weight}}</td>
                                <td>{{dataObj.measurement_unit}}</td>
                                <td>{{dataObj.id_no_statndard_weight}}</td>
                                <td>{{dataObj.modified_by!=""?dataObj.modified_by:dataObj.created_by}}</td>
                                <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td>
                                    <?php if ($edit) { ?>
                                        <a ng-click="editIdStandardWeightMaster(dataObj)"><i class="far fa-edit text-blue pr10" title="Edit"></i></a>
                                        <!--<button class="btn btn-sm btn-info" ng-click="editIdStandardWeightMaster(dataObj)">Edit</button>-->
                                        <!--<button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)">Deactivate</button>-->
                                    <?php } else { ?>
                                        <a type="button" class="btn btn-sm btn-info" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                    <?php } ?>
                                </td>
                                <td ng-if="dataObj.status == 'active'"   class="bg-success text-white">{{dataObj.status| capitalizeWord}}</td>
                                <td ng-if="dataObj.status == 'inactive'" class="bg-danger text-white">{{dataObj.status| capitalizeWord}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getStandardWeightList(newPageNumber)"></dir-pagination-controls>
                </div>

            </div>
        </div>
    </div>	

</div>

<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>

<script type="text/javascript">
                                    var app = angular.module("materialApp", ['angular.chosen', 'angularUtils.directives.dirPagination']);
                                    app.directive('onlyDigits', function () {
                                        return {
                                            require: 'ngModel',
                                            restrict: 'A',
                                            link: function (scope, element, attr, ctrl) {
                                                function inputValue(val) {
                                                    if (val) {
                                                        var digits = val.replace(/[^0-9.]/g, '');

                                                        if (digits.split('.').length > 2) {
                                                            digits = digits.substring(0, digits.length - 1);
                                                        }

                                                        if (digits !== val) {
                                                            ctrl.$setViewValue(digits);
                                                            ctrl.$render();
                                                        }
                                                        return parseFloat(digits);
                                                    }
                                                    return undefined;
                                                }
                                                ctrl.$parsers.push(inputValue);
                                            }
                                        };
                                    });
                                    app.filter('format', function () {
                                        return function (item) {
                                            var t = item.split(/[- :]/);
                                            var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                            var time = d.getTime();
                                            return time;
                                        };
                                    });
                                    app.filter('capitalizeWord', function () {

                                        return function (text) {

                                            return (!!text) ? text.charAt(0).toUpperCase() + text.substr(1).toLowerCase() : '';

                                        }

                                    });
                                    app.controller("materialCtrl", function ($scope, $http, $filter) {
                                        $scope.measurement_unit = "KG";
                                        $scope.idStandardData = [];
                                        $scope.page = 1;
                                        $scope.total_records = 0;
                                        $scope.records_per_page = 10;
                                        $scope.getStandardWeightList = function (page) {
                                            $scope.page = page;
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetStandardWeightList?module=master&page=' + page,
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.idStandardData = response.data.standard_list;
                                                $scope.total_records = response.data.total_records;
                                                $scope.records_per_page = response.data.records_per_page;
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");

                                            });
                                        }
                                        $scope.getStandardWeightList($scope.page);
                                        $scope.showAddForm = false;
                                        $scope.showForm = function () {
                                            $scope.showAddForm = true;
                                        }

                                        $scope.weightData = [{"id_no_statndard_weight": ""}];
                                        $scope.Add = function (index) {
                                            const obj = {};
                                            for (const key of Object.keys($scope.weightData[index])) {
                                                obj[key] = null;
                                            }
                                            $scope.weightData.push(obj);
                                        }
                                        $scope.Remove = function (index) {
                                            //Remove the item from Array using Index.
                                            $scope.weightData.splice(index, 1);
                                        }
                                        $scope.showAddButton = function (index) {
                                            return index === $scope.weightData.length - 1;
                                        };
                                        $scope.remark = "";
                                        $scope.hideForm = function () {
                                            $scope.recordid = "";
                                            $scope.editmodeheader = false;
                                            $scope.weight = "";
                                            $scope.recordid = "";
                                            $scope.measurement_unit = "KG";
                                            $scope.weightData = [{"id_no_statndard_weight": ""}];
                                            $scope.showAddForm = false;
                                            $scope.remark = "";
                                        }
                                        $scope.saveIdStandard = function () {
                                            var msg = 'Save';
                                            if ($scope.recordid > 0) {
                                                var msg = 'Update';
                                            }

                                            if (confirm("Do You Want To " + msg + " This Record?")) {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/SaveIdStandard',
                                                    method: "POST",
                                                    data: "weight=" + $scope.weight + "&measurement_unit=" + $scope.measurement_unit + "&standard_data=" + encodeURIComponent(angular.toJson($scope.weightData)) + "&recordid=" + $scope.recordid + "&status=" + $scope.active_status + "&remark=" + $scope.remark,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                })
                                                        .then(function (response) {
                                                            alert(response.data.message);
                                                            $scope.hideForm();
                                                            $scope.recordid = "";
                                                            $scope.getStandardWeightList($scope.page);
                                                        },
                                                                function (response) { // optional
                                                                    $scope.hideForm();
                                                                    $scope.recordid = "";
                                                                    $scope.getStandardWeightList($scope.page);
                                                                    console.log("Something went wrong.Please try again");

                                                                });
                                            }
                                        }

                                        //Created By : Bhupendra Kumar
                                        //Date : 18 March 2020
                                        //Decription : For Deactivate the Selected Record
                                        $scope.CommonDelete = function (id) {
                                            var tblname = "pts_mst_id_standard_weight";
                                            var colname = "id";
                                            if (confirm("Do You Want To Deactivate This Record ?")) {
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/CommonDelete',
                                                    method: "POST",
                                                    data: "id=" + id + "&tblname=" + tblname + "&colname=" + colname,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    alert(response.data.message);
                                                    $scope.getStandardWeightList($scope.page);
                                                }, function (error) { // optional
                                                    alert(response.data.message);
                                                    //console.log("Something went wrong.Please try again");
                                                });
                                            }
                                        }

                                        //Created By : Bhupendra Kumar
                                        //Date : 18 March 2020
                                        //Decription : To get values of selected record
                                        $scope.recordid = "";
                                        $scope.status = "";
                                        $scope.active_status = false;
                                        $scope.editIdStandardWeightMaster = function (dataObj) {
                                            $scope.remark = "";
                                            $scope.weightData = [];
                                            var obj = {};
                                            $scope.recordid = dataObj.id;
                                            $scope.weight = dataObj.weight;
                                            $scope.measurement_unit = dataObj.measurement_unit;
                                            var weightDataarray = dataObj.id_no_statndard_weight;
                                            obj = {id_no_statndard_weight: weightDataarray}
                                            $scope.weightData.push(obj);
                                            $scope.showAddForm = true;
                                            $scope.editmodeheader = true;
                                            if (dataObj.status == 'active') {
                                                $scope.active_status = true;
                                            } else {
                                                $scope.active_status = false;
                                            }
                                        }

                                        $scope.toggleSelection = function (event) {
                                            if (event.target.checked == true) {
                                                $scope.active_status = true;
                                            } else {
                                                $scope.active_status = false;
                                            }
                                        };

                                        //created By : Bhupendra kumar
                                        //Date : 6th April 2020
                                        //Description : to check the record is already exist or not on given parameter
                                        $scope.CommonCheckIsExist = function (code, index) {
                                            var tblname = "pts_mst_id_standard_weight";
                                            var col = "id_no_statndard_weight";
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/chkcode',
                                                method: "POST",
                                                data: "col=" + col + "&tblname=" + tblname + "&code=" + code,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (res) {
                                                if (res.data.status == 1) {
                                                    alert("Record already exists at this id.");
                                                    $scope.weightData[index]["id_no_statndard_weight"] = null;
                                                    return false;
                                                }
                                            }, function (error) { // optional
                                            });
                                        }
                                    });
</script>