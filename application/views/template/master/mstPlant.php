<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid">
          <!-- Page Heading -->
			
          <div class="content-wrapper">
            <div class="content-heading executesop-heading">
              <div class="col-sm-5 pl-0">Create Plant</div>
              <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
              <div class="col-sm-7 pr-0">
                <ol class="breadcrumb ml-auto">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
                  <li class="breadcrumb-item active"><a href="<?= base_url()?>create_plant?module_id=<?php echo $_GET['module_id'] ?>">Plant</a></li>
                </ol>
              </div>
            </div>
            
      
      
      <div class="card card-default">
      <div class="card-body">                     
      <div class="row">
      <div class="col-sm-12 text-right">
      
      <a class="btn btn-primary btn-lg" href="<?= base_url()?>plant_view?module_id=<?php echo $_GET['module_id'] ?>" > Back To list</a>  
    &nbsp;&nbsp;&nbsp;
     <?php if ($add) { ?>
    <a class="btn btn-primary btn-lg" href="<?= base_url()?>create_plant?module_id=<?php echo $_GET['module_id'] ?>"> Add Plant</a>
      <?php } else { ?>
                                <button class="btn btn-primary btn-lg" type="button" disabled>Add Plant</button>
<?php } ?>
      </div>
      </div>
      </div>
      </div>
      
      <form id="plant">
    <div class="card card-default">
        <div class="card-body">

	  <fieldset>
                        
                           <div class="col-auto">
                             <!-- <div class="form-row">
                                 <div class="col-lg-12 mb-3"><label for="validationServer01">Sanitization Used <span style="color: red">*</span></label><input class="form-control autocomplete" type="text" placeholder="Sanitization Used" name="solutionname" id="solutionname" required> 
                                 </div>
                              </div>   -->
                              <div class="form-row">   
                                 <div class="col-lg-4 mb-3"><label for="aqty">Plant Code <span style="color: red">*</span></label><input class="form-control" id="pcode" maxlength="20" type="text" placeholder="Plant Code" name="pcode" required>
                                 </div>
                                 <div class="col-lg-4 mb-3"><label for="edso">Plant Name <span style="color: red">*</span></label><input class="form-control" id="pname" maxlength="50" type="text" placeholder="Plant Name" name="pname" required >
                                 </div>
                                 <div class="col-lg-4 mb-3"><label for="rtc">Plant Address</label><textarea class="form-control" aria-label="With textarea" maxlength="500" name="paddress" placeholder="Plant Address" ></textarea>
                                 </div>
                                 
                              </div>

                              <div class="form-row">
                                 <div class="col-lg-4 mb-3"><label for="validationServer01">Plant Contact</label><input class="form-control cc" id="pcontact" type="text" placeholder="Plant Contact" name="pcontact"  maxlength="10" minlength="10" pattern="(.){10,10}" title="Enter 10 digits phone number">
                                    
                                 </div>
                                 <div class="col-lg-4 mb-3"><label for="dpoints"> No of Blocks <span style="color: red">*</span></label><input class="form-control cc" id="pblock" type="text" maxlength="2" placeholder="No. Blocks" name="pblock" required>
                                    
                                 </div>
                                 <div class="col-lg-4 mb-3"><label for="validationServer01">Remarks</label><textarea class="form-control" aria-label="With textarea" name="premarks" placeholder="Enter Remarks" maxlength="100" ></textarea>
                                    
                                 </div>
                                 
                              </div>
    
                        </div>
                     </fieldset>
					 </div>
					 </div>
					  <div class="card card-default">     
            <div class="card-body" >
            <div class="row disable-button-color">
            <div class="col-sm-12 text-center" id="sbtn">
                 <?php if ($add) { ?>
                                <button id="sbbtn" class="btn btn-success btn-lg" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
<?php } else { ?>
                                <button class="btn btn-success btn-lg" type="button" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
<?php } ?>
            </div>
            </div>
            </div>
            </div><!-- END card-->
					 
</form>
      
              </div>			
			
			
			
        </div>

<script type="text/javascript">
$(document).ready(function(){
  $('.cc').bind('keyup paste', function(){
        this.value = this.value.replace(/[^0-9]/g, '');
  });

  $("#pcode").change(function(){
    var pcode = $(this).val();
    $.ajax({
        url: '<?= base_url()?>ponta_sahib/Mastercontroller/checkPlantcode',
        type: 'POST',
        data: {pcode:pcode},
        success: function (res) {
          //console.log(res);
          if(res.status==0){
           // $("#validationnow21").attr("disabled",true);
           // $("#validationnow22").attr("disabled",false);
            alert("Plant Code Already Exists");
            $("#pcode").val("");
          }
        }
      });
  });

   $('#plant').submit(function () {
      $.ajax({
        url: '<?= base_url()?>ponta_sahib/Mastercontroller/mstPlantSubmit',
        type: 'POST',
        data: $(this).serialize(),
        success: function (res) {
          //console.log(res);
          if(res.status==1){
           // $("#validationnow21").attr("disabled",true);
           // $("#validationnow22").attr("disabled",false);
            alert("Plant Added successfully");
            window.location.href = "<?= base_url()?>plant_view?module_id=<?php echo $_GET['module_id']?>";
          }else if(res.status==0){
              alert(res.msg);
          }
        }
      });
      return false;
    });

});  
</script>