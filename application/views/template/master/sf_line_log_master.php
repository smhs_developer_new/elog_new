<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
    <!-- Page Heading -->

    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row">

                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">    	   
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th colspan="5" class="f18" style="vertical-align:middle">Line Master</th>
                                <th class="text-right">
                                    <?php if ($add) { ?>
                                        <button class="btn btn-success" ng-click="showForm()" ng-disabled="editmode">Add (+)</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success" ng-click="showForm()" disabled>Add (+)</button>
                                    <?php } ?>

                                </th>
                            </tr>
                        </thead>
                    </table>  
                </div>
                <div class="col-12">
                    <form name="materialForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-lg-3 mb-3"><label>Line Code <span style="color: red">*</span></label>
                                            <input class="form-control" type="text" ng-change="CommonCheckIsExist(line_log_code)" name="line_log_code" ng-model="line_log_code" ng-readonly="editmode" maxlength="20" required/>
                                        </div>
                                        <div class="col-lg-3 mb-3"><label>Line Name<span style="color: red">*</span></label>
                                            <input class="form-control" type="text"  name="line_log_name" ng-change="CommonCheckNameExist(line_log_name)" ng-model="line_log_name" maxlength="50" required>
                                        </div>
                                        <div class="col-lg-3 mb-3"><label>Block Name <span style="color: red">*</span></label>
                                            <select class="chzn-select form-control" tabindex="4" id="room"  name="block_code"  data-placeholder="Search Block Code" ng-change="getAreaList()" ng-options="dataObj['block_code'] as (dataObj.block_code +   ' => ' +   dataObj.block_name) for dataObj in blockList"  ng-model="block_code" required chosen>
                                                <option value="">Select Block Name</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-3 mb-3"><label>Area Name <span style="color: red">*</span></label>
                                            <select class="chzn-select form-control" tabindex="4" id="room"  name="area_code" data-placeholder="Search Area Code" ng-change="getMasterRoomList()" ng-options="dataObj['area_code'] as (dataObj.area_code +   ' => ' +   dataObj.area_name) for dataObj in areaList"  ng-model="area_code" required chosen>
                                                <option value="">Select Area Name</option>
                                            </select>
                                        </div>
                                    </div>


                                </div>
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-lg-4 mb-3"><label>Room Name <span style="color: red">*</span></label>
                                            <select class="chzn-select form-control" tabindex="4" id="room" ng-change="getFixedEquimentList(room_code)"  name="room_code" data-placeholder="Search Room Code" ng-options="dataObj['room_code'] as (dataObj.room_code +   ' => ' +   dataObj.room_name +   ' => ' +   dataObj.area_code) for dataObj in masterRoomList"  ng-model="room_code" required chosen>
                                                <option value="">Select Room Name</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 mb-3"><label>Select Equipment <span style="color: red">*</span></label>
                                            <!-- <select class="chzn-select form-control" multiple tabindex="4" id="equip"    name="equipment_code" data-placeholder="Search Equipment Code" ng-options="dataObj['equipment_code'] as (dataObj.equipment_name) for dataObj in fixedEquipmentData"  ng-model="equipmentData" required chosen>
                                            </select> -->
                                            <select class="chzn-select form-control" ng-change="getcheck();" multiple tabindex="4" id="equip"    name="equipment_code" data-placeholder="Search Equipment Code" ng-options="dataObj['equipment_code'] as (dataObj.equipment_name) for dataObj in fixedEquipmentData"  ng-model="equipmentData" required chosen>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 mb-3">
                                            <label for="validationServer01">Remarks <span style="color: red" ng-show="editmode">*</span></label>
                                            <textarea class="form-control" aria-label="With textarea" maxlength="100" name="bremarks" ng-model="remark"  ng-required="editmode"></textarea>

                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-4 mb-3" ng-show="editmode" style="padding: 30px 10px 10px 10px;"><label><b>Active/Inactive</b></label>
                                            <label class="switch">
                                                <input type="checkbox" style="margin-top: 13px;" ng-model="active_status" ng-checked="active_status == 'active'" ng-click="toggleSelection($event)">
                                                <span class="slider round"></span>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-success btn-sm" ng-disabled="materialForm.$invalid"  ng-click="saveLineLog()" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        <button ng-click="resetForm();hideForm();" class="btn btn-sm btn-danger"  type="reset" style="padding:4px 21px">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="form-row" ng-show="lineLogData.length == 0">
                <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
                <div class="col-lg-12 text-center" ><h4>(Please Select the above parameters)</h4></div> 
            </div>
            <div class="form-row" style="margin-top:10px;" ng-show="lineLogData.length > 0">   
                <h2 class="f18">Line Master List</h2>
                <table  class="table custom-table">
                    <thead >
                        <tr>
                            <th>Line Code</th>
                            <th>Line Name</th>
                            <th>Equipment Code</th>
                            <th>Block Code</th>
                            <th>Area Code</th>
                            <th>Room Code</th>
                            <th>Last modified By</th>
                            <th>Last modified On</th>
                            <th>Action</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr dir-paginate="dataObj in lineLogData|itemsPerPage:records_per_page" total-items="total_records" style="background:none;">
                            <td>{{dataObj.line_log_code}}</td>
                            <td>{{dataObj.line_log_name}}</td>
                            <td>{{dataObj.equipment_code}}</td>
                            <td>{{dataObj.block_code}}</td>
                            <td>{{dataObj.area_code}}</td>
                            <td>{{dataObj.room_code}}</td>
                            <td>{{dataObj.modified_by!=NULL?dataObj.modified_by:dataObj.created_by}}</td>
                            <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                            <td>
                                <?php if ($edit) { ?>
                                    <a type="button" ng-click="editLineLogMaster(dataObj)"><i class="far fa-edit text-blue pr10"></i></a>
                                    <!-- <button class="btn btn-sm btn-info" ng-click="editLineLogMaster(dataObj)">Edit</button>
                                    <button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)">Deactivate</button> -->
                                <?php } else { ?>
                                    <a type="button" ng-click="editLineLogMaster(dataObj)"><i class="far fa-edit text-blue pr10"></i></a>
                                    <!-- <button class="btn btn-sm btn-info" ng-click="editLineLogMaster(dataObj)" disabled>Edit</button>
                                    <button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.id)" disabled>Deactivate</button> -->
                                <?php } ?>

                            </td>
                            <td ng-if="dataObj.status == 'inactive'" class="bg-danger text-white">Inactive</td>
                            <td ng-if="dataObj.status == 'active'" class="bg-success text-white">Active</td>
                        </tr>
                    </tbody>
                </table>
                <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getLineLogList(newPageNumber)"></dir-pagination-controls>
            </div>
        </div>
    </div>	

</div>

<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>

<script type="text/javascript">
                                var app = angular.module("materialApp", ['angular.chosen', 'angularUtils.directives.dirPagination']);
                                app.directive('chosen', function ($timeout) {

                                    var linker = function (scope, element, attr) {
                                        scope.$watch('masterRoomList', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);
                                        scope.$watch('blockList', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);
                                        scope.$watch('areaList', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);
                                        scope.$watch('fixedEquipmentData', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);
                                        $timeout(function () {
                                            element.chosen();
                                        }, 0, false);
                                    };
                                    return {
                                        restrict: 'A',
                                        link: linker
                                    };
                                });
                                app.filter('format', function () {
                                        return function (item) {
                                            var t = item.split(/[- :]/);
                                            var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                            var time = d.getTime();
                                            return time;
                                        };
                                    });
                                app.controller("materialCtrl", function ($scope, $http, $filter) {
                                    $scope.blockList = [];
                                    $scope.getBlockList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetBlockList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.blockList = response.data.block_data;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");
                                        });
                                    }

                                    $scope.getBlockList();
                                    $scope.areaList = [];
                                    $scope.getAreaList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetAreaList?block_code=' + $scope.block_code,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.areaList = response.data.area_data;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");
                                        });
                                    }


                                    $scope.masterRoomList = [];
                                    $scope.getMasterRoomList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Roomlist?area_code=' + $scope.area_code,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.masterRoomList = response.data.room_data;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
//                                                $scope.getMasterRoomList();
                                    $scope.showAddForm = false;
                                    $scope.showForm = function () {
                                        $scope.showAddForm = true;
                                    }
                                    $scope.remark = '';
                                    $scope.hideForm = function () {
                                        $scope.showAddForm = false;
                                        $scope.editmode = false;
                                        $scope.recordid = '';
                                        delete $scope.block_code;
                                        delete $scope.area_code;
                                        delete $scope.room_code;
                                        delete $scope.equipmentData;
                                        $scope.remark = '';
                                        $scope.active_status = '';
                                    }
                                    $scope.fixedEquipmentData = [];
                                    $scope.getFixedEquimentList = function (room_code) {
                                        if ($scope.recordid > 0) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getfixedequiplist?room_code=' + room_code,
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.fixedEquipmentData = response.data.fixed_euip_list;
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");
                                            });
                                        } else {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getfixedequiplist?room_code=' + room_code + "&in_line=0",
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.fixedEquipmentData = response.data.fixed_euip_list;
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");
                                            });
                                        }

                                    }
                                    $scope.equipmentData = [];
                                    $scope.saveLineLog = function () {
                                        var active_status = ($scope.active_status === undefined) ? 'active' : $scope.active_status
                                        var msg = 'Save';
                                        if ($scope.recordid > 0) {
                                            var msg = 'Update';
                                        }

                                        if (confirm("Do You Want To " + msg + " This Record?")) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/saveLineLogData',
                                                method: "POST",
                                                data: "equipmentData=" + encodeURIComponent(angular.toJson($scope.equipmentData)) + "&room_code=" + $scope.room_code + "&line_log_code=" + $scope.line_log_code + "&line_log_name=" + $scope.line_log_name + "&block_code=" + $scope.block_code + "&area_code=" + $scope.area_code + "&recordid=" + $scope.recordid + "&status=" + active_status + "&remark=" + $scope.remark,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            })
                                                    .then(function (response) {
                                                        /*if (response.data.exclude_equipment.length > 0) {
                                                         alert(response.data.exclude_equipment.toString() + 'already Exists');
                                                         } else {
                                                         alert(response.data.message);
                                                         }*/
                                                        alert(response.data.message);
                                                        $scope.editmode = false;
                                                        $scope.showAddForm = false;
                                                        $scope.equipmentData = [];
                                                        $scope.room_code = "";
                                                        $scope.line_log_code = "";
                                                        $scope.line_log_name = "";
                                                        $scope.block_code = "";
                                                        $scope.area_code = "";
                                                        $scope.recordid = "";
                                                        $scope.remark = "";
                                                        $scope.active_status = "";
                                                        $scope.fixedEquipmentData = [];
                                                        $scope.getLineLogList($scope.page);
                                                    },
                                                            function (response) { // optional
                                                                $scope.showAddForm = false;
                                                                $scope.equipmentData = [];
                                                                $scope.room_code = "";
                                                                $scope.line_log_code = "";
                                                                $scope.line_log_name = "";
                                                                $scope.block_code = "";
                                                                $scope.area_code = "";
                                                                $scope.remark = "";
                                                                $scope.recordid = "";
                                                                $scope.fixedEquipmentData = [];
                                                                console.log("Something went wrong.Please try again");
                                                            });
                                        }
                                    }


                                    $scope.lineLogData = [];
                                    $scope.page = 1;
                                    $scope.total_records = 0;
                                    $scope.records_per_page = 10;
                                    $scope.getLineLogList = function (page) {
                                        $scope.page = page;
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetLineLogList?page=' + page,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.lineLogData = response.data.line_log_data;
                                            $scope.total_records = response.data.total_records;
                                            $scope.records_per_page = response.data.records_per_page;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getLineLogList($scope.page);
                                    //Created By : Bhupendra Kumar
                                    //Date : 18 March 2020
                                    //Decription : For Deactivate the Selected Record
                                    $scope.CommonDelete = function (id) {
                                        var tblname = "pts_mst_line_log";
                                        var colname = "id";
                                        if (confirm("Do You Want To Deactivate This Record ?")) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/CommonDelete',
                                                method: "POST",
                                                data: "id=" + id + "&tblname=" + tblname + "&colname=" + colname,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                alert(response.data.message);
                                                $scope.getLineLogList($scope.page);
                                            }, function (error) { // optional
                                                alert(response.data.message);
                                                //console.log("Something went wrong.Please try again");
                                            });
                                        }
                                    }

                                    //Created By : Bhupendra Kumar
                                    //Date : 18 March 2020
                                    //Decription : To get values of selected record
                                    $scope.recordid = "";
                                    $scope.editLineLogMaster = function (dataObj) {
                                        // console.log(dataObj);
<?php if ($edit == false) { ?>
                                            return false;
<?php } ?>
                                        $scope.editmode = true;
                                        $scope.recordid = dataObj.id;
                                        $scope.line_log_name = dataObj.line_log_name;
                                        $scope.line_log_code = dataObj.line_log_code;
                                        $scope.block_code = dataObj.block_code;
                                        $scope.getAreaList();
                                        $scope.area_code = dataObj.area_code;
                                        $scope.getMasterRoomList();
                                        $scope.room_code = dataObj.room_code;
                                        $scope.getFixedEquimentList($scope.room_code);
                                        $scope.equipcode = [];
                                        $scope.equipcode = dataObj.equipment_code.split(",");
                                        $scope.equipmentData = $scope.equipcode;
                                        $scope.showAddForm = true;
                                        $scope.active_status = dataObj.status;
                                        $scope.remark = "";
                                    }

                                    $scope.getcheck = function () {
                                    }

                                    $scope.getcheck();

                                    //created By : Bhupendra kumar
                                    //Date : 6th April 2020
                                    //Description : to check the record is already exist or not on given parameter
                                    $scope.CommonCheckIsExist = function (code) {
                                        var tblname = "pts_mst_line_log";
                                        var col = "line_log_code";
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/chkcode',
                                            method: "POST",
                                            data: "col=" + col + "&tblname=" + tblname + "&code=" + code,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (res) {
                                            if (res.data.status == 1) {
                                                alert("Record already exists at this code.");
                                                $scope.line_log_code = "";
                                            }
                                        }, function (error) { // optional
                                        });
                                    }
                                    //created By : Bhupendra kumar
                                    //Date : 6th April 2020
                                    //Description : to check the record is already exist or not on given parameter
                                    $scope.CommonCheckNameExist = function (code) {
                                        var tblname = "pts_mst_line_log";
                                        var col = "line_log_name";
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/chkcode',
                                            method: "POST",
                                            data: "col=" + col + "&tblname=" + tblname + "&code=" + code,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (res) {
                                            if (res.data.status == 1) {
                                                alert("Record already exists at this Name.");
                                                $scope.line_log_name = "";
                                                $scope.line_log_code = "";
                                            }
                                        }, function (error) { // optional
                                        });
                                    }
                                    $scope.toggleSelection = function (event) {
                                        if (event.target.checked == true) {
                                            $scope.active_status = 'active';
                                        } else {
                                            $scope.active_status = 'inactive';
                                        }
                                    };
                                });
</script>