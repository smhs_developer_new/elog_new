<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
    <!-- Page Heading -->

    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row">

                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">    	   
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th colspan="5" class="f18" style="vertical-align:middle">Work Assign Master </th>
                                <th class="text-right"> 
                                    <?php if ($add) { ?>
                                        <button class="btn btn-success"  ng-click="showForm()" ng-disabled="editmode">Add (+)</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success"  ng-click="showForm()">Add (+)</button>
                                    <?php } ?>
                                </th>
                            </tr>
                        </thead>
                    </table>  
                </div>
                <div class="col-12">
                    <form name="materialForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row">   
                                        <div class="col-lg-4 mb-3"><label>Role Name <span style="color: red">*</span></label>
                                            <select class="chzn-select form-control" tabindex="4" name="role_id" data-placeholder="Search Role" ng-change="getRoleName()" ng-model="role_id" readonly required chosen ng-disabled="recordid > 0">
                                                <option value="">Select Role</option>
                                                <option ng-repeat="dataObj in setRoteList" value="{{dataObj.id}}" ng-hide="dataObj.id == -1 || dataObj.id == 0 || dataObj.role_description == 'Admin'">{{dataObj.role_description}}</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 mb-3"><label>Delegated Role Name <span style="color: red">*</span></label>
                                            <select multiple class="chzn-select form-control" tabindex="4" name="work_id" data-placeholder="Search Role" ng-options="dataObj['id'] as dataObj.role_description for dataObj in roleList"  ng-model="work_id" required chosen>
                                                <option value="">Select Role</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 mb-3" ng-show="editmode" >
                                            <label for="validationServer01">Remarks <span style="color: red" ng-show="editmode">*</span></label>
                                            <textarea class="form-control" aria-label="With textarea" maxlength="100" name="remark" ng-model="remark"  ng-required="editmode"></textarea>

                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-2 mb-3" ng-show="editmode" style="padding: 30px 10px 10px 10px;"><label><b>Active/Inactive</b></label>
                                            <label class="switch">
                                                <input type="checkbox" style="margin-top: 13px;" ng-model="active_status" ng-checked="active_status == 'active'" ng-click="toggleSelection($event)">
                                                <span class="slider round"></span>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-success btn-sm" ng-disabled="materialForm.$invalid"  ng-click="saveWorkAssign()" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        <button ng-click="resetForm();hideForm();" class="btn btn-sm btn-danger"  type="reset">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="form-row" ng-show="assignRoleList.length == 0">
                <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
                <div class="col-lg-12 text-center" ><h4>(Please Select the above parameters)</h4></div> 
            </div>
            <div class="form-row" style="margin-top:10px;" ng-show="assignRoleList.length > 0">   
                <h2 class="f18">Work Assign Master List</h2>
                <table  class="table custom-table">
                    <thead >
                        <tr>
                            <th>Role Name</th>
                            <th>Delegated Role Name</th>
                            <th>Last Modified By</th>
                            <th>Last Modified On</th>
                            <th>Action</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr dir-paginate="dataObj in assignRoleList|itemsPerPage:records_per_page" total-items="total_records" style="background:none;">
                            <td>{{dataObj.role_name}}</td>
                            <td>{{dataObj.workassign_to_roleid}}</td>
                            <td>{{dataObj.modified_by!=null?dataObj.modified_by:dataObj.created_by}}</td>
                            <td ng-if="dataObj.modified_on != NULL">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                            <td ng-if="dataObj.modified_on == NULL">{{dataObj.created_on|format| date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                            <td>
                                <?php if ($edit) { ?>
                                    <!-- <button class="btn btn-sm btn-info" ng-click="editworkassignMaster(dataObj)">Edit</button>
                                <button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.roleid)">Deactivate</button> -->
                                    <a type="button" ng-click="editworkassignMaster(dataObj)"><i class="far fa-edit text-blue pr10"></i></a>
                                <?php } else { ?>
                                    <!-- <button class="btn btn-sm btn-info" ng-click="editworkassignMaster(dataObj)" disabled>Edit</button>
                                    <button class="btn btn-sm btn-success" ng-click="CommonDelete(dataObj.roleid)" disabled>Deactivate</button> -->
                                    <a type="button" ng-click="editworkassignMaster(dataObj)" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                <?php } ?>
                            </td>
                            <td ng-if="dataObj.isactive == '0'" class="bg-danger text-white">Inactive</td>
                            <td ng-if="dataObj.isactive == '1'" class="bg-success text-white">Active</td>
                        </tr>
                    </tbody>
                </table>
                <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getAssignRoleList(newPageNumber)"></dir-pagination-controls>
            </div>
        </div>
    </div>	

</div>

<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>

<script type="text/javascript">
                                var app = angular.module("materialApp", ['angular.chosen', 'angularUtils.directives.dirPagination']);
                                app.directive('chosen', function ($timeout) {

                                    var linker = function (scope, element, attr) {

                                        scope.$watch('roleList', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);

                                        $timeout(function () {
                                            element.chosen();
                                        }, 0, false);
                                    };
                                    return {
                                        restrict: 'A',
                                        link: linker
                                    };
                                });
                                app.filter('format', function () {
                                    return function (item) {
                                        var t = item.split(/[- :]/);
                                        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                        var time = d.getTime();
                                        return time;
                                    };
                                });
                                app.controller("materialCtrl", function ($scope, $http, $filter) {
                                    $scope.roleList = [];
                                    $scope.setRoteList = [];
                                    $scope.getRoleList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList?status=active',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.roleList = response.data.role_list;
                                            $scope.setRoteList = response.data.role_list;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getRoleList();
                                    $scope.getRoleName = function () {
                                        $scope.work_id = [];
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList?status=active',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.roleList = response.data.role_list;
                                            var role_name = $filter('filter')($scope.setRoteList, {id: $scope.role_id})[0].role_description;
                                            if (role_name == 'IPQA/QC') {
                                                $scope.roleList = $filter('filter')($scope.roleList, {id: $scope.role_id});
                                            } else {
                                                $scope.roleList = $scope.roleList.filter(function (obj) {
                                                    return obj.role_description !== 'IPQA/QC';
                                                });
                                            }
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");

                                        });


                                    }
                                    $scope.assignRoleList = [];
                                    $scope.page = 1;
                                    $scope.total_records = 0;
                                    $scope.records_per_page = 10;
                                    $scope.getAssignRoleList = function (page) {
                                        $scope.page = page;
                                        $scope.assignRoleList = [];
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetAssignRoleList?page=' + page,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.total_records = response.data.total_records;
                                            $scope.records_per_page = response.data.records_per_page;
                                            //$scope.assignRoleList = response.data.role_list;
                                            angular.forEach(response.data.role_list, function (value, key) {
                                                $scope.getAllRoles(value["roleid"], value);
                                            });
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getAllRoles = function (objId, allValues) {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetWorkAssignRoleListofselectedrole',
                                            method: "POST",
                                            data: "roleid=" + objId,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.workassign_to_roleid = "";
                                            angular.forEach(response.data.role_list, function (value, key) {
                                                if (key == 0)
                                                {
                                                    $scope.workassign_to_roleid = $scope.getRoleInfo(value["workassign_to_roleid"]);
                                                } else
                                                {
                                                    $scope.workassign_to_roleid = $scope.workassign_to_roleid + " , " + $scope.getRoleInfo(value["workassign_to_roleid"]);
                                                }
                                            });
                                            // console.log('+++',allValues);
                                            allValues.workassign_to_roleid = $scope.workassign_to_roleid;
                                            $scope.assignRoleList.push(allValues);
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getAssignRoleList($scope.page);
                                    $scope.showAddForm = false;
                                    $scope.showForm = function () {
                                        $scope.remark = "";
                                        $scope.showAddForm = true;
                                    }
                                    $scope.remark = '';
                                    $scope.hideForm = function () {
                                        $scope.showAddForm = false;
                                        $scope.editmode = false;

                                        $scope.recordid = '';
                                        delete $scope.role_id;
                                        $scope.work_id = [];
                                        $scope.remark = '';
                                        $scope.active_status = 1;

                                    }
                                    $scope.getRoleInfo = function (objId) {
                                        var role_name = $filter('filter')($scope.roleList, {id: objId})[0].role_description;
                                        return role_name;
                                    }
                                    $scope.saveWorkAssign = function () {
                                        var active_status = ($scope.active_status === undefined) ? 'active' : $scope.active_status
                                        var msg = 'Save';
                                        if ($scope.recordid > 0) {
                                            var msg = 'Update';
                                        }

                                        if (confirm("Do You Want To " + msg + " This Record?")) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/SaveWorkAssign',
                                                method: "POST",
                                                data: "role_id=" + $scope.role_id + "&work_id=" + encodeURIComponent(angular.toJson($scope.work_id)) + "&recordid=" + $scope.recordid + "&isactive=" + active_status + "&remark=" + $scope.remark,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            })
                                                    .then(function (response) {
                                                        alert(response.data.message);
                                                        $scope.showAddForm = false;
                                                        $scope.editmode = false;
                                                        $scope.role_id = "";
                                                        $scope.recordid = '';
                                                        $scope.work_id = [];
                                                        $scope.remark = "";
                                                        $scope.active_status = "";
                                                        $scope.getAssignRoleList($scope.page);
                                                        window.location.reload();
                                                    },
                                                            function (response) { // optional
                                                                $scope.showAddForm = false;
                                                                $scope.editmode = false;
                                                                $scope.getAssignRoleList($scope.page);
                                                                window.location.reload();
                                                                $scope.role_id = "";
                                                                $scope.recordid = '';
                                                                $scope.work_id = [];
                                                                $scope.remark = "";
                                                                $scope.active_status = "";
                                                                console.log("Something went wrong.Please try again");

                                                            });
                                        }
                                    }

                                    //Created By : Bhupendra Kumar
                                    //Date : 18 March 2020
                                    //Decription : For Deactivate the Selected Record
                                    $scope.CommonDelete = function (id) {
                                        var tblname = "pts_mst_role_workassign_to_mult_roles";
                                        var colname = "roleid";
                                        if (confirm("Do You Want To Deactivate This Record ?")) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/CommonDelete2',
                                                method: "POST",
                                                data: "id=" + id + "&tblname=" + tblname + "&colname=" + colname,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                alert(response.data.message);
                                                $scope.getAssignRoleList($scope.page);
                                            }, function (error) { // optional
                                                alert(response.data.message);
                                                //console.log("Something went wrong.Please try again");
                                            });
                                        }
                                    }

                                    $scope.rolearray = [];
                                    $scope.RoleListdata = [];
                                    $scope.getEditableRecord = function (roleid) {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetEditableRecord',
                                            method: "POST",
                                            data: "roleid=" + roleid,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.RoleListdata = response.data.role_list;
                                            //console.log($scope.RoleListdata);
                                            for (var i = $scope.RoleListdata.length - 1; i >= 0; i--) {
                                                $scope.rolearray.push($scope.RoleListdata[i].workassign_to_roleid);
                                            }
//                                            console.log($scope.rolearray);
                                            $scope.work_id = $scope.rolearray;
                                            $scope.role_id = roleid;
                                            
                                            $scope.remark = '';
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");
                                        });
                                    }

                                    //Created By : Bhupendra Kumar
                                    //Date : 18 March 2020
                                    //Decription : To get values of selected record

                                    $scope.recordid = "";
                                    $scope.editworkassignMaster = function (dataObj) {
                                        //console.log(dataObj);
<?php if ($edit == false) { ?>
                                            return false;
<?php } ?>
                                        $scope.showAddForm = true;
                                        $scope.editmode = true;
                                        $scope.recordid = dataObj.roleid;
                                        $scope.rolearray = [];
                                        $scope.RoleListdata = [];
                                        $scope.role_id = dataObj.roleid;
                                        $scope.getRoleName();
                                        $scope.getEditableRecord(dataObj.roleid);
                                        $scope.remark = "";
                                        $scope.active_status = (dataObj.isactive == 1) ? 'active' : 'inactive';
                                        $("html, body").animate({scrollTop: 0}, "slow");
                                    }
                                    $scope.toggleSelection = function (event) {
                                        if (event.target.checked == true) {
                                            $scope.active_status = 'active';
                                        } else {
                                            $scope.active_status = 'inactive';
                                        }
                                    };
                                });
</script>