<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
    }
}
?>
<div class="container-fluid" ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>
    <!-- Page Heading -->

    <div class="card card-default mt-4">
        <div class="card-body">
            <div class="form-row">

                <div class="form-row col-lg-12 mb-12" style="margin-top:1px;">    	   
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th colspan="5" class="f18" style="vertical-align:middle">Log Configuration Master </th>
                                <th class="text-right">
                                    <?php if ($add) { ?>
                                        <button class="btn btn-success" ng-click="showForm()" ng-disabled="editmode">Add (+)</button>
                                    <?php } else { ?>
                                        <button class="btn btn-success" ng-click="showForm()" disabled>Add (+)</button>
                                    <?php } ?>
                                </th>
                            </tr>
                        </thead>
                    </table>  
                </div>
                <div class="col-12">
                    <form name="materialForm" novalidate>
                        <div class="card card-default" ng-show="showAddForm">
                            <div class="card-body">
                                <div class="col-auto">
                                    <div class="form-row">
                                        <div class="col-lg-4 mb-3"><label><b>Master Logs</b><span style="color: red">*</span></label>
                                            <select class="form-control" ng-change="getlog()" name="log" ng-model="log" required ng-disabled="editmode">
                                                <option value="" selected disabled>Please Select</option>
                                                <option ng-repeat="dataObj in masterLogData" value="{{dataObj.activity_name}}">{{dataObj.activity_name}}</option>
                                            </select>
                                        </div>   
                                        <div class="col-lg-4 mb-3"><label><b>Field Name</b><span style="color: red">*</span></label>
                                            <select class="form-control" ng-change="getrefresh()" name="field_name" ng-model="field_name" required  ng-disabled="editmode">
                                                <option value="" selected disabled>Please Select</option>
                                                <option ng-show="logdata == 'Vertical Sampler and Dies Cleaning Usages Log'" value="Activity">Activity</option>
                                                <option ng-show="logdata == 'Instrument Log Register' || logdata == 'Equipment/Apparatus Log Register (Friabilator)' || logdata == 'Equipment/Apparatus Log Register (Leak Test)'" value="Stage">Stage</option>
                                                <option ng-show="logdata == 'Instrument Log Register'" value="ResultLeft">ResultLeft</option>
                                                <option ng-show="logdata == 'Instrument Log Register'" value="ResultRight">ResultRight</option>
                                                <option ng-show="logdata == 'Equipment/Apparatus Log Register (Friabilator)'" value="cleaning_verification">cleaning_verification</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-4 mb-3"><label><b>Field Value</b><span style="color: red">*</span></label>
                                            <input type="text" class="form-control" name="field_value" ng-model="field_value"  ng-readonly="editmode">
                                        </div>

                                    </div>
                                    <div class="form-row">
                                        <div class="col-lg-4 mb-3"  ng-show="editmode">
                                            <label for="validationServer01">Remarks <span style="color: red" ng-show="editmode">*</span></label>
                                            <textarea class="form-control" aria-label="With textarea" maxlength="100" name="bremarks" ng-model="remark"  ng-required="editmode"></textarea>
                                        </div>
                                        <div class="col-lg-2 mb-3" ng-show="editmode" style="padding: 30px 10px 10px 10px;"><label><b>Active/Inactive</b></label>
                                            <label class="switch">
                                                <input type="checkbox" style="margin-top: 13px;" ng-model="active_status" ng-checked="active_status == 'active'" ng-click="toggleSelection($event)">
                                                <span class="slider round"></span>

                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <button class="btn btn-success btn-sm" ng-disabled="materialForm.$invalid || field_value == ''"  ng-click="saveStage()" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button>
                                        <button ng-click="resetForm();hideForm();" class="btn btn-sm btn-danger"  type="reset">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br>
            <div class="form-row" ng-show="stageData.length == 0">
                <div class="col-lg-12 text-center" ><h2>No Data Found</h2></div>
                <div class="col-lg-12 text-center" ><h4>(Please Select the above parameters)</h4></div> 
            </div>
            <div class="form-row" style="margin-top:10px;" ng-show="stageData.length > 0">   
                <h2 class="f18">Log Configuration List</h2>
                <table  class="table custom-table">
                    <thead >
                        <tr>
                            <th>Log Name</th>
                            <th>Input Field</th>
                            <th>Field Value</th>
                            <th>Last Modified By</th>
                            <th>Last Modified On</th>
                            <th>Action</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr dir-paginate="dataObj in stageData|itemsPerPage:records_per_page" total-items="total_records" style="background:none;">
                            <td>{{dataObj.log}}</td>
                            <td>{{dataObj.field_name}}</td>
                            <td>{{dataObj.field_value}}</td>
                            <td>{{dataObj.modified_by!=null?dataObj.modified_by:dataObj.created_by}}</td>
                            <td ng-if="dataObj.modified_on != null && dataObj.modified_on != '0000-00-00 00:00:00'">{{dataObj.modified_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                            <td ng-if="dataObj.modified_on == null || dataObj.modified_on == '0000-00-00 00:00:00'">{{dataObj.created_on|format| date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                            <td>
                                <?php if ($edit) { ?>
                                    <!-- <button type="button" class="btn btn-sm btn-info" ng-click="editStage(dataObj)">Edit</button>&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-sm btn-danger" ng-click="deleteStage(dataObj.id)">Deactivate</button> -->
                                    <a type="button" ng-click="editStage(dataObj)"><i class="far fa-edit text-blue pr10"></i></a>
                                <?php } else { ?>
                                    <!-- <button type="button" class="btn btn-sm btn-info" ng-click="editStage(dataObj)" disabled>Edit</button>&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-sm btn-danger" ng-click="deleteStage(dataObj.id)" disabled>Deactivate</button> -->
                                    <a type="button" ng-click="editStage(dataObj)" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                <?php } ?>

                            </td>
                            <td ng-if="dataObj.status == 'inactive'" class="bg-danger text-white">Inactive</td>
                            <td ng-if="dataObj.status == 'active'" class="bg-success text-white">Active</td>
                        </tr>
                    </tbody>
                </table>
                <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="getStageList(newPageNumber)"></dir-pagination-controls>
            </div>
        </div>
    </div>	

</div>

<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/dirPagination.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>

<script type="text/javascript">
                                var app = angular.module("materialApp", ['angular.chosen', 'angularUtils.directives.dirPagination']);
                                app.filter('format', function () {
                                    return function (item) {
                                        var t = item.split(/[- :]/);
                                        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                        var time = d.getTime();
                                        return time;
                                    };
                                });
                                app.controller("materialCtrl", function ($scope, $http, $filter) {
                                    $scope.logdata = "";
                                    $scope.getlog = function () {
                                        $scope.logdata = $scope.log;
                                        $scope.field_name = "";
                                        $scope.field_value = "";
                                    }
                                    $scope.getrefresh = function () {
                                        $scope.field_value = "";
                                    }
                                    $scope.masterLogData = [];
                                    $scope.getMasterLogList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getmasteractivitylist?activity_type=log',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.masterLogData = response.data.master_activity_list;
//                                            console.log($scope.masterLogData);
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getMasterLogList();
                                    $scope.stageData = [];
                                    $scope.page = 1;
                                    $scope.total_records = 0;
                                    $scope.records_per_page = 10;
                                    $scope.getStageList = function (page) {
                                        $scope.page = page;
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetStageList?page=' + page,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.stageData = response.data.stage_list;
                                            $scope.total_records = response.data.total_records;
                                            $scope.records_per_page = response.data.records_per_page;
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getStageList($scope.page);
                                    $scope.editmode = false;
                                    $scope.showAddForm = false;
                                    $scope.showForm = function () {
                                        $scope.showAddForm = true;
                                    }
                                    $scope.remark = '';
                                    $scope.hideForm = function () {
                                        $scope.showAddForm = false;
                                        $scope.editmode = false;

                                        $scope.stageId = 0;
                                        $scope.field_name = '';
                                        $scope.field_value = '';
                                        $scope.log = '';
                                        $scope.remark = '';
                                        $scope.active_status = '';

                                    }
                                    $scope.field_name = '';
                                    $scope.field_value = '';
                                    $scope.log = '';
                                    $scope.stageId = 0;
                                    $scope.editStage = function (dataObj) {
                                        //console.log(dataObj);
<?php if ($edit == false) { ?>
                                            return false;
<?php } ?>
                                        $scope.editmode = true;
                                        $scope.showAddForm = true;
                                        $scope.log = dataObj.log;
                                        $scope.field_name = dataObj.field_name;
                                        $scope.field_value = dataObj.field_value;
                                        $scope.stageId = dataObj.id;
                                        $scope.remark = "";
                                        $scope.active_status = dataObj.status;
                                        $("html, body").animate({scrollTop: 0}, "slow");
                                    }
                                    $scope.saveStage = function () {
                                        var active_status = ($scope.active_status === undefined) ? 'active' : $scope.active_status
                                        var msg = 'Save';
                                        if ($scope.stageId > 0) {
                                            var msg = 'Update';
                                        }

                                        if (confirm("Do You Want To " + msg + " This Record?")) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/saveStage',
                                                method: "POST",
                                                data: "log=" + $scope.log + "&field_name=" + $scope.field_name + "&field_value=" + $scope.field_value + "&stage_id=" + $scope.stageId + "&status=" + active_status + "&remark=" + $scope.remark,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            })
                                                    .then(function (response) {
                                                        alert(response.data.message);
                                                        $scope.editmode = false;
                                                        $scope.field_name = '';
                                                        $scope.field_value = '';
                                                        $scope.stageId = 0;
                                                        $scope.remark = '';
                                                        $scope.active_status = '';
                                                        $scope.showAddForm = false;
                                                        $scope.getStageList($scope.page);
                                                    },
                                                            function (response) { // optional
                                                                $scope.editmode = false;
                                                                $scope.field_name = '';
                                                                $scope.field_value = '';
                                                                $scope.stageId = 0;
                                                                $scope.remark = '';
                                                                $scope.active_status = '';
                                                                $scope.showAddForm = false;
                                                                $scope.getStageList($scope.page);
                                                                console.log("Something went wrong.Please try again");
                                                            });
                                        }
                                    }

                                    $scope.deleteStage = function (objId) {
                                        if (confirm("Do You Want To Deactivate This Record?")) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/deleteStage',
                                                method: "POST",
                                                data: "id=" + objId,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            })
                                                    .then(function (response) {
                                                        $scope.getStageList($scope.page);
                                                        alert(response.data.message);
                                                    },
                                                            function (response) { // optional
                                                                $scope.getStageList($scope.page);
                                                                console.log("Something went wrong.Please try again");
                                                            });
                                        }
                                    }
                                    $scope.toggleSelection = function (event) {
                                        if (event.target.checked == true) {
                                            $scope.active_status = 'active';
                                        } else {
                                            $scope.active_status = 'inactive';
                                        }
                                    };
                                });
</script>