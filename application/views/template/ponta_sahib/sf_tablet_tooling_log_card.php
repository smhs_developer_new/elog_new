<style type="text/css">
    .table-responsive.noscroll {
        max-height: none;
        height: auto !important;
    }
</style>
<link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<div  ng-app="tabletApp" ng-controller="tabletCtrl" ng-cloak>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>sf_home">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Tablet Tooling Log Card</li>
        </ol>
    </nav>
    <section class="contentBox blueBorder formWrapper">
        <div class="contentHeader">
            <div class="row">
                <div class="col-12">
                    <h1>Header Details</h1>
                </div>
            </div>
        </div>
        <div class="contentBody">
            <form>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <ul class="activityList">
                                <li style="margin-left: 100px;"><a><b>Form No : </b>{{formno}} </a></li>
                                <li style="margin-left: 100px;"><a><b>Version No : </b>{{versionno}} </a></li>
                                <li style="margin-left: 100px;"><a><b>Effective Date : </b>{{effectivedate| date:'dd-MMM-yyyy'}} </a></li>
                                <li style="margin-left: 100px;"><a><b>Retired Date : </b>{{retireddate| date:'dd-MMM-yyyy'}} </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

    <!-- Pending for Approval start -->
    <section class="contentBox blueBorder formWrapper">


        <div class="contentHeader">

            <div class="row">

                <div class="col-8">
                    <h1>Tablet Tooling Log Card</h1>
                </div>

                <div class="col-4 text-right">

                </div>

            </div>

        </div>

        <div class="contentBody" ng-show="ComDataArray.length == 0 || edit == true">
            <div class="row">
                <!-- <div class="col-sm-6">
                    <div class="form-group">
                        <label>Document No.</label>
                        <input type="text" class="form-control" value="{{docno}}" readonly>    
                    </div>
                </div> -->

                <div class="col-sm-12">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Product</label>
                        <select ng-disabled="edit" class="chzn-select form-control" tabindex="4"  name="product_code" ng-change="GetTabletToolingpuchsetdata()" data-placeholder="Search Product No." ng-options="dataObj['product_code'] as dataObj.product_code for dataObj in productData"  ng-model="product_code" required chosen>
                            <option value="">Select Product No</option>
                        </select> 
                    </div>
                    <p><b ng-if="product_desc != ''">{{product_desc}}</b></p>
                </div>
            </div>
            <div class="table-responsive noscroll"  ng-show="punch_set_data.length > 0"> 
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th> 
                            <th>punch set</th>                           
                            <th>Supplier</th>
                            <th>Upper Punch-Qty./Embossing</th>
                            <th>Lower Punch-Qty./Embossing</th>
                            <th>year</th>                            
                            <th>Dimension</th>
                            <th>Shape</th>
                            <th>Machine</th>
                            <th>Die-Qty.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="dataObj in punch_set_data">
                            <td>
                                <input ng-show="dataObj.ischecked == true && edit" ng-disabled="edit" type="radio" name="select{{$index}}"  checked  ng-click="getCheckedObject(dataObj)">
                                <input ng-show="!edit" ng-disabled="edit" type="radio" value="{{dataObj.ischecked}}" name="select" ng-click="getCheckedObject(dataObj)" ng-model="dataObj.ischecked">
                            </td>   
                            <td>{{dataObj.punch_set}}</td>
                            <td>{{dataObj.supplier}}</td>
                            <td>{{dataObj.upper_punch}}/{{dataObj.upper_embossing}}</td>
                            <td>{{dataObj.lower_punch}}/{{dataObj.lower_embossing}}</td>
                            <td>{{dataObj.year}}</td>
                            <td>{{dataObj.dimension}}</td>
                            <td>{{dataObj.shape}}</td>
                            <td>{{dataObj.machine}}</td>
                            <td>{{dataObj.die_quantity}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="row" ng-show="tabletList.length == 0 && product_code != ''">
                <div class="col-sm-12 text-center">
                    <h2>No Data Found on this product Please assign tablet tooling log to this product first.</h2>
                </div>
            </div>
            <div class="contentBody"  ng-show="selectedData.id > 0">
                <form>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" style="margin-top: 15px;">
                                <ul class="activityList" style="font-size: small;">
                                    <li ng-show="!edit" class="{{current == $index?'active':'' }}" ng-repeat="dataObj in activityList" ng-click="initiateActivity(dataObj, $index)">
                                        <a>{{dataObj.activity_name}} </a>
                                    </li>
                                    <li ng-show="edit" class="{{current == $index?'active':'' }}" ng-repeat="dataObj in activityList">
                                        <a>{{dataObj.activity_name}} </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div ng-show="selectedData.id > 0 && current >= 0">                
                <div class="table-responsive noscroll"  ng-show="currentActivity == 'Issuance' && okhit == false"> 
                    <table class="table table-striped" id="subsetlist">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="select_all" ng-model="is_all" ng-click="selectAll()"> Select All</th>
                                <!-- <th></th> -->
                                <th>Punch Subset</th>
                                <th>Status</th>
                                <th>Last Issued</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="dataObj in punch_subset_data" class="item">
                                <td><input type="checkbox" ng-disabled="dataObj.status != 'available'" name="check" ng-value="true" ng-click="getsubset();" data-subsetcode="{{dataObj.punch_set_code}}" data-status="{{dataObj.status}}"></td>
                                <td>{{dataObj.punch_set_code}}</td>
                                <td>{{dataObj.status}}</td>
                                <td>{{dataObj.last_issued}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <div class="formBtnWrap">
                                <button  ng-click="getissued();" data-toggle="modal" ng-disabled="msg != ''" class="button smallBtn primaryBtn">Ok</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row " ng-show="currentActivity == 'No. of Punch/Die Returned to Storage Cabinet'">
                    <div class="col-sm-12 text-center">Total Qty. Returned/ (Record Individual Punch/Die No.)</div>
                </div>
                <div class="row " ng-show="(currentActivity == 'Issuance' && okhit == true) || currentActivity == 'Inspection / Verification Before Use' || currentActivity == 'Inspection After Use'">
                    <div class="col-sm-12 text-center">Total Qty. Issued/ (Record Individual Punch/Die No.)</div>
                </div>                
                <div class="row" ng-show="(currentActivity == 'Issuance' && okhit == true) || currentActivity == 'No. of Punch/Die Returned to Storage Cabinet' || currentActivity == 'Inspection / Verification Before Use' || currentActivity == 'Inspection After Use'">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>U</label>
                            <input type="text" class="form-control" numeric-only name="field_U" ng-blur="chkvalidationwith_selectedmaster()" maxlength="10" ng-model="field_U" readonly>  
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>L</label>
                            <input type="text" class="form-control" ng-blur="chkvalidationwith_selectedmaster()" numeric-only name="field_L" maxlength="10" ng-model="field_L" readonly>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>D</label>
                            <input type="text" class="form-control" ng-blur="chkvalidationwith_selectedmaster()" valid-number name="field_D" maxlength="10" ng-model="field_D" readonly>
                        </div>
                    </div>
                    <div class="col-sm-6" ng-show="currentActivity == 'Inspection / Verification Before Use' || currentActivity == 'Inspection After Use'">
                        <div class="form-group">
                            <label>Damaged (if any)</label>
                            <select multiple ng-disabled="doc_id > 0" class="chzn-select form-control" tabindex="4" data-placeholder="Search punch set no." name="Damage"  ng-model="Damage" ng-change="getdamagedlength();" chosen>
                                <option value="{{dataObj.id}}" ng-hide="dataObj.status == 'available'" ng-disabled="dataObj.status == 'damaged'" ng-repeat="dataObj in punch_subset_data">{{dataObj.punch_set_code}} => {{dataObj.status}}</option>
                            </select> 
                            <lable>Count : {{totdamage}}</label>

                             <!-- <select class="form-control" ng-model="Damage" name="Damage" >
                                <option value="">Select Punch Set Number</option>
                                <option value="{{dataObj.punch_set_code}}" ng-repeat="dataObj in punch_subset_data">{{dataObj.punch_set_code}}</option>
                            </select> -->
                            <!--<input type="text" class="form-control" only-digits name="Damage" maxlength="10" ng-model="Damage">-->
                        </div>
                    </div>
                    <div class="row col-sm-12">
                        <div class="col-sm-12 text-right">
                            <div class="formBtnWrap">
                                <button ng-show="edit" class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancel</button>&nbsp;&nbsp;&nbsp;
                                <button ng-show="edit" ng-disabled="field_U == '' || field_L == '' || field_D == ''" ng-click="setbtntext('update');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Update</button>&nbsp;&nbsp;&nbsp;
                                <button ng-show="!edit && (currentActivity == 'Issuance' && okhit == true) || currentActivity == 'Inspection After Use'"  ng-click="setbtntext('submit');" data-toggle="modal" ng-disabled="field_U == '' || field_L == '' || field_D == ''" data-target="#loginModal" class="button smallBtn primaryBtn">Done By</button>
                                <button ng-show="!edit && (currentActivity == 'No. of Punch/Die Returned to Storage Cabinet' || currentActivity == 'Inspection / Verification Before Use')"  ng-click="setbtntext('submitwithworkflow');" data-toggle="modal" ng-disabled="field_U == '' || field_L == '' || field_D == ''" data-target="#loginModal" class="button smallBtn primaryBtn">Done By</button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row" ng-show="currentActivity == 'Cleaning Before Use' || currentActivity == 'Cleaning After Use'" >
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>From</label>
                            <div moment-picker="from" format="HH:mm" start-view="hour">
                                <input class="form-control" name="from" maxlength="5" placeholder="hh:mm" ng-model="from"  ng-model-options="{updateOn: 'blur'}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>To</label>
                            <div moment-picker="to" format="HH:mm" start-view="hour">
                                <input class="form-control" name="to" maxlength="5" ng-model="to" ng-blur="chktime();" placeholder="hh:mm" ng-model-options="{updateOn: 'blur'}">
                            </div>
                        </div>
                    </div>
                    <div class="row col-sm-12">
                        <div class="col-sm-12 text-right">
                            <div class="formBtnWrap">
                                <button  ng-click="setbtntext('submit');" data-toggle="modal" ng-disabled="from == '' || to == ''" data-target="#loginModal" class="button smallBtn primaryBtn">Done By</button>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="row"  ng-show="currentActivity == 'Usage'">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>B.No.</label>
                            <input type="text" only-alphanum class="form-control" maxlength="10" name="bno" ng-model="bno"> 
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Tab Qty.Nos.</label>
                            <input type="text" class="form-control" ng-blur="chkqty()" valid-number name="tabqty" maxlength="12" ng-model="tabqty">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Cum. Qty.(Lac)</label>
                            <input type="text" class="form-control" ng-blur="chkqty()" valid-number name="cumqty" maxlength="12" ng-model="cumqty">
                        </div>
                    </div>
                    <div class="col-sm-6" ng-show="currentActivity == 'Inspection / Verification Before Use' || currentActivity == 'Inspection After Use'">
                        <div class="form-group">
                            <label>Cum. Qty./Punch(Lac)</label>
                            <input type="text" class="form-control" ng-blur="chkqty()" only-digits name="cumqtypunch" maxlength="10" ng-model="cumqtypunch">
                        </div>
                    </div>

                    <div class="row col-sm-12">
                        <div class="col-sm-12 text-right">
                            <div class="formBtnWrap">
                                <button  ng-click="setbtntext('submit');" data-toggle="modal" ng-disabled="bno == '' || tabqty == '' || cumqty == ''" data-target="#loginModal" class="button smallBtn primaryBtn">Done By</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>


        </div>
        <div class="contentBody" ng-show="ComDataArray.length > 0 && edit == false">
            <div class="row">
                <!--                 <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Document No.</label>
                                        <input type="text" class="form-control" value="{{ComDataArray[0].doc_no}}" readonly>    
                                    </div>
                                </div> -->

                <div class="col-sm-12">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Product</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].product_code}}" readonly>  
                    </div>
                    <p><b ng-if="product_desc != ''">{{product_desc}}</b></p>
                </div>
            </div>
            <div class="table-responsive noscroll"  ng-show="punch_set_data.length > 0"> 
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th> 
                            <th>punch set</th>                           
                            <th>Supplier</th>
                            <th>Upper Punch-Qty./Embossing</th>
                            <th>Lower Punch-Qty./Embossing</th>
                            <th>year</th>                            
                            <th>Dimension</th>
                            <th>Shape</th>
                            <th>Machine</th>
                            <th>Die-Qty.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="dataObj in punch_set_data">

                            <td><input ng-disabled="true" type="radio" name="select" ng-value="true" ng-click="getCheckedObject(dataObj)" ng-model="dataObj.ischecked"></td>

                            <td>{{dataObj.punch_set}}</td>
                            <td>{{dataObj.supplier}}</td>
                            <td>{{dataObj.upper_punch}}/{{dataObj.upper_embossing}}</td>
                            <td>{{dataObj.lower_punch}}/{{dataObj.lower_embossing}}</td>
                            <td>{{dataObj.year}}</td>
                            <td>{{dataObj.dimension}}</td>
                            <td>{{dataObj.shape}}</td>
                            <td>{{dataObj.machine}}</td>
                            <td>{{dataObj.die_quantity}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- <div class="table-responsive noscroll"  ng-show="punch_set_data.length > 0"> 
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Product Code</th>
                            <th>Supplier</th>
                            <th>punch set</th>
                            <th>Dimension</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="dataObj in punch_set_data">
                            <td><input ng-disabled="true" type="radio" name="select" ng-value="true" ng-click="getCheckedObject(dataObj)" ng-model="dataObj.ischecked"></td>
                            <td>{{dataObj.product_code}}</td>
                            <td>{{dataObj.supplier}}</td>
                            <td>{{dataObj.punch_set}}</td>
                            <td>{{dataObj.dimension}}</td>
                        </tr>
                    </tbody>
                </table>
            </div> -->
            <div class="row" ng-show="tabletList.length == 0 && product_code != ''">
                <div class="col-sm-12 text-center">
                    <h2>No Data Found on this product Please assign tablet tooling log to this product first.</h2>
                </div>
            </div>
            <div class="contentBody"  ng-show="selectedData.id > 0">
                <form>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" style="margin-top: 15px;">
                                <ul class="activityList" style="font-size: small;">
                                    <li class="{{current == $index?'active':'' }}" ng-repeat="dataObj in activityList">
                                        <a>{{dataObj.activity_name}} </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div ng-show="selectedData.id > 0 && current >= 0">                
                <div class="table-responsive noscroll"  ng-show="currentActivity == 'Issuance' && okhit == false"> 
                    <table class="table table-striped" id="subsetlist">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="select_all" ng-model="is_all" ng-click="selectAll()"> Select All</th>
                                <!-- <th></th> -->
                                <th>Punch Subset</th>
                                <th>Status</th>
                                <th>Last Issued</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="dataObj in punch_subset_data" class="item">
                                <td><input type="checkbox" ng-disabled="dataObj.status != 'available'" name="check" ng-value="true" ng-click="getsubset();" data-subsetcode="{{dataObj.punch_set_code}}" data-status="{{dataObj.status}}"></td>
                                <td>{{dataObj.punch_set_code}}</td>
                                <td>{{dataObj.status}}</td>
                                <td>{{dataObj.last_issued}}</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <div class="formBtnWrap">
                                <button  ng-click="getissued();" data-toggle="modal" ng-disabled="msg != ''" class="button smallBtn primaryBtn">Ok</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row " ng-show="currentActivity == 'No. of Punch/Die Returned to Storage Cabinet'">
                    <div class="col-sm-12 text-center">Total Qty. Returned/ (Record Individual Punch/Die No.)</div>
                </div>
                <div class="row " ng-show="(currentActivity == 'Issuance' && okhit == true) || currentActivity == 'Inspection / Verification Before Use' || currentActivity == 'Inspection After Use'">
                    <div class="col-sm-12 text-center">Total Qty. Issued/ (Record Individual Punch/Die No.)</div>
                </div>                
                <div class="row" >

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>U</label>
                            <input readonly type="text" class="form-control" numeric-only name="field_U" ng-blur="chkvalidationwith_selectedmaster()" maxlength="10" value="{{ComDataArray[0].U}}" readonly>  
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>L</label>
                            <input readonly type="text" class="form-control" ng-blur="chkvalidationwith_selectedmaster()" numeric-only name="field_L" maxlength="10" value="{{ComDataArray[0].L}}" readonly>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>D</label>
                            <input readonly type="text" class="form-control" ng-blur="chkvalidationwith_selectedmaster()" valid-number name="field_D" maxlength="10" value="{{ComDataArray[0].D}}" readonly>
                        </div>
                    </div>
                    <div class="col-sm-6" ng-show="currentActivity == 'Inspection / Verification Before Use' || currentActivity == 'Inspection After Use'">
                        <div class="form-group">
                            <!-- <label>Damaged (if any)</label>
                            <input readonly type="text" class="form-control" name="Damage" ng-model="Damage"> -->
                            <label>Damaged (if any)</label>
                            <select multiple disabled class="chzn-select form-control" tabindex="4" data-placeholder="Search punch set no." name="Damage"  ng-model="Damage" chosen>
                                <option value="{{dataObj.id}}" ng-hide="dataObj.last_issued == 'no'" ng-disabled="dataObj.status == 'damaged'" ng-repeat="dataObj in punch_subset_data">{{dataObj.punch_set_code}} => {{dataObj.status}}</option>
                            </select>
                            <lable>Count : {{totdamage}}</label>
                        </div>
                    </div>
                    <div class="row col-sm-12">
                        <div class="col-sm-12 text-right">
                            <div class="formBtnWrap">
                                <button ng-click="editTrasaction()" class="button smallBtn primaryBtn" >Edit</button>&nbsp;&nbsp;&nbsp;
                                <!--<button   ng-click="setbtntext('edit');" data-toggle="modal" ng-disabled="msg != ''" data-target="#loginModal" class="button smallBtn primaryBtn">Edit</button>-->
                                <button disabled ng-show="(currentActivity == 'Issuance' && okhit == true) || currentActivity == 'Inspection After Use'"  ng-click="setbtntext('submit');" data-toggle="modal" ng-disabled="msg != ''" data-target="#loginModal" class="button smallBtn primaryBtn">Done By</button>
                                <button disabled ng-show="currentActivity == 'No. of Punch/Die Returned to Storage Cabinet' || currentActivity == 'Inspection / Verification Before Use'"  ng-click="setbtntext('submitwithworkflow');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Done By</button>
                                <button   ng-click="setbtntext('approval');" data-toggle="modal" ng-disabled="msg != ''" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row" ng-show="currentActivity == 'Cleaning Before Use' || currentActivity == 'Cleaning After Use'" >
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>From</label>
                            <div moment-picker="from" format="HH:mm" start-view="hour">
                                <input readonly class="form-control" name="from" maxlength="5" placeholder="hh:mm" ng-model="from"  ng-model-options="{updateOn: 'blur'}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>To</label>
                            <div moment-picker="to" format="HH:mm" start-view="hour">
                                <input readonly class="form-control" name="to" maxlength="5" ng-model="to" ng-blur="chktime();" placeholder="hh:mm" ng-model-options="{updateOn: 'blur'}">
                            </div>
                        </div>
                    </div>
                    <div class="row col-sm-12">
                        <div class="col-sm-12 text-right">
                            <div class="formBtnWrap">
                                <button disabled ng-click="setbtntext('submit');" data-toggle="modal" ng-disabled="msg != ''" data-target="#loginModal" class="button smallBtn primaryBtn">Done By</button>
                                <button  ng-click="setbtntext('submit');" data-toggle="modal" ng-disabled="msg != ''" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="row"  ng-show="currentActivity == 'Usage'">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>B.No.</label>
                            <input readonly type="text" only-alphanum class="form-control" maxlength="10" name="bno" ng-model="bno"> 
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Tab Qty.Nos.</label>
                            <input readonly type="text" class="form-control" ng-blur="chkqty()" valid-number name="tabqty" maxlength="12" ng-model="tabqty">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Cum. Qty.(Lac)</label>
                            <input readonly type="text" class="form-control" ng-blur="chkqty()" valid-number name="cumqty" maxlength="12" ng-model="cumqty">
                        </div>
                    </div>
                    <div class="col-sm-6" ng-show="currentActivity == 'Inspection / Verification Before Use' || currentActivity == 'Inspection After Use'">
                        <div class="form-group">
                            <label>Cum. Qty./Punch(Lac)</label>
                            <input readonly type="text" class="form-control" ng-blur="chkqty()" only-digits name="cumqtypunch" maxlength="10" ng-model="cumqtypunch">
                        </div>
                    </div>

                    <div class="row col-sm-12">
                        <div class="col-sm-12 text-right">
                            <div class="formBtnWrap">
                                <button disabled  ng-click="setbtntext('submit');" data-toggle="modal" ng-disabled="msg != ''" data-target="#loginModal" class="button smallBtn primaryBtn">Done By</button>
                                <button  ng-click="setbtntext('submit');" data-toggle="modal" ng-disabled="msg != ''" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>


        </div>


    </section>
    <!-- Pending for Approval end-->
    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>js/moment-with-locales.js"></script>
<script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script type="text/javascript">
                                    var app = angular.module("tabletApp", ['angular.chosen', 'moment-picker', 'blockUI']);
                                    app.directive('chosen', function ($timeout) {

                                        var linker = function (scope, element, attr) {

                                            scope.$watch('productData', function () {
                                                $timeout(function () {
                                                    element.trigger('chosen:updated');
                                                }, 0, false);
                                            }, true);
                                            scope.$watch('punch_subset_data', function () {
                                                $timeout(function () {
                                                    element.trigger('chosen:updated');
                                                }, 0, false);
                                            }, true);
                                            $timeout(function () {
                                                element.chosen();
                                            }, 0, false);
                                        };
                                        return {
                                            restrict: 'A',
                                            link: linker
                                        };
                                    });
                                    app.directive('onlyAlphanum', function () {
                                        return {
                                            require: 'ngModel',
                                            link: function (scope, element, attr, ngModelCtrl) {
                                                function fromUser(text) {
                                                    var transformedInput = text.replace(/[^0-9a-zA-Z\-\s]/g, '');
                                                    if (transformedInput !== text) {
                                                        ngModelCtrl.$setViewValue(transformedInput);
                                                        ngModelCtrl.$render();
                                                    }
                                                    return transformedInput; // or return Number(transformedInput)
                                                }
                                                ngModelCtrl.$parsers.push(fromUser);
                                            }
                                        };
                                    });
                                    app.directive('numericOnly', function () {
                                        return {
                                            require: '?ngModel',
                                            link: function (scope, element, attrs, ngModelCtrl) {
                                                if (!ngModelCtrl) {
                                                    return;
                                                }
                                                ngModelCtrl.$parsers.push(function (val) {
                                                    if (angular.isUndefined(val)) {
                                                        var val = '';
                                                    }

                                                    var clean = val.replace(/[^-0-9\\.]/g, '');
                                                    var negativeCheck = clean.split('-');
                                                    var decimalCheck = clean.split('.');
                                                    if (!angular.isUndefined(negativeCheck[1])) {
                                                        negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                                                        clean = negativeCheck[0] + '-' + negativeCheck[1];
                                                        if (negativeCheck[0].length > 0) {
                                                            clean = negativeCheck[0];
                                                        }

                                                    }

                                                    if (!angular.isUndefined(decimalCheck[1])) {
                                                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                                                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                                                    }

                                                    if (val !== clean) {
                                                        ngModelCtrl.$setViewValue(clean);
                                                        ngModelCtrl.$render();
                                                    }
                                                    return clean;
                                                });

                                                element.bind('keypress', function (event) {
                                                    if (event.keyCode === 32) {
                                                        event.preventDefault();
                                                    }
                                                });
                                            }
                                        };
                                    });
                                    app.directive('timeOnly', function () {
                                        return {
                                            require: '?ngModel',
                                            link: function (scope, element, attrs, ngModelCtrl) {
                                                if (!ngModelCtrl) {
                                                    return;
                                                }
                                                ngModelCtrl.$parsers.push(function (val) {
                                                    if (angular.isUndefined(val)) {
                                                        var val = '';
                                                    }

                                                    var clean = val.replace(/[^-0-9\:.]/g, '');
                                                    var negativeCheck = clean.split('-');
                                                    var decimalCheck = clean.split('.');
                                                    if (!angular.isUndefined(negativeCheck[1])) {
                                                        negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                                                        clean = negativeCheck[0] + '-' + negativeCheck[1];
                                                        if (negativeCheck[0].length > 0) {
                                                            clean = negativeCheck[0];
                                                        }

                                                    }

                                                    if (!angular.isUndefined(decimalCheck[1])) {
                                                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                                                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                                                    }

                                                    if (val !== clean) {
                                                        ngModelCtrl.$setViewValue(clean);
                                                        ngModelCtrl.$render();
                                                    }
                                                    return clean;
                                                });

                                                element.bind('keypress', function (event) {
                                                    if (event.keyCode === 32) {
                                                        event.preventDefault();
                                                    }
                                                });
                                            }
                                        };
                                    });
                                    app.directive('onlyDigits', function () {
                                        return {
                                            require: 'ngModel',
                                            restrict: 'A',
                                            link: function (scope, element, attr, ctrl) {
                                                function inputValue(val) {
                                                    if (val) {
                                                        var digits = val.replace(/[^0-9.]/g, '');

                                                        if (digits.split('.').length > 2) {
                                                            digits = digits.substring(0, digits.length - 1);
                                                        }

                                                        if (digits !== val) {
                                                            ctrl.$setViewValue(digits);
                                                            ctrl.$render();
                                                        }
                                                        return parseFloat(digits);
                                                    }
                                                    return undefined;
                                                }
                                                ctrl.$parsers.push(inputValue);
                                            }
                                        };
                                    });
                                    app.directive('validNumber', function () {
                                        return {
                                            require: '?ngModel',
                                            link: function (scope, element, attrs, ngModelCtrl) {
                                                if (!ngModelCtrl) {
                                                    return;
                                                }

                                                ngModelCtrl.$parsers.push(function (val) {
                                                    if (angular.isUndefined(val)) {
                                                        var val = '';
                                                    }

                                                    var clean = val.replace(/[^-0-9\.]/g, '');
                                                    var negativeCheck = clean.split('-');
                                                    var decimalCheck = clean.split('.');
                                                    if (!angular.isUndefined(negativeCheck[1])) {
                                                        negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                                                        clean = negativeCheck[0] + '-' + negativeCheck[1];
                                                        if (negativeCheck[0].length > 0) {
                                                            clean = negativeCheck[0];
                                                        }

                                                    }

                                                    if (!angular.isUndefined(decimalCheck[1])) {
                                                        decimalCheck[1] = decimalCheck[1].slice(0, 2);
                                                        clean = decimalCheck[0] + '.' + decimalCheck[1];
                                                    }

                                                    if (val !== clean) {
                                                        ngModelCtrl.$setViewValue(clean);
                                                        ngModelCtrl.$render();
                                                    }
                                                    return clean;
                                                });

                                                element.bind('keypress', function (event) {
                                                    if (event.keyCode === 32) {
                                                        event.preventDefault();
                                                    }
                                                });
                                            }
                                        };
                                    });
                                    app.controller("tabletCtrl", function ($scope, $http, $filter, blockUI) {
                                        var urlParams = new URLSearchParams(window.location.search);
                                        var room_code = urlParams.get('room_code');
                                        var processid = urlParams.get('processid');
                                        var actid = urlParams.get('actid');
                                        var actname = urlParams.get('actname');

                                        // get header detail 
                                        $scope.headerRecordid = urlParams.get('headerid');
                                        $scope.formno = urlParams.get('formno');
                                        $scope.versionno = urlParams.get('versionno');
                                        $scope.effectivedate = urlParams.get('effectivedate');
                                        $scope.retireddate = urlParams.get('retireddate');
                                        //end

                                        $scope.doc_id = urlParams.get('doc_id');
                                        $scope.product_code = "";
                                        $scope.productData = [];
                                        $scope.mst_mat_id = "";
                                        $scope.currentActivity = '';
                                        $scope.field_U = '';
                                        $scope.field_L = '';
                                        $scope.field_D = '';
                                        $scope.Damage = '';
                                        $scope.from = '';
                                        $scope.to = '';
                                        $scope.bno = '';
                                        $scope.tabqty = '';
                                        $scope.cumqty = '';
                                        $scope.cumqtypunch = '';
                                        $scope.okhit = false;
                                        $scope.punch_set = '';
                                        $scope.subset = '';
                                        $scope.totdamage = 0;

                                        // $scope.getProductList = function () {
                                        //         $http({
                                        //             url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetPunchSetAllocationRecord',
                                        //             method: "GET",
                                        //             headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        //         }).then(function (response) {
                                        //             $scope.productData = response.data.punch_set_data;
                                        //             console.log($scope.productData);
                                        //         }, function (error) { // optional
                                        //             console.log("Something went wrong.Please try again");

                                        //         });
                                        //     }
                                        //     $scope.getProductList();
                                        $scope.selectedsubsetcount = "";
                                        $scope.subssetArray = [];
                                        $scope.getsubset = function () {
                                            $scope.is_all = false;
                                            var myArray = [];
                                            $scope.subset = '';
                                            var val = 0;
                                            $("#subsetlist").find('input[name^="check"]').each(function () {
                                                if ($(this).prop('checked') == true)
                                                {
                                                    var subsetcode = $(this).attr("data-subsetcode");
                                                    if (val == 0) {
                                                        $scope.subset = subsetcode;
                                                    } else {
                                                        $scope.subset = $scope.subset + "," + subsetcode;
                                                    }
                                                    myArray.push({
                                                        subsetcode: subsetcode
                                                    });
                                                    val = 1;
                                                }
                                            });

                                            $scope.subssetArray = myArray;
                                            $scope.selectedsubsetcount = $scope.subssetArray.length;
                                            $scope.field_U = $scope.selectedsubsetcount;
                                            $scope.field_L = $scope.selectedsubsetcount;
                                            $scope.field_D = $scope.selectedsubsetcount;
                                        }

                                        $scope.selectAll = function () {
                                            var myArray = [];
                                            $scope.subset = '';
                                            var val = 0;
                                            if ($scope.is_all) {
                                                $("#subsetlist").find('input[name^="check"]').each(function () {
                                                    var status = $(this).attr("data-status");
                                                    if (status == "available")
                                                    {
                                                        $(this).prop('checked', true);
                                                        var subsetcode = $(this).attr("data-subsetcode");
                                                        if (val == 0) {
                                                            $scope.subset = subsetcode;
                                                        } else {
                                                            $scope.subset = $scope.subset + "," + subsetcode;
                                                        }
                                                        myArray.push({
                                                            subsetcode: subsetcode
                                                        });
                                                        val = 1;
                                                    }

                                                });
                                            } else {
                                                $("#subsetlist").find('input[name^="check"]').each(function () {
                                                    $(this).prop('checked', false);
                                                    $scope.subssetArray = [];
                                                });
                                            }
                                            $scope.subssetArray = myArray;
                                            $scope.selectedsubsetcount = $scope.subssetArray.length;
                                            $scope.field_U = $scope.selectedsubsetcount;
                                            $scope.field_L = $scope.selectedsubsetcount;
                                            $scope.field_D = $scope.selectedsubsetcount;
                                        }

                                        $scope.getissued = function () {
                                            if (($scope.tt_actid == 14 && $scope.tt_acname == "No. of Punch/Die Returned to Storage Cabinet") || ($scope.tt_actid == null && $scope.tt_acname == null))
                                            {
                                                $scope.okhit = true;
                                            } else {
                                                alert("The subsets of this punch set already Issued please released them first using `No. of Punch/Die Returned to Storage Cabinet` tab, Thank You...!");
                                                $scope.okhit = false;
                                                location.reload();
                                                return false;
                                            }
                                        }

                                        $scope.getsubsetissued = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/subsetissue',
                                                method: "POST",
                                                data: "subsets=" + encodeURIComponent(angular.toJson($scope.subssetArray)),
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                //alert(response.data.message);
                                            }, function (error) {
                                                console.log(error);
                                            });
                                        }

                                        $scope.getProductList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.productData = response.data.product_list;
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");

                                            });
                                        }
                                        $scope.docno = "";
                                        $scope.product_desc = "";
                                        $scope.getProductList();
                                        $scope.punch_set_data = [];
//                                        $scope.GetTabletToolingpuchsetdata = function () {
//                                            //$scope.product_desc = $filter('filter')($scope.productData, {product_code: $scope.product_code})[0].product_name;
//                                            $scope.selectedData = {};
//                                            $scope.current = '-1';
//                                            $scope.currentActivity = '';
//                                            $http({
//                                                url: 'Rest/Pontasahibelog/Pontasahib/GetTabletToolingpuchsetdata?product_code=' + $scope.product_code,
//                                                method: "GET",
//                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
//                                            }).then(function (response) {
//                                                $scope.punch_set_data = response.data.punch_set_data;
//                                                if ($scope.ComDataArray.length > 0) {
//                                                    for (var i = 0; i < $scope.punch_set_data.length; i++) {
//                                                        if ($scope.punch_set_data[i]['punch_set'] == $scope.ComDataArray[0].punch_set_number) {
//                                                            $scope.punch_set_data[i]['ischecked'] = true;
//                                                            $scope.tt_actid = $scope.punch_set_data[i]['act_id'];
//                                                            $scope.tt_acname = $scope.punch_set_data[i]['act_name'];
//                                                            $scope.getCheckedObject($scope.punch_set_data[i]);
//                                                        }
//                                                    }
//
//                                                    $scope.getMasterActivityList();
//
//                                                }
//                                            }, function (error) { // optional
//                                                console.log("Something went wrong.Please try again");
//
//                                            });
//                                        }


                                        $scope.GetTabletToolingpuchsetdata = function () {
                                            //$scope.product_desc = $filter('filter')($scope.productData, {product_code: $scope.product_code})[0].product_name;
                                            $scope.selectedData = {};
                                            $scope.current = '-1';
                                            $scope.currentActivity = '';
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetTabletToolingpuchsetdata?product_code=' + $scope.product_code + '&room_code=' + room_code,
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.punch_set_data = response.data.punch_set_data;
                                                if ($scope.ComDataArray.length > 0) {
                                                    for (var i = 0; i < $scope.punch_set_data.length; i++) {
                                                        if ($scope.punch_set_data[i]['punch_set'] == $scope.ComDataArray[0].punch_set_number) {
                                                            $scope.punch_set_data[i]['ischecked'] = true;
                                                            $scope.tt_actid = $scope.punch_set_data[i]['act_id'];
                                                            $scope.tt_acname = $scope.punch_set_data[i]['act_name'];
                                                            $scope.getCheckedObject($scope.punch_set_data[i]);
                                                        }
                                                    }

                                                    $scope.getMasterActivityList();

                                                }
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");

                                            });
                                        }

                                        $scope.selectedData = {};
                                        $scope.punch_subset_data = [];

                                        $scope.tt_actid = "";
                                        $scope.tt_acname = "";
                                        $scope.uld = "";
                                        $scope.supplier="";
                                        $scope.getCheckedObject = function (dataObj) {
                                            $scope.tt_actid = dataObj.act_id;
                                            $scope.tt_acname = dataObj.act_name;
                                            $scope.supplier=dataObj.supplier;
                                            $scope.uld = dataObj.uld;
                                            $scope.field_U = $scope.uld;
                                            $scope.field_L = $scope.uld;
                                            $scope.field_D = $scope.uld;

                                            $scope.punch_subset_data = [];
                                            $scope.selectedData = {};
                                            $scope.current = '-1';
                                            $scope.currentActivity = '';
                                            if (dataObj.ischecked != undefined) {

                                                $scope.selectedData = dataObj;
                                            } else {
                                                $scope.selectedData = {};
                                            }
                                            $scope.punch_set = $scope.selectedData['punch_set'];

                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetTabletToolingpuchsubset?punch_set=' + $scope.punch_set + "&product_code=" + $scope.product_code+"&room_code="+room_code,
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.punch_subset_data = response.data.punch_subset_data[0];
                                                $scope.old_activity = response.data.punch_subset_data[1];
                                                if($scope.old_activity.length>0){
                                                    $scope.ComDataArray = $scope.old_activity;
                                                    var index = $scope.activityList.indexOf($filter('filter')($scope.activityList, {activity_name: $scope.old_activity[0].act_type})[0]);
                                                    var rahul = $filter('filter')($scope.activityList, {activity_name: $scope.old_activity[0].act_type})[0];
                                                    $scope.initiateActivity(rahul, index);
                                                }
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");

                                            });
                                        }
                                        $scope.msg = "";
                                        $scope.chkvalidationwith_selectedmaster = function () {
                                            $scope.msg = "";
                                            if ($scope.selectedData != '')
                                            {
                                                var field_U = $scope.field_U;
                                                var field_L = $scope.field_L;
                                                var field_D = $scope.field_D;
                                                if (parseFloat(field_U) > parseFloat($scope.selectedData['upper_punch_qty'])) {
                                                    alert("U value must be smaller or equal than upper punch of above selected master");
                                                    $scope.msg = "1";
                                                    $scope.field_U = "";
                                                } else if (parseFloat(field_L) > parseFloat($scope.selectedData['lower_punch_qty'])) {
                                                    alert("L value must be smaller or equal than lower punch of above selected master");
                                                    $scope.msg = "1";
                                                    $scope.field_L = "";
                                                } else if (parseFloat(field_D) > parseFloat($scope.selectedData['die_quantity'])) {
                                                    alert("D value must be smaller or equal than die of above selected master");
                                                    $scope.msg = "1";
                                                    $scope.field_D = "";
                                                }
                                            }
                                        }
                                        $scope.chktime = function () {
                                            var from = $scope.from;
                                            var fromtime = from.replace(/:/g, '');
                                            var to = $scope.to;
                                            var totime = to.replace(/:/g, '');
                                            if (parseInt(fromtime) > parseInt(totime)) {
                                                alert('from time must smaller than to time');
                                                //$scope.from="";
                                                $scope.to = "";
                                            }
                                        }
                                        $scope.chkqty = function () {
                                            $scope.msg = "";
                                            var cumqty = $scope.cumqty;
                                            var tabqty = $scope.tabqty;
//                                            if (parseFloat(cumqty) > parseFloat(tabqty)) {
//                                                alert('Cum. Qty must smaller than Tab Qty');
//                                                $scope.cumqty = "";
//                                                $scope.msg = "1";
//                                            }
                                        }
                                        $scope.current = '-1';
                                        $scope.currentActivity = '';
                                        $scope.activityList = [];
                                        $scope.getMasterActivityList = function () {

                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getmasteractivitylist?activity_type=tabletooling',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.activityList = response.data.master_activity_list;
                                                if ($scope.ComDataArray.length > 0) {
                                                    var index = $scope.activityList.indexOf($filter('filter')($scope.activityList, {activity_name: $scope.ComDataArray[0].act_type})[0]);
                                                    var rahul = $filter('filter')($scope.activityList, {activity_name: $scope.ComDataArray[0].act_type})[0];
                                                    $scope.initiateActivity(rahul, index);
//                                                    for (var i = 0; i < $scope.activityList.length; i++) {
//                                                        if ($scope.activityList[i]['activity_name'] == $scope.ComDataArray[0].act_type) {
//                                                            //alert($scope.activityList[i]);
//                                                            $scope.initiateActivity($scope.activityList[i], i);
//                                                        }
//                                                    }
//                                                    $scope.currentActivity = $scope.ComDataArray[0].act_type;
//                                                    alert($scope.currentActivity);

                                                }
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");

                                            });
                                        }
                                        $scope.getMasterActivityList();
                                        //$scope.activityList = ["Issuance", "Cleaning Before Use", "Inspection/Verification Before Use", "Usage", "Cleaning After Use", "Inspection After Use", "No. of Punch/Die Returned to Storage Cabinet"];
                                        $scope.initiateActivity = function (dataObj, index) {
//                                        alert(index);
                                            //if ($scope.tt_actid == null || (parseInt(dataObj.id) >= parseInt($scope.tt_actid))) {
                                            $scope.current = index;
                                            $scope.currentActivity = dataObj.activity_name;
//                                            alert($scope.currentActivity);
                                            $scope.Actid = dataObj.id;
                                            $scope.Actname = dataObj.activity_name;
                                            $scope.okhit = false;
                                            // } else if (parseInt($scope.tt_actid) == 14 && dataObj.id == 8) {
                                            //     $scope.current = index;
                                            //     $scope.currentActivity = dataObj.activity_name;
                                            //     $scope.Actid = dataObj.id;
                                            //     $scope.Actname = dataObj.activity_name;
                                            // } else {
                                            //     $scope.current = '-1';
                                            //     alert("Please select " + $scope.tt_acname + " or another activity in ascending order.");
                                            // }

                                            if ($scope.ComDataArray.length == 0) {
                                                // $scope.field_U = '';
                                                // $scope.field_L = '';
                                                // $scope.field_D = '';
                                                $scope.Damage = '';
                                                $scope.from = '';
                                                $scope.to = '';
                                                $scope.bno = '';
                                                $scope.tabqty = '';
                                                $scope.cumqty = '';
                                                $scope.cumqtypunch = '';
                                                $scope.msg = "";
                                                $scope.totdamage = 0;

                                            }
                                        }

//*******************Bhupendra's code Started**********************//
                                        $scope.getdamagedlength = function ()
                                        {
                                            $scope.totdamage = $scope.Damage.length;
                                        }

                                        $scope.setbtntext = function (btntext)
                                        {

                                            $scope.btntext = btntext;
                                        }
                                        $scope.setfromtime = function (fromtime)
                                        {

                                            $scope.from = fromtime;
                                        }
                                        $scope.settotime = function (totime)
                                        {

                                            $scope.to = totime;
                                        }
                                        //Login Function
                                        //Created by Bhupendra
                                        //Date : 28/01/2020                                    
                                        $scope.login = function () {
                                            var response = confirm("Do you really want to perform this task?");
                                            if (response == true)
                                            {
                                                $('.loader').show();
                                                pwd2 = SHA256($scope.password);
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                                    method: "POST",
                                                    data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&module_id=" + actid + "&module_type=log" + "&action=" + $scope.btntext + "&auth=" + btoa(btoa($scope.password)) + "&act_id=" + $scope.Actid+ "&room_code=" + room_code,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
//                                                    alert(response.data.message);
                                                    if (response.data.message == "")
                                                    {
                                                        if ($scope.btntext == 'approval')
                                                        {
                                                            //Calling Approval function
                                                            //Created by Bhupendra
                                                            //Date : 10/02/2020
                                                            $scope.getapproved(room_code, $scope.ComDataArray[0].id, $scope.ComDataArray[0].activity_id, $scope.ComDataArray[0].workflowstatus, $scope.ComDataArray[0].workflownextstep, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, 'stop', $scope.ComDataArray[0].doc_id)
                                                        } else if ($scope.btntext == 'submit')
                                                        {
                                                            //Calling Stoped function
                                                            //Created by Bhupendra
                                                            //Date : 10/02/2020

                                                            $scope.getsubmit(room_code, processid, actid, actname, $scope.docno, $scope.mst_mat_id, $scope.currentActivity, $scope.field_U, $scope.field_L, $scope.field_D, encodeURIComponent(angular.toJson($scope.Damage)), $scope.from, $scope.to, $scope.bno, $scope.tabqty, $scope.cumqty, $scope.cumqtypunch, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.product_code, $scope.punch_set, $scope.subset, $scope.Actid,$scope.supplier)

                                                        } else if ($scope.btntext == 'submitwithworkflow')
                                                        {
                                                            //Calling Stoped function
                                                            //Created by Bhupendra
                                                            //Date : 10/02/2020

                                                            $scope.getsubmitwithworkflow(room_code, processid, $scope.Actid, $scope.Actname, $scope.docno, $scope.mst_mat_id, $scope.currentActivity, $scope.field_U, $scope.field_L, $scope.field_D, encodeURIComponent(angular.toJson($scope.Damage)), $scope.from, $scope.to, $scope.bno, $scope.tabqty, $scope.cumqty, $scope.cumqtypunch, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.product_code, $scope.punch_set, $scope.subset, $scope.Actid,$scope.supplier)

                                                        } else if ($scope.btntext == 'update')
                                                        {
                                                            var role = "";
                                                            if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                                if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                                    var workflowtype = 'start';
                                                                } else {
                                                                    var workflowtype = 'stop';
                                                                }

                                                                var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                                var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                            } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep == 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {

                                                                var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflowstatus})[0].role_id;
                                                                var role = $filter('filter')($scope.roleList, {id: desc})[0].id;

                                                            } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time != null) {
                                                                if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                                    var workflowtype = 'start';
                                                                } else {
                                                                    var workflowtype = 'stop';
                                                                }
                                                                var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                                var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                            }
                                                            var userdata = response.data.userdata;
                                                            if (userdata.role_id >= $scope.ComDataArray[0].last_approval.role_id && userdata.role_id > 1) {
                                                                $scope.updatesubmit(room_code, processid, $scope.Actid, $scope.Actname, $scope.ComDataArray[0].doc_no, $scope.mst_mat_id, $scope.currentActivity, $scope.field_U, $scope.field_L, $scope.field_D, encodeURIComponent(angular.toJson($scope.Damage)), $scope.from, $scope.to, $scope.bno, $scope.tabqty, $scope.cumqty, $scope.cumqtypunch, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.ComDataArray[0].product_code, $scope.punch_set, $scope.subset, $scope.ComDataArray[0].edit_id, $scope.ComDataArray[0].is_in_workflow)

                                                            } else {
                                                                $('.loader').hide();
                                                                alert("You are not allowed to edit this.");
                                                                $scope.edit = false;
                                                            }
                                                        }
                                                    } else {
                                                        $('.loader').hide();
                                                        alert(response.data.message);
                                                    }
                                                }, function (error) {
                                                    $('.loader').hide();
                                                    console.log(error);
                                                });
                                            }
                                        }
                                        //End Login Function

                                        //Approval function :- To Approve running activity by auth & privilege chk as well as sent that activity to next approval which exists in workflow
                                        //Created by Bhupendra
                                        //Date : 28/01/2020
                                        $scope.getapproved = function (roomcode, actlogtblid, actid, workflowstatus, workflownextstep, roleid, empid, empname, email, remark, actstatus, docno) {

                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/com_approval_fun',
                                                method: "POST",
                                                data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&workflowstatus=" + workflowstatus + "&workflownextstep=" + workflownextstep + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&actstatus=" + actstatus + "&docno=" + docno + "&tblname=pts_trn_tablet_tooling",

                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                alert(response.data.message);
                                                if (response.data.next_step == "-1")
                                                {
                                                    window.location.href = "<?php echo base_url() ?>home";
                                                } else
                                                {
                                                    location.reload();
                                                    $('.loader').hide();
                                                    // $scope.resetLoginForm();
                                                    // angular.element("#btnclose").trigger('click');
                                                    // $scope.getLastFilterActivity();
                                                    //$scope.getdocno();
                                                }


                                            }, function (error) {
                                            $('.loader').hide();
                                                console.log(error);
                                            });
                                        }
                                        //*********************************************************************************
                                        //Start function :- To Start new activity by auth chk.
                                        //Created by Bhupendra
                                        //Date : 4/02/2020
                                        $scope.getsubmit = function (roomcode, processid, actid, actname, docno, mst_mat_id, currentActivity, U, L, D, Damage, from, to, bno, tabqty, cumqty, cumqtypunch, roleid, empid, empname, email, remark, product_code, punch_set, Subset, tt_actid,supplier) {

                                            if (currentActivity == 'Usage' && $scope.uld > 0) {
                                                cumqtypunch = cumqty / $scope.uld;
                                                cumqtypunch = cumqtypunch.toFixed(2);
                                            }
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/submit_tablet_tooling_log_record',
                                                method: "POST",
                                                data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&mst_mat_id=" + mst_mat_id + "&currentActivity=" + currentActivity + "&U=" + U + "&L=" + L + "&D=" + D + "&Damage=" + Damage + "&from=" + from + "&to=" + to + "&bno=" + bno + "&tabqty=" + tabqty + "&cumqty=" + cumqty + "&cumqtypunch=" + cumqtypunch + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&product_code=" + product_code + "&punch_set=" + punch_set + "&Subset=" + Subset + "&tt_actid=" + tt_actid + "&headerRecordid=" + $scope.headerRecordid+"&supplier="+supplier,

                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                alert(response.data.message);
                                                //console.log(response.data.acttblid);
                                                $scope.doc_id = response.data.acttblid.doc_no;
                                                if(response.data.result==true){
                                                    if (currentActivity == 'Issuance')
                                                {
                                                    $scope.getsubsetissued();
                                                }
                                                }
                                                
                                                // $scope.resetLoginForm();
                                                // $scope.selectedData.id = 0;
                                                // $scope.tabletList = [];
                                                // $scope.punch_set_data = [];
                                                // $scope.product_code = '';
                                                // $scope.current = '-1';
                                                // $scope.currentActivity = '';
                                                // $scope.field_U = '';
                                                // $scope.field_L = '';
                                                // $scope.field_D = '';
                                                // $scope.Damage = '';
                                                // $scope.from = '';
                                                // $scope.to = '';
                                                // $scope.bno = '';
                                                // $scope.tabqty = '';
                                                // $scope.cumqty = '';
                                                // $scope.cumqtypunch = '';
                                                // $scope.punch_set = '';
                                                // $scope.Subset = '';
                                                // angular.element("#btnclose").trigger('click');
                                                // $scope.getLastFilterActivity();
//                                                
                                                window.location.reload();
                                                $('.loader').hide();

                                            }, function (error) {
                                                $('.loader').hide();
                                                console.log(error);
                                            });
                                        }



//                                        $scope.getsubmitwithworkflow = function (roomcode, processid, actid, actname, docno, mst_mat_id, currentActivity, U, L, D, Damage, from, to, bno, tabqty, cumqty, cumqtypunch, roleid, empid, empname, email, remark, product_code, punch_set, Subset, tt_actid,supplier) {
//                                            $http({
//                                                url: 'Rest/Pontasahibelog/Pontasahib/submit_tablet_tooling_log_record_withworkflow',
//                                                method: "POST",
//                                                data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&mst_mat_id=" + mst_mat_id + "&currentActivity=" + currentActivity + "&U=" + U + "&L=" + L + "&D=" + D + "&Damage=" + Damage + "&from=" + from + "&to=" + to + "&bno=" + bno + "&tabqty=" + tabqty + "&cumqty=" + cumqty + "&cumqtypunch=" + cumqtypunch + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&product_code=" + product_code + "&punch_set=" + punch_set + "&Subset=" + Subset + "&tt_actid=" + tt_actid + "&headerRecordid=" + $scope.headerRecordid+"&supplier="+supplier,
//
//                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
//                                            }).then(function (response) {
//                                                alert(response.data.message);
//                                                console.log(response.data.acttblid);
//                                                $scope.doc_id = response.data.acttblid.doc_no;
//                                                if ($scope.doc_id != '') {
//                                                    var uri = window.location.href;
//                                                    window.location.href = updateQueryStringParameter(uri, "doc_id", $scope.doc_id);
//                                                } else {
//                                                    window.location.href = window.location.href + "&doc_id=" + $scope.doc_id;
//                                                }
//                                                //window.location.reload();
//                                                $('.loader').hide();
//
//                                            }, function (error){
//                                                $('.loader').hide();
//                                                console.log(error);
//                                            });
//                                        }



                                       $scope.getsubmitwithworkflow = function (roomcode, processid, actid, actname, docno, mst_mat_id, currentActivity, U, L, D, Damage, from, to, bno, tabqty, cumqty, cumqtypunch, roleid, empid, empname, email, remark, product_code, punch_set, Subset, tt_actid,supplier) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/submit_tablet_tooling_log_record_withworkflow',
                                                method: "POST",
                                                data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&mst_mat_id=" + mst_mat_id + "&currentActivity=" + currentActivity + "&U=" + U + "&L=" + L + "&D=" + D + "&Damage=" + Damage + "&from=" + from + "&to=" + to + "&bno=" + bno + "&tabqty=" + tabqty + "&cumqty=" + cumqty + "&cumqtypunch=" + cumqtypunch + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&product_code=" + product_code + "&punch_set=" + punch_set + "&Subset=" + Subset + "&tt_actid=" + tt_actid + "&headerRecordid=" + $scope.headerRecordid+"&supplier="+supplier,

                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                alert(response.data.message);
                                                //console.log(response.data.acttblid);
                                                $scope.doc_id = '';
                                                if(response.data.result){
                                                    $scope.doc_id = response.data.acttblid.doc_no;
                                                }
                                                if ($scope.doc_id != '') {
                                                    var uri = window.location.href;
                                                    window.location.href = updateQueryStringParameter(uri, "doc_id", $scope.doc_id);
                                                } else {
                                                    window.location.href = window.location.href + "&doc_id=" + $scope.doc_id;
                                                }
                                                //window.location.reload();
                                                $('.loader').hide();

                                            }, function (error){
                                                $('.loader').hide();
                                                console.log(error);
                                            });
                                        }




                                        $scope.updatesubmit = function (roomcode, processid, actid, actname, docno, mst_mat_id, currentActivity, U, L, D, Damage, from, to, bno, tabqty, cumqty, cumqtypunch, roleid, empid, empname, email, remark, product_code, punch_set, Subset, update_id, is_in_workflow) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Update_tablet_tooling_log_record_withworkflow',
                                                method: "POST",
                                                data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&mst_mat_id=" + mst_mat_id + "&currentActivity=" + currentActivity + "&U=" + U + "&L=" + L + "&D=" + D + "&Damage=" + Damage + "&from=" + from + "&to=" + to + "&bno=" + bno + "&tabqty=" + tabqty + "&cumqty=" + cumqty + "&cumqtypunch=" + cumqtypunch + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&product_code=" + product_code + "&punch_set=" + punch_set + "&Subset=" + Subset + "&update_id=" + update_id + "&is_in_workflow=" + is_in_workflow,

                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                alert(response.data.message);
                                                location.reload();
                                                $('.loader').hide();
                                            }, function (error) {
                                                $('.loader').hide();
                                                console.log(error);
                                            });
                                        }
                                        //*********************************************************************************************


                                        //Get Last unapproved Room Activity
                                        $scope.ComDataArray = [];
                                        $scope.getLastFilterActivity = function () {
                                            $scope.getProductList();
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetLastFilterActivityForTT?docno=' + $scope.doc_id + "&table_name=pts_trn_tablet_tooling",
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.ComDataArray = response.data.row_data;
                                                if ($scope.ComDataArray.length > 0) {
                                                    $scope.product_code = $scope.ComDataArray[0].product_code;
                                                    $scope.GetTabletToolingpuchsetdata();

                                                    $scope.mst_mat_id = $scope.ComDataArray[0].mst_matrial_id;
                                                    $scope.field_U = $scope.ComDataArray[0].U;
                                                    $scope.field_L = $scope.ComDataArray[0].L;
                                                    $scope.field_D = $scope.ComDataArray[0].D;
                                                    if ($scope.ComDataArray[0].damaged != '') {
                                                        $scope.Damage = $scope.ComDataArray[0].damaged.split(',');
                                                        $scope.totdamage = $scope.Damage.length;
                                                    } else {
                                                        $scope.totdamage = 0;
                                                    }
                                                    $scope.from = $scope.ComDataArray[0].tablet_tooling_from;
                                                    $scope.to = $scope.ComDataArray[0].tablet_tooling_to;
                                                    $scope.bno = $scope.ComDataArray[0].b_no;
                                                    $scope.tabqty = $scope.ComDataArray[0].tab_qty;
                                                    $scope.cumqty = $scope.ComDataArray[0].cum_qty;
                                                    $scope.cumqtypunch = $scope.ComDataArray[0].cum_qty_punch;
                                                }
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        if ($scope.doc_id != '' && $scope.doc_id != 0 && $scope.doc_id != null)
                                        {
                                            $scope.getLastFilterActivity();
                                        }

//*******************Rahul's code Started**********************//
                                        $scope.CancilUpdate = function () {
                                            $scope.edit = false;
                                        }
                                        $scope.edit = false;
                                        $scope.editTrasaction = function () {
                                            if (confirm("Do you really want to edit ?"))
                                            {
                                                $scope.edit = true;
                                                $scope.product_code = $scope.ComDataArray[0].product_code;

                                                $scope.mst_mat_id = $scope.ComDataArray[0].mst_matrial_id;
                                                $scope.field_U = $scope.ComDataArray[0].U;
                                                $scope.field_L = $scope.ComDataArray[0].L;
                                                $scope.field_D = $scope.ComDataArray[0].D;
                                                $scope.Damage = $scope.ComDataArray[0].damaged.split(',');
                                                $scope.from = $scope.ComDataArray[0].tablet_tooling_from;
                                                $scope.to = $scope.ComDataArray[0].tablet_tooling_to;
                                                $scope.bno = $scope.ComDataArray[0].b_no;
                                                $scope.tabqty = $scope.ComDataArray[0].tab_qty;
                                                $scope.cumqty = $scope.ComDataArray[0].cum_qty;
                                                $scope.cumqtypunch = $scope.ComDataArray[0].cum_qty_punch;

                                            }
                                        }
                                        //*******************End Bhupendra's code***************************//
                                        //Reset Login Form
                                        $scope.resetLoginForm = function () {
                                            $scope.username = "";
                                            $scope.password = "";
                                            $scope.remark = "";
                                        }
                                    });
                                    function updateQueryStringParameter(uri, key, value) {
                                        var re = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
                                        if (value === undefined) {
                                            if (uri.match(re)) {
                                                return uri.replace(re, '$1$2');
                                            } else {
                                                return uri;
                                            }
                                        } else {
                                            if (uri.match(re)) {
                                                return uri.replace(re, '$1' + key + "=" + value + '$2');
                                            } else {
                                                var hash = '';
                                                if (uri.indexOf('#') !== -1) {
                                                    hash = uri.replace(/.*#/, '#');
                                                    uri = uri.replace(/#.*/, '');
                                                }
                                                var separator = uri.indexOf('?') !== -1 ? "&" : "?";
                                                return uri + separator + key + "=" + value + hash;
                                            }
                                        }
                                    }
</script>