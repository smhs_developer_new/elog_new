<style type="text/css">
    .table td, .table th {
        padding: 0px !important;
        text-align: center;
    }
</style>
<div ng-app="balanceApp" ng-controller="balanceCtrl" ng-cloak>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>sf_home">Home</a></li>
            <li class="breadcrumb-item"><a href="#">{{room_code_show}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Balance Calibration Record</li>
        </ol>
    </nav>
    <section class="contentBox blueBorder formWrapper">
            <div class="contentHeader">
                <div class="row">
                    <div class="col-12">
                        <h1>Header Details</h1>
                    </div>
                </div>
            </div>
            <div class="contentBody">
                <form>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <ul class="activityList">
                                    <li style="margin-left: 100px;"><a><b>Form No : </b>{{formno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Version No : </b>{{versionno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Effective Date : </b>{{effectivedate | date:'dd-MMM-yyyy'}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Retired Date : </b>{{retireddate | date:'dd-MMM-yyyy'}} </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    <section class="contentBox blueBorder formWrapper">
        <div class="contentHeader">
            <div class="row">
                <div class="col-12">
                    <h1>Balance Calibration Record</h1>
                </div>
            </div>
        </div>

        <div class="contentBody"  ng-show="ComDataArray.length == 0 || edit == true">
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Document No.</label>
                        <input type="text" class="form-control" value="{{docno}}" readonly> 
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label><span class="asterisk">* </span> Balance No.</label>
                        <select ng-disabled="edit == true" class="chosen form-control" tabindex="4" name="balance_no" data-placeholder="Search Balance Number" ng-options="dataObj['id'] as dataObj.balance_no for dataObj in balanceCalibrationData" ng-change="getBalanceInfo()"  ng-model="balance_no" required chosen>
<!--                            <option value="">Select Balance No.</option>-->
                        </select>
                    </div>
                </div>
                <div class="col-sm-2">
                        <div class="form-group">
                            <label for="batchField">Capacity</label>
                             <input type="text" class="form-control" value="{{capacity}}" readonly> 
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="batchField">UOM</label>
                             <input type="text" class="form-control" value="{{UOM}}" readonly> 
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="batchField">Least Count</label>
                             <input type="text" class="form-control" value="{{least_count}}" readonly> 
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">                            
                        <div class="form-row"  ng-show="AcceptanceLimit_Data.length > 0"> 
                            <table class="table ">
                                <thead class="table-dark">
                                    <tr><td colspan="4"><center>Acceptance Limit</center></td></tr>
                                <tr>
                                    <th>Standard wt.</th>
                                    <th>Measurement</th>
                                    <th>Upper Limit</th>
                                    <th>Lower Limit</th>                     
                                </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="dataObj in AcceptanceLimit_Data">
                                        <td>{{dataObj.standard_value}}</td>
                                        <td>{{dataObj.measurement_unit}}</td>               
                                        <td>{{dataObj.max_value}}</td>
                                        <td>{{dataObj.min_value}}</td>                      
                                    </tr>

                                </tbody>
                                <thead class="table-dark">
                                    <tr><td colspan="4"><center>End Of Acceptance Limit</center></td></tr>
                                </thead>
                            </table>

                        </div> 
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Frequency</label> 
                        <select ng-disabled="edit == true" class="form-control" ng-model="frequency_id" name="frequency_id" ng-change="getStandardData()" required>
                            <option value="">Select Frequency</option>
                            <option value="{{dataObj.id}}" ng-repeat="dataObj in frequencyData" ng-hide="dataObj.frequency_name=='Type -A' || dataObj.frequency_name=='Type -B'">{{dataObj.frequency_name}}</option>
                        </select>

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Spirit Level</label>
                            <select ng-model="spirit_level" class="form-control" ng-change="getSpiritAndTimeUpdated()">
                            <option value="" >Select</option>
                            <option value="ok">OK</option>
                            <option value="notok">Not Ok</option>
                            <option value="NA">NA</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Balance Clock's Time</label>
                         <select ng-model="time" class="form-control" ng-change="getSpiritAndTimeUpdated()">
                            <option value="" >Select</option>
                            <option value="ok">OK</option>
                            <option value="notok">Not Ok</option>
                            <option value="NA">NA</option>
                        </select>
                    </div>
                </div>
            </div>



            <form name="balanceForm" novalidate>
                <ul class="customGrid" ng-show="standardData.length > 0">

                    <li class="listHead">
                        <div class="row">
                            <div class="col-md-2">Standard Wt. Used     </div>
                            <div class="col-md-6">ID No.    </div>
                            <div class="col-md-2">Wt. Observed </div>
                            <div class="col-md-2">Difference</div>
                            <!-- <div class="col-md-2">Spirit Level</div>
                            <div class="col-md-2">Time</div> -->
                        </div>
                    </li>

                    <li class="listItem" ng-repeat="dataObj in standardData">
                        <div class="row">

                            <div class="col-sm-4 col-md-2 align-self-center">
                                <div>
                                    <div>
                                        <label class="">Standard Wt. Used</label>
                                    </div>
                                    <div>{{dataObj.standard_value}} {{dataObj.measurement_unit}}</div>
                                </div>
                            </div>

                            <div class="col-sm-4 col-md-6 align-self-center">
                                <div>
                                    <label class="">ID No.</label>
                                    <select ng-disabled="edit == true" class=" form-control" multiple name="id_standard{{$index}}" id="{{$index}}"  data-placeholder="Search Balance No." ng-change="checkStandardWeight(dataObj, $index)" ng-options="key as (dataObj1.weight +      ' =>' + dataObj1.id_no_statndard_weight +      ' =>' +      dataObj1.measurement_unit) for (key,dataObj1) in idStandardWeightList" ng-model="dataObj.id_standard_key" required chosen>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4 col-md-2 align-self-center">
                                <div>
                                    <label class="">Wt. Observed</label>
                                    <input class="form-control" type="text" only-digits placeholder="{{placeholder}}" name="observed_weight" ng-model="dataObj.observed_weight"  required>
                                </div>
                            </div>

                            <div class="col-sm-4 col-md-2 align-self-center">
                                <div>
                                    <label class="">Difference</label>
                                    <div>
                                        <!--{{dataObj.difference = dataObj.observed_weight - dataObj.standard_value | number : valafterDecimal == 0 ? 0 : decimalval}}-->
                                        {{dataObj.difference = getDiff(dataObj.observed_weight,dataObj.standard_value)}} 
                                    <!-- <input type="hidden" name="dataObj.difference" id="dataObj.difference" ng-model="dataObj.difference" value="{{dataObj.difference = dataObj.observed_weight - dataObj.standard_value | number : valafterDecimal == 0 ? 0 : decimalval}} " /> -->
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="col-sm-4 col-md-2 align-self-center">
                                <div>
                                    <div>
                                        <label class="">Spirit Level</label>
                                    </div>
                                   <select ng-model="dataObj.spirit_level" class="form-control">
                                        <option value="">Select</option>
                                        <option value="ok">OK</option>
                                        <option value="notok">Not Ok</option>
                                        <option value="NA">NA</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4 col-md-2 align-self-center">
                                <div>
                                    <div>
                                        <label class="">Time</label> 
                                    </div>
                                    <select ng-model="dataObj.time" class="form-control">
                                        <option value="">Select</option>
                                        <option value="ok">OK</option>
                                        <option value="notok">Not Ok</option>
                                        <option value="NA">NA</option>
                                    </select>

                                </div>
                            </div>
                            -->
                        </div>
                    </li>
                </ul>
                <p class="text-danger" ng-show="standardData.length == 0 && balance_no!='' && frequency_id!=''">There is no Standard Weight assigned to this Frequency.</p>

                <!--Grid-->



                <div class="row">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <button ng-show="edit" class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancel</button>&nbsp;&nbsp;&nbsp;
                            <button ng-show="edit" id="upbtn" ng-click="setbtntext('update');" ng-disabled="swabForm.$invalid" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Update</button>&nbsp;&nbsp;&nbsp;
                            <button ng-show="!edit" id="dnbtn" ng-disabled="balanceForm.$invalid || !frequency_id || totalchk == '1' || spirit_level == '' || time == '' || standardData.length==0" data-toggle="modal" ng-click="setbtntext('submit');" class="button smallBtn primaryBtn">Done By</button> &nbsp;&nbsp;&nbsp;&nbsp;
                            <button ng-show="!edit" ng-disabled="ComDataArray.length == 0"  data-toggle="modal" ng-click="setbtntext('approval');" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button>

                        </div>
                    </div>
                </div>

            </form>
        </div>
        <div class="contentBody" ng-if="ComDataArray.length > 0 && edit == false">

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Document No.</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].doc_no}}" readonly> 
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label><span class="asterisk">* </span> Balance No.</label>
                        <input type="text" class="form-control" value="{{balance_info.balance_no}}" readonly> 
                    </div>
                </div>
                <div class="col-sm-2">
                        <div class="form-group">
                            <label for="batchField">Capacity</label>
                             <input type="text" class="form-control" value="{{capacity}}" readonly> 
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="batchField">UOM</label>
                             <input type="text" class="form-control" value="{{UOM}}" readonly> 
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="batchField">Least Count</label>
                             <input type="text" class="form-control" value="{{balance_info.least_count}}" readonly>
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">                            
                        <div class="form-row"  ng-show="AcceptanceLimit_Data.length > 0"> 
                            <table class="table ">
                                <thead class="table-dark">
                                    <tr><td colspan="4"><center>Acceptance Limit</center></td></tr>
                                <tr>
                                    <th>Standard wt.</th>
                                    <th>Measurement</th>
                                    <th>Upper Limit</th>
                                    <th>Lower Limit</th>                     
                                </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="dataObj in AcceptanceLimit_Data">
                                        <td>{{dataObj.standard_value}}</td>
                                        <td>{{dataObj.measurement_unit}}</td>               
                                        <td>{{dataObj.max_value}}</td>
                                        <td>{{dataObj.min_value}}</td>                      
                                    </tr>

                                </tbody>
                                <thead class="table-dark">
                                    <tr><td colspan="4"><center>End Of Acceptance Limit</center></td></tr>
                                </thead>
                            </table>

                        </div> 
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Frequency</label> 
                        <input type="text" class="form-control" value="{{getFrequencyName(ComDataArray[0].frequency_id)}}" readonly> 

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Spirit Level</label> 
                        <input type="text" class="form-control" value="{{ComDataArray[0].sprit_level_status}}" readonly> 

                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Time</label> 
                        <input type="text" class="form-control" value="{{ComDataArray[0].time_status}}" readonly> 

                    </div>
                </div>
            </div>
            <ul class="customGrid">
                <li class="listHead">
                    <div class="row">
                        <div class="col-md-2">Standard Wt. Used</div>
                        <div class="col-md-6">ID No.</div>
                        <div class="col-md-2">Wt. Observed </div>
                        <div class="col-md-2">Difference</div>
                        <!-- <div class="col-md-2">Spirit Level</div>
                        <div class="col-md-2">Time</div> -->
                    </div>
                </li>

                <li class="listItem" ng-repeat="dataObj in ComDataArray">
                    <div class="row">
                        <div class="col-md-2">{{dataObj.standerd_wt}} {{dataObj.measurement_unit}}</div>
                        <div class="col-md-6">{{dataObj.id_no_of_st_wt}}</div>
                        <div class="col-md-2">{{dataObj.oberved_wt | number : decimalval}}</div>
                        <div class="col-md-2">{{dataObj.diff | number : decimalval }}</div>
                        <!-- <div class="col-md-2">{{dataObj.sprit_level_status}}</div>
                        <div class="col-md-2">{{dataObj.time_status}}</div> -->
                    </div>
                </li>
            </ul>


            <!--Grid-->



            <div class="row">
                <div class="col-sm-12 text-right">
                    <div class="formBtnWrap">
                        <button ng-click="editTrasaction()" class="button smallBtn primaryBtn" >Edit</button>&nbsp;&nbsp;&nbsp;
                        <!-- <button disabled data-toggle="modal" ng-click="setbtntext('submit');" class="button smallBtn primaryBtn">Done By</button> &nbsp;&nbsp;&nbsp;&nbsp; -->
                        <button data-toggle="modal" ng-click="setbtntext('approval');" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button>

                    </div>
                </div>
            </div>


        </div>
    </section>
    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script>
                            var app = angular.module("balanceApp", ['angular.chosen', 'blockUI']);
                            app.directive('chosen', function ($timeout) {
                                var linker = function (scope, element, attr) {

                                    scope.$watch('idStandardWeightList', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    scope.$watch('balanceCalibrationData', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    $timeout(function () {
                                        element.chosen();
                                    }, 0, false);
                                };
                                return {
                                    restrict: 'A',
                                    link: linker
                                };
                            });
                            app.directive('onlyDigits', function () {
                                return {
                                    require: '?ngModel',
                                    link: function (scope, element, attrs, ngModelCtrl) {
                                        if (!ngModelCtrl) {
                                            return;
                                        }

                                        ngModelCtrl.$parsers.push(function (val) {
                                            if (angular.isUndefined(val)) {
                                                var val = '';
                                            }

                                            var clean = val.replace(/[^0-9\.]/g, '');
                                            var negativeCheck = clean.split('-');
                                            var decimalCheck = clean.split('.');
                                            if (!angular.isUndefined(negativeCheck[1])) {
                                                negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                                                clean = negativeCheck[0] + '-' + negativeCheck[1];
                                                if (negativeCheck[0].length > 0) {
                                                    clean = negativeCheck[0];
                                                }

                                            }

                                            if (!angular.isUndefined(decimalCheck[1])) {
                                                decimalCheck[1] = decimalCheck[1].slice(0, $valafterDecimal);
                                                clean = decimalCheck[0] + '.' + decimalCheck[1];
                                            }                                            

                                            if (val !== clean) {
                                                ngModelCtrl.$setViewValue(clean);
                                                ngModelCtrl.$render();
                                            }
                                            return clean;
                                        });

                                        element.bind('keypress', function (event) {
                                            if (event.keyCode === 32) {
                                                event.preventDefault();
                                            }
                                        });
                                    }
                                };
                            });
                            // app.directive('onlyDigits', function () {
                            //     return {
                            //         require: 'ngModel',
                            //         restrict: 'A',
                            //         link: function (scope, element, attr, ctrl) {
                            //             function inputValue(val) {
                            //                 if (val) {
                            //                     var digits = val.replace(/[^0-9.]/g, '');

                            //                     if (digits.split('.').length > 2) {
                            //                         digits = digits.substring(0, digits.length - 1);
                            //                     }

                            //                     if (digits !== val) {
                            //                         ctrl.$setViewValue(digits);
                            //                         ctrl.$render();
                            //                     }
                            //                     return parseFloat(digits);
                            //                 }
                            //                 return undefined;
                            //             }
                            //             ctrl.$parsers.push(inputValue);
                            //         }
                            //     };
                            // });
                            app.controller("balanceCtrl", function ($scope, $http, $filter, blockUI) {
                                var urlParams = new URLSearchParams(window.location.search);
                                var myParam = urlParams.get('room_code');
                                var processid = urlParams.get('processid');
                                var actid = urlParams.get('actid');
                                var actname = urlParams.get('actname');

                                // get header detail 
                                $scope.headerRecordid = urlParams.get('headerid');
                                $scope.formno = urlParams.get('formno');
                                $scope.versionno = urlParams.get('versionno');
                                $scope.effectivedate = urlParams.get('effectivedate');
                                $scope.retireddate = urlParams.get('retireddate');
                                //end

                                $scope.room_code_show = myParam;
                                $scope.balanceCalibrationData = [];
                                $scope.spirit_level = "";
                                $scope.time = "";
                                $scope.balance_no='';
                                $scope.frequency_id='';
                                $valafterDecimal=0;
                                $scope.decimalvalcheck = false;
                                $scope.decimalval=0;
                                $scope.placeholder='';
                                $scope.roleList = [];
                                $scope.balance_info='';
                                
                                        $scope.getRoleList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.roleList = response.data.role_list;
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.getRoleList();
                                $scope.getBalanceCalibrationList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetBalanceCalibrationList?room='+myParam,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.balanceCalibrationData = response.data.balance_calibration_list;
                                        console.log($scope.balanceCalibrationData);
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.getBalanceCalibrationList();
                                $scope.getIdNoStandardWeightList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetIdNoStandardWeightList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.idStandardWeightList = response.data.id_standard_list;
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.getIdNoStandardWeightList();
                                //Get Document Number code
                                $scope.docno = "";
                                // $scope.getdocno = function () {
                                //     $http({
                                //         url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getdocno',
                                //         method: "GET",
                                //         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                //     }).then(function (response) {
                                //         $scope.docno = response.data.docno;
                                //     }, function (error) { // optional

                                //         console.log("Something went wrong.Please try again");
                                //     });
                                // }
                                // $scope.getdocno();
                                //End of Get Document number function code
                                
                                $scope.balno = "";
                                $scope.getBalanceInfo = function () {                                    
                                    $scope.capacity2 = $filter('filter')($scope.balanceCalibrationData, {id: $scope.balance_no})[0].capacity;
									$scope.capacity = $scope.capacity2.substring(0, $scope.capacity2.length -2);
									$scope.UOM = $scope.capacity2.substring($scope.capacity2.length -2);
                                    $scope.balno = $filter('filter')($scope.balanceCalibrationData, {id: $scope.balance_no})[0].balance_no;
                                    $scope.least_count = $filter('filter')($scope.balanceCalibrationData, {id: $scope.balance_no})[0].least_count;                                    
                                    $valafterDecimal = $scope.countvalafterdecimal($scope.least_count);
                                    $scope.acceptance_limit = $filter('filter')($scope.balanceCalibrationData, {id: $scope.balance_no})[0].acceptance_limit;
                                    $scope.standardData = [];
                                    $scope.frequency_id = "";
                                    $scope.getAcceptanceLimitData($scope.balance_no);
                                }

                                $scope.frequencyData = [];
                                $scope.getFrequencyList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetFrequencyList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.frequencyData = response.data.frequency_list;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.getFrequencyList();
                                $scope.standardData = [];
                                $scope.chkarray = [];
                                $scope.getStandardData = function () {
                                    $scope.chkarray = [];
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetStandardData?balance_no=' + $scope.balno + '&frequency_id=' + $scope.frequency_id,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.standardData = response.data.standard_list;
                                        //console.log($scope.standardData);
                                        angular.forEach($scope.standardData, function (value, key) {
                                            var m =1;
                                            if(value.measurement_unit=='KG'){
                                                m = 1000;
                                            }
                                            $scope.chkarray.push({strdwt: value.standard_value*m, idtotalwt: 0});
                                            $scope.standardData[key]['doc_no'] = $scope.docno;
                                            $scope.standardData[key]['room_code'] = myParam;
                                            $scope.standardData[key]['balid'] = $scope.balance_no;
                                            $scope.standardData[key]['spirit_level'] = "";
                                            $scope.standardData[key]['id_standard'] = [];
                                            $scope.standardData[key]['time'] = "";
                                        });
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                    });
                                }

                                $scope.getSpiritAndTimeUpdated = function(){
                                    angular.forEach($scope.standardData, function (value, key) {
                                        $scope.standardData[key]['spirit_level'] = $scope.spirit_level;
                                        $scope.standardData[key]['time'] = $scope.time;
                                    });
                                }

                                //*******************Bhupendra's code Started**********************//

                                $scope.AcceptanceLimit_Data = [];
                                $scope.getAcceptanceLimitData = function (Balanceno) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetAcceptanceLimitData?balance_no=' + Balanceno,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.AcceptanceLimit_Data = response.data.AcceptanceLimit_list;
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.vari= 0;
                                $scope.keepon = true;
                                $scope.setbtntext = function (btntext)
                                {
                                    if($scope.vari== 0 && $scope.keepon==true){
                                        angular.forEach($scope.standardData, function (value, key) {
                                            var temp = [];
                                            angular.forEach(value.id_standard, function (value1, key1) {
                                                temp.push(value1.id);
                                            });
                                             $scope.vari= 1;
                                            $scope.standardData[key]['id_standard'] = [];
                                            $scope.standardData[key]['id_standard'] = temp;
                                        });
                                    }
                                    $scope.keepon = true;
                                    $scope.btntext = btntext;
                                    angular.forEach($scope.chkarray, function (value, key) {
                                        if ($scope.keepon == true)
                                        {
                                            if (value.strdwt == value.idtotalwt)
                                            {
                                                $scope.flag = 1;
                                                $scope.keepon = true;
                                            } else
                                            {
                                                $scope.flag = 0;
                                                $scope.keepon = false;
                                            }
                                        }
                                    });
                                    if ($scope.flag == 1)
                                    {
                                        if ($scope.edit == false)
                                        {
                                            $("#loginModal").modal();
                                        } else {
                                            alert("Please select less than or equal to standard Weight value");
                                        }
                                    }
                                }

                                $scope.getDiff = function(observedWeight,StandardValue){
                                    var digit = $scope.countvalafterdecimal($scope.least_count);
                                    var diff = (observedWeight-StandardValue).toFixed(digit);
                                    return diff;
                                }
                                $scope.edit = false;

                                $scope.getval = function ()
                                {
                                    alert($scope.spirit_level);
                                }
                                //Login Function
                                //Created by Bhupendra
                                //Date : 28/01/2020                                    
                                $scope.login = function () {
                                    angular.forEach($scope.standardData, function (value, key) {
                                        //delete $scope.standardData[key]['id_standard_key'];
                                    });
                                    var response = confirm("Do you really want to perform this task?");
                                    if (response == true)
                                    {
                                        $('.loader').show();
                                        pwd2 = SHA256($scope.password);
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                            method: "POST",
                                            data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&module_id=" + actid + "&module_type=log" + "&action=" + $scope.btntext+ "&auth=" + btoa(btoa($scope.password))+"&act_id="+actid,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            if (response.data.message == "")
                                            {
                                                if ($scope.btntext == 'approval')
                                                {
                                                    //Calling Approval function
                                                    //Created by Bhupendra
                                                    //Date : 10/02/2020
                                                    $scope.getapproved(myParam, $scope.ComDataArray[0].id, $scope.ComDataArray[0].activity_id, $scope.ComDataArray[0].workflowstatus, $scope.ComDataArray[0].workflownextstep, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, 'stop', $scope.ComDataArray[0].doc_id)
                                                } else if ($scope.btntext == 'submit')
                                                {
                                                    //Calling Stoped function
                                                    //Created by Bhupendra
                                                    //Date : 10/02/2020
                                                    $scope.getsubmit(myParam, processid, actid, actname, $scope.docno, $scope.standardData, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark)
                                                } else if ($scope.btntext == 'edit')
                                                {
//                                                    $scope.standardData = [];
//                                                    console.log($scope.ComDataArray[0]);
//                                                    //edit Functionality
//                                                    //Created by Rahul
//                                                    //Date : 04/05/2020
//                                                    var userdata = response.data.userdata;
//                                                    if (userdata.role_id > $scope.ComDataArray[0].done_by_role_id) {
//                                                        $scope.standardData = [];
//                                                        $scope.edit = true;
//                                                        $scope.docno = $scope.ComDataArray[0].doc_no;
//                                                        $scope.is_in_workflow = $scope.ComDataArray[0].is_in_workflow;
//                                                        $scope.balance_no = $scope.ComDataArray[0].balance_id;
//                                                        $scope.balno = $scope.ComDataArray[0].balance_no;
//                                                        $scope.frequency_id = $scope.ComDataArray[0].frequency_id;
//                                                        $scope.spirit_level = $scope.ComDataArray[0].sprit_level_status;
//                                                        $scope.time = $scope.ComDataArray[0].time_status;
//                                                        //$scope.getStandardData();
//                                                        $scope.getBalanceData($scope.ComDataArray[0].balance_id);
//                                                        $scope.getAcceptanceLimitData($scope.ComDataArray[0].balance_id);
//                                                        console.log($scope.balance_info);
//                                                        $scope.capacity = $scope.balance_info.capacity;
//                                                        $scope.least_count = $scope.balance_info.least_count;
//
//                                                        angular.forEach($scope.ComDataArray, function (value, key) {
//                                                            var st_wt_index_array = value.id_no_st_wt_index.split(',');
//                                                            //var st_wt_array = value.id_no_of_st_wt.split(',');
//                                                            $scope.standardData.push({
//                                                                standard_value: value.standerd_wt,
//                                                                room_code: myParam,
//                                                                balance_no: $scope.balno,
//                                                                balid: $scope.balance_no,
//                                                                //id_standard:st_wt_array,
//
//                                                                id_standard_key: st_wt_index_array,
//                                                                observed_weight: value.oberved_wt,
//                                                                spirit_level: $scope.spirit_level,
//                                                                time: $scope.time,
//                                                                editid: value.edit_id,
//                                                            });
//                                                            $scope.standardData[key]['id_standard'] = [];
//                                                        });
//                                                        console.log($scope.standardData);
//                                                        //$scope.getProductDetail($scope.product_no);
//                                                    } else {
//                                                        alert("You are not allowed to edit this.")
//                                                        $scope.edit = false;
//                                                    }
//                                                    $scope.resetLoginForm();
//                                                    angular.element("#btnclose").trigger('click');
                                                } else if ($scope.btntext == 'update') {
                                                    //update Functionality
                                                    //Created by Rahul
                                                    //Date : 04/05/2020
                                                    var role = "";
                                                    if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                        if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }

                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep == 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {

                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflowstatus})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;

                                                    } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time != null) {
                                                        if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }
                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    }
//                                                  
                                                    var userdata = response.data.userdata;
                                                    if (userdata.role_id >= $scope.ComDataArray[0].last_approval.role_id && userdata.role_id > 1) {
                                                        if($scope.chkonsubmit())
                                                        {
                                                            $scope.updatesubmit(myParam, processid, actid, actname, $scope.docno, $scope.standardData, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.is_in_workflow);
                                                        }
                                                        else{
                                                            $('.loader').hide();
                                                            alert("Please set observed wt decimal digits to "+$scope.decimalval+" Places");
                                                        }                                                        
                                                    } else {
                                                        $('.loader').hide();
                                                        alert("You are not allowed to edit this.")
                                                        $scope.edit = false;
                                                    }
                                                }
                                            } else {
                                                $('.loader').hide();
                                                alert(response.data.message);
                                            }
                                        }, function (error) {
                                            $('.loader').hide();
                                            //console.log(error);
                                        });
                                    }
                                }
                                //End Login Function

                                //Approval function :- To Approve running activity by auth & privilege chk as well as sent that activity to next approval which exists in workflow
                                //Created by Bhupendra
                                //Date : 28/01/2020
                                $scope.getapproved = function (roomcode, actlogtblid, actid, workflowstatus, workflownextstep, roleid, empid, empname, email, remark, actstatus, docno) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/com_approval_fun',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&workflowstatus=" + workflowstatus + "&workflownextstep=" + workflownextstep + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&actstatus=" + actstatus + "&docno=" + docno + "&tblname=pts_trn_balance_calibration",

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        if (response.data.next_step == "-1")
                                        {
                                            window.location.href = "<?php echo base_url() ?>home";
                                        } else
                                        {
                                        $scope.resetLoginForm();
                                        angular.element("#btnclose").trigger('click');
                                        $scope.standardData = [];
                                        $scope.frequency_id = "";
                                        $scope.balance_no = "";
                                        $scope.capacity = "";
                                        $scope.least_count = "";
                                        $scope.acceptance_limit = "";
                                        $scope.AcceptanceLimit_Data = [];
                                        $scope.spirit_level = "";
                                        $scope.time = "";
                                        $scope.getLastFilterActivity();
                                        $('.loader').hide();
                                        //$scope.getdocno();
                                        }
                                    }, function (error) {
                                        //console.log(error);
                                        $('.loader').hide();
                                    });
                                }
                                //*********************************************************************************
                                //Start function :- To Start new activity by auth chk.
                                //Created by Bhupendra
                                //Date : 4/02/2020                    
                                $scope.getsubmit = function (roomcode, processid, actid, actname, docno, standardData, roleid, empid, empname, email, remark) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/submit_bal_calibration_record',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&standardData=" + encodeURIComponent(angular.toJson(standardData)) + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&headerRecordid=" + $scope.headerRecordid,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        angular.element("#btnclose").trigger('click');
                                        location.reload();
//                            $scope.standardData = [];
//                            $scope.frequency_id = "";
//                            $scope.balance_no = "";
//                            $scope.capacity = "";
//                            $scope.least_count = "";
//                            $scope.acceptance_limit ="";
//                            $scope.getLastFilterActivity();
                                        //$scope.getdocno();

                                    }, function (error) {
                                        $('.loader').hide();
                                        //console.log(error);
                                    });
                                }
                                //*********************************************************************************************

                                //*********************************************************************************
                                //Start function :- To Update started Activity.
                                //Created by Raul Chauhan
                                //Date : 05/05/2020



                                $scope.updatesubmit = function (roomcode, processid, actid, actname, docno, standardData, roleid, empid, empname, email, remark, is_in_workflow) {

                                    angular.forEach($scope.standardData, function (value, key) {
                                        $scope.standardData[key]['spirit_level'] = $scope.spirit_level;
                                        $scope.standardData[key]['time'] = $scope.time;
                                    });

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/update_bal_calibration_record',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&standardData=" + encodeURIComponent(angular.toJson(standardData)) + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&is_in_workflow=" + is_in_workflow,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.edit = false;
                                        angular.element("#btnclose").trigger('click');
                                        $scope.resetLoginForm();
                                        $scope.getLastFilterActivity();
                                        $('.loader').hide();
                                    }, function (error) {
                                        //console.log(error);
                                        $('.loader').hide();
                                    });
                                }

                                $scope.CancilUpdate = function () {
                                    $scope.edit = false;
                                }


                                //Get Last unapproved Room Activity
                                $scope.ComDataArray = [];
                                $scope.getLastFilterActivity = function () {
                                    $scope.ComDataArray = [];
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetLastFilterActivity?room_code=' + myParam + "&table_name=pts_trn_balance_calibration",
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.ComDataArray = response.data.row_data;
                                        //console.log($scope.ComDataArray);
                                        if ($scope.ComDataArray.length > 0) {
                                            
                                            //$scope.$watch('balance_info', function () {
                                                $scope.getBalanceData($scope.ComDataArray[0].balance_id);
                                                $scope.getAcceptanceLimitData($scope.ComDataArray[0].balance_id);
                                                $scope.spirit_level = $scope.ComDataArray[0].sprit_level_status;
                                                $scope.time = $scope.ComDataArray[0].time_status;
                                            //});                                            
                                        }
                                    }, function (error) { // optional

                                        //console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getLastFilterActivity();

                                //*******************Rahul's code Started**********************//
                                $scope.editTrasaction = function () {
                                    if (confirm("Do you really want to edit ?"))
                                    {
                                        $scope.standardData = [];
                                        $scope.edit = true;
                                        $scope.docno = $scope.ComDataArray[0].doc_no;
                                        $scope.is_in_workflow = $scope.ComDataArray[0].is_in_workflow;
                                        $scope.balance_no = $scope.ComDataArray[0].balance_id;
                                        $scope.balno = $scope.ComDataArray[0].balance_no;
                                        $scope.frequency_id = $scope.ComDataArray[0].frequency_id;
                                        $scope.spirit_level = $scope.ComDataArray[0].sprit_level_status;
                                        $scope.time = $scope.ComDataArray[0].time_status;
                                        //$scope.getStandardData();
                                        $scope.getBalanceData($scope.ComDataArray[0].balance_id);
                                        $scope.getAcceptanceLimitData($scope.ComDataArray[0].balance_id);
                                        $scope.capacity2 = $scope.balance_info.capacity;
                                        $scope.capacity = $scope.capacity2.substring(0, $scope.capacity2.length -2);
                                        $scope.UOM = $scope.capacity2.substring($scope.capacity2.length -2);
                                        $scope.least_count = $scope.balance_info.least_count; 
                                        $valafterDecimal = $scope.countvalafterdecimal($scope.least_count);                                      
                                        angular.forEach($scope.ComDataArray, function (value, key) {
                                            var st_wt_index_array = value.id_no_st_wt_index.split(',');
                                            //var st_wt_array = value.id_no_of_st_wt.split(',');
                                            $scope.standardData.push({
                                                standard_value: value.standerd_wt,
                                                room_code: myParam,
                                                balance_no: $scope.balno,
                                                balid: $scope.balance_no,
                                                //id_standard:st_wt_array,

                                                id_standard_key: st_wt_index_array,
                                                observed_weight: value.oberved_wt,
                                                spirit_level: $scope.spirit_level,
                                                time: $scope.time,
                                                editid: value.edit_id,
                                            });
                                            $scope.standardData[key]['id_standard'] = [];
                                        });
                                    }
                                }

                                //*******************End Rahul's code***************************//
                                
                                $scope.getBalanceinfoData = function ($balanceId) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getbalance_info?id=' + $balanceId,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.balance_info = response.data.balance_info[0];
                                        console.log($scope.balance_info);
                                        $scope.capacity = $scope.balance_info.capacity.substring(0, $scope.balance_info.capacity.length -2);
                                        $scope.UOM = $scope.balance_info.capacity.substring($scope.balance_info.capacity.length -2);
                                        $valafterDecimal = $scope.countvalafterdecimal($scope.balance_info.least_count);
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }

                                $scope.getBalanceData = function (balanceId) {
                                    $scope.getBalanceinfoData(balanceId);
                                    //$scope.balance_info = $filter('filter')($scope.balanceCalibrationData, {id: balanceId})[0];
                                    // $scope.capacity = $scope.balance_info.capacity.substring(0, $scope.balance_info.capacity.length -2);
                                    // $scope.UOM = $scope.balance_info.capacity.substring($scope.balance_info.capacity.length -2);
                                    // $valafterDecimal = $scope.countvalafterdecimal($scope.balance_info.least_count);

                                }


                                $scope.getFrequencyName = function (objId) {
                                    var frequency_name = $filter('filter')($scope.frequencyData, {id: objId})[0].frequency_name;
                                    return frequency_name;
                                }

                                //Reset Login Form
                                $scope.resetLoginForm = function () {
                                    $scope.username = "";
                                    $scope.password = "";
                                    $scope.remark = "";
                                    $scope.vari = 0;
                                }

                                //Check Standard Weight
                                $scope.totalchk = 0;
                                $scope.total = "";
                                $scope.checkStandardWeight = function (dataObj, index) {
                                    //debugger;
                                    var total = 0;
                                    $scope.vari = 0;
                                    $scope.totalchk = 0;
                                    $scope.standardData[index]['id_standard'] = [];
                                    $scope.standardData[index]['spirit_level'] = $scope.spirit_level;
                                    $scope.standardData[index]['time'] = $scope.time;
                                    angular.forEach($scope.standardData[index]['id_standard_key'], function (value, key) {
                                        $scope.standardData[index]['id_standard'].push({id: $scope.idStandardWeightList[value].id_no_statndard_weight, weight: $scope.idStandardWeightList[value].weight,measurement_unit:$scope.idStandardWeightList[value].measurement_unit});

                                    });
                                    angular.forEach($scope.standardData[index]['id_standard'], function (value, key) {
                                        var multiplier = 1;
                                        if(value.measurement_unit=='KG'){
                                            multiplier = 1000;
                                        }
                                        total = total + parseFloat(value.weight*multiplier);
                                        $scope.total = total;
                                    });
                                    var multiplier1 = 1;
                                    if($scope.standardData[index]['measurement_unit']=='KG'){
                                        multiplier1 = 1000;
                                    }
                                    if (total <= parseFloat($scope.standardData[index]['standard_value']*multiplier1)) {
                                    } else {
                                        alert("Please select less than or equal to standard Weight value");
                                        $scope.standardData[index]['id_standard'] = [];
                                        $scope.standardData[index]['id_standard_key'] = [];
                                    }
                                    if (total < parseFloat($scope.standardData[index]['standard_value']*multiplier1)) {
                                        $scope.totalchk = 1;
                                    } else if (total > parseFloat($scope.standardData[index]['standard_value']*multiplier1)) {
                                        $scope.totalchk = 1;
                                    } else {
                                        $scope.totalchk = 0;
                                    }
                                    $scope.chkarray[index]['idtotalwt'] = $scope.total;
                                }

                                //Auther : Bhupendra kumar
                                //Date : 11Dec2020
                                //Desc : This function is used to get the length of digits after decimal of least count
                                $scope.countvalafterdecimal = function (str) {
                                    var obj = str.split('.');
                                    var n = obj[1].length;
                                    var str='0.';
                                    $scope.decimalval = obj[1].length;
                                    for(i=0 ; i<obj[1].length ; i++)
                                    {
                                        str = str + '0';
                                    }
                                    $scope.placeholder = str;
                                    return parseInt(n);
                                }
                                //Auther : Bhupendra kumar
                                //Date : 15Dec2020
                                //Desc : This function is used to get the length of digits after decimal of observed wt
                                $scope.chklength = function (str){
                                    if(str != 'undefined' && str != '' && str != null) {
                                        var obj = str.split('.');                                        
                                        if(obj.length < 2)
                                        {
                                            $('#upbtn').hide();
                                            $('#dnbtn').hide();
                                        } else {   
                                            var n = obj[1].length;                                 
                                            if(n < $valafterDecimal){
                                                $('#upbtn').hide();
                                                $('#dnbtn').hide();                                           
                                            } else {
                                                $('#upbtn').show();
                                                $('#dnbtn').show();
                                            }
                                        }
                                    }else{
                                        $('#upbtn').hide();
                                        $('#dnbtn').hide();
                                    }
                                }

                                $scope.chkonsubmit = function ()
                                {
                                    var keepGoing = true;
                                    var isbug=0;
                                    angular.forEach($scope.standardData, function (value, key) {
                                        if(keepGoing) {
                                            if(value['observed_weight'] != 'undefined' && value['observed_weight'] != '' && value['observed_weight'] != null) {
                                                var obj = value['observed_weight'].split('.');                                        
                                                if(obj.length < 2)
                                                {
                                                    isbug=1;
                                                    keepGoing = false;
                                                } else {   
                                                    var n = obj[1].length;                                 
                                                    if(n != $valafterDecimal){
                                                        isbug=1;  
                                                        keepGoing = false;                                       
                                                    } else {
                                                        isbug=0;
                                                        keepGoing = true;
                                                    }
                                                }
                                            }else{
                                                isbug=1;
                                                keepGoing = false;
                                            } 
                                        }                                                                                 
                                    });
                                    if(isbug==0){return true;}else{return false;}
                                }
                            });
</script>