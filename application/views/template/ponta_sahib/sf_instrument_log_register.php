<div ng-app="instrumentApp" ng-controller="instrumentCtrl" ng-cloak>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>sf_home">Home</a></li>
            <li class="breadcrumb-item"><a href="#">{{room_code_show}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Instrument Log Register</li>
        </ol>
    </nav>
    <section class="contentBox blueBorder formWrapper">
            <div class="contentHeader">
                <div class="row">
                    <div class="col-12">
                        <h1>Header Details</h1>
                    </div>
                </div>
            </div>
            <div class="contentBody">
                <form>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <ul class="activityList">
                                    <li style="margin-left: 100px;"><a><b>Form No : </b>{{formno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Version No : </b>{{versionno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Effective Date : </b>{{effectivedate | date:'dd-MMM-yyyy'}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Retired Date : </b>{{retireddate | date:'dd-MMM-yyyy'}} </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    <section class="contentBox blueBorder formWrapper">


        <div class="contentHeader">

            <div class="row">
                <div class="col-12">
                    <h1>Instrument Log Register</h1>
                </div>
            </div>

        </div>

        <div class="contentBody">
            <div ng-show="ComDataArray.length == 0 || edit == true">
                <form name="instrumentForm" novalidate>
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Document No.</label>
                                <input type="text" class="form-control" value="{{docno}}" readonly>    
                            </div>
                        </div>
                        <div class="col-sm-6" ng-show="edit == true">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Instrument No.</label>
                                <input type="text" class="form-control" value="{{ComDataArray[0].instrument_code}}" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6" ng-show="instrumentLogData.length > 0 && edit == false">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Instrument No.</label>
                                <select class="chosen form-control" tabindex="4"  name="instrument_code"  data-placeholder="Search Instrument No" ng-options="dataObj['equipment_code'] as (dataObj.equipment_name +      ' => ' +      dataObj.equipment_code) for dataObj in instrumentLogData"  ng-model="instrument_code" required chosen>
                                    <option value="" selected disabled>Select Instrument No.</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6" ng-show="instrumentLogData.length == 0 && edit == false;">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Instrument No.</label>
                                <select class="form-control">
                                    <option value="">All Equipments are in used or not defined in master</option>
                                </select>
                            </div>
                        </div>


                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Stage </label>
                                <select class="form-control" name="Stage" ng-model="stage" required>
                                    <option value="" selected disabled>Please Select</option>
                                    <option value="{{dataObj.field_value}}" ng-repeat="dataObj in stageData">{{dataObj.field_value}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Test</label>
                                <textarea class="form-control" name="Test" ng-model="test" maxlength="100" required></textarea>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Batch No.</label>
                                <p ng-show="batchInProductData.length > 0"><b>{{batch_no}}</b></p>
                                <input ng-show="batchInProductData.length == 0" maxlength="10" class="form-control" type="text" placeholder="Batch No." name="batch_no" ng-model="batch_no" >
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Product Name/Material Name</label>
                                <div class="col-sm-12" ng-show="batchInProductData.length == 0">
                                    <select class="chosen form-control" tabindex="4"  name="product_code" ng-change="getProductDetail(product_no)" data-placeholder="Search Product No." ng-options="dataObj['product_code'] as dataObj.product_code for dataObj in productData"  ng-model="product_no" required chosen>
                                        <option value="" selected disabled>Select Product No</option>
                                    </select>
                                </div>
                                <div class="col-sm-6" ng-show="batchInProductData.length > 0">
                                    <p><b>{{product_no}}</b></p>
                                </div>
                                <div class="col-sm-6" ng-if="product_desc != ''">
                                    <p><b>{{product_desc}}</b></p>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <div class="formBtnWrap">
                                <button ng-show="!edit" ng-click="setbtntext('submit');" ng-disabled="instrumentForm.$invalid" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Activity Start</button>&nbsp;&nbsp;&nbsp;
                                <button ng-show="edit" class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancel</button>&nbsp;&nbsp;&nbsp;
                                <button ng-show="edit" ng-click="setbtntext('update');" ng-disabled="instrumentForm.$invalid" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Activity Update</button>&nbsp;&nbsp;&nbsp;
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div  ng-if="ComDataArray.length > 0 && edit == false">
                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Document No.</label>
                            <input type="text" class="form-control" value="{{ComDataArray[0].doc_no}}" readonly>    
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Instrument No.</label>
                            <input type="text" class="form-control" value="{{ComDataArray[0].instrument_code}}" readonly> 
                        </div>
                    </div>


                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Stage </label>
                            <input type="text" class="form-control" value="{{ComDataArray[0].stage}}" readonly>   
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Test</label>
                            <input type="text" class="form-control" value="{{ComDataArray[0].test}}" readonly>   
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Batch No.</label>
                            <input type="text" class="form-control" value="{{ComDataArray[0].batch_no}}" readonly>   
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Product Name/Material Name</label>
                            <input type="text" class="form-control" value="{{ComDataArray[0].product_code}}" readonly>   
                        </div>
                    </div>
                </div>



            </div>

            <form name="checkinstrumentForm" ng-if="ComDataArray.length > 0 && edit == false" novalidate>
                <div>
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Result Left</label>
                                <div class="switch-field col2">
                                    <select class="form-control" ng-change="setresultleft(result_left)" name="result_left" ng-model="result_left" required>
                                        <option value='' selected>Please Select</option>
                                        <option value="{{dataObj.field_value}}" ng-repeat="dataObj in ResultLeftData">{{dataObj.field_value}}</option>
                                    </select>
                                    <!-- <input type="radio" id="result-left-yes" ng-model="result_left"  name="result_left" value="ok" >
                                    <label for="result-left-yes">OK</label>
                                    <input type="radio" id="result-left-no" ng-model="result_left"  name="result_left" value="notok">
                                    <label for="result-left-no">Not Ok</label>
                                    <input type="radio" id="result-left-na" ng-model="result_left"  name="result_left" value="NA">
                                    <label for="result-left-na">NA</label> -->
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-3">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Result Right</label>
                                <div class="switch-field col2">
                                    <select class="form-control" ng-change="setresultright(result_right)" name="result_right" ng-model="result_right" required>
                                        <option value='' selected >Please Select</option>
                                        <option value="{{dataObj.field_value}}" ng-repeat="dataObj in ResultRightData">{{dataObj.field_value}}</option>
                                    </select>
                                    <!-- <input type="radio" id="result-right-yes" ng-model="result_right"  name="result_right" value="yes">
                                    <label for="result-right-yes">OK</label>
                                    <input type="radio" id="result-right-no" ng-model="result_right"  name="result_right" value="no">
                                    <label for="result-right-no">Not Ok</label>
                                    <input type="radio" id="result-right-na" ng-model="result_right"  name="result_right" value="NA">
                                    <label for="result-right-na">NA</label> -->
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Remark</label>
                                <textarea class="form-control" name="remark"  ng-model="remark" maxlength="100"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <div class="formBtnWrap">
                                <button ng-click="editTrasaction()" class="button smallBtn primaryBtn" >Edit</button>&nbsp;&nbsp;&nbsp;
                                <button ng-click="setbtntext('approval');" ng-disabled="ComDataArray.length == 0 || result_right == '' || result_left == '' || result_right == undefined || result_left == undefined || remark == ''" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Activity Stop</button>&nbsp;&nbsp;&nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </form>










        </div>


    </section>
    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>

<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script type="text/javascript">
                                    var app = angular.module("instrumentApp", ['angular.chosen', 'blockUI']);
                                    app.directive('chosen', function ($timeout) {

                                        var linker = function (scope, element, attr) {

                                            scope.$watch('instrumentLogData', function () {
                                                $timeout(function () {
                                                    element.trigger('chosen:updated');
                                                }, 0, false);
                                            }, true);
                                            scope.$watch('productData', function () {
                                                $timeout(function () {
                                                    element.trigger('chosen:updated');
                                                }, 0, false);
                                            }, true);
                                            $timeout(function () {
                                                element.chosen();
                                            }, 0, false);
                                        };
                                        return {
                                            restrict: 'A',
                                            link: linker
                                        };
                                    });
                                    app.controller("instrumentCtrl", function ($scope, $http, $filter, blockUI) {
                                        var urlParams = new URLSearchParams(window.location.search);
                                        var room_code = urlParams.get('room_code');
                                        var processid = urlParams.get('processid');
                                        var actid = urlParams.get('actid');
                                        var actname = urlParams.get('actname');

                                        // get header detail 
                                $scope.headerRecordid = urlParams.get('headerid');
                                $scope.formno = urlParams.get('formno');
                                $scope.versionno = urlParams.get('versionno');
                                $scope.effectivedate = urlParams.get('effectivedate');
                                $scope.retireddate = urlParams.get('retireddate');
                                //end

                                        $scope.result_right = "";
                                        $scope.result_left = "";
                                        $scope.remark = "";
                                        $scope.room_code_show = room_code;
                                        $scope.roleList = [];
                                        $scope.getRoleList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.roleList = response.data.role_list;
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.getRoleList();
                                        $scope.batchInProductData = [];
                                        $scope.getRoomBatchInProductData = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getRoomBatchInProductData?room_code=' + room_code,
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.batchInProductData = response.data.batch_product_list;
                                                if ($scope.batchInProductData.length > 0) {
                                                    $scope.batch_no = $scope.batchInProductData[0].batch_no;
                                                    $scope.product_no = $scope.batchInProductData[0].product_code;
                                                    $scope.product_desc = $scope.batchInProductData[0].product_desc;
                                                }
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");

                                            });
                                        }
                                        $scope.getRoomBatchInProductData();
                                        $scope.instrumentLogData = [];

                                        $scope.getInstrumentList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetInstrumentList?equipment_type=Instrument',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.instrumentLogData = response.data.instrument_data;
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.getInstrumentList();
                                        //Get Document Number code
                                        $scope.docno = "";
                                        // $scope.getdocno = function () {
                                        //     $http({
                                        //         url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getdocno',
                                        //         method: "GET",
                                        //         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        //     }).then(function (response) {
                                        //         $scope.docno = response.data.docno;
                                        //     }, function (error) { // optional

                                        //         console.log("Something went wrong.Please try again");
                                        //     });
                                        // }
                                        // $scope.getdocno();
                                        //End of Get Document number function code
                                        //Get Stage List
                                        $scope.getStageList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetStageList?log=Instrument Log Register&field_name=Stage',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.stageData = response.data.stage_list;
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");

                                            });
                                        }
                                        $scope.getStageList();

                                        $scope.ResultLeftData = [];
                                        //Get Result Left List
                                        $scope.getResultLeftList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetStageList?log=Instrument Log Register&field_name=ResultLeft',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.ResultLeftData = response.data.stage_list;
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");

                                            });
                                        }
                                        $scope.getResultLeftList();

                                        $scope.ResultRightData = [];
                                        //Get Result Right List
                                        $scope.getResultRightList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetStageList?log=Instrument Log Register&field_name=ResultRight',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.ResultRightData = response.data.stage_list;
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");

                                            });
                                        }
                                        $scope.getResultRightList();
                                        $scope.productData = [];
                                        $scope.getProductList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.productData = response.data.product_list;
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");

                                            });
                                        }
                                        $scope.getProductList();
                                        $scope.product_desc = "";
                                        $scope.getProductDetail = function (dataObj) {
                                            $scope.productcode = dataObj;
                                            $scope.product_desc = $filter('filter')($scope.productData, {product_code: dataObj})[0].product_name;
                                        }


                                        /////////////////////////////////////////



//*******************Bhupendra's code Started**********************//

                                        $scope.setbtntext = function (btntext)
                                        {
                                            $scope.btntext = btntext;
                                        }
                                        $scope.setresultright = function (result)
                                        {
                                            $scope.result_right = result;
                                        }
                                        $scope.setresultleft = function (result)
                                        {
                                            $scope.result_left = result;
                                        }
                                        $scope.edit = false;
                                        //Login Function
                                        //Created by Bhupendra
                                        //Date : 28/01/2020                                    
                                        $scope.login = function () {
                                            var response = confirm("Do you really want to perform this task?");
                                            if (response == true)
                                            {
                                                $('.loader').show();
                                                pwd2 = SHA256($scope.password);
                                                $http({
                                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                                    method: "POST",
                                                    data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&module_id=" + actid + "&module_type=log" + "&action=" + $scope.btntext+ "&auth=" + btoa(btoa($scope.password))+"&act_id="+actid,
                                                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                }).then(function (response) {
                                                    if (response.data.message == "")
                                                    {
                                                        var equipment_code = "";
                                                        var selectedfilters = "";
                                                        var temp = [];
                                                        angular.forEach($scope.selectedFilter, function (obj, index) {
                                                            temp.push(obj.id);
                                                        });
                                                        var selectedfilters = temp.toString();

                                                        if ($scope.btntext == 'approval')
                                                        {
                                                            //Calling Approval function
                                                            //Created by Bhupendra
                                                            //Date : 10/02/2020
                                                            // if ($scope.result_right == "") {
                                                            //     alert("Please select result right");
                                                            //     return false;
                                                            // } 
                                                            // else if ($scope.result_left == "") {
                                                            //     alert("Please select result left");
                                                            //     return false;
                                                            // } 
                                                            $scope.getapproved(room_code, $scope.ComDataArray[0].id, $scope.ComDataArray[0].activity_id, $scope.ComDataArray[0].workflowstatus, $scope.ComDataArray[0].workflownextstep, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, 'stop', $scope.ComDataArray[0].doc_id, $scope.result_right, $scope.result_left)
                                                        } else if ($scope.btntext == 'submit')
                                                        {
                                                            //Calling Stoped function
                                                            //Created by Bhupendra
                                                            //Date : 10/02/2020

                                                            $scope.getsubmit(room_code, processid, actid, actname, $scope.docno, $scope.instrument_code, $scope.product_no, $scope.batch_no, $scope.stage, $scope.test, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark,$scope.headerRecordid)
                                                        } else if ($scope.btntext == 'edit') {
                                                            //edit Functionality
                                                            //Created by Rahul
                                                            //Date : 04/05/2020
//                                                            var userdata = response.data.userdata;
//                                                            if (userdata.role_id > $scope.ComDataArray[0].done_by_role_id) {
//                                                                $scope.edit = true;
//                                                                $scope.docno = $scope.ComDataArray[0].doc_no;
//                                                                $scope.instrument_code = $scope.ComDataArray[0].instrument_code;
//                                                                $scope.stage = $scope.ComDataArray[0].stage;
//                                                                $scope.test = $scope.ComDataArray[0].test;
//                                                                if ($scope.batchInProductData.length > 0) {
//                                                                    $scope.batch_no = $scope.batchInProductData[0].batch_no;
//                                                                    $scope.product_no = $scope.batchInProductData[0].product_code;
//                                                                } else {
//                                                                    $scope.batch_no = $scope.ComDataArray[0].batch_no;
//                                                                    $scope.product_no = $scope.ComDataArray[0].product_code;
//                                                                }
//                                                                $scope.getProductDetail($scope.product_no);
//                                                                $scope.is_in_workflow = $scope.ComDataArray[0].is_in_workflow;
//                                                            } else {
//                                                                alert("You are not allowed to edit this.");
//                                                                $scope.edit = false;
//                                                            }
//                                                            $scope.resetLoginForm();
//                                                            angular.element("#btnclose").trigger('click');
                                                        } else if ($scope.btntext == 'update')
                                                        {
                                                            //update Functionality
                                                            //Created by Rahul
                                                            //Date : 04/05/2020
//                                                            var userdata = response.data.userdata;
//                                                            if (userdata.role_id > $scope.ComDataArray[0].done_by_role_id) {
                                                            var role = "";
                                                            if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                                if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                                    var workflowtype = 'start';
                                                                } else {
                                                                    var workflowtype = 'stop';
                                                                }

                                                                var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                                var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                            } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep == 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {

                                                                var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflowstatus})[0].role_id;
                                                                var role = $filter('filter')($scope.roleList, {id: desc})[0].id;

                                                            } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time != null) {
                                                                if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                                    var workflowtype = 'start';
                                                                } else {
                                                                    var workflowtype = 'stop';
                                                                }
                                                                var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                                var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                            }
//                                                  
                                                            var userdata = response.data.userdata;
                                                            if (userdata.role_id >= $scope.ComDataArray[0].last_approval.role_id && userdata.role_id > 1) {
                                                                $scope.updatesubmit(room_code, processid, actid, actname, $scope.docno, $scope.instrument_code, $scope.product_no, $scope.batch_no, $scope.stage, $scope.test, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.ComDataArray[0].edit_id, $scope.is_in_workflow);
                                                            } else {
                                                                $('.loader').hide();
                                                                alert("You are not allowed to edit this.");
                                                                $scope.edit = false;
                                                            }
                                                        }
                                                    } else {
                                                        $('.loader').hide();
                                                        alert(response.data.message);
                                                    }
                                                }, function (error) {
                                                    $('.loader').hide();
                                                    console.log(error);
                                                });
                                            }
                                        }
                                        //End Login Function

                                        //Approval function :- To Approve running activity by auth & privilege chk as well as sent that activity to next approval which exists in workflow
                                        //Created by Bhupendra
                                        //Date : 28/01/2020
                                        $scope.getapproved = function (roomcode, actlogtblid, actid, workflowstatus, workflownextstep, roleid, empid, empname, email, remark, actstatus, docno, result_rgt, result_lft) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/com_no_approval_fun2',
                                                method: "POST",
                                                data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&workflowstatus=" + workflowstatus + "&workflownextstep=" + workflownextstep + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&actstatus=" + actstatus + "&docno=" + docno + "&result_rgt=" + result_rgt + "&result_lft=" + result_lft + "&tblname=pts_trn_instrument_log_register",

                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                alert(response.data.message);
                                                if (response.data.next_step == "-1")
                                                {
                                                    window.location.href = "<?php echo base_url() ?>home";
                                                } else
                                                {
                                                    location.reload();
                                                    $('.loader').hide();
                                                }
                                                // $scope.resetLoginForm();
                                                // $scope.result_right = "";
                                                // $scope.result_left = "";
                                                // $scope.remark = "";
                                                // $scope.instrument_code = "";
                                                // $scope.product_no = "";
                                                // $scope.batch_no = "";
                                                // $scope.stage = "";
                                                // $scope.test = "";
                                                // $scope.product_desc = "";
                                                // angular.element("#btnclose").trigger('click');
                                                // $scope.getLastFilterActivity();
                                                // $scope.getdocno();

                                            }, function (error) {
                                                $('.loader').hide();
                                                console.log(error);
                                            });
                                        }
//*********************************************************************************
//Start function :- To Start new activity by auth chk.
//Created by Bhupendra
//Date : 4/02/2020
                                        $scope.getsubmit = function (roomcode, processid, actid, actname, docno, instrument_code, product_no, batch_no, stage, test, roleid, empid, empname, email, remark,headerRecordid) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/submit_instrument_log_record',
                                                method: "POST",
                                                data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&instrument_code=" + instrument_code + "&product_no=" + product_no + "&batch_no=" + batch_no + "&stage=" + stage + "&test=" + test + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark+"&headerRecordid="+headerRecordid,

                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                alert(response.data.message);
                                                $scope.result_right = "";
                                                $scope.result_left = "";
                                                $scope.remark = "";
                                                $scope.instrument_code = "";
                                                $scope.product_no = "";
                                                $scope.batch_no = "";
                                                $scope.stage = "";
                                                $scope.test = "";
                                                $scope.product_desc = "";
                                                angular.element("#btnclose").trigger('click');
                                                $scope.resetLoginForm();
                                                $scope.getLastFilterActivity();
                                                $('.loader').hide();
                                                //$scope.getdocno();

                                            }, function (error) {
                                                $('.loader').hide();
                                                console.log(error);
                                            });
                                        }
//*********************************************************************************************


//*********************************************************************************
//Start function :- To Update started Activity.
//Created by Raul Chauhan
//Date : 04/05/2020
                                        $scope.updatesubmit = function (roomcode, processid, actid, actname, docno, instrument_code, product_no, batch_no, stage, test, roleid, empid, empname, email, remark, update_id, is_in_workflow) {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/update_instrument_log_record',
                                                method: "POST",
                                                data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&instrument_code=" + instrument_code + "&product_no=" + product_no + "&batch_no=" + batch_no + "&stage=" + stage + "&test=" + test + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&update_id=" + update_id + "&is_in_workflow=" + is_in_workflow,

                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                alert(response.data.message);
                                                $scope.edit = false;
                                                angular.element("#btnclose").trigger('click');
                                                $scope.resetLoginForm();
                                                $scope.getLastFilterActivity();
                                                $('.loader').hide();
                                            }, function (error) {
                                                $('.loader').hide();
                                                console.log(error);
                                            });
                                        }

                                        $scope.CancilUpdate = function () {
                                            $scope.edit = false;
                                        }
//*********************************************************************************************

                                        //Get Last unapproved Room Activity
                                        $scope.ComDataArray = [];
                                        $scope.getLastFilterActivity = function () {
                                            $scope.ComDataArray = [];
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetLastFilterActivity?room_code=' + room_code + "&table_name=pts_trn_instrument_log_register",
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.ComDataArray = response.data.row_data;
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.getLastFilterActivity();

                                        //*******************Rahul's code Started**********************//
                                        $scope.editTrasaction = function () {
                                            if (confirm("Do you really want to edit ?"))
                                            {
                                                $scope.edit = true;
                                                $scope.docno = $scope.ComDataArray[0].doc_no;
                                                $scope.instrument_code = $scope.ComDataArray[0].instrument_code;
                                                $scope.stage = $scope.ComDataArray[0].stage;
                                                $scope.test = $scope.ComDataArray[0].test;
                                                if ($scope.batchInProductData.length > 0) {
                                                    $scope.batch_no = $scope.batchInProductData[0].batch_no;
                                                    $scope.product_no = $scope.batchInProductData[0].product_code;
                                                } else {
                                                    $scope.batch_no = $scope.ComDataArray[0].batch_no;
                                                    $scope.product_no = $scope.ComDataArray[0].product_code;
                                                }
                                                $scope.getProductDetail($scope.product_no);
                                                $scope.is_in_workflow = $scope.ComDataArray[0].is_in_workflow;
                                            }
                                        }

                                        //*******************End Rahul's code***************************//
                                        //Reset Login Form
                                        $scope.resetLoginForm = function () {
                                            $scope.username = "";
                                            $scope.password = "";
                                            $scope.remark = "";
                                        }
                                    });
</script>