<div  ng-app="materialApp" ng-controller="materialCtrl" ng-cloak>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>sf_home">Home</a></li>
            <li class="breadcrumb-item"><a href="#">{{room_code_show}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Material Retrieval & Relocation</li>
        </ol>
    </nav>
    <section class="contentBox blueBorder formWrapper">
        <div class="contentHeader">
            <div class="row">
                <div class="col-12">
                    <h1>Header Details</h1>
                </div>
            </div>
        </div>
        <div class="contentBody">
            <form>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <ul class="activityList">
                                <li style="margin-left: 100px;"><a><b>Form No : </b>{{formno}} </a></li>
                                <li style="margin-left: 100px;"><a><b>Version No : </b>{{versionno}} </a></li>
                                <li style="margin-left: 100px;"><a><b>Effective Date : </b>{{effectivedate| date:'dd-MMM-yyyy'}} </a></li>
                                <li style="margin-left: 100px;"><a><b>Retired Date : </b>{{retireddate| date:'dd-MMM-yyyy'}} </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

    <section class="contentBox blueBorder formWrapper">


        <div class="contentHeader">

            <div class="row">
                <div class="col-12">
                    <h1>Material Retrieval & Relocation Record for Cold Room</h1>
                </div>
            </div>

        </div>

        <div class="contentBody" ng-show="(ComDataArray.length == 0 || edit == true) && !batchRunning">
            <form name="materialForm" novalidate>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Document No.</label>
                            <input type="text" class="form-control" value="{{docno}}" readonly>    
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Action</label>
                            <p><input type="radio" id="condition" ng-click="resetarray('in')" ng-disabled="edit" name="mat_condition" value="in" ng-model="material_condition" required> Material IN&nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="radio" ng-click="resetarray('out')" name="mat_condition" value="out" ng-disabled="edit" ng-model="material_condition" required> Material OUT</p>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-6" ng-show="material_condition == 'in'">
                        <div class="form-group" ng-show="batchInProductData.length == 0">
                            <label><span class="asterisk">* </span>Code</label>
                            <select  class="chzn-select form-control" tabindex="4"  name="material_code"  data-placeholder="Search Product No." ng-change="GetProductDetailbycode(dataObj.product_code)" ng-options="dataObj['product_code'] as dataObj.product_code for dataObj in productData"  ng-model="material_code" chosen>
                                <option value="NA">Select Product No</option>
                            </select>
                            <p ><b>{{product_desc}}</b></p>
                        </div>  
                        <div class="form-group" ng-show="batchInProductData.length > 0">
                            <label><span class="asterisk">* </span>Product Code</label>
                            <p><b>{{product_desc}}</b></p>
                        </div>
                    </div>
                    <div class="col-sm-6" ng-show="material_condition == 'out'">
                        <div class="form-group" ng-show="batchInProductData.length == 0">
                            <label><span class="asterisk">* </span>Code</label>
                            <select  class="chzn-select form-control" tabindex="4"  name="material_code"  data-placeholder="Search Product No." ng-change="GetProductDetailbycode(dataObj.product_code);getMaterialInBatchList(material_code)" ng-options="dataObj['product_code'] as dataObj.product_code for dataObj in productData"  ng-model="material_code" chosen>
                                <option value="NA">Select Product No</option>
                            </select>
                            <p ><b>{{product_desc}}</b></p>
                        </div>
                        <div class="form-group" ng-show="batchInProductData.length > 0">
                            <label><span class="asterisk">* </span>Product Code</label>
                            <p><b>{{product_desc}}</b></p>
                        </div>
                    </div>

                    <div class="col-sm-6" ng-show="material_condition == 'in'">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Batch No.</label>
                            <p ng-show="batchInProductData.length > 0"><b>{{material_batch_no}}</b></p>
                            <input ng-show="batchInProductData.length == 0" ng-blur="getMaterialDetail()" class="form-control" type="text" placeholder="Batch No." name="product_no" ng-model="material_batch_no" >
                        </div>
                    </div>
                    <div class="col-sm-6" ng-show="material_condition == 'out'">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Batch No.</label>
                            <p ng-show="batchInProductData.length > 0"><b>{{material_batch_no}}</b></p>
                            <select  ng-show="batchInProductData.length == 0" class="form-control" tabindex="4"  name="product_no"  data-placeholder="Batch No." ng-change="getMaterialDetail()" ng-options="dataObj['material_batch_no'] as dataObj.material_batch_no for dataObj in materialInBatchData"  ng-model="material_batch_no">
                                <option value="">Select Batch No.</option>
                            </select>
                            <!--<input ng-show="batchInProductData.length == 0" ng-blur="getMaterialDetail()" class="form-control" type="text" placeholder="Batch No." name="product_no" ng-model="material_batch_no" >-->
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Type</label>
                            <select class="form-control" name="material_type" ng-model="material_type">
                                <option value="A">A</option>
                                <option value="P">P</option>
                                <option value="E">E</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row" ng-show="matdata.length > 0 && material_condition == 'out'">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <table class="table custom-table" >
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Room code</th>
                                        <th>Code</th>
                                        <th>Batch no</th>
                                        <th>Type</th>
                                        <th>In By</th>
                                    </tr>
                                </thead>
                                <tbody ng-show="matdata.length > 0">
                                    <tr ng-repeat="dataobj in matdata">
                                        <td>{{dataobj.created_on}}</td>
                                        <td>{{dataobj.room_code}}</td>
                                        <td>{{dataobj.material_code}}</td>
                                        <td>{{dataobj.material_batch_no}}</td>
                                        <td>{{dataobj.material_type}}</td>
                                        <td>{{dataobj.done_by_user_name}} </td>                                      
                                    </tr>
                                </tbody>
                            </table>                        
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <button ng-show="edit" class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancel</button>&nbsp;&nbsp;&nbsp;
                            <button ng-show="edit" ng-click="setbtntext('update');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Update</button>&nbsp;&nbsp;&nbsp;
                            <button ng-show="!edit" ng-click="setbtntext('submit');" ng-disabled="materialForm.$invalid || material_code == '' || material_batch_no == '' || material_code == undefined || material_batch_no == undefined" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Done By</button> &nbsp;&nbsp;&nbsp;&nbsp;
                            <button ng-show="!edit" ng-click="setbtntext('approval');" disabled data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
        <div class="contentBody" ng-show="(ComDataArray.length > 0 && edit == false) && !batchRunning">

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Document No.</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].doc_no}}" readonly>    
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Action</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].material_condition}}" readonly>   
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Code</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].material_code}}" readonly>   
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Batch No.</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].material_batch_no}}" readonly>      
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Type</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].material_type}}" readonly>   
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-sm-12 text-right">
                    <div class="formBtnWrap">
                        <button ng-click="editTrasaction()" class="button smallBtn primaryBtn" >Edit</button>&nbsp;&nbsp;&nbsp;
                        <!-- <button ng-click="setbtntext('submit');" disabled data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Done By</button> &nbsp;&nbsp;&nbsp;&nbsp; -->
                        <button ng-click="setbtntext('approval');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button>

                    </div>
                </div>
            </div>


        </div>
        <div class="contentBody" ng-show="batchRunning">Batch In Is Done. Please Do Batch Out First.</div>

    </section>
    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script type="text/javascript">
                            var app = angular.module("materialApp", ['angular.chosen', 'blockUI']);
                            app.directive('chosen', function ($timeout) {

                                var linker = function (scope, element, attr) {

                                    scope.$watch('productData', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    $timeout(function () {
                                        element.chosen();
                                    }, 0, false);
                                };
                                return {
                                    restrict: 'A',
                                    link: linker
                                };
                            });
                            app.controller("materialCtrl", function ($scope, $http, $filter, blockUI) {
                                var urlParams = new URLSearchParams(window.location.search);
                                var room_code = urlParams.get('room_code');
                                $scope.material_condition = 'in';
                                $scope.material_type = "A";
                                $scope.materialData = [];
                                var processid = urlParams.get('processid');
                                var actid = urlParams.get('actid');
                                var actname = urlParams.get('actname');

                                // get header detail 
                                $scope.headerRecordid = urlParams.get('headerid');
                                $scope.formno = urlParams.get('formno');
                                $scope.versionno = urlParams.get('versionno');
                                $scope.effectivedate = urlParams.get('effectivedate');
                                $scope.retireddate = urlParams.get('retireddate');
                                $scope.terminal_id='<?php echo $_SERVER['REMOTE_ADDR']?>';
                                //end

                                $scope.batchRunning = false;
                                $scope.getbatchRunnig = function(){
                                    $scope.batchRunning = false;
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetrunningBatchData?terminal_id='+$scope.terminal_id,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        if(response.data.BatchDetail.length>0){
                                            $scope.batchRunning = true;
                                        }
                                    }, function (error) {
                                        console.log(error);
                                    });
                                }
                                $scope.getbatchRunnig();
                                $scope.room_code_show = room_code;
                                $scope.roleList = [];
                                $scope.getRoleList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.roleList = response.data.role_list;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getRoleList();
                                //Get Employee Role List
                                $scope.employeRole = [];
                                $scope.getEmployeRoleList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getEmployeRoleList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.employeRole = response.data.role_data;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getEmployeRoleList();
                                $scope.batchInProductData = [];
                                $scope.getRoomBatchInProductData = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getRoomBatchInProductData?room_code=' + room_code,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.batchInProductData = response.data.batch_product_list;
                                        if ($scope.batchInProductData.length > 0) {

                                            $scope.material_batch_no = $scope.batchInProductData[0].batch_no;
                                            $scope.material_code = $scope.batchInProductData[0].product_code;
                                            $scope.product_desc = $scope.batchInProductData[0].product_desc;

                                            $scope.getProductDetail($scope.material_code);

                                        }
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.resetarray = function (value) {
                                    if (value == 'in') {
                                        $scope.docno = "";
                                        $scope.materialInBatchData = [];
                                        $scope.material_batch_no="";
                                        
                                    } else {
                                        $scope.material_code="";
                                        $scope.product_desc = "";
                                        $scope.getMaterialDetail();
                                    }

                                }
                                $scope.getRoomBatchInProductData();
                                $scope.productData = [];
                                $scope.getProductList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.productData = response.data.product_list;
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getProductList();
                                $scope.product_desc = "";
                                $scope.getProductDetail = function (dataObj) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/chkmatIn_Out',
                                        method: "POST",
                                        data: "matcode=" + $scope.material_code,
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (res) {
                                        if ($scope.material_condition == 'out') {
                                            if (res.data.row_data.length > 0 && res.data.row_data[0].material_condition == "in") {
                                                $scope.docno = res.data.row_data[0].doc_no;
                                            }
                                        } else {
                                            //$scope.getdocno();
                                        }
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                    });
                                    $scope.product_desc = $filter('filter')($scope.productData, {product_code: dataObj})[0].product_name;
                                }

                                $scope.GetProductDetailbycode = function (dataObj) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductDetailbycode',
                                        method: "POST",
                                        data: "matcode=" + $scope.material_code,
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (res) {
                                        $scope.product_desc = res.data.row_data[0].product_name;
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                    });
                                }

                                $scope.matdata = [];
                                $scope.getMaterialDetail = function () {
                                    $scope.matdata = [];
                                    if ($scope.material_condition == 'out')
                                    {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getmatindtl',
                                            method: "POST",
                                            data: "matcode=" + $scope.material_code + "&matbatchcode=" + $scope.material_batch_no,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (res) {
                                            if(res.data.row_data.length > 0){
                                            $scope.matdata = res.data.row_data;
                                            //$scope.docno = '';//$scope.matdata[0].doc_no;
                                            $scope.material_type = $scope.matdata[0].material_type;
                                        }
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                }

                                $scope.docno = "";

//*******************Bhupendra's code Started**********************//

                                $scope.setbtntext = function (btntext)
                                {
                                    $scope.btntext = btntext;
                                }
                                $scope.edit = false;
                                //Login Function
                                //Created by Bhupendra
                                //Date : 28/01/2020                                    
                                $scope.login = function () {
                                    var response = confirm("Do you really want to perform this task?");
                                    if (response == true)
                                    {
                                        $('.loader').show();
                                        pwd2 = SHA256($scope.password);
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                            method: "POST",
                                            data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&module_id=" + actid + "&module_type=log" + "&action=" + $scope.btntext + "&auth=" + btoa(btoa($scope.password)) + "&act_id=" + actid,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            if (response.data.message == "")
                                            {
                                                if ($scope.btntext == 'approval')
                                                {
                                                    //Calling Approval function
                                                    //Created by Bhupendra
                                                    //Date : 10/02/2020

                                                    $scope.getapproved(room_code, $scope.ComDataArray[0].id, $scope.ComDataArray[0].activity_id, $scope.ComDataArray[0].workflowstatus, $scope.ComDataArray[0].workflownextstep, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, 'stop', $scope.ComDataArray[0].doc_id, $scope.ComDataArray[0].material_condition);

                                                } else if ($scope.btntext == 'submit')
                                                {
                                                    //Calling Stoped function
                                                    //Created by Bhupendra
                                                    //Date : 10/02/2020
                                                    $scope.formData = [{"material_code": $scope.material_code, "material_batch_no": $scope.material_batch_no, "material_type": $scope.material_type}];
                                                    $scope.getsubmit(room_code, processid, actid, actname, $scope.docno, $scope.material_condition, $scope.formData, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark);

                                                    // $scope.getLastMaterialRecord = function () {
                                                    //     $http({
                                                    //         url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/chkmatIn_Out',
                                                    //         method: "POST",
                                                    //         data: "matcode=" + $scope.material_code,
                                                    //         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    //     }).then(function (res) {
                                                    //         console.log($scope.material_type);
                                                    //         if (res.data.message == "No recode found")
                                                    //         {
                                                    //             $scope.formData = [{"material_code": $scope.material_code, "material_batch_no": $scope.material_batch_no, "material_type": $scope.material_type}];
                                                    //             $scope.getsubmit(room_code, processid, actid, actname, $scope.docno, $scope.material_condition, $scope.formData, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark);
                                                    //         } else
                                                    //         {
                                                    //             if (res.data.row_data[0]['material_condition'] == $scope.material_condition)
                                                    //             {
                                                    //                 if (res.data.row_data[0]['material_condition'] == 'in')
                                                    //                 {
                                                    //                     alert("This material is already " + res.data.row_data[0]['material_condition'] + ", please do out first");
                                                    //                 } else
                                                    //                 {
                                                    //                     alert("This material is already " + res.data.row_data[0]['material_condition'] + ", please do in first");
                                                    //                 }
                                                    //                 angular.element("#btnclose").trigger('click');
                                                    //                 return false;
                                                    //             } else
                                                    //             {
                                                    //                 $scope.formData = [{"material_code": $scope.material_code, "material_batch_no": $scope.material_batch_no, "material_type": $scope.material_type}];
                                                    //                 $scope.getsubmit(room_code, processid, actid, actname, $scope.docno, $scope.material_condition, $scope.formData, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark);
                                                    //             }
                                                    //         }
                                                    //     }, function (error) { // optional
                                                    //         console.log("Something went wrong.Please try again");
                                                    //     });
                                                    // }
                                                    // $scope.getLastMaterialRecord();
                                                } else if ($scope.btntext == 'edit')
                                                {
                                                    //edit Functionality
                                                    //Created by Rahul
                                                    //Date : 04/05/2020
//                                                    var userdata = response.data.userdata;
//                                                    if (userdata.role_id > $scope.ComDataArray[0].done_by_role_id) {
//                                                        $scope.edit = true;
//                                                        $scope.docno = $scope.ComDataArray[0].doc_no;
//                                                        $scope.is_in_workflow = $scope.ComDataArray[0].is_in_workflow;
//                                                        $scope.material_condition = $scope.ComDataArray[0].material_condition;
//                                                        $scope.material_code = $scope.ComDataArray[0].material_code;
//                                                        $scope.material_batch_no = $scope.ComDataArray[0].material_batch_no;
//                                                        $scope.material_type = $scope.ComDataArray[0].material_type;
//                                                        $scope.getProductDetail($scope.product_no);
//                                                    } else {
//                                                        alert("You are not allowed to edit this.");
//                                                        $scope.edit = false;
//                                                    }
//                                                    $scope.resetLoginForm();
//                                                    angular.element("#btnclose").trigger('click');
                                                } else if ($scope.btntext == 'update') {
                                                    //update Functionality
                                                    //Created by Rahul
                                                    //Date : 04/05/2020
                                                    var role = "";
                                                    if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                        if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }

                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep == 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {

                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflowstatus})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;

                                                    } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time != null) {
                                                        if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }
                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    }
//                                                  
                                                    var userdata = response.data.userdata;
                                                    if (userdata.role_id >= $scope.ComDataArray[0].last_approval.role_id && userdata.role_id > 1) {
//                                                    if (userdata.role_id > $scope.ComDataArray[0].done_by_role_id) {
                                                        $scope.updatesubmit(room_code, processid, actid, actname, $scope.docno, $scope.material_condition, $scope.material_code, $scope.material_batch_no, $scope.material_type, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.ComDataArray[0].edit_id, $scope.is_in_workflow);
                                                    } else {
                                                        $('.loader').hide();
                                                        alert("You are not allowed to edit this.");
                                                        $scope.edit = false;
                                                    }
                                                }
                                            } else {
                                                $('.loader').hide();
                                                alert(response.data.message);
                                            }
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                }
                                //End Login Function

                                //Approval function :- To Approve running activity by auth & privilege chk as well as sent that activity to next approval which exists in workflow
                                //Created by Bhupendra
                                //Date : 28/01/2020

                                $scope.getapproved = function (roomcode, actlogtblid, actid, workflowstatus, workflownextstep, roleid, empid, empname, email, remark, actstatus, docno, material_condition) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/com_approval_fun',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&workflowstatus=" + workflowstatus + "&workflownextstep=" + workflownextstep + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&actstatus=" + actstatus + "&docno=" + docno + "&tblname=pts_trn_material_retreival_relocation&material_condition=" + material_condition,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        if (response.data.next_step == "-1")
                                        {
                                            window.location.href = "<?php echo base_url() ?>home";
                                        } else
                                        {
                                            $scope.resetLoginForm();
                                            angular.element("#btnclose").trigger('click');
                                            $scope.docno = "";
                                            $scope.standardData = [];
                                            $scope.product_desc = "";
                                            $scope.material_code = "";
                                            $scope.material_batch_no = "";
                                            $scope.getLastFilterActivity();
                                            $('.loader').hide();
                                            //$scope.getdocno();
                                        }
                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }
                                //*********************************************************************************
                                //Start function :- To Start new activity by auth chk.
                                //Created by Bhupendra
                                //Date : 4/02/2020
                                $scope.getsubmit = function (roomcode, processid, actid, actname, docno, material_condition, formData, roleid, empid, empname, email, remark) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/submit_mat_retreival_record',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&material_condition=" + material_condition + "&formData=" + encodeURIComponent(angular.toJson(formData)) + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&headerRecordid=" + $scope.headerRecordid,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        angular.element("#btnclose").trigger('click');
                                        $scope.standardData = [];
                                        $scope.product_desc = "";
                                        $scope.material_code = "";
                                        $scope.material_batch_no = "";
                                        $scope.matdata = [];
                                        $scope.formData = [{"material_code": "", "material_desc": "", "material_batch_no": "", "material_type": ""}];
                                        $scope.getRoomBatchInProductData();
                                        $scope.getLastFilterActivity();
                                        $('.loader').hide();
                                        //$scope.getdocno();

                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }
                                //*********************************************************************************************

                                //*********************************************************************************
                                //Start function :- To Update started Activity.
                                //Created by Raul Chauhan
                                //Date : 05/05/2020

                                $scope.updatesubmit = function (roomcode, processid, actid, actname, docno, material_condition, material_code, material_batch_no, material_type, roleid, empid, empname, email, remark, update_id, is_in_workflow) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/update_mat_retreival_record',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&material_condition=" + material_condition + "&material_code=" + material_code + "&material_batch_no=" + material_batch_no + "&material_type=" + material_type + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&update_id=" + update_id + "&is_in_workflow=" + is_in_workflow,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.edit = false;
                                        angular.element("#btnclose").trigger('click');
                                        $scope.resetLoginForm();
                                        $scope.getLastFilterActivity();
                                        //$scope.getLastProductAndBatchNumber($scope.process_log, $scope.portable_log);
                                        $('.loader').hide();
                                        //$scope.getdocno();

                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }

                                $scope.CancilUpdate = function () {
                                    $scope.edit = false;
                                }


                                //Get Last unapproved Room Activity
                                $scope.ComDataArray = [];
                                $scope.getLastFilterActivity = function () {
                                    $scope.ComDataArray = [];
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetLastFilterActivity?room_code=' + room_code + "&table_name=pts_trn_material_retreival_relocation",
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        var temp = response.data.row_data;
                                        if (temp.length > 0 && temp[0].material_condition == 'out') {
                                            $scope.ComDataArray[0] = temp[0];
                                            $scope.getMaterialInBatchList($scope.ComDataArray[0].material_code);
                                        } else {
                                            if (temp.length > 0) {
                                                $scope.ComDataArray[0] = temp[0];
                                            }
                                        }


                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }

                                $scope.getLastFilterActivity();

                                //*******************Rahul's code Started**********************//
                                $scope.editTrasaction = function () {
                                    if (confirm("Do you really want to edit ?"))
                                    {
                                        $scope.edit = true;
                                        $scope.docno = $scope.ComDataArray[0].doc_no;
                                        $scope.is_in_workflow = $scope.ComDataArray[0].is_in_workflow;                                        
                                        $scope.material_condition = $scope.ComDataArray[0].material_condition;
                                        $scope.material_code = $scope.ComDataArray[0].material_code;
                                        $scope.material_batch_no = $scope.ComDataArray[0].material_batch_no;
                                        $scope.material_type = $scope.ComDataArray[0].material_type;
                                        $scope.GetProductDetailbycode($scope.product_no);
                                    }
                                }

                                //*******************End Rahul's code***************************//
                                //Reset Login Form
                                $scope.resetLoginForm = function () {
                                    $scope.username = "";
                                    $scope.password = "";
                                    $scope.remark = "";
                                }

                                //Date-21-10-2020
                                //Function to get Batch Number for Material Out condition for product
                                $scope.materialInBatchData = [];
                                $scope.getMaterialInBatchList = function (material_code) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getMaterialInBatchList',
                                        method: "POST",
                                        data: "roomcode=" + room_code + "&material_code=" + material_code,
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.materialInBatchData = response.data.batch_list;
                                    }, function (error) {
                                        console.log(error);
                                    });
                                }

                            });
</script>
