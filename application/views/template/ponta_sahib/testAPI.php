<div ng-app="testAPIApp" ng-controller="testAPI" ng-cloak>
    <div>
        <section>
            <div class="contentBody">
                <form>
                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <div class="formBtnWrap">
                                <button ng-show="!edit" data-toggle="modal" data-target="#loginModal"  ng-click="login();"  class="button smallBtn primaryBtn">Send to workflow</button>                                
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>    
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="https://cdn.rawgit.com/McNull/angular-block-ui/v0.1.1/dist/angular-block-ui.min.js"></script>
<script>
    var app = angular.module("testAPIApp", ['angular.chosen', 'blockUI']);
    app.directive('chosen', function ($timeout) {
        var linker = function (scope, element, attr) {
            scope.$watch('productData', function () {
            $timeout(function () {
            element.trigger('chosen:updated');
                }, 0, false);
                }, true);
                $timeout(function () {
                element.chosen();
                    }, 0, false);
                    };
                    return {
                    restrict: 'A',
                link: linker
            };
        });
        app.controller("testAPI", function ($scope, $http, $filter, blockUI, $timeout) {
        $scope.actid = 11;
        //*******************Bhupendra's code Started**********************//
            //Login Function 
            //Created by Bhupendra 
            //Date : 28/01/2020                                    
            $scope.login = function () {
            var response = confirm("Do You Really Want To Prform This Task?");
            if (response == true)
            {
                pwd2 = SHA256($scope.password);
                $http({
                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/UserAuth',
                    method: "POST",
                    data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&actid=" + $scope.actid ,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    }).then(function (response) {
                        alert(response.data.message);
                    }, function (error) {
                        console.log(error);
                    });
            }
            }                                
        });
</script>
