<!DOCTYPE html>
<html lang="en-US">
<head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/chosen.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/elogbook.css">

    <title>E-Logbook</title>
    <link href="<?php echo base_url(); ?>css/fontfamily.css?family=Source+Sans+Pro:400,600,700&display=swap" rel="stylesheet">
    
    
    <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/screenfull.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/elogbook.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/angular-block-ui.min.css"/>
<style>
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
        </style>
  </head>
   <!--<div id="google_translate_element"></div>-->