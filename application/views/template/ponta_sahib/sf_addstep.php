<?php
$actname = $_GET['actname'];
$wftype = $_GET['wftype'];
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
$confirm_msg = 'Save';
if ($this->uri->segment(3) == 'editstep') {
    $confirm_msg = 'Update';
}
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        } else {
            $edit = false;
            $add = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1500);</script>";
    }
}
?>
<div class="container-fluid">
    <!-- Page Heading -->

    <div class="content-wrapper">
        <div class="row topheader">
            <div class="col-md-6">
                <h3><?php if ($this->uri->segment(3) == 'editstep') { ?> Edit <?php } else { ?> Add<?php } ?> Step in <?php echo $actname ?> for <?php echo $wftype ?> workflow</h3>
            </div>
        </div>

        <div class="card shadow mb-4">
            <div class="card-body">
                <div id="steps" class="addoperator">   
                    <?php
                    if (isset($data['wfdtl']) && $data['wfdtl'] == "") {
                        
                    } else {
                        if (isset($data['wfdtl'])) {
                            foreach ($data['wfdtl']->result() as $row) {
                                ?>
                                <div class='row' id='<?php echo "stepdiv" . $row->id ?>'><input type='hidden' name='role' id='role' value='<?php echo $row->role_id ?>' data-id='<?php echo $row->role_id ?>' data-level='<?php echo $row->role_level ?>' data-role='<?php echo $row->role_description ?>' /><div class='col-xl-3'></div><div class='col-xl-6'><div class='card' style='background-color: transparent;border-color:transparent;margin-bottom:0rem;'><div class='card-body bg-info'><div class='d-flex align-items-center'><div class='text-center'><img class='img-thumbnail circle img-fluid thumb64' src='<?php echo base_url(); ?>img/user/06.jpg' alt='Image'></div><div class='text-center'><h3 class='m-0'><?php echo $row->role_description ?></h3></div><div class='ml-auto align-self-start mt-3'><a name='role' value='<?php echo $row->role_id ?>' data-level='<?php echo $row->role_level ?>' data-role='<?php echo $row->role_description ?>' class='btn btn-danger' href='#' onclick="getremove('<?php echo $row->id ?>');"><em class='mr-2 fas fa-trash'></em><span>Delete</span></a></div></div></div><center><em class='fa-2x mt-2  fas fa-arrow-down'></em></center></div></div><div class='col-xl-3'></div></div>			
                                <?php
                            }
                        }
                    }
                    ?> 
                </div>

                <?php if ($this->uri->segment(3) == 'editstep') { ?>
                    <div class="row">
                        <div class="col-xl-3">
                        </div>
                        <div class="col-xl-6">
                            <!-- START card-->
                            <div class="card">                  
                                <!-- START card footer-->
                                <div class="card-footer">
                                    <div>
                                        <center>
                                            <div class="form-row">
                                                <div class="col-lg-8">
                                                    <label>Remarks<span style="color: red">* </span></label>
                                                    <textarea class="form-control" aria-label="With textarea" name="remarks" placeholder="Enter Remarks" id="remark"></textarea>
                                                </div>
                                                <div class="col-lg-2"><label><b>Active/Inactive</b></label>
                                                    <label class="switch">
                                                        <?php if ($_GET['status'] == 1) { ?>
                                                            <input type="checkbox" class="toggle-task" style="margin-top: 13px;" checked id="ev-click">
                                                        <?php } else { ?>
                                                            <input type="checkbox" class="toggle-task" style="margin-top: 13px;" id="ev-click">
                                                        <?php } ?>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    <input type="hidden" name="active_status" id="active_status" value="<?php echo $_GET['status'] ?>"/>
                                                </div>
                                            </div>


                                        </center>
                                    </div>
                                </div><!-- END card-footer-->
                            </div><!-- END card-->
                        </div>
                        <div class="col-xl-3">
                        </div>
                    </div>
                <?php } ?>
                <div class="row">
                    <div class="col-xl-3">
                    </div>
                    <div class="col-xl-6">
                        <!-- START card-->
                        <div class="card">                  
                            <!-- START card footer-->
                            <div class="card-footer">
                                <div>
                                    <center>
                                        <input type="hidden" id="type" name="type" value="<?php echo $data['type']; ?>" />
                                        <input type="hidden" id="actid" name="actid" value="<?php echo $data['actid']; ?>" />
                                        <?php if ($add) { ?>
                                            <?php if ($this->uri->segment(3) == 'editstep') { ?>
                                                <input class="btn btn-primary btn-sm" type="button" onclick="addstep()" id="addbtn" name="addbtn" value="Edit step" />&nbsp;&nbsp;&nbsp;&nbsp;
                                            <?php } else { ?>
                                                <input class="btn btn-primary btn-sm" type="button" onclick="addstep()" id="addbtn" name="addbtn" value="Add step" />&nbsp;&nbsp;&nbsp;&nbsp;
                                            <?php } ?>
                                            <input class="btn btn-success btn-sm" type="button" onclick="addworkflow();" name="Save" value="Save" />
                                            <input class="btn btn-danger btn-sm" type="button" onclick="cancilWorkflow()" name="Cancil" value="Cancel">
                                        <?php } else { ?>
                                            <?php if ($this->uri->segment(3) == 'editstep') { ?>
                                                <input class="btn btn-primary btn-sm" type="button" onclick="addstep()" id="addbtn" name="addbtn" value="Edit step" disabled/>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <?php } else { ?>
                                                <input class="btn btn-primary btn-sm" type="button" onclick="addstep()" id="addbtn" name="addbtn" value="Add step" disabled/>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <?php } ?>
                                            <input class="btn btn-success btn-sm" type="button" onclick="addworkflow();" name="Save" value="Save" disabled>
                                            <input class="btn btn-danger btn-sm" type="button" onclick="" name="Cancil" value="Cancel" disabled>
                                        <?php } ?>


                                    </center></div>
                            </div><!-- END card-footer-->
                        </div><!-- END card-->
                    </div>
                    <div class="col-xl-3">
                    </div>
                </div>

                <div class="row mt-4" id="flowstep" data-flag="0" style="text-align: center; display: none;">
                    <div class="col-xl-8" style="margin:0 auto;">
                        <div class="card card-default">
                            <div class="card-footer"><div class="card-title">Select Role</div></div>
                            <div class="card-body">
                                <!--                                <div style="display: inline-flex;">-->
                                <div class="workflow-img">
                                    <?php
                                    if ($data['role'] == "") {
                                        
                                    } else {
                                        foreach ($data['role']->result() as $row) {
                                            if ($row->role_description !== 'Admin') {
                                                ?>
                                                <div class="col-xl-2"><img class="img-thumbnail circle img-fluid thumb64" src="<?php echo base_url(); ?>img/user/06.jpg" alt="Image">
                                                    <p><a href="#" onclick="getrole('<?php echo $row->id ?>', '<?php echo $row->role_level ?>', '<?php echo $row->role_description ?>');"><?php echo $row->role_description ?></a></p>
                                                </div>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <!-- START card footer--><div class="card-footer"></div><!-- END card-footer--></div>
                    </div>
                </div>



            </div>
        </div>

    </div>

</div>

<script type="text/javascript">
    var divid = 0;

    var confirm_msg = '<?php echo $confirm_msg; ?>';//if (confirm("Do You Want To "+msg+" This Record?")) {
    var rolearray = [];
    function addstep()
    {
        if ($("#flowstep").attr('data-flag') == '0')
        {
            $("#flowstep").show(1000);
            $("#addbtn").val('Less step');
            $("#flowstep").attr('data-flag', '1');
        } else
        {
            $("#flowstep").hide(1000);
            $("#addbtn").val('Add step');
            $("#flowstep").attr('data-flag', '0');
        }

    }

    function getrole(id, level, role)
    {
        debugger;
        level = parseInt(level);
        var rolematch = 0;
        var rolehigherchk = 0;
        var QA_QC_poss_chk = 0;
        // if (id == 3 || id == 5)
        // {
        //     if (rolearray.length < 2)
        //     {
        //         alert('QA or QC should be defined only at-least 3rd Level in the approval sequence');
        //         QA_QC_poss_chk = 1;
        //         return false;
        //     } else
        //     {
        //         QA_QC_poss_chk = 0;
        //     }
        // }
        //Check dulicate role
        $.each(rolearray, function (i, val) {
            if (id == val["id"])
            {
                alert('Same role can not be took twice. ');
                rolematch = 1;
                return false;
            } else
            {

                rolematch = 0;
            }
            if (level <= val["level"])
            {
                alert('Next level in the approval sequence should be higher than previous one.');
                rolehigherchk = 1;
                return false;
            } else
            {

                rolehigherchk = 0;
            }
        });
        if (rolematch == 0 && rolehigherchk == 0 && QA_QC_poss_chk == 0)
        {
            rolearray.push({"id": id, "level": level, "role": role});
            cols = "";
            cols += "<div class='row' id='stepdiv" + id + "' style='display:none;'><input type='hidden' name='role' id='role' value='" + id + "' data-id='" + id + "' data-level='" + level + "' data-role='" + role + "' /><div class='col-xl-3'></div><div class='col-xl-6'><div class='card' style='background-color: transparent;border-color:transparent;margin-bottom:0rem;'><div class='card-body bg-info'><div class='d-flex align-items-center'><div class='text-center'><img class='img-thumbnail circle img-fluid thumb64' src='<?php echo base_url(); ?>img/user/06.jpg' alt='Image'></div><div class='text-center'><h3 class='m-0'>" + role + "</h3></div><div class='ml-auto align-self-start mt-3'><a name='role' value='" + id + "' data-level='" + level + "' data-role='" + role + "' class='btn btn-danger' href='#' onclick='getremove(" + id + ");'><em class='mr-2 fas fa-trash'></em><span>Delete</span></a></div></div></div><center><em class='fa-2x mt-2  fas fa-arrow-down'></em></center></div></div><div class='col-xl-3'></div></div>";
            $("#steps").append(cols);
            $("#stepdiv" + id).show(1000);
            $("#flowstep").hide(1000);
            $("#addbtn").val('Add step');
            $("#flowstep").attr('data-flag', '0');
        }
    }
    function getremove(obj)
    {
        debugger;
        $('#stepdiv' + obj).remove();
        rolearray = [];
        $("#steps").find('input[name^="role"]').each(function () {
            var id = $(this).val();
            var level = $(this).attr("data-level");
            var role = $(this).attr("data-role");
            rolearray.push({"id": id, "level": level, "role": role});
        });
    }

    function addworkflow()
    {
        if ('<?php echo $this->uri->segment(3) ?>' == 'editstep') {
            if ($('#remark').val() == '') {
                alert('Please provide Remark Message');
                return false;
            } else {
                var status = $("#active_status").val();
                var remark = $('#remark').val();
            }
        } else {
            var status = "1";
            var remark = "";
        }
//        debugger;
        var obj = 0;
        var act = $("#actid").val();
        var flowtype = $("#type").val();
        var myArray = [];
        $("#steps").find('input[name^="role"]').each(function () {
            obj++;
            var role = $(this).val();
            var roledesc = $(this).attr('data-role');
            myArray.push({
                role: role, roledesc: roledesc
            });
        });

        if (myArray.length > 1)
        {
            if (confirm("Do You Want To " + confirm_msg + " This Record?")) {
                try {
                    $.ajax({
                        url: "<?= base_url() ?>makeworkflow",
                        type: 'POST',
                        data: {flowtype: flowtype, act: act, myArray: myArray, status: status, remark: remark},
                        success: function (res) {
                            if (res == "-1")
                            {
                                alert("Please select at-least 2 level to create workflow...");
                            } else if (res == "-2")
                            {
                                alert("This workflow is already exists please delete the previous one to make it again, Thank you...!");
                                window.location.href = "<?= base_url() ?>workflow?module_id=" + '<?php echo $_GET['module_id'] ?>';
                            } else if (res > 0) {
                                alert("Workflow successfully " + confirm_msg + 'd...');
                                window.location.href = "<?= base_url() ?>workflow?module_id=" + '<?php echo $_GET['module_id'] ?>';
                            } else {
                                alert("Something went wrong...!");
                            }
                        }
                    });
                } catch (err) {
                    alert(err.message);
                }


            }
        } else
        {
            alert("Please select at-least 2 level to create workflow...");
        }
    }
    function cancilWorkflow() {
        window.location.href = "<?= base_url() ?>workflow?module_id=" + '<?php echo $_GET['module_id'] ?>';
    }

    $("#ev-click").click(function () {
        if (this.checked) {
            $("#active_status").val('1');
        }
        if (!this.checked) {
            $("#active_status").val('0');
        }
    });

    $(document).ready(function () {
        rolearray = [];
        $("#steps").find('input[name^="role"]').each(function () {
            var id = $(this).val();
            var level = $(this).attr("data-level");
            var role = $(this).attr("data-role");
            rolearray.push({"id": id, "level": level, "role": role});
        });
    });
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>