<div ng-app="vaccumApp" ng-controller="vaccumCtrl" ng-cloak>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>sf_home">Home</a></li>
            <li class="breadcrumb-item"><a href="#">{{room_code_show}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Vacuum Cleaner Logbook</li>
        </ol>
    </nav>
    <section class="contentBox blueBorder formWrapper">
            <div class="contentHeader">
                <div class="row">
                    <div class="col-12">
                        <h1>Header Details</h1>
                    </div>
                </div>
            </div>
            <div class="contentBody">
                <form>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <ul class="activityList">
                                    <li style="margin-left: 100px;"><a><b>Form No : </b>{{formno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Version No : </b>{{versionno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Effective Date : </b>{{effectivedate | date:'dd-MMM-yyyy'}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Retired Date : </b>{{retireddate | date:'dd-MMM-yyyy'}} </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    <section class="contentBox blueBorder formWrapper">


        <div class="contentHeader">

            <div class="row">
                <div class="col-12">
                    <h1>Vacuum Cleaner Logbook</h1>
                </div>
            </div>

        </div>

        <div class="contentBody">
            <div ng-show="vaccumCleanerData.length == 0 || edit == true">
                <form name="vaccumForm" novalidate>
                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Document No.</label>
                                <input type="text" class="form-control" value="{{docno}}" readonly="">
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Vacuum Cleaner ID</label>
                                <select class="chosen form-control" tabindex="4"  name="vaccum_code"  data-placeholder="Search Vacuum Cleaner" ng-options="dataObj['equipment_code'] as (dataObj.equipment_name +   ' => ' +   dataObj.equipment_code) for dataObj in vaccumCleanerList"  ng-model="vaccum_code" required chosen>
                                    <option value="">Select Vacuum Cleaner</option>
                                </select>
                            </div>
                        </div>


                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Batch No./LR No.</label>
                                <p ng-show="batchInProductData.length > 0"><b>{{batch_no}}</b></p>
                                <input ng-show="batchInProductData.length == 0" maxlength="10" class="form-control" type="text" placeholder="Batch No." name="batch_no" ng-model="batch_no" >
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Product Name/Material Name</label>
                                <div class="col-sm-12" ng-show="batchInProductData.length == 0">
                                    <select class="chzn-select form-control" tabindex="4"  name="product_code" ng-change="getProductDetail(product_no)" data-placeholder="Search Product No." ng-options="dataObj['product_code'] as dataObj.product_code for dataObj in productData"  ng-model="product_no" required chosen>
                                        <option value="">Select Product No</option>
                                    </select>
                                </div>
                                <div class="col-sm-12" >
                                    <p><b ng-show="batchInProductData.length > 0">{{product_no}}</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b ng-if="product_desc != ''">{{product_desc}}</b></p>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <div class="formBtnWrap">
                                <button ng-show="edit" class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancel</button>&nbsp;&nbsp;&nbsp;
                                <button ng-show="edit" ng-disabled="vaccumForm.$invalid" ng-click="setbtntext('update');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Update</button>&nbsp;&nbsp;&nbsp;
                                <button ng-show="!edit" ng-disabled="vaccumForm.$invalid" data-toggle="modal" ng-click="setbtntext('submit');" data-target="#loginModal" class="button smallBtn primaryBtn">Activity Start</button> &nbsp;&nbsp;&nbsp;&nbsp;
                                <button ng-show="!edit" disabled data-toggle="modal" ng-click="setbtntext('stop');" data-target="#loginModal" class="button smallBtn primaryBtn">Activity Stop</button> &nbsp;&nbsp;&nbsp;&nbsp;
                                <button  ng-show="!edit" ng-disabled="vaccumCleanerData.length == 0" data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('approval');" class="button smallBtn primaryBtn">Approve</button></div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
        <div ng-show="vaccumCleanerData.length > 0 && edit == false">
            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Document No.</label>
                        <input type="text" class="form-control" value="{{vaccumCleanerData[0].doc_no}}" readonly="">
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Vacuum Cleaner ID</label>
                        <input type="text" class="form-control" value="{{vaccumCleanerData[0].vaccum_cleaner_code}}" readonly="">

                    </div>
                </div>


            </div>


            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Batch No./LR No.</label>
                        <input type="text" class="form-control" value="{{vaccumCleanerData[0].batch_no}}" readonly="">
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Product Name/Material Name</label>
                        <input type="text" class="form-control" value="{{vaccumCleanerData[0].product_code}}" readonly="">

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-12 text-right">
                    <div class="formBtnWrap">
                        <button ng-click="editTrasaction()" class="button smallBtn primaryBtn" >Edit</button>&nbsp;&nbsp;&nbsp;
                        <button data-toggle="modal" ng-disabled="(vaccumCleanerData[0].activity_stop > 0)" ng-click="setbtntext('stop');" data-target="#loginModal" class="button smallBtn primaryBtn">Activity Stop</button> &nbsp;&nbsp;&nbsp;&nbsp;
                        <button  data-toggle="modal" ng-disabled="(vaccumCleanerData[0].activity_stop == '' || vaccumCleanerData[0].activity_stop == null)" data-target="#loginModal" ng-click="setbtntext('approval');" class="button smallBtn primaryBtn">Approve</button></div>

                </div>
            </div>
        </div>
        <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>

<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script type="text/javascript">
                            var app = angular.module("vaccumApp", ['angular.chosen', 'blockUI']);
                            app.directive('chosen', function ($timeout) {

                                var linker = function (scope, element, attr) {

                                    scope.$watch('vaccumCleanerList', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    scope.$watch('productData', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    $timeout(function () {
                                        element.chosen();
                                    }, 0, false);
                                };
                                return {
                                    restrict: 'A',
                                    link: linker
                                };
                            });
                            app.controller("vaccumCtrl", function ($scope, $http, $filter, blockUI) {
                                var urlParams = new URLSearchParams(window.location.search);
                                var myParam = urlParams.get('room_code');
                                var processid = urlParams.get('processid');
                                var actid = urlParams.get('actid');
                                var actname = urlParams.get('actname');

                                // get header detail 
                                $scope.headerRecordid = urlParams.get('headerid');
                                $scope.formno = urlParams.get('formno');
                                $scope.versionno = urlParams.get('versionno');
                                $scope.effectivedate = urlParams.get('effectivedate');
                                $scope.retireddate = urlParams.get('retireddate');
				$scope.docid = urlParams.get('doc_id') || '';;
                                //end

                                $scope.room_code_show = myParam;
                                $scope.roleList = [];
                                        $scope.getRoleList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.roleList = response.data.role_list;
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.getRoleList();
                                //Get Employee Role List
                                $scope.employeRole = [];
                                $scope.getEmployeRoleList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getEmployeRoleList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.employeRole = response.data.role_data;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getEmployeRoleList();
                                $scope.batchInProductData = [];
                                $scope.getRoomBatchInProductData = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getRoomBatchInProductData?room_code=' + myParam,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.batchInProductData = response.data.batch_product_list;
                                        if ($scope.batchInProductData.length > 0) {
                                            $scope.batch_no = $scope.batchInProductData[0].batch_no;
                                            $scope.product_no = $scope.batchInProductData[0].product_code;
                                            $scope.product_desc = $scope.batchInProductData[0].product_desc;
                                        }
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.getRoomBatchInProductData();
                                $scope.frequencyList = [];
                                var frequency_name = '';
                                $scope.vaccumCleanerList = [];
                                //Get Document Number code
                                $scope.docno = "";
                                // $scope.getdocno = function () {
                                //     $http({
                                //         url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getdocno',
                                //         method: "GET",
                                //         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                //     }).then(function (response) {
                                //         $scope.docno = response.data.docno;
                                //     }, function (error) { // optional

                                //         console.log("Something went wrong.Please try again");
                                //     });
                                // }
                                // $scope.getdocno();
                                //End of Get Document number function code
                                $scope.getVaccumCleanerList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetVaccumCleanerlist?room_code=' + myParam,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.vaccumCleanerList = response.data.vaccum_cleaner_list;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.getVaccumCleanerList();

                                $scope.productData = [];
                                $scope.getProductList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.productData = response.data.product_list;
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.getProductList();
                                $scope.product_desc = "";
                                $scope.getProductDetail = function (dataObj) {
                                    $scope.productcode = dataObj;
                                    $scope.product_desc = $filter('filter')($scope.productData, {product_code: dataObj})[0].product_name;
                                }


//*******************Bhupendra's code Started**********************//

                                $scope.setbtntext = function (btntext)
                                {
                                    $scope.btntext = btntext;
                                }
                                $scope.edit = false;
                                //Login Function
                                //Created by Bhupendra
                                //Date : 28/01/2020                                    
                                $scope.login = function () {
                                    var response = confirm("Do you really want to perform this task?");
                                    if (response == true)
                                    {
                                        $('.loader').show();
                                        pwd2 = SHA256($scope.password);
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                            method: "POST",
                                            data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&module_id=" + actid + "&module_type=log" + "&action=" + $scope.btntext+ "&auth=" + btoa(btoa($scope.password))+"&act_id="+actid,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            if (response.data.message == "")
                                            {
                                                var equipment_code = "";
                                                var selectedfilters = "";
                                                var temp = [];
                                                angular.forEach($scope.selectedFilter, function (obj, index) {
                                                    temp.push(obj.id);
                                                });
                                                var selectedfilters = temp.toString();
                                                if ($scope.batch_no == "" || $scope.batch_no == undefined)
                                                {
                                                    $scope.batch_no = "NA";
                                                }

                                                if ($scope.btntext == 'approval')
                                                {
                                                    //Calling Approval function
                                                    //Created by Bhupendra
                                                    //Date : 10/02/2020
                                                    $scope.getapproved(myParam, $scope.vaccumCleanerData[0].id, $scope.vaccumCleanerData[0].activity_id, $scope.vaccumCleanerData[0].workflowstatus, $scope.vaccumCleanerData[0].workflownextstep, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, 'stop', $scope.vaccumCleanerData[0].doc_no)
                                                } else if ($scope.btntext == 'stop') {
                                                    //Calling Stoped function
                                                    //Created by Bhupendra 
                                                    //Date : 29/01/2020
                                                    $scope.getstoped(myParam, $scope.vaccumCleanerData[0].id, actid, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark)
                                                } else if ($scope.btntext == 'submit')
                                                {
                                                    //Calling Stoped function
                                                    //Created by Bhupendra
                                                    //Date : 10/02/2020
                                                    $scope.getsubmit(myParam, processid, actid, actname, $scope.docno, $scope.vaccum_code, $scope.product_no, $scope.batch_no, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark)
                                                } else if ($scope.btntext == 'edit')
                                                {
//                                                        //edit Functionality
//                                                        //Created by Rahul
//                                                        //Date : 04/05/2020
//                                                    var userdata = response.data.userdata;
//                                                    if (userdata.role_id > $scope.vaccumCleanerData[0].done_by_role_id) {
//                                                        $scope.edit = true;
//                                                        $scope.docno = $scope.vaccumCleanerData[0].doc_no;
//                                                        $scope.vaccum_code = $scope.vaccumCleanerData[0].vaccum_cleaner_code;
//
//                                                        if ($scope.batchInProductData.length > 0) {
//                                                            $scope.batch_no = $scope.batchInProductData[0].batch_no;
//                                                            $scope.product_no = $scope.batchInProductData[0].product_code;
//                                                        } else {
//                                                            $scope.batch_no = $scope.vaccumCleanerData[0].batch_no;
//                                                            $scope.product_no = $scope.vaccumCleanerData[0].product_code;
//                                                        }
//                                                        $scope.getProductDetail($scope.product_no);
//                                                        $scope.is_in_workflow = $scope.vaccumCleanerData[0].is_in_workflow;
//                                                    } else {
//                                                        alert("You are not allowed to edit this.");
//                                                        $scope.edit = false;
//                                                    }
//
//                                                    $scope.resetLoginForm();
//                                                    angular.element("#btnclose").trigger('click');
                                                } else if ($scope.btntext == 'update')
                                                {
                                                    debugger;
                                                    //update Functionality
                                                    //Created by Rahul
                                                    //Date : 04/05/2020
                                                    var role = "";
                                                    if ($scope.vaccumCleanerData[0].activity_level.length > 0 && $scope.vaccumCleanerData[0].workflownextstep > 0 && $scope.vaccumCleanerData[0].start_time != null && $scope.vaccumCleanerData[0].stop_time == null) {
                                                        if ($scope.vaccumCleanerData[0].start_time != null && $scope.vaccumCleanerData[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }

                                                        var desc = $filter('filter')($scope.vaccumCleanerData[0].activity_level, {status_id: $scope.vaccumCleanerData[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    } else if ($scope.vaccumCleanerData[0].activity_level.length > 0 && $scope.vaccumCleanerData[0].workflownextstep == 0 && $scope.vaccumCleanerData[0].start_time != null && $scope.vaccumCleanerData[0].stop_time == null) {

                                                        var desc = $filter('filter')($scope.vaccumCleanerData[0].activity_level, {status_id: $scope.vaccumCleanerData[0].workflowstatus})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;

                                                    } else if ($scope.vaccumCleanerData[0].activity_level.length > 0 && $scope.vaccumCleanerData[0].workflownextstep > 0 && $scope.vaccumCleanerData[0].start_time != null && $scope.vaccumCleanerData[0].stop_time != null) {
                                                        if ($scope.vaccumCleanerData[0].start_time != null && $scope.vaccumCleanerData[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }
                                                        var desc = $filter('filter')($scope.vaccumCleanerData[0].activity_level, {status_id: $scope.vaccumCleanerData[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    }
//                                                  
                                                    var userdata = response.data.userdata;
                                                    if (userdata.role_id >= $scope.vaccumCleanerData[0].last_approval.role_id && userdata.role_id > 1) {
                                                        $scope.updatesubmit(myParam, processid, actid, actname, $scope.docno, $scope.vaccum_code, $scope.product_no, $scope.batch_no, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.vaccumCleanerData[0].edit_id, $scope.is_in_workflow);
                                                    } else {
                                                        $('.loader').hide();
                                                        alert("You are not allowed to edit this.");
                                                        $scope.edit = false;
                                                    }

                                                }
                                            } else {
                                                $('.loader').hide();
                                                alert(response.data.message);
                                            }
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                }
                                //End Login Function

                                //Approval function :- To Approve running activity by auth & privilege chk as well as sent that activity to next approval which exists in workflow
                                //Created by Bhupendra
                                //Date : 28/01/2020
                                $scope.getapproved = function (roomcode, actlogtblid, actid, workflowstatus, workflownextstep, roleid, empid, empname, email, remark, actstatus, docno) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/com_approval_fun',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&workflowstatus=" + workflowstatus + "&workflownextstep=" + workflownextstep + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&actstatus=" + actstatus + "&docno=" + docno + "&tblname=pts_trn_vaccum_cleaner",

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        if (response.data.next_step == "-1")
                                        {
                                            window.location.href = "<?php echo base_url() ?>home";
                                        } else
                                        {
                                        $('.loader').hide();
                                        location.reload();
                                        $scope.resetLoginForm();
                                        angular.element("#btnclose").trigger('click');
                                        $scope.product_desc = "";
                                        $scope.getLastFilterActivity();
                                        //$scope.getdocno();
                                        }
                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }
                                //***************************************************************************************
                                //Stoped function :- To Stop running activity by auth chk.
                                //Created by Bhupendra 
                                //Date : 28/01/2020                                    
                                $scope.getstoped = function (roomcode, actlogtblid, actid, roleid, empid, empname, email, remark) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getvaccumcleanerstopped',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        console.log(response); 
                                        $('.loader').hide();
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        angular.element("#btnclose").trigger('click');
                                        $scope.getLastFilterActivity();
                                        //$scope.getdocno();

                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }
                                //*********************************************************************************
                                //Start function :- To Start new activity by auth chk.
                                //Created by Bhupendra
                                //Date : 4/02/2020
                                $scope.getsubmit = function (roomcode, processid, actid, actname, docno, vaccum_code, product_no, batch_no, roleid, empid, empname, email, remark) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/submit_vaccum_cleaner_record',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&vaccum_code=" + vaccum_code + "&product_no=" + product_no + "&batch_no=" + batch_no + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&headerRecordid=" + $scope.headerRecordid,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $('.loader').hide();
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        $scope.vaccum_code = "";
                                        $scope.product_no = "";
                                        $scope.batch_no = "";
                                        angular.element("#btnclose").trigger('click');
                                        $scope.getLastFilterActivity();
                                        //$scope.getdocno();

                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }
                                //*********************************************************************************************

                                //*********************************************************************************
                                //Start function :- To Update started Activity.
                                //Created by Raul Chauhan
                                //Date : 05/05/2020
                                $scope.updatesubmit = function (roomcode, processid, actid, actname, docno, vaccum_code, product_no, batch_no, roleid, empid, empname, email, remark, update_id, is_in_workflow) {



                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/update_vaccum_cleaner_record',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&vaccum_code=" + vaccum_code + "&product_no=" + product_no + "&batch_no=" + batch_no + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&update_id=" + update_id + "&is_in_workflow=" + is_in_workflow,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $('.loader').hide();
                                        alert(response.data.message);
                                        $scope.edit = false;
                                        angular.element("#btnclose").trigger('click');
                                        $scope.resetLoginForm();
                                        $scope.getLastFilterActivity();
                                        //$scope.getdocno();

                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }
                                //*********************************************************************************************


                                $scope.CancilUpdate = function () {
                                    $scope.edit = false;
                                }
                                //Get Last unapproved Room Activity
                                $scope.vaccumCleanerData = [];
                                $scope.getLastFilterActivity = function () {
                                    $scope.vaccumCleanerData = [];
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetLastFilterActivity?room_code=' + myParam + "&table_name=pts_trn_vaccum_cleaner&doc_no="+$scope.docid,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {

                                        $scope.vaccumCleanerData = response.data.row_data;
                                        //$scope.getInprogressActivityList(processid)
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getLastFilterActivity();

                                //*******************Rahul's code Started**********************//
                                $scope.editTrasaction = function () {
                                    if (confirm("Do you really want to edit ?"))
                                    {
                                        $scope.edit = true;
                                        $scope.docno = $scope.vaccumCleanerData[0].doc_no;
                                        $scope.vaccum_code = $scope.vaccumCleanerData[0].vaccum_cleaner_code;

                                        if ($scope.batchInProductData.length > 0) {
                                            $scope.batch_no = $scope.batchInProductData[0].batch_no;
                                            $scope.product_no = $scope.batchInProductData[0].product_code;
                                        } else {
                                            $scope.batch_no = $scope.vaccumCleanerData[0].batch_no;
                                            $scope.product_no = $scope.vaccumCleanerData[0].product_code;
                                        }
                                        $scope.getProductDetail($scope.product_no);
                                        $scope.is_in_workflow = $scope.vaccumCleanerData[0].is_in_workflow;
                                    }
                                }

                                //*******************End Rahul's code***************************//
                                //Reset Login Form
                                $scope.resetLoginForm = function () {
                                    $scope.username = "";
                                    $scope.password = "";
                                    $scope.remark = "";
                                }
                            });
</script>