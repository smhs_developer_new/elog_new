<style>
    .active {
    background-color: rgb(34,139,34) !important;
    color: white;
    border-color: rgb(34,139,34) !important;
}
</style>
<!-- quick link section start-->
<div ng-controller="homeCtrl" ng-app="homeApp" ng-cloak>
    <section class="contentBox paddact redBorder">
        <div class="contentHeader">

            <div class="row">

                <div class="col-8">
                    <h1>Quick Links</h1>
                </div>

                <!--                <div class="col-4 text-right">
                                    <button ng-click="viewAllBlocks()" class="button smallBtn primaryBtn">
                                        View All
                                    </button>
                                </div>-->

            </div>

        </div>

        <div class="contentBody">

            <ul class="linkListWrap">
                <li class="icon qaIcon">
                    <a href="<?php echo base_url() ?>pending_approval">
                        <div class="linkText">QA Verification</div>
                        <div class="logButton">
                            Log Activity
                        </div>
                    </a>
                </li>

                <li class="icon inprogressIcon">
                    <a href="<?php echo base_url() ?>inprogress_activity">
                        <div class="linkText">In Progress</div>
                        <div class="logButton">
                            Log Activity
                        </div>
                    </a>
                </li>
                <li ng-repeat="roomObj in roomData" ng-click="getProcessLogActivity(roomObj, $index)"  class="icon">
                    <a href="#">
                        <div class="linkText">{{roomObj.room_code}}</div>
                        <div class="logButton {{current == $index?'active':'' }}">
                            {{roomObj.room_name}}
                        </div>
                    </a>
                </li>

            </ul>

            <ul class="linkListWrap" >
                <li class="icon ipqaIcon" ng-if="activityList.length > 0">
                    <a href="#" data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('roomin');" >
                        <div class="linkText">Room In</div>
                        <div class="logButton">
                            Log Activity
                        </div>
                    </a>
                </li>
                <li class="icon ipqaIcon" ng-if="activityList.length > 0">
                    <a href="#" data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('roomout');">
                        <div class="linkText">Room Out</div>
                        <div class="logButton">
                            Log Activity
                        </div>
                    </a>
                </li>
                <li class="icon ipqaIcon" ng-if="activityList.length > 0 && room_type != 'IPQA'">
                    <a href="#" data-toggle="modal" data-target="#batchinModal" ng-click="setbtntext('batchin');" >
                        <div class="linkText">Batch In</div>
                        <div class="logButton">
                            Log Activity
                        </div>
                    </a>
                </li>
                <li class="icon ipqaIcon" ng-if="activityList.length > 0 && room_type != 'IPQA'">
                    <a href="#" data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('batchout');">
                        <div class="linkText">Batch Out</div>
                        <div class="logButton">
                            Log Activity
                        </div>
                    </a>
                </li>



            </ul>
        </div>


    </section>
    <!-- quick link section end-->
    <section class="contentBox paddact redBorder new-sectionbox" ng-if="activityList.length > 0">
        <div class="contentHeader">
            <div class="row">
                <div class="col-md-12">
                        <ul class="menuItemsWrap" ng-if="activityList.length > 0">
                        <li class="menuItems" ng-repeat="activityObj in activityList">
                            <a href="<?php echo base_url() ?>{{activityObj.activity_url}}?room_id={{activeRoomData.room_id}}&room_code={{activeRoomData.room_code}}&actname={{activityObj.activity_name}}&processid={{activityObj.mst_act_id}}&actid={{activityObj.mst_act_id}}&room_type={{room_type}}">
                                <div class="menuImg">
                                    <img src="<?php echo base_url() ?>assets/images/log1img.png" alt="log1img">
                                </div>
                                <div class="menuTxt">
                                    {{activityObj.activity_name}}
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="contentBox blueBorder pl0">
        <div class="row rooming-table padd0">
            <div class="col-sm-6">
                <!-- ngIf: roomInUserData.length > 0 --><div class="container ng-scope" ng-if="roomInUserData.length > 0">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-lg-12 text-center"><h4>Room In User Details</h4></div>
                            <table class="table runningacttable2">
                                <thead>
                                    <tr border="1">
                                        <th>User Name</th>
                                        <th>Room</th>
                                        <th>Role</th>
                                        <th>Room in Date</th>
                                        <th>Room in Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="dataObj in roomInUserData">
                                        <td>{{dataObj.emp_name}}</td>
                                        <td>{{dataObj.room_code}}</td>
                                        <td>{{dataObj.role_description}}</td>
                                        <td>{{dataObj.RoominDate}}</td>
                                        <td>{{dataObj.RoominTime}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- end ngIf: roomInUserData.length > 0 -->
            </div>
            <div class="col-sm-6 plr0">
                <!-- ngIf: runningBatchData.length > 0 --><div class="container ng-scope" ng-if="runningBatchData.length > 0">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-lg-12 text-center"><h4>Running Batch Details</h4></div>
                            <table class="table runningacttable2">
                                <thead>
                                    <tr border="1">
                                        <th>User Name</th>
                                        <th>Room</th>
                                        <th>Running Batch</th>
                                        <th>Product</th>
                                        <th>Batch Start Date</th>
                                        <th>Batch Start Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="dataObj in runningBatchData" class="ng-scope">
                                        <td>{{dataObj.emp_name}}
                                        <td>{{dataObj.room_code}}</td>
                                        <td>{{dataObj.batch_no}}</td>
                                        <td>{{dataObj.product_code}}</td>
                                        <td>{{dataObj.BatchDate}}</td>
                                        <td>{{dataObj.BatchTime}}</td>
                                    </tr><!-- end ngRepeat: dataObj in runningBatchData -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- end ngIf: runningBatchData.length > 0 -->
            </div>
        </div>
    </div>

    <!-- activity section start-->
    <section class="contentBox blueBorder activityinprogress" ng-show="inProgressActivityData.length > 0">
        <div class="contentHeader">
            <div class="row">
                <div class="col-12">
                    <h1>Activity in progress</h1>
                </div>
            </div>

        </div>

        <div class="contentBody">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Doc Id</th>
                            <th>Room Code</th>
                            <th>Equipment Code</th>
                            <th>Start Date</th>
                            <th>Start Time</th>
                            <th>Started By</th>
                            <th>Current Activity</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr ng-repeat="dataObj in inProgressActivityData">
                            <td>{{dataObj.doc_id}}</td>
                            <td>{{dataObj.room_code}}</td>
                            <td>{{dataObj.equip_code}}</td>
                            <td>{{dataObj.start_date}}</td><td> {{dataObj.start_time}}</td>
                            <td>{{dataObj.user_name}}</td>
                            <td>{{dataObj.activity_name}} - {{dataObj.activity_remarks}}</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- activity section end-->
    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
    <?php $this->load->view("template/ponta_sahib/batchin_modal"); ?>

    <!--Mega menu-->
    <div class="modal fade megaMenu" id="activity_list_modal"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true">
        <div class="modal-dialog modal-dialog-slideout modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{show_room_code}} - <span>{{show_room_name}}</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="menuItemsWrap" ng-if="activityList.length > 0">
                        <li class="menuItems" ng-repeat="activityObj in activityList">
                            <a href="<?php echo base_url() ?>{{activityObj.activity_url}}?room_id={{activeRoomData.room_id}}&room_code={{activeRoomData.room_code}}&actname={{activityObj.activity_name}}&processid={{activityObj.mst_act_id}}&actid={{activityObj.mst_act_id}}&room_type={{room_type}}">
                                <div class="menuImg">
                                    <img src="<?php echo base_url() ?>assets/images/log1img.png" alt="log1img">
                                </div>
                                <div class="menuTxt">
                                    {{activityObj.activity_name}}
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <!--Mega menu end-->
</div>
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/toaster.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="https://cdn.rawgit.com/McNull/angular-block-ui/v0.1.1/dist/angular-block-ui.min.js"></script>
<script>
                            var app = angular.module("homeApp", ['toaster', 'blockUI', 'angular.chosen']);
                            app.directive('chosen', function ($timeout) {

                                var linker = function (scope, element, attr) {

                                    scope.$watch('productData', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    $timeout(function () {
                                        element.chosen();
                                    }, 0, false);
                                };
                                return {
                                    restrict: 'A',
                                    link: linker
                                };
                            });
                            app.controller("homeCtrl", function ($scope, $http, $filter, toaster, blockUI, $timeout) {
                                //$scope.device_id = "59.144.179.216";
                                //$scope.device_id = "103.211.15.1";

                                $scope.showAllBlocks = false;
                                $scope.roomData = [];
                                $scope.getRoomList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist?system_id=' + $scope.device_id,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.roomData = response.data.room_list;
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getRoomList();
                                $scope.getInprogressActivityList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetInprogressActivityList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.inProgressActivityData = response.data.in_progress_activity;
                                        console.log($scope.inProgressActivityData);
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getInprogressActivityList();
                                $scope.current = '-1';
                                $scope.activityList = [];
                                $scope.activeRoomData = {};
                                $scope.show_room_name = '';
                                $scope.show_room_code = '';
                                $scope.getProcessLogActivity = function (dataObj, index) {
                                    //$("#activity_list_modal").modal('show');
                                    $scope.activeRoomData = dataObj;
                                    $scope.room_type = dataObj.room_type;
                                    $scope.show_room_name = dataObj.room_name;
                                    $scope.show_room_code = dataObj.room_code;
                                    $scope.current = index
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getactivitylistforroom?room_id=' + dataObj.room_id,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.activityList = response.data.activity_list;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.selectedroomData = {};
                                $scope.getroomname = function (dataObj, index) {
                                    $scope.selectedroomData = dataObj;
                                    $scope.current = index
                                }

                                $scope.btntext = '';
                                $scope.setbtntext = function (btntext)
                                {
                                    $scope.btntext = btntext;
                                }

                                $scope.batchlogin = function (uid, pwd)
                                {

                                    $scope.username = uid;
                                    $scope.password = pwd;
                                    $scope.login();
                                }
                                //Login Function
                                //Created by Bhupendra
                                //Date : 26/02/2020
                                $scope.login = function () {
                                    var response = confirm("Do You Really Want To Do This Task");
                                    if (response == true)
                                    {
                                        pwd2 = SHA256($scope.password);
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                            method: "POST",
                                            data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&action=" + $scope.btntext,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            console.log(response);
                                            if (response.data.message == "")
                                            {
                                                if ($scope.btntext == 'roomin')
                                                {
                                                    $scope.getroomin($scope.activeRoomData.room_code, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2)
                                                } else if ($scope.btntext == 'roomout')
                                                {
                                                    $scope.getroomout($scope.activeRoomData.room_code, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2)
                                                } else if ($scope.btntext == 'batchin')
                                                {

                                                    $scope.getbatchin($scope.activeRoomData.room_code, $scope.batchcod, $scope.prodcode, $scope.product_desc, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2)
                                                } else if ($scope.btntext == 'batchout')
                                                {
                                                    $scope.getbatchout($scope.activeRoomData.room_code, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2)
                                                }

                                            } else {
                                                alert(response.data.message);
                                            }
                                        }, function (error) {
                                            console.log(error);
                                        });
                                    }
                                }
                                //End Login Function

                                //Room In function :- To Record the details of person who is room in
                                //Created by Bhupendra
                                //Date : 26/02/2020
                                $scope.isuserroomin = 0;
                                $scope.getroomin = function (roomcode, roleid, empid, empname, email) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getroomin',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        angular.element("#btnclose").trigger('click');
                                        $scope.isuserroomin = 1;
                                        $scope.getRoomInUserDetail();
                                        $scope.getrunningBatchData();
                                        //$scope.getProcessLogActivity($scope.selectedroomData,$scope.current);
                                    }, function (error) {
                                        console.log(error);
                                    });
                                }

                                //Room Out function :- To Inactive the person who was room in
                                //Created by Bhupendra
                                //Date : 26/02/2020

                                $scope.getroomout = function (roomcode, roleid, empid, empname, email) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getroomout',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        angular.element("#btnclose").trigger('click');
                                        $scope.getRoomInUserDetail();
                                        $scope.getrunningBatchData();
                                    }, function (error) {
                                        console.log(error);
                                    });
                                }

                                //Batch in function :- To start a batch in the selected room
                                //Created by Bhupendra
                                //Date : 28/02/2020

                                $scope.getbatchin = function (roomcode, batchcod, prodcode, product_desc, roleid, empid, empname, email) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getbatchin',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&batchcod=" + batchcod + "&prodcode=" + prodcode + "&product_desc=" + product_desc + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        angular.element("#batchbtnclose").trigger('click');
                                        $scope.getRoomInUserDetail();
                                        $scope.getrunningBatchData();
                                    }, function (error) {
                                        console.log(error);
                                    });
                                }

                                //Batch Out function :- To stop the running batch in the selected room
                                //Created by Bhupendra
                                //Date : 28/02/2020

                                $scope.getbatchout = function (roomcode, roleid, empid, empname, email) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getbatchout',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        angular.element("#btnclose").trigger('click');
                                        $scope.getRoomInUserDetail();
                                        $scope.getrunningBatchData();
                                    }, function (error) {
                                        console.log(error);
                                    });
                                }
                                //*********************************************************************************
                                $scope.roomInUserData = [];
                                $scope.getRoomInUserDetail = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoomInUserDetail',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.roomInUserData = response.data.roomin_user;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }

                                $scope.getRoomInUserDetail();

                                $scope.runningBatchData = [];
                                $scope.getrunningBatchData = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetrunningBatchData',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        console.log(response);
                                        $scope.runningBatchData = response.data.BatchDetail;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }

                                $scope.getrunningBatchData();

                                $scope.productData = [];
                                $scope.GetProductList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.productData = response.data.product_list;
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.GetProductList();
                                $scope.product_desc = "";
                                $scope.getProductDetail = function (dataObj) {
                                    $scope.product_desc = $filter('filter')($scope.productData, {product_code: dataObj})[0].product_name;
                                }


                                //Reset Login Form
                                $scope.resetLoginForm = function () {
                                    $scope.username = "";
                                    $scope.password = "";
                                    $scope.remark = "";
                                }

                                $scope.viewAllBlocks = function () {
                                    $scope.showAllBlocks = true;
                                }
                            });
</script>