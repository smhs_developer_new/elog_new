<div  ng-app="environApp" ng-controller="environCtrl" ng-cloak>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>sf_home">Home</a></li>
            <li class="breadcrumb-item"><a href="#">{{room_code_show}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Environmental Condition</li>
        </ol>
    </nav>
    <section class="contentBox blueBorder formWrapper">
        <div class="contentHeader">
            <div class="row">
                <div class="col-12">
                    <h1>Header Details</h1>
                </div>
            </div>
        </div>
        <div class="contentBody">
            <form>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <ul class="activityList">
                                <li><a><b>Form No : </b>{{formno}} </a></li>
                                <li><a><b>Version No : </b>{{versionno}} </a></li>
                                <li><a><b>Effective Date : </b>{{effectivedate | date:'dd-MMM-yyyy'}} </a></li>
                                <li><a><b>Retired Date : </b>{{retireddate | date:'dd-MMM-yyyy'}} </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <section class="contentBox blueBorder formWrapper">


        <div class="contentHeader">

            <div class="row">
                <div class="col-12">
                    <h1>Environmental Condition/Pressure Differential Record</h1>
                </div>
            </div>

        </div>

        <div class="contentBody" ng-show="ComDataArray.length == 0 || edit == true">
            <div id="header" ng-show="canedit == 1">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Document No.</label>
                            <input type="text" class="form-control" value="{{docno}} " readonly="">
                            <p>  </p>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Data Logger ID</label>
                            <select disabled class="form-control" name="logger_id" ng-model="logger_id" required>
                                <option ng-repeat="dataObj in loggerData" value="{{dataObj}}" selected>{{dataObj}}</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Magnehelic Gauge ID</label>
                            <select disabled class="form-control" name="gauge_id" ng-model="gauge_id" required>
                                <option ng-repeat="dataObj in gaugeData" value="{{dataObj}}" selected>{{dataObj}}</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12"><p><span class="text-danger">Define specification limit</span></p></div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Temperature <sup>o</sup>C</label>
                            <div class="input-group">
                                <select class="form-control" name="from_temp" ng-model="from_temp" ng-change="checkTemperature()">
                                    <option value="NLT">NLT</option>
                                    <option value="NMT">NMT</option>
                                </select>
                                <input class="form-control" type="text" only-digits placeholder="To" name="to_temp" ng-model="to_temp" ng-change="checkTemperature()">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Relative Humidity % RH</label>
                            <div class="input-group">
                                <select class="form-control" name="from_humidity" ng-model="from_humidity" ng-change="checkRelativeHumidity()" >
                                    <option value="NLT">NLT</option>
                                    <option value="NMT">NMT</option>
                                </select>
                                <input class="form-control" type="text" only-digits placeholder="To" name="to_humidity" ng-model="to_humidity" ng-change="checkRelativeHumidity()" >
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Pressure Differential Positive <input type="checkbox"  placeholder="From" style="margin-left: 210px;margin-top: -17px" name="is_positive" ng-model="is_positive"></label>

                            <br>
                            <label ng-show="!is_positive"><span class="asterisk">* </span>Pressure Differential</label>
                            <div class="input-group" ng-show="!is_positive">
                                <input class="form-control" only-digits type="text" ng-change="checkPD()" placeholder="From" name="from_pressure" ng-model="from_pressure">
                                <input class="form-control" only-digits type="text" ng-change="checkPD()" placeholder="To" name="to_pressure" ng-model="to_pressure">
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Pressure Differential Unit (+)</label>
                            <select class="form-control" name="pressure" ng-model="pressure">
                                <option value="">Select Pressure Differential</option>
                                <option value="PASCAL">PASCAL</option>
                                <option value="MM WC">MM WC</option>
                            </select>
                        </div>
                    </div>



                </div>
                <div class="row" ng-show="!showform">
                    <div class="col-sm-6 text-right">
                        <div>
                            <button ng-disabled="logger_id=='' || gauge_id=='' || to_temp == '' || to_humidity == '' || pressure == '' || (!is_positive && (from_pressure=='' || to_pressure==''))"  ng-click="saveMasterData()"  class="button smallBtn primaryBtn">Save</button></div>

                    </div>
                </div>
            </div>
            <!--************************************************************-->
            <div id="header" ng-show="canedit == 0">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Document No.</label>
                            <input type="text" class="form-control" value="{{docno}} " readonly="">
                            <p>  </p>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Data Logger ID</label>
                            <input type="text" class="form-control" name="Dlogid" value="{{HeaderDataArray[0].data_log_id}}" readonly="">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Magnehelic Gauge ID</label>
                            <input type="text" class="form-control" name="Magid" value="{{HeaderDataArray[0].magnelic_id}}" readonly="">
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12"><p><span class="text-danger">Define specification limit</span></p></div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label ><span class="asterisk">* </span>Temperature <sup>o</sup>C</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="tempmin" value="{{HeaderDataArray[0].temp_from}}" readonly="">
                                <input type="text" class="form-control" name="tempmax" value="{{HeaderDataArray[0].temp_to}}" readonly="">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Relative Humidity % RH</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="rhmin" value="{{HeaderDataArray[0].rh_from}}" readonly="">
                                <input type="text" class="form-control" name="rhmax" value="{{HeaderDataArray[0].rh_to}}" readonly="">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Pressure Differential Positive 
                                <input type="checkbox"  placeholder="From" style="margin-left: 210px;margin-top: -17px" disabled name="is_positive" ng-model="is_positive" readonly></label>

                            <br>
                            <label ng-show="!is_positive"><span class="asterisk">* </span>Pressure Differential</label>
                            <div class="input-group" ng-show="!is_positive">
                                <input type="text" class="form-control" name="frompressure" value="{{HeaderDataArray[0].from_pressure==''?'NA':HeaderDataArray[0].from_pressure}}" readonly="">
                                <input type="text" class="form-control" name="topressure" value="{{HeaderDataArray[0].to_pressure==''?'NA':HeaderDataArray[0].to_pressure}}" readonly="">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Pressure Differential Unit (+)</label>
                            <input type="text" class="form-control" name="globepressurediff" value="{{HeaderDataArray[0].globe_pressure_diff}}" readonly="">
                        </div>
                    </div>



                </div>
                <div class="row" ng-show="canedit == 0 && edit == false">
                    <div class="col-sm-6 text-right">
                        <div>
                            <button ng-click="EditMasterData()"  class="button smallBtn primaryBtn">Edit</button></div>

                    </div>
                </div>
            </div>
            <form name="tempForm" novalidate>
                <div ng-show="showform || edit == true">
                    <div class="row">
                        <div class="col-sm-12"><p><span class="text-danger">Record Observed Value</span></p></div>
                        <div class="col-sm-6">
                            <label><span class="asterisk">* </span>Pressure Differential(ZERO CHECK)</label>
                            <div class="switch-field col2">
                                <input type="radio" id="pressureOk" ng-click="checkZero(1)" ng-modal="pre_diff" name="pre_diff" value="ok" checked/>
                                <label for="pressureOk">Ok</label>
                                <input type="radio" id="pressureNotOk" ng-click="checkZero(0)" ng-modal="pre_diff" name="pre_diff" value="notok" />
                                <label for="pressureNotOk">Not Ok</label>
                            </div>
                        </div>

                        <div class="col-sm-6" ng-show="!is_positive">
                            <label><span class="asterisk">* </span>Pressure Differential Reading</label>
                            <input class="form-control" type="text" placeholder="Reading" ng-change="checkPD()" name="reading" ng-model="reading" required>
                            <span class="text-danger" ng-show="pd_message != ''">{{pd_message}}</span>
                        </div>
                        <div class="col-sm-6" ng-show="is_positive">
                            <label><span class="asterisk">* </span>Pressure Differential Reading</label>
                            <input class="form-control" type="text" placeholder="Reading" only-digits name="reading" ng-model="reading" required>
                        </div>
                    </div>



                    <div class="row" ng-show="to_temp != ''">
                        <div class="col-sm-6">
                            <label><span class="asterisk">* </span>Current Temperature <sup>o</sup>C</label>
                            <input class="form-control" type="text" only-digits placeholder="Temperature" name="temp" ng-model="temp" ng-change="checkTemperature()" required>
                            <span class="text-danger" ng-show="message != ''">{{message}}</span>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Temperature Range <sup>o</sup>C </label>
                                <div class="input-group">
                                    <input class="form-control" type="text" only-digits  name="min_temp" ng-model="min_temp" placeholder="Min">
                                    <input class="form-control" type="text" only-digits name="max_temp" ng-model="max_temp" placeholder="Max">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row" ng-show="to_humidity != ''">
                        <div class="col-sm-6">
                            <label><span class="asterisk">* </span>Current Relative Humidity % RH</label>
                            <input class="form-control" type="text" name="rh_value" placeholder="Enter %RH" ng-model="rh_value" ng-change="checkRelativeHumidity()" required >
                            <span class="text-danger" ng-show="rh_message != ''">{{rh_message}}</span>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label><span class="asterisk"> </span>Relative Humidity Range % RH</label>
                                <div class="input-group">
                                    <input class="form-control" type="text" only-digits name="min_rh" ng-model="min_rh" placeholder="Min" required>
                                    <input class="form-control" type="text" only-digits name="max_rh" ng-model="max_rh"  placeholder="Max" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <div class="formBtnWrap">
                                <button ng-show="edit" class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancel</button>&nbsp;&nbsp;&nbsp;
                                <button ng-show="edit" ng-click="setbtntext('update');" ng-disabled="tempForm.$invalid || message!='' || pd_message!='' || rh_message != ''" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Update</button>&nbsp;&nbsp;&nbsp;
                                <button ng-show="!edit" ng-disabled="(to_humidity != '' && rh_message != '') || (to_temp != '' && message != '') || (from_pressure != '' && to_pressure != '' && pd_message != '') || !zero_check" data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('submit');" class="button smallBtn primaryBtn">Done By</button> &nbsp;&nbsp;&nbsp;&nbsp;
                                <button ng-show="!edit" ng-disabled="ComDataArray.length == 0" ng-click="setbtntext('approval');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button></div>

                        </div>
                    </div>
                </div>




            </form>
        </div>
        <div class="contentBody" ng-if="ComDataArray.length > 0 && edit == false">

            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label>Document No.</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].doc_no}}" readonly="">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Data Logger ID</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].data_log_id}}" readonly="">
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Magnehelic Gauge ID</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].magnelic_id}}" readonly="">
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Temperature <sup>o</sup>C</label>
                        <div class="input-group">
                            <input type="text" class="form-control" value="{{(ComDataArray[0].temp_from=='')?'NA':ComDataArray[0].temp_from}}" readonly="">
                            <input type="text" class="form-control" value="{{(ComDataArray[0].temp_to=='')?'NA':ComDataArray[0].temp_to}}" readonly="">
                        </div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Relative Humidity % RH</label>
                        <div class="input-group">
                            <input type="text" class="form-control" value="{{(ComDataArray[0].rh_from=='')?'NA':ComDataArray[0].rh_from}}" readonly="">
                            <input type="text" class="form-control" value="{{(CComDataArray[0].rh_to=='')?'NA':ComDataArray[0].rh_to}}" readonly="">
                        </div>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label ng-show="ComDataArray[0].positive_check == '1'">Pressure Differential Positive
                            <input ng-show="ComDataArray[0].positive_check == '1'" type="checkbox" disabled  placeholder="From" style="margin-left: 210px;margin-top: -17px" name="is_positive" checked readonly>
                        </label>
                        <label ng-show="ComDataArray[0].positive_check == '0'">Pressure Differential</label>
                        <div class="input-group" ng-show="ComDataArray[0].positive_check == '0'">
                            <input type="text" class="form-control" value="{{(ComDataArray[0].from_pressure=='')?'NA':ComDataArray[0].from_pressure}}" readonly="">
                            <input type="text" class="form-control" value="{{(ComDataArray[0].to_pressure=='')?'NA':ComDataArray[0].to_pressure}}" readonly="">
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Pressure Differential Unit (+)</label>
                        <input type="text" class="form-control" value="{{(ComDataArray[0].globe_pressure_diff=='')?'NA':ComDataArray[0].globe_pressure_diff}}" readonly="">
                    </div>
                </div>                
            </div>



            <div class="row">
                <div class="col-sm-6">
                    <label>Pressure Differential(ZERO CHECK)</label>
                    <input type="text" class="form-control" value="{{(ComDataArray[0].pressure_diff=='')?'NA':ComDataArray[0].pressure_diff}}" readonly="">
                </div>

                <div class="col-sm-6">
                    <label>Reading</label>
                    <input type="text" class="form-control" value="{{(ComDataArray[0].reading=='')?'NA':ComDataArray[0].reading}}" readonly="">
                </div>
            </div>



            <div class="row">
                <div class="col-sm-6">
                    <label>Current Temperature</label>
                    <input type="text" class="form-control" value="{{(ComDataArray[0].temp=='')?'NA':ComDataArray[0].temp}}" readonly="">
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Temperature Range</label>
                        <div class="input-group">
                            <input type="text" class="form-control" value="{{(ComDataArray[0].temp_min=='')?'NA':ComDataArray[0].temp_min}}" readonly="">
                            <input type="text" class="form-control" value="{{(ComDataArray[0].temp_max=='')?'NA':ComDataArray[0].temp_max}}" readonly="">
                        </div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-6">
                    <label>Current Relative Humidity(%RH)</label>
                    <input type="text" class="form-control" value="{{(ComDataArray[0].rh=='')?'NA':ComDataArray[0].rh}}" readonly="">
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Relative Humidity Range</label>
                        <div class="input-group">
                            <input type="text" class="form-control" value="{{(ComDataArray[0].rh_min=='')?'NA':ComDataArray[0].rh_min}}" readonly="">
                            <input type="text" class="form-control" value="{{(ComDataArray[0].rh_max=='')?'NA':ComDataArray[0].rh_max}}" readonly="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" ng-show="fileupload == true;">
                <form id="fileUpload" name="fileUpload" enctype="multipart/form-data" method="post" action="#" style="float:left; width:100%;">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-2"><label>Select File:</label></div>
                            <div class="col-sm-6"><input id="file_id" type="file" (change)="onSelectFile($event)" accept="application/pdf" name="file" file-upload/></div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-9">
                                <button class="btn btn-sm btn-danger" ng-click="cancilFileUpload()" type="button">Cancel</button>&nbsp;&nbsp;
                                <button ng-disabled="files.length == 0" data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('upload');" class="btn btn-sm btn-success" type="button">Upload</button>
                                <!--<button ng-click="setbtntext('upload');" class="btn btn-sm btn-success" type="button">Upload</button>                                        </div>-->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row mb-3" ng-show="attachment_list.length > 0">
                <div class="col-sm-6">
                    <h1>Attachment List</h1>
                </div>
            </div>
            <div class="row mb-3" ng-show="attachment_list.length > 0">
                <div class="col-sm-7">
                    <table class="table">
                        <thead style="border-top:10px">
                            <tr>
                                <th>File Name </th>
                                <th>Role</th>
                                <th>Uploaded By</th>
                                <th>Uploaded On</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody >
                            <tr ng-repeat="dataObj in attachment_list">
                                <td>{{dataObj.file_name}}</td>
                                <td>{{dataObj.role_description}}</td>
                                <td>{{dataObj.uploaded_by}}</td>
                                <td>{{dataObj.uploaded_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td>
                                    <a href="<?php echo base_url() ?>assets/files/{{ComDataArray[0].act_id}}/{{dataObj.file_name}}" target="_blank" class="btn btn-sm btn-info" >View</a>&nbsp;&nbsp;
                                    <button data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('delete');removeFile(dataObj)" class="btn btn-sm btn-danger" type="button">Delete</button></td>
                            </tr>

                        </tbody>
                    </table>
                </div>

            </div>
            <div class="row mb-3">
                <div class="col-sm-12 text-right">
                    <div class="formBtnWrap">
                        <button ng-click="showFileUpload()" class="button smallBtn primaryBtn" >Attach File</button>&nbsp;&nbsp;&nbsp;
                        <button ng-click="EditTrasaction()" data-toggle="modal" data-target="#" class="button smallBtn primaryBtn" >Edit</button>&nbsp;&nbsp;&nbsp;
                        <button ng-click="setbtntext('approval');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button>

                    </div>
                </div>
            </div>


        </div>


    </section>
    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script type="text/javascript">
                            var app = angular.module("environApp", ['angular.chosen', 'blockUI']);
                            app.directive('onlyDigits', function () {
                                return {
                                    require: '?ngModel',
                                    link: function (scope, element, attrs, ngModelCtrl) {
                                        if (!ngModelCtrl) {
                                            return;
                                        }

                                        ngModelCtrl.$parsers.push(function (val) {
                                            if (angular.isUndefined(val)) {
                                                var val = '';
                                            }

                                            var clean = val.replace(/[^0-9\.]/g, '');
                                            var negativeCheck = clean.split('-');
                                            var decimalCheck = clean.split('.');
                                            if (!angular.isUndefined(negativeCheck[1])) {
                                                negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                                                clean = negativeCheck[0] + '-' + negativeCheck[1];
                                                if (negativeCheck[0].length > 0) {
                                                    clean = negativeCheck[0];
                                                }

                                            }

                                            if (!angular.isUndefined(decimalCheck[1])) {
                                                decimalCheck[1] = decimalCheck[1].slice(0, 1);
                                                clean = decimalCheck[0] + '.' + decimalCheck[1];
                                            }

                                            if (val !== clean) {
                                                ngModelCtrl.$setViewValue(clean);
                                                ngModelCtrl.$render();
                                            }
                                            return clean;
                                        });

                                        element.bind('keypress', function (event) {
                                            if (event.keyCode === 32) {
                                                event.preventDefault();
                                            }
                                        });
                                    }
                                };
                            });
                            
                            app.directive('fileUpload', function () {
                                return {
                                    scope: true, //create a new scope
                                    link: function (scope, el, attrs) {
                                        el.bind('change', function (event) {
                                            var files = event.target.files;
                                            //iterate files since 'multiple' may be specified on the element
                                            for (var i = 0; i < files.length; i++) {
                                                //emit event upward
                                                scope.$emit("fileSelected", {
                                                    file: files[i]
                                                });
                                                //               console.log(file);
                                            }
                                        });
                                    }
                                };
                            });
                            app.filter('format', function () {
                                return function (item) {
                                    var t = item.split(/[- :]/);
                                    var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                    var time = d.getTime();
                                    return time;
                                };
                            });
                            app.controller("environCtrl", function ($scope, $http, $filter, blockUI) {
                                var urlParams = new URLSearchParams(window.location.search);
                                var myParam = urlParams.get('room_code');
                                var processid = urlParams.get('processid');
                                var actid = urlParams.get('actid');
                                var actname = urlParams.get('actname');
                                var roleid = empid = empname = email ='';
                                // get header detail 
                                $scope.headerRecordid = urlParams.get('headerid');
                                $scope.formno = urlParams.get('formno');
                                $scope.versionno = urlParams.get('versionno');
                                $scope.effectivedate = urlParams.get('effectivedate');
                                $scope.retireddate = urlParams.get('retireddate');
                                //end

                                $scope.room_code_show = myParam;
                                $scope.loggerData = [];
                                $scope.gaugeData = [];
                                $scope.pressure_differential = "ok";
                                $scope.from_temp = 'NLT';
                                $scope.to_temp = '';
                                $scope.from_humidity = 'NLT';
                                $scope.to_humidity = '';
                                $scope.pressure = '';
                                $scope.from_pressure = '';
                                $scope.to_pressure = '';
                                $scope.roleList = [];
                                $scope.getRoleList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.roleList = response.data.role_list;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getRoleList();
                                $scope.getLoggerList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist?room_code=' + myParam,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        var data = response.data.room_list;
                                        $scope.loggerData = data[0].logger_id.split(",");
                                        $scope.gaugeData = data[0].gauge_id.split(",");
                                        $scope.logger_id=$scope.loggerData[0];
                                        $scope.gauge_id=$scope.gaugeData[0];
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                //Get Document Number code
                                $scope.docno = "";
                                // $scope.getdocno = function () {
                                //     $http({
                                //         url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getdocno',
                                //         method: "GET",
                                //         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                //     }).then(function (response) {
                                //         $scope.docno = response.data.docno;
                                //     }, function (error) { // optional

                                //         console.log("Something went wrong.Please try again");
                                //     });
                                // }
                                // $scope.getdocno();
                                //End of Get Document number function code
                                $scope.getLoggerList();
                                $scope.message = '';
                                $scope.checkTemperature = function () {
                                    if(!$scope.edit){
                                        if (isNaN($scope.from_temp) || isNaN($scope.to_temp)) {
                                            if ($scope.from_temp == 'NLT') {
                                                $scope.message = '';
                                            } else {
                                                $scope.message = "Please set NLT or NMT in from field";
                                            }
                                            if ($scope.from_temp == 'NLT' && $scope.to_temp != 'NMT') {
                                                if (parseInt($scope.temp) >= parseInt($scope.to_temp)) {
                                                    $scope.message = '';
                                                } else {
                                                    $scope.message = "Temperature is out of range";
                                                }
                                            }
                                            if ($scope.from_temp == 'NMT' && $scope.to_temp != 'NMT' && $scope.to_temp != 'NLT') {
                                                if (parseInt($scope.temp) <= parseInt($scope.to_temp)) {
                                                    $scope.message = '';
                                                } else {
                                                    $scope.message = "Temperature is out of range";
                                                }
                                            }
                                        } else {
                                            if (parseInt($scope.temp) >= parseInt($scope.from_temp) && parseInt($scope.temp) <= parseInt($scope.to_temp)) {
                                                $scope.message = '';
                                            } else {
                                                $scope.message = "Please set temerature between from and to range";
                                            }
                                        }
                                    }else{
                                        if ($scope.HeaderDataArray[0].temp_from == '' || $scope.HeaderDataArray[0].temp_from == 'NA') {
                                            $scope.message = "Please set NLT or NMT in from field";
                                        } else {
                                            $scope.message = "";
                                            if ($scope.HeaderDataArray[0].temp_from == 'NLT') {
                                                if (parseInt($scope.temp) >= parseInt($scope.HeaderDataArray[0].temp_to)) {
                                                    $scope.message = '';
                                                } else {
                                                    $scope.message = "Temperature is out of range";
                                                }
                                            }else{
                                                if (parseInt($scope.temp) <= parseInt($scope.HeaderDataArray[0].temp_to)) {
                                                    $scope.message = '';
                                                } else {
                                                    $scope.message = "Temperature is out of range";
                                                }
                                            }
                                        }

                                    }

                                }
                                $scope.checkdigitafterdecimal = function (val) {
                                    var val = val;
                                    var array = val.split(".");
                                    if (array.length > 1)
                                    {
                                        if (array[1].length > 1)
                                        {
                                            return false;
                                        }
                                    }
                                }
                                $scope.rh_message = '';
                                $scope.checkRelativeHumidity = function () {
                                    if(!$scope.edit){
                                        if (isNaN($scope.from_humidity) || isNaN($scope.to_humidity)) {
                                            if ($scope.from_humidity == 'NLT') {
                                                $scope.rh_message = '';
                                            } else {
                                                $scope.rh_message = "Please set NLT or NMT in from field";
                                            }
                                            if ($scope.from_humidity == 'NLT' && $scope.to_humidity != 'NMT') {
                                                if (parseInt($scope.rh_value) >= parseInt($scope.to_humidity)) {
                                                    $scope.rh_message = '';
                                                } else {
                                                    $scope.rh_message = "Relative Humidity is out of range";
                                                }
                                            }
                                            if ($scope.from_humidity == 'NMT' && $scope.to_humidity != 'NLT' && $scope.to_humidity != 'NMT') {
                                                if (parseInt($scope.rh_value) <= parseInt($scope.to_humidity)) {
                                                    $scope.rh_message = '';
                                                } else {
                                                    $scope.rh_message = "Relative Humidity is out of range";
                                                }
                                            }
                                        } else {
                                            if (parseInt($scope.rh_value) >= parseInt($scope.from_humidity) && parseInt($scope.rh_value) <= parseInt($scope.to_humidity)) {
                                                $scope.rh_message = '';
                                            } else {
                                                $scope.rh_message = "Please set RH between from and to range";
                                            }
                                        }
                                    }else{
                                        $scope.rh_message = "";
                                        if ($scope.HeaderDataArray[0].rh_from == 'NLT') {
                                            if (parseInt($scope.rh_value)>= parseInt($scope.HeaderDataArray[0].rh_to)) {
                                                $scope.rh_message = '';
                                            } else {
                                                $scope.rh_message = "Relative Humidity is out of range";
                                            }
                                        }else{
                                            if (parseInt($scope.rh_value) <= parseInt($scope.HeaderDataArray[0].rh_to)) {
                                                $scope.rh_message = '';
                                            } else {
                                                $scope.rh_message = "Relative Humidity is out of range";
                                            }
                                        }
                                    }
                                }
                                $scope.pd_message = '';
                                $scope.checkPD = function () {
                                    debugger;
                                    if (isNaN($scope.from_pressure) || isNaN($scope.to_pressure)) {
                                        if (parseInt($scope.reading) >= parseInt($scope.from_pressure) && parseInt($scope.reading) <= parseInt($scope.to_pressure)) {
                                            $scope.pd_message = '';
                                        } else {
                                            $scope.pd_message = "Please set PD between from and to range";
                                        }

                                    } else {
                                        if (parseInt($scope.reading) >= parseInt($scope.from_pressure) && parseInt($scope.reading) <= parseInt($scope.to_pressure)) {
                                            $scope.pd_message = '';
                                        } else {
                                            $scope.pd_message = "Please set PD between from and to range";
                                        }

                                    }

                                }
                                $scope.showform = false;
                                $scope.last_id = "";
                                $scope.canedit = 1;
                                $scope.zero_check = true;
                                $scope.checkZero = function (obj) {
                                    if (obj == 1) {
                                        $scope.zero_check = true;
                                    } else {
                                        $scope.zero_check = false;
                                    }
                                }
                                $scope.pre_diff = true;
                                $scope.is_positive = false;
                                $scope.HeaderDataArray = [];
                                //Save Master Data By Rahul Chauhan
                                $scope.saveMasterData = function () {
                                    $scope.canedit = 0;
                                    $scope.showform = true;
                                    $scope.HeaderDataArray[0] = {};
                                    $scope.HeaderDataArray[0].data_log_id = $scope.logger_id;
                                    $scope.HeaderDataArray[0].magnelic_id = $scope.gauge_id;
                                    $scope.HeaderDataArray[0].temp_from = $scope.from_temp;
                                    $scope.HeaderDataArray[0].temp_to = $scope.to_temp;
                                    $scope.HeaderDataArray[0].rh_from = $scope.from_humidity;
                                    $scope.HeaderDataArray[0].rh_to = $scope.to_humidity;
                                    $scope.HeaderDataArray[0].from_pressure = $scope.from_pressure;
                                    $scope.HeaderDataArray[0].to_pressure = $scope.to_pressure;
                                    $scope.HeaderDataArray[0].globe_pressure_diff = $scope.pressure;
                                    $scope.HeaderDataArray[0].is_positive = $scope.is_positive;
//                                    $http({
//                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/saveMasterData',
//                                        method: "POST",
//                                        data: "roomcode=" + myParam + "&logger_id=" + $scope.logger_id + "&gauge_id=" + $scope.gauge_id + "&pressure=" + $scope.pressure + "&from_temp=" + $scope.from_temp + "&to_temp=" + $scope.to_temp + "&from_humidity=" + $scope.from_humidity + "&to_humidity=" + $scope.to_humidity + '&from_pressure=' + $scope.from_pressure + "&to_pressure=" + $scope.to_pressure + "&docno=" + $scope.docno + "&is_positive=" + $scope.is_positive,
//
//                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
//                                    }).then(function (response) {
//                                        if (response.data.last_id > 0) {
//                                            $scope.showform = true;
//                                            $scope.last_id = response.data.last_id;
//                                            $scope.canedit = 0;
//                                            $scope.getLastHeaderData();
//                                        } else {
//                                            alert("Please try again");
//                                        }
//
//                                    }, function (error) {
//                                        console.log(error);
//                                    });
                                }

                                $scope.EditMasterData = function () {
                                    if (confirm("If you Edit header then you have to fill details again."))
                                    {
                                        $scope.ComDataArray = [];
                                        $scope.HeaderDataArray = [];
                                        $scope.canedit = 1;
                                        $scope.showform = false;
                                        $scope.showform = false;
                                        $scope.reading = "";
                                        $scope.temp = "";
                                        $scope.min_temp = "";
                                        $scope.max_temp = "";
                                        $scope.rh_value = "";
                                        $scope.min_rh = "";
                                        $scope.max_rh = "";
                                        $scope.message = '';
                                        $scope.rh_message = '';
                                        $scope.pd_message = '';
                                    }
                                }

//*******************Bhupendra's code Started**********************//
                                $scope.EditTrasaction = function () {
                                    if (confirm("Do you really want to edit ?"))
                                    {
                                        $scope.HeaderDataArray = [];
                                        $scope.canedit = 0;
                                        $scope.edit = true;
                                        if ($scope.ComDataArray[0].positive_check == '1') {
                                            $scope.is_positive = true;
                                        } else {
                                            $scope.is_positive = false;
                                        }
                                        $scope.docno = $scope.ComDataArray[0].doc_no;
                                        angular.forEach($scope.ComDataArray, function (value, key) {
                                            $scope.HeaderDataArray.push({
                                                data_log_id: value.data_log_id,
                                                magnelic_id: value.magnelic_id,
                                                temp_from: (value.temp_from == '') ? "NA" : value.temp_from,
                                                temp_to: (value.temp_to == '') ? "NA" : value.temp_to,
                                                rh_from: (value.rh_from == '') ? "NA" : value.rh_from,
                                                rh_to: (value.rh_to == '') ? "NA" : value.rh_to,
                                                from_pressure: (value.from_pressure == '') ? "NA" : value.from_pressure,
                                                to_pressure: (value.to_pressure == '') ? "NA" : value.to_pressure,
                                                globe_pressure_diff: (value.globe_pressure_diff == '') ? "NA" : value.globe_pressure_diff
                                            });
                                        });

                                        $scope.pre_diff = $scope.ComDataArray[0].pressure_diff;
                                        $scope.reading = $scope.ComDataArray[0].reading;
                                        $scope.temp = $scope.ComDataArray[0].temp;
                                        $scope.min_temp = $scope.ComDataArray[0].temp_min;
                                        $scope.max_temp = $scope.ComDataArray[0].temp_max;
                                        $scope.rh_value = $scope.ComDataArray[0].rh;
                                        $scope.min_rh = $scope.ComDataArray[0].rh_min;
                                        $scope.max_rh = $scope.ComDataArray[0].rh_max;
                                        $scope.from_pressure = $scope.ComDataArray[0].from_pressure;
                                        $scope.to_pressure = $scope.ComDataArray[0].to_pressure;
                                        $scope.to_temp = $scope.ComDataArray[0].temp_to;
                                        $scope.to_humidity = $scope.ComDataArray[0].rh_to;

                                    }
                                }

                                $scope.product_no = "NA";
                                $scope.batch_no = "NA";
                                $scope.setbtntext = function (btntext)
                                {
                                    $scope.btntext = btntext;
                                }
                                $scope.edit = false;

                                //Login Function
                                //Created by Bhupendra
                                //Date : 28/01/2020                                    
                                $scope.login = function () {
                                    var response = confirm("Do you really want to perform this task?");
                                    if (response == true)
                                    {
                                        $('.loader').show();
                                        pwd2 = SHA256($scope.password);
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                            method: "POST",
                                            data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&module_id=" + actid + "&module_type=log" + "&action=" + $scope.btntext + "&auth=" + btoa(btoa($scope.password))+"&act_id="+actid,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            if (response.data.message == ""){
                                                roleid = response.data.userdata.role_id;
                                                empid = response.data.userdata.id;
                                                empname = response.data.userdata.emp_name;
                                                email = response.data.userdata.email2;
                                                var equipment_code = "";
                                                var selectedfilters = "";
                                                var temp = [];
                                                angular.forEach($scope.selectedFilter, function (obj, index) {
                                                    temp.push(obj.id);
                                                });
                                                var selectedfilters = temp.toString();

                                                if ($scope.btntext == 'approval')
                                                {
                                                    //Calling Approval function
                                                    //Created by Bhupendra
                                                    //Date : 10/02/2020
                                                    $scope.getapproved(myParam, $scope.ComDataArray[0].id, $scope.ComDataArray[0].activity_id, $scope.ComDataArray[0].workflowstatus, $scope.ComDataArray[0].workflownextstep, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, 'stop', $scope.ComDataArray[0].doc_id);
                                                } else if ($scope.btntext == 'submit')
                                                {
                                                    //Calling Stoped function
                                                    //Created by Bhupendra
                                                    //Date : 10/02/2020
                                                    if ($scope.pre_diff) {
                                                        $scope.pressure_differential = "ok"
                                                    } else {
                                                        $scope.pressure_differential = "notok"
                                                    }
                                                    $scope.getsubmit(myParam, processid, actid, actname, $scope.docno, $scope.product_no, $scope.batch_no, $scope.logger_id, $scope.gauge_id, $scope.pressure_differential, $scope.reading, $scope.temp, $scope.min_temp, $scope.max_temp, $scope.rh_value, $scope.min_rh, $scope.max_rh, $scope.from_temp, $scope.to_temp, $scope.from_humidity, $scope.to_humidity, $scope.pressure, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.from_pressure, $scope.to_pressure);
                                                } else if ($scope.btntext == 'upload')
                                                {
                                                    //edit Functionality
                                                    //Created by Rahul
                                                    //Date : 04/05/2020
//                                                    var role = "";
                                                    if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                        if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }

                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep == 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {

                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflowstatus})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;

                                                    } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time != null) {
                                                        if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }
                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    }
//                                                  
                                                    var userdata = response.data.userdata;
                                                    if ((role != "" && userdata.role_id >= role) || (userdata.role_id >= $scope.ComDataArray[0].last_approval.role_id)) {
                                                        var myForm = document.getElementById('fileUpload');
                                                        var formData = new FormData(myForm);
                                                        formData.append('roomcode', myParam);
                                                        formData.append('act_id', $scope.ComDataArray[0].act_id);
                                                        formData.append('processid', $scope.ComDataArray[0].roomlogactivity);
                                                        formData.append('activity_id', $scope.ComDataArray[0].mst_act_id);
                                                        formData.append('roleid', response.data.userdata.role_id);
                                                        formData.append('empid', response.data.userdata.id);
                                                        formData.append('empname', response.data.userdata.emp_name);
                                                        formData.append('remark', $scope.remark);
                                                        angular.forEach($scope.files, function (value, key) {
                                                            var file = value.file;
                                                            formData.append('file[]', file);
                                                        });

                                                        $scope.attachLogFile(formData);
                                                    } else {
                                                        alert("You are not allowed to Upload the attachment.");
                                                        $scope.fileupload = false;
                                                        $scope.files = [];
                                                        $('.loader').hide();
                                                    }
                                                } else if ($scope.btntext == 'delete')
                                                {
                                                    //Delete Functionality
                                                    //Created by Rahul
                                                    //Date : 16/06/2020
                                                    var userdata = response.data.userdata;
                                                    if (userdata.role_id >= $scope.upload_by_role_id) {
                                                        $scope.deleteFile($scope.remove_id, response.data.userdata.id, $scope.ComDataArray[0].act_id, $scope.remove_file_name);
                                                    } else {
                                                        alert("You are not allowed to remove the attachment.");
                                                        $scope.fileupload = false;
                                                        $scope.files = [];
                                                        $('.loader').hide();
                                                    }
                                                } else if ($scope.btntext == 'update') {
                                                    //update Functionality
                                                    //Created by Bhupendra kumar
                                                    //Date : 31/05/2020
                                                    var role = "";
                                                    if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                        if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }

                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep == 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {

                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflowstatus})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;

                                                    } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time != null) {
                                                        if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }
                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    }
//                                                  
                                                    var userdata = response.data.userdata;
                                                    if (userdata.role_id >= $scope.ComDataArray[0].last_approval.role_id && userdata.role_id > 1) {
                                                        $scope.updatesubmit(myParam, processid, actid, actname, $scope.docno, $scope.pressure_differential, $scope.reading, $scope.temp, $scope.min_temp, $scope.max_temp, $scope.rh_value, $scope.min_rh, $scope.max_rh, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.ComDataArray[0].is_in_workflow, $scope.ComDataArray[0].edit_id);
                                                    } else {
                                                        alert("You are not allowed to edit this.")
                                                        $scope.edit = false;
                                                        $('.loader').hide();
                                                    }
                                                }
                                            } else {
                                                alert(response.data.message);
                                                $('.loader').hide();
                                            }
                                        }, function (error) {
                                            console.log(error);
                                            $('.loader').hide();
                                        });
                                    }
                                }
                                //End Login Function

                                //Approval function :- To Approve running activity by auth & privilege chk as well as sent that activity to next approval which exists in workflow
                                //Created by Bhupendra
                                //Date : 28/01/2020
                                $scope.getapproved = function (roomcode, actlogtblid, actid, workflowstatus, workflownextstep, roleid, empid, empname, email, remark, actstatus, docno) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/com_approval_fun',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&workflowstatus=" + workflowstatus + "&workflownextstep=" + workflownextstep + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&actstatus=" + actstatus + "&docno=" + docno + "&tblname=pts_trn_env_cond_diff",

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        if (response.data.next_step == "-1")
                                        {
                                            window.location.href = "<?php echo base_url() ?>home";
                                        } else
                                        {
                                            $scope.resetLoginForm();
                                            angular.element("#btnclose").trigger('click');
                                            $scope.getLastFilterActivity();
                                            $('.loader').hide();
                                            //$scope.getdocno();
                                        }
                                    }, function (error) {
                                        console.log(error);
                                        $('.loader').hide();
                                    });
                                }
                                //*********************************************************************************
                                //Start function :- To Start new activity by auth chk.
                                //Created by Bhupendra
                                //Date : 4/02/2020
                                $scope.hits =0;
                                $scope.getsubmit = function (roomcode, processid, actid, actname, docno, product_no, batch_no, logger_id, gauge_id, pressure_differential, reading, temp, min_temp, max_temp, rh_value, min_rh, max_rh, from_temp, to_temp, from_humidity, to_humidity, pressure, roleid, empid, empname, email, remark, from_pressure, to_pressure) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/submit_env_con_diff_record',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&product_no=" + product_no + "&batch_no=" + batch_no + "&logger_id=" + logger_id + "&gauge_id=" + gauge_id + "&pressure_differential=" + pressure_differential + "&reading=" + reading + "&temp=" + temp + "&min_temp=" + min_temp + "&max_temp=" + max_temp + "&rh_value=" + rh_value + "&min_rh=" + min_rh + "&max_rh=" + max_rh + "&from_temp=" + from_temp + "&to_temp=" + to_temp + "&from_humidity=" + from_humidity + "&to_humidity=" + to_humidity + "&pressure=" + pressure + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + '&from_pressure=' + from_pressure + "&to_pressure=" + to_pressure + "&headerRecordid=" + $scope.headerRecordid,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.hits = $scope.hits+1;
                                        if($scope.hits<'4'){
                                            if(response.data.message=='0'){
                                                $scope.getsubmit(myParam, processid, actid, actname, $scope.docno, $scope.product_no, $scope.batch_no, $scope.logger_id, $scope.gauge_id, $scope.pressure_differential, $scope.reading, $scope.temp, $scope.min_temp, $scope.max_temp, $scope.rh_value, $scope.min_rh, $scope.max_rh, $scope.from_temp, $scope.to_temp, $scope.from_humidity, $scope.to_humidity, $scope.pressure, roleid, empid, empname, email, $scope.remark, $scope.from_pressure, $scope.to_pressure)
                                            }else{
                                                alert(response.data.message);
                                                $scope.showform = false;
                                                $scope.canedit = 1;
                                                $scope.docno = "";
                                                $scope.logger_id = "";
                                                $scope.gauge_id = "";
                                                $scope.pressure_differential = "ok";
                                                $scope.reading = "";
                                                $scope.temp = "";
                                                $scope.min_temp = "";
                                                $scope.max_temp = "";
                                                $scope.rh_value = "";
                                                $scope.min_rh = "";
                                                $scope.max_rh = "";
                                                $scope.from_temp = "NLT";
                                                $scope.to_temp = "";
                                                $scope.from_humidity = "NLT";
                                                $scope.to_humidity = "";
                                                $scope.pressure = "";
                                                $scope.from_pressure = "";
                                                $scope.to_pressure = "";
                                                angular.element("#btnclose").trigger('click');
                                                $scope.resetLoginForm();
                                                $scope.getLastFilterActivity();
                                                $('.loader').hide();
                                                //$scope.getdocno();
                                            }
                                        }else{
                                            alert('3 Consecutive Hits Failed.Please Try Again');
                                            $('.loader').hide();
                                        }
                                    }, function (error) {
                                        console.log(error);
                                        $('.loader').hide();
                                    });
                                }


                                // Start function :- To update Acivity
                                // Created by Bhupendra kumar 
                                // Date : 30/05/2020

                                $scope.updatesubmit = function (roomcode, processid, actid, actname, docno, pressure_differential, reading, temp, min_temp, max_temp, rh_value, min_rh, max_rh, roleid, empid, empname, email, remark, is_in_workflow, update_id) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/update_env_con_diff_record',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&pressure_differential=" + pressure_differential + "&reading=" + reading + "&temp=" + temp + "&min_temp=" + min_temp + "&max_temp=" + max_temp + "&rh_value=" + rh_value + "&min_rh=" + min_rh + "&max_rh=" + max_rh + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&is_in_workflow=" + is_in_workflow + "&update_id=" + update_id,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.stopApprove = 1;
                                        $scope.edit = false;
                                        $scope.resetLoginForm();
                                        $scope.getLastFilterActivity();
                                        angular.element("#btnclose").trigger('click');
                                        $('.loader').hide();
                                    }, function (error) {
                                        console.log(error);
                                        $('.loader').hide();
                                    });
                                }
                                $scope.CancilUpdate = function () {
                                    $scope.edit = false;
                                }
//*********************************************************************************************

                                //Get Last unapproved Room Activity
                                $scope.ComDataArray = [];
                                $scope.getLastFilterActivity = function () {
                                    $scope.ComDataArray = [];
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetLastFilterActivity?room_code=' + myParam + "&table_name=pts_trn_env_cond_diff",
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.ComDataArray = response.data.row_data;
                                        if($scope.ComDataArray.length > 0){
                                        $scope.attachment_list = $scope.ComDataArray[0].attachment_list;
                                    }
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getLastFilterActivity();

                                $scope.HeaderDataArray = [];
                                $scope.getLastHeaderData = function () {
                                    $scope.HeaderDataArray = [];
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetHeaderData?room_code=' + myParam + "&table_name=pts_trn_env_cond_diff",
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.HeaderDataArray = response.data.row_data;
                                        if ($scope.HeaderDataArray[0].positive_check == 1) {
                                            $scope.is_positive = true;
                                        } else {
                                            $scope.is_positive = false;
                                        }
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                //Reset Login Form
                                $scope.resetLoginForm = function () {
                                    $scope.username = "";
                                    $scope.password = "";
                                    $scope.remark = "";
                                }

//*******************End Bhupendra's code***************************//

//File Attachment
                                $scope.fileupload = false;
                                $scope.showFileUpload = function () {
                                    $scope.fileupload = true;
                                }
                                $scope.cancilFileUpload = function () {
                                    $('#file_id').val('');
                                    $scope.files = [];
                                    $scope.fileupload = false;
                                }
                                $scope.files = [];
                                $scope.$on("fileSelected", function (event, args) {
                                    var item = args;
                                    var full_path = item.file['name'];
                                    item['file_ext'] = full_path.split(".")[1];

                                    $scope.files.push(item);

                                    var reader = new FileReader();

                                    reader.addEventListener("load", function () {
                                        $scope.$apply(function () {
                                            item.src = reader.result;
                                        });
                                    }, false);

                                    if (item.file) {
                                        reader.readAsDataURL(item.file);
                                    }
                                });
                                $scope.attachLogFile = function (form_data) {
                                    var request = {
                                        method: 'POST',
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/attachLogFile',
                                        data: form_data,
                                        headers: {
                                            'Content-Type': undefined
                                        }
                                    };

                                    // SEND THE FILES.
                                    $http(request).then(function (response) {
                                        alert(response.data.message);
                                        window.location.reload();
                                        $scope.resetLoginForm();
                                        $scope.fileupload = false;
                                        $scope.getLastFilterActivity();
                                        angular.element("#btnclose").trigger('click');
                                        $('.loader').hide();
                                    }, function (error) {
                                        console.log(error);
                                        $('.loader').hide();
                                    });

                                }
                                $scope.remove_id = "";
                                $scope.upload_by_role_id = "";
                                $scope.remove_file_name = "";
                                $scope.removeFile = function (obj) {
                                    $scope.remove_id = obj.id;
                                    $scope.upload_by_role_id = obj.role_id;
                                    $scope.remove_file_name = obj.file_name;
                                }

                                $scope.deleteFile = function (objId, user_id, act_id, file_name) {
                                    if (confirm("Do you really want to Remove This attachment ?"))
                                    {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/removeAttachment',
                                            method: "POST",
                                            data: "id=" + objId + "&user_id=" + user_id + "&act_id=" + act_id + "&file_name=" + file_name,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.remove_id = "";
                                            $scope.upload_by_role_id = "";
                                            $scope.remove_file_name = "";
                                            alert(response.data.message);
                                            $scope.resetLoginForm();
                                            $scope.getLastFilterActivity();
                                            angular.element("#btnclose").trigger('click');
                                            $('.loader').hide();
                                        }, function (error) {
                                            console.log(error);
                                            $('.loader').hide();
                                        });
                                    }
                                }

                            });

</script>
