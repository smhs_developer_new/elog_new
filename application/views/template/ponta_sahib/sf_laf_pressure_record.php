<div ng-app="lafApp" ng-controller="lafCtrl" ng-cloak>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>sf_home">Home</a></li>
            <li class="breadcrumb-item"><a href="#">{{room_code_show}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">LAF Pressure Differential Record</li>
        </ol>
    </nav>
    <section class="contentBox blueBorder formWrapper">
            <div class="contentHeader">
                <div class="row">
                    <div class="col-12">
                        <h1>Header Details</h1>
                    </div>
                </div>
            </div>
            <div class="contentBody">
                <form>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <ul class="activityList">
                                    <li style="margin-left: 100px;"><a><b>Form No : </b>{{formno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Version No : </b>{{versionno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Effective Date : </b>{{effectivedate | date:'dd-MMM-yyyy'}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Retired Date : </b>{{retireddate | date:'dd-MMM-yyyy'}} </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    <section class="contentBox blueBorder formWrapper">


        <div class="contentHeader">

            <div class="row">
                <div class="col-12">
                    <h1>LAF Pressure Differential Record</h1>
                </div>
            </div>

        </div>

        <div class="contentBody" ng-show="ComDataArray.length == 0 || edit == true">
            <form name="boothForm" novalidate>
                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Document No.</label>
                            <input type="text" class="form-control" value="{{docno}}" readonly="">
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Dispensing Booth No.</label>
                            <select class="chosen form-control" tabindex="4"  name="booth_code"  data-placeholder="Search Dispensing Booth No." ng-options="dataObj['equipment_code'] as (dataObj.equipment_name +    ' => ' +    dataObj.equipment_code) for dataObj in dispensingBoothData"  ng-model="booth_code" required chosen>
                                <option value="">Select Dispensing Booth No.</option>
                            </select>

                            <small class="form-text text-muted" style="display: none;">
                                Desmopressin Acetate Nasal solution, 0.01 (Nasal spray), 5ml/vial
                            </small>
                        </div>
                    </div>


                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span> M-I 5 to 15</label>
                            <input class="form-control" type="text" name="mi5_mi15" ng-model="mi5_mi15" required>   
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span> M-II 0 to -5</label>
                            <input class="form-control" type="text"  name="mi0_mi5" ng-model="mi0_mi5" required>  
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span> G-I 1.0 to 3</label>
                            <input class="form-control" type="text"  name="gi0_gi3" ng-model="gi0_gi3" required>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span> G-II 1.5 to 5</label>
                            <input class="form-control" type="text"  name="gi15_gi5" ng-model="gi15_gi5" required>  
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span> G-III 8 to 14</label>
                            <input class="form-control" type="text"  name="gi8_gi14" ng-model="gi8_gi14" required>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <button ng-show="edit" class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancel</button>&nbsp;&nbsp;&nbsp;
                            <button ng-show="edit" ng-click="setbtntext('update');" ng-disabled="boothForm.$invalid" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Update</button>&nbsp;&nbsp;&nbsp;
                            <button ng-show="!edit" ng-click="setbtntext('submit');" ng-disabled="boothForm.$invalid" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Done By</button> &nbsp;&nbsp;&nbsp;&nbsp;
                            <button ng-show="!edit" ng-click="setbtntext('approval');" disabled data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button></div>

                    </div>
                </div>


            </form>
        </div>
        <div class="contentBody" ng-if="ComDataArray.length > 0 && edit == false">

            <div class="row">

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Document No.</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].doc_no}}" readonly="">
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label><span class="asterisk">* </span>Dispensing Booth No.</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].booth_no}}" readonly="">
                    </div>
                </div>


            </div>


            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label><span class="asterisk">* </span> M-I 5 to 15</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].m1}}" readonly="">
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label><span class="asterisk">* </span> M-II 0 to -5</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].m2}}" readonly="">
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label><span class="asterisk">* </span> G-I 1.0 to 3</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].g1}}" readonly="">
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label><span class="asterisk">* </span> G-II 1.5 to 5</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].g2}}" readonly="">
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label><span class="asterisk">* </span> G-III 8 to 14</label>
                        <input type="text" class="form-control" value="{{ComDataArray[0].g3}}" readonly="">
                    </div>
                </div>
            </div>
            <div class="row" ng-show="fileupload == true;">
                <form id="fileUpload" name="fileUpload" enctype="multipart/form-data" method="post" action="#" style="float:left; width:100%;">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-2"><label>Select File:</label></div>
                            <div class="col-sm-6"><input id="file_id" type="file" (change)="onSelectFile($event)" accept="application/pdf" name="file" file-upload/></div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-9">
                                <button class="btn btn-sm btn-danger" ng-click="cancilFileUpload()" type="button">Cancel</button>&nbsp;&nbsp;
                                <button ng-disabled="files.length == 0" data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('upload');" class="btn btn-sm btn-success" type="button">Upload</button>
                                <!--<button ng-click="setbtntext('upload');" class="btn btn-sm btn-success" type="button">Upload</button>                                        </div>-->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="row mb-3" ng-show="attachment_list.length > 0">
                <div class="col-sm-6">
                    <h1>Attachment List</h1>
                </div>
            </div>
            <div class="row mb-3" ng-show="attachment_list.length > 0">
                <div class="col-sm-7">
                    <table class="table">
                        <thead style="border-top:10px">
                            <tr>
                                <th>File Name </th>
                                <th>Role</th>
                                <th>Uploaded By</th>
                                 <th>Uploaded On</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody >
                            <tr ng-repeat="dataObj in attachment_list">
                                <td>{{dataObj.file_name}}</td>
                                <td>{{dataObj.role_description}}</td>
                                <td>{{dataObj.uploaded_by}}</td>
                                <td>{{dataObj.uploaded_on | format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                <td>
                                    <a href="<?php echo base_url() ?>assets/files/{{ComDataArray[0].act_id}}/{{dataObj.file_name}}" target="_blank" class="btn btn-sm btn-info" >View</a>&nbsp;&nbsp;
                                    <button data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('delete');removeFile(dataObj)" class="btn btn-sm btn-danger" type="button">Delete</button></td>
                            </tr>

                        </tbody>
                    </table>
                </div>

            </div>

            <div class="row mb-3">
                <div class="col-sm-12 text-right">
                    <div class="formBtnWrap">
                        <button ng-click="showFileUpload()" class="button smallBtn primaryBtn" >Attach File</button>&nbsp;&nbsp;&nbsp;
                        <button ng-click="editTrasaction()" class="button smallBtn primaryBtn" >Edit</button>&nbsp;&nbsp;&nbsp;
                        <button ng-click="setbtntext('approval');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button></div>
                </div>
            </div>
        </div>

        <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>


<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script type="text/javascript">
                            var app = angular.module("lafApp", ['angular.chosen', 'blockUI']);
                            app.directive('chosen', function ($timeout) {

                                var linker = function (scope, element, attr) {

                                    scope.$watch('dispensingBoothData', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    $timeout(function () {
                                        element.chosen();
                                    }, 0, false);
                                };
                                return {
                                    restrict: 'A',
                                    link: linker
                                };
                            });
                            app.directive('fileUpload', function () {
                                return {
                                    scope: true, //create a new scope
                                    link: function (scope, el, attrs) {
                                        el.bind('change', function (event) {
                                            var files = event.target.files;
                                            //iterate files since 'multiple' may be specified on the element
                                            for (var i = 0; i < files.length; i++) {
                                                //emit event upward
                                                scope.$emit("fileSelected", {
                                                    file: files[i]
                                                });
                                                //               console.log(file);
                                            }
                                        });
                                    }
                                };
                            });
                             app.filter('format', function () {
                                    return function (item) {
                                        var t = item.split(/[- :]/);
                                        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                        var time = d.getTime();
                                        return time;
                                    };
                                });
                            app.controller("lafCtrl", function ($scope, $http, $filter, blockUI) {
                                var urlParams = new URLSearchParams(window.location.search);
                                var room_code = urlParams.get('room_code');
                                var processid = urlParams.get('processid');
                                var actid = urlParams.get('actid');
                                var actname = urlParams.get('actname');

                                // get header detail 
                                $scope.headerRecordid = urlParams.get('headerid');
                                $scope.formno = urlParams.get('formno');
                                $scope.versionno = urlParams.get('versionno');
                                $scope.effectivedate = urlParams.get('effectivedate');
                                $scope.retireddate = urlParams.get('retireddate');
                                //end

                                $scope.room_code_show = room_code;
                                $scope.roleList = [];
                                $scope.getRoleList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.roleList = response.data.role_list;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getRoleList();
                                $scope.dispensingBoothData = [];
                                $scope.getDispensingBooth = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetDispensingBoothList?room_code=' + room_code,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.dispensingBoothData = response.data.booth_list;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.getDispensingBooth();
                                //Get Document Number code
                                $scope.docno = "";
                                // $scope.getdocno = function () {
                                //     $http({
                                //         url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getdocno',
                                //         method: "GET",
                                //         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                //     }).then(function (response) {
                                //         $scope.docno = response.data.docno;
                                //     }, function (error) { // optional

                                //         console.log("Something went wrong.Please try again");
                                //     });
                                // }
                                // $scope.getdocno();
                                //End of Get Document number function code

                                //*******************Bhupendra's code Started**********************//

                                $scope.setbtntext = function (btntext)
                                {
                                    $scope.btntext = btntext;
                                }
                                $scope.edit = false;
                                //Login Function
                                //Created by Bhupendra
                                //Date : 28/01/2020                                    
                                $scope.login = function () {
                                    var response = confirm("Do you really want to perform this task?");
                                    if (response == true)
                                    {
                                        $('.loader').show();
                                        pwd2 = SHA256($scope.password);
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                            method: "POST",
                                            data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&module_id=" + actid + "&module_type=log" + "&action=" + $scope.btntext+ "&auth=" + btoa(btoa($scope.password))+"&act_id="+actid,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            if (response.data.message == "")
                                            {
                                                if ($scope.btntext == 'approval')
                                                {
                                                    //Calling Approval function
                                                    //Created by Bhupendra
                                                    //Date : 10/02/2020 
                                                    $scope.getapproved(room_code, $scope.ComDataArray[0].id, $scope.ComDataArray[0].activity_id, $scope.ComDataArray[0].workflowstatus, $scope.ComDataArray[0].workflownextstep, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, 'stop', $scope.ComDataArray[0].doc_id)
                                                } else if ($scope.btntext == 'submit')
                                                {
                                                    //Calling Stoped function
                                                    //Created by Bhupendra
                                                    //Date : 10/02/2020
                                                    $scope.getsubmit(room_code, processid, actid, actname, $scope.docno, $scope.booth_code, $scope.mi5_mi15, $scope.mi0_mi5, $scope.gi0_gi3, $scope.gi15_gi5, $scope.gi8_gi14, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark)
                                                } else if ($scope.btntext == 'upload')
                                                {
                                                    //edit Functionality
                                                    //Created by Rahul
                                                    //Date : 04/05/2020
//                                                    var role = "";
                                                    if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                        if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }

                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep == 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {

                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflowstatus})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;

                                                    } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time != null) {
                                                        if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }
                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    }
//                                                  
                                                    var userdata = response.data.userdata;
                                                    if (role != "" && userdata.role_id >= 1) {
                                                        var myForm = document.getElementById('fileUpload');
                                                        var formData = new FormData(myForm);
                                                        formData.append('roomcode', room_code);
                                                        formData.append('act_id', $scope.ComDataArray[0].act_id);
                                                        formData.append('processid', $scope.ComDataArray[0].roomlogactivity);
                                                        formData.append('activity_id', $scope.ComDataArray[0].mst_act_id);
                                                        formData.append('roleid', response.data.userdata.role_id);
                                                        formData.append('empid', response.data.userdata.id);
                                                        formData.append('empname', response.data.userdata.emp_name);
                                                        formData.append('remark', $scope.remark);
                                                        angular.forEach($scope.files, function (value, key) {
                                                            var file = value.file;
                                                            formData.append('file[]', file);
                                                        });

                                                        $scope.attachLogFile(formData);
                                                    } else {
                                                        $('.loader').hide();
                                                        alert("You are not allowed to Upload the attachment.");
                                                        $scope.fileupload = false;
                                                        $scope.files = [];
                                                    }
                                                } else if ($scope.btntext == 'delete')
                                                {
                                                    //Delete Functionality
                                                    //Created by Rahul
                                                    //Date : 16/06/2020
                                                    var userdata = response.data.userdata;
                                                    if (userdata.role_id >= $scope.upload_by_role_id) {
                                                        $scope.deleteFile($scope.remove_id, response.data.userdata.id,$scope.ComDataArray[0].act_id,$scope.remove_file_name);
                                                    } else {
                                                        $('.loader').hide();
                                                        alert("You are not allowed to remove the attachment.");
                                                        $scope.fileupload = false;
                                                        $scope.files = [];
                                                    }
                                                } else if ($scope.btntext == 'update') {
                                                    //update Functionality
                                                    //Created by Rahul
                                                    //Date : 04/05/2020
                                                    var role = "";
                                                    if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                        if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }

                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep == 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {

                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflowstatus})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;

                                                    } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time != null) {
                                                        if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }
                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    }
//                                                  
                                                    var userdata = response.data.userdata;
                                                    if ((role != "" && userdata.role_id >= role && userdata.role_id > $scope.ComDataArray[0].done_by_role_id) || (userdata.role_id >= $scope.ComDataArray[0].last_approval.role_id && userdata.role_id > $scope.ComDataArray[0].done_by_role_id)) {
                                                        $scope.updatesubmit(room_code, processid, actid, actname, $scope.docno, $scope.booth_code, $scope.mi5_mi15, $scope.mi0_mi5, $scope.gi0_gi3, $scope.gi15_gi5, $scope.gi8_gi14, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.ComDataArray[0].edit_id, $scope.is_in_workflow);
                                                    } else {
                                                        $('.loader').hide();
                                                        alert("You are not allowed to edit this.");
                                                        $scope.edit = false;
                                                    }
                                                }
                                            } else {
                                                $('.loader').hide();
                                                alert(response.data.message);
                                            }
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                }
                                //End Login Function

                                //Approval function :- To Approve running activity by auth & privilege chk as well as sent that activity to next approval which exists in workflow
                                //Created by Bhupendra
                                //Date : 28/01/2020
                                $scope.getapproved = function (roomcode, actlogtblid, actid, workflowstatus, workflownextstep, roleid, empid, empname, email, remark, actstatus, docno) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/com_approval_fun',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&workflowstatus=" + workflowstatus + "&workflownextstep=" + workflownextstep + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&actstatus=" + actstatus + "&docno=" + docno + "&tblname=pts_trn_laf_pressure_diff",

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        if (response.data.next_step == "-1")
                                        {
                                            window.location.href = "<?php echo base_url() ?>home";
                                        } else
                                        {
                                            $scope.resetLoginForm();
                                            location.reload();
                                            $scope.booth_code = "";
                                            $scope.mi5_mi15 = "";
                                            $scope.mi0_mi5 = "";
                                            $scope.gi0_gi3 = "";
                                            $scope.gi15_gi5 = "";
                                            $scope.gi8_gi14 = "";
                                            angular.element("#btnclose").trigger('click');
                                            $scope.getLastFilterActivity();
                                            $('.loader').hide();
                                            //$scope.getdocno();
                                        }
                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }
                                //*********************************************************************************
                                //Start function :- To Start new activity by auth chk.
                                //Created by Bhupendra
                                //Date : 4/02/2020
                                $scope.getsubmit = function (roomcode, processid, actid, actname, docno, booth_code, mi5_mi15, mi0_mi5, gi0_gi3, gi15_gi5, gi8_gi14, roleid, empid, empname, email, remark) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/submit_laf_pressure_record',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&booth_code=" + booth_code + "&mi5_mi15=" + mi5_mi15 + "&mi0_mi5=" + mi0_mi5 + "&gi0_gi3=" + gi0_gi3 + "&gi15_gi5=" + gi15_gi5 + "&gi8_gi14=" + gi8_gi14 + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&headerRecordid=" + $scope.headerRecordid,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        $scope.booth_code = "";
                                        $scope.mi5_mi15 = "";
                                        $scope.mi0_mi5 = "";
                                        $scope.gi0_gi3 = "";
                                        $scope.gi15_gi5 = "";
                                        $scope.gi8_gi14 = "";
                                        angular.element("#btnclose").trigger('click');
                                        $scope.getLastFilterActivity();
                                        $('.loader').hide();
                                        //$scope.getdocno();

                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }
                                //*********************************************************************************************

                                //*********************************************************************************
                                //Start function :- To Update started Activity.
                                //Created by Raul Chauhan
                                //Date : 05/05/2020
                                $scope.updatesubmit = function (roomcode, processid, actid, actname, docno, booth_code, mi5_mi15, mi0_mi5, gi0_gi3, gi15_gi5, gi8_gi14, roleid, empid, empname, email, remark, update_id, is_in_workflow) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/update_laf_pressure_record',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&booth_code=" + booth_code + "&mi5_mi15=" + mi5_mi15 + "&mi0_mi5=" + mi0_mi5 + "&gi0_gi3=" + gi0_gi3 + "&gi15_gi5=" + gi15_gi5 + "&gi8_gi14=" + gi8_gi14 + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&update_id=" + update_id + "&is_in_workflow=" + is_in_workflow,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.edit = false;
                                        angular.element("#btnclose").trigger('click');
                                        $scope.resetLoginForm();
                                        $scope.getLastFilterActivity();
                                        $('.loader').hide();
                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }

                                $scope.CancilUpdate = function () {
                                    $scope.edit = false;
                                }
                                //*********************************************************************************************


                                //Get Last unapproved Room Activity
                                $scope.ComDataArray = [];
                                $scope.getLastFilterActivity = function () {
                                    $scope.ComDataArray = [];
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetLastFilterActivity?room_code=' + room_code + "&table_name=pts_trn_laf_pressure_diff",
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.ComDataArray = response.data.row_data;
                                        if($scope.ComDataArray.length> 0){
                                        $scope.attachment_list = $scope.ComDataArray[0].attachment_list;
                                    }
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getLastFilterActivity();

                                //*******************Rahul's code Started**********************//
                                $scope.editTrasaction = function () {
                                    if (confirm("Do you really want to edit ?"))
                                    {
                                        $scope.edit = true;
                                        $scope.docno = $scope.ComDataArray[0].doc_no;
                                        $scope.booth_code = $scope.ComDataArray[0].booth_no;
                                        $scope.mi5_mi15 = $scope.ComDataArray[0].m1;
                                        $scope.mi0_mi5 = $scope.ComDataArray[0].m2;
                                        $scope.gi0_gi3 = $scope.ComDataArray[0].g1;
                                        $scope.gi15_gi5 = $scope.ComDataArray[0].g2;
                                        $scope.gi8_gi14 = $scope.ComDataArray[0].g3;
                                        $scope.is_in_workflow = $scope.ComDataArray[0].is_in_workflow;
                                    }
                                }

                                //*******************End Rahul's code***************************//
                                //Reset Login Form
                                $scope.resetLoginForm = function () {
                                    $scope.username = "";
                                    $scope.password = "";
                                    $scope.remark = "";
                                }

                                //File Attachment
                                $scope.fileupload = false;
                                $scope.showFileUpload = function () {
                                    $scope.fileupload = true;
                                }
                                $scope.cancilFileUpload = function () {
                                    $('#file_id').val('');
                                    $scope.files = [];
                                    $scope.fileupload = false;
                                }
                                $scope.files = [];
                                $scope.$on("fileSelected", function (event, args) {
                                    var item = args;
                                    var full_path = item.file['name'];
                                    item['file_ext'] = full_path.split(".")[1];

                                    $scope.files.push(item);

                                    var reader = new FileReader();

                                    reader.addEventListener("load", function () {
                                        $scope.$apply(function () {
                                            item.src = reader.result;
                                        });
                                    }, false);

                                    if (item.file) {
                                        reader.readAsDataURL(item.file);
                                    }
                                });
                                $scope.attachLogFile = function (form_data) {
                                    var request = {
                                        method: 'POST',
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/attachLogFile',
                                        data: form_data,
                                        headers: {
                                            'Content-Type': undefined
                                        }
                                    };

                                    // SEND THE FILES.
                                    $http(request).then(function (response) {
                                        alert(response.data.message);
                                        window.location.reload();
                                        $scope.resetLoginForm();
                                        $scope.fileupload = false;
                                        $scope.getLastFilterActivity();
                                        angular.element("#btnclose").trigger('click');
                                        $('.loader').hide();
                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });

                                }
                                $scope.remove_id = "";
                                $scope.upload_by_role_id = "";
                                $scope.remove_file_name = "";
                                $scope.removeFile = function (obj) {
                                    $scope.remove_id = obj.id;
                                    $scope.upload_by_role_id = obj.role_id;
                                     $scope.remove_file_name = obj.file_name;
                                }

                                $scope.deleteFile = function (objId, user_id,act_id,file_name) {
                                    if (confirm("Do you really want to Remove This attachment ?"))
                                    {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/removeAttachment',
                                            method: "POST",
                                            data: "id=" + objId + "&user_id=" + user_id+"&act_id="+act_id+"&file_name="+file_name,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.remove_id = "";
                                            $scope.upload_by_role_id = "";
                                            $scope.remove_file_name = "";
                                            alert(response.data.message);
                                            $scope.resetLoginForm();
                                            $scope.getLastFilterActivity();
                                            angular.element("#btnclose").trigger('click');
                                            $('.loader').hide();
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                }

                            });
</script>