<style>
    .active {
        background-color: rgb(34,139,34) !important;
        color: white;
        border-color: rgb(34,139,34) !important;
    }

    /*  Reset popup css */
    .reset_popup{
        background: rgba(0,0,0,0.5);
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        margin: 0;
        display: flex!important;
        align-items: center;
        justify-content: center;
    }
    .reset_popup .modal-dialog{
        width:450px;
    }

    .reset_popup .modal-header .close{
        margin: -7px;
    }
    
</style>
<input type="hidden" id="base" value="<?php echo base_url(); ?>">
<!-- quick link section start-->
<div ng-controller="homeCtrl" ng-app="homeApp" ng-cloak>
    <section class="contentBox paddact redBorder">
        <div class="contentHeader">

            <div class="row">

                <div class="col-8">
                    <h1>Quick Links</h1>
                </div>

                <!--                <div class="col-4 text-right">
                                    <button ng-click="viewAllBlocks()" class="button smallBtn primaryBtn">
                                        View All
                                    </button>
                                </div>-->

            </div>

        </div>
        <div class="contentBody scroll-body">
            <ul class="linkListWrap">
                <li class="icon qaIcon">
                    <a href="<?php echo base_url() ?>pending_approval">
                        <div class="linkText">QA Verification</div>
                        <div class="logButton">
                            Log Activity
                        </div>
                    </a>
                </li>

                <li class="icon inprogressIcon">
                    <a href="<?php echo base_url() ?>inprogress_activity">
                        <div class="linkText">In Progress</div>
                        <div class="logButton">
                            Log Activity
                        </div>
                    </a>
                </li>
                <li ng-repeat="roomObj in roomData" ng-click="getProcessLogActivity(roomObj, $index)"  class="icon">
                    <a href="#">
                        <div class="linkText">{{roomObj.room_code}}</div>
                        <div class="logButton {{current == $index?'active':'' }}">
                            {{roomObj.room_name}}
                        </div>
                    </a>
                </li>

            </ul>

            <ul class="linkListWrap" >
                <li class="icon ipqaIcon" ng-if="activityList.length > 0">
                    <a href="#" data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('roomin');" >
                        <div class="linkText">Room In</div>
                        <div class="logButton">
                            Log Activity
                        </div>
                    </a>
                </li>
                <li class="icon ipqaIcon" ng-if="activityList.length > 0">
                    <a href="#" data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('roomout');">
                        <div class="linkText">Room Out</div>
                        <div class="logButton">
                            Log Activity
                        </div>
                    </a>
                </li>
                <li class="icon ipqaIcon" ng-if="activityList.length > 0 && room_type != 'IPQA'">
                    <a href="#" data-toggle="modal" data-target="#batchinModal" ng-click="setbtntext('batchin');" >
                        <div class="linkText">Batch In</div>
                        <div class="logButton">
                            Log Activity
                        </div>
                    </a>
                </li>
                <li class="icon ipqaIcon" ng-if="activityList.length > 0 && room_type != 'IPQA'">
                    <a href="#" data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('batchout');">
                        <div class="linkText">Batch Out</div>
                        <div class="logButton">
                            Log Activity
                        </div>
                    </a>
                </li>



            </ul>
        </div>


    </section>
    <!-- quick link section end-->
    <section class="contentBox paddact redBorder new-sectionbox" ng-if="activityList.length > 0">
        <div class="contentHeader">
            <div class="row">
                <div class="col-md-12">
                    <ul class="menuItemsWrap" ng-if="activityList.length > 0">
                        <li class="menuItems" ng-repeat="activityObj in activityList">
                            <a href="#" ng-click="GetmstHeaderData(activityObj.activity_url, activeRoomData.room_id, activeRoomData.room_code, activityObj.activity_name, activityObj.mst_act_id, activityObj.mst_act_id, room_type)">
                            <!-- <a href="<?php echo base_url() ?>{{activityObj.activity_url}}?room_id={{activeRoomData.room_id}}&room_code={{activeRoomData.room_code}}&actname={{activityObj.activity_name}}&processid={{activityObj.mst_act_id}}&actid={{activityObj.mst_act_id}}&room_type={{room_type}}"> -->
                                <div class="menuImg">
                                    <img src="<?php echo base_url() ?>assets/images/log1img.png" alt="log1img">
                                </div>
                                <div class="menuTxt">
                                    {{activityObj.activity_name}}
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="contentBox blueBorder pl0">
        <div class="row rooming-table padd0">
            <div class="col-sm-6">
                <!-- ngIf: roomInUserData.length > 0 --><div class="container ng-scope" ng-if="roomInUserData.length > 0">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-lg-12 text-center"><h4>Room In User Details</h4></div>
                            <table class="table runningacttable2">
                                <thead>
                                    <tr border="1">
                                        <th>User Name</th>
                                        <th>Room</th>
                                        <th>Role</th>
                                        <th>Room in Date</th>
                                        <th>Room in Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="dataObj in roomInUserData">
                                        <td>{{dataObj.emp_name}}</td>
                                        <td>{{dataObj.room_code}}</td>
                                        <td>{{dataObj.role_description}}</td>
                                        <td>{{dataObj.RoominDate| date:'dd-MMM-yyyy'}}</td>
                                        <td>{{dataObj.RoominTime| date:'HH:mm:ss'}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- end ngIf: roomInUserData.length > 0 -->
            </div>
            <div class="col-sm-6 plr0">
                <!-- ngIf: runningBatchData.length > 0 --><div class="container ng-scope" ng-if="runningBatchData.length > 0">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-lg-12 text-center"><h4>Running Batch Details</h4></div>
                            <table class="table runningacttable2">
                                <thead>
                                    <tr border="1">
                                        <th>User Name</th>
                                        <th>Room</th>
                                        <th>Running Batch</th>
                                        <th>Product</th>
                                        <th>Batch Start Date</th>
                                        <th>Batch Start Time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="dataObj in runningBatchData" class="ng-scope">
                                        <td>{{dataObj.emp_name}}
                                        <td>{{dataObj.room_code}}</td>
                                        <td>{{dataObj.batch_no}}</td>
                                        <td>{{dataObj.product_code}}</td>
                                        <td>{{dataObj.BatchDate| date:'dd-MMM-yyyy'}}</td>
                                        <td>{{dataObj.BatchTime | date:'HH:mm:ss'}}</td>
                                    </tr><!-- end ngRepeat: dataObj in runningBatchData -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- end ngIf: runningBatchData.length > 0 -->
            </div>
        </div>
    </div>

    <!-- activity section start-->
    <section class="contentBox blueBorder activityinprogress" ng-show="inProgressActivityData.length > 0">
        <div class="contentHeader">
            <div class="row">
                <div class="col-12">
                    <h1>Activity in progress</h1>
                </div>
            </div>

        </div>

        <div class="contentBody">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Doc Id</th>
                            <th>Room Code</th>
                            <th>Equipment Code</th>
                            <th>Start Date</th>
                            <th>Start Time</th>
                            <th>Started By</th>
                            <th>Current Activity</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr ng-repeat="dataObj in inProgressActivityData">
                            <td>{{dataObj.doc_id}}</td>
                            <td>{{dataObj.room_code}}</td>
                            <td>{{dataObj.equip_code}}</td>
                            <td>{{dataObj.start_date | date:'dd-MMM-yyyy' }}</td><td> {{dataObj.start_time}}</td>
                            <td>{{dataObj.user_name}}</td>
                            <td>{{dataObj.activity_name | date:'HH:mm:ss'}} - {{dataObj.activity_remarks}}</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- activity section end-->
    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
    <?php $this->load->view("template/ponta_sahib/batchin_modal"); ?>

    <!--Mega menu-->
    <div class="modal fade megaMenu" id="activity_list_modal"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true">
        <div class="modal-dialog modal-dialog-slideout modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{show_room_code}} - <span>{{show_room_name}}</span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="menuItemsWrap" ng-if="activityList.length > 0">
                        <li class="menuItems" ng-repeat="activityObj in activityList">
                            <a href="<?php echo base_url() ?>{{activityObj.activity_url}}?room_id={{activeRoomData.room_id}}&room_code={{activeRoomData.room_code}}&actname={{activityObj.activity_name}}&processid={{activityObj.mst_act_id}}&actid={{activityObj.mst_act_id}}&room_type={{room_type}}">
                                <div class="menuImg">
                                    <img src="<?php echo base_url() ?>assets/images/log1img.png" alt="log1img">
                                </div>
                                <div class="menuTxt">
                                    {{activityObj.activity_name}}
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
    <!--Mega menu end-->
    <!--Reset password model-->
    <div class="modal reset_popup" ng-show="resetPass == true;" style="display:block" id="loginModal"  role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title login-label" id="exampleModalLabel">Reset Password</h4>
                    <button type="button" class="close" data-dismiss="modal" id="btnclose" ng-click="cancilReset()"><span aria-hidden="true">&times;</span></button>
                </div>
                <form>
                    <div class="modal-body">

                        <div class="row mb-2">
                            <div class="col-sm-4"><label for="user Id" class="control-label login-label">Password:</label></div>
                            <div class="col-sm-8"><input type="password" ng-model="new_password" class="form-control" name="username"></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4"><label for="Password" class="control-label login-label"> Confirm Password:</label></div>
                            <div class="col-sm-8"><input type="password" name="password" ng-model="confpassword" class="form-control"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-danger" >Cancel</button>
                        <button type="button"  ng-disabled="new_password == '' || confpassword == ''" class="btn btn-primary" ng-click="resetPassword();">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--End Reset password model-->
</div>
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/toaster.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script>
                            var app = angular.module("homeApp", ['toaster', 'blockUI', 'angular.chosen']);
                            app.directive('chosen', function ($timeout) {

                                var linker = function (scope, element, attr) {

                                    scope.$watch('productData', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    $timeout(function () {
                                        element.chosen();
                                    }, 0, false);
                                };
                                return {
                                    restrict: 'A',
                                    link: linker
                                };
                            });
                            app.controller("homeCtrl", function ($scope, $http, $filter, toaster, blockUI, $timeout) {

                               // $scope.terminal_id = "103.211.15.1";
                                $scope.terminal_id='<?php echo $_SERVER['REMOTE_ADDR']?>';
                                //alert($scope.terminal_id);
//                                $scope.getServerList = function () {
//                                    $http({
//                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetServerList',
//                                        method: "GET",
//                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
//                                    }).then(function (response) {
//                                        $scope.serverData = response.data.ip_list;
//                                        $scope.device_id = $scope.serverData[0].server_ip;
//                                        $scope.getRoomList();
//                                    }, function (error) { // optional
//                                        console.log("Something went wrong.Please try again");
//                                    });
//                                }
//                                $scope.getServerList();
                                $scope.showAllBlocks = false;
                                $scope.roomData = [];
                                $scope.getRoomList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist?terminal_id='+$scope.terminal_id,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.roomData = response.data.room_list;
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getRoomList();
                                $scope.getInprogressActivityList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetInprogressActivityList?terminal_id='+$scope.terminal_id,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.inProgressActivityData = response.data.in_progress_activity;
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getInprogressActivityList();
                                $scope.current = '-1';
                                $scope.activityList = [];
                                $scope.activeRoomData = {};
                                $scope.show_room_name = '';
                                $scope.show_room_code = '';
                                $scope.getProcessLogActivity = function (dataObj, index) {
                                    //$("#activity_list_modal").modal('show');
                                    $scope.activeRoomData = dataObj;
                                    $scope.room_type = dataObj.room_type;
                                    $scope.show_room_name = dataObj.room_name;
                                    $scope.show_room_code = dataObj.room_code;
                                    $scope.current = index
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getactivitylistforroom?room_id=' + dataObj.room_id,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.activityList = response.data.activity_list;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.selectedroomData = {};
                                $scope.getroomname = function (dataObj, index) {
                                    $scope.selectedroomData = dataObj;
                                    $scope.current = index
                                }

                                $scope.btntext = '';
                                $scope.setbtntext = function (btntext)
                                {
                                    $scope.btntext = btntext;
                                }

                                $scope.batchlogin = function (uid, pwd)
                                {

                                    $scope.username = uid;
                                    $scope.password = pwd;
                                    $scope.login();
                                }
                                //Login Function
                                //Created by Bhupendra
                                //Date : 26/02/2020
                                $scope.resetPass = false;
                                $scope.user_data = {};
                                $scope.login = function () {
                                    var response = confirm("Do You Really Want To Do This Task?");
                                    if (response == true)
                                    {
                                        pwd2 = SHA256($scope.password);
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                            method: "POST",
                                            data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&action=" + $scope.btntext + "&auth=" + btoa(btoa($scope.password))+"&room_code="+$scope.activeRoomData.room_code,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            if (response.data.message == "")
                                            {
                                                if ($scope.btntext == 'roomin')
                                                {
                                                    $scope.getroomin($scope.activeRoomData.room_code, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2,$scope.remark)
                                                } else if ($scope.btntext == 'roomout')
                                                {
                                                    $scope.getroomout($scope.activeRoomData.room_code, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2,$scope.remark)
                                                } else if ($scope.btntext == 'batchin')
                                                {

                                                    $scope.getbatchin($scope.activeRoomData.room_code, $scope.batchcod, $scope.prodcode, $scope.product_desc, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2)
                                                } else if ($scope.btntext == 'batchout')
                                                {
                                                    $scope.getbatchout($scope.activeRoomData.room_code, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2)
                                                }

                                            } else {
                                                if (response.data.message == 'password') {
                                                    alert("Please reset your password first");
                                                    angular.element("#btnclose").trigger('click');
                                                    $scope.user_data = response.data.userdata;
                                                    $scope.resetPass = true;
                                                } else {
                                                    alert(response.data.message);
                                                }


                                            }
                                        }, function (error) {
                                            console.log(error);
                                        });
                                    }
                                }
                                //End Login Function

                                //Room In function :- To Record the details of person who is room in
                                //Created by Bhupendra
                                //Date : 26/02/2020
                                $scope.isuserroomin = 0;
                                $scope.getroomin = function (roomcode, roleid, empid, empname, email,remark) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getroomin',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" +remark,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        angular.element("#btnclose").trigger('click');
                                        $scope.isuserroomin = 1;
                                        $scope.getRoomInUserDetail();
                                        $scope.getrunningBatchData();
                                        //$scope.getProcessLogActivity($scope.selectedroomData,$scope.current);
                                    }, function (error) {
                                        console.log(error);
                                    });
                                }

                                //Room Out function :- To Inactive the person who was room in
                                //Created by Bhupendra
                                //Date : 26/02/2020

                                $scope.getroomout = function (roomcode, roleid, empid, empname, email, remark) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getroomout',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark="+remark ,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        angular.element("#btnclose").trigger('click');
                                        $scope.getRoomInUserDetail();
                                        $scope.getrunningBatchData();
                                    }, function (error) {
                                        console.log(error);
                                    });
                                }

                                //Batch in function :- To start a batch in the selected room
                                //Created by Bhupendra
                                //Date : 28/02/2020

                                $scope.getbatchin = function (roomcode, batchcod, prodcode, product_desc, roleid, empid, empname, email) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getbatchin',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&batchcod=" + batchcod + "&prodcode=" + prodcode + "&product_desc=" + product_desc + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        $scope.resetBatchInForm();
                                        angular.element("#batchbtnclose").trigger('click');
                                        $scope.getRoomInUserDetail();
                                        $scope.getrunningBatchData();
                                    }, function (error) {
                                        console.log(error);
                                    });
                                }

                                //Batch Out function :- To stop the running batch in the selected room
                                //Created by Bhupendra
                                //Date : 28/02/2020

                                $scope.getbatchout = function (roomcode, roleid, empid, empname, email) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getbatchout',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        angular.element("#btnclose").trigger('click');
                                        $scope.getRoomInUserDetail();
                                        $scope.getrunningBatchData();
                                    }, function (error) {
                                        console.log(error);
                                    });
                                }
                                //*********************************************************************************
                                $scope.roomInUserData = [];
                                $scope.getRoomInUserDetail = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoomInUserDetail?terminal_id='+$scope.terminal_id,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.roomInUserData = response.data.roomin_user;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }

                                $scope.getRoomInUserDetail();

                                $scope.runningBatchData = [];
                                $scope.getrunningBatchData = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetrunningBatchData?terminal_id='+$scope.terminal_id,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.runningBatchData = response.data.BatchDetail;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }

                                $scope.getrunningBatchData();

                                $scope.productData = [];
                                $scope.GetProductList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.productData = response.data.product_list;
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.GetProductList();
                                $scope.product_desc = "";
                                $scope.getProductDetail = function (dataObj) {
                                    $scope.product_desc = $filter('filter')($scope.productData, {product_code: dataObj})[0].product_name;
                                }


                                //Reset Login Form
                                $scope.resetLoginForm = function () {
                                    $scope.username = "";
                                    $scope.password = "";
                                    $scope.remark = "";
                                }

                                //Reset BatchIn Form
                                $scope.resetBatchInForm = function () {
                                    $scope.batchusername = "";
                                    $scope.batchpassword = "";
                                    $scope.batchcod = "";
                                    $scope.prodcode = "";
                                }
                                $scope.viewAllBlocks = function () {
                                    $scope.showAllBlocks = true;
                                }

                                $scope.HeaderData = [];
                                $scope.GetmstHeaderData = function (activity_url, room_id, room_code, activity_name, mst_act_id, mst_act_id, room_type) {
                                    //room_id={{activeRoomData.room_id}}&room_code={{activeRoomData.room_code}}&actname={{activityObj.activity_name}}&processid={{activityObj.mst_act_id}}&actid={{activityObj.mst_act_id}}&room_type={{room_type}})
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetmstHeaderData?logid=' + mst_act_id,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        if (response.data.result == true)
                                        {
                                            $scope.HeaderData = response.data.HeaderData;
                                            $scope.headerid = $scope.HeaderData[0]["id"];
                                            $scope.formno = $scope.HeaderData[0]["form_no"];
                                            $scope.versionno = $scope.HeaderData[0]["user_version_no"];
                                            $scope.effectivedate = $scope.HeaderData[0]["effective_date"];
                                            if ($scope.HeaderData[0]["retired_date"] == null) {
                                                $scope.retireddate = 'NA';
                                            } else {
                                                $scope.retireddate = $scope.HeaderData[0]["retired_date"];
                                            }
                                            window.location.href = "<?php echo base_url() ?>" + activity_url + "?room_id=" + room_id + "&room_code=" + room_code + "&actname=" + activity_name + "&processid=" + mst_act_id + "&actid=" + mst_act_id + "&room_type=" + room_type + "&formno=" + $scope.formno + "&versionno=" + $scope.versionno + "&effectivedate=" + $scope.effectivedate + "&retireddate=" + $scope.retireddate + "&headerid=" + $scope.headerid;
                                        } else
                                        {
                                            alert("No version of Log Name is effective.");
                                            return false;
                                        }
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                //reset pasword
                                $scope.resetPassword = function () {
                                    var newPassword = $scope.new_password;
                                    var upperCount = newPassword.replace(/[^A-Z]/g, "").length;
                                    var lowerCount = newPassword.replace(/[^a-z]/g, "").length;
                                    var numberCount = newPassword.replace(/[^0-9]/g, "").length;
                                    var minNumberofChars = 8;
                                    var maxNumberofChars = 16;
                                    var regularExpression = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,16}/;
                                    var empname = $scope.user_data.emp_name;
                                    var cont = $scope.user_data.emp_contact;
                                    var userid = $scope.user_data.emp_email;
                                    var name = empname.toUpperCase().split(" ");
                                    var firstChar = newPassword.charAt(0);
                                    if (firstChar <= '9' && firstChar >= '0') {
                                        alert("Password should not Start with Numeric Value");
                                        return false;
                                    }

                                    for (var i = 0; i <= name.length; i++) {
                                        var tempnew = newPassword.toUpperCase();
                                        if (name[i] != '') {
                                            if (tempnew.indexOf(name[i]) != -1) {
                                                alert("Password should not contain First Name or Surname");
                                                return false;
                                            }
                                        }
                                    }
                                    if (newPassword.toUpperCase().indexOf(userid.toUpperCase()) != -1) {
                                        alert("Password should not contain Userid");
                                        return false;
                                    }
                                    
                                    if (newPassword.length < minNumberofChars) {
                                        alert("Password should be atleast 8 character");
                                        return false;
                                    }
                                    if (upperCount < 1) {
                                        alert("Password should contain at least one upper case letter: (A – Z)");
                                        return false;
                                    }
                                    if (lowerCount < 1) {
                                        alert("Password should contain at least one lower case letter: (a – z)");
                                        return false;
                                    }
                                    if (numberCount < 1) {
                                        alert("Password should contain at least one number: (0 – 9)");
                                        return false;
                                    }

                                    if (!regularExpression.test(newPassword)) {
                                        alert("Password should contain at least one Special Characters");
                                        return false;
                                    }
                                    if ($scope.confpassword != $scope.confpassword) {
                                        alert("Confirm Password is not same as Password");
                                        return false;
                                    }

                                    var user_id = $scope.user_data.id;
                                    var pwd = SHA256($scope.new_password);
                                    $http({
                                        //url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/resetPassword',//'<?php //echo base_url() ?>Login/resetPassword',
                                        url: '<?php echo base_url() ?>Login/resetPassword',//'<?php //echo base_url() ?>Login/resetPassword',
                                        method: "POST",
                                        data: "user_id=" + user_id + "&password=" + pwd,
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
//                                        alert(response.data.message);
//                                        window.location.href = "<?php echo base_url() ?>sf_home";
//                                        console.log(response);
                                        alert(response.data.message);
                                        if(response.data.result){
                                            $scope.user_data = {};
                                            $scope.resetPass = false;
                                            window.location.href = "<?php echo base_url() ?>sf_home";
                                        }
                                    }, function (error) { // optional
                                        alert(error.data.message);
                                    });


                                }

                                $scope.cancilReset = function () {
                                    window.location.href = "<?php echo base_url() ?>sf_home";
                                }

                            });
</script>
<script>
    /**
     *  Secure Hash Algorithm (SHA256)
     *  http://www.webtoolkit.info/
     *  Original code by Angel Marin, Paul Johnston
     **/

    function SHA256(s) {
        var chrsz = 8;
        var hexcase = 0;
        function safe_add(x, y) {
            var lsw = (x & 0xFFFF) + (y & 0xFFFF);
            var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
            return (msw << 16) | (lsw & 0xFFFF);
        }

        function S(X, n) {
            return (X >>> n) | (X << (32 - n));
        }
        function R(X, n) {
            return (X >>> n);
        }
        function Ch(x, y, z) {
            return ((x & y) ^ ((~x) & z));
        }
        function Maj(x, y, z) {
            return ((x & y) ^ (x & z) ^ (y & z));
        }
        function Sigma0256(x) {
            return (S(x, 2) ^ S(x, 13) ^ S(x, 22));
        }
        function Sigma1256(x) {
            return (S(x, 6) ^ S(x, 11) ^ S(x, 25));
        }
        function Gamma0256(x) {
            return (S(x, 7) ^ S(x, 18) ^ R(x, 3));
        }
        function Gamma1256(x) {
            return (S(x, 17) ^ S(x, 19) ^ R(x, 10));
        }

        function core_sha256(m, l) {
            var K = new Array(0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786, 0xFC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA, 0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147, 0x6CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070, 0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2);
            var HASH = new Array(0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19);
            var W = new Array(64);
            var a, b, c, d, e, f, g, h, i, j;
            var T1, T2;
            m[l >> 5] |= 0x80 << (24 - l % 32);
            m[((l + 64 >> 9) << 4) + 15] = l;
            for (var i = 0; i < m.length; i += 16) {
                a = HASH[0];
                b = HASH[1];
                c = HASH[2];
                d = HASH[3];
                e = HASH[4];
                f = HASH[5];
                g = HASH[6];
                h = HASH[7];
                for (var j = 0; j < 64; j++) {
                    if (j < 16)
                        W[j] = m[j + i];
                    else
                        W[j] = safe_add(safe_add(safe_add(Gamma1256(W[j - 2]), W[j - 7]), Gamma0256(W[j - 15])), W[j - 16]);
                    T1 = safe_add(safe_add(safe_add(safe_add(h, Sigma1256(e)), Ch(e, f, g)), K[j]), W[j]);
                    T2 = safe_add(Sigma0256(a), Maj(a, b, c));
                    h = g;
                    g = f;
                    f = e;
                    e = safe_add(d, T1);
                    d = c;
                    c = b;
                    b = a;
                    a = safe_add(T1, T2);
                }

                HASH[0] = safe_add(a, HASH[0]);
                HASH[1] = safe_add(b, HASH[1]);
                HASH[2] = safe_add(c, HASH[2]);
                HASH[3] = safe_add(d, HASH[3]);
                HASH[4] = safe_add(e, HASH[4]);
                HASH[5] = safe_add(f, HASH[5]);
                HASH[6] = safe_add(g, HASH[6]);
                HASH[7] = safe_add(h, HASH[7]);
            }
            return HASH;
        }

        function str2binb(str) {
            var bin = Array();
            var mask = (1 << chrsz) - 1;
            for (var i = 0; i < str.length * chrsz; i += chrsz) {
                bin[i >> 5] |= (str.charCodeAt(i / chrsz) & mask) << (24 - i % 32);
            }
            return bin;
        }

        function Utf8Encode(string) {
            string = string.replace(/\r\n/g, "\n");
            var utftext = "";
            for (var n = 0; n < string.length; n++) {

                var c = string.charCodeAt(n);
                if (c < 128) {
                    utftext += String.fromCharCode(c);
                } else if ((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                } else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }

            }

            return utftext;
        }

        function binb2hex(binarray) {
            var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
            var str = "";
            for (var i = 0; i < binarray.length * 4; i++) {
                str += hex_tab.charAt((binarray[i >> 2] >> ((3 - i % 4) * 8 + 4)) & 0xF) +
                        hex_tab.charAt((binarray[i >> 2] >> ((3 - i % 4) * 8)) & 0xF);
            }
            return str;
        }

        s = Utf8Encode(s);
        return binb2hex(core_sha256(str2binb(s), s.length * chrsz));
    }
</script>
<script>
    function Encrypt(str) {
        if (!str)
            str = "";
        str = (str == "undefined" || str == "null") ? "" : str;
        try {
            var key = 146;
            var pos = 0;
            ostr = '';
            while (pos < str.length) {
                ostr = ostr + String.fromCharCode(str.charCodeAt(pos) ^ key);
                pos += 1;
            }

            return ostr;
        } catch (ex) {
            return '';
        }
    }

    function Decrypt(str) {
        if (!str)
            str = "";
        str = (str == "undefined" || str == "null") ? "" : str;
        try {
            var key = 146;
            var pos = 0;
            ostr = '';
            while (pos < str.length) {
                ostr = ostr + String.fromCharCode(key ^ str.charCodeAt(pos));
                pos += 1;
            }

            return ostr;
        } catch (ex) {
            return '';
        }
    }
</script>
<script type="text/javascript">
<!--

    var keyStr = "ABCDEFGHIJKLMNOP" +
            "QRSTUVWXYZabcdef" +
            "ghijklmnopqrstuv" +
            "wxyz0123456789+/" +
            "=";
    function encode64(input) {
        input = escape(input);
        var output = "";
        var chr1, chr2, chr3 = "";
        var enc1, enc2, enc3, enc4 = "";
        var i = 0;
        do {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
                    keyStr.charAt(enc1) +
                    keyStr.charAt(enc2) +
                    keyStr.charAt(enc3) +
                    keyStr.charAt(enc4);
            chr1 = chr2 = chr3 = "";
            enc1 = enc2 = enc3 = enc4 = "";
        } while (i < input.length);
        return output;
    }

    function decode64(input) {
        var output = "";
        var chr1, chr2, chr3 = "";
        var enc1, enc2, enc3, enc4 = "";
        var i = 0;
        // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
        var base64test = /[^A-Za-z0-9\+\/\=]/g;
        if (base64test.exec(input)) {
            alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
        }
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        do {
            enc1 = keyStr.indexOf(input.charAt(i++));
            enc2 = keyStr.indexOf(input.charAt(i++));
            enc3 = keyStr.indexOf(input.charAt(i++));
            enc4 = keyStr.indexOf(input.charAt(i++));
            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;
            output = output + String.fromCharCode(chr1);
            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

            chr1 = chr2 = chr3 = "";
            enc1 = enc2 = enc3 = enc4 = "";
        } while (i < input.length);
        return unescape(output);
    }

    //--></script>
