<html lang="en">
    <?php $this->load->view('template/ponta_sahib/ponta_header'); ?>
    <body>

        <div class="mainWrapper">
            <input type="hidden" id="base" value="<?php echo base_url(); ?>">
            <header class="mainHeader">
                <div class="container-fluid">

                    <div class="logo">
                        <a href="#"><img src="<?php echo base_url(); ?>assets/images/E-Logbook-logo.png" alt="E-Logbook-logo"></a>
                    </div>
                    <div class="toogleFullScreen" data-toggle-fullscreen>
                        <a href="#"><img src="<?php echo base_url(); ?>assets/images/zoom.png" alt="toogleFullScreen"></a>
                    </div>

                </div>

            </header>
            <main class="container-fluid">
                <?php echo $the_view_content; ?>
            </main>
            <?php $this->load->view('template/ponta_sahib/ponta_footer'); ?>
        </div>
        <script src="<?php echo base_url(); ?>js/network.js"></script>
    </body>
</html>