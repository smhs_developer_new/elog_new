<div ng-app="inProgressApp" ng-controller="inProgressCtrl" ng-cloak>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>sf_home">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">In Progress</li>
        </ol>
    </nav>

    <!-- Pending for Approval start -->
    <section class="contentBox blueBorder">


        <div class="contentHeader">

            <div class="row">

                <div class="col-8">
                    <h1>In Progress</h1>
                </div>

                <div class="col-4 text-right">

                </div>

            </div>

        </div>

        <div class="contentBody">
            <div class="table-responsive noscroll" ng-show="inProgressActivityData.length > 0">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Doc Id</th>
                            <th>Room Code</th>
                            <th>Equipment Code</th>
                            <th>Start Date</th>
                            <th>Start Time</th>
                            <th>Started By/Done By</th>
                            <!--<th>Role(Started By)</th>-->
                            <th>Stop Date</th>
                            <th>Stop Time</th>
                            <th>Stop By</th>
                            <!--<th>Role(Stop By)</th>-->
                            <th>Batch No.</th>
                            <th>Product Code</th>
                            <th>Product Disc.</th>
                            <th>Current Activity</th>
                            <th>Next Step(Pending Action)</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="dataObj in inProgressActivityData">
                            <td>{{dataObj.doc_id}}</td>
                            <td>{{dataObj.room_code}}</td>
                            <td>{{dataObj.equip_code}}</td>
                            <td>{{dataObj.start_date| date:'dd-MMM-yyyy'}}</td>
                            <td>{{dataObj.start_time}}</td>
                            <td>{{dataObj.user_name}}</td>
                            <!--<td>{{dataObj.user_id > 0 ? getRoleName(dataObj.user_id) : ""}}</td>-->
                            <td>{{dataObj.stop_date| date:'dd-MMM-yyyy'}}</td>
                            <td>{{dataObj.stop_time}}</td>
                            <td>{{dataObj.stop_by}}</td>
                            <!--<td>{{dataObj.stop_by_id > 0 ? getRoleName(dataObj.stop_by_id) : ""}}</td>-->
                            <td>{{dataObj.batch_no!=''?dataObj.batch_no:'NA'}}</td>
                            <td>{{dataObj.product_code!=''?dataObj.product_code:'NA'}}</td>
                            <td>{{(dataObj.product_code != 'NA' && dataObj.product_code != '') ? getProductDesc(dataObj.product_code) : 'NA'}}</td>
                            <td>{{dataObj.activity_remarks}}</td>
                            <!--<td ng-if="dataObj.activity_stop == NULL && dataObj.workflowstatus > 0 && dataObj.next_step > 0 && dataObj.workflownextstep != 0">Pending for {{getNextApproval(dataObj, $index, dataObj.workflownextstep)}} Approval</td>-->
                            <td ng-if="dataObj.activity_stop == NULL && dataObj.workflowstatus > 0 && dataObj.next_step > 0 && dataObj.workflownextstep != 0 && dataObj.processid!=29 && dataObj.processid!=18">Pending for {{getNextApproval(dataObj, $index, dataObj.workflownextstep)}} Approval</td>
                            <td ng-if="dataObj.activity_stop != NULL && dataObj.workflowstatus > 0 && dataObj.next_step > 0 && dataObj.workflownextstep != 0">Pending for {{getNextApproval(dataObj, $index, dataObj.workflownextstep)}} Approval</td>
                            <td ng-if="dataObj.next_step == NULL && dataObj.activity_stop == NULL && dataObj.workflowstatus != 0">Pending for operator to Activity Stop</td>
                            <td ng-if="dataObj.next_step == NULL && dataObj.activity_stop != NULL && dataObj.workflowstatus == 0">Pending for operator to Activity Stop</td>
                            <td ng-if="dataObj.activity_stop == NULL && dataObj.workflownextstep == 0">Pending for operator to Activity Stop</td>
                            <td ng-if="dataObj.activity_stop == NULL && dataObj.workflownextstep != 0 && (dataObj.processid==18)">Pending for operator to Activity Stop</td>
                            <td ng-if="dataObj.activity_stop == NULL && dataObj.workflownextstep != 0 && dataObj.processid==29 && dataObj.selection_status=='from'">Pending for operator to Activity Stop</td>
                            <td ng-if="dataObj.activity_stop == NULL && dataObj.workflowstatus > 0 && dataObj.next_step > 0 && dataObj.workflownextstep != 0 && dataObj.processid==29 && dataObj.selection_status=='to'">Pending for {{getNextApproval(dataObj, $index, dataObj.workflownextstep)}} Approval</td>
<!--                        <td ng-if="dataObj.activity_stop == NULL && dataObj.workflowstatus > 0 && dataObj.next_step > 0 && dataObj.workflownextstep!=0">Pending for {{getNextApproval(dataObj,$index,dataObj.workflownextstep)}} Approval</td>
                            <td ng-if="dataObj.activity_stop != NULL && dataObj.workflowstatus > 0 && dataObj.next_step > 0 && dataObj.workflownextstep!=0">Pending for {{getNextApproval(dataObj,$index,dataObj.workflownextstep)}} Approval</td>
                            <td ng-if="dataObj.next_step == NULL && dataObj.activity_stop == NULL && dataObj.workflowstatus != 0">Pending for Activity Stop</td>
                            <td ng-if="dataObj.next_step == NULL && dataObj.activity_stop == NULL && dataObj.workflowstatus == 0">Pending for Activity Stop</td>
                            <td ng-if="dataObj.activity_stop != NULL && dataObj.workflownextstep == 0">Pending for Activity Stop</td>-->
                            <td> 
                                <div class="dropdown">
                                    <button class="button smallBtn primaryBtn dropdown-toggle" type="button" id="actionMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Action
                                    </button>
                                    <!--                                    <div class="dropdown-menu" aria-labelledby="actionMenuButton" ng-if="dataObj.type == 'home'">
                                                                            <a href="#" data-toggle="modal" class="dropdown-item" data-target="#loginModal" ng-hide="((dataObj.workflownextstep > 0 && dataObj.workflowstatus > 0) || (dataObj.activity_stop > 0))"  ng-click="setActdata(dataObj, 'stop');"  >Activity Stop</a>&nbsp;
                                                                            <a href="#" data-toggle="modal" class="dropdown-item" data-target="#loginModal"  ng-hide="((dataObj.workflownextstep > 0 && dataObj.workflowstatus > 0) || (dataObj.activity_stop > 0))" ng-click="setActdata(dataObj, 'takeover');"   >Takeover</a>&nbsp;
                                                                            <a href="#" data-toggle="modal" class="dropdown-item" data-target="#loginModal" ng-hide="(dataObj.activity_stop != NULL && dataObj.activity_stop != '' && dataObj.workflownextstep == 0 && dataObj.workflowstatus == 0) || ((dataObj.activity_stop == NULL || dataObj.activity_stop == '') && dataObj.workflownextstep == 0 && dataObj.workflowstatus != 0) || (dataObj.workflownextstep == 0 && dataObj.workflowstatus == 0)"  ng-click="setActdata(dataObj, 'approval');"  >Approve</a>
                                                                        </div>-->
                                    <!--                                    <div class="dropdown-menu" aria-labelledby="actionMenuButton" ng-if="dataObj.type == 'log' || dataObj.activity_url == 'vertical_sampler_dies_cleaning' || dataObj.type == 'home' || dataObj.type == 'tabletooling'">-->
                                    <div class="dropdown-menu" aria-labelledby="actionMenuButton">

                                        <a style="margin-top: 10px;" class="dropdown-item" href="<?php echo base_url() ?>{{dataObj.activity_url}}?room_code={{dataObj.room_code}}&actname={{dataObj.activity_name}}&processid={{dataObj.processid}}&actid={{dataObj.mst_act_id}}&room_type={{dataObj.room_type}}&doc_id={{dataObj.doc_id}}&formno={{dataObj.form_no}}&versionno={{dataObj.version_no}}&effectivedate={{dataObj.effective_date}}&retireddate={{dataObj.retired_date}}&headerid={{dataObj.headerid}}">
                                            Review & Approve
                                        </a>
                                    </div>
                                </div>


                            </td>
                        </tr>





                    </tbody>
                </table>
            </div>
            <div class="row" ng-show="inProgressActivityData.length == 0">
                <div class="col-sm-12 text-center">
                    <h2>No Activity Found</h2>
                </div>
            </div>

        </div>


    </section>
    <!-- Pending for Approval end-->
    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>

<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script>
                var app = angular.module("inProgressApp", ['blockUI']);
                app.controller("inProgressCtrl", function ($scope, $http, $filter, blockUI) {
                   //$scope.terminal_id = "103.211.15.1";
                    $scope.terminal_id='<?php echo $_SERVER['REMOTE_ADDR']?>';
                    $scope.employeRole = [];
                    $scope.getEmployeRoleList = function () {
                        $http({
                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getEmployeRoleList',
                            method: "GET",
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        }).then(function (response) {
                            $scope.employeRole = response.data.role_data;
                        }, function (error) { // optional

                            console.log("Something went wrong.Please try again");
                        });
                    }
                    $scope.getEmployeRoleList();
                    $scope.getRoleName = function (user_id) {
                        var role = $filter('filter')($scope.employeRole, {id: user_id})[0].role_description;
                        return role;
                    }
                    $scope.productData = [];
                    $scope.GetProductList = function () {
                        $http({
                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                            method: "GET",
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        }).then(function (response) {
                            $scope.productData = response.data.product_list;
                        }, function (error) { // optional
                            console.log("Something went wrong.Please try again");
                        });
                    }
                    $scope.GetProductList();
                    $scope.getProductDesc = function (product_code) {
                        var desc = $filter('filter')($scope.productData, {product_code: product_code})[0].product_name;
                        return desc;
                    }
                    $scope.roleList = [];
                    $scope.getRoleList = function () {
                        $http({
                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList',
                            method: "GET",
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        }).then(function (response) {
                            $scope.roleList = response.data.role_list;
                        }, function (error) { // optional

                            console.log("Something went wrong.Please try again");
                        });
                    }
                    $scope.getRoleList();
                    $scope.getNextApproval = function (dataObj, index, nextstep) {

                        if (dataObj.activity_level.length > 0) {
                            var desc = $filter('filter')(dataObj.activity_level, {status_id: nextstep})[0].role_id;
                            var role = $filter('filter')($scope.roleList, {id: desc})[0].role_description;
//                        return desc;
                            return role;
                        }
//                        
                    }
                    $scope.inProgressActivityData = [];
                    $scope.getInprogressActivityList = function () {
                        $http({
                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetInprogressActivityList?terminal_id='+$scope.terminal_id,
                            method: "GET",
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        }).then(function (response) {
                            $scope.inProgressActivityData = response.data.in_progress_activity;
                        }, function (error) { // optional

                            console.log("Something went wrong.Please try again");
                        });
                    }
                    $scope.getInprogressActivityList();
                    $scope.activityList = [];
                    $scope.getMstActivityList = function () {
                        $http({
                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetMstActivityList',
                            method: "GET",
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        }).then(function (response) {
                            $scope.activityList = response.data.mst_act_list;
                        }, function (error) { // optional

                            console.log("Something went wrong.Please try again");
                        });
                    }
                    $scope.getMstActivityList();
                    $scope.masterActivityData = [];
                    $scope.getMasterActivityList = function () {
                        $http({
                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getmasteractivitylist?activity_type=home',
                            method: "GET",
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        }).then(function (response) {
                            $scope.masterActivityData = response.data.master_activity_list;
                        }, function (error) { // optional
                            console.log("Something went wrong.Please try again");

                        });
                    }
                    $scope.getMasterActivityList();
                    $scope.actdataobj = {};
                    $scope.btntext = "";
                    $scope.setActdata = function (obj, btntext)
                    {
                        $scope.actdataobj = obj;
                        $scope.btntext = btntext;
                    }
                    //Login Function 
                    //Created by Bhupendra 
                    //Date : 28/01/2020                                    
                    $scope.login = function () {
                        var actname = "";
                        var actid = $filter('filter')($scope.activityList, {activity_name: $scope.actdataobj.activity_name})[0].id;
                        if (actid == 16 || actid == 17 || actid == 30) {
                            var actname = $filter('filter')($scope.masterActivityData, {id: $scope.actdataobj.activity_id})[0].activity_name;
                        }
                        var response = confirm("Do you really want to perform this task?");
                        if (response == true)
                        {
                            pwd2 = SHA256($scope.password);
                            $http({
                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                method: "POST",
                                data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&module_id=" + actid + "&module_type=log" + "&action=" + $scope.btntext + "&act_type=" + actname + "&room_code=" + $scope.actdataobj.room_code + "&auth=" + btoa(btoa($scope.password)),
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            }).then(function (response) {
                                if (response.data.message == "")
                                {

                                    if ($scope.btntext == 'approval')
                                    {
                                        //Calling Approval function
                                        //Created by Bhupendra 
                                        //Date : 28/01/2020
                                        var actstatus = "";
                                        if ($scope.actdataobj.activity_stop != "" && $scope.actdataobj.activity_stop != null)
                                        {
                                            actstatus = 'stop';
                                        } else {
                                            actstatus = 'start';
                                        }

                                        $scope.getapproved($scope.actdataobj.room_code, $scope.actdataobj.act_id, $scope.actdataobj.activity_id, $scope.actdataobj.workflowstatus, $scope.actdataobj.workflownextstep, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, actstatus)
                                        //alert($scope.actdataobj.activity_stop);
                                        //alert(actstatus);

                                    } else if ($scope.btntext == 'stop')
                                    {
                                        //Calling Stoped function
                                        //Created by Bhupendra 
                                        //Date : 29/01/2020
                                        $scope.getstoped($scope.actdataobj.room_code, $scope.actdataobj.act_id, $scope.actdataobj.activity_id, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark)
                                    } else if ($scope.btntext == 'takeover')
                                    {
                                        $scope.gettakeover($scope.actdataobj.room_code, $scope.actdataobj.act_id, $scope.actdataobj.activity_id, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark)
                                    }
                                } else {
                                    alert(response.data.message);
                                }
                            }, function (error) {
                                console.log(error);
                            });
                        }
                    }
                    //End Login Function

                    //Approval function :- To Approve running activity by auth & privilege chk as well as sent that activity to next approval which exists in workflow
                    //Created by Bhupendra 
                    //Date : 28/01/2020
                    $scope.getapproved = function (roomcode, actlogtblid, actid, workflowstatus, workflownextstep, roleid, empid, empname, email, remark, actstatus) {
                        $http({
                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getapproved?curr_url=inprogress_activity',
                            method: "POST",
                            data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&workflowstatus=" + workflowstatus + "&workflownextstep=" + workflownextstep + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&actstatus=" + actstatus,
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        }).then(function (response) {
                            alert(response.data.message);
                            $scope.getInprogressActivityList();
                            $scope.resetLoginForm();
                            angular.element("#btnclose").trigger('click');
                        }, function (error) {
                            console.log(error);
                        });
                    }
                    //*********************************************************************************************

                    //Stoped function :- To Stop running activity by auth chk.
                    //Created by Bhupendra 
                    //Date : 28/01/2020
                    $scope.getstoped = function (roomcode, actlogtblid, actid, roleid, empid, empname, email, remark) {
                        $http({
                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getstoped?curr_url=inprogress_activity',
                            method: "POST",
                            data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark,
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        }).then(function (response) {
                            alert(response.data.message);
                            $scope.getInprogressActivityList();
                            $scope.resetLoginForm();
                            angular.element("#btnclose").trigger('click');
                        }, function (error) {
                            console.log(error);
                        });
                    }
                    //*********************************************************************************************

                    //Takeover function :- To Takeover running activity by auth chk.
                    //Created by Bhupendra 
                    //Date : 28/01/2020
                    $scope.gettakeover = function (actlogtblid, actid, roleid, empid, empname, email, remark) {
                        $http({
                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/gettakeover?curr_url=inprogress_activity',
                            method: "POST",
                            data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark,
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        }).then(function (response) {
                            alert(response.data.message);
                            $scope.getInprogressActivityList();
                            $scope.resetLoginForm();
                            angular.element("#btnclose").trigger('click');
                        }, function (error) {
                            console.log(error);
                        });
                    }
                    //*********************************************************************************************
                    //Reset Login Form
                    $scope.resetLoginForm = function () {
                        $scope.username = "";
                        $scope.password = "";
                        $scope.remark = "";
                    }
                });
</script>
