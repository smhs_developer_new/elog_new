<div ng-app="airFilterApp" ng-controller="airFilterCtrl" ng-cloak>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>sf_home">Home</a></li>
            <li class="breadcrumb-item"><a href="#">{{room_code_show}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Return air filter cleaning record</li>
        </ol>
    </nav>
    <section class="contentBox blueBorder formWrapper">
        <div class="contentHeader">
            <div class="row">
                <div class="col-12">
                    <h1>Header Details</h1>
                </div>
            </div>
        </div>
        <div class="contentBody">
            <form>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <ul class="activityList">
                                <li style="margin-left: 100px;"><a><b>Form No : </b>{{formno}} </a></li>
                                <li style="margin-left: 100px;"><a><b>Version No : </b>{{versionno}} </a></li>
                                <li style="margin-left: 100px;"><a><b>Effective Date : </b>{{effectivedate| date:'dd-MMM-yyyy'}} </a></li>
                                <li style="margin-left: 100px;"><a><b>Retired Date : </b>{{retireddate| date:'dd-MMM-yyyy'}} </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <section class="contentBox blueBorder formWrapper">


        <div class="contentHeader">

            <div class="row">
                <div class="col-12">
                    <h1>Return Air Filter Cleaning Record</h1>
                </div>
            </div>

        </div>

        <div class="contentBody" ng-show="roomFilterData.length == 0 || edit == true || (roomFilterData[0]['activity_start'] > 0 && roomFilterData[0]['activity_stop'] == null)">
            <form name="airForm" novalidate>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Document No.</label>
                            <input type="text" class="form-control" value="{{docno}}" readonly="">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="frequencyField"><span class="asterisk">* </span> Frequency</label>
                            <select class="form-control" ng-model="frequency" ng-disabled="roomFilterData[0]['activity_start'] > 0 && edit == false" ng-change="selectFrequency(frequency)" required>
                                <option value="" >Select Frequency</option>
                                <option value="{{dataObj.id}}" ng-repeat="dataObj in frequencyList">{{dataObj.frequency_name}}</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="row" ng-show="room_type == 'room' || room_type == 'IPQA'">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="batchField"><span class="asterisk">* </span> Batch No.</label>
                            <p ng-show="batchInProductData.length > 0 && edit == false"><b>{{batch_no}}</b></p>
                            <input ng-show="batchInProductData.length == 0 || edit == true" maxlength="10" class="form-control" ng-disabled="roomFilterData[0]['activity_start'] > 0 && edit == false"  type="text" placeholder="Batch No." name="batch_no" ng-model="batch_no" >   
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="productField"><span class="asterisk">* </span>Product No.</label>
                            <div  ng-show="batchInProductData.length == 0 || edit == true">
                                <select class="chosen form-control" tabindex="4" ng-disabled="roomFilterData[0]['activity_start'] > 0 && edit == false"   name="product_code"  data-placeholder="Search Product No." ng-change="getProductDetail(product_no)" ng-options="dataObj['product_code'] as dataObj.product_code for dataObj in productData"  ng-model="product_no" chosen>
                                    <option value="NA">Select Product No</option>
                                </select>
                                <!-- <small class="form-text text-muted">
                                    Desmopressin Acetate Nasal solution, 0.01 (Nasal spray), 5ml/vial
                                </small> -->
                            </div>
                            <p><b ng-show="batchInProductData.length > 0 && edit == false">{{product_no}}</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b ng-if="product_desc != ''">{{product_desc}}</b></p>






                        </div>
                    </div>
                </div>
                <div class="row" ng-show="room_type == 'corridor'">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="batchField"><span class="asterisk">* </span> Batch No.</label>
                            <input class="form-control" type="text" placeholder="Batch No." name="batch_no" ng-model="batch_no" disabled>   
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="productField"><span class="asterisk">* </span>Product No.{{product_no}}</label>
                            <div>
                                <input class="form-control" type="text" placeholder="Product No." name="product_code" ng-model="product_no" disabled>   

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" ng-show="roomFilterData.length == 0">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <button ng-show="!edit && (room_type == 'room' || room_type=='IPQA')" ng-disabled="frequency == '' || product_no == 'NA'" data-toggle="modal"  data-target="#loginModal" ng-click="setbtntext('submit');" class="button smallBtn primaryBtn">Activity Start</button> &nbsp;&nbsp;&nbsp;&nbsp;
                            <button ng-show="!edit && room_type == 'corridor'" ng-disabled="frequency == '' || product_no == ''" data-toggle="modal"  data-target="#loginModal" ng-click="setbtntext('submit');" class="button smallBtn primaryBtn">Activity Start</button> &nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                </div>


                <div class="row"  ng-show="edit == false">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="filter"><span class="asterisk">* </span>Filter.</label>                                
                            <div class="col-sm-12" ng-if="room_type == 'corridor'">
                                <div class="contentBody">
                                    <ul class="eqplistWrap">
                                        <li>
                                            <div class="row eqpItemWrap">
                                                <div class="col-sm-9 align-self-center">
                                                    <div class="eqpName">
                                                        Select All
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 align-self-center">
                                                    <div class="eqpControl">
                                                        Cleaning &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Replace
                                                        <!--                                                        <label class="switch">
                                                                                                                    <input type="checkbox"  id="selectall" name="selectall" ng-model="selectall" class="toggle-task ng-valid ng-dirty ng-touched" ng-click="SelectAllFilets()">
                                                                                                                    <span class="slider round"></span>
                                                                                                                </label>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="eqplistWrap">
                                        <li ng-repeat="dataObj in filterList">
                                            <div class="row eqpItemWrap">
                                                <div class="col-sm-9 align-self-center">
                                                    <div class="eqpName">
                                                        {{dataObj.filter_name}}
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 align-self-center">
                                                    <div class="eqpControl" id="chkboxs">
                                                        <label class="switch">
                                                            <input type="checkbox" ng-disabled="selectall"  class="toggle-task ng-valid ng-dirty ng-touched" ng-model="dataObj.ischecked" id="check{{dataObj.id}}" ng-click="selectFilter(dataObj, $index)" name="filter" >
                                                            <span class="slider round"></span>
                                                        </label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <label class="switch">
                                                            <input type="checkbox"  class="toggle-task ng-valid ng-dirty ng-touched" ng-model="dataObj.isreplace" id="replace{{dataObj.id}}" ng-click="selectReplace(dataObj, $index)" name="replace" >
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-12" ng-if="room_type != 'corridor'">
                                <div class="contentBody">
                                    <ul class="eqplistWrap">
                                        <li>
                                            <div class="row eqpItemWrap">
                                                <div class="col-sm-9 align-self-center">
                                                    <div class="eqpName">
                                                        Select All
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 align-self-center">
                                                    <div class="eqpControl">
                                                        Cleaning &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Replace
                                                        <!--                                                        <label class="switch">
                                                                                                                    <input type="checkbox"  id="selectall" name="selectall" ng-model="selectall" class="toggle-task ng-valid ng-dirty ng-touched" ng-click="SelectAllFilets()">
                                                                                                                    <span class="slider round"></span>
                                                                                                                </label>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="eqplistWrap">
                                        <li ng-repeat="dataObj in filterList">
                                            <div class="row eqpItemWrap">
                                                <div class="col-sm-9 align-self-center">
                                                    <div class="eqpName">
                                                        {{dataObj.filter_name}}
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 align-self-center">
                                                    <div class="eqpControl" id="chkboxs">
                                                        <label class="switch">
                                                            <input type="checkbox"  class="toggle-task ng-valid ng-dirty ng-touched" ng-model="dataObj.ischecked" id="check{{dataObj.id}}" ng-click="selectFilter(dataObj, $index)" name="filter" >
                                                            <span class="slider round"></span>
                                                        </label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <label class="switch">
                                                            <input type="checkbox"  class="toggle-task ng-valid ng-dirty ng-touched" ng-model="dataObj.isreplace" id="replace{{dataObj.id}}" ng-click="selectReplace(dataObj, $index)" name="replace" >
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <!-- for edit time -->
                <div class="row" ng-show="edit == true">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="filter"><span class="asterisk">* </span>Filter.</label>                                
                            <div class="col-sm-12" ng-if="room_type == 'corridor'">
                                <div class="contentBody">
                                    <ul class="eqplistWrap">
                                        <li>
                                            <div class="row eqpItemWrap">
                                                <div class="col-sm-9 align-self-center">
                                                    <div class="eqpName">
                                                        Select All
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 align-self-center">
                                                    <div class="eqpControl">
                                                        Cleaningnbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Replace
                                                        <!--                                                        <label class="switch">
                                                                                                                    <input type="checkbox"  id="selectall" name="selectall" ng-model="selectall" class="toggle-task ng-valid ng-dirty ng-touched" ng-click="SelectAllFilets()">
                                                                                                                    <span class="slider round"></span>
                                                                                                                </label>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="eqplistWrap">
                                        <li ng-repeat="dataObj in filterList">
                                            <div class="row eqpItemWrap">
                                                <div class="col-sm-9 align-self-center">
                                                    <div class="eqpName">
                                                        {{dataObj.filter_name}}
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 align-self-center">
                                                    <div class="eqpControl" id="chkboxs">
                                                        <label class="switch">
                                                            <input type="checkbox" disabled  class="toggle-task ng-valid ng-dirty ng-touched" ng-model="dataObj.ischecked" id="check{{dataObj.id}}" ng-click="selectFilter(dataObj, $index)" name="filter" >
                                                            <span class="slider round"></span>
                                                        </label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <label class="switch">
                                                            <input type="checkbox" disabled class="toggle-task ng-valid ng-dirty ng-touched" ng-model="dataObj.isreplace" id="replace{{dataObj.id}}" ng-click="selectReplace(dataObj, $index)" name="replace" >
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-12" ng-if="room_type != 'corridor'">
                                <div class="contentBody">
                                    <ul class="eqplistWrap">
                                        <li>
                                            <div class="row eqpItemWrap">
                                                <div class="col-sm-9 align-self-center">
                                                    <div class="eqpName">
                                                        Select All
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 align-self-center">
                                                    <div class="eqpControl">
                                                        Cleaning &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Replace
                                                        <!--                                                        <label class="switch">
                                                                                                                    <input type="checkbox"  id="selectall" name="selectall" ng-model="selectall" class="toggle-task ng-valid ng-dirty ng-touched" ng-click="SelectAllFilets()">
                                                                                                                    <span class="slider round"></span>
                                                                                                                </label>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <ul class="eqplistWrap">
                                        <li ng-repeat="dataObj in filterList">
                                            <div class="row eqpItemWrap">
                                                <div class="col-sm-9 align-self-center">
                                                    <div class="eqpName">
                                                        {{dataObj.filter_name}}
                                                    </div>
                                                </div>
                                                <div class="col-sm-3 align-self-center">
                                                    <div class="eqpControl" id="chkboxs">
                                                        <label class="switch">
                                                            <input type="checkbox" disabled  class="toggle-task ng-valid ng-dirty ng-touched" ng-model="dataObj.ischecked" id="check{{dataObj.id}}" ng-click="selectFilter(dataObj, $index)" name="filter" >
                                                            <span class="slider round"></span>
                                                        </label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <label class="switch">
                                                            <input type="checkbox" disabled  class="toggle-task ng-valid ng-dirty ng-touched" ng-model="dataObj.isreplace" id="replace{{dataObj.id}}" ng-click="selectReplace(dataObj, $index)" name="replace" >
                                                            <span class="slider round"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!--                    <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="filter"><span class="asterisk">* </span>Filter.</label>                                
                                                <div class="col-sm-12" ng-if="room_type == 'corridor'">
                                                    <div class="contentBody">
                                                        <ul class="eqplistWrap">
                                                            <li>
                                                                <div class="row eqpItemWrap">
                                                                    <div class="col-sm-9 align-self-center">
                                                                        <div class="eqpName">
                                                                            Select All
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3 align-self-center">
                                                                        <div class="eqpControl">
                                                                            <label class="switch">
                                                                                <input type="checkbox" disabled id="Eselectall" name="selectall" ng-model="selectall" class="toggle-task ng-valid ng-dirty ng-touched">
                                                                                <span class="slider round"></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                        <ul class="eqplistWrap">
                                                            <li ng-repeat="dataObj in filterList">
                                                                <div class="row eqpItemWrap">
                                                                    <div class="col-sm-9 align-self-center">
                                                                        <div class="eqpName">
                                                                            {{dataObj.filter_name}}
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3 align-self-center">
                                                                        <div class="eqpControl" id="chkboxs">
                                                                            <label class="switch">
                                                                                <input type="checkbox" disabled ng-disabled="selectall"  class="toggle-task ng-valid ng-dirty ng-touched" ng-model="dataObj.ischecked" id="check{{dataObj.id}}" name="filter" >
                                                                                <span class="slider round"></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12" ng-if="room_type != 'corridor'">
                                                    <div class="contentBody">
                                                        <ul class="eqplistWrap">
                                                            <li>
                                                                <div class="row eqpItemWrap">
                                                                    <div class="col-sm-9 align-self-center">
                                                                        <div class="eqpName">
                                                                            Select All
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3 align-self-center">
                                                                        <div class="eqpControl">
                                                                            <label class="switch">
                                                                                <input type="checkbox" disabled checked id="Eselectall" name="selectall" ng-model="selectall" class="toggle-task ng-valid ng-dirty ng-touched">
                                                                                <span class="slider round"></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                        <ul class="eqplistWrap">
                                                            <li ng-repeat="dataObj in filterList">
                                                                <div class="row eqpItemWrap">
                                                                    <div class="col-sm-9 align-self-center">
                                                                        <div class="eqpName">
                                                                            {{dataObj.filter_name}}
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3 align-self-center">
                                                                        <div class="eqpControl" id="chkboxs">
                                                                            <label class="switch">
                                                                                <input type="checkbox" disabled class="toggle-task ng-valid ng-dirty ng-touched" ng-model="dataObj.ischecked" id="check{{dataObj.id}}" name="filter" >
                                                                                <span class="slider round"></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                </div>
                <!-- End -->

                <div class="row">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <div  ng-if="airForm.$invalid">
                                <button ng-show="edit" class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancel</button>&nbsp;&nbsp;&nbsp;
                                <button ng-show="edit" ng-click="setbtntext('update');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Update</button>&nbsp;&nbsp;&nbsp;
                                <button  ng-show="!edit" ng-disabled="selectedReplace.length == 0 && selectedFilter.length == 0 || !(roomFilterData[0]['activity_start'])" data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('stop');" class="button smallBtn primaryBtn">Activity Stop</button>
                                <button  ng-show="!edit" ng-disabled="roomFilterData[0]['activity_stop'] == null" data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('approval');" class="button smallBtn primaryBtn">Activity Approve</button>
                            </div>
                            <div  ng-if="!airForm.$invalid">
                                <button ng-show="edit" class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancel</button>&nbsp;&nbsp;&nbsp;
                                <button ng-show="edit" ng-click="setbtntext('update');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Update</button>&nbsp;&nbsp;&nbsp;
                                <button ng-show="!edit" ng-disabled="selectedReplace.length == 0 && selectedFilter.length == 0 || !(roomFilterData[0]['activity_start'])" data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('stop');" class="button smallBtn primaryBtn">Activity Stop</button>
                                <button ng-show="!edit" ng-disabled="roomFilterData[0]['activity_stop'] == null" data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('approval');" class="button smallBtn primaryBtn">Activity Approve</button>
                            </div>

                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="contentBody" ng-show="roomFilterData.length > 0 && edit == false && roomFilterData[0]['activity_stop'] > 0">

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Document No.</label>
                        <input type="text" class="form-control" value="{{roomFilterData[0].doc_no}}" readonly="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="frequencyField"><span class="asterisk">* </span> Frequency</label>
                        <input type="text" class="form-control" value="{{frequency_name}}" readonly="">
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="batchField"><span class="asterisk">* </span> Batch No.</label>
                        <input type="text" class="form-control" value="{{roomFilterData[0].batch_no}}" readonly="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="productField"><span class="asterisk">* </span>Product No.</label>
                        <input type="text" class="form-control" value="{{roomFilterData[0].product_code}}" readonly="">



                    </div>
                </div>
            </div>


            <!--            <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="filter"><span class="asterisk">* </span>Filter.</label>   
                                    <input type="text" class="form-control" value="{{roomFilterData[0].filter}} " readonly="">
            
                                </div>
                            </div>
                        </div>-->
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <!--<label for="filter"><span class="asterisk">* </span>Filter.</label>-->
                        <!--                        <div class="col-sm-12">
                                                    <div class="contentBody">
                                                        <ul class="eqplistWrap" ng-if="room_type != 'corridor'">
                                                            <li>
                                                                <div class="row eqpItemWrap">
                                                                    <div class="col-sm-9 align-self-center">
                                                                        <div class="eqpName">
                                                                            Select All
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3 align-self-center">
                                                                        <div class="eqpControl">
                                                                            <label class="switch">
                                                                                <input type="checkbox" disabled   name="selectall" class="toggle-task ng-valid ng-dirty ng-touched" checked >
                                                                                <span class="slider round"></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                        <ul class="eqplistWrap" ng-if="room_type == 'corridor'">
                                                            <li>
                                                                <div class="row eqpItemWrap">
                                                                    <div class="col-sm-9 align-self-center">
                                                                        <div class="eqpName">
                                                                            Select All
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3 align-self-center">
                                                                        <div class="eqpControl">
                                                                            <label class="switch">
                                                                                <input ng-if="!showSelect" type="checkbox" disabled  name="selectall" class="toggle-task ng-valid ng-dirty ng-touched" >
                                                                                <input ng-if="showSelect" checked disabled type="checkbox"  name="selectall" class="toggle-task ng-valid ng-dirty ng-touched" >
                                                                                <span class="slider round"></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                        <ul class="eqplistWrap">
                                                            <li ng-repeat="dataObj in filterList" >
                                                                <div class="row eqpItemWrap">
                                                                    <div class="col-sm-9 align-self-center">
                                                                        <div class="eqpName">
                                                                            {{dataObj.filter_name}}
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-3 align-self-center" >
                                                                        <div class="eqpControl" id="chkboxs">
                                                                            <label class="switch" ng-if="room_type != 'corridor'">
                                                                                <input type="checkbox" checked disabled  class="toggle-task ng-valid ng-dirty ng-touched"  id="check{{$index}}" name="filter" >
                                                                                <span class="slider round"></span>
                                                                            </label>
                                                                            <label class="switch" ng-if="room_type == 'corridor' && showSelect">
                                                                                <input type="checkbox" checked disabled class="toggle-task ng-valid ng-dirty ng-touched"  id="check{{$index}}" name="filter" >
                                                                                <span class="slider round"></span>
                                                                            </label>
                                                                            <label class="switch" ng-if="room_type == 'corridor' && !showSelect">
                                                                                <input type="checkbox" disabled checked ng-if="!(roomFilterData[0].filter_array.indexOf(dataObj.filter_name) === -1)"  class="toggle-task ng-valid ng-dirty ng-touched"  id="check{{$index}}" name="filter" >
                                                                                <span class="slider round"></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>-->
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="filter"><span class="asterisk">* </span>Filter.</label>                                
                                <div class="col-sm-12" ng-if="room_type == 'corridor'">
                                    <div class="contentBody">
                                        <ul class="eqplistWrap">
                                            <li>
                                                <div class="row eqpItemWrap">
                                                    <div class="col-sm-9 align-self-center">
                                                        <div class="eqpName">
                                                            Select All
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 align-self-center">
                                                        <div class="eqpControl">
                                                            Cleaning &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Replace
                                                            <!--                                                        <label class="switch">
                                                                                                                        <input type="checkbox"  id="selectall" name="selectall" ng-model="selectall" class="toggle-task ng-valid ng-dirty ng-touched" ng-click="SelectAllFilets()">
                                                                                                                        <span class="slider round"></span>
                                                                                                                    </label>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="eqplistWrap">
                                            <li ng-repeat="dataObj in filterList">
                                                <div class="row eqpItemWrap">
                                                    <div class="col-sm-9 align-self-center">
                                                        <div class="eqpName">
                                                            {{dataObj.filter_name}}
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 align-self-center">
                                                        <div class="eqpControl" id="chkboxs">
                                                            <label class="switch">
                                                                <input type="checkbox" disabled  class="toggle-task ng-valid ng-dirty ng-touched" ng-model="dataObj.ischecked" id="check{{dataObj.id}}" ng-click="selectFilter(dataObj, $index)" name="filter" >
                                                                <span class="slider round"></span>
                                                            </label>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <label class="switch">
                                                                <input type="checkbox" disabled class="toggle-task ng-valid ng-dirty ng-touched" ng-model="dataObj.isreplace" id="replace{{dataObj.id}}" ng-click="selectReplace(dataObj, $index)" name="replace" >
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-12" ng-if="room_type != 'corridor'">
                                    <div class="contentBody">
                                        <ul class="eqplistWrap">
                                            <li>
                                                <div class="row eqpItemWrap">
                                                    <div class="col-sm-9 align-self-center">
                                                        <div class="eqpName">
                                                            Select All
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 align-self-center">
                                                        <div class="eqpControl">
                                                            Cleaning &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Replace
                                                            <!--                                                        <label class="switch">
                                                                                                                        <input type="checkbox"  id="selectall" name="selectall" ng-model="selectall" class="toggle-task ng-valid ng-dirty ng-touched" ng-click="SelectAllFilets()">
                                                                                                                        <span class="slider round"></span>
                                                                                                                    </label>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                        <ul class="eqplistWrap">
                                            <li ng-repeat="dataObj in filterList">
                                                <div class="row eqpItemWrap">
                                                    <div class="col-sm-9 align-self-center">
                                                        <div class="eqpName">
                                                            {{dataObj.filter_name}}
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 align-self-center">
                                                        <div class="eqpControl" id="chkboxs">
                                                            <label class="switch">
                                                                <input type="checkbox" disabled class="toggle-task ng-valid ng-dirty ng-touched" ng-model="dataObj.ischecked" id="check{{dataObj.id}}" ng-click="selectFilter(dataObj, $index)" name="filter" >
                                                                <span class="slider round"></span>
                                                            </label>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <label class="switch">
                                                                <input type="checkbox" disabled class="toggle-task ng-valid ng-dirty ng-touched" ng-model="dataObj.isreplace" id="replace{{dataObj.id}}" ng-click="selectReplace(dataObj, $index)" name="replace" >
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-sm-12 text-right">
                    <div class="formBtnWrap">
                        <button ng-click="editTrasaction()" class="button smallBtn primaryBtn" >Edit</button>&nbsp;&nbsp;&nbsp;
                        <button  data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('approval');" class="button smallBtn primaryBtn">Activity Approve</button>
                    </div>
                </div>
            </div>

        </div>


    </section>
    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>js/lodash.min.js"></script>
<script src="<?php echo base_url() ?>js/angularjs-dropdown-multiselect.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script type="text/javascript">
                            var app = angular.module("airFilterApp", ['angular.chosen', 'angularjs-dropdown-multiselect', 'blockUI']);
                            app.directive('chosen', function ($timeout) {

                                var linker = function (scope, element, attr) {

                                    scope.$watch('productData', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    scope.$watch('frequencyList', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    $timeout(function () {
                                        element.chosen();
                                    }, 0, false);
                                };
                                return {
                                    restrict: 'A',
                                    link: linker
                                };
                            });
                            app.filter('contains', function () {
                                return function (array, needle) {
                                    return array.indexOf(needle) >= 0;
                                };
                            });
                            app.controller("airFilterCtrl", function ($scope, $http, $filter, blockUI) {
                                var urlParams = new URLSearchParams(window.location.search);
                                var myParam = urlParams.get('room_code');
                                var processid = urlParams.get('processid');
                                var actid = urlParams.get('actid');
                                var actname = urlParams.get('actname');

                                // get header detail 
                                $scope.headerRecordid = urlParams.get('headerid');
                                $scope.formno = urlParams.get('formno');
                                $scope.versionno = urlParams.get('versionno');
                                $scope.effectivedate = urlParams.get('effectivedate');
                                $scope.retireddate = urlParams.get('retireddate');
                                //end

                                $scope.room_code_show = myParam;
                                $scope.batchInProductData = [];
                                $scope.frequency = "";
                                $scope.batch_no = "NA";
                                $scope.product_no = "NA";
                                $scope.showBatch = false;
                                $scope.selectFrequency = function (obj) {
                                    if (obj == undefined) {
                                        $scope.frequency = "";
                                    }
                                }
                                $scope.roleList = [];
                                $scope.getRoleList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.roleList = response.data.role_list;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getRoleList();
                                $scope.getRoomBatchInProductData = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getRoomBatchInProductData?room_code=' + myParam,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.batchInProductData = response.data.batch_product_list;
                                        if ($scope.batchInProductData.length > 0) {
                                            $scope.batch_no = $scope.batchInProductData[0].batch_no;
                                            $scope.product_no = $scope.batchInProductData[0].product_code;
                                            $scope.product_desc = $scope.batchInProductData[0].product_desc;
                                            $scope.showBatch = true;
                                        }
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.getRoomBatchInProductData();
                                $scope.room_type = urlParams.get('room_type');
                                $scope.isShow = true;
                                if ($scope.room_type == 'room' || $scope.room_type == 'IPQA') {
                                    $scope.isShow = false;

                                } else {
                                    $scope.batch_no = 'NA';
                                    $scope.product_no = 'NA';
                                }
                                $scope.frequencyList = [];
                                var frequency_name = '';
                                //Get Document Number code
                                $scope.docno = "";

                                // $scope.getdocno = function () {
                                //     $http({
                                //         url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getdocno',
                                //         method: "GET",
                                //         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                //     }).then(function (response) {
                                //         $scope.docno = response.data.docno;
                                //     }, function (error) { // optional

                                //         console.log("Something went wrong.Please try again");
                                //     });
                                // }
                                // $scope.getdocno();
                                //End of Get Document number function code
                                $scope.getfrequencyData = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetFrequencyList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.frequencyList = response.data.frequency_list;
                                        $scope.getLastFilterActivity();
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getfrequencyData();

                                $scope.filterList = [];
                                $scope.getRoomWiseFilterData = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoomWiseFilterList?room_code=' + myParam,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.filterList = response.data.filter_list;
                                        angular.forEach($scope.filterList, function (obj, key) {
                                            $scope.filterList[key]['selected'] = false;
                                        });
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getRoomWiseFilterData();

                                $scope.productData = [];
                                $scope.getProductList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.productData = response.data.product_list;
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.getProductList();
                                $scope.product_desc = "";
                                $scope.getProductDetail = function (dataObj) {
                                    $scope.productcode = dataObj;
                                    $scope.product_desc = $filter('filter')($scope.productData, {product_code: dataObj})[0].product_name;
                                }


                                $scope.selectedFilter = [];
                                $scope.setting2 = {
                                    scrollableHeight: '200px',
                                    scrollable: true,
                                    enableSearch: false
                                };
                                $scope.example2settings = {
                                    displayProp: 'id'
                                };


//*******************Bhupendra's code Started**********************//

                                $scope.setbtntext = function (btntext)
                                {
                                    $scope.btntext = btntext;
                                }
                                $scope.setFilterValue = function (dataObj) {
                                    $scope.selectedFilter = [];
                                    $scope.selectedFilter.push(dataObj);
                                }
                                $scope.selectedFilter = [];
                                $scope.selectFilter = function (dataObj, index) {
                                    $("#selectall").prop('checked', false);
                                    if (document.getElementById("check" + dataObj.id).checked) {
                                        $scope.selectedFilter.push(dataObj.filter_name);
                                        $scope.filterList[index]['isreplace'] = false;
                                        if ($scope.selectedReplace.includes(dataObj.filter_name)) {
                                            removeA($scope.selectedReplace, dataObj.filter_name);
                                        }
                                        //$scope.selectedReplace.splice($scope.selectedReplace.indexOf(dataObj.filter_name), 1);
                                    } else {
                                        $scope.selectedFilter.splice($scope.selectedFilter.indexOf(dataObj.filter_name), 1);
                                        //$scope.selectedFilter.slice(dataObj.filter_name);
                                    }
                                }
                                $scope.selectedReplace = [];
                                $scope.selectReplace = function (dataObj, index) {
                                    $("#selectall").prop('checked', false);
                                    if (document.getElementById("replace" + dataObj.id).checked) {
                                        $scope.filterList[index]['ischecked'] = false;
                                        $scope.selectedReplace.push(dataObj.filter_name);
                                        if ($scope.selectedFilter.includes(dataObj.filter_name)) {
                                            removeA($scope.selectedFilter, dataObj.filter_name);
                                        }
                                        //$scope.selectedFilter.splice($scope.selectedFilter.indexOf(dataObj.filter_name), 1);
                                    } else {
                                        $scope.selectedReplace.splice($scope.selectedReplace.indexOf(dataObj.filter_name), 1);
                                        //$scope.selectedFilter.slice(dataObj.filter_name);
                                    }
                                }
                                function removeA(arr) {
                                    var what, a = arguments, L = a.length, ax;
                                    while (L > 1 && arr.length) {
                                        what = a[--L];
                                        while ((ax = arr.indexOf(what)) !== -1) {
                                            arr.splice(ax, 1);
                                        }
                                    }
                                    return arr;
                                }

                                $scope.SelectAllFilets = function () {
                                    $scope.selectedFilter = [];
                                    if (document.getElementById("selectall").checked) {
                                        angular.forEach($scope.filterList, function (obj, index) {
                                            $scope.filterList[index]['ischecked'] = true;
                                            $("#check" + obj.id).prop('checked', true);
                                            $scope.selectedFilter.push(obj.filter_name);
                                        });

                                    } else {
                                        angular.forEach($scope.filterList, function (obj, index) {
                                            $scope.filterList[index]['ischecked'] = false;
                                            $("#check" + obj.id).prop('checked', false);
                                        });
                                        $scope.selectedFilter = [];
                                    }
                                }

                                $scope.edit = false;
                                //Login Function
                                //Created by Bhupendra
                                //Date : 28/01/2020                                    
                                $scope.login = function () {
                                    var response = confirm("Do you really want to perform this task?");
                                    if (response == true)
                                    {
                                        $('.loader').show();
                                        pwd2 = SHA256($scope.password);
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                            method: "POST",
                                            data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&module_id=" + actid + "&module_type=log" + "&action=" + $scope.btntext + "&auth=" + btoa(btoa($scope.password)) + "&act_id=" + actid,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            if (response.data.message == "")
                                            {

                                                var equipment_code = "";
                                                var selectedfilters = "";
                                                var selectedReplaces = "";
                                                var temp = [];
                                                angular.forEach($scope.selectedFilter, function (obj, index) {
                                                    temp.push(obj);
                                                });
                                                var selectedfilters = temp.toString();
                                                var tempr = [];
                                                angular.forEach($scope.selectedReplace, function (obj, index) {
                                                    tempr.push(obj);
                                                });
                                                var selectedReplaces = tempr.toString();

                                                if ($scope.btntext == 'approval')
                                                {
                                                    //Calling Approval function
                                                    //Created by Bhupendra
                                                    //Date : 10/02/2020
//                                                    alert($scope.roomFilterData[0].workflowstatus);return false;
                                                    $scope.getapproved(myParam, $scope.roomFilterData[0].id, $scope.roomFilterData[0].activity_id, $scope.roomFilterData[0].workflowstatus, $scope.roomFilterData[0].workflownextstep, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, 'stop', $scope.roomFilterData[0].doc_id)
                                                } else if ($scope.btntext == 'submit')
                                                {
                                                    //Calling Stoped function
                                                    //Created by Bhupendra
                                                    //Date : 10/02/2020
                                                    $scope.getsubmit(myParam, processid, actid, actname, $scope.docno, $scope.frequency, $scope.product_no, $scope.batch_no, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark);
                                                } else if ($scope.btntext == 'stop')
                                                {
                                                    if($scope.room_type=='room' || $scope.room_type=='IPQA'){
                                                        var roomFilter=$scope.selectedFilter.concat($scope.selectedReplace);
                                                        if($scope.filterList.length==roomFilter.length){
                                                            $scope.getstoped(myParam, $scope.roomFilterData[0].id, $scope.roomFilterData[0].activity_id, $scope.docno, $scope.frequency, $scope.product_no, $scope.batch_no, selectedfilters, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, selectedReplaces, $scope.roomFilterData[0].edit_id);
                                                        }else{
                                                            $('.loader').hide();
                                                            alert("Please select all filters");
                                                            return false;
                                                        }
                                                    }else{
                                                        $scope.getstoped(myParam, $scope.roomFilterData[0].id, $scope.roomFilterData[0].activity_id, $scope.docno, $scope.frequency, $scope.product_no, $scope.batch_no, selectedfilters, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, selectedReplaces, $scope.roomFilterData[0].edit_id);
                                                    }
                                                    
                                                } else if ($scope.btntext == 'edit')
                                                {
                                                    //edit Functionality
                                                    //Created by Rahul
                                                    //Date : 04/05/2020
//                                                    var userdata = response.data.userdata;
//                                                    if (userdata.role_id > $scope.roomFilterData[0].done_by_role_id) {
//                                                        $scope.edit = true;
//                                                        $scope.docno = $scope.roomFilterData[0].doc_no;
//                                                        $scope.batch_no = $scope.roomFilterData[0].batch_no;
//                                                        $scope.product_no = $scope.roomFilterData[0].product_code;
//                                                        $scope.getProductDetail($scope.product_no);
//                                                        $scope.frequency = $scope.roomFilterData[0].frequency_id;
//                                                        $scope.is_in_workflow = $scope.roomFilterData[0].is_in_workflow;
//                                                        var tempFilter = $scope.roomFilterData[0].filter.split(",");
//                                                        if (tempFilter.length == $scope.filterList.length) {
//
//                                                            $scope.selectedFilter = [];
//                                                            $("#Eselectall").prop('checked', true);
//                                                            angular.forEach($scope.filterList, function (obj, index) {
//                                                                $scope.filterList[index]['ischecked'] = true;
//                                                                $("#check" + obj.id).prop('checked', true);
//                                                                $scope.selectedFilter.push(obj.filter_name);
//                                                            });
//                                                        } else {
//                                                            angular.forEach(tempFilter, function (obj, index) {
//                                                                var temp = $filter('filter')($scope.filterList, {filter_name: obj})[0];
//                                                                $("#check" + temp.id).prop('checked', true);
//                                                                $scope.selectedFilter.push(obj);
//                                                            });
//                                                        }
//                                                    } else {
//                                                        alert("You are not allowed to edit this.")
//                                                        $scope.edit = false;
//                                                    }
//                                                    $scope.resetLoginForm();
//                                                    angular.element("#btnclose").trigger('click');
                                                } else if ($scope.btntext == 'update') {
                                                    //update Functionality
                                                    //Created by Rahul
                                                    //Date : 04/05/2020
                                                    var role = "";
                                                    if ($scope.roomFilterData[0].activity_level.length > 0 && $scope.roomFilterData[0].workflownextstep > 0 && $scope.roomFilterData[0].start_time != null && $scope.roomFilterData[0].stop_time == null) {
                                                        if ($scope.roomFilterData[0].start_time != null && $scope.roomFilterData[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }

                                                        var desc = $filter('filter')($scope.roomFilterData[0].activity_level, {status_id: $scope.roomFilterData[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    } else if ($scope.roomFilterData[0].activity_level.length > 0 && $scope.roomFilterData[0].workflownextstep == 0 && $scope.roomFilterData[0].start_time != null && $scope.roomFilterData[0].stop_time == null) {

                                                        var desc = $filter('filter')($scope.roomFilterData[0].activity_level, {status_id: $scope.roomFilterData[0].workflowstatus})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;

                                                    } else if ($scope.roomFilterData[0].activity_level.length > 0 && $scope.roomFilterData[0].workflownextstep > 0 && $scope.roomFilterData[0].start_time != null && $scope.roomFilterData[0].stop_time != null) {
                                                        if ($scope.roomFilterData[0].start_time != null && $scope.roomFilterData[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }
                                                        var desc = $filter('filter')($scope.roomFilterData[0].activity_level, {status_id: $scope.roomFilterData[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    }
//                                                  
                                                    var userdata = response.data.userdata;
                                                    if (userdata.role_id >= $scope.roomFilterData[0].last_approval.role_id && userdata.role_id > 1) {
                                                        $scope.updatesubmit(myParam, processid, actid, actname, $scope.docno, $scope.frequency, $scope.product_no, $scope.batch_no, selectedfilters, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.roomFilterData[0].edit_id, $scope.is_in_workflow, selectedReplaces);
                                                    } else {
                                                        $('.loader').hide();
                                                        alert("You are not allowed to edit this.")
                                                        $scope.edit = false;
                                                    }
                                                }
                                            } else {
                                                $('.loader').hide();
                                                alert(response.data.message);
                                            }
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                }
                                //End Login Function

                                //Approval function :- To Approve running activity by auth & privilege chk as well as sent that activity to next approval which exists in workflow
                                //Created by Bhupendra
                                //Date : 28/01/2020
                                $scope.getapproved = function (roomcode, actlogtblid, actid, workflowstatus, workflownextstep, roleid, empid, empname, email, remark, actstatus, docno) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/com_approval_fun',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&workflowstatus=" + workflowstatus + "&workflownextstep=" + workflownextstep + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&actstatus=" + actstatus + "&docno=" + docno + "&tblname=pts_trn_return_air_filter",

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        //$scope.getInprogressActivityList(actlogtblid);                  
                                        alert(response.data.message);
                                        if (response.data.next_step == "-1")
                                        {
                                            window.location.href = "<?php echo base_url() ?>home";
                                        } else
                                        {
                                            $scope.resetLoginForm();
                                            $scope.getLastFilterActivity();
                                            $scope.frequency = "";
                                            $scope.batch_no = "NA";
                                            $scope.product_no = "NA";
                                            $scope.product_desc = "";
                                            $scope.selectedFilter = [];
                                            angular.element("#btnclose").trigger('click');
                                            $("#selectall").prop('checked', false);
                                            angular.forEach($scope.filterList, function (obj, index) {
                                                $scope.filterList[index]['ischecked'] = false;
                                                $("#check" + obj.id).prop('checked', false);
                                            });
                                            $scope.selectedFilter = [];
                                            //$scope.getdocno();
                                            $scope.getRoomBatchInProductData();
                                            $('.loader').hide();
                                        }
                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }
                                //*********************************************************************************
                                //Start function :- To Start new activity by auth chk.
                                //Created by Bhupendra
                                //Date : 4/02/2020
                                $scope.getsubmit = function (roomcode, processid, actid, actname, docno, frequency, product_no, batch_no, roleid, empid, empname, email, remark) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/submit_return_air_filter',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&frequency=" + frequency + "&product_no=" + product_no + "&batch_no=" + batch_no + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&headerRecordid=" + $scope.headerRecordid,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        $scope.getRoomBatchInProductData();
                                        $scope.getLastFilterActivity();
                                        $scope.frequency = "";
                                        $scope.batch_no = "";
                                        $scope.product_no = "NA";
                                        $scope.product_desc = "";
                                        angular.element("#btnclose").trigger('click');
                                        $("#selectall").prop('checked', false);
                                        angular.forEach($scope.filterList, function (obj, index) {
                                            $scope.filterList[index]['ischecked'] = false;
                                            $("#check" + obj.id).prop('checked', false);
                                        });
                                        $scope.selectedFilter = [];
                                        //$scope.getInprogressActivityList(response.data.acttblid);                  
                                        angular.element("#btnclose").trigger('click');
                                        $('.loader').hide();
                                        //$scope.getdocno();
                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }
                                //*********************************************************************************************

                                $scope.getstoped = function (roomcode, actlogtblid, actid, docno, frequency, product_no, batch_no, selectedfilters, roleid, empid, empname, email, remark, selectedReplaces, stop_id) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getairfilterstopped',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&docno=" + docno + "&frequency=" + frequency + "&product_no=" + product_no + "&batch_no=" + batch_no + "&selectedfilters=" + selectedfilters + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&headerRecordid=" + $scope.headerRecordid + "&selectedReplaces=" + selectedReplaces + "&stop_id=" + stop_id,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        $scope.getRoomBatchInProductData();
                                        $scope.getLastFilterActivity();
                                        $scope.frequency = "";
                                        $scope.batch_no = "";
                                        $scope.product_no = "NA";
                                        $scope.product_desc = "";
                                        angular.element("#btnclose").trigger('click');
                                        $("#selectall").prop('checked', false);
                                        angular.forEach($scope.filterList, function (obj, index) {
                                            $scope.filterList[index]['ischecked'] = false;
                                            $("#check" + obj.id).prop('checked', false);
                                        });
                                        $scope.selectedFilter = [];
                                        $scope.selectedReplace = [];
                                        //$scope.getInprogressActivityList(response.data.acttblid);                  
                                        angular.element("#btnclose").trigger('click');
                                        $('.loader').hide();
                                        //$scope.getdocno();
                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }

                                //This function :- is used inprogress activity by last inserted id.
                                //Created by Bhupendra
                                //Date : 4/02/2020
                                $scope.inProgressActivityData = [];
                                $scope.getInprogressActivityList = function (actid) {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetInprogressActivityList?actid=' + actid,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.inProgressActivityData = response.data.in_progress_activity;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }

                                //*******************End Bhupendra's code***************************//

                                //*********************************************************************************
                                //Start function :- To Update started Activity.
                                //Created by Raul Chauhan
                                //Date : 05/05/2020
                                $scope.updatesubmit = function (roomcode, processid, actid, actname, docno, frequency, product_no, batch_no, selectedfilters, roleid, empid, empname, email, remark, update_id, is_in_workflow, selectedReplaces) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/update_return_air_filter',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&frequency=" + frequency + "&product_no=" + product_no + "&batch_no=" + batch_no + "&selectedfilters=" + selectedfilters + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&update_id=" + update_id + "&is_in_workflow=" + is_in_workflow + "&selectedReplaces=" + selectedReplaces,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.edit = false;
                                        angular.element("#btnclose").trigger('click');
                                        $scope.resetLoginForm();
                                        $scope.getLastFilterActivity();
                                        $scope.selectedFilter = [];
                                        $('.loader').hide();
                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }
                                //*********************************************************************************************
                                $scope.CancilUpdate = function () {
                                    $scope.edit = false;
                                }
                                //Get Last unapproved Room Activity
                                $scope.roomFilterData = [];
                                $scope.showSelect = false;
                                $scope.getLastFilterActivity = function () {
                                    if($scope.frequencyList.length==0){
                                        $scope.getfrequencyData();
                                    }
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetLastFilterActivity?room_code=' + myParam + "&table_name=pts_trn_return_air_filter",
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {

                                        $scope.roomFilterData = response.data.row_data;
                                        if ($scope.roomFilterData.length > 0) {
                                            var tempArray = [];
                                            $scope.docno = $scope.roomFilterData[0].doc_id;
                                            $scope.frequency = $scope.roomFilterData[0].frequency_id;
                                            $scope.batch_no = $scope.roomFilterData[0].batch_no == '' ? 'NA' : $scope.roomFilterData[0].batch_no;
                                            $scope.product_no = $scope.roomFilterData[0].product_code;
                                            $scope.getfrequencyName($scope.roomFilterData[0].frequency_id);
                                            $scope.roomFilterData[0]['filter_array'] = $scope.roomFilterData[0].filter.split(',');
                                            $scope.selectedReplace = $scope.roomFilterData[0].replace_filter.split(',');
                                            $scope.selectedFilter = $scope.roomFilterData[0].clean_filter.split(',');
                                            angular.forEach($scope.filterList, function (obj, index) {
                                                if ($scope.selectedFilter.includes(obj.filter_name)) {
                                                    $scope.filterList[index]['ischecked'] = true;
                                                }
                                                if ($scope.selectedReplace.includes(obj.filter_name)) {
                                                    $scope.filterList[index]['isreplace'] = true;
                                                }
                                                tempArray.push(obj.filter_name);
                                            });
                                            var difference = [];

                                            for (var i = tempArray.length; i--; ) {
                                                if ($scope.roomFilterData[0]['filter_array'].indexOf(tempArray[i]) === -1)
                                                    difference.push(tempArray[i]);
                                            }
//                                            alert(difference.length);
                                            if (difference.length == 0) {
                                                $scope.showSelect = true;
                                            } else {
                                                $scope.showSelect = false;
                                            }

                                        }
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });

                                }
                                $scope.getLastFilterActivity();
                                $scope.getfrequencyName = function (objId) {
                                    $scope.frequency_name = $filter('filter')($scope.frequencyList, {id: objId})[0].frequency_name;
                                }

                                //*******************Rahul's code Started**********************//
                                $scope.editTrasaction = function () {
                                    if (confirm("Do you really want to edit ?"))
                                    {
                                        $scope.edit = true;
                                        $scope.docno = $scope.roomFilterData[0].doc_no;
                                        $scope.batch_no = $scope.roomFilterData[0].batch_no;
                                        $scope.product_no = $scope.roomFilterData[0].product_code;
                                        $scope.getProductDetail($scope.product_no);
                                        $scope.frequency = $scope.roomFilterData[0].frequency_id;
                                        $scope.is_in_workflow = $scope.roomFilterData[0].is_in_workflow;
                                        var tempFilter = $scope.roomFilterData[0].filter.split(",");
                                        angular.forEach($scope.filterList, function (obj, index) {
                                            if ($scope.selectedFilter.includes(obj.filter_name)) {
                                                $scope.filterList[index]['ischecked'] = true;
                                            }
                                            if ($scope.selectedReplace.includes(obj.filter_name)) {
                                                $scope.filterList[index]['isreplace'] = true;
                                            }
                                        });
//                                        if (tempFilter.length == $scope.filterList.length) {
//
//                                            $scope.selectedFilter = [];
//                                            $("#Eselectall").prop('checked', true);
//                                            angular.forEach($scope.filterList, function (obj, index) {
//                                                $scope.filterList[index]['ischecked'] = true;
//                                                $("#check" + obj.id).prop('checked', true);
//                                                $scope.selectedFilter.push(obj.filter_name);
//                                            });
//                                        } else {
//                                            angular.forEach(tempFilter, function (obj, index) {
//                                                var temp = $filter('filter')($scope.filterList, {filter_name: obj})[0];
//                                                $("#check" + temp.id).prop('checked', true);
//                                                $scope.selectedFilter.push(obj);
//                                            });
//                                        }
                                    }
                                }

                                //*******************End Rahul's code***************************//

                                //Reset Login Form
                                $scope.resetLoginForm = function () {
                                    $scope.username = "";
                                    $scope.password = "";
                                    $scope.remark = "";
                                }


                            });
</script>