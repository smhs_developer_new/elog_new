<?php
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'master', "status" => 'active'))->row_array();
//print_r($res);exit;
$edit = false;
$add = false;
$view = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $edit = true;
    $add = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_create'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1 && $res['is_create'] == 0) {
            $edit = true;
            $add = false;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 0) {
            $edit = false;
            $add = true;
        } elseif ($res['is_create'] == 1 && $res['is_edit'] == 1) {
            $edit = true;
            $add = true;
        }elseif ($res['is_view'] == 1) {
            $edit = false;
            $add = false;
            $view = true;
        } else {
            $edit = false;
            $add = false;
        } 
//        echo $add.'&&'.$edit.'&&'.$view;exit;
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Master")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",1500);</script>";
    }
}
function searchByValue($id,$type, $array) {
       foreach ($array as $key => $val) {
           if ($val->activity_id == $id && $val->workflowtype==$type) {
             return $val;
           }
       }
       return null;
    }
?>
<div class="container-fluid">
          <!-- Page Heading -->
			
			<div class="content-wrapper">
                <div class="row topheader">
               <div class="col-md-6">
                <h2 class="f18">Workflow</h2>
               </div>
               <div class="col-md-6">
                   <?php if ($add) { ?>
                                        <input class="btn btn-sm btn-info float-right mb-2 text-right" type="button" name="addbtn" onclick="addflow();" id="addbtn" value="Add workflow">
                                    <?php } else { ?>
                                        <input class="btn btn-sm btn-info float-right mb-2 text-right" type="button" name="addbtn" onclick="addflow();"  id="addbtn" value="Add workflow" disabled>
                                    <?php } ?>
                
               </div>
                </div>
      </div>

      <div class="card card-default" id="flow" data-flag="0" style="display: none;">
        <div class="card-body" style="margin: 0 auto;">
          <div class="row">
		      <div class="col-sm-12"><div><span class="float-left"><h4>List of Activities</h4></span></div></div>
		      <div class="modal-body">
          <section class="page-tasks" id="Listofequip">
          <ul class="task-list list-unstyled" id="eqlist">
              <?php if($data['activities']=="")
          {

          }
          else
          {//echo '<pre>';print_r($data['wf']->result());
            foreach($data['activities']->result() as $row) {
                if($row->type == 'home') { ?>
                      <li> 
            <span class="view bg-info"><div class="row"><div class="col-sm-12 pl0"><div class="row">
            <div class="col-sm-8 mt-2 mb-2"><!--<img class="mb-2 mt-2 wt42" src="<?php echo base_url(); ?>img/<?php echo $row->activity_icon ?>">--><span class="f15"><?php echo $row->activity_name ?></span></div>
            <div class="col-sm-4 disable-button-color float-right text-right" id="show19">
                <?php $searchValue1 = searchByValue($row->id,'start', $data['wf']->result());
                if(!empty($searchValue1)){?>
                <a class="btn btn-labeled btn-danger mt-2 mb-2" type="button" name="btndb19" id="btnbd19" onclick="showmessage()">Start</a> 
                <?php }else{?>
                   <a class="btn btn-labeled btn-danger mt-2 mb-2" type="button" name="btndb19" id="btnbd19" href="<?= base_url()?>ponta_sahib/Ptsadmin/addstep/<?= $row->id ?>/start?module_id=<?php echo $_GET['module_id']?>&actname=<?= $row->activity_name ?>&wftype=Start">Start</a> 
               <?php }
               
                ?>   
                    <?php $searchValue2 = searchByValue($row->id,'stop', $data['wf']->result());
                if(!empty($searchValue2)){?>
                    <a class="btn btn-labeled btn-danger mt-2 mb-2" disabled type="button" name="btndb19" id="btnbd19" onclick="showmessage()">Stop</a></div>
               <?php  }else{?>
                   <a class="btn btn-labeled btn-danger mt-2 mb-2" type="button" name="btndb19" id="btnbd19" href="<?= base_url()?>ponta_sahib/Ptsadmin/addstep/<?= $row->id ?>/stop?module_id=<?php echo $_GET['module_id']?>&actname=<?= $row->activity_name ?>&wftype=Stop">Stop</a></div>
               <?php }
               
                ?>   
            
            </div>
            </div>
            </div>
            </span> 
            </li>
            <?php } else { 
                if($row->is_need_approval=='yes'){
                ?>
             <li> 
            <span class="view bg-info"><div class="row"><div class="col-sm-12 pl0"><div class="row">
            <div class="col-sm-8 mt-2 mb-2"><!--<img class="mb-2 mt-2 wt42" src="<?php echo base_url(); ?>img/<?php echo $row->activity_icon ?>">--><span class="f15"><?php echo $row->activity_name ?></span></div>
            <div class="col-sm-4 disable-button-color float-right text-right" id="show19">             
            <!--<a class="btn btn-labeled btn-danger mt-2 mb-2" type="button" name="btndb19" id="btnbd19" href="<?= base_url()?>ponta_sahib/Ptsadmin/addstep/<?= $row->id ?>/start">Start</a>-->
               <?php $searchValue3 = searchByValue($row->id,'stop', $data['wf']->result());
                if(!empty($searchValue3)){?>
                    <a class="btn btn-labeled btn-danger mt-2 mb-2" disabled type="button" name="btndb19" id="btnbd19" onclick="showmessage()">Stop</a></div>
                <?php }else{?>
                   <a class="btn btn-labeled btn-danger mt-2 mb-2" type="button" name="btndb19" id="btnbd19" href="<?= base_url()?>ponta_sahib/Ptsadmin/addstep/<?= $row->id ?>/stop?module_id=<?php echo $_GET['module_id']?>&actname=<?= $row->activity_name ?>&wftype=Stop">Stop</a></div>
               <?php }
               
                ?>             
            
            </div>
            </div>
            </div>
            </span> 
            </li>
          <?php } } }} ?>
          </ul>
        </section>
		
		</div>
          </div>		
		  
        </div>
      </div>
      
       <div class="content-wrapper">
        <div class="card shadow mb-4">
          <div class="card-body">
            <table class="table table-bordered custom-table">
              <tr>
                <!--<th>S.no.</th>-->
                <th>Activity</th>
                <th>Type</th>
                <th>Last Modified By</th>
                <th>Last Modified On</th>
                <th>Action</th>
                <th>Status</th>
              </tr>
              <?php if(count($data['wf']->result())) { ?>
            <?php $i=0; foreach($data['wf']->result() as $row) { $i++; ?> 
						<tr data-row-id="0">
						<!--<td class="text-left" style=""><?php //echo $i ?></td>-->
						<td class="text-left" style=""><?php echo $row->activity_name ?></td>
            <td class="text-left" style=""><?php echo $row->workflowtype ?></td>
            <td class="text-left" style=""><?php echo $row->created_by ?></td>
						<td class="text-left" style=""><?php echo date('d-M-Y H:i:s',strtotime($row->created_on)) ?></td>
						<td class="text-left" style="">
                                        <?php if ($edit) { 
                                            $inprogress=$this->db->get_where("pts_trn_user_activity_log",array("activity_id"=>$row->activity_id, "workflownextstep > " => 0))->result_array();
                                            if(!empty($inprogress)){?>
                                                    <a type="button" onclick="showMessage('<?php echo $row->activity_name?>','edit')" class="command-edit"><i class="far fa-edit text-blue pr10"></i></a>
                                                <!--<a type="button" onclick="showMessage('<?php echo $row->activity_name?>','delete')" class="command-delete"><i class="fas fa-trash text-blue"></i></a>-->
<!--                                                <a type="button" disabled href="<?= base_url()?>ponta_sahib/Ptsadmin/editstep/<?= $row->activity_id ?>/<?= $row->workflowtype ?>?module_id=<?php echo $_GET['module_id']?>&actname=<?= $row->activity_name ?>&wftype=<?= $row->workflowtype ?>" class=" command-edit" data-id="10243"><i class="far fa-edit text-blue pr10"></i></a>
						<a type="button" disabled class="command-delete" onclick="deleteworkflow('<?php echo $row->activity_id ?>','<?php echo $row->workflowtype ?>')" data-id="10243"><i class="fas fa-trash text-blue"></i></a>-->
                                           <?php } else {?>
<a type="button" href="<?= base_url()?>ponta_sahib/Ptsadmin/editstep/<?= $row->activity_id ?>/<?= $row->workflowtype ?>?module_id=<?php echo $_GET['module_id']?>&actname=<?= $row->activity_name ?>&wftype=<?= $row->workflowtype ?>&status=<?= $row->is_active?>" class=" command-edit" data-id="10243"><i class="far fa-edit text-blue pr10"></i></a>
						<!--<a type="button" class="command-delete" onclick="deleteworkflow('<?php echo $row->activity_id ?>','<?php echo $row->workflowtype ?>')" data-id="10243"><i class="fas fa-trash text-blue"></i></a>-->
<?php }?>
                                                    
                                                
                                        <?php } else if($view) {?>
                                        <a type="button" class=" command-edit" data-id="10243" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                             <!--<a type="button" href="<?= base_url()?>ponta_sahib/Ptsadmin/editstep/<?= $row->activity_id ?>/<?= $row->workflowtype ?>?module_id=<?php echo $_GET['module_id']?>&actname=<?= $row->activity_name ?>&wftype=<?= $row->workflowtype ?>" class=" command-edit" data-id="10243"><i class="far fa-edit text-blue pr10"></i></a>-->
                                             <!--<button disabled class="btn btn-sm btn-info"><i class="fas fa-trash text-blue"></i></button>-->
                                        <?php }else{ ?>
                                        <a type="button" class=" command-edit" data-id="10243" disabled><i class="far fa-edit text-blue pr10"></i></a>
                                                <!--<button disabled class="btn btn-sm btn-info"><i class="far fa-edit text-blue pr10"></i></button>-->
                                        <?php } ?>
						</td>
                                                <?php if($row->is_active==1){?>
                                                <td class="bg-success text-white">Active</td>
                                                <?php } else {?>
                                       <td class="bg-danger text-white">Inactive</td>
                                   <?php }?>
                                                
						</tr>
            <?php } } else { ?>
              <tr data-row-id="0"><td colspan="5"><center>No record exist.</center></td></tr>
            <?php } ?>
              
            </table>
            </div>
            </div>
          </div>
        </div>

  <!-- Custom scripts for all pages-->
  
<script type="text/javascript">
    function showmessage(){
        alert("This workflow is already exists please delete the previous one to make it again, Thank you...!");
    }
     function addflow()
     {
        if($("#flow").attr('data-flag')=='0')
        {
        $("#flow").show(1000);
        $("#addbtn").val('Less workflow');
        $("#flow").attr('data-flag','1');
        }
        else
        {
        $("#flow").hide(1000);
        $("#addbtn").val('Add workflow');
        $("#flow").attr('data-flag','0');
        }

     }
     function go(act)
     {

     }

     function deleteworkflow(actid,type)
     {
         var response = confirm("Do You Really Want To Delete This Record ?");
         if (response == true)
         {
         var act=actid;
         var type=type;
         $.ajax({
            url: "<?= base_url()?>deleteworkflow",
            type: 'POST',
            data: {act:act,type:type},
            success: function(res) {
            if(res>0){
               alert("Workflow successfully deleted...");
               window.location.href = "<?= base_url()?>workflow?module_id=<?php echo $_GET['module_id']?>";
            }
            else
            {
               alert("Something went wrong");
            }
         }
         }); 
      }
   }
   
   function showMessage(type,action){
   alert("You can't "+action+" this because "+type+" is inprogress");
   }
   </script>