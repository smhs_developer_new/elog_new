<div  ng-app="qaApprovalApp" ng-controller="qaApprovalCtrl" ng-cloak>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>sf_home">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Pending for Approval</li>
        </ol>
    </nav>

    <!-- Pending for Approval start -->
    <section class="contentBox blueBorder">


        <div class="contentHeader">

            <div class="row">

                <div class="col-8">
                    <h1>Pending for Approval</h1>
                </div>

                <div class="col-4 text-right">

                </div>

            </div>

        </div>

        <div class="contentBody">
            <div class="table-responsive noscroll">
<!--                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Doc Id</th>
                            <th>Room Code</th>
                            <th>Start Date</th>
                            <th>Start Time</th>
                            <th>Stop Date</th>
                            <th>Stop Time</th>
                            <th>Started By</th>
                            <th>Current Activity</th>
                            <th width="15%">Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="dataObj in qaActivityData">
                            <td>{{dataObj.doc_id}}</td>
                            <td>{{dataObj.room_code}}</td>
                            <td>{{dataObj.start_date | date:'dd-MMM-yyyy'}}</td>
                            <td>{{dataObj.start_time}}</td>
                            <td>{{dataObj.stop_date!=null? dataObj.stop_date :'----' | date:'dd-MMM-yyyy'}}</td>
                            <td>{{dataObj.stop_time!=null?dataObj.stop_time:'----'}}</td>
                            <td>{{dataObj.user_name}}</td>
                            <td>{{dataObj.activity_name}} - {{dataObj.activity_remarks}}</td>
                            <td><button class="button smallBtn primaryBtn symbol loginAction" data-toggle="modal" data-target="#loginModal" ng-click="setActdata(dataObj, 'approval');"   ><img src="<?php echo base_url(); ?>assets/images/tick.png" alt=""> Approve</button>
                            </td>
                        </tr>



                    </tbody>
                </table>-->
                
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Doc Id</th>
                            <th>Room Code</th>
                            <th>Equipment Code</th>
                            <th>Start Date</th>
                            <th>Start Time</th>
                            <th>Started By/Done By</th>
                            <th>Stop Date</th>
                            <th>Stop Time</th>
                            <th>Stop By</th>
                            <th>Batch No.</th>
                            <th>Product Code</th>
                            <th>Product Disc.</th>
                            <th>Current Activity</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="dataObj in qaActivityData">
                            <td>{{dataObj.doc_id}}</td>
                            <td>{{dataObj.room_code}}</td>
                            <td>{{dataObj.equip_code}}</td>
                            <td>{{dataObj.start_date| date:'dd-MMM-yyyy'}}</td>
                            <td>{{dataObj.start_time}}</td>
                            <td>{{dataObj.user_name}}</td>
                            <td>{{dataObj.stop_date| date:'dd-MMM-yyyy'}}</td>
                            <td>{{dataObj.stop_time}}</td>
                            <td>{{dataObj.stop_by}}</td>
                            <td>{{dataObj.batch_no!=''?dataObj.batch_no:'NA'}}</td>
                            <td>{{dataObj.product_code!=''?dataObj.product_code:'NA'}}</td>
                            <td>{{(dataObj.product_code != 'NA' && dataObj.product_code != '') ? getProductDesc(dataObj.product_code) : 'NA'}}</td>
                            <td>{{dataObj.activity_remarks}}</td>
                            <td ng-show="dataObj.workflownextstep=='-1'">
                                <button style="width: 110px;" class="button smallBtn primaryBtn symbol loginAction" data-toggle="modal" data-target="#loginModal" ng-click="setActdata(dataObj, 'approval');"   ><img src="<?php echo base_url(); ?>assets/images/tick.png" alt=""> Approve</button>
                            </td>
                            <td ng-show="dataObj.workflownextstep>=0">
                                <button style="width: 110px;" class="button smallBtn primaryBtn symbol loginAction" ng-click="showMessage()"   ><img src="<?php echo base_url(); ?>assets/images/tick.png" alt=""> Approve</button>
                            </td>
                        </tr>





                    </tbody>
                </table>
            </div>

        </div>


    </section>
    <!-- Pending for Approval end-->
    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script>
                                var app = angular.module("qaApprovalApp", ['blockUI']);
                                app.controller("qaApprovalCtrl", function ($scope, $http, $filter, blockUI) {
//                                    $scope.terminal_id = "10.240.240.240";
                                    $scope.terminal_id='<?php echo $_SERVER['REMOTE_ADDR']?>';
                                    $scope.qaActivityData = [];
                                    $scope.getQaActivityList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetQaApprovalActivityList?terminal_id='+$scope.terminal_id,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.qaActivityData = response.data.qa_approval_activity;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.productData = [];
                    $scope.GetProductList = function () {
                        $http({
                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                            method: "GET",
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        }).then(function (response) {
                            $scope.productData = response.data.product_list;
                        }, function (error) { // optional
                            console.log("Something went wrong.Please try again");
                        });
                    }
                    $scope.GetProductList();
                    $scope.getProductDesc = function (product_code) {
                        var desc = $filter('filter')($scope.productData, {product_code: product_code})[0].product_name;
                        return desc;
                    }
                                    $scope.getQaActivityList();
                                    $scope.actdataobj = {};
                                    $scope.setActdata = function (obj, btntext)
                                    {  
                                        $scope.actdataobj = obj;
                                    }
                                    $scope.activityList = [];
                                    $scope.getMstActivityList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetMstActivityList?type=QA',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.activityList = response.data.mst_act_list;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getMstActivityList();
                                    $scope.masterActivityData = [];
                                    $scope.getMasterActivityList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getmasteractivitylist?activity_type=home',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.masterActivityData = response.data.master_activity_list;
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getMasterActivityList();
                                    $scope.showMessage=function(){
                                    alert("Activity is In-Progress, Please complete the activity before performing QA Verification");
                                    }
                                    //Login Function 
                                    //Created by Bhupendra 
                                    //Date : 28/01/2020                                    
                                    $scope.login = function () {
                                        var actname = "";
                                        var actid = $scope.actdataobj.processid;
                                        if (actid == 16 || actid == 17 || actid == 30) {
                                            var actname = $filter('filter')($scope.masterActivityData, {id: $scope.actdataobj.activity_id})[0].activity_name;
                                        }
                                        var response = confirm("Do you really want to perform this task?");
                                        if (response == true)
                                        {
                                            pwd2 = SHA256($scope.password);
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                                method: "POST",
                                                data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&module_id=" + actid + "&module_type=log" + "&action=approval"+"&act_type="+actname+"&room_code="+$scope.actdataobj.room_code+ "&auth=" + btoa(btoa($scope.password)),
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                if (response.data.message == "")
                                                {
                                                    $scope.getQAapproved($scope.actdataobj.act_id, $scope.actdataobj.activity_id, $scope.actdataobj.workflowstatus, $scope.actdataobj.workflownextstep, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark);
                                                } else {
                                                    alert(response.data.message);
                                                }
                                            }, function (error) {
                                                console.log(error);
                                            });
                                        }
                                    }
                                    //End Login Function

                                    //Approval function :- To Approve running activity by auth & privilege chk as well as sent that activity to next approval which exists in workflow
                                    //Created by Bhupendra 
                                    //Date : 28/01/2020
                                    $scope.getQAapproved = function (actlogtblid, actid, workflowstatus, workflownextstep, roleid, empid, empname, email, remark) {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getQAapproved',
                                            method: "POST",
                                            data: "actlogtblid=" + actlogtblid + "&actid=" + actid + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark,

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            alert(response.data.message);
                                            angular.element("#btnclose").trigger('click');
                                            $scope.resetLoginForm();

                                            $scope.getQaActivityList();
                                        }, function (error) {
                                            console.log(error);
                                        });
                                    }
                                    //********************************************************************************************
                                    //Reset Login Form
                                    $scope.resetLoginForm = function () {
                                        $scope.username = "";
                                        $scope.password = "";
                                        $scope.remark = "";
                                    }
                                });
</script>