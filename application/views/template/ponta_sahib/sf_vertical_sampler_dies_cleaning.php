<div ng-app="verticalApp" ng-controller="verticalCtrl" ng-cloak>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>sf_home">Home</a></li>
            <li class="breadcrumb-item"><a href="#">{{room_code_show}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Vertical Sampler and Dies Cleaning Usages Log</li>
        </ol>
    </nav>
    <section class="contentBox blueBorder formWrapper">
            <div class="contentHeader">
                <div class="row">
                    <div class="col-12">
                        <h1>Header Details</h1>
                    </div>
                </div>
            </div>
            <div class="contentBody">
                <form>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <ul class="activityList">
                                    <li style="margin-left: 100px;"><a><b>Form No : </b>{{formno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Version No : </b>{{versionno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Effective Date : </b>{{effectivedate | date:'dd-MMM-yyyy'}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Retired Date : </b>{{retireddate | date:'dd-MMM-yyyy'}} </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    <section class="contentBox blueBorder formWrapper">


        <div class="contentHeader">

            <div class="row">
                <div class="col-12">
                    <h1>Vertical Sampler and Dies Cleaning Usages Log</h1>
                </div>
            </div>

        </div>

        <div class="contentBody" ng-show="inProgressActivityData.length == 0 || edit == true">
            <form name="samplerForm" novalidate>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Document No.</label>
                            <input type="text" class="form-control" ng-model="inProgressActivityData[0].doc_id" readonly>    
                        </div>
                    </div>
                    <div class="col-sm-6" ng-show="edit == true">
                        <div class="form-group" >
                            <label><span class="asterisk">* </span>Sampling Rod ID No.</label>
                            <input type="text" class="form-control" value="{{inProgressActivityData[0].sampling_rod_id}}" readonly>

                        </div>
                    </div>
                    <div class="col-sm-6" ng-show="sampleRodData.length > 0 && edit == false">
                        <div class="form-group" >
                            <label><span class="asterisk">* </span>Sampling Rod ID No.</label>
                            <select class="chosen form-control" tabindex="4"  name="sampling_rod_id"  data-placeholder="Search Sampling Rod ID" ng-options="dataObj['equipment_code'] as (dataObj.equipment_name +             ' => ' +             dataObj.equipment_code) for dataObj in sampleRodData"  ng-model="sampling_rod_id" required chosen>
                                <option value="">Select Sampling Rod ID No</option>
                            </select>

                        </div>
                    </div>
                    <div class="col-sm-6" ng-show="sampleRodData.length == 0 && edit == false">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Sampling Rod ID No.</label>
                            <select class="form-control">
                                <option value="">All Sampling Rod IDs are in used or not defined in master</option>
                            </select>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Activity</label>
                            <ul class="activityList">
                                <li class="{{current == $index?'active':'' }}" ng-repeat="dataObj in masterActivityData"><a  ng-click="initiateActivity(dataObj, $index)">{{dataObj.field_value}} </a></li>

                            </ul>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Batch No.</label>
                            <p ng-show="batchInProductData.length > 0"><b>{{batch_no}}</b></p>
                            <input ng-show="batchInProductData.length == 0" maxlength="10" class="form-control" type="text" placeholder="Batch No." name="batch_no" ng-model="batch_no" > 
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Product No.</label>
                            <div class="col-sm-12" ng-show="batchInProductData.length == 0">
                                <select class="chzn-select form-control" tabindex="4"  name="product_code" ng-change="getProductDetail(product_no)" data-placeholder="Search Product No." ng-options="dataObj['product_code'] as dataObj.product_code for dataObj in productData"  ng-model="product_no" required chosen>
                                    <option value="#">Select Product No</option>
                                </select>
                            </div>
                            <div class="col-sm-6" ng-show="batchInProductData.length > 0">
                                <p><b>{{product_no}}</b></p>
                            </div>
                            <div class="col-sm-6" ng-if="product_desc != ''">
                                <p><b>{{product_desc}}</b></p>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Dies No.</label>
                            <input class="form-control" ng-change="getdiesno(dies_no)" type="text" maxlength="10" placeholder="Dies No." name="dies_no" ng-model="dies_no" required>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <button ng-show="edit" class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancel</button>&nbsp;&nbsp;&nbsp;
                            <button ng-show="edit" ng-disabled="(product_no == '' || product_no == NULL) || current < 0 || sampling_rod_id == undefined" ng-click="setbtntext('update');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Update</button>&nbsp;&nbsp;&nbsp;
                            
							<button ng-show="!edit" data-toggle="modal" data-target="#loginModal" ng-disabled="(product_no == '' || product_no == NULL) || inProgressActivityData.length > 0 || current < 0 || sampling_rod_id == undefined || dies_no == '' || dies_no == undefined || batch_no == '' || batch_no == undefined || product_no == '' || product_no == undefined"  ng-click="setbtntext('start');"  class="button smallBtn primaryBtn" >Activity Start</button>&nbsp;&nbsp;&nbsp;
                            
							<button  ng-show="!edit" data-toggle="modal" data-target="#loginModal"  ng-disabled="(product_no == '' || product_no == NULL) || ((inProgressActivityData[0].workflownextstep > 0 && inProgressActivityData[0].workflowstatus > 0) || (inProgressActivityData[0].activity_stop > 0)) || current < 0 || sampling_rod_id == undefined || dies_no == '' || stopApprove == 0"  ng-click="setbtntext('stop');"  class="button smallBtn primaryBtn" >Activity Stop</button>&nbsp;&nbsp;&nbsp;
                            <button   ng-show="!edit" data-toggle="modal" data-target="#loginModal" ng-disabled="(product_no == '' || product_no == NULL) || (inProgressActivityData[0].activity_stop != NULL && inProgressActivityData[0].activity_stop != '' && inProgressActivityData[0].workflownextstep == 0 && inProgressActivityData[0].workflowstatus == 0) || ((inProgressActivityData[0].activity_stop == NULL || inProgressActivityData[0].activity_stop == '') && inProgressActivityData[0].workflownextstep == 0 && inProgressActivityData[0].workflowstatus != 0) || (inProgressActivityData[0].workflownextstep == 0 && inProgressActivityData[0].workflowstatus == 0) || current < 0 || sampling_rod_id == undefined || dies_no == '' || stopApprove == 0"  ng-click="setbtntext('approval');" class="button smallBtn primaryBtn" >Approve</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
        <div class="contentBody" ng-show="inProgressActivityData.length > 0 && edit == false">
            <form name="samplerForm" novalidate>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Document No.</label>
                            <input type="text" class="form-control" ng-model="inProgressActivityData[0].doc_id" readonly>    
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Sampling Rod ID No.</label>
                            <input type="text" class="form-control" value="{{inProgressActivityData[0].equip_code}}" readonly> 
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Activity</label>
                            <ul class="activityList">
                                <li class="{{current == $index?'active':'' }}" ng-repeat="dataObj in masterActivityData"><a>{{dataObj.field_value}} </a></li>

                            </ul>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Batch No.</label>
                            <input type="text" class="form-control" value="{{inProgressActivityData[0].batch_no}}" readonly>   
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Product No.</label>
                            <input type="text" class="form-control" value="{{inProgressActivityData[0].product_code!=''?inProgressActivityData[0].product_code:'NA'}}" readonly>
                        </div>
                    </div>

                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Dies No.</label>
                            <input type="text" class="form-control" value="{{inProgressActivityData[0].dies_no}}" readonly>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <button ng-click="editTrasaction()" class="button smallBtn primaryBtn" >Edit</button>&nbsp;&nbsp;&nbsp;
                            <button data-toggle="modal" data-target="#loginModal"  ng-disabled="(inProgressActivityData[0].activity_stop > 0)"  ng-click="setbtntext('stop');"  class="button smallBtn primaryBtn" >Activity Stop</button>&nbsp;&nbsp;&nbsp;
                            <button data-toggle="modal" data-target="#loginModal" ng-disabled="(inProgressActivityData[0].activity_stop != NULL && inProgressActivityData[0].activity_stop != '' && inProgressActivityData[0].workflownextstep == 0 && inProgressActivityData[0].workflowstatus == 0) || ((inProgressActivityData[0].activity_stop == NULL || inProgressActivityData[0].activity_stop == '') && inProgressActivityData[0].workflownextstep == 0 && inProgressActivityData[0].workflowstatus != 0) || (inProgressActivityData[0].workflownextstep == 0 && inProgressActivityData[0].workflowstatus == 0)"  ng-click="setbtntext('approval');" class="button smallBtn primaryBtn" >Approve</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>


    </section>
    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>

<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script type="text/javascript">
                                var app = angular.module("verticalApp", ['angular.chosen', 'blockUI']);
                                app.directive('chosen', function ($timeout) {

                                    var linker = function (scope, element, attr) {

                                        scope.$watch('sampleRodData', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);
                                        scope.$watch('productData', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);
                                        $timeout(function () {
                                            element.chosen();
                                        }, 0, false);
                                    };
                                    return {
                                        restrict: 'A',
                                        link: linker
                                    };
                                });
                                app.controller("verticalCtrl", function ($scope, $http, $filter, blockUI) {
                                    var urlParams = new URLSearchParams(window.location.search);
                                    var myParam = urlParams.get('room_code');
                                    var processid = urlParams.get('processid');
                                    var Actidd = urlParams.get('actid');
                                    var mst_activity = urlParams.get('actid');
                                    var Actnamee = urlParams.get('actname');
                                    var doc_id = urlParams.get('doc_id');

                                    // get header detail 
                                $scope.headerRecordid = urlParams.get('headerid');
                                $scope.formno = urlParams.get('formno');
                                $scope.versionno = urlParams.get('versionno');
                                $scope.effectivedate = urlParams.get('effectivedate');
                                $scope.retireddate = urlParams.get('retireddate');
                                //end

                                    $scope.room_code_show = myParam;
                                    $scope.batchInProductData = [];
                                    $scope.stopApprove = 0;
                                    //Check for edit Privelege
                                    $scope.roleList = [];
                                    $scope.getRoleList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.roleList = response.data.role_list;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getRoleList();
                                    $scope.employeRole = [];
                                    $scope.getEmployeRoleList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getEmployeRoleList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.employeRole = response.data.role_data;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getEmployeRoleList();
                                    $scope.getRoomBatchInProductData = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getRoomBatchInProductData?room_code=' + myParam,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.batchInProductData = response.data.batch_product_list;
                                            if ($scope.batchInProductData.length > 0) {
                                                $scope.batch_no = $scope.batchInProductData[0].batch_no;
                                                $scope.product_no = $scope.batchInProductData[0].product_code;
                                                $scope.product_desc = $scope.batchInProductData[0].product_desc;
                                                $scope.productcode = $scope.batchInProductData[0].product_code;
                                            }
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getRoomBatchInProductData();
                                    $scope.sampleRodData = [];
                                    $scope.getInstrumentList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetInstrumentList?equipment_type=Samplingrod',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.sampleRodData = response.data.instrument_data;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getInstrumentList();

                                    //Get Document Number code
                                    $scope.docno = "";
                                    // $scope.getdocno = function () {
                                    //     $http({
                                    //         url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getdocno',
                                    //         method: "GET",
                                    //         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    //     }).then(function (response) {
                                    //         $scope.docno = response.data.docno;
                                    //     }, function (error) { // optional

                                    //         console.log("Something went wrong.Please try again");
                                    //     });

                                    // }
                                    //$scope.getdocno();
                                    //End of Get Document number function code
                                    $scope.masterActivityData = [];
                                    $scope.getMasterActivityList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetVerticalmasteractivitylist',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.masterActivityData = response.data.master_activity_list;
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getMasterActivityList();
                                    $scope.current = '-1';
                                    $scope.Actid = '';
                                    $scope.Actname = '';
                                    $scope.productcode = '';
                                    $scope.product_no = "";
                                    $scope.batchno = '';
				    $scope.dies_no = "";
                                    $scope.current_name = '';

                                    $scope.initiateActivity = function (dataObj, index) {
                                        $scope.current = index;
                                        $scope.current_name=dataObj.field_value;
                                        if ($scope.batchInProductData.length == 0) {
                                            if (dataObj.field_value == 'Cleaning')
                                            {
                                                /*$scope.product_no = "NA";
                                                $scope.batch_no = "NA";
                                                $scope.product_desc = "";
                                                $scope.dies_no = "NA";*/
						$scope.product_no = "";
                                                $scope.batch_no = "";
                                                $scope.product_desc = "";
                                                $scope.dies_no = "";
                                                 }else
                                            {
                                                $scope.product_no = "";
                                                $scope.batch_no = "";
                                                $scope.product_desc = "";
                                                $scope.dies_no = "";
                                                angular.forEach($scope.productData, function (obj, index) {
                                                    if (obj.product_code == "NA") {
                                                        $scope.productData.splice(index, 1);
                                                    }
                                                });

                                            }
                                        }
                                        if (dataObj.field_value == 'Cleaning')
                                        {
                                            angular.forEach($scope.productData, function (obj, index) {
                                                    if (obj.product_code == "NA") {
                                                        $scope.productData.splice(index, 1);
                                                    }
                                                });
                                            $scope.productData.unshift({"product_code" : "NA", "product_name" : "NA"});
                                            $scope.dies_no = "";

                                        } else
                                        {
                                            $scope.dies_no = "";
                                            angular.forEach($scope.productData, function (obj, index) {
                                                    if (obj.product_code == "NA") {
                                                        $scope.productData.splice(index, 1);
                                                    }
                                            });
                                        }
                                        $scope.Actid = dataObj.id;
                                        $scope.Actname = dataObj.field_value;
                                    }

                                    $scope.productData = [];
                                    $scope.getProductList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.productData = response.data.product_list;
                                            $scope.productData.unshift({"product_code" : "NA", "product_name" : "NA"});
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getProductList();
                                    $scope.product_desc = "";
                                    $scope.getProductDetail = function (dataObj) {
                                        $scope.productcode = dataObj;
                                        $scope.product_desc = $filter('filter')($scope.productData, {product_code: dataObj})[0].product_name;
                                    }

                                    //*******************Bhupendra's code Started**********************//

                                    $scope.setbtntext = function (btntext)
                                    {
                                        $scope.btntext = btntext;
                                    }
                                    $scope.getdiesno = function (diesno)
                                    {
                                        $scope.dies_no = diesno;
										//$scope.dies_no = '';
                                    }
                                    $scope.edit = false;
                                    //Login Function 
                                    //Created by Bhupendra 
                                    //Date : 28/01/2020                                    
                                    $scope.login = function () {
                                        var response = confirm("Do you really want to perform this task?");
                                        if (response == true)
                                        {
                                            $('.loader').show();
                                            pwd2 = SHA256($scope.password);
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                                method: "POST",
                                                data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&module_id=" + mst_activity + "&module_type=log" + "&action=" + $scope.btntext+ "&auth=" + btoa(btoa($scope.password))+"&act_id="+mst_activity,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                if (response.data.message == "")
                                                {

                                                    var equipment_code = $scope.sampling_rod_id;
                                                    if ($scope.btntext == 'approval')
                                                    {
                                                        //Calling Approval function
                                                        //Created by Bhupendra 
                                                        //Date : 28/01/2020
                                                        var actstatus = "";
                                                        if ($scope.inProgressActivityData[0].activity_stop != "" && $scope.inProgressActivityData[0].activity_stop != null)
                                                        {
                                                            actstatus = 'stop';
                                                        } else
                                                        {
                                                            actstatus = 'start';
                                                        }
                                                        $scope.getapproved(myParam, $scope.inProgressActivityData[0].act_id, $scope.inProgressActivityData[0].activity_id, $scope.inProgressActivityData[0].workflowstatus, $scope.inProgressActivityData[0].workflownextstep, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, actstatus)
                                                    } else if ($scope.btntext == 'stop')
                                                    {
                                                        //Calling Stoped function
                                                        //Created by Bhupendra 
                                                        //Date : 29/01/2020
                                                        $scope.getstoped(myParam, $scope.inProgressActivityData[0].act_id, Actidd, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark)
                                                    } else if ($scope.btntext == 'start')
                                                    {
                                                        //Actnamee = Actnamee + '/' + $scope.Actname;
                                                        $scope.getstart(myParam, processid, equipment_code, Actidd, Actnamee, $scope.productcode, $scope.batch_no, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.docno, $scope.sampling_rod_id, $scope.dies_no, $scope.current,$scope.current_name)
                                                    } else if ($scope.btntext == 'edit')
                                                    {
//                                                        //edit Functionality
//                                                        //Created by Rahul
//                                                        //Date : 06/05/2020
//                                                        var userdata = response.data.userdata;
//                                                        var role_id = $filter('filter')($scope.employeRole, {id: $scope.inProgressActivityData[0].user_id})[0].role_id;
//                                                        if (userdata.role_id > role_id) {
//                                                            $scope.edit = true;
//                                                            $scope.docno = $scope.inProgressActivityData[0].doc_id;
//                                                            $scope.sampling_rod_id = $scope.inProgressActivityData[0].equip_code;
//                                                            $scope.current = $scope.inProgressActivityData[0].act_name;
//                                                            $scope.dies_no = $scope.inProgressActivityData[0].dies_no;
//                                                            if ($scope.batchInProductData.length > 0) {
//                                                                $scope.batch_no = $scope.batchInProductData[0].batch_no;
//                                                                $scope.product_no = $scope.batchInProductData[0].product_code;
//                                                            } else {
//                                                                $scope.batch_no = $scope.inProgressActivityData[0].batch_no;
//                                                                $scope.product_no = $scope.inProgressActivityData[0].product_code;
//                                                            }
//                                                            $scope.getProductDetail($scope.product_no);
//                                                        } else {
//                                                            alert("You are not allowed to edit this.");
//                                                            $scope.edit = false;
//                                                        }
//
//                                                        $scope.resetLoginForm();
//                                                        angular.element("#btnclose").trigger('click');
                                                    } else if ($scope.btntext == 'update')
                                                    {
                                                        //update Functionality
                                                        //Created by Rahul
                                                        //Date : 06/05/2020
                                                        var role = "";
                                                        if ($scope.inProgressActivityData[0].activity_level.length > 0 && $scope.inProgressActivityData[0].workflownextstep > 0 && $scope.inProgressActivityData[0].start_time != null && $scope.inProgressActivityData[0].stop_time == null) {
                                                            if ($scope.inProgressActivityData[0].start_time != null && $scope.inProgressActivityData[0].stop_time == null) {
                                                                var workflowtype = 'start';
                                                            } else {
                                                                var workflowtype = 'stop';
                                                            }

                                                            var desc = $filter('filter')($scope.inProgressActivityData[0].activity_level, {status_id: $scope.inProgressActivityData[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                            var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                        } else if ($scope.inProgressActivityData[0].activity_level.length > 0 && $scope.inProgressActivityData[0].workflownextstep == 0 && $scope.inProgressActivityData[0].start_time != null && $scope.inProgressActivityData[0].stop_time == null) {
                                                            var desc = $filter('filter')($scope.inProgressActivityData[0].activity_level, {status_id: $scope.inProgressActivityData[0].activity_level[0].workflowstatus})[0].role_id;
                                                            var role = $filter('filter')($scope.roleList, {id: desc})[0].id;

                                                        } else if ($scope.inProgressActivityData[0].activity_level.length > 0 && $scope.inProgressActivityData[0].workflownextstep > 0 && $scope.inProgressActivityData[0].start_time != null && $scope.inProgressActivityData[0].stop_time != null) {
                                                            if ($scope.inProgressActivityData[0].start_time != null && $scope.inProgressActivityData[0].stop_time == null) {
                                                                var workflowtype = 'start';
                                                            } else {
                                                                var workflowtype = 'stop';
                                                            }
                                                            var desc = $filter('filter')($scope.inProgressActivityData[0].activity_level, {status_id: $scope.inProgressActivityData[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                            var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                        }
                                                        
                                                        var userdata = response.data.userdata;
//                                                    var role_id = $filter('filter')($scope.employeRole, {id: $scope.inProgressActivityData[0].user_id})[0].role_id;
                                                        if (userdata.role_id >= $scope.inProgressActivityData[0].last_approval.role_id && userdata.role_id > 1) {
                                                            //Actnamee = Actnamee + '/' + $scope.Actname;
                                                            $scope.updatesubmit(myParam, processid, equipment_code, Actidd, Actnamee, $scope.productcode, $scope.batch_no, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.docno, $scope.sampling_rod_id, $scope.dies_no, $scope.current, $scope.inProgressActivityData[0].log_id,$scope.current_name);
                                                        } else {
                                                            $('.loader').hide();
                                                            alert("You are not allowed to edit this.");
                                                            $scope.edit = false;
                                                        }
                                                    }
                                                } else {
                                                    $('.loader').hide();
                                                    alert(response.data.message);
                                                }
                                            }, function (error) {
                                                $('.loader').hide();
                                                console.log(error);
                                            });
                                        }
                                    }
                                    //End Login Function

                                    //Approval function :- To Approve running activity by auth & privilege chk as well as sent that activity to next approval which exists in workflow
                                    //Created by Bhupendra 
                                    //Date : 28/01/2020
                                    $scope.getapproved = function (roomcode, actlogtblid, actid, workflowstatus, workflownextstep, roleid, empid, empname, email, remark, actstatus) {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getapproved',
                                            method: "POST",
                                            data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&workflowstatus=" + workflowstatus + "&workflownextstep=" + workflownextstep + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&actstatus=" + actstatus,

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            alert(response.data.message);
                                            if (response.data.message == "Approved Successfully")
                                            {
                                                if (response.data.next_step == "-1")
                                                {
                                                    window.location.href = "<?php echo base_url() ?>home";
                                                } else
                                                {
                                                    $scope.sampling_rod_id = "";
                                                    $scope.dies_no = "";
                                                    $scope.batch_no = "";
                                                    $scope.product_no = "";
                                                    $scope.current = '-1';
                                                    $scope.product_desc = "";
                                                    $scope.getInprogressActivityList(actlogtblid, '');
                                                    $scope.resetLoginForm();
                                                    angular.element("#btnclose").trigger('click');
                                                    location.reload();
                                                    $('.loader').hide();
                                                }
                                            }
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                    //*********************************************************************************

                                    //Stoped function :- To Stop running activity by auth chk.
                                    //Created by Bhupendra 
                                    //Date : 28/01/2020                                    
                                    $scope.getstoped = function (roomcode, actlogtblid, actid, roleid, empid, empname, email, remark) {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getstoped',
                                            method: "POST",
                                            data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark,

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.getInprogressActivityList(actlogtblid, '');
                                            alert(response.data.message);
                                            $scope.resetLoginForm();
                                            angular.element("#btnclose").trigger('click');
                                            $('.loader').hide();
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                    //********************************************************************************

                                    //Start function :- To Start new activity by auth chk.
                                    //Created by Bhupendra 
                                    //Date : 4/02/2020
                                    var acttblid = 0;
                                    $scope.getstart = function (roomcode, processid, equipmentcode, actid, actname, product_no, batch_no, roleid, empid, empname, email, remark, docno, sampling_rod_id, dies_no, actname2,current_name) {

                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getstarted',
                                            method: "POST",
                                            data: "roomcode=" + roomcode + "&processid=" + processid + "&equipmentcode=" + equipmentcode + "&actid=" + actid + "&actname=" + actname + "&product_no=" + product_no + "&batch_no=" + batch_no + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&docno=" + docno + "&sampling_rod_id=" + sampling_rod_id + "&dies_no=" + dies_no + "&module_id=" + mst_activity + "&actname2=" + actname2 + "&headerRecordid=" + $scope.headerRecordid+"&current_name="+current_name,

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            alert(response.data.message);
                                            $scope.stopApprove = 1;
                                            $scope.resetLoginForm();
                                            var acttblid = response.data.acttblid;
                                            $scope.getInprogressActivityList(acttblid, 'start');
                                            angular.element("#btnclose").trigger('click');
                                            $('.loader').hide();
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                    //*********************************************************************************************

                                    //Start function :- To Update Activity.
                                    //Created by Rahul Chauhan 
                                    //Date : 06/05/2020

                                    $scope.updatesubmit = function (roomcode, processid, equipmentcode, actid, actname, product_no, batch_no, roleid, empid, empname, email, remark, docno, sampling_rod_id, dies_no, actname2, update_id,current_name) {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/updatestarted',
                                            method: "POST",
                                            data: "roomcode=" + roomcode + "&processid=" + processid + "&equipmentcode=" + equipmentcode + "&actid=" + actid + "&actname=" + actname + "&product_no=" + product_no + "&batch_no=" + batch_no + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&docno=" + docno + "&sampling_rod_id=" + sampling_rod_id + "&dies_no=" + dies_no + "&module_id=" + mst_activity + "&actname2=" + actname2 + "&update_id=" + update_id+"&current_name="+current_name,

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            alert(response.data.message);
                                            $scope.stopApprove = 1;
                                            $scope.resetLoginForm();
                                            $scope.edit = false;
                                            $scope.getInprogressActivityList(response.data.acttblid, '');
                                            angular.element("#btnclose").trigger('click');
                                            $('.loader').hide();
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                    $scope.CancilUpdate = function () {
                                        $scope.edit = false;
                                    }
                                    //*********************************************************************************************
                                    //This function :- is used inprogress activity by last inserted id.
                                    //Created by Bhupendra 
                                    //Date : 4/02/2020
                                    $scope.inProgressActivityData = [];
                                    $scope.getInprogressActivityList = function (acttblid, type) {
                                        if (acttblid > 0) {
                                            var query = '?actid=' + acttblid + "&doc_id=";
                                        } else {
                                            var query = '?actid=&doc_id=' + doc_id;
                                        }
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetInprogressActivityList' + query,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.inProgressActivityData = response.data.in_progress_activity;
                                            if ($scope.inProgressActivityData.length > 0) {
                                                $scope.current = $scope.inProgressActivityData[0].act_name;
                                                $scope.doc_no = $scope.inProgressActivityData[0].doc_id;
                                                if (acttblid > 0 && type == 'start') {

                                                    if (doc_id != '') {
                                                        var uri = window.location.href;
                                                        window.location.href = updateQueryStringParameter(uri, "doc_id", $scope.doc_no);
                                                    } else {
                                                        window.location.href = window.location.href + "&doc_id=" + $scope.doc_no;
                                                    }

                                                }
                                            }

                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getInprogressActivityList();



                                    //*******************Rahul's code Started**********************//
                                    $scope.editTrasaction = function () {
                                        if (confirm("Do you really want to edit ?"))
                                        {
                                            $scope.edit = true;
                                            $scope.docno = $scope.inProgressActivityData[0].doc_id;
                                            $scope.sampling_rod_id = $scope.inProgressActivityData[0].equip_code;
                                            $scope.current = $scope.inProgressActivityData[0].act_name;
                                            $scope.current_name = $scope.masterActivityData[$scope.current]['field_value'];
                                            $scope.dies_no = $scope.inProgressActivityData[0].dies_no;
                                            if ($scope.batchInProductData.length > 0) {
                                                $scope.batch_no = $scope.batchInProductData[0].batch_no;
                                                $scope.product_no = $scope.batchInProductData[0].product_code;
                                            } else {
                                                $scope.batch_no = $scope.inProgressActivityData[0].batch_no;
                                                $scope.product_no = $scope.inProgressActivityData[0].product_code;
                                            }
                                            $scope.getProductDetail($scope.product_no);
                                        }
                                    }

                                    //*******************End Rahul's code***************************//
                                    //Reset Login Form
                                    $scope.resetLoginForm = function () {
                                        $scope.username = "";
                                        $scope.password = "";
                                        $scope.remark = "";
                                    }

                                    function updateQueryStringParameter(uri, key, value) {
                                        var re = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
                                        if (value === undefined) {
                                            if (uri.match(re)) {
                                                return uri.replace(re, '$1$2');
                                            } else {
                                                return uri;
                                            }
                                        } else {
                                            if (uri.match(re)) {
                                                return uri.replace(re, '$1' + key + "=" + value + '$2');
                                            } else {
                                                var hash = '';
                                                if (uri.indexOf('#') !== -1) {
                                                    hash = uri.replace(/.*#/, '#');
                                                    uri = uri.replace(/#.*/, '');
                                                }
                                                var separator = uri.indexOf('?') !== -1 ? "&" : "?";
                                                return uri + separator + key + "=" + value + hash;
                                            }
                                        }
                                    }

                                });
</script>