<div  ng-app="swabApp" ng-controller="swabCtrl" ng-cloak>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>sf_home">Home</a></li>
            <li class="breadcrumb-item"><a href="#">{{room_code_show}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Swab Sample Record</li>
        </ol>
    </nav>
    <section class="contentBox blueBorder formWrapper">
            <div class="contentHeader">
                <div class="row">
                    <div class="col-12">
                        <h1>Header Details</h1>
                    </div>
                </div>
            </div>
            <div class="contentBody">
                <form>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <ul class="activityList">
                                    <li style="margin-left: 100px;"><a><b>Form No : </b>{{formno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Version No : </b>{{versionno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Effective Date : </b>{{effectivedate | date:'dd-MMM-yyyy'}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Retired Date : </b>{{retireddate | date:'dd-MMM-yyyy'}} </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    <section class="contentBox blueBorder formWrapper">


        <div class="contentHeader">

            <div class="row">
                <div class="col-12">
                    <h1>Swab Sample Record</h1>
                </div>
            </div>

        </div>

        <div class="contentBody"  ng-show="ComDataArray.length == 0 || edit == true">
            <form name="swabForm" novalidate>
                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Document No.</label>
                            <input type="text" class="form-control" value="{{docno}}" readonly>    
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>AR Number</label>
                            <input class="form-control" type="text" name="ar_number" maxlength="20" ng-model="ar_number" required >
                        </div>
                    </div>


                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Equipment ID</label>
                            <select class="chzn-select form-control" tabindex="4" ng-change="getEquipmentName()"  name="equipment_code" data-placeholder="Search Equipment No." ng-options="dataObj['equipment_code'] as (dataObj.equipment_code +  ' (' +  dataObj.equipment_name +  ')') for dataObj in equipmentData"  ng-model="equipment_code" required chosen>
                                <option value="">Select Equipment</option>
                            </select> 
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Equipment Description</label>
                            <input type="text" class="form-control" value="{{equipment_desc}}" readonly>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Previous Batch No.</label>
                            <input class="form-control" type="text" placeholder="Batch No." maxlength="10" name="batch_no" ng-model="batch_no" >
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Previous Product No.</label>
                            <select  class="form-control" tabindex="4" ng-change="getProductDetail(product_no)"  name="product_no" data-placeholder="Search Product No." ng-options="dataObj['product_code'] as (dataObj.product_code) for dataObj in productList"  ng-model="product_no" required chosen>
                                <option value="">Select Product code</option>
                            </select>

                            <p ng-if="product_desc != ''"><b>{{product_desc}}</b></p>
                        </div>
                    </div>                    
                </div>


                <div class="row">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <button ng-show="edit" class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancel</button>&nbsp;&nbsp;&nbsp;
                            <button ng-show="edit" ng-click="setbtntext('update');" ng-disabled="swabForm.$invalid" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Update</button>&nbsp;&nbsp;&nbsp;
                            <button ng-show="!edit" ng-click="setbtntext('submit');" ng-disabled="swabForm.$invalid" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Done By</button>
                            <!--<button ng-show="!edit" disabled ng-click="setbtntext('approval');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button>-->

                        </div>
                    </div>
                </div>

            </form>
        </div>
        <div class="contentBody"  ng-if="ComDataArray.length > 0 && edit == false">
            <form>
                <div class="row">

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Document No.</label>
                            <input type="text" class="form-control" value="{{ComDataArray[0].doc_no}}" readonly>    
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>AR Number</label>
                            <input type="text" class="form-control" value="{{ComDataArray[0].ar_numer}}" readonly> 
                        </div>
                    </div>


                </div>


                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Equipment ID</label>
                            <input type="text" class="form-control" value="{{ComDataArray[0].equipment_id}}" readonly> 
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Equipment Description</label>
                            <input type="text" class="form-control" value="{{ComDataArray[0].equipment_desc}}" readonly> 
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Previous Batch No.</label>
                            <input type="text" class="form-control" value="{{ComDataArray[0].pre_barch_no}}" readonly> 
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Previous Product No.</label>
                            <input type="text" class="form-control" value="{{ComDataArray[0].pre_product_code}}" readonly> 
                        </div>
                    </div>                    
                </div>


                <div class="row">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <button ng-click="editTrasaction()" class="button smallBtn primaryBtn" >Edit</button>&nbsp;&nbsp;&nbsp;
                            <button ng-click="setbtntext('approval');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button>

                        </div>
                    </div>
                </div>

            </form>
        </div>


    </section>
    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>

<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script>
                                var app = angular.module("swabApp", ['angular.chosen', 'blockUI']);
                                app.directive('chosen', function ($timeout) {

                                    var linker = function (scope, element, attr) {

                                        scope.$watch('equipmentData', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);
                                        scope.$watch('productList', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);
                                        $timeout(function () {
                                            element.chosen();
                                        }, 0, false);
                                    };
                                    return {
                                        restrict: 'A',
                                        link: linker
                                    };
                                });
                                app.controller("swabCtrl", function ($scope, $http, $filter, blockUI) {
                                    var urlParams = new URLSearchParams(window.location.search);
                                    var room_id = urlParams.get('room_id');
                                    var room_code = urlParams.get('room_code');
                                    var processid = urlParams.get('processid');
                                    var actid = urlParams.get('actid');
                                    var actname = urlParams.get('actname');

                                    // get header detail 
                                $scope.headerRecordid = urlParams.get('headerid');
                                $scope.formno = urlParams.get('formno');
                                $scope.versionno = urlParams.get('versionno');
                                $scope.effectivedate = urlParams.get('effectivedate');
                                $scope.retireddate = urlParams.get('retireddate');
                                //end

                                    $scope.room_code_show = room_code;
                                    $scope.roleList = [];
                                        $scope.getRoleList = function () {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList',
                                                method: "GET",
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.roleList = response.data.role_list;
                                            }, function (error) { // optional

                                                console.log("Something went wrong.Please try again");
                                            });
                                        }
                                        $scope.getRoleList();
                                    $scope.equipmentData = [];
                                    $scope.getEquipmentList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetEquipmentlist',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.equipmentData = response.data.equipment_list;
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    //Get Document Number code
                                    $scope.docno = "";
                                    // $scope.getdocno = function () {
                                    //     $http({
                                    //         url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getdocno',
                                    //         method: "GET",
                                    //         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    //     }).then(function (response) {
                                    //         $scope.docno = response.data.docno;
                                    //     }, function (error) { // optional

                                    //         console.log("Something went wrong.Please try again");
                                    //     });
                                    // }
                                    // $scope.getdocno();
                                    //End of Get Document number function code
                                    $scope.getEquipmentList();
                                    $scope.LastActivityData = [];
                                    $scope.getEquipmentName = function () {
                                        $scope.equipment_desc = $filter('filter')($scope.equipmentData, {equipment_code: $scope.equipment_code})[0].equipment_name;
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetLastBactProductActivity?equipment_code=' + $scope.equipment_code,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.LastActivityData = response.data.last_activity_data;
                                            if ($scope.LastActivityData.length > 0) {
                                                $scope.batch_no = $scope.LastActivityData[0].batch_no;
                                                $scope.product_no = $scope.LastActivityData[0].product_code;
                                                $scope.product_desc = $filter('filter')($scope.productList, {product_code: $scope.product_n})[0].product_name;
                                            } else {
                                                $scope.batch_no = "";
                                                $scope.product_no = "";
                                                $scope.product_desc = "";
                                            }
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");
                                        });
                                    }

                                    $scope.productList = [];
                                    $scope.getProductList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.productList = response.data.product_list;
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getProductList();
                                    $scope.product_desc = "";
                                    $scope.getProductDetail = function (dataObj) {
                                        $scope.product_desc = $filter('filter')($scope.productList, {product_code: dataObj})[0].product_name;
                                    }

                                    //*******************Bhupendra's code Started**********************//

                                    $scope.setbtntext = function (btntext)
                                    {
                                        $scope.btntext = btntext;
                                    }
                                    $scope.edit = false;
                                    //Login Function
                                    //Created by Bhupendra
                                    //Date : 28/01/2020                                    
                                    $scope.login = function () {
                                        var response = confirm("Do you really want to perform this task?");
                                        if (response == true)
                                        {
                                            $('.loader').show();
                                            pwd2 = SHA256($scope.password);
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                                method: "POST",
                                                data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&module_id=" + actid + "&module_type=log" + "&action=" + $scope.btntext+ "&auth=" + btoa(btoa($scope.password))+"&act_id="+actid,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                if (response.data.message == "")
                                                {
                                                    if ($scope.btntext == 'approval')
                                                    {
                                                        //Calling Approval function
                                                        //Created by Bhupendra
                                                        //Date : 10/02/2020
                                                        $scope.getapproved(room_code, $scope.ComDataArray[0].id, $scope.ComDataArray[0].activity_id, $scope.ComDataArray[0].workflowstatus, $scope.ComDataArray[0].workflownextstep, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, 'stop', $scope.ComDataArray[0].doc_id)
                                                    } else if ($scope.btntext == 'submit')
                                                    {
                                                        //Calling Stoped function
                                                        //Created by Bhupendra
                                                        //Date : 10/02/2020
                                                        $scope.getsubmit(room_code, processid, actid, actname, $scope.docno, $scope.ar_number, $scope.equipment_code, $scope.equipment_desc, $scope.product_no, $scope.batch_no, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark)
                                                    } else if ($scope.btntext == 'edit')
                                                    {
                                                        //edit Functionality
                                                        //Created by Rahul
                                                        //Date : 04/05/2020
//                                                        var userdata = response.data.userdata;
//                                                        if (userdata.role_id > $scope.ComDataArray[0].done_by_role_id) {
//                                                            $scope.edit = true;
//                                                            $scope.docno = $scope.ComDataArray[0].doc_no;
//                                                            $scope.is_in_workflow = $scope.ComDataArray[0].is_in_workflow;
//                                                            $scope.ar_number = $scope.ComDataArray[0].ar_numer;
//                                                            $scope.equipment_code = $scope.ComDataArray[0].equipment_id;
//                                                            $scope.equipment_desc = $scope.ComDataArray[0].equipment_desc;
//                                                            $scope.batch_no = $scope.ComDataArray[0].pre_barch_no;
//                                                            $scope.product_no = $scope.ComDataArray[0].pre_product_code;
//                                                            $scope.getProductDetail($scope.product_no);
//                                                        } else {
//                                                            alert("You are not allowed to edit this.");
//                                                            $scope.edit = false;
//                                                        }
//                                                        $scope.resetLoginForm();
//                                                        angular.element("#btnclose").trigger('click');
                                                    } else if ($scope.btntext == 'update') {
                                                        //update Functionality
                                                        //Created by Rahul
                                                        //Date : 04/05/2020
                                                        var role = "";
                                                    if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                        if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }

                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep == 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {

                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflowstatus})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;

                                                    } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time != null) {
                                                        if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                            var workflowtype = 'start';
                                                        } else {
                                                            var workflowtype = 'stop';
                                                        }
                                                        var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                        var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                    }
//                                                  
                                                    var userdata = response.data.userdata;
                                                    if (userdata.role_id >= $scope.ComDataArray[0].last_approval.role_id && userdata.role_id > 1) {
                                                            $scope.updatesubmit(room_code, processid, actid, actname, $scope.docno, $scope.ar_number, $scope.equipment_code, $scope.equipment_desc, $scope.product_no, $scope.batch_no, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.ComDataArray[0].edit_id, $scope.is_in_workflow);
                                                        } else {
                                                            $('.loader').hide();
                                                            alert("You are not allowed to edit this.");
                                                            $scope.edit = false;
                                                        }
                                                    }
                                                } else {
                                                    $('.loader').hide();
                                                    alert(response.data.message);
                                                }
                                            }, function (error) {
                                                $('.loader').hide();
                                                console.log(error);
                                            });
                                        }
                                    }
                                    //End Login Function

                                    //Approval function :- To Approve running activity by auth & privilege chk as well as sent that activity to next approval which exists in workflow
                                    //Created by Bhupendra
                                    //Date : 28/01/2020
                                    $scope.getapproved = function (roomcode, actlogtblid, actid, workflowstatus, workflownextstep, roleid, empid, empname, email, remark, actstatus, docno) {

                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/com_no_approval_fun2',
                                            method: "POST",
                                            data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&workflowstatus=" + workflowstatus + "&workflownextstep=" + workflownextstep + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&actstatus=" + actstatus + "&docno=" + docno + "&tblname=pts_trn_swab_sample_record",

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            alert(response.data.message);
                                            if (response.data.next_step == "-1")
                                            {
                                                window.location.href = "<?php echo base_url() ?>home";
                                                $('.loader').hide();
                                            } else
                                            {
                                                $scope.resetLoginForm();
                                                $scope.ar_number = "";
                                                $scope.equipment_code = "";
                                                $scope.equipment_desc = "";
                                                $scope.product_no = "";
                                                $scope.batch_no = "";
                                                location.reload();
                                                angular.element("#btnclose").trigger('click');
                                                $scope.getLastFilterActivity();
                                                $scope.getLastProductAndBatchNumber($scope.process_log, $scope.portable_log);
                                                $('.loader').hide();
                                                //$scope.getdocno();
                                            }

                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                    //*********************************************************************************
                                    //Start function :- To Start new activity by auth chk.
                                    //Created by Bhupendra
                                    //Date : 4/02/2020
                                    $scope.getsubmit = function (roomcode, processid, actid, actname, docno, ar_number, equipment_code, equipment_desc, product_no, batch_no, roleid, empid, empname, email, remark) {

                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/submit_swab_sample_record',
                                            method: "POST",
                                            data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&ar_number=" + ar_number + "&equipment_code=" + equipment_code + "&equipment_desc=" + equipment_desc + "&product_no=" + product_no + "&batch_no=" + batch_no + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&headerRecordid=" + $scope.headerRecordid,

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            alert(response.data.message);
                                            $scope.resetLoginForm();
                                            $scope.ar_number = "";
                                            $scope.equipment_code = "";
                                            $scope.equipment_desc = "";
                                            $scope.product_no = "";
                                            $scope.batch_no = "";
                                            $scope.product_desc = "";
                                            angular.element("#btnclose").trigger('click');
                                            $scope.getLastFilterActivity();
                                            $('.loader').hide();
                                            $scope.getLastProductAndBatchNumber($scope.process_log, $scope.portable_log);
                                            $('.loader').hide();
                                            //$scope.getdocno();

                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                    //*********************************************************************************************

                                    //*********************************************************************************
                                    //Start function :- To Update started Activity.
                                    //Created by Raul Chauhan
                                    //Date : 05/05/2020
                                    $scope.updatesubmit = function (roomcode, processid, actid, actname, docno, ar_number, equipment_code, equipment_desc, product_no, batch_no, roleid, empid, empname, email, remark, update_id, is_in_workflow) {

                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/update_swab_sample_record',
                                            method: "POST",
                                            data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&ar_number=" + ar_number + "&equipment_code=" + equipment_code + "&equipment_desc=" + equipment_desc + "&product_no=" + product_no + "&batch_no=" + batch_no + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&update_id=" + update_id + "&is_in_workflow=" + is_in_workflow,

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            alert(response.data.message);
                                            $scope.edit = false;
                                            angular.element("#btnclose").trigger('click');
                                            $scope.resetLoginForm();
                                            $scope.getLastFilterActivity();
                                            $('.loader').hide();
                                            $scope.getLastProductAndBatchNumber($scope.process_log, $scope.portable_log);
                                            //$('.loader').hide();
                                            //$scope.getdocno();

                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }

                                    $scope.CancilUpdate = function () {
                                        $scope.edit = false;
                                    }
                                    //*********************************************************************************************


                                    //Get Last unapproved Room Activity
                                    $scope.ComDataArray = [];
                                    $scope.getLastFilterActivity = function () {
                                        $scope.ComDataArray = [];
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetLastFilterActivity?room_code=' + room_code + "&table_name=pts_trn_swab_sample_record",
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.ComDataArray = response.data.row_data;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getLastFilterActivity();

                                    //*******************Rahul's code Started**********************//
                                    $scope.editTrasaction = function () {
                                        if (confirm("Do you really want to edit ?"))
                                        {
                                            $scope.edit = true;
                                            $scope.docno = $scope.ComDataArray[0].doc_no;
                                            $scope.is_in_workflow = $scope.ComDataArray[0].is_in_workflow;
                                            $scope.ar_number = $scope.ComDataArray[0].ar_numer;
                                            $scope.equipment_code = $scope.ComDataArray[0].equipment_id;
                                            $scope.equipment_desc = $scope.ComDataArray[0].equipment_desc;
                                            $scope.batch_no = $scope.ComDataArray[0].pre_barch_no;
                                            $scope.product_no = $scope.ComDataArray[0].pre_product_code;
                                            $scope.getProductDetail($scope.product_no);
                                        }
                                    }

                                    //*******************End Rahul's code***************************//
                                    //Reset Login Form
                                    $scope.resetLoginForm = function () {
                                        $scope.username = "";
                                        $scope.password = "";
                                        $scope.remark = "";
                                    }
                                });
</script>