<div ng-app="prefilterApp" ng-controller="prefilterCtrl" ng-cloak>

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>sf_home">Home</a></li>
            <li class="breadcrumb-item"><a href="#">{{room_code_show}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Pre-Filter Cleaning Record of LAF</li>
        </ol>
    </nav>
    <section class="contentBox blueBorder formWrapper">
        <div class="contentHeader">
            <div class="row">
                <div class="col-12">
                    <h1>Header Details</h1>
                </div>
            </div>
        </div>
        <div class="contentBody">
            <form>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <ul class="activityList">
                                <li style="margin-left: 100px;"><a><b>Form No : </b>{{formno}} </a></li>
                                <li style="margin-left: 100px;"><a><b>Version No : </b>{{versionno}} </a></li>
                                <li style="margin-left: 100px;"><a><b>Effective Date : </b>{{effectivedate| date:'dd-MMM-yyyy'}} </a></li>
                                <li style="margin-left: 100px;"><a><b>Retired Date : </b>{{retireddate| date:'dd-MMM-yyyy'}} </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <section class="contentBox blueBorder formWrapper">


        <div class="contentHeader">

            <div class="row">
                <div class="col-12">
                    <h1>Pre-Filter Cleaning Record of LAF</h1>
                </div>
            </div>

        </div>
        <div class="contentBody" ng-show="batchRunning">
            Batch In is not allowed for this activity. Please Perform Batch Out First.
        </div>
        <div class="contentBody" ng-show="!batchRunning">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group" ng-if="ComDataArray.length == 0">
                        <label><span class="asterisk">* </span>Cleaning Verification</label>
                        <p> <input type="radio" id="from" name="seletion_status" ng-click="getSaveData();getRoomBatchInProductData();getselection('from')" ng-model="seletion_status" value="from" required> &nbsp;&nbsp;&nbsp;&nbsp;From&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input required id="to" type="radio" name="seletion_status" ng-click="getSaveData();GetPreBactProductofPrefilter();getselection('to')" ng-model="seletion_status" value="to">&nbsp;&nbsp;&nbsp;&nbsp;To</p>
                        <!--                            <div class="switch-field col2">
                                                        <input type="radio" id="radio-one" name="switch-one" value="yes" checked="">
                                                        <label for="radio-one">From</label>
                                                        <input type="radio" id="radio-two" name="switch-one" value="no">
                                                        <label for="radio-two">To</label>
                                                    </div>-->

                    </div>
                    <div class="form-group" ng-if="ComDataArray.length > 0">
                        <label><span class="asterisk">* </span>Cleaning Verification</label>
                        <p> <input type="radio" id="condition" name="seletion_status" ng-click="getSaveData();getRoomBatchInProductData();getselection('from')" ng-model="seletion_status" value="from" disabled> &nbsp;&nbsp;&nbsp;&nbsp;From&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input required type="radio" name="seletion_status" ng-click="getSaveData();GetPreBactProductofPrefilter();getselection('to')" ng-model="seletion_status" value="to" disabled>&nbsp;&nbsp;&nbsp;&nbsp;To</p>
                        <!--                            <div class="switch-field col2">
                                                        <input type="radio" id="radio-one" name="switch-one" value="yes" checked="">
                                                        <label for="radio-one">From</label>
                                                        <input type="radio" id="radio-two" name="switch-one" value="no">
                                                        <label for="radio-two">To</label>
                                                    </div>-->

                    </div>
                </div>
            </div>

            <form name="preFilterForm" novalidate ng-show="ComDataArray.length == 0 || edit == true">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Document No.</label>
                            <input type="text" class="form-control" value="{{docno}}" readonly>    
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Product No.</label>
                            <div class="col-sm-12" ng-show="batchInProductData.length == 0 && PreBactProduct.length == 0">
                                <select  class="form-control" tabindex="4" ng-change="getProductDetail(product_code)"  name="product_code" data-placeholder="Search Product No." ng-options="dataObj['product_code'] as (dataObj.product_code) for dataObj in productData"  ng-model="product_code" required chosen>
                                    <option value="">Select Product code</option>
                                </select>
                            </div>
                            <div class="col-sm-12">
                                <p><b  ng-show="batchInProductData.length > 0 || PreBactProduct.length > 0">{{product_code}}</b>
                                    <b ng-if="product_desc != ''">{{product_desc}}</b></p></p>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="row">


                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Batch No.</label>
                            <p ng-show="batchInProductData.length > 0 || PreBactProduct.length > 0"><b>{{batch_no}}</b></p>
                            <input ng-show="batchInProductData.length == 0 && PreBactProduct.length == 0" class="form-control" maxlength="10" type="text" name="batch_no" ng-model="batch_no" >   
                        </div>
                    </div>
                </div>


                <div class="dispensingBooth" ng-if="seletion_status == 'from'">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="formSubHeading">Dispensing Booth</div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Pre-Filter Cleaning</label><br>
                                <input type="radio" name="pre_filter_cleaning" ng-model="pre_filter_cleaning" ng-click="getcleaning('yes')" value="yes" required> YES&nbsp;&nbsp;<input type="radio" ng-click="getcleaning('no')" name="pre_filter_cleaning" ng-model="pre_filter_cleaning" value="no" required>NO
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Outer Surface Cleaning</label> <br>

                                <input type="radio" name="outer_surface_cleaning" ng-model="outer_surface_cleaning" ng-click="getouterSurface('yes')" value="yes" required> YES&nbsp;&nbsp;<input type="radio" ng-click="getouterSurface('no')" name="outer_surface_cleaning" ng-model="outer_surface_cleaning" value="no" required>NO

                            </div>
                        </div>
                    </div>
                </div>


                <div class="row" ng-if="seletion_status == 'from'">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <button ng-show="edit" class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancil</button>&nbsp;&nbsp;&nbsp;
                            <button ng-show="edit" ng-disabled="preFilterForm.$invalid" ng-click="setbtntext('update');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Update</button>&nbsp;&nbsp;&nbsp;
                            <button ng-show="!edit" ng-disabled="preFilterForm.$invalid || batch_no == ''" ng-click="setbtntext('submit');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Activity Start</button> &nbsp;&nbsp;&nbsp;&nbsp;
                            <button ng-show="!edit" disabled ng-click="setbtntext('stop');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Activity Stop</button> &nbsp;&nbsp;&nbsp;&nbsp;
                            <button ng-show="!edit" disabled ng-click="setbtntext('approval');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Activity Approve</button>

                        </div>
                    </div>
                </div>
                <div class="row" ng-show="seletion_status == 'to'">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <button ng-show="edit" class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancel</button>&nbsp;&nbsp;&nbsp;
                            <button ng-show="edit" ng-disabled="preFilterForm.$invalid" ng-click="setbtntext('update');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Update</button>&nbsp;&nbsp;&nbsp;
                            <button ng-show="!edit" ng-disabled="preFilterForm.$invalid" ng-click="setbtntext('submit');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Done By</button> &nbsp;&nbsp;&nbsp;&nbsp;
                            <button ng-show="!edit" disabled ng-click="setbtntext('approval');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button>

                        </div>
                    </div>
                </div>

            </form>
            <div ng-if="ComDataArray.length > 0 && edit == false">



                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Document No.</label>
                            <input type="text" class="form-control" value="{{ComDataArray[0].doc_no}}" readonly>    
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Product No.</label>
                            <input type="text" class="form-control" value="{{ComDataArray[0].product_code}}" readonly>  

                        </div>
                    </div>


                </div>
                <div class="row">


                    <div class="col-sm-6">
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Batch No.</label>
                            <input type="text" class="form-control" value="{{ComDataArray[0].batch_no}}" readonly>  
                        </div>
                    </div>
                </div>


                <div class="dispensingBooth" ng-if="seletion_status == 'from'">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="formSubHeading">Dispensing Booth</div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Pre-Filter Cleaning</label><br>
                                <p> {{ComDataArray[0].pre_filter_cleaning}}</p>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Outer Surface Cleaning</label> <br>

                                <p>{{ComDataArray[0].outer_surface_cleaning}}</p>

                            </div>
                        </div>
                    </div>
                </div>


                <div class="row" ng-if="seletion_status == 'from'">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <button ng-click="editTrasaction()" class="button smallBtn primaryBtn" >Edit</button>&nbsp;&nbsp;&nbsp;
                            <button ng-disabled="(ComDataArray[0].activity_stop > 0)" ng-click="setbtntext('stop');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Activity Stop</button> &nbsp;&nbsp;&nbsp;&nbsp;
                            <button ng-disabled="(ComDataArray[0].activity_stop == '' || ComDataArray[0].activity_stop == null)" ng-click="setbtntext('approval');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Activity Approve</button>

                        </div>
                    </div>
                </div>
                <div class="row" ng-show="seletion_status == 'to'">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <button ng-click="editTrasaction()" class="button smallBtn primaryBtn" >Edit</button>&nbsp;&nbsp;&nbsp;
                            <button ng-click="setbtntext('approval');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </section>
    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>

<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script type="text/javascript">
                                var app = angular.module("prefilterApp", ['angular.chosen', 'blockUI']);
                                app.directive('chosen', function ($timeout) {

                                    var linker = function (scope, element, attr) {

                                        scope.$watch('apparatusData', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);
                                        scope.$watch('productData', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);
                                        $timeout(function () {
                                            element.chosen();
                                        }, 0, false);
                                    };
                                    return {
                                        restrict: 'A',
                                        link: linker
                                    };
                                });
                                app.controller("prefilterCtrl", function ($scope, $http, $filter, blockUI) {
                                    var urlParams = new URLSearchParams(window.location.search);
                                    var room_code = urlParams.get('room_code');
                                    var processid = urlParams.get('processid');
                                    var actid = urlParams.get('actid');
                                    var actname = urlParams.get('actname');

                                    // get header detail 
                                    $scope.headerRecordid = urlParams.get('headerid');
                                    $scope.formno = urlParams.get('formno');
                                    $scope.versionno = urlParams.get('versionno');
                                    $scope.effectivedate = urlParams.get('effectivedate');
                                    $scope.retireddate = urlParams.get('retireddate');
                                    //end

                                    $scope.room_code_show = room_code;
                                    $scope.roleList = [];
                                    $scope.getRoleList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.roleList = response.data.role_list;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getRoleList();
                                    $scope.batchInProductData = [];
                                    $scope.PreBactProduct = [];
                                    $scope.batchRunning = false;
                                    $scope.getRoomBatchInProductData = function () {
                                        $scope.batchInProductData = [];
                                        $scope.PreBactProduct = [];
                                        $scope.batch_no = "";
                                        $scope.product_desc = "";
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getRoomBatchInProductData?room_code=' + room_code,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.batchInProductData = response.data.batch_product_list;
                                            if ($scope.batchInProductData.length > 0) {
                                                $scope.batchRunning = true;
                                                alert("Batch In is not allowed for this activity. Please Perform Batch Out First.");
//                                                $scope.batch_no = $scope.batchInProductData[0].batch_no;
//                                                $scope.product_code = $scope.batchInProductData[0].product_code;
//                                                $scope.product_desc = $scope.batchInProductData[0].product_desc;
                                            }else{
                                                $scope.batch_no = $scope.product_code = $scope.product_desc = '';
                                            }
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.GetPreBactProductofPrefilter = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetPreBactProductofPrefilter?room_code=' + room_code,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.PreBactProduct = response.data.PreBactProduct;
                                            if ($scope.PreBactProduct.length > 0) {
                                                $scope.batch_no = $scope.PreBactProduct[0].batch_no;
                                                $scope.product_code = $scope.PreBactProduct[0].product_code;
                                                $scope.product_desc = $scope.PreBactProduct[0].product_name;
                                            }else{
                                                $scope.batch_no = $scope.product_code = $scope.product_desc = '';
                                            }
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getRoomBatchInProductData();
                                    $scope.pre_filter_cleaning = 'yes';
                                    $scope.outer_surface_cleaning = 'yes';

                                    $scope.seletion_status = 'from';
                                    $scope.productData = [];
                                    $scope.getProductList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.productData = response.data.product_list;
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    //Get Document Number code
                                    $scope.docno = "";
                                    // $scope.getdocno = function () {
                                    //     $http({
                                    //         url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getdocno',
                                    //         method: "GET",
                                    //         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    //     }).then(function (response) {
                                    //         $scope.docno = response.data.docno;
                                    //     }, function (error) { // optional

                                    //         console.log("Something went wrong.Please try again");
                                    //     });
                                    // }
                                    // $scope.getdocno();
                                    //End of Get Document number function code
                                    $scope.getProductList();
                                    $scope.dispensingBoothData = [];
                                    $scope.getDispensingBoothList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetDispensingBoothList?room_code=' + room_code,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.dispensingBoothData = response.data.booth_list;
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getDispensingBoothList();

                                    $scope.product_desc = "";
                                    $scope.getProductDetail = function (dataObj) {
                                        $scope.productcode = dataObj;
                                        $scope.product_desc = $filter('filter')($scope.productData, {product_code: dataObj})[0].product_name;
                                    }



                                    //*******************Bhupendra's code Started**********************//

                                    $scope.setbtntext = function (btntext)
                                    {
                                        $scope.btntext = btntext;
                                    }
                                    $scope.edit = false;
                                    $scope.outersurface = "yes";
                                    $scope.cleaning = "yes";
                                    $scope.selection = "from";
                                    $scope.seletion_status = "from";
                                    $scope.getouterSurface = function (val) {
                                        $scope.outersurface = val;
                                    }

                                    $scope.getcleaning = function (val) {
                                        $scope.cleaning = val;
                                    }
                                    $scope.getselection = function (val) {
                                        $scope.selection = val;
                                        $scope.seletion_status=$scope.selection;
                                    }

                                    //Login Function
                                    //Created by Bhupendra
                                    //Date : 28/01/2020                                    
                                    $scope.login = function () {
                                        var response = confirm("Do you really want to perform this task?");
                                        if (response == true)
                                        {
                                            $('.loader').show();
                                            pwd2 = SHA256($scope.password);
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                                method: "POST",
                                                data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&module_id=" + actid + "&module_type=log" + "&action=" + $scope.btntext + "&auth=" + btoa(btoa($scope.password)) + "&act_id=" + actid,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                if (response.data.message == "")
                                                {
                                                    if ($scope.btntext == 'approval')
                                                    {
                                                        //Calling Approval function
                                                        //Created by Bhupendra
                                                        //Date : 10/02/2020

                                                        $scope.getapproved(room_code, $scope.ComDataArray[0].id, $scope.ComDataArray[0].activity_id, $scope.ComDataArray[0].workflowstatus, $scope.ComDataArray[0].workflownextstep, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, 'stop', $scope.ComDataArray[0].doc_id, $scope.seletion_status)
                                                    } else if ($scope.btntext == 'submit')
                                                    {
                                                        //Calling Stoped function
                                                        //Created by Bhupendra
                                                        //Date : 10/02/2020
                                                        $scope.getsubmit(room_code, processid, actid, actname, $scope.docno, $scope.selection, $scope.product_code, $scope.batch_no, $scope.cleaning, $scope.outersurface, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark)
                                                    } else if ($scope.btntext == 'stop')
                                                    {
                                                        $scope.getstoped(room_code, $scope.ComDataArray[0].id, $scope.ComDataArray[0].activity_id, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark)
                                                    } else if ($scope.btntext == 'edit')
                                                    {
//                                                        //edit Functionality
//                                                        //Created by Rahul
//                                                        //Date : 06/05/2020
//                                                        var userdata = response.data.userdata;
//                                                        if (userdata.role_id > $scope.filterList[0].done_by_role_id) {
//                                                            $scope.edit = true;
//
//                                                            $scope.docno = $scope.ComDataArray[0].doc_no;
//
//                                                            $scope.seletion_status = $scope.ComDataArray[0].selection_status;
//                                                            $scope.pre_filter_cleaning = $scope.ComDataArray[0].pre_filter_cleaning;
//                                                            $scope.outer_surface_cleaning = $scope.ComDataArray[0].outer_surface_cleaning;
//                                                            if ($scope.batchInProductData.length > 0) {
//                                                                $scope.batch_no = $scope.batchInProductData[0].batch_no;
//                                                                $scope.product_code = $scope.batchInProductData[0].product_code;
//                                                            } else {
//                                                                $scope.batch_no = $scope.ComDataArray[0].batch_no;
//                                                                $scope.product_code = $scope.ComDataArray[0].product_code;
//                                                            }
//                                                            $scope.getProductDetail($scope.product_code);
//                                                            $scope.is_in_workflow = $scope.ComDataArray[0].is_in_workflow;
//                                                            $scope.outersurface = $scope.ComDataArray[0].outer_surface_cleaning;
//                                                            $scope.cleaning = $scope.ComDataArray[0].pre_filter_cleaning;
//                                                            $scope.selection = $scope.ComDataArray[0].selection_status;
//                                                        } else {
//                                                            alert("You are not allowed to edit this.");
//                                                            $scope.edit = false;
//                                                        }
//
//                                                        $scope.resetLoginForm();
//                                                        angular.element("#btnclose").trigger('click');
                                                    } else if ($scope.btntext == 'update')
                                                    {
                                                        //update Functionality
                                                        //Created by Rahul
                                                        //Date : 06/05/2020
                                                        var role = "";
                                                        if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                            if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                                var workflowtype = 'start';
                                                            } else {
                                                                var workflowtype = 'stop';
                                                            }

                                                            var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                            var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                        } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep == 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {

                                                            var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflowstatus})[0].role_id;
                                                            var role = $filter('filter')($scope.roleList, {id: desc})[0].id;

                                                        } else if ($scope.ComDataArray[0].activity_level.length > 0 && $scope.ComDataArray[0].workflownextstep > 0 && $scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time != null) {
                                                            if ($scope.ComDataArray[0].start_time != null && $scope.ComDataArray[0].stop_time == null) {
                                                                var workflowtype = 'start';
                                                            } else {
                                                                var workflowtype = 'stop';
                                                            }
                                                            var desc = $filter('filter')($scope.ComDataArray[0].activity_level, {status_id: $scope.ComDataArray[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                            var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                        }
//                                                  
                                                        var userdata = response.data.userdata;
                                                        if (userdata.role_id >= $scope.ComDataArray[0].last_approval.role_id && userdata.role_id > 1) {
                                                            $scope.updatesubmit(room_code, processid, actid, actname, $scope.docno, $scope.selection, $scope.product_code, $scope.batch_no, $scope.cleaning, $scope.outersurface, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.filterList[0].edit_id, $scope.is_in_workflow);
                                                        } else {
                                                            $('.loader').hide();
                                                            alert("You are not allowed to edit this.");
                                                            $scope.edit = false;
                                                        }
                                                    }
                                                } else {
                                                    $('.loader').hide();
                                                    alert(response.data.message);
                                                }
                                            }, function (error) {
                                                $('.loader').hide();
                                                console.log(error);
                                            });
                                        }
                                    }
                                    //End Login Function
                                    //Approval function :- To Approve running activity by auth & privilege chk as well as sent that activity to next approval which exists in workflow
                                    //Created by Bhupendra
                                    //Date : 28/01/2020
                                    $scope.getapproved = function (roomcode, actlogtblid, actid, workflowstatus, workflownextstep, roleid, empid, empname, email, remark, actstatus, docno, seletion_status) {

                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/com_approval_fun',
                                            method: "POST",
                                            data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&workflowstatus=" + workflowstatus + "&workflownextstep=" + workflownextstep + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&actstatus=" + actstatus + "&docno=" + docno + "&tblname=pts_trn_pre_filter_cleaning",

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            alert(response.data.message);
                                            if (response.data.next_step == "-1")
                                            {
                                                
                                                if (seletion_status == 'to') {
                                                    $http({
                                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/udatePreFilterToStatus',
                                                        method: "POST",
                                                        data: "seletion_status=" + seletion_status,
                                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                    }).then(function (response) {
                                                        window.location.href = "<?php echo base_url() ?>home";
                                                    }, function (error) {
                                                        console.log(error);
                                                    });
                                                }else{
                                                window.location.href = "<?php echo base_url() ?>home";
                                                }
                                                
                                            } else
                                            {
                                                $scope.resetLoginForm();
                                                $scope.batch_no = "";
                                                $scope.product_code = "";
                                                $scope.product_desc = "";
                                                angular.element("#btnclose").trigger('click');
                                                $scope.getLastFilterActivity();
                                                $('.loader').hide();
                                                //$scope.getdocno();
                                            }
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                    //*********************************************************************************
                                    //Start function :- To Start new activity by auth chk.
                                    //Created by Bhupendra
                                    //Date : 4/02/2020
                                    $scope.getsubmit = function (roomcode, processid, actid, actname, docno, seletion_status, product_code, batch_no, pre_filter_cleaning, outer_surface_cleaning, roleid, empid, empname, email, remark) {

                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/submit_pre_filter_cleaning_record',
                                            method: "POST",
                                            data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&seletion_status=" + seletion_status + "&product_code=" + product_code + "&batch_no=" + batch_no + "&pre_filter_cleaning=" + pre_filter_cleaning + "&outer_surface_cleaning=" + outer_surface_cleaning + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&headerRecordid=" + $scope.headerRecordid,

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            if (response.data.message == "Successfully Submited.")
                                            {
                                                alert("Activity Successfully Started.");
                                            } else
                                            {
                                                alert(response.data.message);
                                            }
                                            $scope.resetLoginForm();
                                            angular.element("#btnclose").trigger('click');
                                            $scope.getLastFilterActivity();
                                            $('.loader').hide();
                                            //$scope.getdocno();

                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                    //*********************************************************************************
                                    //Start function :- To Update Activity.
                                    //Created by Rahul Chauhan
                                    //Date : 06/05/2020
                                    $scope.updatesubmit = function (roomcode, processid, actid, actname, docno, seletion_status, product_code, batch_no, pre_filter_cleaning, outer_surface_cleaning, roleid, empid, empname, email, remark, update_id, is_in_workflow) {

                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/update_pre_filter_cleaning_record',
                                            method: "POST",
                                            data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&seletion_status=" + seletion_status + "&product_code=" + product_code + "&batch_no=" + batch_no + "&pre_filter_cleaning=" + pre_filter_cleaning + "&outer_surface_cleaning=" + outer_surface_cleaning + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&update_id=" + update_id + "&is_in_workflow=" + is_in_workflow,

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            if (response.data.message == "Successfully Updated.")
                                            {
                                                alert("Activity Successfully Updated.");
                                            } else
                                            {
                                                alert(response.data.message);
                                            }
                                            $scope.edit = false;
                                            angular.element("#btnclose").trigger('click');
                                            $scope.resetLoginForm();
                                            $scope.getLastFilterActivity();
                                            $('.loader').hide();
                                            //$scope.getdocno();

                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }


                                    //Stoped function :- To Stop running activity by auth chk.
                                    //Created by Bhupendra 
                                    //Date : 15/04/2020  
                                    $scope.getstoped = function (roomcode, actlogtblid, actid, roleid, empid, empname, email, remark) {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getprefilterstopped',
                                            method: "POST",
                                            data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark,

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            alert(response.data.message);
                                            $scope.resetLoginForm();
                                            angular.element("#btnclose").trigger('click');
                                            $scope.getLastFilterActivity();
                                            $('.loader').hide();
                                            //$scope.getdocno();

                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                    //*********************************************************************************************


                                    //Get Last unapproved Room Activity
                                    $scope.filterList = [];
                                    $scope.getLastFilterActivity = function () {
                                        $scope.ComDataArray = [];
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetLastFilterActivity?room_code=' + room_code + "&table_name=pts_trn_pre_filter_cleaning",
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.filterList = response.data.row_data;
                                            $scope.seletion_status=$scope.filterList[0].selection_status;
                                            $scope.getSaveData();
                                            

                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getLastFilterActivity();

                                    $scope.getSaveData = function () {
                                        $scope.ComDataArray = $filter('filter')($scope.filterList, {selection_status: $scope.seletion_status});
                                        //$scope.GetPreBactProductofPrefilter();
                                    }

                                    $scope.CancilUpdate = function () {
                                        $scope.edit = false;
                                    }



                                    //*******************Rahul's code Started**********************//
                                    $scope.editTrasaction = function () {
                                        if (confirm("Do you really want to edit ?"))
                                        {
                                            $scope.edit = true;

                                            $scope.docno = $scope.ComDataArray[0].doc_no;

                                            $scope.seletion_status = $scope.ComDataArray[0].selection_status;
                                            $scope.pre_filter_cleaning = $scope.ComDataArray[0].pre_filter_cleaning;
                                            $scope.outer_surface_cleaning = $scope.ComDataArray[0].outer_surface_cleaning;
                                            if ($scope.batchInProductData.length > 0) {
                                                $scope.batch_no = $scope.batchInProductData[0].batch_no;
                                                $scope.product_code = $scope.batchInProductData[0].product_code;
                                            } else {
                                                $scope.batch_no = $scope.ComDataArray[0].batch_no;
                                                $scope.product_code = $scope.ComDataArray[0].product_code;
                                            }
                                            $scope.getProductDetail($scope.product_code);
                                            $scope.is_in_workflow = $scope.ComDataArray[0].is_in_workflow;
                                            $scope.outersurface = $scope.ComDataArray[0].outer_surface_cleaning;
                                            $scope.cleaning = $scope.ComDataArray[0].pre_filter_cleaning;
                                            $scope.selection = $scope.ComDataArray[0].selection_status;
                                        }
                                    }

                                    //*******************End Rahul's code***************************//
                                    //Reset Login Form
                                    $scope.resetLoginForm = function () {
                                        $scope.username = "";
                                        $scope.password = "";
                                        $scope.remark = "";
                                    }



                                });
</script>