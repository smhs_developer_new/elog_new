<link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
<div ng-app="punchApp" ng-controller="punchLogCtrl" ng-cloak>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>sf_home">Home</a></li>
            <li class="breadcrumb-item"><a href="#">{{room_code_show}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Punch Set allocation Record</li>
        </ol>
    </nav>
    <section class="contentBox blueBorder formWrapper">
        <div class="contentHeader">
            <div class="row">
                <div class="col-12">
                    <h1>Header Details</h1>
                </div>
            </div>
        </div>
        <div class="contentBody">
            <form>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <ul class="activityList">
                                <li style="margin-left: 100px;"><a><b>Form No : </b>{{formno}} </a></li>
                                <li style="margin-left: 100px;"><a><b>Version No : </b>{{versionno}} </a></li>
                                <li style="margin-left: 100px;"><a><b>Effective Date : </b>{{effectivedate | date:'dd-MMM-yyyy'}} </a></li>
                                <li style="margin-left: 100px;"><a><b>Retired Date : </b>{{retireddate | date:'dd-MMM-yyyy'}} </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <section class="contentBox blueBorder formWrapper">
        <div class="contentHeader">

            <div class="row">
                <div class="col-12">
                    <h1>Punch Set allocation Record</h1>
                </div>
            </div>

        </div>
        <!-- Begin Page Content -->
        <div class="contentBody" ng-show="ComDataArray.length == 0 || edit == true">
            <form name="punchSetForm" novalidate>
                <div class="form-row">
                    <div class="col-lg-4 mb-3">
                        <label>Product Code</label>
                        <select  class="form-control" tabindex="4" ng-change="getProductDetail(product_code)"  name="product_code" data-placeholder="Search Product No." ng-options="dataObj['product_code'] as (dataObj.product_code) for dataObj in productData"  ng-model="product_code" required chosen>
                            <option value="">Select Product code</option>
                        </select>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Product Desc.</label><br>
                        {{product_desc}}
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Dimension</label>
                        <input class="form-control" type="text" placeholder="Dimension" name="dimension" ng-model="dimension" required>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Date of Mfg.</label>
                        <div  moment-picker="dom" format="MMM-YYYY" start-view="month" locale="en" max-date ="max_date">
                            <input class="form-control" name="dom" placeholder="Date of Mfg." ng-model="dom" ng-model-options="{updateOn: 'blur'}" tabindex="0" autocomplete="off" readonly>
                        </div>
<!--<input class="form-control" type="text" placeholder="Date of Mfg." name="dof" ng-model="dof" required>-->
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Supplier</label>
                        <input class="form-control" maxlength="50" only-alphawithspace type="text" placeholder="Supplier" name="supplier" ng-model="supplier" required>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Type of Tooling</label>
    <!--                    <select id="scode" name="scode" class="form-control">
                            <option value="" selected="" disabled="">Select Type of Tooling </option>
                        </select>-->
                        <input class="form-control" type="text" maxlength="10" only-alpha placeholder="Select Type of Tooling" name="tot" ng-model="tot" required>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Punch Set Number</label>
                        <input class="form-control" type="text" maxlength="10" ng-change="checkDuplicatePunchSet()" only-alphanumwithslace placeholder="Punch Set Number" name="punch_set" ng-model="punch_set" required>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>No. of Subset</label>
                        <input class="form-control" valid-number type="text" maxlength="2" placeholder="No. of Subset" name="no_of_subset" ng-model="no_of_subset" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <!--<button class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancel</button>&nbsp;&nbsp;&nbsp;-->
                            <!--<button ng-click="setbtntext('update');" ng-disabled="boothForm.$invalid" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Update</button>&nbsp;&nbsp;&nbsp;-->
                            <!--<button  ng-click="setbtntext('submit');" ng-disabled="boothForm.$invalid" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Edit</button> &nbsp;&nbsp;&nbsp;&nbsp;-->
                            <button  ng-click="setbtntext('submit');" ng-disabled="punchSetForm.$invalid" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Done By</button> &nbsp;&nbsp;&nbsp;&nbsp;
                            <button  ng-click="setbtntext('approval');" disabled data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button></div>

                    </div>
                </div>
            </form>
        </div>
        <div class="contentBody" ng-if="ComDataArray.length > 0 && edit == false">
            <form name="punchSetForm" novalidate>
                <div class="form-row">
                    <div class="col-lg-4 mb-3">
                        <label>Product Code</label>
                        <input class="form-control" type="text" placeholder="Dimension" name="dimension" value="{{ComDataArray[0].product_code}}" readonly>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Product Desc.</label><br>
                        {{product_desc}}
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Dimension</label>
                        <input class="form-control" type="text" placeholder="Dimension" name="dimension" value="{{ComDataArray[0].dimension}}" readonly>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Date of Mfg.</label>

                        <input class="form-control" type="text" placeholder="Date of Mfg." name="dof" value="{{ComDataArray[0].dom | date:'dd-MMM-yyyy'}}" readonly>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Supplier</label>
                        <input class="form-control" type="text" placeholder="Supplier" name="supplier" value="{{ComDataArray[0].supplier}}" readonly>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Type of Tooling</label>
    <!--                    <select id="scode" name="scode" class="form-control">
                            <option value="" selected="" disabled="">Select Type of Tooling </option>
                        </select>-->
                        <input class="form-control" type="text" placeholder="Select Type of Tooling" name="tot" value="{{ComDataArray[0].tot}}" readonly>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>Punch Set Number</label>
                        <input class="form-control" type="text" placeholder="Punch Set Number" name="punch_set" value="{{ComDataArray[0].punch_set}}" readonly>
                    </div>
                    <div class="col-lg-4 mb-3">
                        <label>No. of Subset</label>
                        <input class="form-control" valid-number type="text" maxlength="2" placeholder="No. of Subset" name="no_of_subset" value="{{ComDataArray[0].no_of_subset}}" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <!--<button class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancel</button>&nbsp;&nbsp;&nbsp;-->
                            <!--<button ng-click="setbtntext('update');" ng-disabled="boothForm.$invalid" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Update</button>&nbsp;&nbsp;&nbsp;-->
                            <!--<button  ng-click="setbtntext('submit');" ng-disabled="boothForm.$invalid" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Edit</button> &nbsp;&nbsp;&nbsp;&nbsp;-->
                            <button  ng-click="setbtntext('submit');" disabled data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Done By</button> &nbsp;&nbsp;&nbsp;&nbsp;
                            <button  ng-click="setbtntext('approval');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn">Checked By</button></div>

                    </div>
                </div>
            </form>
        </div>

        <!-- Equipment list modal start -->
    </section>

    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>    
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>js/moment-with-locales.js"></script>
<script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script>
                            var app = angular.module("punchApp", ['angular.chosen', 'blockUI', 'moment-picker']);
                            app.directive('chosen', function ($timeout) {

                                var linker = function (scope, element, attr) {

                                    scope.$watch('productData', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    $timeout(function () {
                                        element.chosen();
                                    }, 0, false);
                                };
                                return {
                                    restrict: 'A',
                                    link: linker
                                };
                            });
                            app.directive('validNumber', function () {
                                return {
                                    require: '?ngModel',
                                    link: function (scope, element, attrs, ngModelCtrl) {
                                        if (!ngModelCtrl) {
                                            return;
                                        }

                                        ngModelCtrl.$parsers.push(function (val) {
                                            if (angular.isUndefined(val)) {
                                                var val = '';
                                            }

                                            var clean = val.replace(/[^-0-9]/g, '');
                                            var negativeCheck = clean.split('-');
                                            var decimalCheck = clean.split('.');
                                            if (!angular.isUndefined(negativeCheck[1])) {
                                                negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                                                clean = negativeCheck[0] + '-' + negativeCheck[1];
                                                if (negativeCheck[0].length > 0) {
                                                    clean = negativeCheck[0];
                                                }

                                            }

                                            if (!angular.isUndefined(decimalCheck[1])) {
                                                decimalCheck[1] = decimalCheck[1].slice(0, 2);
                                                clean = decimalCheck[0] + '.' + decimalCheck[1];
                                            }

                                            if (val !== clean) {
                                                ngModelCtrl.$setViewValue(clean);
                                                ngModelCtrl.$render();
                                            }
                                            return clean;
                                        });

                                        element.bind('keypress', function (event) {
                                            if (event.keyCode === 32) {
                                                event.preventDefault();
                                            }
                                        });
                                    }
                                };
                            });
                            app.directive('fileUpload', function () {
                                return {
                                    scope: true, //create a new scope
                                    link: function (scope, el, attrs) {
                                        el.bind('change', function (event) {
                                            var files = event.target.files;
                                            //iterate files since 'multiple' may be specified on the element
                                            for (var i = 0; i < files.length; i++) {
                                                //emit event upward
                                                scope.$emit("fileSelected", {
                                                    file: files[i]
                                                });
                                                //               console.log(file);
                                            }
                                        });
                                    }
                                };
                            });
app.directive('onlyAlphawithspace', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attr, ngModelCtrl) {
      function fromUser(text) {
        var transformedInput = text.replace(/[^a-zA-Z\s]/g, '');
        if (transformedInput !== text) {
          ngModelCtrl.$setViewValue(transformedInput);
          ngModelCtrl.$render();
        }
        return transformedInput; // or return Number(transformedInput)
      }
      ngModelCtrl.$parsers.push(fromUser);
    }
  };
});
app.directive('onlyAlpha', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attr, ngModelCtrl) {
      function fromUser(text) {
        var transformedInput = text.replace(/[^a-zA-Z]/g, '');
        if (transformedInput !== text) {
          ngModelCtrl.$setViewValue(transformedInput);
          ngModelCtrl.$render();
        }
        return transformedInput; // or return Number(transformedInput)
      }
      ngModelCtrl.$parsers.push(fromUser);
    }
  };
});
app.directive('onlyAlphanumwithslace', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attr, ngModelCtrl) {
      function fromUser(text) {
        var transformedInput = text.replace(/[^0-9a-zA-Z\/\-]/g, '');
        if (transformedInput !== text) {
          ngModelCtrl.$setViewValue(transformedInput);
          ngModelCtrl.$render();
        }
        return transformedInput; // or return Number(transformedInput)
      }
      ngModelCtrl.$parsers.push(fromUser);
    }
  };
});
app.directive('onlyAlphanumwithspace', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attr, ngModelCtrl) {
      function fromUser(text) {
        var transformedInput = text.replace(/[^0-9a-zA-Z\s]/g, '');
        if (transformedInput !== text) {
          ngModelCtrl.$setViewValue(transformedInput);
          ngModelCtrl.$render();
        }
        return transformedInput; // or return Number(transformedInput)
      }
      ngModelCtrl.$parsers.push(fromUser);
    }
  };
});
                            app.filter('format', function () {
                                return function (item) {
                                    var t = item.split(/[- :]/);
                                    var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                    var time = d.getTime();
                                    return time;
                                };
                            });
                            app.controller("punchLogCtrl", function ($scope, $http, $filter, blockUI, $timeout) {
                                $scope.max_date = moment().format('MMM-YYYY');
                                var urlParams = new URLSearchParams(window.location.search);
                                var room_code = urlParams.get('room_code');
                                var processid = urlParams.get('processid');
                                var actid = urlParams.get('actid');
                                var actname = urlParams.get('actname');

                                $scope.headerRecordid = urlParams.get('headerid');
                                $scope.formno = urlParams.get('formno');
                                $scope.versionno = urlParams.get('versionno');
                                $scope.effectivedate = urlParams.get('effectivedate');
                                $scope.retireddate = urlParams.get('retireddate');

                                $scope.showBatch = false;
                                $scope.batch_no = 'NA';
                                $scope.product_no = "";
                                $scope.room_code_show = room_code;
                                $scope.batchInProductData = [];
                                $scope.stopApprove = 0;
                                $scope.employeRole = [];
                                $scope.getEmployeRoleList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getEmployeRoleList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.employeRole = response.data.role_data;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.checkDuplicatePunchSet = function(){
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/checkPunchSetDuplicacy?punchSet='+$scope.punch_set,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        var count = response.data.count;
                                        if(count>0){
                                            alert('This Punch Set Number Has Already Been Used.');
                                            $scope.punch_set = '';
                                        }
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.getEmployeRoleList();
                                //Get Product List
                                $scope.productData = [];
                                $scope.getProductList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.productData = response.data.product_list;
                                        $scope.product_no = $scope.product_no == "" ? 'NA' : $scope.product_no;
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.getProductList();
                                $scope.product_desc = "";
                                $scope.getProductDetail = function (dataObj) {
                                    $scope.getProductList();
                                    $scope.productcode = dataObj;
                                    if ($scope.productcode != '') {
                                        $scope.product_desc = $filter('filter')($scope.productData, {product_code: $scope.productcode})[0].product_name;
                                    }
                                }
//                                        $scope.$watch('productcode', function (newval, oldval) {
//                                            if ($scope.productcode != '') {
//                                                $scope.product_desc = $filter('filter')($scope.productData, {product_code: $scope.productcode})[0].product_name;
//                                            }
//                                        });
                                $scope.getRoomBatchInProductData = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getRoomBatchInProductData?room_code=' + room_code,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.batchInProductData = response.data.batch_product_list;
                                        if ($scope.batchInProductData.length > 0) {
                                            $scope.productcode = $scope.batchInProductData[0].product_code;
                                            $scope.batch_no = $scope.batchInProductData[0].batch_no;
                                            $scope.product_no = $scope.batchInProductData[0].product_code;
                                            if (doc_id == "" || doc_id == null || doc_id == undefined)
                                            {
                                                $scope.product_desc = $scope.batchInProductData[0].product_desc;
                                            }
                                        }
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.getRoomBatchInProductData();
                                $scope.ReasonData = [];
                                $scope.GetReasonData = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetReasonData',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.ReasonData = response.data.ReasonData;
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.GetReasonData();


                                //*******************Bhupendra's code Started**********************//
                                $scope.setbtntext = function (btntext)
                                {
                                    $scope.btntext = btntext;
                                }
                                $scope.edit = false;
                                $scope.docno = "";
                                //Login Function 
                                //Created by Bhupendra 
                                //Date : 28/01/2020                                    
                                $scope.login = function () {
                                    var response = confirm("Do You Really Want To Perform This Task?");
                                    if (response == true)
                                    {
                                        $('.loader').show();
                                        pwd2 = SHA256($scope.password);
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                            method: "POST",
                                            data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&module_id=" + actid + "&module_type=log" + "&action=" + $scope.btntext + "&act_type=" + $scope.Actname + "&room_code=" + room_code + "&auth=" + btoa(btoa($scope.password))+"&act_id="+actid,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            if (response.data.message == "")
                                            {

                                                if ($scope.btntext == 'approval')
                                                {
                                                    //Calling Approval function
                                                    //Created by Rahul 
                                                    //Date : 04/08/2020
                                                    $scope.getapproved(room_code, $scope.ComDataArray[0].id, $scope.ComDataArray[0].activity_id, $scope.ComDataArray[0].workflowstatus, $scope.ComDataArray[0].workflownextstep, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, 'start', $scope.ComDataArray[0].doc_id)
                                                } else if ($scope.btntext == 'submit')
                                                {
                                                    $scope.getsubmit(room_code, processid, actid, actname, $scope.docno, $scope.product_code, $scope.dimension, $scope.dom, $scope.supplier, $scope.tot, $scope.punch_set, $scope.no_of_subset, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark)
                                                } else if ($scope.btntext == 'upload')
                                                {

                                                } else if ($scope.btntext == 'delete') {
                                                    //Delete Functionality
                                                    //Created by Rahul
                                                    //Date : 16/06/2020
                                                    var userdata = response.data.userdata;
                                                    if (userdata.role_id >= $scope.upload_by_role_id) {
                                                        $scope.deleteFile($scope.remove_id, response.data.userdata.id, $scope.inProgressActivityData[0].act_id, $scope.remove_file_name);
                                                    } else {
                                                        $('.loader').hide();
                                                        alert("You are not allowed to remove the attachment.");
                                                        $scope.fileupload = false;
                                                        $scope.files = [];
                                                    }
                                                } else if ($scope.btntext == 'update')
                                                {

                                                }
                                            } else {
                                                $('.loader').hide();
                                                alert(response.data.message);
                                            }
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                }
                                //End Login Function

                                //Approval function :- To Approve running activity by auth & privilege chk as well as sent that activity to next approval which exists in workflow
                                //Created by Rahul Chauhan
                                //Date : 04/08/2020
                                $scope.getapproved = function (roomcode, actlogtblid, actid, workflowstatus, workflownextstep, roleid, empid, empname, email, remark, actstatus, docno) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/com_approval_fun',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&workflowstatus=" + workflowstatus + "&workflownextstep=" + workflownextstep + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&actstatus=" + actstatus + "&docno=" + docno + "&tblname=pts_trn_punch_set_allocation",

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        if (response.data.next_step == "-1")
                                        {
                                            window.location.href = "<?php echo base_url() ?>home";
                                        } else
                                        {
                                            $scope.resetLoginForm();
                                            location.reload();
                                            $scope.product_code = "";
                                            $scope.dimension = "";
                                            $scope.dom = "";
                                            $scope.supplier = "";
                                            $scope.tot = "";
                                            $scope.punch_set = "";
                                            $scope.no_of_subset = "";
                                            angular.element("#btnclose").trigger('click');
                                            $scope.getLastFilterActivity();
                                            $('.loader').hide();
                                            //$scope.getdocno();
                                        }
                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }
                                //*********************************************************************************

                                //*********************************************************************************
                                //Start function :- To Start new activity by auth chk.
                                //Created by Rahul Chauhan
                                //Date : 4/08/2020
                                $scope.getsubmit = function (roomcode, processid, actid, actname, docno, product_no, dimension, dom, supplier, tot, punch_set, no_of_subset, roleid, empid, empname, email, remark) {

                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/submit_punch_set_allocation',
                                        method: "POST",
                                        data: "roomcode=" + roomcode + "&processid=" + processid + "&actid=" + actid + "&actname=" + actname + "&docno=" + docno + "&product_no=" + product_no + "&dimension=" + dimension + "&dom=" + dom + "&supplier=" + supplier + "&tot=" + tot + "&punch_set=" + punch_set + "&no_of_subset=" + no_of_subset + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&headerRecordid=" + $scope.headerRecordid,

                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        alert(response.data.message);
                                        $scope.resetLoginForm();
                                        $scope.product_code = "";
                                        $scope.dimension = "";
                                        $scope.dom = "";
                                        $scope.supplier = "";
                                        $scope.tot = "";
                                        $scope.punch_set = "";
                                        $scope.no_of_subset = "";
                                        angular.element("#btnclose").trigger('click');
                                        $scope.getLastFilterActivity();
                                        $('.loader').hide();
                                        //$scope.getdocno();

                                    }, function (error) {
                                        $('.loader').hide();
                                        console.log(error);
                                    });
                                }
                                //*********************************************************************************************

                                $scope.CancilUpdate = function () {
                                    $scope.edit = false;
                                }

                                //This function :- is used inprogress activity by last inserted id.
                                //Created by Rahul Chauhan 
                                //Date : 4/08/2020
                                $scope.ComDataArray = [];
                                $scope.getLastFilterActivity = function () {
                                    $scope.ComDataArray = [];
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetLastFilterActivity?room_code=' + room_code + "&table_name=pts_trn_punch_set_allocation",
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.ComDataArray = response.data.row_data;
                                        if ($scope.ComDataArray.length > 0) {
                                            $scope.attachment_list = $scope.ComDataArray[0].attachment_list;
                                            $scope.getProductDetail($scope.ComDataArray[0].product_code);
                                        }
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getLastFilterActivity();


                                //*******************End Bhupendra's code***************************//
                                //Reset Login Form
                                $scope.resetLoginForm = function () {
                                    $scope.username = "";
                                    $scope.password = "";
                                    $scope.remark = "";
                                }

                                function updateQueryStringParameter(uri, key, value) {
                                    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
                                    if (value === undefined) {
                                        if (uri.match(re)) {
                                            return uri.replace(re, '$1$2');
                                        } else {
                                            return uri;
                                        }
                                    } else {
                                        if (uri.match(re)) {
                                            return uri.replace(re, '$1' + key + "=" + value + '$2');
                                        } else {
                                            var hash = '';
                                            if (uri.indexOf('#') !== -1) {
                                                hash = uri.replace(/.*#/, '#');
                                                uri = uri.replace(/#.*/, '');
                                            }
                                            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
                                            return uri + separator + key + "=" + value + hash;
                                        }
                                    }
                                }




                                //Check for edit Privelege
                                $scope.roleList = [];
                                $scope.getRoleList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.roleList = response.data.role_list;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getRoleList();

                                //File Attachment
                                $scope.fileupload = false;
                                $scope.showFileUpload = function () {
                                    $scope.fileupload = true;
                                }
                                $scope.cancilFileUpload = function () {
                                    $('#file_id').val('');
                                    $scope.files = [];
                                    $scope.fileupload = false;
                                }
                                $scope.files = [];
                                $scope.$on("fileSelected", function (event, args) {
                                    var item = args;
                                    var full_path = item.file['name'];
                                    item['file_ext'] = full_path.split(".")[1];

                                    $scope.files.push(item);

                                    var reader = new FileReader();

                                    reader.addEventListener("load", function () {
                                        $scope.$apply(function () {
                                            item.src = reader.result;
                                        });
                                    }, false);

                                    if (item.file) {
                                        reader.readAsDataURL(item.file);
                                    }
                                });
                                $scope.attachLogFile = function (form_data) {
                                    var request = {
                                        method: 'POST',
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/attachLogFile',
                                        data: form_data,
                                        headers: {
                                            'Content-Type': undefined
                                        }
                                    };

                                    // SEND THE FILES.
                                    $http(request).then(function (response) {
                                        alert(response.data.message);
                                        window.location.reload();
                                        $scope.resetLoginForm();
                                        $scope.fileupload = false;
                                        $scope.getLastFilterActivity();
                                        angular.element("#btnclose").trigger('click');
                                    }, function (error) {
                                        console.log(error);
                                    });

                                }
                                $scope.remove_id = "";
                                $scope.upload_by_role_id = "";
                                $scope.remove_file_name = "";
                                $scope.removeFile = function (obj) {
                                    $scope.remove_id = obj.id;
                                    $scope.upload_by_role_id = obj.role_id;
                                    $scope.remove_file_name = obj.file_name;
                                }

                                $scope.deleteFile = function (objId, user_id, act_id, file_name) {
                                    if (confirm("Do you really want to Remove This attachment ?"))
                                    {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/removeAttachment',
                                            method: "POST",
                                            data: "id=" + objId + "&user_id=" + user_id + "&act_id=" + act_id + "&file_name=" + file_name,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.remove_id = "";
                                            $scope.upload_by_role_id = "";
                                            $scope.remove_file_name = "";
                                            alert(response.data.message);
                                            $scope.resetLoginForm();
                                            $scope.getLastFilterActivity();
                                            angular.element("#btnclose").trigger('click');
                                            $('.loader').hide();
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                }
                            });
</script>
