<div ng-app="portableLogApp" ng-controller="portableLogCtrl" ng-cloak>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url() ?>sf_home">Home</a></li>
            <li class="breadcrumb-item"><a href="#">{{room_code_show}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Line Log</li>
        </ol>
    </nav>
    <div ng-show="inProgressActivityData.length == 0 || edit == true">
        <!--Equipment list start-->
        <section class="contentBox blueBorder formWrapper">
            <div class="contentHeader">
                <div class="row">
                    <div class="col-12">
                        <h1>Header Details</h1>
                    </div>
                </div>
            </div>
            <div class="contentBody">
                <form>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <ul class="activityList">
                                    <li style="margin-left: 100px;"><a><b>Form No : </b>{{formno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Version No : </b>{{versionno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Effective Date : </b>{{effectivedate| date:'dd-MMM-yyyy'}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Retired Date : </b>{{retireddate| date:'dd-MMM-yyyy'}} </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <section class="contentBox blueBorder formWrapper">
            <div class="contentHeader">
                <div class="row">
                    <div class="col-7 col-md-8 align-self-center">
                        <h1>Line</h1>
                    </div>
                    <div class="col-5 col-md-4 text-right">

                    </div>
                </div>
            </div>
            <div class="contentBody">
                <ul class="eqplistWrap" ng-show="edit == true">
                    <li ng-repeat="dataObj in equipmentData">
                        <div class="row eqpItemWrap">
                            <div class="col-sm-9 align-self-center">
                                <div class="eqpName">
                                    <b>Code : </b>{{dataObj.equipment_code}}
                                </div>
                            </div>
                            <div class="col-sm-3 align-self-center">
                                <div class="eqpControl">
                                    <label class="switch">
                                        <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" ng-checked="true" name="quipment">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <ul class="eqplistWrap" ng-show="edit == false">
                    <li ng-repeat="(key,dataObj) in lineLogData">
                        <div class="row eqpItemWrap">
                            <div class="col-sm-9 align-self-center">
                                <div class="eqpName">
                                    {{dataObj.line_log_name}}({{dataObj.equipment_code}})
                                </div>
                            </div>
                            <div class="col-sm-3 align-self-center">
                                <div class="eqpControl">
                                    <label class="switch">
                                        <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" ng-click="selectEquipment(dataObj, $index)"  ng-model="dataObj.ischecked"  name="quipment">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li ng-if="!showfixed">
                        <div class="col-sm-12 align-self-center">
                            <div class="eqpName">
                                All Lines are occupied or there is no line in Line master...!
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <!--Equipment list end-->


        <section class="contentBox blueBorder formWrapper">
            <div class="contentHeader">
                <div class="row">
                    <div class="col-12">
                        <h1>Select Activity</h1>
                    </div>
                </div>
            </div>
            <div class="contentBody"  ng-if="equipmentData[0].ischecked == true">
                <form>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" style="margin-top: 15px;">
                                <ul class="activityList" ng-show="edit == true">
                                    <li class="{{current == $index?'active':'' }}" ng-repeat="dataObj in masterActivityData" ng-hide="dataObj.activity_name == 'Type C Cleaning'">
                                        <a>{{dataObj.activity_name}} </a>
                                    </li>
                                </ul>
                                <ul class="activityList" ng-show="edit == false">
                                    <li class="{{current == $index?'active':'' }}" ng-repeat="dataObj in masterActivityData" ng-hide="dataObj.activity_name == 'Type C Cleaning'" ng-click="initiateActivity(dataObj, $index)">
                                        <a>{{dataObj.activity_name}} </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>


        <section class="contentBox blueBorder formWrapper">
            <div class="contentHeader">
                <div class="row">
                    <div class="col-12">
                        <h1>Initiate Activity</h1>
                    </div>
                </div>
            </div>
            <div class="contentBody"  ng-show="equipmentData[0].ischecked == true && current >= 0">
                <form>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Batch No.</label>
                                <p ng-show="showBatch"><b>{{batch_no}}</b></p>
                                <input ng-hide="showBatch" maxlength="10" type="text" class="form-control" placeholder="Enter Batch No."  name="batch_no" ng-model="batch_no">    
                            </div>
                        </div>
                        <div class="col-sm-6" >
                            <div class="form-group">
                                <label><span class="asterisk">* </span>Product Code</label>

                                <div class="col-sm-12" ng-hide="showBatch">
                                    <select class="chzn-select form-control" tabindex="4"  name="product_code" ng-change="getProductDetail(product_no)" data-placeholder="Search Product No." ng-options="dataObj['product_code'] as dataObj.product_code for dataObj in productData"  ng-model="product_no" chosen>
                                        <option value="">Select Product No</option>
                                    </select>
                                </div>
                                <div class="col-sm-12" >
                                    <p><b ng-show="showBatch">{{product_no}}</b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b ng-if="product_desc != ''">{{product_desc}}</b></p>
                                </div>


                            </div>
                        </div>
                        <!--                             <div class="col-sm-3" ng-show="batchInProductData.length > 0">
                                                            <p><b>{{product_no}}</b></p>
                                                    </div>
                                                    <div class="col-sm-5" ng-if="product_desc != ''">
                                                        <p><b>{{product_desc}}</b></p>
                                                    </div>-->
                    </div>
                    <div class="row" ng-show="Actname == 'Production' && inProgressActivityData[0].workflownextstep == '0'">   
                        <div class="col-sm-2"></div>
                        <div class="col-sm-2"> <label> <b>Reason</b> </label></div>
                        <div class="col-sm-3">
                            <select class="chzn-select form-control" data-placeholder="Select Reason" name="stopreason" ng-model="stopreason">
                                <option value="">Please Select Reason</option>
                                <option ng-repeat="dataObj in ReasonData" value="{{dataObj.stop_reason}}">{{dataObj.stop_reason}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-right">
                            <div class="formBtnWrap">
                                <button ng-show="edit" class="button smallBtn btn-danger" ng-click="CancilUpdate()" >Cancel</button>&nbsp;&nbsp;&nbsp;
                                <button ng-show="edit" ng-disabled="(product_no == '' || product_no == NULL) || current < 0" ng-click="setbtntext('update');" data-toggle="modal" data-target="#loginModal" class="button smallBtn primaryBtn" >Update</button>&nbsp;&nbsp;&nbsp;
                                <button ng-show="!edit" data-toggle="modal" data-target="#loginModal" ng-disabled="(product_no == '' || product_no == NULL) || inProgressActivityData.length > 0"  ng-click="setbtntext('start');"  class="button smallBtn primaryBtn" >Activity Start</button>&nbsp;&nbsp;&nbsp;
                                <button ng-show="!edit" data-toggle="modal" data-target="#loginModal"  ng-disabled="(product_no == '' || product_no == NULL) || ((inProgressActivityData[0].workflownextstep > 0 && inProgressActivityData[0].workflowstatus > 0) || (inProgressActivityData[0].activity_stop > 0)) || stopApprove == 0"  ng-click="setbtntext('stop');"  class="button smallBtn primaryBtn" >Activity Stop</button>&nbsp;&nbsp;&nbsp;
                                <button ng-show="!edit" data-toggle="modal" data-target="#loginModal" ng-disabled="(product_no == '' || product_no == NULL) || (inProgressActivityData[0].activity_stop != NULL && inProgressActivityData[0].activity_stop != '' && inProgressActivityData[0].workflownextstep == 0 && inProgressActivityData[0].workflowstatus == 0) || ((inProgressActivityData[0].activity_stop == NULL || inProgressActivityData[0].activity_stop == '') && inProgressActivityData[0].workflownextstep == 0 && inProgressActivityData[0].workflowstatus != 0) || (inProgressActivityData[0].workflownextstep == 0 && inProgressActivityData[0].workflowstatus == 0) || stopApprove == 0"  ng-click="setbtntext('approval');" class="button smallBtn primaryBtn" >Approve</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
    <div ng-show="inProgressActivityData.length > 0 && edit == false">
        <!--Equipment list start-->
        <section class="contentBox blueBorder formWrapper">
            <div class="contentHeader">
                <div class="row">
                    <div class="col-12">
                        <h1>Header Details</h1>
                    </div>
                </div>
            </div>
            <div class="contentBody">
                <form>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <ul class="activityList">
                                    <li style="margin-left: 100px;"><a><b>Form No : </b>{{formno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Version No : </b>{{versionno}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Effective Date : </b>{{effectivedate| date:'dd-MMM-yyyy'}} </a></li>
                                    <li style="margin-left: 100px;"><a><b>Retired Date : </b>{{retireddate| date:'dd-MMM-yyyy'}} </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <section class="contentBox blueBorder formWrapper">
            <div class="contentHeader">
                <div class="row">
                    <div class="col-7 col-md-8 align-self-center">
                        <h1>Line</h1>
                    </div>
                    <div class="col-5 col-md-4 text-right">

                    </div>
                </div>
            </div>
            <div class="contentBody">
                <ul class="eqplistWrap">
                    <li ng-repeat="dataObj in equipmentData">
                        <div class="row eqpItemWrap">
                            <div class="col-sm-9 align-self-center">
                                <div class="eqpName">
                                    {{dataObj.equipment_code}}
                                </div>
                            </div>
                            <div class="col-sm-3 align-self-center">
                                <div class="eqpControl">
                                    <label class="switch">
                                        <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" ng-checked="true" name="quipment">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <!--Equipment list end-->


        <section class="contentBox blueBorder formWrapper">
            <div class="contentHeader">
                <div class="row">
                    <div class="col-12">
                        <h1>Select Activity</h1>
                    </div>
                </div>
            </div>
            <div class="contentBody">
                <form>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" style="margin-top: 15px;">
                                <ul class="activityList">
                                    <li class="{{current == $index?'active':'' }}" ng-repeat="dataObj in masterActivityData" ng-hide="dataObj.activity_name == 'Type C Cleaning'">
                                        <a>{{dataObj.activity_name}} </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>


        <section class="contentBox blueBorder formWrapper">
            <div class="contentHeader">
                <div class="row">
                    <div class="col-12">
                        <h1>Initiate Activity</h1>
                    </div>
                </div>
            </div>
            <div class="contentBody" >
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Batch No.</label>
                            <input type="text" class="form-control" value="{{inProgressActivityData[0].batch_no}}" readonly> 
                        </div>
                    </div>
                    <div class="col-sm-6" >
                        <div class="form-group">
                            <label><span class="asterisk">* </span>Product Code</label>
                            <input type="text" class="form-control" value="{{inProgressActivityData[0].product_code}}" readonly>
                            <div class="col-sm-12" >
                                <b ng-if="product_desc != ''">{{product_desc}}</b></p>
                            </div>

                        </div>
                    </div>
                    <!--                             <div class="col-sm-3" ng-show="batchInProductData.length > 0">
                                                        <p><b>{{product_no}}</b></p>
                                                </div>
                                                <div class="col-sm-5" ng-if="product_desc != ''">
                                                    <p><b>{{product_desc}}</b></p>
                                                </div>-->
                </div>
                <div class="row" ng-show="fileupload == true;">
                    <form id="fileUpload" name="fileUpload" enctype="multipart/form-data" method="post" action="#" style="float:left; width:100%;">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-2"><label>Select File:</label></div>
                                <div class="col-sm-6"><input id="file_id" type="file" (change)="onSelectFile($event)" accept="application/pdf" name="file" file-upload/></div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-9">
                                    <button class="btn btn-sm btn-danger" ng-click="cancilFileUpload()" type="button">Cancel</button>&nbsp;&nbsp;
                                    <button ng-disabled="files.length == 0" data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('upload');" class="btn btn-sm btn-success" type="button">Upload</button>
                                    <!--<button ng-click="setbtntext('upload');" class="btn btn-sm btn-success" type="button">Upload</button>                                        </div>-->
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row mb-3" ng-show="attachment_list.length > 0">
                    <div class="col-sm-6">
                        <h1>Attachment List</h1>
                    </div>
                </div>
                <div class="row mb-3" ng-show="attachment_list.length > 0">
                    <div class="col-sm-7">
                        <table class="table">
                            <thead style="border-top:10px">
                                <tr>
                                    <th>File Name </th>
                                    <th>Role</th>
                                    <th>Uploaded By</th>
                                    <th>Uploaded On</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody >
                                <tr ng-repeat="dataObj in attachment_list">
                                    <td>{{dataObj.file_name}}</td>
                                    <td>{{dataObj.role_description}}</td>
                                    <td>{{dataObj.uploaded_by}}</td>
                                    <td>{{dataObj.uploaded_on| format | date:'dd-MMM-yyyy HH:mm:ss'}}</td>
                                    <td>
                                        <a href="<?php echo base_url() ?>assets/files/{{inProgressActivityData[0].act_id}}/{{dataObj.file_name}}" target="_blank" class="btn btn-sm btn-info" >View</a>&nbsp;&nbsp;
                                        <button data-toggle="modal" data-target="#loginModal" ng-click="setbtntext('delete');removeFile(dataObj)" class="btn btn-sm btn-danger" type="button">Delete</button></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="row" ng-show="Actname == 'Production' && inProgressActivityData[0].workflownextstep == '0'">   
                    <div class="col-sm-2"></div>
                    <div class="col-sm-2"> <label> <b>Reason</b> </label></div>
                    <div class="col-sm-3">
                        <select class="chzn-select form-control" data-placeholder="Select Reason" name="stopreason" ng-model="stopreason">
                            <option value="">Please Select Reason</option>
                            <option ng-repeat="dataObj in ReasonData" value="{{dataObj.stop_reason}}">{{dataObj.stop_reason}}</option>
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-sm-12 text-right">
                        <div class="formBtnWrap">
                            <button ng-click="showFileUpload()" class="button smallBtn primaryBtn" >Attach File</button>&nbsp;&nbsp;&nbsp;
                            <button ng-click="EditTrasaction()" data-toggle="modal" data-target="#" class="button smallBtn primaryBtn" >Edit</button>&nbsp;&nbsp;&nbsp;
                            <button data-toggle="modal" data-target="#loginModal"  ng-disabled="((inProgressActivityData[0].workflownextstep > 0 && inProgressActivityData[0].workflowstatus > 0) || (inProgressActivityData[0].activity_stop > 0))"  ng-click="setbtntext('stop');"  class="button smallBtn primaryBtn" >Activity Stop</button>&nbsp;&nbsp;&nbsp;
                            <button data-toggle="modal" data-target="#loginModal" ng-disabled="(inProgressActivityData[0].activity_stop != NULL && inProgressActivityData[0].activity_stop != '' && inProgressActivityData[0].workflownextstep == 0 && inProgressActivityData[0].workflowstatus == 0) || ((inProgressActivityData[0].activity_stop == NULL || inProgressActivityData[0].activity_stop == '') && inProgressActivityData[0].workflownextstep == 0 && inProgressActivityData[0].workflowstatus != 0) || (inProgressActivityData[0].workflownextstep == 0 && inProgressActivityData[0].workflowstatus == 0)"  ng-click="setbtntext('approval');" class="button smallBtn primaryBtn" >Approve</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- Equipment list modal start -->
    <?php $this->load->view("template/ponta_sahib/sf_login_modal"); ?>
</div>    
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url() ?>js/angular-block-ui.min.js"></script>
<script>
                                var app = angular.module("portableLogApp", ['angular.chosen', 'blockUI']);

                                app.directive('chosen', function ($timeout) {

                                    var linker = function (scope, element, attr) {

                                        scope.$watch('productData', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);
                                        $timeout(function () {
                                            element.chosen();
                                        }, 0, false);
                                    };
                                    return {
                                        restrict: 'A',
                                        link: linker
                                    };
                                });
                                app.directive('fileUpload', function () {
                                    return {
                                        scope: true, //create a new scope
                                        link: function (scope, el, attrs) {
                                            el.bind('change', function (event) {
                                                var files = event.target.files;
                                                //iterate files since 'multiple' may be specified on the element
                                                for (var i = 0; i < files.length; i++) {
                                                    //emit event upward
                                                    scope.$emit("fileSelected", {
                                                        file: files[i]
                                                    });
                                                    //               console.log(file);
                                                }
                                            });
                                        }
                                    };
                                });
                                app.filter('format', function () {
                                    return function (item) {
                                        var t = item.split(/[- :]/);
                                        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                        var time = d.getTime();
                                        return time;
                                    };
                                });
                                app.controller("portableLogCtrl", function ($scope, $http, $filter, blockUI, $timeout) {
                                    var urlParams = new URLSearchParams(window.location.search);
                                    var myParam = urlParams.get('room_code');
                                    var processid = urlParams.get('processid');
                                    var mst_activity = urlParams.get('actid');
                                    $scope.room_code_show = myParam;
                                    var doc_id = urlParams.get('doc_id');

                                    $scope.headerRecordid = urlParams.get('headerid');
                                    $scope.formno = urlParams.get('formno');
                                    $scope.versionno = urlParams.get('versionno');
                                    $scope.effectivedate = urlParams.get('effectivedate');
                                    $scope.retireddate = urlParams.get('retireddate');

                                    $scope.showBatch = false;
                                    $scope.batch_no = 'NA';
                                    $scope.batchInProductData = [];
                                    $scope.stopApprove = 0;
                                    $scope.employeRole = [];
                                    $scope.getEmployeRoleList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getEmployeRoleList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.employeRole = response.data.role_data;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getEmployeRoleList();
                                    //Get Product List
                                    $scope.productData = [];
                                    $scope.getProductList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.productData = response.data.product_list;
                                            $scope.product_no = $scope.product_no == "" ? 'NA' : $scope.product_no;
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getProductList();
                                    $scope.product_desc = "";
                                    $scope.productcode = "";
                                    $scope.getProductDetail = function (dataObj) {
                                        $scope.getProductList();
                                        $scope.productcode = dataObj;
                                        if ($scope.productcode != '') {
                                            $scope.product_desc = $filter('filter')($scope.productData, {product_code: $scope.productcode})[0].product_name;
                                        }
                                    }
//                                        $scope.$watch('productcode', function (newval, oldval) {
//                                            if ($scope.productcode != '') {
//                                                $scope.product_desc = $filter('filter')($scope.productData, {product_code: $scope.productcode})[0].product_name;
//                                            }
//                                        });
                                    $scope.getRoomBatchInProductData = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getRoomBatchInProductData?room_code=' + myParam,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.batchInProductData = response.data.batch_product_list;
                                            if ($scope.batchInProductData.length > 0) {
                                                $scope.productcode = $scope.batchInProductData[0].product_code;
                                                $scope.batch_no = $scope.batchInProductData[0].batch_no;
                                                $scope.product_no = $scope.batchInProductData[0].product_code;
                                                if (doc_id == "" || doc_id == null || doc_id == undefined)
                                                {
                                                    $scope.product_desc = $scope.batchInProductData[0].product_desc;
                                                }
//                                                    else
//                                                    {
//                                                        $scope.getProductDetail($scope.batchInProductData[0].product_code);
//                                                    }
                                            }
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getRoomBatchInProductData();
                                    $scope.poratableEquipmentData = [];
                                    $scope.getPortableEquimentList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getportableequiplist?room_code=' + myParam,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.poratableEquipmentData = response.data.potable_euip_list;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getPortableEquimentList();
                                    $scope.lineLogData = [];
                                    $scope.showfixed = true;
                                    $scope.getLineLogList = function (room_code) {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetLineLogList?room_code=' + myParam,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.lineLogData = response.data.line_log_data;
                                            $scope.showfixed = response.data.result;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getLineLogList();

                                    $scope.fixedEquipmentData = [];
                                    $scope.getFixedEquimentList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getfixedequiplist?room_code=' + myParam + "&type=Line",
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.fixedEquipmentData = response.data.fixed_euip_list;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getFixedEquimentList();
                                    $scope.ReasonData = [];
                                    $scope.GetReasonData = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetReasonData',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.ReasonData = response.data.ReasonData;
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.GetReasonData();
                                    $scope.equipmentData = [];
                                    $scope.showMasterActivity = false;
                                    $scope.selectEquipment = function (dataObj, $index) {
                                        $scope.current = '-1';
                                        if (dataObj.ischecked) {
                                            if ($scope.equipmentData.length > 0) {
                                                if (equipmentExists(dataObj.id) == true) {

                                                } else {
                                                    $scope.equipmentData.push(dataObj);
                                                }
                                            } else {
                                                $scope.equipmentData.push(dataObj);
                                            }
                                        } else {
                                            angular.forEach($scope.equipmentData, function (obj, index) {
                                                if (obj.id == dataObj.id) {
                                                    $scope.equipmentData.splice(index, 1);
                                                }
                                            });
                                        }
                                    }

                                    function equipmentExists(objId) {
                                        return $scope.equipmentData.some(function (el) {
                                            return el.id === objId;
                                        });
                                    }
                                    $scope.masterActivityData = [];
                                    $scope.getMasterActivityList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getmasteractivitylist?activity_type=home',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.masterActivityData = response.data.master_activity_list;
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getMasterActivityList();

                                    $scope.HeaderData = [];
//                                $scope.GetmstHeaderData = function () {
//                                    $http({
//                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetmstHeaderData',
//                                        method: "GET",
//                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
//                                    }).then(function (response) {
//                                        console.log(response);
//                                        if(response.data.result == true)
//                                        {
//                                            $scope.HeaderData = response.data.HeaderData;
//                                            $scope.formno = $scope.HeaderData[0]["form_no"];
//                                            $scope.versionno = $scope.HeaderData[0]["version_no"];
//                                            $scope.effectivedate = $scope.HeaderData[0]["effective_date"];
//                                            $scope.retireddate = $scope.HeaderData[0]["retired_date"];
//                                        }
//                                        else
//                                        {
//                                            alert("No version of Log Name is effective.");
//                                            window.location.href = "<?php echo base_url() ?>home";
//                                        }       
//                                    }, function (error) { // optional
//                                        console.log("Something went wrong.Please try again");
//
//                                    });
//                                }
//                                $scope.GetmstHeaderData();

                                    $scope.current = '-1';
                                    $scope.Actid = '';
                                    $scope.Actname = '';
                                    $scope.productcode = '';
                                    $scope.batchno = '';
                                    $scope.initiateActivity = function (dataObj, index) {
                                        $scope.current = index;
                                        $scope.showBatch = false;
                                        if ($scope.batchInProductData.length > 0) {
                                            $scope.productcode = $scope.batchInProductData[0].product_code;
                                            $scope.batch_no = $scope.batchInProductData[0].batch_no;
                                            $scope.product_no = $scope.batchInProductData[0].product_code;
                                            $scope.product_desc = $scope.batchInProductData[0].product_desc;
                                            $scope.showBatch = true;
                                        }
                                        $scope.current = index;
                                        $scope.Actid = dataObj.id;
                                        $scope.Actname = dataObj.activity_name;
                                        if ((dataObj.activity_name == 'Type A Cleaning' || dataObj.activity_name == 'Type B Cleaning' || dataObj.activity_name == 'Maintenance'))
                                        {
                                            if ($scope.batchInProductData.length > 0) {
                                                alert("There is already a running batch please do batch out first..!");
                                                $scope.current = '-1';
                                                $('.loader').hide();
                                                return false;
                                            } else if ($scope.previousBatchInProductData.length > 0) {
                                                $scope.productcode = $scope.previousBatchInProductData[0].product_code;
                                                $scope.batch_no = $scope.previousBatchInProductData[0].batch_no;
                                                $scope.product_no = $scope.previousBatchInProductData[0].product_code;
                                                $scope.product_desc = $scope.previousBatchInProductData[0].product_desc;
                                                $scope.showBatch = true;
                                            }
                                        } else if (dataObj.activity_name == 'Type D Cleaning')
                                        {
                                            if ($scope.previousBatchInProductData.length > 0) {
                                                $scope.productcode = $scope.previousBatchInProductData[0].product_code;
                                                $scope.batch_no = $scope.previousBatchInProductData[0].batch_no;
                                                $scope.product_no = $scope.previousBatchInProductData[0].product_code;
                                                $scope.product_desc = $scope.previousBatchInProductData[0].product_desc;
                                                $scope.showBatch = true;
                                            }
                                        } else if (dataObj.activity_name == 'Production' || dataObj.activity_name == 'Sampling')
                                        {
                                            if ($scope.batchInProductData.length == 0)
                                            {
                                                alert("Please do batch in first..!");
                                                $scope.current = '-1';
                                                $('.loader').hide();
                                                return false;
                                            }
                                            if (dataObj.activity_name == 'Production' || dataObj.activity_name == 'Sampling')
                                            {
                                                $scope.Get_Last_Type_AorB_record(myParam);

                                            }
                                        } else {
                                            if ($scope.batchInProductData.length == 0) {
                                                $scope.product_no = "";
                                                $scope.batch_no = "";
                                                $scope.product_desc = "";
                                            }

                                        }
                                    }

                                    //*******************Bhupendra's code Started**********************//
                                    $scope.EditTrasaction = function () {
                                        if (confirm("Do you really want to edit ?"))
                                        {
                                            $scope.edit = true;
                                            $scope.equipmentData = [];
                                            $scope.docno = $scope.inProgressActivityData[0].doc_id;
                                            var equipmentData = $scope.inProgressActivityData[0].equip_code.split(",");
                                            angular.forEach(equipmentData, function (value, key) {
                                                $scope.equipmentData.push({"equipment_code": value, "ischecked": true});
                                            });
                                            $scope.Actid = $scope.inProgressActivityData[0].activity_id;
                                            if ($scope.Actid >= 4)
                                            {
                                                $scope.current = $scope.inProgressActivityData[0].activity_id - 2;
                                            } else
                                            {
                                                $scope.current = $scope.inProgressActivityData[0].activity_id - 1;
                                            }
                                            // if ($scope.batchInProductData.length > 0) {
                                            //     $scope.batch_no = $scope.batchInProductData[0].batch_no;
                                            //     $scope.product_no = $scope.batchInProductData[0].product_code;
                                            // } else {
                                            $scope.batch_no = $scope.inProgressActivityData[0].batch_no;
                                            $scope.product_no = $scope.inProgressActivityData[0].product_code;
                                            //}
                                            $scope.productcode = $scope.inProgressActivityData[0].product_code;
                                            $scope.getProductDetail($scope.inProgressActivityData[0].product_code);
                                            $scope.Actname = $scope.inProgressActivityData[0].home_activity;
                                        }
                                    }

                                    $scope.setbtntext = function (btntext)
                                    {
                                        $scope.btntext = btntext;
                                    }
                                    $scope.edit = false;
                                    //Login Function 
                                    //Created by Bhupendra 
                                    //Date : 28/01/2020  
                                    $scope.stopreason = '';
                                    $scope.login = function () {
                                        var response = confirm("Do You Really Want To Approve This ?");
                                        if (response == true)
                                        {
                                            $('.loader').show();
                                            pwd2 = SHA256($scope.password);
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Login',
                                                method: "POST",
                                                data: "email=" + $scope.username + "&password=" + pwd2 + "&remark=" + $scope.remark + "&module_id=" + mst_activity + "&module_type=log" + "&action=" + $scope.btntext + "&act_type=" + $scope.Actname + "&room_code=" + myParam + "&auth=" + btoa(btoa($scope.password)) + "&act_id=" + $scope.Actid,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                if (response.data.message == "")
                                                {
                                                    var temp = [];
                                                    angular.forEach($scope.equipmentData, function (obj, index) {
                                                        temp.push(obj.line_log_code);
                                                    });
                                                    var equipment_code = temp.toString();

                                                    if ($scope.btntext == 'approval')
                                                    {
                                                        //Calling Approval function
                                                        //Created by Bhupendra 
                                                        //Date : 28/01/2020
                                                        var actstatus = "";
                                                        if ($scope.inProgressActivityData[0].activity_stop != "" && $scope.inProgressActivityData[0].activity_stop != null)
                                                        {
                                                            actstatus = 'stop';
                                                        } else
                                                        {
                                                            actstatus = 'start';
                                                        }
                                                        $scope.getapproved(myParam, $scope.inProgressActivityData[0].act_id, $scope.inProgressActivityData[0].activity_id, $scope.inProgressActivityData[0].workflowstatus, $scope.inProgressActivityData[0].workflownextstep, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, actstatus)
                                                    } else if ($scope.btntext == 'stop')
                                                    {
                                                        //Calling Stoped function
                                                        //Created by Bhupendra 
                                                        //Date : 29/01/2020
                                                        if ($scope.Actid == 6 && $scope.stopreason == '') {
                                                            alert("Please Select Stop Reason");
                                                            $('.loader').hide();
                                                            return false;
                                                        }
                                                        $scope.getstoped(myParam, $scope.inProgressActivityData[0].act_id, $scope.Actid, $scope.inProgressActivityData[0].mst_act_id, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.stopreason)
                                                    } else if ($scope.btntext == 'start')
                                                    {
                                                        $scope.getstart(myParam, processid, equipment_code, $scope.Actid, $scope.Actname, $scope.productcode, $scope.batch_no, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark)
                                                    } else if ($scope.btntext == 'upload')
                                                    {
                                                        //Upload Functionality
                                                        //Created by Rahul
                                                        //Date : 15/06/2020
                                                        //angular.element( document.querySelector( '#filUpload' ) )
                                                        //angular.element('#filUpload')
                                                        var role = "";
                                                        if ($scope.inProgressActivityData[0].activity_level.length > 0 && $scope.inProgressActivityData[0].workflownextstep > 0 && $scope.inProgressActivityData[0].start_time != null && $scope.inProgressActivityData[0].stop_time == null) {
                                                            if ($scope.inProgressActivityData[0].start_time != null && $scope.inProgressActivityData[0].stop_time == null) {
                                                                var workflowtype = 'start';
                                                            } else {
                                                                var workflowtype = 'stop';
                                                            }

                                                            var desc = $filter('filter')($scope.inProgressActivityData[0].activity_level, {status_id: $scope.inProgressActivityData[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                            var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                        } else if ($scope.inProgressActivityData[0].activity_level.length > 0 && $scope.inProgressActivityData[0].workflownextstep == 0 && $scope.inProgressActivityData[0].start_time != null && $scope.inProgressActivityData[0].stop_time == null) {

                                                            var desc = $filter('filter')($scope.inProgressActivityData[0].activity_level, {status_id: $scope.inProgressActivityData[0].workflowstatus})[0].role_id;
                                                            var role = $filter('filter')($scope.roleList, {id: desc})[0].id;

                                                        } else if ($scope.inProgressActivityData[0].activity_level.length > 0 && $scope.inProgressActivityData[0].workflownextstep > 0 && $scope.inProgressActivityData[0].start_time != null && $scope.inProgressActivityData[0].stop_time != null) {
                                                            if ($scope.inProgressActivityData[0].start_time != null && $scope.inProgressActivityData[0].stop_time == null) {
                                                                var workflowtype = 'start';
                                                            } else {
                                                                var workflowtype = 'stop';
                                                            }
                                                            var desc = $filter('filter')($scope.inProgressActivityData[0].activity_level, {status_id: $scope.inProgressActivityData[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                            var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                        }
                                                        var userdata = response.data.userdata;
//                                                    var role_id = $filter('filter')($scope.employeRole, {id: $scope.inProgressActivityData[0].user_id})[0].role_id;
                                                        if ((role != "" && userdata.role_id >= role) || (userdata.role_id >= $scope.inProgressActivityData[0].last_approval.role_id)) {
                                                            var myForm = document.getElementById('fileUpload');
                                                            var formData = new FormData(myForm);
                                                            formData.append('roomcode', myParam);
                                                            formData.append('act_id', $scope.inProgressActivityData[0].act_id);
                                                            formData.append('processid', $scope.inProgressActivityData[0].processid);
                                                            formData.append('activity_id', $scope.inProgressActivityData[0].mst_act_id);
                                                            formData.append('roleid', response.data.userdata.role_id);
                                                            formData.append('empid', response.data.userdata.id);
                                                            formData.append('empname', response.data.userdata.emp_name);
                                                            formData.append('remark', $scope.remark);
                                                            angular.forEach($scope.files, function (value, key) {
                                                                var file = value.file;
                                                                formData.append('file[]', file);
                                                            });
                                                            $scope.attachLogFile(formData);
                                                        } else {
                                                            $('.loader').hide();
                                                            alert("You are not allowed to Upload the attachment.");
                                                            $scope.fileupload = false;
                                                            $scope.files = [];
                                                        }
                                                    } else if ($scope.btntext == 'delete') {
                                                        //Delete Functionality
                                                        //Created by Rahul
                                                        //Date : 16/06/2020
                                                        var userdata = response.data.userdata;
                                                        if (userdata.role_id >= $scope.upload_by_role_id) {
                                                            $scope.deleteFile($scope.remove_id, response.data.userdata.id, $scope.inProgressActivityData[0].act_id, $scope.remove_file_name);
                                                        } else {
                                                            alert("You are not allowed to remove the attachment.");
                                                            $scope.fileupload = false;
                                                            $scope.files = [];
                                                        }
                                                    } else if ($scope.btntext == 'update')
                                                    {
                                                        //update Functionality
                                                        //Created by Rahul
                                                        //Date : 08/05/2020
//                                                    alert($scope.inProgressActivityData[0].log_id);return false;
                                                        var role = "";
                                                        if ($scope.inProgressActivityData[0].activity_level.length > 0 && $scope.inProgressActivityData[0].workflownextstep > 0 && $scope.inProgressActivityData[0].start_time != null && $scope.inProgressActivityData[0].stop_time == null) {
                                                            if ($scope.inProgressActivityData[0].start_time != null && $scope.inProgressActivityData[0].stop_time == null) {
                                                                var workflowtype = 'start';
                                                            } else {
                                                                var workflowtype = 'stop';
                                                            }
                                                            var desc = $filter('filter')($scope.inProgressActivityData[0].activity_level, {status_id: $scope.inProgressActivityData[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                            var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                        } else if ($scope.inProgressActivityData[0].activity_level.length > 0 && $scope.inProgressActivityData[0].workflownextstep == 0 && $scope.inProgressActivityData[0].start_time != null && $scope.inProgressActivityData[0].stop_time == null) {

                                                            var desc = $filter('filter')($scope.inProgressActivityData[0].activity_level, {status_id: $scope.inProgressActivityData[0].workflowstatus})[0].role_id;
                                                            var role = $filter('filter')($scope.roleList, {id: desc})[0].id;

                                                        } else if ($scope.inProgressActivityData[0].activity_level.length > 0 && $scope.inProgressActivityData[0].workflownextstep > 0 && $scope.inProgressActivityData[0].start_time != null && $scope.inProgressActivityData[0].stop_time != null) {
                                                            if ($scope.inProgressActivityData[0].start_time != null && $scope.inProgressActivityData[0].stop_time == null) {
                                                                var workflowtype = 'start';
                                                            } else {
                                                                var workflowtype = 'stop';
                                                            }
                                                            var desc = $filter('filter')($scope.inProgressActivityData[0].activity_level, {status_id: $scope.inProgressActivityData[0].workflownextstep, workflowtype: workflowtype})[0].role_id;
                                                            var role = $filter('filter')($scope.roleList, {id: desc})[0].id;
                                                        }
                                                        var userdata = response.data.userdata;
//                                                    var role_id = $filter('filter')($scope.employeRole, {id: $scope.inProgressActivityData[0].user_id})[0].role_id;

                                                        if (userdata.role_id >= $scope.inProgressActivityData[0].last_approval.role_id && userdata.role_id > 1) {
                                                            var temp = [];
                                                            angular.forEach($scope.equipmentData, function (obj, index) {
                                                                temp.push(obj.equipment_code);
                                                            });
                                                            var equipment_code = temp.toString();
//                                                        $scope.getstart(myParam, processid, equipment_code, $scope.Actid, $scope.Actname, $scope.productcode, $scope.batch_no, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark)
                                                            $scope.updatesubmit(myParam, processid, equipment_code, $scope.Actid, $scope.Actname, $scope.productcode, $scope.batch_no, response.data.userdata.role_id, response.data.userdata.id, response.data.userdata.emp_name, response.data.userdata.email2, $scope.remark, $scope.inProgressActivityData[0].log_id);
                                                        } else {
                                                            $('.loader').hide();
                                                            alert("You are not allowed to edit this.")
                                                            $scope.edit = false;
                                                        }
                                                    }
                                                } else {
                                                    $('.loader').hide();
                                                    alert(response.data.message);
                                                }
                                            }, function (error) {
                                                $('.loader').hide();
                                                console.log(error);
                                            });
                                        }
                                    }
                                    //End Login Function

                                    //Approval function :- To Approve running activity by auth & privilege chk as well as sent that activity to next approval which exists in workflow
                                    //Created by Bhupendra 
                                    //Date : 28/01/2020
                                    $scope.getapproved = function (roomcode, actlogtblid, actid, workflowstatus, workflownextstep, roleid, empid, empname, email, remark, actstatus) {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getapproved',
                                            method: "POST",
                                            data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&workflowstatus=" + workflowstatus + "&workflownextstep=" + workflownextstep + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&actstatus=" + actstatus,

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.getInprogressActivityList(actlogtblid, '');
                                            alert(response.data.message);
                                            if (response.data.next_step == "-1")
                                            {
                                                window.location.href = "<?php echo base_url() ?>home";
                                            } else
                                            {
                                                $scope.resetLoginForm();
                                                angular.element("#btnclose").trigger('click');
                                                window.location.reload();
                                                $('.loader').hide();
                                            }
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                    //*********************************************************************************

                                    //Stoped function :- To Stop running activity by auth chk.
                                    //Created by Bhupendra 
                                    //Date : 28/01/2020                                    
                                    $scope.getstoped = function (roomcode, actlogtblid, actid, mst_act_id, roleid, empid, empname, email, remark, stopreason) {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getstoped',
                                            method: "POST",
                                            data: "roomcode=" + roomcode + "&actlogtblid=" + actlogtblid + "&actid=" + actid + "&mst_act_id=" + mst_act_id + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&stopreason=" + stopreason,

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.getInprogressActivityList(actlogtblid, '');
                                            alert(response.data.message);
                                            window.location.reload();
                                            $scope.resetLoginForm();
                                            angular.element("#btnclose").trigger('click');
                                            $('.loader').hide();
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                    //********************************************************************************

                                    //Start function :- To Start new activity by auth chk.
                                    //Created by Bhupendra 
                                    //Date : 4/02/2020 

                                    $scope.getstart = function (roomcode, processid, equipmentcode, actid, actname, product_no, batch_no, roleid, empid, empname, email, remark) {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getstarted',
                                            method: "POST",
                                            data: "roomcode=" + roomcode + "&processid=" + processid + "&equipmentcode=" + equipmentcode + "&actid=" + actid + "&actname=" + actname + "&product_no=" + product_no + "&batch_no=" + batch_no + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&module_id=" + mst_activity + "&headerRecordid=" + $scope.headerRecordid + "&Logname=Linelog",

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            alert(response.data.message);
                                            $scope.stopApprove = 1;
                                            $scope.resetLoginForm();
                                            var acttblid = response.data.acttblid;
                                            $scope.getInprogressActivityList(acttblid, 'start');
                                            angular.element("#btnclose").trigger('click');
                                            $('.loader').hide();
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                    //*********************************************************************************************

                                    //function :- To Get Last TypeA or TypeB Record
                                    //Created by Bhupendra kumar
                                    //Date : 21/05/2020
                                    $scope.Get_Last_Type_AorB_record = function (roomcode) {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetLastTypeAorBrecord',
                                            method: "POST",
                                            data: "roomcode=" + roomcode,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            if (response.data.res != 'go')
                                            {
                                                alert(response.data.res);
                                                $scope.current = '-1';
                                                $('.loader').hide();
                                                return false;
                                            }
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");

                                        });
                                    }

                                    //Start function :- To update Acivity
                                    //Created by Rahul Chauhan 
                                    //Date : 08/05/2020

                                    $scope.updatesubmit = function (roomcode, processid, equipmentcode, actid, actname, product_no, batch_no, roleid, empid, empname, email, remark, update_id) {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/updatestarted',
                                            method: "POST",
                                            data: "roomcode=" + roomcode + "&processid=" + processid + "&equipmentcode=" + equipmentcode + "&actid=" + actid + "&actname=" + actname + "&product_no=" + product_no + "&batch_no=" + batch_no + "&roleid=" + roleid + "&empid=" + empid + "&empname=" + empname + "&email=" + email + "&remark=" + remark + "&module_id=" + mst_activity + "&update_id=" + update_id,

                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            alert(response.data.message);
                                            $scope.stopApprove = 1;
                                            $scope.resetLoginForm();
                                            $scope.edit = false;
                                            $scope.getInprogressActivityList(response.data.acttblid, '');
                                            angular.element("#btnclose").trigger('click');
                                            $('.loader').hide();
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });
                                    }
                                    $scope.CancilUpdate = function () {
                                        $scope.edit = false;
                                    }


                                    //This function :- is used inprogress activity by last inserted id.
                                    //Created by Bhupendra 
                                    //Date : 4/02/2020
                                    $scope.inProgressActivityData = [];
                                    $scope.getInprogressActivityList = function (acttblid, type) {
                                        if (acttblid > 0) {
                                            var query = '?actid=' + acttblid + "&doc_id=";
                                        } else {
                                            var query = '?actid=&doc_id=' + doc_id;
                                        }
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetInprogressActivityList' + query,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.inProgressActivityData = response.data.in_progress_activity;
                                            if ($scope.inProgressActivityData.length > 0) {
                                                $scope.equipmentData = [];
                                                $scope.Actid = $scope.inProgressActivityData[0].activity_id;
                                                if ($scope.Actid >= 4)
                                                {
                                                    $scope.current = $scope.inProgressActivityData[0].activity_id - 2;
                                                } else
                                                {
                                                    $scope.current = $scope.inProgressActivityData[0].activity_id - 1;
                                                }
                                                $scope.doc_no = $scope.inProgressActivityData[0].doc_id;
                                                $scope.Actname = $scope.inProgressActivityData[0].home_activity;

                                                $scope.attachment_list = $scope.inProgressActivityData[0].attachment_list;
                                                var equipmentData = $scope.inProgressActivityData[0].equip_code.split(",");
                                                $scope.getProductDetail($scope.inProgressActivityData[0].product_code);
                                                angular.forEach(equipmentData, function (value, key) {
                                                    $scope.equipmentData.push({"equipment_code": value});
                                                });
                                                if (acttblid > 0 && type == 'start') {

                                                    if (doc_id != '') {
                                                        var uri = window.location.href;
                                                        window.location.href = updateQueryStringParameter(uri, "doc_id", $scope.doc_no);
                                                    } else {
                                                        window.location.href = window.location.href + "&doc_id=" + $scope.doc_no;
                                                    }

                                                }
                                            }
                                            // if(response.data.message == 'NoMasterActivityFound')
                                            // {
                                            //   alert("Doc gen");
                                            // }

                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    if (doc_id != null) {
                                        $scope.getInprogressActivityList();
                                    }

                                    //*******************End Bhupendra's code***************************//
                                    //Reset Login Form
                                    $scope.resetLoginForm = function () {
                                        $scope.username = "";
                                        $scope.password = "";
                                        $scope.remark = "";
                                    }

                                    function updateQueryStringParameter(uri, key, value) {
                                        var re = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
                                        if (value === undefined) {
                                            if (uri.match(re)) {
                                                return uri.replace(re, '$1$2');
                                            } else {
                                                return uri;
                                            }
                                        } else {
                                            if (uri.match(re)) {
                                                return uri.replace(re, '$1' + key + "=" + value + '$2');
                                            } else {
                                                var hash = '';
                                                if (uri.indexOf('#') !== -1) {
                                                    hash = uri.replace(/.*#/, '#');
                                                    uri = uri.replace(/#.*/, '');
                                                }
                                                var separator = uri.indexOf('?') !== -1 ? "&" : "?";
                                                return uri + separator + key + "=" + value + hash;
                                            }
                                        }
                                    }

                                    $scope.previousBatchInProductData = [];
                                    $scope.getpreviousRoomBatchInProductData = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getPreviousRoomBatchInProductData?room_code=' + myParam,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.previousBatchInProductData = response.data.batch_product_list;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getpreviousRoomBatchInProductData();
                                    //Check for edit Privelege
                                    $scope.roleList = [];
                                    $scope.getRoleList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetRoleList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.roleList = response.data.role_list;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getRoleList();

                                    //File Attachment
                                    $scope.fileupload = false;
                                    $scope.showFileUpload = function () {
                                        $scope.fileupload = true;
                                    }
                                    $scope.cancilFileUpload = function () {
                                        $('#file_id').val('');
                                        $scope.files = [];
                                        $scope.fileupload = false;
                                    }
                                    $scope.files = [];
                                    $scope.$on("fileSelected", function (event, args) {
                                        var item = args;
                                        var full_path = item.file['name'];
                                        item['file_ext'] = full_path.split(".")[1];

                                        $scope.files.push(item);

                                        var reader = new FileReader();

                                        reader.addEventListener("load", function () {
                                            $scope.$apply(function () {
                                                item.src = reader.result;
                                            });
                                        }, false);

                                        if (item.file) {
                                            reader.readAsDataURL(item.file);
                                        }
                                    });
                                    $scope.attachLogFile = function (form_data) {
                                        var request = {
                                            method: 'POST',
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/attachLogFile',
                                            data: form_data,
                                            headers: {
                                                'Content-Type': undefined
                                            }
                                        };

                                        // SEND THE FILES.
                                        $http(request).then(function (response) {
                                            alert(response.data.message);
                                            window.location.reload();
                                            $scope.resetLoginForm();
                                            $scope.fileupload = false;
                                            $scope.getInprogressActivityList(response.data.acttblid, '');
                                            angular.element("#btnclose").trigger('click');
                                            $('.loader').hide();
                                        }, function (error) {
                                            $('.loader').hide();
                                            console.log(error);
                                        });

                                    }
                                    $scope.remove_id = "";
                                    $scope.upload_by_role_id = "";
                                    $scope.remove_file_name = "";
                                    $scope.removeFile = function (obj) {
                                        $scope.remove_id = obj.id;
                                        $scope.upload_by_role_id = obj.role_id;
                                        $scope.remove_file_name = obj.file_name;
                                    }

                                    $scope.deleteFile = function (objId, user_id, act_id, file_name) {
                                        if (confirm("Do you really want to Remove This attachment ?"))
                                        {
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/removeAttachment',
                                                method: "POST",
                                                data: "id=" + objId + "&user_id=" + user_id + "&act_id=" + act_id + "&file_name=" + file_name,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.remove_id = "";
                                                $scope.upload_by_role_id = "";
                                                $scope.remove_file_name = "";
                                                alert(response.data.message);
                                                $scope.resetLoginForm();
                                                $scope.getInprogressActivityList(response.data.acttblid, '');
                                                angular.element("#btnclose").trigger('click');
                                                $('.loader').hide();
                                            }, function (error) {
                                                $('.loader').hide();
                                                console.log(error);
                                            });
                                        }
                                    }
                                });
</script>
