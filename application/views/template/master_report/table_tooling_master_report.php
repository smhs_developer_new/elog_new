<?php if(!empty($res)){?>
    <div class="card-body ht750" id="audit_trail2" style="margin-top:10px;padding-top:10px">
        <div class="form-row scroll-table" style="margin-top:10px;">
            <style>
                #tbldata {border-collapse:collapse; table-layout:fixed; width:310px; height: 100px !important;}
                #tbldata td {width:100px;word-wrap:break-word; height:100px;font-size: 20px;}
            </style>
            <table id="tbldata" border="1" style="width:100%;color:black;">
                <thead style="max-height:165px;">
                    <tr style="max-height:120px;">
                        <td colspan="12">
                            <div class="row">
                                <div class="col-log-4" style="margin-left:40px;"><img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo"></div>
                                <div class="col-log-8" style="margin-left:240px; text-align: center"><h2>Tablet Tooling Card Log List</h2></div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color:#4e73df;color:#fff;height:60px;">
                        <th width="8%">Product Code</th>
                        <th width="10%">Supplier</th>
                        <th width="8%">Upper Punch-Qty./Embossing</th>
                        <th width="8%">Lower Punch-Qty./Embossing</th>
                        <th width="8%">Year</th>
                        <th width="8%">Punch Set</th>
                        <th width="8%">Shape</th>
                        <th width="8%">Machine</th>
                        <th width="8%">Die- Quantity</th>
                        <th width="8%">Last Modified By</th>
                        <th width="10%">Last Modified On</th>
                        <th width="8%">Status</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($res as $r){?>
                    <tr style="max-height: 100px">
                        <td><?php echo $r['product_code'];?></td>
                        <td><?php echo $r['supplier'];?></td>
                        <td><?php echo $r['upper_punch_qty'];?></td>
                        <td><?php echo $r['lower_punch_qty'];?></td>
                        <td><?php echo $r['year'];?></td>
                        <td><?php echo $r['no_of_subset'];?></td>
                        <td><?php echo $r['shape'];?></td>
                        <td><?php echo $r['machine'];?></td>
                        <td><?php echo $r['die_quantity'];?></td>
                        <td><?php echo $r['modified_by'];?></td>
                        <td><?php echo $r['modified_on'];?></td>
                        <td><?php echo $r['status'];?></td>
                    </tr>
                <?php } ?>
                </tbody>                                
            </table>
        </div>
    </div>
<?php } ?>