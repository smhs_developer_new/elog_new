<?php if(!empty($res)){?>
    <div class="card-body ht750" id="audit_trail2" style="margin-top:10px;padding-top:10px">
        <div class="form-row scroll-table" style="margin-top:10px;">
            <style>
                #tbldata {border-collapse:collapse; table-layout:fixed; width:310px; height: 100px !important;}
                #tbldata td {width:100px;word-wrap:break-word; height:100px;font-size: 20px;}
            </style>
            <table id="tbldata" border="1" style="width:100%;color:black;">
                <thead style="max-height:165px;">
                    <tr style="max-height:120px;">
                        <td colspan="8">
                            <div class="row">
                                <div class="col-log-4" style="margin-left:40px;"><img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo"></div>
                                <div class="col-log-8" style="margin-left:240px; text-align: center"><h2>Area List</h2></div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color:#4e73df;color:#fff;height:60px;">
                        <th width="11%">Area Code</th>
                        <th width="15%">Area Name</th>
                        <th width="11%">No. Of Rooms</th>
                        <th width="11%">Block Code</th>
                        <th width="11%">Sub Block Code</th>
                        <th width="11%">Last Modified By</th>
                        <th width="15%">Last Modified On</th>
                        <th width="15%">Status</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($res as $r) {?>
                    <tr style="max-height: 100px">
                        <td><?php echo $r['area_code'];?></td>
                        <td><?php echo $r['area_name'];?></td>
                        <td><?php echo $r['number_of_rooms'];?></td> 
                        <td><?php echo $r['block_code'];?></td>  
                        <td><?php echo $r['department_code'];?></td>
                        <td><?php echo $r['modified_by'];?></td>
                        <td><?php echo $r['modified_on'];?></td>
                        <td><?php echo $r['is_active'];?></td>
                    </tr>
                <?php } ?>
                </tbody>                            
            </table>
        </div>
    </div>
<?php } ?>