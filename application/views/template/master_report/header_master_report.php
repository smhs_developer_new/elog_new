<?php if(!empty($res)){?>
    <div class="card-body ht750" id="audit_trail2" style="margin-top:10px;padding-top:10px">
        <div class="form-row scroll-table" style="margin-top:10px;">
            <style>
                #tbldata {border-collapse:collapse; table-layout:fixed; width:310px; height: 100px !important;}
                #tbldata td {width:100px;word-wrap:break-word; height:100px;font-size: 20px;}
            </style>
            <table id="tbldata" border="1" style="width:100%;color:black;">
                <thead style="max-height:165px;">
                    <tr style="max-height:120px;">
                        <td colspan="9">
                            <div class="row">
                                <div class="col-log-4" style="margin-left:40px;"><img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo"></div>
                                <div class="col-log-8" style="margin-left:240px; text-align: center"><h2>Report Header List</h2></div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color:#4e73df;color:#fff;height:60px;">
                        <th width="15%">Header Name</th>
                        <th width="15%">Form No.</th>
                        <th width="10%">Version No.</th>
                        <th width="10%">Effective Date</th>
                        <th width="10%">Retired Date</th>
                        <th width="10%">Document No.</th>
                        <th width="10%">Last Modified By</th>
                        <th width="12%">Last Modified On</th>
                        <th width="8%">Status</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($res as $r) {?>
                    <tr style="max-height: 100px">
                        <td><?php echo $r['activity_name'];?></td>
                        <td><?php echo $r['form_no'];?></td>
                        <td><?php echo $r['version_no'];?></td>
                        <td><?php echo $r['effective_date'];?></td>
                        <td><?php echo $r['retired_date'];?></td>
                        <td><?php echo $r['document_no'];?></td>
                        <td><?php echo $r['modified_by'];?></td>
                        <td><?php echo $r['modified_on'];?></td>
                        <td><?php echo ucwords($r['status']);?></td>
                    </tr>
                <?php } ?>
                </tbody>                                
            </table>
        </div>
    </div>
<?php } ?>