<?php if(!empty($data)){?>
    <div class="card-body ht750" id="audit_trail2">
        <div class="form-row scroll-table" style="margin-top:10px;">
            <style>
                #tbldata {border-collapse:collapse; table-layout:fixed; width:310px; height: 100px !important;}
                #tbldata td {width:100px;word-wrap:break-word; height:100px;font-size: 20px;}
            </style>
            <table id="tbldata" border="1" style="width:100%;color:black;">
                <thead style="max-height:165px;">
                    <tr style="max-height:120px;">
                        <td colspan="9">
                            <div class="row">
                                <div class="col-log-4" style="margin-left:40px;"><img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo"></div>
                                <div class="col-log-8" style="margin-left:280px; text-align: center"><h2>Employee List</h2></div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color:#4e73df;color:#fff;height:60px;">
                        <th width="5%">S.No.</th>
                        <th width="10%">Emp. Code</th>
                        <th width="25%">Emp. Name</th>
                        <th width="10%">Emp. Id</th>
                        <th width="10%">Contact</th>
                        <th width="10%">Role</th>
                        <th width="10%">Block</th>
                        <th width="10%">Created By</th>
                        <th width="10%">Created On</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($data as $row) {?>
                    <tr style="max-height: 100px">
                        <td><?php echo $row['sn']; ?></td>
                        <td><?php echo $row['emp_code']; ?></td>
                        <td><?php echo $row['emp_name']; ?></td>
                        <td><?php echo $row['emp_email']; ?></td>
                        <td><?php echo $row['emp_contact']; ?></td>
                        <td><?php echo $row['role_name']; ?></td>
                        <td><?php echo $row['block_code']; ?></td>
                        <td><?php echo $row['created_by']; ?></td>
                        <td><?php echo $row['created_on']; ?></td>
                    </tr>
                <?php } ?>
                </tbody>                                
            </table>
        </div>
    </div>
<?php } ?>

