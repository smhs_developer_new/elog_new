<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'report',"status"=>'active'))->row_array();
$show = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $show = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_edit']!=1) {
            $url=base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '".$url."';\",1);</script>";
        } elseif ($res['is_edit'] == 1) {
            $show = true;
        } else {
            $show = false;
        }
    } else {
        $url=base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '".$url."';\",3000);</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="smhs" content="">

        <title>E-Logbook</title>

        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url() ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>css/fontfamily.css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="<?php echo base_url() ?>css/sb-admin-2.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/custom.css" rel="stylesheet">

        <link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
        <style>
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
            .pagination {
                display: inline-block;
                padding-left: 0;
                margin: 20px 0;
                border-radius: 4px;
            }
            .pagination > li {
                display: inline;
            }
        </style>


<!-- <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" /> -->
    </head>

    <body id="page-top" ng-app="reportApp" ng-controller="reportCtrl" ng-cloak>

        <!-- Page Wrapper -->
        <div id="wrapper">

            <?php $this->load->view("template/report/report_header"); ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->

                <div class="content-wrapper">
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="col-auto">
                                <div class="form-row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-4 text-center"><h2>Master Form download reports</h2></div>
                                </div>
                                <div class="form-row">
<!--                                    <div class="col-lg-3 mb-3"><label><b>Block</b></label>
                                        <select class="form-control" tabindex="4" name="block_name" data-placeholder="Search Block" ng-change="hideReport()"  ng-model="block_name">
                                            <option value="">Select Block</option>
                                            <option value="{{dataObj.block_code}}" ng-repeat="dataObj in blockListData">{{dataObj.block_code}}=>{{dataObj.block_name}}</option>
                                        </select>
                                    </div>-->
                                    <div class="col-lg-3 mb-3"><label><b>Master Forms</b></label>
                                        <select class="form-control" tabindex="4" name="form_name" data-placeholder="Search Form" ng-change="hideReport()" id="form_name"  ng-model="form_name">
                                            <option value="">Select Master Form</option>
                                            <option value="{{dataObj.master_url}}" ng-repeat="dataObj in masterFormListData" ng-hide="dataObj.id==22 || dataObj.id==24">{{dataObj.master_name}}</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label></label>
                                        <button style="margin-top: 25px;" type="button" ng-click="masterReportData()" class="btn btn-success">Go</button>
                                    </div>
                                    <div class="col-lg-4 mb-4 text-center">
                                        <div class="row">
                                            <div class="col-lg-2 showReport" ng-if="masterTableData.length > 0"><button style="margin-top:25px;" onclick="ExportPdf()"><i class="fas fa-file-pdf" style="font-size:24px"></i></button></div>
                                            <div class="col-lg-2 showReport" ng-if="masterTableData.length > 0"><button style="margin-top:25px;" onclick="printExcel()"><i class="fas fa-file-excel" style="font-size:24px"></i></button></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="master_data_div" style="display:none"></div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->
        <script type="x/kendo-template" id="page-template">
                        #
        var footer_name = foot_name;
    #
                        <div class="page-template">
                            <div class="footer">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-3"><b>Print By : <?php echo $this->session->userdata('empname') ?></b></div>
                                                <div class="col-md-3"><b>Date : <?php echo date("d-M-Y"); ?></b></div>
                                                <div class="col-md-3"><b>Time :#=footer_name#</b></div>
                                                <div class="col-md-3"><b>Page #: pageNum # of #: totalPages #</b></div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </script>
        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>SMHS. &copy; 2020 - 2021. All Rights Reserved.</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->
<style>
            /* Page Template for the exported PDF */
            .page-template {
              font-family: "DejaVu Sans", "Arial", "sans-serif";
              position: absolute;
              width: 100%;
              height: 100%;
              color: black;
              top: 0;
              left: 0;
            }
            .page-template .header {
              position: absolute;
              top: 0px;
              left: 30px;
              right: 30px;
              color: black;
              bottom:20px;
              margin-bottom: 10px;
              padding-bottom: 10px;
            }
            .page-template .footer {
              position: absolute;
              bottom: 10px;
              left: 30px;
              right: 30px;
              border-top: 1px solid black;
              text-align: center;
              color: #000000;
            }
            .k-grid-header .k-header {
                height: 10px;
                padding: 0;
              }

              .k-grid tbody tr {
                line-height: 14px !important;
              }

              .k-grid tbody td {
                padding: 0;
              }
        </style>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="<?php echo base_url() ?>user/logout">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->

<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>js/lodash.min.js"></script>
<script src="<?php echo base_url() ?>js/angularjs-dropdown-multiselect.min.js"></script>
<script src="<?php echo base_url() ?>js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>js/kendo.all.min.js"></script>
<script src="<?php echo base_url() ?>js/moment-with-locales.js"></script>
<script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
<script src="<?php echo base_url() ?>js/dirPagination.js"></script> <!-- Pagination link -->
<script type="text/javascript">
                                var foot_name = '';
                                var url_base_path = '<?php echo base_url() ?>';
                                var app = angular.module("reportApp", ['angular.chosen']);
                                app.controller("reportCtrl", function ($scope, $http, $filter) {
                                    foot_name = moment(new Date()).format('HH:mm:ss');
                                    $scope.MasterTrailData = [];
                                    $scope.current_date = moment().format('DD-MMM-YYYY');
                                    $scope.start_date = moment().format('DD-MMM-YYYY');
                                    $scope.end_date = moment().format('DD-MMM-YYYY');
                                    $scope.form_name="";
                                    $scope.masterReportData = function () {
                                        var valid = false;
                                        if (($scope.form_name!= "" && $scope.form_name!= "undefined")) {
                                            valid = true;
                                        }
                                        if (valid) {
                                            $scope.masterTableData = [];
                                            $http({
                                                url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/masterReportAll',
                                                method: "POST",
                                                data: "block_code=" + $scope.block_name + "&form_name=" + $scope.form_name,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.masterTableData = response.data.data;
                                                $('#master_data_div').html(response.data.data);
                                            }, function (error) { // optional
                                                toaster.pop('error', "error", "Something went wrong.Please try again");
                                            });
                                        } else {
                                            alert("Please Select Master Form");
                                        }
                                    }
//                                    $scope.masterReportData();

                                    $scope.getMasterFormList = function () {
                                        $scope.masterFormListData = [];
                                        $http({
                                            url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/getMasterData',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.masterFormListData = response.data.master_list;
                                            //console.log($scope.masterFormListData);
                                        }, function (error) { // optional
                                            toaster.pop('error', "error", "Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getMasterFormList();
                                    
                                    $scope.getBlockList = function () {
                                        $scope.blockListData = [];
                                        $http({
                                            url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/GetBlockList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.blockListData = response.data.block_data;
                                            //console.log($scope.blockListData);
                                        }, function (error) { // optional
                                            toaster.pop('error', "error", "Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getBlockList();

                                    $scope.hideReport = function() {
                                        $('.showReport').hide();
                                        foot_name =  moment(new Date()).format('HH:mm:ss');
                                    };

                                });
                                function ExportPdf() {
                                    $("#master_data_div").show();
                                    //$("#pdf_download").show();
                                    var pdf_name = $( "#form_name option:selected" ).text();
                                    kendo.drawing
                                            .drawDOM("#master_data_div",
                                                    {
                                                        paperSize: "A3",
                                                        landscape: true,
                                                        pageable: true,
                                                        repeatHeaders:true,
                                                        template: $("#page-template").html(),
                                                        margin: {top: "100px", bottom: "20px"},
                                                        color:"black",
                                                        //padding: {top: "40px", bottom: "40px"},
                                                        scale: 0.7,
                                                        height: 1200
                                                    })
                                            .then(function (group) {
                                                kendo.drawing.pdf.saveAs(group, pdf_name+".pdf")
                                            }).always(function () {
                                        //$("#pdf_download").hide();
                                        $("#master_data_div").hide();
                                    });
                                }

                                function printDiv()
                                {
                                  //$("#master_data_div").show();
                                  var divToPrint=document.getElementById('master_data_div');

                                  var newWin=window.open('','Print-Window');

                                  newWin.document.open();

                                  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

                                  newWin.document.close();

                                  setTimeout(function(){newWin.close();},1000);
                                  $("#master_data_div").hide();
                                }

                                function printExcel()
                                {
                                   var form_name = $("select[name=form_name]").val();//$('#form_name').val();
                                   var form_val = $("#form_name option:selected").text();//$('#form_name').val();
                                   var block_code = $("select[name=block_name]").val();//$('#block_code').val();
                                   if((form_name != '' || block_code !='' || form_val !='') && (form_name != 'undefined' || block_code !='undefined' || form_val !="undefined")){
                                        url = url_base_path + 'Rest/Pontasahibelog/Pontasahib/masterReportAll?operation=excel&form_name='+form_name+'&block_code='+block_code+'&form_val='+form_val;
                                        window.open(url, '_blank');
                                   }
                                }
</script>
<script src="<?php echo base_url(); ?>assets/js/disable_editor.js"></script>
</body>

</html>
