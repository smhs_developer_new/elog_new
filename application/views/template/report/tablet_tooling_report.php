<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'report', "status" => 'active'))->row_array();
$show = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $show = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_edit'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1) {
            $show = true;
        } else {
            $show = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",3000);</script>";
    }
}
$this->db->order_by("id", "DESC");
$footerData = $this->db->get_where("pts_mst_report_footer", array("report_id" => $_GET['module_id'], "status" => "active"))->result_array();
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="smhs" content="">

        <title>E-Logbook</title>

        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url() ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="<?php echo base_url() ?>css/fontfamily.css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="<?php echo base_url() ?>css/sb-admin-2.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/custom.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
        <style>
            [ng\:cloak],
            [ng-cloak],
            [data-ng-cloak],
            [x-ng-cloak],
            .ng-cloak,
            .x-ng-cloak {
                display: none !important;
            }
        </style>

    </head>

    <body id="page-top" ng-app="reportApp" ng-controller="reportCtrl" ng-cloak>

        <!-- Page Wrapper -->
        <div id="wrapper">
            <?php $this->load->view("template/report/report_header"); ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->

                <div class="content-wrapper">
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="col-auto">
                                <div class="form-row">
                                    <div class="col-lg-3 mb-3"><label><b>Room Name</b> <span style="color: red">*</span></label>

                                        <select class="chzn-select form-control" tabindex="4" name="room_code" data-placeholder="Search Room Code" ng-change="refreshEqupList()" ng-options="dataObj['room_code'] as (dataObj.room_code +           ' => ' +           dataObj.room_name +           ' => ' +           dataObj.area_id) for dataObj in sysRoomData"  ng-model="room_code" required chosen>
                                            <option value="">Select Room Name</option>
                                        </select>


                                    </div>
                                    <div class="col-lg-2 mb-3"><label>Product Name <span style="color: red">*</span></label>
                                        <select class="chzn-select form-control" tabindex="4"
                                                ng-change="GetTabletToolingpuchsetdata()" name="product_no"
                                                data-placeholder="Search Product No."
                                                ng-options="dataObj['product_code'] as (dataObj.product_code) for dataObj in productData"
                                                ng-model="product_no" required chosen>
                                            <option value="">Select Product code</option>
                                        </select>
                                        <p><b ng-if="product_desc != ''">{{product_desc}}</b></p>
                                    </div>
                                    <div class="col-lg-2 mb-3"><label>Punch set <span style="color: red">*</span></label>
                                        <select class="chzn-select form-control" tabindex="4"
                                                ng-change="getheaderDetail()" name="punchset"
                                                data-placeholder="Search punch_set No."
                                                ng-options="dataObj['punch_set'] as (dataObj.punch_set) for dataObj in punchsetdata"
                                                ng-model="punchset" required chosen>
                                            <option value="">Select punch_set no</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 mb-3"><label><b>Start Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-12 ng-isolate-scope" moment-picker="start_date" format="DD-MMM-YYYY" start-view="month" locale="en" today="true" max-date="current_date">

                                                <input class="form-control ng-pristine ng-valid ng-scope moment-picker-input ng-empty ng-touched" name="start_date" placeholder="Select Start Date" ng-model="start_date" ng-model-options="{updateOn: 'blur'}" tabindex="0" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 mb-3"><label><b>End Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-12 ng-isolate-scope" moment-picker="end_date" format="DD-MMM-YYYY" start-view="month" min-date="start_date" locale="en" today="true" max-date="current_date">

                                                <input class="form-control  ng-pristine ng-valid ng-scope moment-picker-input ng-empty ng-touched" name="end_date" placeholder="Select End Date" ng-model="end_date" ng-model-options="{updateOn: 'blur'}" tabindex="0" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1 mb-3"><label></label>
                                        <button style="margin-top: 25px;" class="btn btn-success"
                                                ng-click="getHeaderList()">Go</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="card card-default mt-4">
                    <div class="form-row">
                        <div class="col-lg-10 text-center"></div>
                        <?php if ($show) { ?>
                            <div class="col-lg-2" ng-if="prolog.length > 0"><button style="margin-top:5px;"  onclick="ExportPdf()"><i class="fas fa-file-pdf" style="font-size:24px"></i></button></div>
                        <?php } ?>
                    </div>

                    <!--                <div class="form-row">
                                        <div class="col-lg-10 text-center"></div>
                                        <div class="col-lg-2" ng-if="prolog.length > 0"><button style="margin-top:5px;"
                                                onclick="ExportPdf()"><i class="fas fa-file-pdf" style="font-size:24px"></i></button></div>
                                    </div>-->
                    <div class="card-body" id="tablet_tooling_log">
                        <!--<div class="form-row">
                                <div class="col-md-2">
                                    <img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo">
    
                                </div>
                                <div class="col-md-10">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <th><b>TITLE</b></th>
                                                <td colspan="4">Swab Sample</td>
                                            </tr>
                                            <tr>
                                            <tr>
                                                <th>Form No.</th>
                                                <th>Version No. &amp; Status</th>
                                                <th>Effective Date</th>
                                                <th>Retired Date</th>
                                                <th>Document Reference No.</th>
                                            </tr>
                                        </tbody>
                                        <tbody>
                                            <tr>
                                                <td>{{headerData.length > 0?reportHeader.form_no:'NA'}}</td>
                                                <td>{{headerData.length > 0?reportHeader.version_no+' & Effective':'NA'}} </td>
                                                <td>{{headerData.length > 0?(reportHeader.effective_date| date:'dd-MMM-yyyy'):'NA'}}</td>
                                                <td>{{headerData.length > 0?(reportHeader.retired_date| date:'dd-MMM-yyyy'):'NA'}}</td>
                                                <td>{{headerData.length > 0?reportHeader.document_no:'NA'}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>-->
                        <div class="form-row">
                            <div class="col-md-2">
                                <img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo">

                            </div>
                            <div class="col-lg-10 text-center">
                                <h2>TABLET TOOLING LOG CARD</h2>
                            </div>
                        </div>
                        <div class="form-row scroll-table" style="margin-top:10px;">
                            <table class="table table-bordered">
                                <tr>
                                    <th>Product</th>
                                    <td>{{header_data[0].product_code}}</td>
                                    <th>Supplier</th>
                                    <td>{{header_data[0].supplier}}</td>
                                    <th>Upper Punch-Qty./Embossing</th>
                                    <td>{{header_data[0].upper_punch + '/' + header_data[0].upper_embossing}}</td>
                                </tr>
                                <tr>
                                    <th>Dimension</th>
                                    <td>{{header_data[0].dimension}}</td>
                                    <th>Year</th>
                                    <td>{{header_data[0].year}}</td>
                                    <th>Lower Punch-Qty./Embossing</th>
                                    <td>{{header_data[0].lower_punch + '/' + header_data[0].lower_embossing}}</td>
                                </tr>
                                <tr>
                                    <th>Shape</th>
                                    <td>{{header_data[0].shape}}</td>
                                    <th>Machine</th>
                                    <td>{{header_data[0].machine}}</td>
                                    <th>Die- Quantity</th>
                                    <td>{{header_data[0].die_quantity}}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-row scroll-table" style="margin-top:10px;">
                            <table class="table table-bordered custom-table">
                                <thead style="border-top:10px">
                                    <tr>
                                        <th rowspan="3">Date </th>
                                        <th colspan="3">Issuance</th>
                                        <th colspan="3">Cleaning Before Use</th>
                                        <th colspan="7">Inspection/Verification before use</th>
                                        <th colspan="4">Usage</th>
                                        <th colspan="3">Cleaning after use</th>
                                        <th colspan="5">Inspection after use</th>
                                        <th colspan="5">Number of punch/die Returned to Storage Cabinet</th>
                                        <th rowspan="3">Remark</th>
                                    </tr>
                                    <tr>
                                        <th colspan="3">Total Qty. issued(Record individual punch/die No.)</th>
                                        <th rowspan="2">From</th>
                                        <th rowspan="2">To</th>
                                        <th rowspan="2">Cleaned By</th>
                                        <th colspan="3">Total Qty. Inspected</th>
                                        <th rowspan="2">Damaged(If any)</th>
                                        <th rowspan="2">Inspected By</th>
                                        <th rowspan="2">Checked By</th>
                                        <th rowspan="2">Verified By(QA)</th>
                                        <th rowspan="2">B. No.</th>
                                        <th rowspan="2">Tab Qty Nos.</th>
                                        <th rowspan="2">Cum Qty(Lac)</th>
                                        <th rowspan="2">Cum Qty / Punch (Lac)</th>
                                        <th rowspan="2">From</th>
                                        <th rowspan="2">To</th>
                                        <th rowspan="2">Cleaned By</th>
                                        <th colspan="3">Total Qty. Inspected</th>
                                        <th rowspan="2">Damaged(If any)</th>
                                        <th rowspan="2">Inspected By</th>
                                        <th rowspan="2">U</th>
                                        <th rowspan="2">L</th>
                                        <th rowspan="2">D</th>
                                        <th rowspan="2">Returned By</th>
                                        <th rowspan="2">Checked By</th>
                                    </tr>
                                    <tr>
                                        <th>U</th>
                                        <th>L</th>
                                        <th>D</th>
                                        <th>U</th>
                                        <th>L</th>
                                        <th>D</th>
                                        <th>U</th>
                                        <th>L</th>
                                        <th>D</th>
                                    </tr>
                                </thead>
                                <tbody ng-repeat="dataObj in prolog">
                                    <tr ng-if="dataObj.act_type == 'Issuance'">
                                        <td>{{dataObj.created_date}}</td>
                                        <td>{{dataObj.U}}</td>
                                        <td>{{dataObj.L}}</td>
                                        <td>{{dataObj.D}}</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>{{dataObj.done_by_remark}}</td>
                                    </tr>
                                    <tr  ng-if="dataObj.act_type == 'Cleaning Before Use'">
                                        <td>{{dataObj.created_date}}</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>{{dataObj.tablet_tooling_from}}</td>
                                        <td>{{dataObj.tablet_tooling_to}}</td>
                                        <td>{{dataObj.user_name}}<br>({{dataObj.created_date + ' ' + dataObj.usr_act_log_start_time}})</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>{{dataObj.done_by_remark}}</td>
                                    </tr>
                                    <tr  ng-if="dataObj.act_type == 'Inspection / Verification Before Use'">
                                        <td>{{dataObj.created_date}}</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>{{dataObj.U}}</td>
                                        <td>{{dataObj.L}}</td>
                                        <td>{{dataObj.D}}</td>
                                        <td>{{dataObj.damaged}}</td>
                                        <td>{{dataObj.user_name}}<br>({{dataObj.created_date + ' ' + dataObj.usr_act_log_start_time}})</td>
                                        <td>{{dataObj.checked_by}}<br>({{dataObj.stopdate + ' ' + dataObj.usr_act_log_stop_time}})</td>
                                        <td>{{dataObj.qaApproved}}<br>({{dataObj.qaDate}})</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>{{dataObj.done_by_remark}}</td>
                                    </tr>
                                    <tr ng-if="dataObj.act_type == 'Usage'">
                                        <td>{{dataObj.created_date}}</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>{{dataObj.b_no}}</td>
                                        <td>{{dataObj.tab_qty}}</td>
                                        <td>{{dataObj.cum_qty}}</td>
                                        <td>{{dataObj.cum_qty_punch}}</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>{{dataObj.done_by_remark}}</td>
                                    </tr>
                                    <tr  ng-if="dataObj.act_type == 'Cleaning After Use'">
                                        <td>{{dataObj.created_date}}</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>{{dataObj.tablet_tooling_from}}</td>
                                        <td>{{dataObj.tablet_tooling_to}}</td>
                                        <td>{{dataObj.user_name}}<br>({{dataObj.created_date + ' ' + dataObj.usr_act_log_start_time}})</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>{{dataObj.done_by_remark}}</td>
                                    </tr>
                                    <tr  ng-if="dataObj.act_type == 'Inspection After Use'">
                                        <td>{{dataObj.created_date}}</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>{{dataObj.U}}</td>
                                        <td>{{dataObj.L}}</td>
                                        <td>{{dataObj.D}}</td>
                                        <td>{{dataObj.damaged}}</td>
                                        <td>{{dataObj.user_name}}<br>({{dataObj.created_date + ' ' + dataObj.usr_act_log_start_time}})</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>{{dataObj.done_by_remark}}</td>
                                    </tr>
                                    <tr  ng-if="dataObj.act_type == 'No. of Punch/Die Returned to Storage Cabinet'">
                                        <td>{{dataObj.created_date}}</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>NA</td>
                                        <td>{{dataObj.U}}</td>
                                        <td>{{dataObj.L}}</td>
                                        <td>{{dataObj.D}}</td>
                                        <td>{{dataObj.user_name}}<br>({{dataObj.created_date + ' ' + dataObj.usr_act_log_start_time}})</td>
                                        <td>{{dataObj.checked_by}}<br>({{dataObj.stopdate + ' ' + dataObj.usr_act_log_stop_time}})</td>
                                        <td>{{dataObj.done_by_remark}}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="col-lg-12 text-center" ng-show="prolog.length == 0">
                                <h2>No Data Found</h2>
                            </div>
                            <div class="col-lg-12 text-center" ng-show="prolog.length == 0">
                                <h4>(Please Select the above parameters)</h4>
                            </div>
                        </div>
                        <div class="form-row" style="margin-top:10px;">
                            <div class="col-md-3">
                                <p>U=UPPER PUNCH</p>
                            </div>
                            <div class="col-md-3">
                                <p>L=LOWER PUNCH</p>
                            </div>
                            <div class="col-md-3">
                                <p>D=DIE</p>
                            </div>
                        </div>
                        <div class="form-row" style="margin-top:10px;" ng-show="headerData.length > 0">
                            <b>FORM NO.</b>&nbsp;{{headerData.length > 0?reportHeader.footer_name:'NA'}}
                        </div>
                        <div class="form-row pb-2" id="pdf_download" style="display:none">
                            <div class="col-md-4"></div>
                            <div class="col-md-3">Print By : <?php echo $this->session->userdata('empname') ?></div>
                            <div class="col-md-3">Date : <?php echo date("d-m-Y"); ?></div>
                            <div class="col-md-2">Time : <?php echo date("H:i:s"); ?></div>
                        </div>
                        <!-- <?php if (!empty($footerData)) { ?>
                                    <div class="form-row" style="margin-top:10px;">
                                        <b>FORM NO.</b>&nbsp;<?php echo $footerData[0]['footer_name'] ?>
                                </div>
                        <?php } ?> -->
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>SMHS. &copy; 2020 - 2021. All Rights Reserved.</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div> <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="<?php echo base_url() ?>user/logout">Logout</a>
            </div>
        </div>
    </div>
</div>
<script type="text/kendo-template" id="page-template">
    #
    var footer_name = foot_name;
    #
    <div class="page-template">
    <div class="footer">
    <table class="table">
    <tr>
    <td>
    <div class="form-row" style="margin-top:10px;">
    <div class="col-md-3"><b>FORM NO.</b>&nbsp;<div id="footer_no1">#=footer_name#</div></div>
    <div class="col-md-3">
    <p>U=UPPER PUNCH</p>
    </div>
    <div class="col-md-3">
    <p>L=LOWER PUNCH</p>
    </div>
    <div class="col-md-3">
    <p>D=DIE</p>
    </div>
    </div>
    <div class="row">
    <div class="col-md-3"><b>Print By : <?php echo $this->session->userdata('empname') ?></b></div>
    <div class="col-md-3"><b>Date : <?php echo date("d-M-Y"); ?></b></div>
    <div class="col-md-3"><b>Time :<?php $timezone = "Asia/Kolkata";
                        date_default_timezone_set($timezone);
                        echo date("H:i:s"); ?></b></div>
    <div class="col-md-3"><b>Page #: pageNum # of #: totalPages #</b></div>
    </div>
    </td>
    </tr>
    </table>
    </div>
    </div>
</script>
<div id="master_data_div" style="display: none;">
    <div class="card-body ht750" id="audit_trail2" style="margin-top:10px;padding-top:10px">                      
        <div class="form-row scroll-table" style="margin-top:10px;">
            <style>
                #tbldata {border-collapse:collapse; table-layout:fixed; width:310px; height: 100px !important;}
                #tbldata td {width:100px;word-wrap:break-word; height:100px;font-size: 15px;}
                #tbldata tr td, #tbldata tr th {word-break: break-word!important; padding: 4px!important;font-size:15px!important;}
                #tbldata tr th{/*color:#fff!important;*/font-size:15px!important;}
                #tbldata .table{color:#000!important;font-size:15px!important;}
            </style>
            <table id="tbldata" border="1" style="width:100%;color:black;">
                <thead style="max-height:165px;">
                    <tr style="max-height:80px;">
                        <td colspan="32">
                            <div class="form-row scroll-table">
                                <div class="col-md-2">
                                    <img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo">
                                </div>
                                <div class="col-md-10 text-center">
                                    <h2><b>TABLET TOOLING LOG CARD</b></h2>
                                </div>
                            </div>
                            <div class="form-row scroll-table">
                                <!-- ngIf: prolog.length > 0 -->
                                <div class="col-12">
                                    <table class="table table-bordered" style="colr:black !important">
                                        <tr>
                                            <th style="height:20px !important">Product</th>
                                            <td style="height:20px !important">{{header_data[0].product_code}}</td>
                                            <th style="height:20px !important">Supplier</th>
                                            <td style="height:20px !important">{{header_data[0].supplier}}</td>
                                            <th style="height:20px !important">Upper Punch-Qty./Embossing</th>
                                            <td style="height:20px !important">{{header_data[0].upper_punch + '/' + header_data[0].upper_embossing}}</td>
                                        </tr>
                                        <tr>
                                            <th style="height:20px !important">Dimension</th>
                                            <td style="height:20px !important">{{header_data[0].dimension}}</td>
                                            <th style="height:20px !important">Year</th>
                                            <td style="height:20px !important">{{header_data[0].year}}</td>
                                            <th style="height:20px !important">Lower Punch-Qty./Embossing</th>
                                            <td style="height:20px !important">{{header_data[0].lower_punch + '/' + header_data[0].lower_embossing}}</td>
                                        </tr>
                                        <tr>
                                            <th style="height:20px !important">Shape</th>
                                            <td style="height:20px !important">{{header_data[0].shape}}</td>
                                            <th style="height:20px !important">Machine</th>
                                            <td style="height:20px !important">{{header_data[0].machine}}</td>
                                            <th style="height:20px !important">Die- Quantity</th>
                                            <td style="height:20px !important">{{header_data[0].die_quantity}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color:#4e73df;color:#fff;height:60px;">
                        <th rowspan="3">Date </th>
                        <th colspan="3">Issuance</th>
                        <th colspan="3">Cleaning Before Use</th>
                        <th colspan="7">Inspection/Verification before use</th>
                        <th colspan="4">Usage</th>
                        <th colspan="3">Cleaning after use</th>
                        <th colspan="5">Inspection after use</th>
                        <th colspan="5">Number of punch/die Returned to Storage Cabinet</th>
                        <th rowspan="3">Remark</th>
                    </tr>
                    <tr style="background-color:#4e73df;color:#fff;height:40px;">
                        <th colspan="3">Total Qty. issued(Record individual punch/die No.)</th>
                        <th rowspan="2">From</th>
                        <th rowspan="2">To</th>
                        <th rowspan="2">Cleaned By</th>
                        <th colspan="3">Total Qty. Inspected</th>
                        <th rowspan="2">Damaged(If any)</th>
                        <th rowspan="2">Inspected By</th>
                        <th rowspan="2">Checked By</th>
                        <th rowspan="2">Verified By(QA)</th>
                        <th rowspan="2">B. No.</th>
                        <th rowspan="2">Tab Qty Nos.</th>
                        <th rowspan="2">Cum Qty(Lac)</th>
                        <th rowspan="2">Cum Qty / Punch (Lac)</th>
                        <th rowspan="2">From</th>
                        <th rowspan="2">To</th>
                        <th rowspan="2">Cleaned By</th>
                        <th colspan="3">Total Qty. Inspected</th>
                        <th rowspan="2">Damaged(If any)</th>
                        <th rowspan="2">Inspected By</th>
                        <th rowspan="2">U</th>
                        <th rowspan="2">L</th>
                        <th rowspan="2">D</th>
                        <th rowspan="2">Returned By</th>
                        <th rowspan="2">Checked By</th>
                    </tr>
                    <tr style="background-color:#4e73df;color:#fff;height:40px;">
                        <th>U</th>
                        <th>L</th>
                        <th>D</th>
                        <th>U</th>
                        <th>L</th>
                        <th>D</th>
                        <th>U</th>
                        <th>L</th>
                        <th>D</th>
                    </tr>
                </thead>
                <tbody ng-repeat="dataObj in prolog">

                    <tr style="max-height: 100px;" ng-if="dataObj.act_type == 'Issuance'">
                        <td>{{dataObj.created_date}}</td>
                        <td>{{dataObj.U}}</td>
                        <td>{{dataObj.L}}</td>
                        <td>{{dataObj.D}}</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>{{dataObj.done_by_remark}}</td>
                    </tr>
                    <tr style="max-height: 100px;" ng-if="dataObj.act_type == 'Cleaning Before Use'">
                        <td>{{dataObj.created_date}}</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>{{dataObj.tablet_tooling_from}}</td>
                        <td>{{dataObj.tablet_tooling_to}}</td>
                        <td>{{dataObj.user_name}}<br>({{dataObj.created_date + ' ' + dataObj.usr_act_log_start_time}})</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>{{dataObj.done_by_remark}}</td>
                    </tr>
                    <tr style="max-height: 100px;"  ng-if="dataObj.act_type == 'Inspection / Verification Before Use'">
                        <td>{{dataObj.created_date}}</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>{{dataObj.U}}</td>
                        <td>{{dataObj.L}}</td>
                        <td>{{dataObj.D}}</td>
                        <td>{{dataObj.damaged}}</td>
                        <td>{{dataObj.user_name}}<br>({{dataObj.created_date + ' ' + dataObj.usr_act_log_start_time}})</td>
                        <td>{{dataObj.checked_by}}<br>({{dataObj.stopdate + ' ' + dataObj.usr_act_log_stop_time}})</td>
                        <td>{{dataObj.qaApproved}}<br>({{dataObj.qaDate}})</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>{{dataObj.done_by_remark}}</td>
                    </tr>
                    <tr style="max-height: 100px;"  ng-if="dataObj.act_type == 'Usage'">
                        <td>{{dataObj.created_date}}</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>{{dataObj.b_no}}</td>
                        <td>{{dataObj.tab_qty}}</td>
                        <td>{{dataObj.cum_qty}}</td>
                        <td>{{dataObj.cum_qty_punch}}</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>{{dataObj.done_by_remark}}</td>
                    </tr>
                    <tr style="max-height: 100px;"  ng-if="dataObj.act_type == 'Cleaning After Use'">
                        <td>{{dataObj.created_date}}</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>{{dataObj.tablet_tooling_from}}</td>
                        <td>{{dataObj.tablet_tooling_to}}</td>
                        <td>{{dataObj.user_name}}<br>({{dataObj.created_date + ' ' + dataObj.usr_act_log_start_time}})</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>{{dataObj.done_by_remark}}</td>
                    </tr>
                    <tr style="max-height: 100px;"  ng-if="dataObj.act_type == 'Inspection After Use'">
                        <td>{{dataObj.created_date}}</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>{{dataObj.U}}</td>
                        <td>{{dataObj.L}}</td>
                        <td>{{dataObj.D}}</td>
                        <td>{{dataObj.damaged}}</td>
                        <td style="height:180px;">{{dataObj.user_name}}<br>({{dataObj.created_date + ' ' + dataObj.usr_act_log_start_time}})</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>{{dataObj.done_by_remark}}</td>
                    </tr>
                    <tr style="max-height: 100px;"  ng-if="dataObj.act_type == 'No. of Punch/Die Returned to Storage Cabinet'">
                        <td>{{dataObj.created_date}}</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>{{dataObj.U}}</td>
                        <td>{{dataObj.L}}</td>
                        <td>{{dataObj.D}}</td>
                        <td>{{dataObj.user_name}}<br>({{dataObj.created_date + ' ' + dataObj.usr_act_log_start_time}})</td>
                        <td>{{dataObj.checked_by}}<br>({{dataObj.stopdate + ' ' + dataObj.usr_act_log_stop_time}})</td>
                        <td>{{dataObj.done_by_remark}}</td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>
<style>
    /* Page Template for the exported PDF */
    .page-template {
        font-family: "DejaVu Sans", "Arial", "sans-serif";
        position: absolute;
        width: 100%;
        height: 100%;
        color: black;
        top: 0;
        left: 0;
    }
    .page-template .header {
        position: absolute;
        top: 0px;
        left: 30px;
        right: 30px;
        color: black;
        bottom:20px;
        margin-bottom: 10px;
        padding-bottom: 10px;
    }
    .page-template .footer {
        position: absolute;
        bottom: 0px;
        left: 30px;
        right: 30px;
        border-top: 1px solid black;
        text-align: center;
        color: #000000;
    }
    .k-grid-header .k-header {
        height: 10px;
        padding: 0;
    }

    .k-grid tbody tr {
        line-height: 14px !important;
    }

    .k-grid tbody td {
        padding: 0;
    }
</style>
<!-- Bootstrap core JavaScript-->

<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>js/lodash.min.js"></script>
<script src="<?php echo base_url() ?>js/angularjs-dropdown-multiselect.min.js"></script>
<script src="<?php echo base_url() ?>js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>js/kendo.all.min.js"></script>
<script src="<?php echo base_url() ?>js/moment-with-locales.js"></script>
<script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
<script type="text/javascript">
                            var foot_name = '';
                            var url_base_path = '<?php echo base_url() ?>';
                            var app = angular.module("reportApp", ['angular.chosen', 'moment-picker']);
                            app.directive('chosen', function ($timeout) {

                                var linker = function (scope, element, attr) {

                                    scope.$watch('productData', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    scope.$watch('punchsetdata', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    scope.$watch('sysRoomData', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    $timeout(function () {
                                        element.chosen();
                                    }, 0, false);
                                };
                                return {
                                    restrict: 'A',
                                    link: linker
                                };
                            });

                            app.controller("reportCtrl", function ($scope, $http, $filter) {
                                $scope.monthArry = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov",
                                    "Dec"
                                ];
                                // checking header
                                $scope.current_date = moment().format('DD-MMM-YYYY');
                                $scope.start_date = moment().format('DD-MMM-YYYY');
                                $scope.end_date = moment().format('DD-MMM-YYYY');
                                $scope.headerData = [];
                                $scope.reportHeader = {};
                                $scope.getHeaderList = function () {
                                    if ($scope.start_date == "" || $scope.start_date == undefined || $scope.end_date == "" || $scope.end_date == undefined)
                                    {
                                        alert("Please Select the date range first...!");
                                        return false;
                                    }
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getHeaderByLogid',
                                        method: "POST",
                                        data: "header_id=" + <?php echo $_GET['mst_act_id'] ?> + "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date,
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.headerData = response.data.header_list;
                                        if ($scope.headerData.length > 0)
                                        {
                                            if ($scope.headerData.length > 1)
                                            {
                                                alert("There are more than one header in this date range please change it, Thank you...!");
                                                $scope.headerData = [];
                                                return false;
                                            } else
                                            {
                                                $scope.reportHeader = response.data.header_list[0];
                                                foot_name = $scope.reportHeader.footer_name;
                                                $scope.getReportData();
                                            }
                                        } else
                                        {
                                            alert("No version is effective in selected date range, please change the dates or check with administrator for \"Report Header/Footer Master\"");
                                            $scope.headerData = [];
                                            return false;
                                        }
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                //End
                                $scope.sysRoomData = [];
                                $scope.prolog = [];
                                $scope.getDeviceRoomList = function () {
                                    $http({
                                        url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/Getassignedroomlist',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.sysRoomData = response.data.room_list;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getDeviceRoomList();
                                $scope.productData = [];
                                $scope.GetProductList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProductList',
                                        method: "GET",
                                        headers: {
                                            'Content-Type': 'application/x-www-form-urlencoded'
                                        },
                                    }).then(function (response) {
                                        $scope.productData = response.data.product_list;
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.GetProductList();
                                $scope.tabletList = [];
                                $scope.getTabletToolingLogList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetTabletToolingLogList?product_code=' +
                                                $scope.product_no,
                                        method: "GET",
                                        headers: {
                                            'Content-Type': 'application/x-www-form-urlencoded'
                                        },
                                    }).then(function (response) {
                                        $scope.tabletList = response.data.tablet_list;
                                        //console.log($scope.tabletList);
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");

                                    });
                                }

                                $scope.punchsetdata = [];
                                $scope.product_desc = "";
                                $scope.GetTabletToolingpuchsetdata = function () {
                                    $scope.product_desc = $filter('filter')($scope.productData, {product_code: $scope.product_no})[0].product_name;
                                    $scope.punchsetdata = [];
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetTabletToolingpuchsetdata2?product_code=' + $scope.product_no,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.punchsetdata = response.data.punch_set_data;
                                        //console.log($scope.punchsetdata);
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                    });
                                }

                                $scope.header_data = [];
                                $scope.getheaderDetail = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetPunchSetDataRecord?punch_set=' + $scope.punchset + "&product_code=" + $scope.product_no + "&room_code=" + $scope.room_code,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.header_data = response.data.tablet_list;
                                        //console.log($scope.header_data);
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                    });
                                }


                                $scope.toolingData = [];
                                $scope.prolog = [];
                                $scope.getReportData = function () {
                                    var valid = true;
                                    if (($scope.product_no == "" || $scope.product_no == undefined) || ($scope.room_code == "" || $scope.room_code == undefined) || ($scope.start_date == "" || $scope.start_date == undefined) || ($scope.end_date == "" || $scope.end_date == undefined)) {
                                        valid = false;
                                    }
                                    if (valid) {
                                        $scope.prolog = [];
                                        $scope.toolingData = [];
                                        $http({
                                            url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/getToolingReportData',
                                            method: "POST",
                                            data: "product_no=" + $scope.product_no + "&punch_set=" + $scope.punchset + "&room_code=" + $scope.room_code + "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date,
                                            headers: {
                                                'Content-Type': 'application/x-www-form-urlencoded'
                                            },
                                        }).then(function (response) {
                                            $scope.toolingData = response.data.row_data;
                                            //console.log($scope.toolingData);
                                            var startdate = "";
                                            var starttime = "";
                                            var stoptime = "";
                                            var startcheckedby = "";
                                            var stopcheckedby = "";
                                            var isworkflow = "no";
                                            var qaApproved = "";
                                            var damagedarray = "";
                                            var damaged = "";
                                            var predoc = "";
                                            var stopdate = "";
                                            var qaDate = "";
                                            angular.forEach($scope.toolingData, function (value, key) {

                                                if (predoc != value["doc_id"])
                                                {
                                                    //console.log(predoc);
                                                    //console.log(value["doc_id"]);
                                                    startdate = "";
                                                    starttime = "";
                                                    stoptime = "";
                                                    startcheckedby = "";
                                                    stopcheckedby = "";
                                                    isworkflow = "no";
                                                    qaApproved = "";
                                                    damagedarray = "";
                                                    damaged = "";
                                                    predoc = "";
                                                    stopdate = "";
                                                    qaDate = "";
                                                }

                                                if (value['workflow_type'] == 'start') {
                                                    stopcheckedby = "";
                                                        stopdate = "";
                                                        stoptime = "";
                                                    if (value['apprl_status'] == 'N/A') {
                                                        startdate = moment(value['start_date']).format('DD-MMM-YYYY');
                                                        starttime = value['start_time'] != null ? value['start_time'] : value['usr_act_log_start_time'];
                                                        isworkflow = "no";
                                                    } else {
                                                        startdate = value['usr_act_log_start_date'];
                                                        starttime = value['usr_act_log_start_time'];
                                                        //stoptime = value['usr_act_log_stop_time'];
                                                        //startcheckedby = value['done_by_user_name'];
                                                        isworkflow = "yes";
                                                    }
                                                } else if (value['workflow_type'] == 'stop') {

                                                    if (value['apprl_status'] == 'N/A') {
                                                        isworkflow = "no";
                                                    } else {
                                                        stopcheckedby = "";
                                                        stopdate = "";
                                                        stoptime = "";
                                                        stoptime = value['usr_act_log_stop_time'];
                                                        stopcheckedby = value['checked_by'];
                                                        isworkflow = "yes";
                                                        stopdate = value["usr_act_log_stop_date"];
                                                    }
                                                } else {
                                                    if (isworkflow == "no") {
                                                        startcheckedby = "";
                                                        stopcheckedby = "";
                                                    }
                                                    if (value['apprl_status'] == 'QAapprove')
                                                    {
                                                        qaApproved = value['checked_by'];
                                                        qaDate = value['checked_date'];
                                                        isworkflow = "yes";
                                                    }
                                                    startdate = value['usr_act_log_start_date'] != null ? value['usr_act_log_start_date'] : moment(value['start_date']).format('DD-MMM-YYYY');
                                                    starttime = moment(value['created_on']).format('HH:mm:ss');
                                                    stoptime = value['usr_act_log_stop_time'];
                                                }
                                                if (value['damaged'] != '' && value['damaged'] != null) {
                                                    damagedarray = value['damaged'].split(',');
                                                    damaged = damagedarray.length;
                                                } else
                                                {
                                                    damaged = "NA";
                                                }
                                                $scope.prolog[value['id']] = {
                                                    "created_date": startdate,
                                                    "usr_act_log_start_time": starttime,
                                                    "usr_act_log_stop_time": stoptime,
                                                    "user_name": value['done_by_user_name'],
                                                    "checked_by": stopcheckedby,
                                                    "qaApproved": qaApproved,
                                                    "done_by_remark": value['done_by_remark'],
                                                    "act_type": value['act_type'],
                                                    "U": value['U'],
                                                    "L": value['L'],
                                                    "D": value['D'],
                                                    "tablet_tooling_from": value['tablet_tooling_from'],
                                                    "tablet_tooling_to": value['tablet_tooling_to'],
                                                    "damaged": damaged,
                                                    "b_no": value['b_no'],
                                                    "tab_qty": value['tab_qty'],
                                                    "cum_qty": value['cum_qty'],
                                                    "cum_qty_punch": value['cum_qty_punch'],
                                                    "stopdate": stopdate,
                                                    "qaDate": qaDate
                                                };
                                                predoc = value["doc_id"];
                                            });
                                            $scope.prolog = $scope.prolog.filter(function () {
                                                return true
                                            });
                                            $scope.prolog.sort(function (a, b) {
                                                return a.created_date - b.created_date;
                                            });
//                    console.log($scope.prolog);

                                        }, function (error) { // optional
                                            toaster.pop('error', "error", "Something went wrong.Please try again");
                                        });
                                    } else {
                                        alert("Please Select Area/Year/Month");

                                    }
                                }


                            });

                            function ExportPdf() {
                                $("#master_data_div").show();
                                kendo.drawing.drawDOM("#master_data_div", {
                                    paperSize: "A3",
                                    landscape: true,
                                    pageable: true,
                                    repeatHeaders: true,
                                    template: $("#page-template").html(),
                                    margin: {top: "100px", bottom: "10px"},
                                    color: "black",
                                    //padding: {top: "40px", bottom: "40px"},
                                    scale: 0.7,
                                    height: 800
                                }).then(function (group) {
                                    kendo.drawing.pdf.saveAs(group, "tablet_tooling.pdf")
                                }).always(function () {
                                    //$("#pdf_download").hide();
                                    $("#master_data_div").hide();
                                });
                            }
</script>
<script src="<?php echo base_url(); ?>assets/js/disable_editor.js"></script>
</body>

</html>