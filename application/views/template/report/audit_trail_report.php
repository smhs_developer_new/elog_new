<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'report', "status" => 'active'))->row_array();
$show = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $show = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_edit'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1) {
            $show = true;
        } else {
            $show = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",3000);</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="smhs" content="">

        <title>E-Logbook</title>

        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url() ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>css/fontfamily.css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="<?php echo base_url() ?>css/sb-admin-2.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/custom.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/kendo.common-material.min.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/kendo.material.min.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/kendo.material.mobile.min.css" />
        <style>
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
            .pagination {
                display: inline-block;
                padding-left: 0;
                margin: 20px 0;
                border-radius: 4px;
            }
            .pagination > li {
                display: inline;
            }

            .content-loader {
                /* position: absolute;
                left: 50%;
                top: 50%; */
                text-align: center;
                display: block;
                left: 54%;
                position: fixed;
                top: 50%;
                z-index: 9;
                margin: -35px auto;
                height: 70px;
                width: 70px;
            }
        </style>

        <!-- <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" /> -->
    </head> 

    <body id="page-top" ng-app="reportApp" ng-controller="reportCtrl" ng-cloak>
        <!-- Page Wrapper -->
        <div id="wrapper">
            <?php $this->load->view("template/report/report_header"); ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="content-wrapper">
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="col-auto">
                                <div class="form-row">
                                    <div class="col-lg-3 mb-3"><label><b>From Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-10" moment-picker="start_date" format="DD-MMM-YYYY"  start-view="month"    locale="en" today="true" max-date="current_date">
                                                <input class="form-control" name="start_date" placeholder="Select Start Date" ng-model="start_date" ng-model-options="{updateOn: 'blur'}" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label><b>To Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-10" moment-picker="end_date" format="DD-MMM-YYYY"   start-view="month" min-date="start_date"  locale="en" today="true" max-date="current_date">
                                                <input class="form-control" name="end_date" placeholder="Select End Date" ng-model="end_date" ng-model-options="{updateOn: 'blur'}" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label><b>User Name</b></label>
                                        <select class="form-control" tabindex="4" name="user_name" data-placeholder="Search User"  ng-model="user_name">
                                            <option value="">Select User Name</option>
                                            <option value="{{dataObj.emp_name}}" ng-repeat="dataObj in userListData">{{dataObj.emp_email}} / {{dataObj.emp_name}}</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label></label>
                                        <button style="margin-top: 25px;" type="button" class="btn btn-success" ng-click="resetReportData(1)">Go</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card card-default mt-4">
                    <div class="card-body ht750" id="audit_trail">
                        <div class="form-row pb-2" id="pdf_download" style="display:none">
                            <div class="col-md-4"></div>
                            <div class="col-md-3">User Name : <?php echo $this->session->userdata('empname') ?></div>
                            <div class="col-md-3">Date : <?php echo date("d-m-Y"); ?></div>
                            <div class="col-md-2">Time : <?php $timezone = "Asia/Kolkata";date_default_timezone_set($timezone);echo date("H:i:s");?></div>
                        </div>
                        <div class="form-row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4 text-center"><h2>Audit Trail Report</h2></div>
                            <?php if ($show) { ?>
                               <div class="col-lg-4 text-right" ng-if="auditTrailData.length > 0"> <button style="margin-top:-15px;"  class="btn_trns" onclick="ExportPdf()"><i class="fas fa-file-pdf pdf_btn" style="color:red;"></i></button></div>
                            <?php } ?>
                            </div>
                        <div class="form-row scroll-table" style="margin-top:10px;">
                            <div ng-show='loading' style="text-align:center" class='content-loader'><img src="<?= base_url('assets/images/loading.gif') ?>" width="70px"></div>
                            <table class="table custom-table tableP runningacttable" style="min-width: 1400px;">
                                <thead>
                                    <tr>
                                        <th>Block Code</th>
                                        <th>User ID</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Doc No.</th>
                                        <th>Room No/<br/>Room Desc</th>
                                        <th>Event Type</th>
                                        <th>Batch No</th>
                                        <th>Product Code and Description</th>
                                        <th style="min-width:300px">Event Detail- Created</th>
                                        <th style="min-width:300px">Event Detail- Modified</th>
                                    </tr>
                                </thead>
                                <tbody dir-paginate="dataObj in auditTrailData |itemsPerPage:itemsPerPage" total-items="total_count" ng-show="auditTrailData.length > 0">
                                    <tr>
                                        <td>{{(dataObj.room_code != null && dataObj.room_code != 0 && dataObj.room_code !='') ? getBlockInfo(dataObj.room_code) :(dataObj.blockCode!=null && dataObj.blockCode!='')? dataObj.blockCode:'NA'}}</td>
                                        <td>{{dataObj.user_name}}</td>
                                        <td>{{dataObj.datetime| format | date:'dd-MMM-yyyy'}}</td>
                                        <td>{{dataObj.datetime| format | date:'HH:mm:ss'}}</td>
                                        <td>{{dataObj.doc_id!=null && dataObj.doc_id!=0?dataObj.doc_id:'NA'}}</td> 
                                        <td>{{dataObj.room_code!=null && dataObj.room_code!=0?dataObj.room_code:'NA'}}/{{dataObj.room_name!=null && dataObj.room_name!=0?dataObj.room_name:'NA'}}</td>
                                        <td>{{dataObj.logname| removeUnderscores | capitalize}} {{dataObj.record_type!='master' && dataObj.actname!=null && dataObj.actname!=0 && dataObj.logname!=dataObj.actname?' / '+dataObj.actname:''}}</td>
                                        <td>{{dataObj.batch_no!=null && dataObj.batch_no!=0?dataObj.batch_no:'NA'}}</td>
                                        <td ng-if="dataObj.product_code != null && dataObj.product_code != 0">{{dataObj.product_code}}({{dataObj.product_name}})</td>
                                        <td ng-if="dataObj.product_code == null || dataObj.product_code == 0">NA</td>
                                        <td style="min-width:300px">
                                            <p ng-if="(dataObj.workflow_type == 'start' && dataObj.approval_status == 'N/A' && dataObj.workflowtype_count =='DoneBy')">{{'Activity has been done by with Remarks(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'start' && dataObj.approval_status == 'N/A' && dataObj.workflowtype_count !='DoneBy')">{{'Activity has been Started with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'stop' && dataObj.approval_status == 'N/A' && dataObj.workflowtype_count =='DoneBy')">{{'Activity has been done by with Remarks(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'stop' && dataObj.approval_status == 'N/A' && dataObj.workflowtype_count !='DoneBy')">{{'Activity has been Stopped with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'start' && dataObj.approval_status == 'approve')">{{'Activity Start has been Approved with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'stop' && dataObj.approval_status == 'approve' &&  dataObj.workflowtype_count !='DoneBy')">{{'Activity has been stopped with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'stop' && dataObj.approval_status == 'approve' &&  dataObj.workflowtype_count =='DoneBy')">{{'Activity has been checked by with remarks(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == NULL && dataObj.approval_status == 'QAapprove')">{{'Activity has been QApproved with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == '0' && dataObj.approval_status == 'file uploaded')"><span ng-bind-html="dataObj.uploadedFileData"></span></p>
                                            <p ng-if="(dataObj.workflow_type == '0' && dataObj.approval_status == 'file deleted')"><span ng-bind-html="dataObj.deletedFileData"></span></p>
                                            <p ng-if="dataObj.logname == 'Room In'">Successful log in : {{dataObj.room_code!=null && dataObj.room_code!=0?dataObj.room_code:'NA'}}<br>{{dataObj.insertedData}}</p>
                                            <p ng-if="dataObj.logname == 'Room Out'">Successful log Out : {{dataObj.room_code!=null && dataObj.room_code!=0?dataObj.room_code:'NA'}}<br>{{dataObj.insertedData}}</p>
                                            <p ng-if="dataObj.logname == 'Batch In'">Successful Batch in : {{dataObj.batch_no!=null && dataObj.batch_no!=0?dataObj.batch_no:'NA'}} Room Code : {{dataObj.room_code!=null && dataObj.room_code!=0?dataObj.room_code:'NA'}}</p>
                                            <p ng-if="dataObj.logname == 'Batch Out'">Successful Batch Out : {{dataObj.batch_no!=null && dataObj.batch_no!=0?dataObj.batch_no:'NA'}} Room Code : {{dataObj.room_code!=null && dataObj.room_code!=0?dataObj.room_code:'NA'}}</p>
                                            <p ng-if="dataObj.logname == 'User Login' || dataObj.logname == 'User Logout' ">{{dataObj.insertedData}}</p>
                                            <p ng-if="(dataObj.insertedData != '' && (dataObj.logname != 'Room In' && dataObj.logname != 'Room Out' && dataObj.logname != 'User Login' && dataObj.logname != 'User Logout'))">Below detail has been created: <br><span ng-bind-html="dataObj.insertedData"></span></p>
                                            <p ng-if="dataObj.insertedWorkflowData != ''"><span ng-bind-html="dataObj.insertedWorkflowData"></span></p>
                                            <p ng-if="dataObj.insertedAccessData != ''" ng-bind-html="dataObj.insertedAccessData"></p>
                                            <p ng-if="dataObj.workflow_type == 0 && dataObj.insertedAccessData == '' && dataObj.insertedWorkflowData == '' && dataObj.approval_status == 0 && dataObj.insertedData == '' && dataObj.logname != 'Room In' && dataObj.logname != 'Room Out' && dataObj.logname != 'Batch In' && dataObj.logname != 'Batch Out' && dataObj.logname != 'User Login'">NA</p>
                                        </td>
                                        <td style="min-width:300px">
                                            <p ng-if="dataObj.updatedData != ''">Below detail has been updated: <br><span ng-bind-html="dataObj.updatedData"></span></p>
                                            <p ng-if="dataObj.updatedWorkflowData != ''"><span ng-bind-html="dataObj.updatedWorkflowData"></span></p>
                                            <p ng-if="dataObj.updatedAccessData != ''"><span ng-bind-html="dataObj.updatedAccessData"></span></p>
                                            <p ng-if="dataObj.deletedData != ''">Below Detailed Record is Deactivated : <br>{{dataObj.deletedData}}</p>
                                            <p ng-if="dataObj.updatedData == '' && dataObj.updatedWorkflowData == '' && dataObj.updatedAccessData == '' && dataObj.deletedData == ''">NA</p>
                                        </td>
                                    </tr>
                                </tbody>                                
                            </table>
                            <div class="col-lg-12">
                                <dir-pagination-controls max-size="8" direction-links="true" boundary-links="true" on-page-change="auditTrailReportData(newPageNumber)"></dir-pagination-controls>
                            </div>
                            <div class="col-lg-12 text-center" ng-show="auditTrailData.length == 0 && loading == false"><h2>No Data Found</h2></div>
                        </div>
                    </div>

                <!-- Below code for audit report pdf out put -->                    
                <div id="1" style="display:none;">
                    <div id="wrapfrm"></div>
                    <div class="card-body ht750" id="audit_trail2" style="display:block;margin-top: 10px;padding-top:120px">
                        <div class="form-row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4 text-center"><h2>Audit Trail Report</h2></div>
                        </div>
                        <div class="form-row scroll-table" style="margin-top:10px;">
                            <style>
                                #tbldata {border-collapse:collapse; table-layout:fixed; width:310px; height: 1000px !important;}
                                #tbldata td {width:100px; word-wrap:break-word; height: 200px !important;}
                                #tbldata tr td, #tbldata tr th {word-break: break-word!important;padding: 4px!important;font-size:25px!important;padding:5px;}
                                #tbldata tr th{color:#fff!important;font-size:25px!important;padding:5px;}
                                #tbldata tr td p{font-size:25px!important;color:black!important;padding:5px;}
                                #tbldata tr td{font-size:25px!important;color:black!important;padding:5px;}
                            </style>
                            <table id="tbldata" border="1" style="width: 100%;">
                                <thead>
                                    <tr style="background-color: #4e73df;color: #fff;">
                                        <th width="5%">Block Code</th>
                                        <th width="5%">User ID</th>
                                        <th width="5%">Date</th>
                                        <th width="5%">Time</th>
                                        <th width="5%">Doc No.</th>
                                        <th width="10%">Room No/<br/>Room Desc</th>
                                        <th width="5%">Event Type</th>
                                        <th width="5%">Batch No</th>
                                        <th width="10%">Product Code and Description</th>
                                        <th width="20%">Event Detail- Created</th>
                                        <th width="20%">Event Detail- Modified</th>
                                    </tr>
                                </thead>
                                <tbody ng-repeat="dataObj in auditTrailData2" ng-show="auditTrailData2.length > 0">
                                    <tr>
                                        <td>{{(dataObj.room_code != null && dataObj.room_code != 0) ? getBlockInfo(dataObj.room_code) : 'NA'}}</td>
                                        <td>{{dataObj.user_name}}</td>
                                        <td>{{dataObj.datetime| format | date:'dd-MMM-yyyy'}}</td>
                                        <td>{{dataObj.datetime| format | date:'HH:mm:ss'}}</td>
                                        <td>{{dataObj.doc_id!=null && dataObj.doc_id!=0?dataObj.doc_id:'NA'}}</td> 
                                        <td>{{dataObj.room_code!=null && dataObj.room_code!=0?dataObj.room_code:'NA'}}/{{dataObj.room_name!=null && dataObj.room_name!=0?dataObj.room_name:'NA'}}</td>
                                        <td>{{dataObj.logname| removeUnderscores | capitalize}} {{dataObj.record_type!='master' && dataObj.actname!=null && dataObj.actname!=0 && dataObj.logname!=dataObj.actname?' / '+dataObj.actname:''}}</td>
                                        <td>{{dataObj.batch_no!=null && dataObj.batch_no!=0?dataObj.batch_no:'NA'}}</td>
                                        <td ng-if="dataObj.product_code != null && dataObj.product_code != 0">{{dataObj.product_code}}({{dataObj.product_name}})</td>
                                        <td ng-if="dataObj.product_code == null || dataObj.product_code == 0">NA</td>
                                        <td width="25%">
                                            <p ng-if="(dataObj.workflow_type == 'start' && dataObj.approval_status == 'N/A' && dataObj.workflowtype_count =='DoneBy')">{{'Activity has been done by with Remarks(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'start' && dataObj.approval_status == 'N/A' && dataObj.workflowtype_count !='DoneBy')">{{'Activity has been Started with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'stop' && dataObj.approval_status == 'N/A' && dataObj.workflowtype_count =='DoneBy')">{{'Activity has been done by with Remarks(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'stop' && dataObj.approval_status == 'N/A' && dataObj.workflowtype_count !='DoneBy')">{{'Activity has been Stopped with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'start' && dataObj.approval_status == 'approve')">{{'Activity Start has been Approved with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'stop' && dataObj.approval_status == 'approve' &&  dataObj.workflowtype_count !='DoneBy')">{{'Activity has been Stopped with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'stop' && dataObj.approval_status == 'approve' &&  dataObj.workflowtype_count =='DoneBy')">{{'Activity has been checked by with remarks(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == NULL && dataObj.approval_status == 'QAapprove')">{{'Activity has been QApproved with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == '0' && dataObj.approval_status == 'file uploaded')"><span ng-bind-html="dataObj.uploadedFileData"></span></p>
                                            <p ng-if="(dataObj.workflow_type == '0' && dataObj.approval_status == 'file deleted')"><span ng-bind-html="dataObj.deletedFileData"></span></p>
                                            <p ng-if="dataObj.logname == 'Room In'">Successful log in : {{dataObj.room_code!=null && dataObj.room_code!=0?dataObj.room_code:'NA'}}<br>{{dataObj.insertedData}}</p>
                                            <p ng-if="dataObj.logname == 'Room Out'">Successful log Out : {{dataObj.room_code!=null && dataObj.room_code!=0?dataObj.room_code:'NA'}}<br>{{dataObj.insertedData}}</p>
                                            <p ng-if="dataObj.logname == 'Batch In'">Successful Batch in : {{dataObj.batch_no!=null && dataObj.batch_no!=0?dataObj.batch_no:'NA'}} Room Code : {{dataObj.room_code!=null && dataObj.room_code!=0?dataObj.room_code:'NA'}}</p>
                                            <p ng-if="dataObj.logname == 'Batch Out'">Successful Batch Out : {{dataObj.batch_no!=null && dataObj.batch_no!=0?dataObj.batch_no:'NA'}} Room Code : {{dataObj.room_code!=null && dataObj.room_code!=0?dataObj.room_code:'NA'}}</p>
                                            <p ng-if="dataObj.logname == 'User Login' || dataObj.logname == 'User Logout' ">{{dataObj.insertedData}}</p>
                                            <p ng-if="(dataObj.insertedData != '' && (dataObj.logname != 'Room In' && dataObj.logname != 'Room Out' && dataObj.logname != 'User Login' && dataObj.logname != 'User Logout'))">Below detail has been created: <br><span ng-bind-html="dataObj.insertedData"></span></p>
                                            <p ng-if="dataObj.insertedWorkflowData != ''"><span ng-bind-html="dataObj.insertedWorkflowData"></span></p>
                                            <p ng-if="dataObj.insertedAccessData != ''" ng-bind-html="dataObj.insertedAccessData"></p>
                                            <p ng-if="dataObj.workflow_type == 0 && dataObj.insertedAccessData == '' && dataObj.insertedWorkflowData == '' && dataObj.approval_status == 0 && dataObj.insertedData == '' && dataObj.logname != 'Room In' && dataObj.logname != 'Room Out' && dataObj.logname != 'Batch In' && dataObj.logname != 'Batch Out' && dataObj.logname != 'User Login'">NA</p>
                                        </td>
                                        <td width="25%">
                                            <p ng-if="dataObj.updatedData != ''">Below detail has been updated: <br><span ng-bind-html="dataObj.updatedData"></span></p>
                                            <p ng-if="dataObj.updatedWorkflowData != ''"><span ng-bind-html="dataObj.updatedWorkflowData"></span></p>
                                            <p ng-if="dataObj.updatedAccessData != ''"><span ng-bind-html="dataObj.updatedAccessData"></span></p>
                                            <p ng-if="dataObj.deletedData != ''">Below Detailed Record is Deactivated : <br>{{dataObj.deletedData}}</p>
                                            <p ng-if="dataObj.updatedData == '' && dataObj.updatedWorkflowData == '' && dataObj.updatedAccessData == '' && dataObj.deletedData == ''">NA</p>
                                        </td>
                                    </tr>
                                </tbody>                                
                            </table>
                        </div>
                    </div>
                </div>
                <script type="x/kendo-template" id="page-template">
                    <div class="page-template">
                        <div class="header">
                            <table class="table">
                                <tr>
                                    <td>
                                        <img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo">
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="footer">
                            <table class="table">
                                <tr>
                                    <td>
                                        <div class="row">
                                            <div class="col-md-3"><b>Print By : <?php echo $this->session->userdata('empname') ?></b></div>
                                            <div class="col-md-3"><b>Date : <?php echo date("d-M-Y"); ?></b></div>
                                            <div class="col-md-3"><b>Time :<?php $timezone = "Asia/Kolkata";date_default_timezone_set($timezone);echo date("H:i:s");?></b></div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-md-3"><b>Page #: pageNum # of #: totalPages #</b></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </script>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>SMHS. &copy; 2020 - 2021. All Rights Reserved.</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->
<style>
            /* Page Template for the exported PDF */
            .page-template {
              font-family: "DejaVu Sans", "Arial", "sans-serif";
              position: absolute;
              width: 100%;
              height: 100%;
              top: 0;
              left: 0;
            }
            .page-template .header {
              position: absolute;
              top: 0px;
              left: 30px;
              right: 30px;
              color: #888;
              bottom:20px;
              margin-bottom: 20px;
              padding-bottom: 20px;
            }
            .page-template .footer {
              position: absolute;
              bottom: 10px;
              left: 30px;
              right: 30px;
              border-top: 1px solid #888;
              text-align: center;
              color: #888;
            }
            .k-grid-header .k-header {
                height: 20px;
                padding: 0;
              }

              .k-grid tbody tr {
                line-height: 14px !important;
              }

              .k-grid tbody td {
                padding: 0;
              }
            #wrapfrm {
                position: absolute;
                z-index: 2;
                opacity: 1;
                width: 100%;
                height: 100%;
                background-color: white;
            }

        </style>
    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="<?php echo base_url() ?>user/logout">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->

<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>js/lodash.min.js"></script>
<script src="<?php echo base_url() ?>js/angularjs-dropdown-multiselect.min.js"></script>
<script src="<?php echo base_url() ?>js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>js/kendo.all.min.js"></script>
<script src="<?php echo base_url() ?>js/moment-with-locales.js"></script>
<script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
<script src="<?php echo base_url() ?>js/dirPagination.js"></script> <!-- Pagination link -->
<script type="text/javascript">
                                                var izx = 1;
                                                var userData = <?php echo $userData; ?>;
                                                var roleData = <?php echo $roleData; ?>;
                                                var url_base_path = '<?php echo base_url() ?>';
                                                var app = angular.module("reportApp", ['angular.chosen', 'moment-picker', 'angularUtils.directives.dirPagination']);
                                                app.controller("reportCtrl", function ($scope, $http, $filter, $sce) {
                                                    $scope.auditTrailData = [];
                                                    $scope.pageno = 1; // initialize page no to 1
                                                    $scope.total_count = 0;
                                                    $scope.itemsPerPage = 5; //this could be a dynamic value from a drop down
                                                    $scope.user_name = "";
                                                    $scope.loading = false;
                                                    // $scope.start_date = moment().add(-30, 'days').format('DD-MMM-YYYY');
                                                    $scope.current_date = moment().format('DD-MMM-YYYY');
                                                    $scope.start_date = moment().format('DD-MMM-YYYY');
                                                    $scope.end_date = moment().format('DD-MMM-YYYY');
                                                    $scope.terminalnumbersList = [];
                                                    $scope.printing = false;

                                                    $scope.getUsersList = function () {
                                                        $scope.userListData = [];
                                                        $http({
                                                            url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/getUsersList',
                                                            method: "GET",
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        }).then(function (response) {
                                                            $scope.userListData = response.data.user_data;
                                                        }, function (error) { // optional
                                                            toaster.pop('error', "error", "Something went wrong.Please try again");
                                                        });
                                                    }
                                                    $scope.getUsersList();

                                                    $scope.getRoomwiseBlockList = function () {
                                                        $scope.blockData = [];
                                                        $http({
                                                            url: url_base_path + 'ponta_sahib/Mastercontroller/getRoomwiseBlockList',
                                                            method: "GET",
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        }).then(function (response) {
                                                            $scope.blockData = response.data.block_list;
                                                        }, function (error) { // optional
                                                            scope.blockData=[];
                                                            toaster.pop('error', "error", "Something went wrong.Please try again");
                                                        });
                                                    }
                                                    $scope.getRoomwiseBlockList();
                                                    //console.log($scope.blockData);
                                                    $scope.getBlockInfo = function (room_code) {
                                                        var block_code = $filter('filter')($scope.blockData, {room_code: room_code});
                                                        if(block_code.length>0){
                                                            var block = block_code[0].block_code;
                                                        }else{
                                                            var block = 'NA';
                                                        }
                                                        //[0].block_code;
                                                        return block;
                                                    }

                                                    $scope.getterminalnumbersList = function () {
                                                        $scope.terminalnumbersList = [];
                                                        $http({
                                                            url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/getterminalnumbersList',
                                                            method: "POST",
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        }).then(function (response) {
                                                            $scope.terminalnumbersList = response.data.treminaldata;
                                                            //console.log($scope.terminalnumbersList);
                                                        }, function (error) { // optional
                                                            toaster.pop('error', "error", "Something went wrong.Please try again");
                                                        });
                                                    }
                                                    $scope.getterminalnumbersList();

                                                    $scope.getterminalnumber = function (roomcode) {
                                                        var deviceid = $filter('filter')($scope.terminalnumbersList, {room_code: roomcode});
                                                        if (typeof deviceid[0] !== 'undefined') {
                                                            deviceidnew = deviceid[0].device_id;
                                                        } else {
                                                            deviceidnew = 'NA';
                                                        }
                                                        return deviceidnew;

                                                    }
                                                    $scope.rooms_array =[];
                                                    $scope.roomList = [];
                                                    $scope.geRoomList = function () {
                                                        $http({
                                                            url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/Roomlist',
                                                            method: "GET",
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        }).then(function (response) {
                                                            var rooms = response.data.room_data;
                                                            if(rooms.length>0){
                                                                for(var i =0;i<rooms.length; i++){
                                                                    var room_coded= rooms[i]['room_code'];
                                                                    $scope.rooms_array[room_coded] = rooms[i]['room_name'];
                                                                }
                                                            }
                                                        }, function (error) { // optional
                                                            $scope.roomList = [];
                                                            console.log("Something went wrong.Please try again");
                                                        });
                                                    }
                                                    $scope.base_url = '<?php echo base_url()?>';
                                                    $scope.geRoomList();
                                                    $scope.auditTrailReportDataPrint = function () {
                                                        $scope.printing = true;
                                                        var valid = true;
                                                        if (($scope.start_date == "") || ($scope.end_date == "")) {
                                                            valid = true;
                                                        }
                                                        if (valid) {
                                                            $scope.loading = true;

                                                            $http({
                                                                url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/auditTrailReportListPrint',
                                                                method: "POST",
                                                                data: "user_name=" + $scope.user_name + "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date,
                                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                            }).then(function (response) {
                                                                $scope.auditTrailData2 = [];
                                                                $scope.auditTrailData2 = response.data.row_data2;
                                                                angular.forEach($scope.auditTrailData2, function (value, key) {
                                                                    if((value.master_previous_data1=='') || (value.master_previous_data1==null) || value.master_previous_data1=='undefined'){
                                                                        value.master_previous_data = value.master_previous_data;
                                                                    }else{
                                                                        value.master_previous_data = value.master_previous_data1;
                                                                    }
                                                                    var insertedData = "";
                                                                    var updatedData = "";
                                                                    var deletedData = "";
                                                                    var insertedWorkflowData = "";
                                                                    var updatedWorkflowData = "";
                                                                    var insertedAccessData = "";
                                                                    var updatedAccessData = "";
                                                                    var uploadedFileData ="";
                                                                    var deletedFileData ="";
                                                                    var blockCode ="";
                                                                    var table_fields = JSON.parse(value.table_extra_field);
                                                                    if (table_fields!=null && typeof table_fields[0] !== 'undefined') {
                                                                        table_fields = table_fields[0];
                                                                        if (typeof table_fields[0] !== 'undefined') {
                                                                            table_fields = table_fields[0];
                                                                        }
                                                                    }
                                                                    if (value.command_type == 'insert' && value.record_type == 'log' && value.workflow_type == 'start' && value.approval_status == 'N/A') {
                                                                        delete table_fields.act_id;
                                                                        delete table_fields.done_by_user_id;
                                                                        delete table_fields.done_by_user_name;
                                                                        delete table_fields.done_by_role_id;
                                                                        delete table_fields.done_by_email;
                                                                        delete table_fields.done_by_remark;
                                                                        delete table_fields.is_in_workflow;
                                                                        delete table_fields.updated_by;
                                                                        delete table_fields.updated_on;
                                                                        delete table_fields.update_remark;
                                                                        delete table_fields.standerd_wt;
                                                                        delete table_fields.id_no_st_wt_index;
                                                                        insertedData = table_fields;
                                                                    } else if (value.command_type == 'update' && value.record_type == 'log') {
                                                                        delete table_fields.act_id;
                                                                        delete table_fields.done_by_user_id;
                                                                        delete table_fields.done_by_user_name;
                                                                        delete table_fields.done_by_role_id;
                                                                        delete table_fields.done_by_email;
                                                                        delete table_fields.done_by_remark;
                                                                        delete table_fields.is_in_workflow;
                                                                        delete table_fields.updated_by;
                                                                        delete table_fields.updated_on;
                                                                        delete table_fields.update_remark;
                                                                        if (value.previous_data != "" && value.previous_data != null && value.previous_data != undefined) {
                                                                            var previous_data_fields = JSON.parse(value.previous_data);
                                                                            delete previous_data_fields.act_id;
                                                                            delete previous_data_fields.done_by_user_id;
                                                                            delete previous_data_fields.done_by_user_name;
                                                                            delete previous_data_fields.done_by_role_id;
                                                                            delete previous_data_fields.done_by_email;
                                                                            delete previous_data_fields.done_by_remark;
                                                                            delete previous_data_fields.is_in_workflow;
                                                                            delete previous_data_fields.updated_by;
                                                                            delete previous_data_fields.updated_on;
                                                                            delete previous_data_fields.update_remark;
                                                                            var differing = getDifference(table_fields, previous_data_fields);
                                                                            if (differing == "" || differing == undefined) {
                                                                                updatedData = "{Nothing update}";
                                                                            } else {
                                                                                updatedData = differing;
                                                                            }
                                                                        } else {
                                                                            updatedData = table_fields;
                                                                        }

                                                                    } else if (value.command_type == 'delete' && value.record_type == 'master') {
                                                                        deletedData = table_fields;
                                                                    } else if (value.command_type == 'insert' && value.record_type == 'master' && value.table_name != "pts_mst_user_mgmt") {
                                                                        if (table_fields.hasOwnProperty('block_code')) {
                                                                            blockCode = table_fields.block_code;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('created_date')) {
                                                                            delete table_fields.created_date;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('created_on')) {
                                                                            delete table_fields.created_on;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('created_by')) {
                                                                            delete table_fields.created_by;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('modified_on')) {
                                                                            delete table_fields.modified_on;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('modified_by')) {
                                                                            delete table_fields.modified_by;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('emp_password')) {
                                                                            delete table_fields.emp_password;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('is_active')) {
                                                                            table_fields.is_active = (table_fields.is_active == 1) ? 'Active' : 'Inactive';
                                                                        }
                                                                        insertedData = table_fields;
                                                                    } else if (value.command_type == 'update' && value.record_type == 'master') {
                                                                        if (table_fields.hasOwnProperty('block_code')) {
                                                                            blockCode = table_fields.block_code;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('created_date')) {
                                                                            delete table_fields.created_date;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('created_on')) {
                                                                            delete table_fields.created_on;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('created_by')) {
                                                                            delete table_fields.created_by;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('modified_on')) {
                                                                            delete table_fields.modified_on;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('modified_by')) {
                                                                            delete table_fields.modified_by;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('role_level')) {
                                                                            delete table_fields.role_level;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('role_code')) {
                                                                            delete table_fields.role_code;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('header_id')) {
                                                                            delete table_fields.header_id;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('retired_date')) {
                                                                            if(table_fields.retired_date==""){
                                                                                delete table_fields.retired_date;
                                                                            }
                                                                        }
                                                                        if (table_fields.hasOwnProperty('is_active')) {
                                                                            table_fields.is_active = (table_fields.is_active == 1) ? 'Active' : 'Inactive';
                                                                        }

                                                                        if (value.master_previous_data != "" && value.master_previous_data != null && value.master_previous_data != 'null' &&  value.master_previous_data != undefined) {
                                                                            var master_previous_data_fields = JSON.parse(value.master_previous_data);
                                                                            //console.log(master_previous_data_fields+"master");
                                                                            if (master_previous_data_fields.hasOwnProperty('created_date')) {
                                                                                delete master_previous_data_fields.created_date;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('emp_email')) {
                                                                                delete master_previous_data_fields.emp_email;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('user_id')) {
                                                                                delete master_previous_data_fields.user_id;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('created_on')) {
                                                                                delete master_previous_data_fields.created_on;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('created_by')) {
                                                                                delete master_previous_data_fields.created_by;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('modified_on')) {
                                                                                delete master_previous_data_fields.modified_on;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('modified_by')) {
                                                                                delete master_previous_data_fields.modified_by;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('instrument_code')) {
                                                                                delete master_previous_data_fields.instrument_code;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('remarks')) {
                                                                                delete master_previous_data_fields.remarks;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('remark')) {
                                                                                delete master_previous_data_fields.remark;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('retired_date')) {
                                                                                if(master_previous_data_fields.retired_date==""){
                                                                                    delete master_previous_data_fields.retired_date;
                                                                                }
                                                                            }

                                                                            if (master_previous_data_fields.hasOwnProperty('header_name')) {
                                                                                delete master_previous_data_fields.header_name;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('role_description')) {
                                                                                delete master_previous_data_fields.role_description;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('id_no_statndard_weight')) {
                                                                                delete master_previous_data_fields.id_no_statndard_weight ;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('is_active')) {
                                                                                master_previous_data_fields.is_active = (master_previous_data_fields.is_active == 1) ? 'Active' : 'Inactive';
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('balance_no')) {
                                                                                delete master_previous_data_fields.balance_no ;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('records_updated')) {
                                                                                delete master_previous_data_fields.records_updated ;
                                                                            }
                                                                            var differing = getDifference(table_fields, master_previous_data_fields);
                                                                            if (differing == "" || differing == undefined) {
                                                                                updatedData = "{Nothing update}";
                                                                            } else {
                                                                                updatedData = differing;
                                                                            }
                                                                        } else {
                                                                            if(value.table_name=='pts_trn_room_log_activity' || value.table_name=='pts_mst_balance_frequency_data'){
                                                                                if(table_fields.hasOwnProperty('room_code')){
                                                                                    $scope.auditTrailData2[key]['room_code'] = table_fields.room_code;
                                                                                    if(($scope.rooms_array[table_fields.room_code])!='undefined'){
                                                                                        $scope.auditTrailData2[key]['room_name'] = $scope.rooms_array[table_fields.room_code];
                                                                                    }
                                                                                }
                                                                                table_fields = getDifference(table_fields, {});
                                                                            }
                                                                            updatedData = table_fields;
                                                                        }
                                                                    }
                                                                    if (updatedData != '') {
                                                                        updatedData = JSON.stringify(updatedData).replace(/["]/g, '');
                                                                    }
                                                                    if (insertedData != '') {
                                                                        var finalinsertedData = "";
                                                                        if(typeof insertedData!='string' && typeof insertedData!='undefined' && typeof insertedData!=null){
                                                                            $.each(insertedData, function (key, value) {
                                                                                if(value!=null){
                                                                                    if (typeof value === 'object') {
                                                                                        $.each(value, function (keynew, valuenew) {
                                                                                            if (valuenew != 'undefined') {
                                                                                                finalinsertedData += "<b>" + keynew + " :</b>" + valuenew + ", ";
                                                                                            }
                                                                                        });
                                                                                    } else {
                                                                                        if (value != 'undefined') {
                                                                                            finalinsertedData += "<b>" + key + " :</b>" + value + ", ";
                                                                                        }
                                                                                    }
                                                                                }
                                                                            });
                                                                        }
                                                                        insertedData = JSON.stringify(finalinsertedData).replace(/["]/g, '');
                                                                    }
                                                                    if (deletedData != '') {
                                                                        deletedData = JSON.stringify(deletedData).replace(/[{}]/g, '');
                                                                    }

                                                                    if (value.table_name == "mst_room" || value.table_name == "pts_mst_sys_id") {
                                                                        $scope.auditTrailData2[key]['room_code'] = table_fields.room_code;
                                                                        $scope.auditTrailData2[key]['room_name'] = table_fields.room_name;
                                                                    }
                                                                    if (value.table_name == "pts_trn_workflowsteps" && value.command_type == 'insert' && (value.record_type != "master") && (value.approval_status != 'file deleted' || value.approval_status != 'file uploaded')) {
                                                                        insertedWorkflowData =  $sce.trustAsHtml(table_fields.activity);
                                                                        insertedData = "";
                                                                    }
                                                                    if (value.table_name == "pts_trn_workflowsteps" && value.command_type == 'update' && (value.record_type != "master") && (value.approval_status != 'file deleted' && value.approval_status != 'file uploaded')) {
                                                                        updatedWorkflowData = $sce.trustAsHtml(table_fields.activity);
                                                                        updatedData = '';
                                                                    }
                                                                    if (value.approval_status == 'file uploaded') {
                                                                        uploadedFileData = $sce.trustAsHtml("File has been uploaded <a href='"+ $scope.base_url+ value.master_previous_data+"' target='_blank'>"+value.previous_data+"</a>");
                                                                    }
                                                                    if (value.approval_status == 'file deleted') {
                                                                        //deletedFileData = $sce.trustAsHtml("File has been deleted <a href='"+ $scope.base_url+ value.master_previous_data+"' target='_blank'>"+value.previous_data+"</a>");
                                                                        deletedFileData = $sce.trustAsHtml("File has been deleted "+value.previous_data);
                                                                    }
                                                                    if ((value.logname == "Room Out" || value.logname=="Room In") && (value.command_type == 0)) {
                                                                        if(table_fields!=0){
                                                                            insertedData = "Remarks : " + table_fields['remark'];
                                                                        }else{
                                                                            insertedData = "";
                                                                        }
                                                                    }
                                                                    if (value.logname == "User Login"  || value.logname == "User Logout") {
                                                                        if(value.table_extra_field!=0 && value.table_extra_field!=null){
                                                                            var x  = JSON.parse(value.table_extra_field);
                                                                            insertedData = x.reason;
                                                                        }else{
                                                                            insertedData = "";
                                                                        }
                                                                    }
                                                                    if (value.table_name == "pts_mst_user_mgmt") {
                                                                        if (typeof table_fields.role_id != 'undefined') {
                                                                            if (roleData.length > 0) {
                                                                                var role_name = $filter('filter')(roleData, function (d) {
                                                                                    return d.id === table_fields.role_id;
                                                                                });
                                                                                if (typeof role_name[0] !== 'undefined') {
                                                                                    role_name = role_name[0].role_description;
                                                                                } else {
                                                                                    role_name = '';
                                                                                }
                                                                            } else {
                                                                                var role_name = '';

                                                                            }
                                                                            var role_named = typeof table_fields.Roles_Assigned != 'undefined'?table_fields.Roles_Assigned:'';
                                                                            if(value.command_type == 'insert'){
                                                                                insertedAccessData = $sce.trustAsHtml("Access of " + $filter('capitalize')(table_fields.module_type) + "<p> Assigned to Role: " + role_name + "</p>");
                                                                            }else{
                                                                                updatedAccessData = $sce.trustAsHtml("Access of " +table_fields.module_type + "<p> Assigned to Role: " + role_name  + " Rights Modified. Role : " + role_name + ' <br>'+ role_named+ "</p>");
                                                                            }
                                                                            insertedData = "";
                                                                            updatedData = '';
                                                                        }
                                                                        else if (typeof table_fields.user_id != 'undefined') {
                                                                            if (userData.length > 0) {
                                                                                var user_name = $filter('filter')(userData, function (d) {
                                                                                    return d.id === table_fields.user_id;
                                                                                });
                                                                                if (typeof user_name[0] !== 'undefined') {
                                                                                    user_name = user_name[0].emp_email;
                                                                                } else {
                                                                                    user_name = 'Unknown';
                                                                                }
                                                                            } else {
                                                                                var user_name = table_fields.user_id;

                                                                            }
                                                                            var logs = '';
                                                                            if (typeof table_fields.logs != 'undefined') {
                                                                                logs = table_fields.logs;
                                                                            }
                                                                            var rooms_access = '';
                                                                            if (typeof table_fields.room_code != 'undefined') {
                                                                                rooms_access = "room_code:"+ table_fields.room_code+'<br>';
                                                                            }
                                                                            var access_status = '';
                                                                            if (typeof table_fields.status != 'undefined') {
                                                                                access_status = "<br>Status:"+ table_fields.status;
                                                                            }
                                                                            //insertedAccessData = $sce.trustAsHtml("Access of " + $filter('capitalize')(table_fields.module_type) + "<p> Assigned to User ID : " + user_name+ ' '+ logs+ "</p>");
                                                                            //updatedAccessData = "Rights Modified. User Id : " + value.user_name+ ' '+ logs;
                                                                            if(value.command_type == 'insert'){
                                                                                insertedAccessData = $sce.trustAsHtml("Rights of " + $filter('capitalize')(table_fields.module_type) + "<p> Assigned to User ID: " + user_name +  '<br>'+ rooms_access+ logs +access_status+"</p>");
                                                                            }else{
                                                                                updatedAccessData = $sce.trustAsHtml("Rights of " +table_fields.module_type + "<p> Updated For User ID: " + user_name  + '<br>'+rooms_access + logs +access_status+"</p>");
                                                                            }
                                                                            insertedData = "";
                                                                            updatedData = '';
                                                                        }
                                                                    }

//                                                    console.log(JSON.parse(value.table_extra_field));s
                                                                    if(table_fields.hasOwnProperty('room_code')){
                                                                        $scope.auditTrailData2[key]['room_code'] = table_fields.room_code;
                                                                        if($scope.rooms_array[table_fields.room_code]!='undefined'){
                                                                            $scope.auditTrailData2[key]['room_name'] = $scope.rooms_array[table_fields.room_code];
                                                                        }
                                                                    }
                                                                    $scope.auditTrailData2[key]['blockCode'] = blockCode;
                                                                    $scope.auditTrailData2[key]['insertedData'] = $sce.trustAsHtml(insertedData);
                                                                    $scope.auditTrailData2[key]['updatedData'] = $sce.trustAsHtml(updatedData);
                                                                    $scope.auditTrailData2[key]['insertedWorkflowData'] = insertedWorkflowData;
                                                                    $scope.auditTrailData2[key]['updatedWorkflowData'] = updatedWorkflowData;
                                                                    $scope.auditTrailData2[key]['insertedAccessData'] = insertedAccessData;
                                                                    $scope.auditTrailData2[key]['updatedAccessData'] = updatedAccessData;
                                                                    $scope.auditTrailData2[key]['deletedData'] = deletedData;
                                                                    $scope.auditTrailData2[key]['deletedFileData'] = deletedFileData;
                                                                    $scope.auditTrailData2[key]['uploadedFileData'] = uploadedFileData;
                                                                });
                                                                $scope.total_count = response.data.total_count;
                                                            })
                                                                    .finally(function () {

                                                                    });
                                                        } else {
                                                            alert("Please Select Room No/Start Date/End Date");
                                                        }
                                                    }
                                                    $scope.auditTrailReportDataPrint();
                                                    $scope.auditTrailReportData = function (pageno) {
                                                        var valid = true;
                                                        if (($scope.start_date == "") || ($scope.end_date == "")) {
                                                            valid = true;
                                                        }

                                                        if (valid) {
                                                            $scope.loading = true;
                                                            $http({
                                                                url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/auditTrailReportList?page_limit=' + $scope.itemsPerPage + "&page_no=" + pageno,
                                                                method: "POST",
                                                                data: "user_name=" + $scope.user_name + "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date,
                                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                            }).then(function (response) {
                                                                $scope.auditTrailData = [];
                                                                $scope.auditTrailData = response.data.row_data;
                                                                angular.forEach($scope.auditTrailData, function (value, key) {
                                                                    if((value.master_previous_data1=='') || (value.master_previous_data1==null) || value.master_previous_data1=='undefined'){
                                                                        value.master_previous_data = value.master_previous_data;
                                                                    }else{
                                                                        value.master_previous_data = value.master_previous_data1;
                                                                    }
                                                                    var insertedData = "";
                                                                    var updatedData = "";
                                                                    var deletedData = "";
                                                                    var insertedWorkflowData = "";
                                                                    var updatedWorkflowData = "";
                                                                    var insertedAccessData = "";
                                                                    var updatedAccessData = "";
                                                                    var uploadedFileData ="";
                                                                    var deletedFileData ="";
                                                                    var blockCode ="";
                                                                    var table_fields = JSON.parse(value.table_extra_field);
                                                                    if (table_fields!=null && typeof table_fields[0] !== 'undefined') {
                                                                        table_fields = table_fields[0];
                                                                        if (typeof table_fields[0] !== 'undefined') {
                                                                            table_fields = table_fields[0];
                                                                        }
                                                                    }
                                                                    if (value.command_type == 'insert' && value.record_type == 'log' && value.workflow_type == 'start' && value.approval_status == 'N/A') {
                                                                        delete table_fields.act_id;
                                                                        delete table_fields.done_by_user_id;
                                                                        delete table_fields.done_by_user_name;
                                                                        delete table_fields.done_by_role_id;
                                                                        delete table_fields.done_by_email;
                                                                        delete table_fields.done_by_remark;
                                                                        delete table_fields.is_in_workflow;
                                                                        delete table_fields.updated_by;
                                                                        delete table_fields.standerd_wt;
                                                                        delete table_fields.id_no_st_wt_index;
                                                                        delete table_fields.updated_on;
                                                                        delete table_fields.update_remark;
                                                                        insertedData = table_fields;
                                                                    } else if (value.command_type == 'update' && value.record_type == 'log') {
                                                                        delete table_fields.act_id;
                                                                        delete table_fields.done_by_user_id;
                                                                        delete table_fields.done_by_user_name;
                                                                        delete table_fields.done_by_role_id;
                                                                        delete table_fields.done_by_email;
                                                                        delete table_fields.done_by_remark;
                                                                        delete table_fields.is_in_workflow;
                                                                        delete table_fields.updated_by;
                                                                        delete table_fields.updated_on;
                                                                        delete table_fields.update_remark;
                                                                        if (value.previous_data != "" && value.previous_data != null && value.previous_data != undefined) {
                                                                            var previous_data_fields = JSON.parse(value.previous_data);
                                                                            delete previous_data_fields.act_id;
                                                                            delete previous_data_fields.done_by_user_id;
                                                                            delete previous_data_fields.done_by_user_name;
                                                                            delete previous_data_fields.done_by_role_id;
                                                                            delete previous_data_fields.done_by_email;
                                                                            delete previous_data_fields.done_by_remark;
                                                                            delete previous_data_fields.is_in_workflow;
                                                                            delete previous_data_fields.updated_by;
                                                                            delete previous_data_fields.updated_on;
                                                                            delete previous_data_fields.update_remark;
                                                                            var differing = getDifference(table_fields, previous_data_fields);
                                                                            if (differing == "" || differing == undefined) {
                                                                                updatedData = "{Nothing update}";
                                                                            } else {
                                                                                updatedData = differing;
                                                                            }
                                                                        } else {
                                                                            updatedData = table_fields;
                                                                        }

                                                                    } else if (value.command_type == 'delete' && value.record_type == 'master') {
                                                                        deletedData = table_fields;
                                                                    } else if (value.command_type == 'insert' && value.record_type == 'master' && value.table_name != "pts_mst_user_mgmt") {
                                                                        if (table_fields.hasOwnProperty('block_code')) {
                                                                            blockCode = table_fields.block_code;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('created_date')) {
                                                                            delete table_fields.created_date;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('created_on')) {
                                                                            delete table_fields.created_on;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('created_by')) {
                                                                            delete table_fields.created_by;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('modified_on')) {
                                                                            delete table_fields.modified_on;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('modified_by')) {
                                                                            delete table_fields.modified_by;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('emp_password')) {
                                                                            delete table_fields.emp_password;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('is_active')) {
                                                                            table_fields.is_active = (table_fields.is_active == 1) ? 'Active' : 'Inactive';
                                                                        }
                                                                        insertedData = table_fields;
                                                                    } else if (value.command_type == 'update' && value.record_type == 'master') {
                                                                        if (table_fields.hasOwnProperty('block_code')) {
                                                                            blockCode = table_fields.block_code;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('created_date')) {
                                                                            delete table_fields.created_date;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('created_on')) {
                                                                            delete table_fields.created_on;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('created_by')) {
                                                                            delete table_fields.created_by;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('modified_on')) {
                                                                            delete table_fields.modified_on;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('modified_by')) {
                                                                            delete table_fields.modified_by;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('role_level')) {
                                                                            delete table_fields.role_level;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('role_code')) {
                                                                            delete table_fields.role_code;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('header_id')) {
                                                                            delete table_fields.header_id;
                                                                        }
                                                                        if (table_fields.hasOwnProperty('retired_date')) {
                                                                            if(table_fields.retired_date==""){
                                                                                delete table_fields.retired_date;
                                                                            }
                                                                        }
                                                                        if (table_fields.hasOwnProperty('is_active')) {
                                                                            table_fields.is_active = (table_fields.is_active == 1) ? 'Active' : 'Inactive';
                                                                        }

                                                                        if (value.master_previous_data != "" && value.master_previous_data != null && value.master_previous_data != 'null' &&  value.master_previous_data != undefined) {
                                                                            var master_previous_data_fields = JSON.parse(value.master_previous_data);
                                                                            //console.log(master_previous_data_fields+"master");
                                                                            if (master_previous_data_fields.hasOwnProperty('created_date')) {
                                                                                delete master_previous_data_fields.created_date;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('emp_email')) {
                                                                                delete master_previous_data_fields.emp_email;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('user_id')) {
                                                                                delete master_previous_data_fields.user_id;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('created_on')) {
                                                                                delete master_previous_data_fields.created_on;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('created_by')) {
                                                                                delete master_previous_data_fields.created_by;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('modified_on')) {
                                                                                delete master_previous_data_fields.modified_on;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('modified_by')) {
                                                                                delete master_previous_data_fields.modified_by;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('instrument_code')) {
                                                                                delete master_previous_data_fields.instrument_code;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('remarks')) {
                                                                                delete master_previous_data_fields.remarks;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('remark')) {
                                                                                delete master_previous_data_fields.remark;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('retired_date')) {
                                                                                if(master_previous_data_fields.retired_date==""){
                                                                                    delete master_previous_data_fields.retired_date;
                                                                                }
                                                                            }

                                                                            if (master_previous_data_fields.hasOwnProperty('header_name')) {
                                                                                delete master_previous_data_fields.header_name;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('role_description')) {
                                                                                delete master_previous_data_fields.role_description;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('id_no_statndard_weight')) {
                                                                                delete master_previous_data_fields.id_no_statndard_weight ;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('is_active')) {
                                                                                master_previous_data_fields.is_active = (master_previous_data_fields.is_active == 1) ? 'Active' : 'Inactive';
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('balance_no')) {
                                                                                delete master_previous_data_fields.balance_no ;
                                                                            }
                                                                            if (master_previous_data_fields.hasOwnProperty('records_updated')) {
                                                                                delete master_previous_data_fields.records_updated ;
                                                                            }
                                                                            var differing = getDifference(table_fields, master_previous_data_fields);
                                                                            if (differing == "" || differing == undefined) {
                                                                                updatedData = "{Nothing update}";
                                                                            } else {
                                                                                updatedData = differing;
                                                                            }
                                                                        } else {
                                                                            if(value.table_name=='pts_trn_room_log_activity' || value.table_name=='pts_mst_balance_frequency_data'){
                                                                                if(table_fields.hasOwnProperty('room_code')){
                                                                                    $scope.auditTrailData[key]['room_code'] = table_fields.room_code;
                                                                                    if(($scope.rooms_array[table_fields.room_code])!='undefined'){
                                                                                        $scope.auditTrailData[key]['room_name'] = $scope.rooms_array[table_fields.room_code];
                                                                                    }
                                                                                }
                                                                                table_fields = getDifference(table_fields, {});
                                                                            }
                                                                            updatedData = table_fields;
                                                                        }
                                                                    }
                                                                    if (updatedData != '') {
                                                                        updatedData = JSON.stringify(updatedData).replace(/["]/g, '');
                                                                    }
                                                                    if (insertedData != '') {
                                                                        var finalinsertedData = "";
                                                                        if(typeof insertedData!='string' && typeof insertedData!='undefined' && typeof insertedData!=null){
                                                                            $.each(insertedData, function (key, value) {
                                                                                if(value!=null){
                                                                                    if (typeof value === 'object') {
                                                                                        $.each(value, function (keynew, valuenew) {
                                                                                            if (valuenew != 'undefined') {
                                                                                                finalinsertedData += "<b>" + keynew + " :</b>" + valuenew + ", ";
                                                                                            }
                                                                                        });
                                                                                    } else {
                                                                                        if (value != 'undefined') {
                                                                                            finalinsertedData += "<b>" + key + " :</b>" + value + ", ";
                                                                                        }
                                                                                    }
                                                                                }
                                                                            });
                                                                        }
                                                                        insertedData = JSON.stringify(finalinsertedData).replace(/["]/g, '');
                                                                    }
                                                                    if (deletedData != '') {
                                                                        deletedData = JSON.stringify(deletedData).replace(/[{}]/g, '');
                                                                    }

                                                                    if (value.table_name == "mst_room" || value.table_name == "pts_mst_sys_id") {
                                                                        $scope.auditTrailData[key]['room_code'] = table_fields.room_code;
                                                                        $scope.auditTrailData[key]['room_name'] = table_fields.room_name;
                                                                    }
                                                                    if (value.table_name == "pts_trn_workflowsteps" && value.command_type == 'insert' && (value.record_type != "master") && (value.approval_status != 'file deleted' || value.approval_status != 'file uploaded')) {
                                                                        insertedWorkflowData =  $sce.trustAsHtml(table_fields.activity);
                                                                        insertedData = "";
                                                                    }
                                                                    if (value.table_name == "pts_trn_workflowsteps" && value.command_type == 'update' && (value.record_type != "master") && (value.approval_status != 'file deleted' && value.approval_status != 'file uploaded')) {
                                                                        updatedWorkflowData = $sce.trustAsHtml(table_fields.activity);
                                                                        updatedData = '';
                                                                    }
                                                                    if (value.approval_status == 'file uploaded') {
                                                                        uploadedFileData = $sce.trustAsHtml("File has been uploaded <a href='"+ $scope.base_url+ value.master_previous_data+"' target='_blank'>"+value.previous_data+"</a>");
                                                                    }
                                                                    if (value.approval_status == 'file deleted') {
                                                                        //deletedFileData = $sce.trustAsHtml("File has been deleted <a href='"+ $scope.base_url+ value.master_previous_data+"' target='_blank'>"+value.previous_data+"</a>");
                                                                        deletedFileData = $sce.trustAsHtml("File has been deleted "+value.previous_data);
                                                                    }
                                                                    if ((value.logname == "Room Out" || value.logname=="Room In") && (value.command_type == 0)) {
                                                                        if(table_fields!=0){
                                                                            insertedData = "Remarks : " + table_fields['remark'];
                                                                        }else{
                                                                            insertedData = "";
                                                                        }
                                                                    }
                                                                    if (value.logname == "User Login"  || value.logname == "User Logout") {
                                                                        if(value.table_extra_field!=0 && value.table_extra_field!=null){
                                                                            var x  = JSON.parse(value.table_extra_field);
                                                                            insertedData = x.reason;
                                                                        }else{
                                                                            insertedData = "";
                                                                        }
                                                                    }
                                                                    if (value.table_name == "pts_mst_user_mgmt") {
                                                                        if (typeof table_fields.role_id != 'undefined') {
                                                                            if (roleData.length > 0) {
                                                                                var role_name = $filter('filter')(roleData, function (d) {
                                                                                    return d.id === table_fields.role_id;
                                                                                });
                                                                                if (typeof role_name[0] !== 'undefined') {
                                                                                    role_name = role_name[0].role_description;
                                                                                } else {
                                                                                    role_name = '';
                                                                                }
                                                                            } else {
                                                                                var role_name = '';

                                                                            }
                                                                            var role_named = typeof table_fields.Roles_Assigned != 'undefined'?table_fields.Roles_Assigned:'';
                                                                            if(value.command_type == 'insert'){
                                                                                insertedAccessData = $sce.trustAsHtml("Access of " + $filter('capitalize')(table_fields.module_type) + "<p> Assigned to Role: " + role_name + '<br> '+role_named+"</p>");
                                                                            }else{
                                                                                updatedAccessData = $sce.trustAsHtml("Access of " +table_fields.module_type + "<p> Assigned to Role: " + role_name  + " Rights Modified. Role : " + role_name + '<br> '+ role_named+ "</p>");
                                                                            }
                                                                            insertedData = "";
                                                                            updatedData = '';
                                                                        }
                                                                        else if (typeof table_fields.user_id != 'undefined') {
                                                                            if (userData.length > 0) {
                                                                                var user_name = $filter('filter')(userData, function (d) {
                                                                                    return d.id === table_fields.user_id;
                                                                                });
                                                                                if (typeof user_name[0] !== 'undefined') {
                                                                                    user_name = user_name[0].emp_email;
                                                                                } else {
                                                                                    user_name = 'Unknown';
                                                                                }
                                                                            } else {
                                                                                var user_name = table_fields.user_id;

                                                                            }
                                                                            var logs = '';
                                                                            if (typeof table_fields.logs != 'undefined') {
                                                                                logs = table_fields.logs;
                                                                            }
                                                                            var rooms_access = '';
                                                                            if (typeof table_fields.room_code != 'undefined') {
                                                                                rooms_access = "room_code:"+ table_fields.room_code+'<br>';
                                                                            }
                                                                            var access_status = '';
                                                                            if (typeof table_fields.status != 'undefined') {
                                                                                access_status = "<br>Status:"+ table_fields.status;
                                                                            }
                                                                            //insertedAccessData = $sce.trustAsHtml("Access of " + $filter('capitalize')(table_fields.module_type) + "<p> Assigned to User ID : " + user_name+ ' '+ logs+ "</p>");
                                                                            //updatedAccessData = "Rights Modified. User Id : " + value.user_name+ ' '+ logs;
                                                                            if(value.command_type == 'insert'){
                                                                                insertedAccessData = $sce.trustAsHtml("Rights of " + $filter('capitalize')(table_fields.module_type) + "<p> Assigned to User ID: " + user_name +  '<br>'+ rooms_access+ logs +access_status+"</p>");
                                                                            }else{
                                                                                updatedAccessData = $sce.trustAsHtml("Rights of " +table_fields.module_type + "<p> Updated For User ID: " + user_name  + '<br>'+rooms_access + logs +access_status+"</p>");
                                                                            }
                                                                            insertedData = "";
                                                                            updatedData = '';
                                                                        }
                                                                    }

//                                                    console.log(JSON.parse(value.table_extra_field));s
                                                                    if(table_fields.hasOwnProperty('room_code')){
                                                                        $scope.auditTrailData[key]['room_code'] = table_fields.room_code;
                                                                        if($scope.rooms_array[table_fields.room_code]!='undefined'){
                                                                            $scope.auditTrailData[key]['room_name'] = $scope.rooms_array[table_fields.room_code];
                                                                        }
                                                                    }
                                                                    $scope.auditTrailData[key]['blockCode'] = blockCode;
                                                                    $scope.auditTrailData[key]['insertedData'] = $sce.trustAsHtml(insertedData);
                                                                    $scope.auditTrailData[key]['updatedData'] = $sce.trustAsHtml(updatedData);
                                                                    $scope.auditTrailData[key]['insertedWorkflowData'] = insertedWorkflowData;
                                                                    $scope.auditTrailData[key]['updatedWorkflowData'] = updatedWorkflowData;
                                                                    $scope.auditTrailData[key]['insertedAccessData'] = insertedAccessData;
                                                                    $scope.auditTrailData[key]['updatedAccessData'] = updatedAccessData;
                                                                    $scope.auditTrailData[key]['deletedData'] = deletedData;
                                                                    $scope.auditTrailData[key]['deletedFileData'] = deletedFileData;
                                                                    $scope.auditTrailData[key]['uploadedFileData'] = uploadedFileData;
                                                                });

                                                                $scope.loading = false;
                                                                $scope.total_count = response.data.total_count;
                                                            }, function (error) { // optional
                                                                toaster.pop('error', "error", "Something went wrong.Please try again");
                                                            });
                                                        } else {
                                                            alert("Please Select Room No/Start Date/End Date");
                                                        }
                                                    }
                                                    $scope.auditTrailReportData($scope.pageno);
                                                    $scope.resetReportData = function (pageno) {
                                                        $scope.pageno = pageno; // initialize page no to 1
                                                        $scope.auditTrailReportData($scope.pageno);
                                                        $scope.auditTrailReportDataPrint();
                                                    }



                                                });
                                                app.filter('removeUnderscores', [function () {
                                                        return function (string) {
                                                            if (!angular.isString(string)) {
                                                                return string;
                                                            }
                                                            return string.replace(/[/_/]/g, ' ');
                                                        };
                                                    }])
                                                app.filter('capitalize', function () {
                                                    return function (input) {
                                                        return (angular.isString(input) && input.length > 0) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : input;
                                                    }
                                                });
                                                // add a custom filter to your module
                                                app.filter('UserFilter', function () {
                                                    // the filter takes an additional input filterIDs
                                                    return function (inputArray, filterIDs) {
                                                        // filter your original array to return only the objects that
                                                        // have their ID in the filterIDs array
                                                        var data = inputArray.filter(function (entry) {
                                                            return this.indexOf(entry.id) !== -1;
                                                        }, filterIDs);
                                                        return data != "" ? data[0].emp_name : "--"; // filterIDs here is what "this" is referencing in the line above
                                                    };
                                                });
                                                function ExportPdf() {
                                                    $("#1").show();
                                                    kendo.drawing
                                                            .drawDOM("#audit_trail2",
                                                                    {
                                                                        paperSize: "A3",
                                                                        landscape: true,
                                                                        pageable: true,
                                                                        repeatHeaders:true,
                                                                        template: $("#page-template").html(),
                                                                        margin: {top: "10px", bottom: "10px"},
                                                                        //padding: {top: "40px", bottom: "40px"},
                                                                        scale: 0.5,
                                                                        height: 500
                                                                    })
                                                            .then(function (group) {
                                                                kendo.drawing.pdf.saveAs(group, "audit_trail.pdf")
                                                            }).always(function () {
                                                        $("#pdf_download").hide();
                                                        $("#1").hide();
                                                    });
                                                }
                                                app.filter('format', function () {
                                                    return function (item) {
                                                        var t = item.split(/[- :]/);
                                                        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                                        var time = d.getTime();
                                                        return time;
                                                    };
                                                });
                                                function getDifference(o1, o2) {
                                                    var diff = {};
                                                    var tmp = null;

                                                    if (typeof o2 !== 'undefined') {
                                                        if (JSON.stringify(o1) === JSON.stringify(o2))
                                                            return;
                                                        for (var k in o1) {
                                                            if (Array.isArray(o1[k]) && Array.isArray(o2[k])) {
                                                                tmp = o1[k].reduce(function (p, c, i) {
                                                                    var _t = getDifference(c, o2[k][i]);
                                                                    if (_t)
                                                                        p.push(_t);
                                                                    return p;
                                                                }, []);
                                                                if (Object.keys(tmp).length > 0)
                                                                    diff[k] = tmp;
                                                            } else if (typeof (o1[k]) === "object" && typeof (o2[k]) === "object") {
                                                                tmp = getDifference(o1[k], o2[k]);
                                                                if (tmp && Object.keys(tmp) > 0)
                                                                    diff[k] = tmp;
                                                            } else if (o1[k] != o2[k]) {
                                                                if (typeof o2[k] === 'undefined') {
                                                                    diff[k] = o1[k]
                                                                } else {
                                                                    diff[k] = o2[k] + " (Old) to " + o1[k] + " (New)"
                                                                    //diff[k] = o2[k] + " to " + o1[k] 
                                                                }
                                                            }
                                                        }
                                                        var finalS = "";
                                                        $.each(diff, function (key, value) {
                                                            if (typeof value !== 'undefined') {
                                                                finalS += "<b>" + key + " :</b> " + value + "<br>";
                                                            }
                                                        });
                                                        return finalS;
                                                    } else {
                                                        return;
                                                    }
                                                }
</script>
<script src="<?php echo base_url(); ?>assets/js/disable_editor.js"></script>
<script src='<?php echo base_url() ?>dist/jspdf.min.js'></script>

<script>
                                                var base64Img = null;
                                                imgToBase64('motherson-new-logo.png', function (base64) {
                                                    base64Img = base64;
                                                });

                                                margins = {
                                                    top: 70,
                                                    bottom: 40,
                                                    left: 30,
                                                    width: 550
                                                };

                                                generate = function ()
                                                {

                                                    //var doc = new jsPDF('p', 'pt', 'a4');
                                                    //doc.setFontSize(18);
                                                    //doc.text(60, 20, '');
                                                    //doc.text(20, 30, '');

                                                    // Add new page
                                                    //doc.addPage();
                                                    //doc.text(20, 20, '');
                                                    //headerFooterFormatting(doc, doc.internal.getNumberOfPages());

                                                    //Save the PDF
                                                    //doc.save('document.pdf');
///////////////////////////////////////////////////
                                                    // $("#audit_trail2").show();
                                                    // var pdf = new jsPDF('p','pt','a4');
                                                    // pdf.addHTML($("#audit_trail2"),function() {
                                                    //     headerFooterFormatting(pdf, pdfs.internal.getNumberOfPages());        
                                                    //     pdf.save('web.pdf');
                                                    // });
                                                    // $("#audit_trail2").hide();
///////////////////////////////////////////////////    
                                                    var doc = new jsPDF('p', 'pt', 'a4');
                                                    doc.setFontSize(13);
                                                    var table = document.querySelector('#audit_trail2')
                                                    //var elementHTML = $("#tbldata").html();
                                                    var specialElementHandlers = {
                                                        '#Element': function (element, renderer)
                                                        {
                                                            return true;
                                                        }
                                                    };
                                                    doc.fromHTML(table, 15, 35, {
                                                        'width': 1200,
                                                        'elementHandlers': specialElementHandlers
                                                    });

                                                    //doc.fromHTML(table,15,15)
                                                    headerFooterFormatting(doc, doc.internal.getNumberOfPages());
                                                    doc.save('document.pdf');



//             var table1 = 
//         tableToJson($('#tbldata').get(0)),
//         cellWidth = 35,
//         rowCount = 0,
//         cellContents,
//         leftMargin = 2,
//         topMargin = 12,
//         topMarginTable = 55,
//         headerRowHeight = 13,
//         rowHeight = 9,

//          l = {
//          orientation: 'l',
//          unit: 'mm',
//          format: 'a3',
//          compress: true,
//          fontSize: 8,
//          lineHeight: 1,
//          autoSize: false,
//          printHeaders: true
//      };

//     var doc = new jsPDF(l, '', '', '');

//     doc.setProperties({
//         title: 'Test PDF Document',
//         subject: 'This is the subject',
//         author: 'author',
//         keywords: 'generated, javascript, web 2.0, ajax',
//         creator: 'author'
//     });

//     doc.cellInitialize();

//    $.each(table1, function (i, row)
//     {

//         rowCount++;

//         $.each(row, function (j, cellContent) {

//             if (rowCount == 1) {
//                 doc.margins = 1;
//                 doc.setFont("helvetica");
//                 doc.setFontType("bold");
//                 doc.setFontSize(9);

//                 doc.cell(leftMargin, topMargin, cellWidth, headerRowHeight, cellContent, i)
//             }
//             else if (rowCount == 2) {
//                 doc.margins = 1;
//                 doc.setFont("times ");
//                 doc.setFontType("italic");  // or for normal font type use ------ doc.setFontType("normal");
//                 doc.setFontSize(8);                    

//                 doc.cell(leftMargin, topMargin, cellWidth, rowHeight, cellContent, i); 
//             }
//             else {

//                 doc.margins = 1;
//                 doc.setFont("courier ");
//                 doc.setFontType("bolditalic ");
//                 doc.setFontSize(6.5);                    

//                 doc.cell(leftMargin, topMargin, cellWidth, rowHeight, cellContent, i);  // 1st=left margin    2nd parameter=top margin,     3rd=row cell width      4th=Row height
//             }
//         })
//     })

// doc.save('sample Report.pdf');
                                                };
/////////////////////////////////////////////////////////////
                                                function headerFooterFormatting(doc, totalPages)
                                                {
                                                    for (var i = totalPages; i >= 1; i--)
                                                    {
                                                        doc.setPage(i);
                                                        //header
                                                        header(doc);

                                                        footer(doc, i, totalPages);
                                                        doc.page++;
                                                    }
                                                }
                                                ;

                                                function header(doc)
                                                {
                                                    doc.setFontSize(15);
                                                    doc.setTextColor(20);
                                                    doc.setFontStyle('normal');

//                                                    if (base64Img) {
//                                                        doc.addImage(base64Img, 'JPEG', margins.left, 10, 100, 40);
//                                                    }

                                                    doc.text("Audit Taril Report", margins.left + 170, 40);
                                                    doc.setLineCap(2);
                                                    doc.line(3, 70, margins.width + 43, 70); // horizontal line
                                                }
                                                ;

// You could either use a function similar to this or pre convert an image with for example http://dopiaza.org/tools/datauri
// http://stackoverflow.com/questions/6150289/how-to-convert-image-into-base64-string-using-javascript
                                                function imgToBase64(url, callback, imgVariable) {

                                                    if (!window.FileReader) {
                                                        callback(null);
                                                        return;
                                                    }
                                                    var xhr = new XMLHttpRequest();
                                                    xhr.responseType = 'blob';
                                                    xhr.onload = function () {
                                                        var reader = new FileReader();
                                                        reader.onloadend = function () {
                                                            imgVariable = reader.result.replace('text/xml', 'image/jpeg', 'image/png');
                                                            callback(imgVariable);
                                                        };
                                                        reader.readAsDataURL(xhr.response);
                                                    };
                                                    xhr.open('GET', url);
                                                    xhr.send();
                                                }
                                                ;

                                                function footer(doc, pageNumber, totalPages) {

                                                    doc.setLineCap(2);
                                                    doc.line(3, 800, margins.width + 43, 800); // horizontal line
                                                    var str = "Page " + pageNumber + " of " + totalPages
                                                    var usrname = "<?php echo $this->session->userdata('empname') ?>";
                                                    var rptdate = "<?php echo date("d-m-Y"); ?>";
                                                    var rpttime = "<?php $timezone = "Asia/Kolkata"; date_default_timezone_set($timezone);echo date("H:i:s");?>";
                                                    doc.setFontSize(10);
                                                    doc.text(str, margins.left, doc.internal.pageSize.height - 20);
                                                    var str2 = " User Name : " + usrname + " | Date " + rptdate + " | Time " + rpttime;
                                                    doc.text(str2, margins.left + 250, doc.internal.pageSize.height - 20);

                                                }
                                                ;

                                                function tableToJson(table) {
                                                    var data = [];

// first row needs to be headers
                                                    var headers = [];
                                                    for (var i = 0; i < table.rows[0].cells.length; i++) {
                                                        headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi, '');
                                                    }

// go through cells
                                                    for (var i = 1; i < table.rows.length; i++) {

                                                        var tableRow = table.rows[i];
                                                        var rowData = {};

                                                        for (var j = 0; j < tableRow.cells.length; j++) {

                                                            rowData[ headers[j] ] = tableRow.cells[j].innerHTML;

                                                        }

                                                        data.push(rowData);
                                                    }

                                                    return data;
                                                }

</script>

<script>

    function printDiv()
    {
        $("#audit_trail2").show();
        var divToPrint = document.getElementById('audit_trail2');

        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');

        newWin.document.close();

        setTimeout(function () {
            newWin.close();
        }, 1000);
        $("#audit_trail2").hide();
    }
</script>
</body>

</html>