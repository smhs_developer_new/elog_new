<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'report', "status" => 'active'))->row_array();
$show = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $show = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_edit'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1) {
            $show = true;
        } else {
            $show = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",3000);</script>";
    }
}
$this->db->order_by("id", "DESC");
$footerData = $this->db->get_where("pts_mst_report_footer", array("report_id" => $_GET['module_id'], "status" => "active"))->result_array();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="smhs" content="">

    <title>E-Logbook</title>

    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url() ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>css/fontfamily.css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php echo base_url() ?>css/sb-admin-2.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
    <style>
        [ng\:cloak],
        [ng-cloak],
        [data-ng-cloak],
        [x-ng-cloak],
        .ng-cloak,
        .x-ng-cloak {
            display: none !important;
        }
    </style>

</head>

<body id="page-top" ng-app="reportApp" ng-controller="reportCtrl" ng-cloak>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php $this->load->view("template/report/report_header"); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <!-- Page Heading -->

            <div class="content-wrapper">
                <div class="card card-default">
                    <div class="card-body">
                        <div class="col-auto">
                            <div class="form-row">
                                <div class="col-lg-3 mb-3"><label><b>Room Name</b> <span style="color: red">*</span></label>

                                    <select class="chzn-select form-control" tabindex="4" name="room_code" data-placeholder="Search Room Code" ng-change="getVaccumCleanerList()" ng-options="dataObj['room_code'] as (dataObj.room_code +           ' => ' +           dataObj.room_name +           ' => ' +           dataObj.area_id) for dataObj in sysRoomData" ng-model="room_code" required chosen>
                                        <option value="">Select Room Name</option>
                                    </select>

                                </div>
                                <div class="col-lg-2 mb-2"><label>Vacuum Cleaner Id <span style="color: red">*</span></label>
                                    <select class="chosen form-control" tabindex="4" name="vaccume_no" data-placeholder="Search Vacuum Cleaner" ng-options="dataObj['equipment_code'] as (dataObj.equipment_name + ' => ' + dataObj.equipment_code) for dataObj in vaccumCleanerList" ng-model="vaccume_no" required chosen>
                                        <option value="">Select Vacuum Cleaner</option>
                                    </select>
                                </div>

                                <div class="col-lg-3 mb-3"><label>Start Date</label>
                                    <div class="row">
                                        <div class="input-group col-md-10" moment-picker="start_date" format="DD-MMM-YYYY" start-view="month" locale="en" today="true" max-date="current_date">

                                            <input class="form-control" name="start_date" placeholder="Select Start Date" ng-model="start_date" ng-model-options="{updateOn: 'blur'}" autocomplete="off" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 mb-3"><label>End Date</label>
                                    <div class="row">
                                        <div class="input-group col-md-10" moment-picker="end_date" format="DD-MMM-YYYY" start-view="month" min-date="start_date" locale="en" today="true" max-date="current_date">

                                            <input class="form-control " name="end_date" placeholder="Select End Date" ng-model="end_date" ng-model-options="{updateOn: 'blur'}" autocomplete="off" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-1 mb-3"><label></label>
                                    <button style="margin-top: 25px;" class="btn btn-success" ng-click="getHeaderList()">Go</button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="card card-default mt-4">
                <div class="form-row">
                    <div class="col-lg-10 text-center"></div>
                    <?php if ($show) { ?>
                        <div class="col-lg-2" ng-if="prolog.length > 0"><button style="margin-top:5px;" onclick="ExportPdf()"><i class="fas fa-file-pdf" style="font-size:24px"></i></button></div>
                    <?php } ?>
                </div>

                <!--                    <div class="form-row">
                        <div class="col-lg-10 text-center"></div>

                        <div class="col-lg-2" ng-if="prolog.length > 0"><button style="margin-top:5px;"
                                                                                onclick="ExportPdf()"><i class="fas fa-file-pdf"
                                                     style="font-size:24px"></i></button></div>
                    </div>-->
                <div class="card-body" id="vaccume-report">
                    <!--<div class="form-row">
                            <div class="col-md-2">
                                <img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo">

                            </div>
                            <div class="col-md-10">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th><b>TITLE</b></th>
                                            <td colspan="4">Swab Sample</td>
                                        </tr>
                                        <tr>
                                        <tr>
                                            <th>Form No.</th>
                                            <th>Version No. &amp; Status</th>
                                            <th>Effective Date</th>
                                            <th>Retired Date</th>
                                            <th>Document Reference No.</th>
                                        </tr>
                                    </tbody>
                                    <tbody>
                                        <tr>
                                            <td>{{headerData.length > 0?reportHeader.form_no:'NA'}}</td>
                                            <td>{{headerData.length > 0?reportHeader.version_no+' & Effective':'NA'}} </td>
                                            <td>{{headerData.length > 0?(reportHeader.effective_date| date:'dd-MMM-yyyy'):'NA'}}</td>
                                            <td>{{headerData.length > 0?(reportHeader.retired_date| date:'dd-MMM-yyyy'):'NA'}}</td>
                                            <td>{{headerData.length > 0?reportHeader.document_no:'NA'}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>-->
                    <div class="form-row">
                        <div class="col-md-2">
                            <img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo">
                        </div>
                        <div class="col-md-6 text-center">
                            <h2>VACUUM CLEANER LOG BOOK</h2>
                        </div>
                        <div class="col-md-4">
                            <table class="table" style="border:0px;">
                                <tr>
                                    <th style="border:0px;">BLOCK/AREA/ROOM:</th>
                                    <td style="border:0px;">{{room_code}}</td>
                                </tr>
                                <tr>
                                    <th style="border:0px;">VACUUM CLEANER ID:</th>
                                    <td style="border:0px;">{{vaccume_no}}</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="form-row scroll-table" style="margin-top:10px;">
                        <table class="table custom-table">
                            <thead style="border-top:10px">
                                <tr>
                                    <th>DATE</th>
                                    <th colspan="2" class="text-center">TIME </th>
                                    <th class="text-center">ACTIVITY</th>
                                    <th rowspan="2" width="20%">PRODUCT NAME / MATERIAL NAME<br><small>(APPLICABLE FOR RM RECIEPT)</small></th>
                                    <th rowspan="2" width="20%">BATCH NO./LR NO.<br><small>(APPLICABLE FOR RM RECIEPT)</small></th>
                                    <th width="40%" class="text-center" colspan="2"> CLEANING</th>
                                    <th>REMARK</th>
                                </tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th class="text-center">FROM TIME</th>
                                    <th class="text-center">TO TIME</th>
                                    <th>&nbsp;</th>
                                    <th class="text-center">DONE BY</th>
                                    <th class="text-center">CHECKED BY</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="dataObj in prolog">
                                    <td>{{dataObj.startdate}}</td>
                                    <td>{{dataObj.starttime}}</td>
                                    <td>{{dataObj.activity_time1}}</td>
                                    <td>{{dataObj.activity}}</td>
                                    <td>{{dataObj.product_code}}</td>
                                    <td>{{dataObj.batch_no}}</td>
                                    <td>{{dataObj.startoperator}}<br>({{dataObj.startremark}})</td>
                                    <td>{{dataObj.stopcheckedby}}<br><span ng-if="dataObj.stopcheckedbyremark != ''">({{dataObj.stopcheckedbyremark}})</span></td>                                        
                                    <td>
                                        <span ng-repeat="remarkObj in dataObj.remark track by $index">{{remarkObj}}<br></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="col-lg-12 text-center" ng-show="prolog.length == 0">
                            <h2>No Data Found</h2>
                        </div>
                        <div class="col-lg-12 text-center" ng-show="prolog.length == 0">
                            <h4>(Please Select the above parameters)</h4>
                        </div>
                    </div>
                    <div class="form-row" style="margin-top:10px;" ng-show="headerData.length > 0">
                    <b>FORM NO.</b>&nbsp;{{headerData.length > 0?headerData[0].footer_name:'NA'}}
                    </div>
                    <div class="form-row pb-2" id="pdf_download" style="display:none">
                        <div class="col-md-3"></div>
                        <div class="col-md-3">Print By : <?php echo $this->session->userdata('empname') ?></div>
                        <div class="col-md-3">Date : <?php echo date("d-m-Y"); ?></div>
                        <div class="col-md-3">Time : <?php echo date("H:i:s"); ?></div>
                    </div>
                    <!-- <?php if (!empty($footerData)) { ?>
                            <div class="form-row" style="margin-top:10px;">
                                <b>FORM NO.</b>&nbsp;<?php echo $footerData[0]['footer_name'] ?>
                        </div>
                        <?php } ?> -->
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>SMHS. &copy; 2020 - 2021. All Rights Reserved.</span>
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

</div> <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="<?php echo base_url() ?>user/logout">Logout</a>
                </div>
            </div>
        </div>
    </div>
<script type="text/kendo-template" id="page-template">
    #
        var footer_name = foot_name;
    #
    <div class="page-template">
        <div class="footer">
            <table class="table">
                <tr>
                    <td>
                        <div class="form-row" style="margin-top:10px;">
                            <b>FORM NO.</b>&nbsp;<div id="footer_no1">#=footer_name#</div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"><b>Print By : <?php echo $this->session->userdata('empname') ?></b></div>
                            <div class="col-md-3"><b>Date : <?php echo date("d-M-Y"); ?></b></div>
                            <div class="col-md-3"><b>Time :<?php $timezone = "Asia/Kolkata";date_default_timezone_set($timezone);echo date("H:i:s");?></b></div>
                            <div class="col-md-3"><b>Page #: pageNum # of #: totalPages #</b></div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</script>
<div id="master_data_div" style="display: none;">
    <div class="card-body ht750" id="audit_trail2" style="margin-top:10px;padding-top:10px">                      
	<div class="form-row scroll-table" style="margin-top:10px;">
            <style>
                    #tbldata {border-collapse:collapse; table-layout:fixed; width:310px; height: 100px !important;}
                    #tbldata td {width:100px !important; word-wrap:break-word; height:100px;font-size: 15px;}
                    
            </style>
            <table id="tbldata" border="1" style="width:100%;color:black;">
                <thead style="max-height:165px;">
                    <tr style="max-height:80px;">
                        <td colspan="9">
                            <div class="form-row">
                                <div class="col-md-2">
                                    <img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo">
                                </div>
                                <div class="col-md-6 text-center">
                                    <h2>VACUUM CLEANER LOG BOOK</h2>
                                </div>
                                <div class="col-md-4">
                                    <table id="equip_width" style="border:0px;">
                                        <tr>
                                            <td style="border:0px;width:200px !important; height: 20px !important"><b>BLOCK/AREA/ROOM:</b></td>
                                            <td style="border:0px;width:200px !important; height: 20px !important"">{{room_code}}</td>
                                        </tr>
                                        <tr>
                                            <td style="border:0px;width:200px !important; height: 20px !important""><b>VACUUM CLEANER ID:</b></td>
                                            <td style="border:0px;width:200px !important; height: 20px !important"">{{vaccume_no}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color:#4e73df;color:#fff;height:60px;">
                        <th>DATE</th>
                        <th colspan="2" class="text-center">TIME </th>
                        <th class="text-center">ACTIVITY</th>
                        <th rowspan="2" width="20%">PRODUCT NAME / MATERIAL NAME<br><small>(APPLICABLE FOR RM RECIEPT)</small></th>
                        <th rowspan="2" width="20%">BATCH NO./LR NO.<br><small>(APPLICABLE FOR RM RECIEPT)</small></th>
                        <th width="40%" class="text-center" colspan="2"> CLEANING</th>
                        <th>REMARK</th>
                    </tr>
                    <tr style="background-color:#4e73df;color:#fff;height:40px;">
                        <th>&nbsp;</th>
                        <th class="text-center">FROM TIME</th>
                        <th class="text-center">TO TIME</th>
                        <th>&nbsp;</th>
                        <th class="text-center">DONE BY</th>
                        <th class="text-center">CHECKED BY</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody >
                    <tr style="max-height: 100px" ng-repeat="dataObj in prolog">
                        <td>{{dataObj.startdate}}</td>
                        <td>{{dataObj.starttime}}</td>
                        <td>{{dataObj.activity_time1}}</td>
                        <td>{{dataObj.activity}}</td>
                        <td>{{dataObj.product_code}}</td>
                        <td>{{dataObj.batch_no}}</td>
                        <td>{{dataObj.startoperator}}<br>({{dataObj.startremark}})</td>
                        <td>{{dataObj.stopcheckedby}}<br><span ng-if="dataObj.stopcheckedbyremark != ''">({{dataObj.stopcheckedbyremark}})</span></td>
                        <td><span ng-repeat="remarkObj in dataObj.remark track by $index">{{remarkObj}}<br></span></td>
                    </tr>
                </tbody>
            </table>
	</div>
    </div>
</div>
<style>
    /* Page Template for the exported PDF */
    .page-template {
      font-family: "DejaVu Sans", "Arial", "sans-serif";
      position: absolute;
      width: 100%;
      height: 100%;
      color: black;
      top: 0;
      left: 0;
    }
    .page-template .header {
      position: absolute;
      top: 0px;
      left: 30px;
      right: 30px;
      color: black;
      bottom:20px;
      margin-bottom: 10px;
      padding-bottom: 10px;
    }
    .page-template .footer {
      position: absolute;
      bottom: 10px;
      left: 30px;
      right: 30px;
      border-top: 1px solid black;
      text-align: center;
      color: #000000;
    }
    .k-grid-header .k-header {
        height: 10px;
        padding: 0;
      }

      .k-grid tbody tr {
        line-height: 14px !important;
      }

      .k-grid tbody td {
        padding: 0;
      }
</style>
    <!-- Bootstrap core JavaScript-->
    
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
    <script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>js/angular.min.js"></script>
    <script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
    <script src="<?php echo base_url() ?>js/lodash.min.js"></script>
    <script src="<?php echo base_url() ?>js/angularjs-dropdown-multiselect.min.js"></script>
    <script src="<?php echo base_url() ?>js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>js/kendo.all.min.js"></script>
    <script src="<?php echo base_url() ?>js/moment-with-locales.js"></script>
    <script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
    <script type="text/javascript">
        var foot_name = '';
        var url_base_path = '<?php echo base_url() ?>';
        var app = angular.module("reportApp", ['angular.chosen', 'moment-picker']);
        app.directive('chosen', function($timeout) {

            var linker = function(scope, element, attr) {

                scope.$watch('sysRoomData', function() {
                    $timeout(function() {
                        element.trigger('chosen:updated');
                    }, 0, false);
                }, true);
                scope.$watch('vaccumCleanerList', function() {
                    $timeout(function() {
                        element.trigger('chosen:updated');
                    }, 0, false);
                }, true);
                $timeout(function() {
                    element.chosen();
                }, 0, false);
            };
            return {
                restrict: 'A',
                link: linker
            };
        });
        app.controller("reportCtrl", function($scope, $http, $filter) {
            // checking header
            $scope.headerData = [];
            $scope.reportHeader = {};
            $scope.current_date = moment().format('DD-MMM-YYYY');
            $scope.start_date = moment().format('DD-MMM-YYYY');
            $scope.end_date = moment().format('DD-MMM-YYYY');
            $scope.getHeaderList = function() {
                if ($scope.start_date == "" || $scope.start_date == undefined || $scope.end_date == "" || $scope.end_date == undefined) {
                    alert("Please Select the date range first...!");
                    return false;
                }
                $http({
                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getHeaderByLogid',
                    method: "POST",
                    data: "header_id=" + <?php echo $_GET['mst_act_id'] ?> + "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).then(function(response) {
                    $scope.headerData = response.data.header_list;
                    if ($scope.headerData.length > 0) {
                        if ($scope.headerData.length > 1) {
                            alert("There are more than one header in this date range please change it, Thank you...!");
                            $scope.headerData = [];
                            return false;
                        } else {
                            $scope.reportHeader = response.data.header_list[0];
                            foot_name = $scope.reportHeader.footer_name;
                            $scope.vaccumeReportData();
                        }
                    } else {
                        alert("No version is effective in selected date range, please change the dates or check with administrator for \"Report Header/Footer Master\"");
                        $scope.headerData = [];
                        return false;
                    }
                }, function(error) { // optional
                    console.log("Something went wrong.Please try again");
                });
            }
            //End                                

            $scope.sysRoomData = [];
            $scope.getDeviceRoomList = function() {
                $http({
                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist',
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).then(function(response) {
                    $scope.sysRoomData = response.data.room_list;
                }, function(error) { // optional

                    console.log("Something went wrong.Please try again");

                });
            }
            $scope.getDeviceRoomList();
            $scope.vaccumCleanerList = [];
            $scope.getVaccumCleanerList = function() {
                $http({
                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetVaccumCleanerlist?room_code=' + $scope.room_code,
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).then(function(response) {
                    $scope.vaccumCleanerList = response.data.vaccum_cleaner_list;
                }, function(error) { // optional

                    console.log("Something went wrong.Please try again");

                });
            }
            $scope.getVaccumCleanerList();

            $scope.vaccumeData = [];

            $scope.room_code = "";
            $scope.vaccume_no = "";
            $scope.vaccumeReportData = function() {
                var valid = true;
                if ($scope.vaccume_no == "" || $scope.vaccume_no == undefined || $scope.room_code == "" || $scope.room_code == undefined || $scope.start_date == "" || $scope.start_date == undefined || $scope.end_date == "" || $scope.end_date == undefined) {
                    valid = false;
                }
                if (valid) {
                    $scope.prolog = [];
                    $scope.vaccumeData = [];
                    $http({
                        url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/getVaccumeReport',
                        method: "POST",
                        data: "vaccume_no=" + $scope.vaccume_no + "&room_code=" + $scope.room_code + "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                    }).then(function(response) {
                        $scope.vaccumeData = response.data.row_data;
                        var startoperator = "";
                        var startremark = "";
                        var startdate = "";
                        var starttime = "";
                        var stopoperator = "";
                        var stopremark = "";
                        var stopdate = "";
                        var stoptime = "";
                        var startcheckedby = "";
                        var startcheckedbyremark = "";
                        var stopcheckedby = "";
                        var stopcheckedbyremark = "";
                        var isworkflow = "no";
                        var remark = "";
                        var qa = 'NA';
                        var qaremark = 'NA';
                        var old_id = '';
                        var activity_time1 = '';
                        angular.forEach($scope.vaccumeData, function(value, key) {
                            if(value.id != old_id){
                                old_id = value.id;
                                startoperator = startremark = startdate = starttime = stopoperator = stopremark = stopdate = stoptime = startcheckedby = '';
                                startcheckedbyremark = stopcheckedby = stopcheckedbyremark = remark = activity_time1 = '';
                                isworkflow = 'no'; qa = qaremark = 'NA'; 
                            }
                            if (value['workflow_type'] == 'start') {
                                if (value['approval_status'] == 'N/A') {
                                    startoperator = value['user_name'];
                                    startdate = value['start_date'];
                                    starttime = value['start_time'];
                                    isworkflow = "no";
                                    startremark = value['activity_time'];
                                    remark = value['user_name'] + ':-' + value['activity_remarks'];
                                    stopdate = "";
                                    stoptime = "";
                                    var tempStr = remark.split("~");
                                    var uniqueNames = getUnique(tempStr);
                                } else {
                                    startcheckedby = value['user_name'];
                                    isworkflow = "yes";
                                    startcheckedbyremark = value['activity_time'];
                                    remark = remark + '~' + value['user_name'] + ':-' + value['activity_remarks'];
                                }
                            } else if (value['workflow_type'] == 'stop') {
                                if (value['approval_status'] == 'N/A') {
                                    stopoperator = value['user_name'];
                                    stopdate = value['stop_date'];
                                    stoptime = value['stop_time'];
                                    isworkflow = "no";
                                    activity_time1 = value['activity_time1'];
                                    stopremark = value['activity_time'];
                                    remark = remark + "~" + value['user_name'] + ':-' + value['activity_remarks'];
                                    var tempStr = remark.split("~");
                                    var uniqueNames = getUnique(tempStr);
                                }else if (value['approval_status'] == 'QAapprove') {
                                    qa = value['user_name'];
                                    isworkflow = "no";
                                    stopremark = value['activity_time'];
                                    remark = remark + "~" + value['user_name'] + ':-' + value['activity_remarks'];
                                    var tempStr = remark.split("~");
                                    var uniqueNames = getUnique(tempStr);
                                } else {
                                    stopcheckedby = value['user_name'];
                                    isworkflow = "yes";
                                    stopcheckedbyremark = value['activity_time'];
                                    remark = remark + "~" + value['user_name'] + ':-' + value['activity_remarks'];
                                    
                                    var tempStr = remark.split("~");
                                    var uniqueNames = getUnique(tempStr);
                                }
                            } else {
                                if (isworkflow == "no") {
                                    startcheckedby = "";
                                    stopcheckedby = "";
                                    stopcheckedbyremark= "";
                                }
                                remark = remark + "~" + value['user_name'] + ':-' + value['activity_remarks'];
                                var tempStr = remark.split("~");
                                var uniqueNames = getUnique(tempStr);
                            }
                            var temp = {
                                "startdate": startdate,"starttime": starttime,"stopdate": stopdate,"stoptime": stoptime,"activity": value['activity_name'],"startoperator": startoperator,"startremark": startremark,"startcheckedby": startcheckedby,"startcheckedbyremark": startcheckedbyremark,"stopoperator": stopoperator,"stopremark": stopremark,"stopcheckedby": stopcheckedby,"stopcheckedbyremark": stopcheckedbyremark,"remark": uniqueNames,"product_code": value['product_code'],"batch_no": value['batch_no'],"qa": value['user_name'],"activity_time1":activity_time1
                            }
                            $scope.prolog[value['id']] = temp;
                        });

                        //                                            var startdate = "";
                        //                                            var starttime = "";
                        //                                            var stoptime = "";
                        //                                            var startcheckedby = "";
                        //                                            var stopcheckedby = "";
                        //                                            var isworkflow = "no";
                        //                                            angular.forEach($scope.vaccumeData, function (value, key) {
                        //
                        //                                                if (value['workflow_type'] == 'start')
                        //                                                {
                        //                                                    if (value['approval_status'] == 'N/A')
                        //                                                    {
                        //                                                        startdate = value['usr_act_log_start_date'];
                        //                                                        starttime = value['usr_act_log_start_time'];
                        //                                                        stoptime = value['usr_act_log_stop_time'];
                        //                                                        isworkflow = "no";
                        //                                                    } else
                        //                                                    {
                        //                                                        startcheckedby = value['done_by_user_name'];
                        //                                                        isworkflow = "yes";
                        //                                                    }
                        //                                                } else if (value['workflow_type'] == 'stop')
                        //                                                {
                        //                                                    if (value['approval_status'] == 'N/A')
                        //                                                    {
                        //
                        //                                                        isworkflow = "no";
                        //                                                    } else
                        //                                                    {
                        //                                                        stoptime = value['usr_act_log_stop_time'];
                        //                                                        stopcheckedby = value['user_name'];
                        //                                                        isworkflow = "yes";
                        //                                                    }
                        //                                                } else
                        //                                                {
                        //                                                    if (isworkflow == "no") {
                        //                                                        startcheckedby = "";
                        //                                                        stopcheckedby = "";
                        //                                                    }
                        //                                                    startdate = $filter('date')(new Date(value['created_date']), 'dd-MM-yyyy');
                        //                                                    starttime = $filter('date')(new Date(value['created_date']), 'HH:mm:ss');
                        //                                                    stoptime = $filter('date')(new Date(value['created_date']), 'HH:mm:ss');
                        //
                        //                                                }
                        //                                                $scope.prolog[value['id']] = {"created_date": startdate, "usr_act_log_start_time": starttime, "usr_act_log_stop_time": stoptime, "product_code": value['product_code'], "batch_no": value['batch_no'], "user_name": value['done_by_user_name'], "done_by_remark": value['done_by_remark'], "checked_by": stopcheckedby};
                        //                                            });

                        $scope.prolog = $scope.prolog.filter(function() {
                            return true
                        });
                        //$scope.prolog.reverse();
                        //console.log($scope.prolog);
                    }, function(error) { // optional

                        console.log("Something went wrong.Please try again");

                    });
                } else {

                    alert("Please Select Room Name and Vaccume Cleaner Name and Start Date and End Date");

                }
            }

            function getUnique(array) {
                var uniqueArray = [];

                // Loop through array values
                for (i = 0; i < array.length; i++) {
                    if (uniqueArray.indexOf(array[i]) === -1) {
                        uniqueArray.push(array[i]);
                    }
                }
                return uniqueArray;
            }
        });

        function ExportPdf() {
            $("#master_data_div").show();
            kendo.drawing.drawDOM("#master_data_div",{
                paperSize: "A3",
                landscape: true,
                pageable: true,
                repeatHeaders:true,
                template: $("#page-template").html(),
                margin: {top: "100px", bottom: "20px"},
                color:"black",
                //padding: {top: "40px", bottom: "40px"},
                scale: 0.7,
                height: 1200
            }).then(function (group) {
                kendo.drawing.pdf.saveAs(group,"vaccum_cleaner.pdf")
            }).always(function () {
                //$("#pdf_download").hide();
                $("#master_data_div").hide();
            });
        }
    </script>
    <script src="<?php echo base_url(); ?>assets/js/disable_editor.js"></script>
</body>

</html>