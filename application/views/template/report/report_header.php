<?php
//$diff = time() - $_SESSION['__ci_last_regenerate'];
//
//if ($diff > 300) {
//    redirect(base_url() . 'User/logout');
//} else {
//    $_SESSION['__ci_last_regenerate'] = time(); //set new timestamp
//}
?><input type="hidden" id="base" value="<?php echo base_url(); ?>">
<?php

$reportData = $this->db->get_where("pts_mst_report", array("status" => "active"))->result_array();
$masterData = $this->db->get_where("pts_mst_master", array("status" => "active"))->result_array();
$url=$_SERVER['REQUEST_URI'];
$end = array_slice(explode('/', $url), -1)[0];
?>
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">



    <!-- Sidebar - Brand -->

    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url(); ?>User/home">

        <div class="sidebar-brand-text mx-3">SMHS eLogBook</div>

    </a>



    <!-- Divider -->

    <hr class="sidebar-divider">



    <div class="sidebar-heading">

        Main Navigation

    </div>



    <!-- Nav Item - Dashboard -->

    <li class="nav-item">

        <a class="nav-link" href="<?php echo base_url(); ?>User/home">

            <i class="fas fa-home"></i>

            <span>Home</span></a>

    </li>



    <!-- Nav Item - Pages Collapse Menu -->

    <li class="nav-item">

        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">

            <i class="fas fa-fw fa-cog"></i>

            <span>Master</span>

        </a>

        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar" style="max-height: 300px;overflow: auto;">

            <!-- <div class="submenu-bg  py-2 collapse-inner rounded scrollbar" id="style-8"> -->
                <div class="submenu-bg  py-2 collapse-inner rounded">
                 <?php
                if (!empty($masterData)) {
                        foreach ($masterData as $key => $value) {  ?>
                            <a class="collapse-item" href="<?php echo base_url(); ?><?php echo $value['master_url'] ?>?module_id=<?php echo $value['id']?>"><?php echo $value['master_name'] ?></a>
                      <?php  }
                    }
                
                ?>

            </div>

        </div>

    </li>



    <!-- Nav Item - Utilities Collapse Menu -->



    <!-- Nav Item - Pages Collapse Menu -->

    <li class="nav-item">

        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">

            <i class="fas fa-fw fa-wrench"></i>

            <span>Report</span>

        </a>

        <div id="collapseUtilities" class="collapse <?php if($end!='home'){ echo 'show';}?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar" style="max-height: 300px;overflow: auto;">

            <!-- <div class="submenu-bg py-2 collapse-inner rounded scrollbar" id="style-8"> -->
                <div class="submenu-bg py-2 collapse-inner rounded">
                <?php
                if (!empty($reportData)) {
                        foreach ($reportData as $key => $value) {  ?>
                            
                            <a class="collapse-item" href="<?php echo base_url(); ?><?php echo $value['report_url'] ?>?module_id=<?php echo $value['id']?>&mst_act_id=<?php echo $value['mst_act_id']?>"><?php echo $value['report_name'] ?></a>
                      <?php  }
                    }
                
                ?>
            </div>

        </div>

    </li>



   <!--  <li class="nav-item">

        <a class="nav-link" href="#">

            <i class="fas fa-home"></i>

            <span>First time Plant Set-up</span></a>

    </li>



    <li class="nav-item">

        <a class="nav-link" href="#">

            <i class="fas fa-home"></i>

            <span>Create Master Data</span></a>

    </li> -->



    <!-- Divider -->

    <hr class="sidebar-divider d-none d-md-block">



    <!-- Sidebar Toggler (Sidebar) -->

    <div class="text-center d-none d-md-inline">

        <button class="rounded-circle border-0" id="sidebarToggle"></button>

    </div>



</ul>


<!-- End of Sidebar -->

<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">
        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

            <!-- Sidebar Toggle (Topbar) -->
            <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
            </button>

            <!-- Topbar Navbar -->
            <ul class="navbar-nav ml-auto">

                <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                <li class="nav-item dropdown no-arrow d-sm-none">
                    <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-search fa-fw"></i>
                    </a>
                    <!-- Dropdown - Messages -->
                    <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                         aria-labelledby="searchDropdown">
                        <form class="form-inline mr-auto w-100 navbar-search">
                            <div class="input-group">
                                <input type="text" class="form-control bg-light border-0 small"
                                       placeholder="Search for..." aria-label="Search"
                                       aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fas fa-search fa-sm"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </li>


                <div class="topbar-divider d-none d-sm-block"></div>

                <!-- Nav Item - User Information -->
                <li class="nav-item dropdown no-arrow">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span
                            class="mr-2 mt-3 d-none d-lg-inline text-gray-600 small"><?php echo $this->session->userdata('empname') ?><p style="text-center">(<?php echo $this->session->userdata('role_description') ?>)</p></span>
                        <img class="img-profile rounded-circle" src="<?php echo base_url(); ?>img/user/02.jpg">
                    </a>
                    <!-- Dropdown - User Information -->
                    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                         aria-labelledby="userDropdown">
                        <!-- <a class="dropdown-item" href="#">
            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
            Profile
            </a>
            <a class="dropdown-item" href="#">
            <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
            Settings
            </a>
            <a class="dropdown-item" href="#">
            <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
            Activity Log
            </a> -->
                        <a class="dropdown-item" href="" data-toggle="modal" data-target="#logoutModal">
                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                            Logout
                        </a>
                    </div>
                </li>

            </ul>
        </nav>
        <!-- End of Topbar -->
        <input type="hidden" id="base" value="<?php echo base_url(); ?>">
        <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
       <script src="<?php echo base_url(); ?>js/network.js"></script>
<script src="<?php echo base_url(); ?>assets/js/session_validation.js"></script>
