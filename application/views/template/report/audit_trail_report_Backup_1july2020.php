<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'report',"status"=>'active'))->row_array();
$show = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $show = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_edit']!=1) {
            $url=base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '".$url."';\",1);</script>";
        } elseif ($res['is_edit'] == 1) {
            $show = true;
        } else {
            $show = false;
        }
    } else {
        $url=base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '".$url."';\",3000);</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="smhs" content="">

        <title>E-Logbook</title>

        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url() ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="<?php echo base_url() ?>css/sb-admin-2.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/custom.css" rel="stylesheet">

        <link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
        <style>
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
            .pagination {
                display: inline-block;
                padding-left: 0;
                margin: 20px 0;
                border-radius: 4px;
            }
            .pagination > li {
                display: inline;
            }
            
            .content-loader {
                position: absolute;
                left: 50%;
                top: 50%;
                z-index: 9;
                margin: -35px auto;
                height: 70px;
                width: 70px;
            }
        </style>


<!-- <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" /> -->
    </head>

    <body id="page-top" ng-app="reportApp" ng-controller="reportCtrl" ng-cloak>

        <!-- Page Wrapper -->
        <div id="wrapper">

            <?php $this->load->view("template/report/report_header"); ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->

                <div class="content-wrapper">
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="col-auto">
                                <div class="form-row">
                                    <div class="col-lg-3 mb-3"><label><b>Start Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-10"
                                                 moment-picker="start_date"
                                                 format="DD-MMM-YYYY"  start-view="month"    locale="en" today="true">

                                                <input class="form-control" name="start_date"
                                                       placeholder="Select Start Date"
                                                       ng-model="start_date"
                                                       ng-model-options="{updateOn: 'blur'}" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label><b>End Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-10"
                                                 moment-picker="end_date"
                                                 format="DD-MMM-YYYY"   start-view="month" min-date="start_date"  locale="en" today="true">

                                                <input class="form-control " name="end_date"
                                                       placeholder="Select End Date"
                                                       ng-model="end_date"
                                                       ng-model-options="{updateOn: 'blur'}" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label><b>User Name</b></label>
                                        <select class="form-control" tabindex="4" name="user_name" data-placeholder="Search User"  ng-model="user_name">
                                            <option value="">Select User Name</option>
                                            <option value="{{dataObj.emp_name}}" ng-repeat="dataObj in userListData">{{dataObj.emp_name}}</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label></label>
                                        <button style="margin-top: 25px;" type="button" class="btn btn-success" ng-click="resetReportData(1)">Go</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="card card-default mt-4">
                    
                    <!--                    <div class="form-row">
                                            <div class="col-lg-10 text-center"></div>
                                            <div class="col-lg-2" ng-if="auditTrailData.length > 0"><button style="margin-top:5px;"  onclick="ExportPdf()"><i class="fas fa-file-pdf" style="font-size:24px"></i></button></div>
                                        </div>-->
                    <div class="card-body ht750" id="audit_trail">
                        <div class="form-row pb-2" id="pdf_download" style="display:none">
                            <div class="col-md-4"></div>
                            <div class="col-md-3">User Name : <?php echo $this->session->userdata('empname') ?></div>
                            <div class="col-md-3">Date : <?php echo date("d-m-Y");?></div>
                            <div class="col-md-2">Time : <?php $timezone = "Asia/Kolkata";
        date_default_timezone_set($timezone); echo date("h:i:sA");?></div>
                        </div>
                        <div class="form-row">
                            <div class="col-lg-4"></div>
                             <div class="col-lg-4 text-center"><h2>Audit Trail Report</h2></div>
                             <?php if ($show) { ?>
                            <div class="col-lg-4 text-right" ng-if="auditTrailData.length > 0"><button style="margin-top:-15px;"  class="btn_trns" onclick="ExportPdf()"><i class="fas fa-file-pdf pdf_btn" style="color:red;"></i></button></div>
                        <?php } ?>
                           
                        </div>
                        <div class="form-row scroll-table" style="margin-top:10px;">
                            <div ng-show='loading' style="text-align:center" class='content-loader'><img src="<?=base_url('assets/images/loading.gif')?>" width="70px"></div>
                            <table class="table custom-table tableP runningacttable">
                                <thead>
                                    <tr>
                                        <!--<th>Device ID</th>-->
                                        <th>User ID</th>
                                        <th>Date-Time</th>
                                        <th>Doc No.</th>
                                        <th>Room</th>
                                        <th>Room Description</th>
                                        <th>Event Type</th>
                                        <th>Activity</th>
                                        <th>Batch No</th>
                                        <th>Product Code and Description</th>
                                        <th>Event Detail- Created</th>
                                        <th>Event Detail- Modified</th>
                                    </tr>
                                </thead>
                                <tbody dir-paginate="dataObj in auditTrailData |itemsPerPage:itemsPerPage" total-items="total_count" ng-show="auditTrailData.length > 0">
                                    <tr>
                                        <!--<td>{{dataObj.device_id}}</td>-->
                                        <td>{{dataObj.user_name}}</td>
                                        <td>{{dataObj.datetime | format | date:'d-MMM-yyyy H:m:s'}}</td>
                                        <td>{{dataObj.doc_id!=null && dataObj.doc_id!=0?dataObj.doc_id:'NA'}}</td> 
                                        <td>{{dataObj.room_code!=null && dataObj.room_code!=0?dataObj.room_code:'NA'}}</td>
                                        <td>{{dataObj.room_name!=null && dataObj.room_name!=0?dataObj.room_name:'NA'}}</td>
                                        <td>{{dataObj.logname | removeUnderscores | capitalize}}</td>
                                        <td >{{dataObj.logname == 'Line Log' || dataObj.logname == 'Process Log' || dataObj.logname == 'Portable Equipment Log' || dataObj.logname == 'LAF Pressure Differential Record Sheet'?dataObj.actname:(dataObj.logname!=0?(dataObj.logname | removeUnderscores | capitalize):'NA')}}</td>
                                        <td>{{dataObj.batch_no!=null && dataObj.batch_no!=0?dataObj.batch_no:'NA'}}</td>
                                        <td ng-if="dataObj.product_code!=null && dataObj.product_code!=0">{{dataObj.product_code}}({{dataObj.product_name}})</td>
                                        <td ng-if="dataObj.product_code==null || dataObj.product_code==0">NA</td>
                                        <td>
<!--                                            <p ng-if="(dataObj.workflow_type == 'start' && dataObj.approval_status == 'N/A')">{{dataObj.logname + '/' + dataObj.actname + ' has been Started by ' + dataObj.user_name + ' with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'stop' && dataObj.approval_status == 'N/A')">{{dataObj.logname + '/' + dataObj.actname + ' has been Stopped by ' + dataObj.user_name + ' with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'start' && dataObj.approval_status == 'approve')">{{dataObj.logname + '/' + dataObj.actname + ' has been Approved by ' + dataObj.user_name + ' in start workflow with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'stop' && dataObj.approval_status == 'approve')">{{dataObj.logname + '/' + dataObj.actname + ' has been Approved by ' + dataObj.user_name + ' in stop workflow with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == NULL && dataObj.approval_status == 'approve')">{{dataObj.logname + '/' + dataObj.actname + ' has been Approved by ' + dataObj.user_name + ' in stop workflow with Remark(' + dataObj.activity_remarks + ')'}}</p>-->
                                            <p ng-if="(dataObj.workflow_type == 'start' && dataObj.approval_status == 'N/A')">{{'Activity has been Started with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'stop' && dataObj.approval_status == 'N/A')">{{'Activity has been Stopped with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'start' && dataObj.approval_status == 'approve')">{{'Activity has been Approved with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'stop' && dataObj.approval_status == 'approve')">{{'Activity has been Approved with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == NULL && dataObj.approval_status == 'approve')">{{'Activity has been Approved with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="dataObj.logname == 'Room In'">Successful log in</p>
                                            <p ng-if="dataObj.logname == 'Room Out'">Successful log Out</p>
                                            <p ng-if="dataObj.logname == 'Batch In'">Successful Batch in</p>
                                            <p ng-if="dataObj.logname == 'Batch Out'">Successful Batch Out</p>
                                            <p ng-if="dataObj.logname == 'User Login'">{{dataObj.action_performed}}</p>
                                            <p ng-if="dataObj.insertedData != ''">Below detail has been created: <br>{{dataObj.insertedData}}</p>
                                            <p ng-if="dataObj.insertedWorkflowData != ''">{{dataObj.insertedWorkflowData}}</p>
                                            <p ng-if="dataObj.insertedAccessData != ''" ng-bind-html="dataObj.insertedAccessData"></p>
                                            <p ng-if="dataObj.workflow_type==0 && dataObj.insertedAccessData=='' && dataObj.insertedWorkflowData=='' && dataObj.approval_status ==0 && dataObj.insertedData=='' && dataObj.logname != 'Room In' && dataObj.logname != 'Room Out' && dataObj.logname != 'Batch In' && dataObj.logname != 'Batch Out' && dataObj.logname != 'User Login'">NA</p>
                                           
                                        </td>
                                        <td>
                                            <p ng-if="dataObj.updatedData!=''">Below detail has been updated: <br>{{dataObj.updatedData}}</p>
                                            <p ng-if="dataObj.updatedWorkflowData!=''">{{dataObj.updatedWorkflowData}}</p>
                                            <p ng-if="dataObj.updatedAccessData!=''">{{dataObj.updatedAccessData}}</p>
                                            <p ng-if="dataObj.deletedData!=''">Below Detailed Record is Deactivated : <br>{{dataObj.deletedData}}</p>
                                            <p ng-if="dataObj.updatedData=='' && dataObj.updatedWorkflowData=='' && dataObj.updatedAccessData=='' && dataObj.deletedData==''">NA</p>
                                       
                                        </td>

<!-- <td>{{dataObj.logname +'/'+ dataObj.actname + 'has been' + dataObj.workflow_type + '\n' + dataObj.approval_status + '\n' + dataObj.activity_remarks }}</td -->
                                    </tr>

                                </tbody>                                
                            </table>
                            <div class="col-lg-12">
                                <dir-pagination-controls
                                    max-size="8"
                                    direction-links="true"
                                    boundary-links="true" 
                                    on-page-change="auditTrailReportData(newPageNumber)" >
                                </dir-pagination-controls>
                            </div>
                            <div class="col-lg-12 text-center" ng-show="auditTrailData.length == 0 && loading == false"><h2>No Data Found</h2></div>
                        </div>
                    </div>
                    <!-- Below code for audit report pdf out put -->                    
                    <div class="card-body ht750" id="audit_trail2" style="display:none"> 
                        <div class="form-row pb-2" id="pdf_download2">
                            <div class="col-md-4"></div>
                            <div class="col-md-3">User Name : <?php echo $this->session->userdata('empname') ?></div>
                            <div class="col-md-3">Date : <?php echo date("d-M-Y");?></div>
                            <div class="col-md-2">Time : <?php $timezone = "Asia/Kolkata";
        date_default_timezone_set($timezone); echo date("h:i:sA");?></div>
                        </div>
                        <div class="form-row">
                            <div class="col-lg-4"></div>
                             <div class="col-lg-4 text-center"><h2>Audit Trail Report</h2></div>
                        </div>
                        <div class="form-row scroll-table" style="margin-top:10px;">
                            <div ng-show='loading' style="text-align:center" class='content-loader'><img src="<?=base_url('assets/images/loading.gif')?>" width="70px"></div>
                            <table class="table custom-table tableP runningacttable">
                                <thead>
                                    <tr>
                                        <!--<th>Device ID</th>-->
                                        <th>User ID</th>
                                        <th>Date-Time</th>
                                        <th>Doc No.</th>
                                        <th>Room</th>
                                        <th>Room Description</th>
                                        <th>Event Type</th>
                                        <th>Activity</th>
                                        <th>Batch No</th>
                                        <th>Product Code and Description</th>
                                        <th>Event Detail- Created</th>
                                        <th>Event Detail- Modified</th>
                                    </tr>
                                </thead>
                                <tbody ng-repeat="dataObj in auditTrailData2" ng-show="auditTrailData2.length > 0">
                                    <tr>
                                        <!--<td>{{dataObj.device_id}}</td>-->
                                        <td>{{dataObj.user_name}}</td>
                                        <td>{{dataObj.datetime | format | date:'d-MMM-yyyy H:m:s'}}</td>
                                        <td>{{dataObj.doc_id!=null && dataObj.doc_id!=0?dataObj.doc_id:'NA'}}</td> 
                                        <td>{{dataObj.room_code!=null && dataObj.room_code!=0?dataObj.room_code:'NA'}}</td>
                                        <td>{{dataObj.room_name!=null && dataObj.room_name!=0?dataObj.room_name:'NA'}}</td>
                                        <td>{{dataObj.logname | removeUnderscores | capitalize}}</td>
                                        <td >{{dataObj.logname == 'Line Log' || dataObj.logname == 'Process Log' || dataObj.logname == 'Portable Equipment Log' || dataObj.logname == 'LAF Pressure Differential Record Sheet'?dataObj.actname:(dataObj.logname!=0?(dataObj.logname | removeUnderscores | capitalize):'NA')}}</td>
                                        <td>{{dataObj.batch_no!=null && dataObj.batch_no!=0?dataObj.batch_no:'NA'}}</td>
                                        <td ng-if="dataObj.product_code!=null && dataObj.product_code!=0">{{dataObj.product_code}}({{dataObj.product_name}})</td>
                                        <td ng-if="dataObj.product_code==null || dataObj.product_code==0">NA</td>
                                        <td>
                                            <p ng-if="(dataObj.workflow_type == 'start' && dataObj.approval_status == 'N/A')">{{'Activity has been Started with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'stop' && dataObj.approval_status == 'N/A')">{{'Activity has been Stopped with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'start' && dataObj.approval_status == 'approve')">{{'Activity has been Approved with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == 'stop' && dataObj.approval_status == 'approve')">{{'Activity has been Approved with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="(dataObj.workflow_type == NULL && dataObj.approval_status == 'approve')">{{'Activity has been Approved with Remark(' + dataObj.activity_remarks + ')'}}</p>
                                            <p ng-if="dataObj.logname == 'Room In'">Successful log in</p>
                                            <p ng-if="dataObj.logname == 'Room Out'">Successful log Out</p>
                                            <p ng-if="dataObj.logname == 'Batch In'">Successful Batch in</p>
                                            <p ng-if="dataObj.logname == 'Batch Out'">Successful Batch Out</p>
                                            <p ng-if="dataObj.logname == 'User Login'">{{dataObj.action_performed}}</p>
                                            <p ng-if="dataObj.insertedData != ''">Below detail has been created: <br>{{dataObj.insertedData}}</p>
                                            <p ng-if="dataObj.insertedWorkflowData != ''">{{dataObj.insertedWorkflowData}}</p>
                                            <p ng-if="dataObj.insertedAccessData != ''" ng-bind-html="dataObj.insertedAccessData"></p>
                                            <p ng-if="dataObj.workflow_type==0 && dataObj.insertedAccessData=='' && dataObj.insertedWorkflowData=='' && dataObj.approval_status ==0 && dataObj.insertedData=='' && dataObj.logname != 'Room In' && dataObj.logname != 'Room Out' && dataObj.logname != 'Batch In' && dataObj.logname != 'Batch Out' && dataObj.logname != 'User Login'">NA</p>
                                        </td>
                                        <td>
                                            <p ng-if="dataObj.updatedData!=''">Below detail has been updated: <br>{{dataObj.updatedData}}</p>
                                            <p ng-if="dataObj.updatedWorkflowData!=''">{{dataObj.updatedWorkflowData}}</p>
                                            <p ng-if="dataObj.updatedAccessData!=''">{{dataObj.updatedAccessData}}</p>
                                            <p ng-if="dataObj.deletedData!=''">Below Detailed Record is Deactivated : <br>{{dataObj.deletedData}}</p>
                                            <p ng-if="dataObj.updatedData=='' && dataObj.updatedWorkflowData=='' && dataObj.updatedAccessData=='' && dataObj.deletedData==''">NA</p>
                                        </td>
                                    </tr>
                                </tbody>                                
                            </table>
                            <div class="col-lg-12 text-center" ng-show="auditTrailData2.length == 0 && loading == false"><h2>No Data Found</h2></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>SMHS. &copy; 2020 - 2021. All Rights Reserved.</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div1
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="<?php echo base_url() ?>user/logout">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.7.0/lodash.min.js"></script>
<script src="<?php echo base_url() ?>js/angularjs-dropdown-multiselect.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2017.2.621/js/jszip.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment-with-locales.js"></script>
<script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
<script src="<?php echo base_url() ?>js/dirPagination.js"></script> <!-- Pagination link -->
<script type="text/javascript">
                                var userData = <?php echo $userData; ?>;
                                var url_base_path = '<?php echo base_url() ?>';
                                var app = angular.module("reportApp", ['angular.chosen', 'moment-picker', 'angularUtils.directives.dirPagination']);
                                app.controller("reportCtrl", function ($scope, $http, $filter, $sce) {
                                    $scope.auditTrailData = [];
                                    $scope.pageno = 1; // initialize page no to 1
                                    $scope.total_count = 0;
                                    $scope.itemsPerPage = 10; //this could be a dynamic value from a drop down
                                    $scope.user_name = "";
                                    $scope.loading = false;
                                    $scope.start_date = moment().add(-30, 'days').format('DD-MMM-YYYY');
                                    $scope.end_date = moment().format('DD-MMM-YYYY');
                                    
                                    $scope.getUsersList = function () {
                                        $scope.userListData = [];
                                        $http({
                                            url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/getUsersList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.userListData = response.data.user_data;
                                        }, function (error) { // optional
                                            toaster.pop('error', "error", "Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getUsersList();
                                    
                                    
                                    $scope.auditTrailReportData = function (pageno) {
                                        var valid = true;
                                        if (($scope.start_date == "") || ($scope.end_date == "")) {
                                            valid = true;
                                        }

                                        if (valid) {
                                            $scope.loading = true;
                                            
                                            $http({
                                                url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/auditTrailReportList?page_limit=' + $scope.itemsPerPage + "&page_no=" + pageno,
                                                method: "POST",
                                                data: "user_name=" + $scope.user_name + "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.auditTrailData = [];
                                                $scope.auditTrailData = response.data.row_data;
                                                console.log(response.data.row_data);
                                                //console.log($scope.auditTrailData);
                                                $scope.auditTrailData2 = [];
                                                $scope.auditTrailData2 = response.data.row_data2;
                                                //console.log($scope.auditTrailData2);
                                                angular.forEach($scope.auditTrailData, function (value, key) {
                                                    var insertedData = "";
                                                    var updatedData = "";
                                                    var deletedData = "";
                                                    var insertedWorkflowData = "";
                                                    var updatedWorkflowData = "";
                                                    var insertedAccessData = "";
                                                    var updatedAccessData = "";
                                                    var table_fields = JSON.parse(value.table_extra_field)
                                                    if(value.command_type == 'insert' && value.record_type == 'log' && value.workflow_type=='start' && value.approval_status=='N/A'){
                                                        delete table_fields.act_id;
                                                        delete table_fields.done_by_user_id;
                                                        delete table_fields.done_by_user_name;
                                                        delete table_fields.done_by_role_id;
                                                        delete table_fields.done_by_email;
                                                        delete table_fields.done_by_remark;
                                                        delete table_fields.is_in_workflow;
                                                        delete table_fields.updated_by;
                                                        delete table_fields.updated_on;
                                                        delete table_fields.update_remark;
                                                        insertedData = table_fields;
                                                    }else if(value.command_type == 'update' && value.record_type == 'log'){
                                                        delete table_fields.act_id;
                                                        delete table_fields.done_by_user_id;
                                                        delete table_fields.done_by_user_name;
                                                        delete table_fields.done_by_role_id;
                                                        delete table_fields.done_by_email;
                                                        delete table_fields.done_by_remark;
                                                        delete table_fields.is_in_workflow;
                                                        delete table_fields.updated_by;
                                                        delete table_fields.updated_on;
                                                        delete table_fields.update_remark;
                                                        if(value.previous_data!="" && value.previous_data!=null && value.previous_data!=undefined){
                                                            var previous_data_fields = JSON.parse(value.previous_data);
                                                            delete previous_data_fields.act_id;
                                                            delete previous_data_fields.done_by_user_id;
                                                            delete previous_data_fields.done_by_user_name;
                                                            delete previous_data_fields.done_by_role_id;
                                                            delete previous_data_fields.done_by_email;
                                                            delete previous_data_fields.done_by_remark;
                                                            delete previous_data_fields.is_in_workflow;
                                                            delete previous_data_fields.updated_by;
                                                            delete previous_data_fields.updated_on;
                                                            delete previous_data_fields.update_remark;
                                                            var differing = getDifference(table_fields, previous_data_fields);
                                                            console.log(differing);
                                                            if(differing=="" || differing==undefined){
                                                                updatedData = "{Nothing update}";
                                                            }else{
                                                                updatedData = differing;
                                                            }
                                                        }else{
                                                            updatedData = table_fields;
                                                        }
                                                        
                                                    }else if(value.command_type == 'delete' && value.record_type == 'master') {
                                                        //console.log(table_fields);
//                                                        delete table_fields.updated_by;
//                                                        delete table_fields.updated_on;
                                                        deletedData = table_fields;
                                                    }else if(value.command_type == 'insert' && value.record_type == 'master'){
                                                        if(table_fields.hasOwnProperty('created_date')){
                                                             delete table_fields.created_date;
                                                        }
                                                        if(table_fields.hasOwnProperty('created_on')){
                                                             delete table_fields.created_on;
                                                        }
                                                        if(table_fields.hasOwnProperty('created_by')){
                                                             delete table_fields.created_by;
                                                        }
                                                        if(table_fields.hasOwnProperty('modified_on')){
                                                             delete table_fields.modified_on;
                                                        }
                                                        if(table_fields.hasOwnProperty('modified_by')){
                                                             delete table_fields.modified_by;
                                                        }
                                                        if(table_fields.hasOwnProperty('emp_password')){
                                                             delete table_fields.emp_password;
                                                        }
                                                        insertedData = table_fields;
                                                    }else if(value.command_type == 'update' && value.record_type == 'master'){
                                                        if(table_fields.hasOwnProperty('created_date')){
                                                             delete table_fields.created_date;
                                                        }
                                                        if(table_fields.hasOwnProperty('created_on')){
                                                             delete table_fields.created_on;
                                                        }
                                                        if(table_fields.hasOwnProperty('created_by')){
                                                             delete table_fields.created_by;
                                                        }
                                                        if(table_fields.hasOwnProperty('modified_on')){
                                                             delete table_fields.modified_on;
                                                        }
                                                        if(table_fields.hasOwnProperty('modified_by')){
                                                             delete table_fields.modified_by;
                                                        }
                                                        if(table_fields.hasOwnProperty('is_blocked')){
                                                             delete table_fields.is_blocked;
                                                        }
                                                        
                                                        
                                                        console.log(value.master_previous_data);
                                                        if(value.master_previous_data!="" && value.master_previous_data!=null && value.master_previous_data!=undefined){
                                                            var master_previous_data_fields = JSON.parse(value.master_previous_data);
                                                            console.log(master_previous_data_fields+"master");
                                                            if(master_previous_data_fields.hasOwnProperty('created_date')){
                                                                    delete master_previous_data_fields.created_date;
                                                               }
                                                               if(master_previous_data_fields.hasOwnProperty('created_on')){
                                                                    delete master_previous_data_fields.created_on;
                                                               }
                                                               if(master_previous_data_fields.hasOwnProperty('created_by')){
                                                                    delete master_previous_data_fields.created_by;
                                                               }
                                                               if(master_previous_data_fields.hasOwnProperty('modified_on')){
                                                                    delete master_previous_data_fields.modified_on;
                                                               }
                                                               if(master_previous_data_fields.hasOwnProperty('modified_by')){
                                                                    delete master_previous_data_fields.modified_by;
                                                               }
                                                               if(master_previous_data_fields.hasOwnProperty('is_blocked')){
                                                                    delete master_previous_data_fields.is_blocked;
                                                               }
                                                               var differing = getDifference(table_fields, master_previous_data_fields);
                                                               if(differing=="" || differing==undefined){
                                                                    updatedData = "{Nothing update}";
                                                                }else{
                                                                    updatedData = differing;
                                                                }
                                                        }else{
                                                            updatedData = table_fields;
                                                        }
                                                    }
                                                    if(updatedData!=''){
                                                        updatedData = JSON.stringify(updatedData).replace(/[{}]/g, ''); 
                                                    }
                                                    if(insertedData!=''){
                                                        insertedData = JSON.stringify(insertedData).replace(/[{}]/g, ''); 
                                                    }
                                                    if(deletedData!=''){
                                                        deletedData = JSON.stringify(deletedData).replace(/[{}]/g, ''); 
                                                    }
                                                    
                                                    if(value.table_name=="mst_room" || value.table_name=="pts_mst_sys_id"){
                                                        $scope.auditTrailData[key]['room_code'] = table_fields.room_code;
                                                        $scope.auditTrailData[key]['room_name'] = table_fields.room_name;
                                                    }
                                                    if(value.table_name=="pts_trn_workflowsteps" && value.command_type == 'insert'){
                                                        insertedWorkflowData = "New Workflow has been created for "+table_fields[0].activity_name;
                                                        insertedData = "";
                                                    }
                                                    if(value.table_name=="pts_trn_workflowsteps" && value.command_type == 'update'){
                                                        updatedWorkflowData = "Workflow has been updated for "+table_fields[0].activity_name;
                                                        updatedData ='';
                                                    }
                                                    if(value.table_name=="pts_mst_user_mgmt"){
//                                                        var user_name = $filter('UserFilter')($scope.userListData, table_fields[0].user_id);
                                                        if(userData.length>0){
                                                            var user_name = $filter('filter')(userData, function (d) {return d.id === table_fields[0].user_id;})[0].emp_name;
                                                        }else{
                                                            var user_name = table_fields[0].user_id;
                                                        }
                                                        
                                                        insertedAccessData = $sce.trustAsHtml("Access of "+$filter('capitalize')(table_fields[0].module_type) + "<p> Assigned to User ID : "+user_name+"</p>");
                                                        updatedAccessData = "Rights Modified. User Id : "+value.user_name;
                                                        insertedData = "";
                                                        updatedData ='';
                                                    }
                                                    
//                                                    console.log(JSON.parse(value.table_extra_field));
                                                
                                                    $scope.auditTrailData[key]['insertedData'] = insertedData;
                                                    $scope.auditTrailData[key]['updatedData'] = updatedData;
                                                    $scope.auditTrailData[key]['insertedWorkflowData'] = insertedWorkflowData;
                                                    $scope.auditTrailData[key]['updatedWorkflowData'] = updatedWorkflowData;
                                                    $scope.auditTrailData[key]['insertedAccessData'] = insertedAccessData;
                                                    $scope.auditTrailData[key]['updatedAccessData'] = updatedAccessData;
                                                    $scope.auditTrailData[key]['deletedData'] = deletedData;
                                                });

                                                //////////////////////////////////////////////////////////////

                                                angular.forEach($scope.auditTrailData2, function (value, key) {
                                                    var insertedData = "";
                                                    var updatedData = "";
                                                    var deletedData = "";
                                                    var insertedWorkflowData = "";
                                                    var updatedWorkflowData = "";
                                                    var insertedAccessData = "";
                                                    var updatedAccessData = "";
                                                    var table_fields = JSON.parse(value.table_extra_field)
                                                    if(value.command_type == 'insert' && value.record_type == 'log' && value.workflow_type=='start' && value.approval_status=='N/A'){
                                                        delete table_fields.act_id;
                                                        delete table_fields.done_by_user_id;
                                                        delete table_fields.done_by_user_name;
                                                        delete table_fields.done_by_role_id;
                                                        delete table_fields.done_by_email;
                                                        delete table_fields.done_by_remark;
                                                        delete table_fields.is_in_workflow;
                                                        delete table_fields.updated_by;
                                                        delete table_fields.updated_on;
                                                        delete table_fields.update_remark;
                                                        insertedData = table_fields;
                                                    }else if(value.command_type == 'update' && value.record_type == 'log'){
                                                        delete table_fields.act_id;
                                                        delete table_fields.done_by_user_id;
                                                        delete table_fields.done_by_user_name;
                                                        delete table_fields.done_by_role_id;
                                                        delete table_fields.done_by_email;
                                                        delete table_fields.done_by_remark;
                                                        delete table_fields.is_in_workflow;
                                                        delete table_fields.updated_by;
                                                        delete table_fields.updated_on;
                                                        delete table_fields.update_remark;
                                                        if(value.previous_data!="" && value.previous_data!=null && value.previous_data!=undefined){
                                                            var previous_data_fields = JSON.parse(value.previous_data);
                                                            delete previous_data_fields.act_id;
                                                            delete previous_data_fields.done_by_user_id;
                                                            delete previous_data_fields.done_by_user_name;
                                                            delete previous_data_fields.done_by_role_id;
                                                            delete previous_data_fields.done_by_email;
                                                            delete previous_data_fields.done_by_remark;
                                                            delete previous_data_fields.is_in_workflow;
                                                            delete previous_data_fields.updated_by;
                                                            delete previous_data_fields.updated_on;
                                                            delete previous_data_fields.update_remark;
                                                            var differing = getDifference(table_fields, previous_data_fields);
                                                            console.log(differing);
                                                            if(differing=="" || differing==undefined){
                                                                updatedData = "{Nothing update}";
                                                            }else{
                                                                updatedData = differing;
                                                            }
                                                        }else{
                                                            updatedData = table_fields;
                                                        }
                                                        
                                                    }else if(value.command_type == 'delete' && value.record_type == 'master') {
                                                        console.log(table_fields);
//                                                        delete table_fields.updated_by;
//                                                        delete table_fields.updated_on;
                                                        deletedData = table_fields;
                                                    }else if(value.command_type == 'insert' && value.record_type == 'master'){
                                                        if(table_fields.hasOwnProperty('created_date')){
                                                             delete table_fields.created_date;
                                                        }
                                                        if(table_fields.hasOwnProperty('created_on')){
                                                             delete table_fields.created_on;
                                                        }
                                                        if(table_fields.hasOwnProperty('created_by')){
                                                             delete table_fields.created_by;
                                                        }
                                                        if(table_fields.hasOwnProperty('modified_on')){
                                                             delete table_fields.modified_on;
                                                        }
                                                        if(table_fields.hasOwnProperty('modified_by')){
                                                             delete table_fields.modified_by;
                                                        }
                                                        if(table_fields.hasOwnProperty('emp_password')){
                                                             delete table_fields.emp_password;
                                                        }
                                                        insertedData = table_fields;
                                                    }else if(value.command_type == 'update' && value.record_type == 'master'){
                                                        if(table_fields.hasOwnProperty('created_date')){
                                                             delete table_fields.created_date;
                                                        }
                                                        if(table_fields.hasOwnProperty('created_on')){
                                                             delete table_fields.created_on;
                                                        }
                                                        if(table_fields.hasOwnProperty('created_by')){
                                                             delete table_fields.created_by;
                                                        }
                                                        if(table_fields.hasOwnProperty('modified_on')){
                                                             delete table_fields.modified_on;
                                                        }
                                                        if(table_fields.hasOwnProperty('modified_by')){
                                                             delete table_fields.modified_by;
                                                        }
                                                        if(table_fields.hasOwnProperty('is_blocked')){
                                                             delete table_fields.is_blocked;
                                                        }
                                                        
                                                        
                                                        console.log(value.master_previous_data);
                                                        if(value.master_previous_data!="" && value.master_previous_data!=null && value.master_previous_data!=undefined){
                                                            var master_previous_data_fields = JSON.parse(value.master_previous_data);
                                                            console.log(master_previous_data_fields+"master");
                                                            if(master_previous_data_fields.hasOwnProperty('created_date')){
                                                                    delete master_previous_data_fields.created_date;
                                                               }
                                                               if(master_previous_data_fields.hasOwnProperty('created_on')){
                                                                    delete master_previous_data_fields.created_on;
                                                               }
                                                               if(master_previous_data_fields.hasOwnProperty('created_by')){
                                                                    delete master_previous_data_fields.created_by;
                                                               }
                                                               if(master_previous_data_fields.hasOwnProperty('modified_on')){
                                                                    delete master_previous_data_fields.modified_on;
                                                               }
                                                               if(master_previous_data_fields.hasOwnProperty('modified_by')){
                                                                    delete master_previous_data_fields.modified_by;
                                                               }
                                                               if(master_previous_data_fields.hasOwnProperty('is_blocked')){
                                                                    delete master_previous_data_fields.is_blocked;
                                                               }
                                                               var differing = getDifference(table_fields, master_previous_data_fields);
                                                               if(differing=="" || differing==undefined){
                                                                    updatedData = "{Nothing update}";
                                                                }else{
                                                                    updatedData = differing;
                                                                }
                                                        }else{
                                                            updatedData = table_fields;
                                                        }
                                                    }
                                                    if(updatedData!=''){
                                                        updatedData = JSON.stringify(updatedData).replace(/[{}]/g, ''); 
                                                    }
                                                    if(insertedData!=''){
                                                        insertedData = JSON.stringify(insertedData).replace(/[{}]/g, ''); 
                                                    }
                                                    if(deletedData!=''){
                                                        deletedData = JSON.stringify(deletedData).replace(/[{}]/g, ''); 
                                                    }
                                                    
                                                    if(value.table_name=="mst_room" || value.table_name=="pts_mst_sys_id"){
                                                        $scope.auditTrailData2[key]['room_code'] = table_fields.room_code;
                                                        $scope.auditTrailData2[key]['room_name'] = table_fields.room_name;
                                                    }
                                                    if(value.table_name=="pts_trn_workflowsteps" && value.command_type == 'insert'){
                                                        insertedWorkflowData = "New Workflow has been created for "+table_fields[0].activity_name;
                                                        insertedData = "";
                                                    }
                                                    if(value.table_name=="pts_trn_workflowsteps" && value.command_type == 'update'){
                                                        updatedWorkflowData = "Workflow has been updated for "+table_fields[0].activity_name;
                                                        updatedData ='';
                                                    }
                                                    if(value.table_name=="pts_mst_user_mgmt"){
//                                                        var user_name = $filter('UserFilter')($scope.userListData, table_fields[0].user_id);
                                                        if(userData.length>0){
                                                            var user_name = $filter('filter')(userData, function (d) {return d.id === table_fields[0].user_id;})[0].emp_name;
                                                        }else{
                                                            var user_name = table_fields[0].user_id;
                                                        }
                                                        
                                                        insertedAccessData = $sce.trustAsHtml("Access of "+$filter('capitalize')(table_fields[0].module_type) + "<p> Assigned to User ID : "+user_name+"</p>");
                                                        updatedAccessData = "Rights Modified. User Id : "+value.user_name;
                                                        insertedData = "";
                                                        updatedData ='';
                                                    }

                                                    $scope.auditTrailData2[key]['insertedData'] = insertedData;
                                                    $scope.auditTrailData2[key]['updatedData'] = updatedData;
                                                    $scope.auditTrailData2[key]['insertedWorkflowData'] = insertedWorkflowData;
                                                    $scope.auditTrailData2[key]['updatedWorkflowData'] = updatedWorkflowData;
                                                    $scope.auditTrailData2[key]['insertedAccessData'] = insertedAccessData;
                                                    $scope.auditTrailData2[key]['updatedAccessData'] = updatedAccessData;
                                                    $scope.auditTrailData2[key]['deletedData'] = deletedData;
                                                });

                                                $scope.loading = false;
                                                $scope.total_count = response.data.total_count;
                                            }, function (error) { // optional
                                                toaster.pop('error', "error", "Something went wrong.Please try again");
                                            });
                                        } else {
                                            alert("Please Select Room No/Start Date/End Date");
                                        }
                                    }
                                    $scope.auditTrailReportData($scope.pageno);
                                    $scope.resetReportData = function (pageno) {
                                        $scope.pageno = pageno; // initialize page no to 1
                                        $scope.auditTrailReportData($scope.pageno);
                                    }

                                   

                                });
                                app.filter('removeUnderscores', [function() {
                                    return function(string) {
                                        if (!angular.isString(string)) {
                                            return string;
                                        }
                                        return string.replace(/[/_/]/g, ' ');
                                     };
                                    }])
                                app.filter('capitalize', function() {
                                    return function(input) {
                                      return (angular.isString(input) && input.length > 0) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : input;
                                    }
                                   });
                                       // add a custom filter to your module
                                app.filter('UserFilter', function() {
                                    // the filter takes an additional input filterIDs
                                    return function(inputArray, filterIDs) {
                                        // filter your original array to return only the objects that
                                        // have their ID in the filterIDs array
                                        var data = inputArray.filter(function (entry) {
                                            return this.indexOf(entry.id) !== -1;
                                        }, filterIDs);
                                        return data!=""?data[0].emp_name:"--"; // filterIDs here is what "this" is referencing in the line above
                                    };
                                });
                                function ExportPdf() {
                                    $("#audit_trail2").show();
                                    kendo.drawing
                                            .drawDOM("#audit_trail2",
                                                    {
                                                        paperSize: "A4",
                                                        landscape: true,
                                                        sortable: true,
                                                        pageable: true,
                                                        margin: {top: "1cm", bottom: "1cm"},
                                                        scale: 0.5,
                                                        height: 500
                                                    })
                                            .then(function (group) {
                                                kendo.drawing.pdf.saveAs(group, "audit_trail.pdf")
                                            }).always(function(){
                                                $("#pdf_download").hide();
                                                $("#audit_trail2").hide();
                                            });
                                }
                                app.filter('format', function () {
                                    return function (item) {
                                        var t = item.split(/[- :]/);
                                        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                        var time = d.getTime();
                                        return time;
                                    };
                                });
                                function getDifference(o1, o2) {
                                    var diff = {};
                                    var tmp = null;
                                    if (JSON.stringify(o1) === JSON.stringify(o2)) return;

                                    for (var k in o1) {
                                      if (Array.isArray(o1[k]) && Array.isArray(o2[k])) {
                                        tmp = o1[k].reduce(function(p, c, i) {
                                          var _t = getDifference(c, o2[k][i]);
                                          if (_t)
                                            p.push(_t);
                                          return p;
                                        }, []);
                                        if (Object.keys(tmp).length > 0)
                                          diff[k] = tmp;
                                      } else if (typeof(o1[k]) === "object" && typeof(o2[k]) === "object") {
                                        tmp = getDifference(o1[k], o2[k]);
                                        if (tmp && Object.keys(tmp) > 0)
                                          diff[k] = tmp;
                                      } else if (o1[k] !== o2[k]) {
                                        diff[k] = o2[k]+" to "+o1[k]
                                      }
                                    }
                                    return diff;
                                  }
</script>
</body>

</html>
