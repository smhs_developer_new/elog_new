<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'report', "status" => 'active'))->row_array();
$show = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $show = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_edit'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1) {
            $show = true;
        } else {
            $show = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",3000);</script>";
    }
}
$this->db->order_by("id", "DESC");
$footerData = $this->db->get_where("pts_mst_report_footer", array("report_id" => $_GET['module_id'], "status" => "active"))->result_array();
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="smhs" content="">

        <title>E-Logbook</title>

        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url() ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="<?php echo base_url() ?>/css/fontfamily.css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="<?php echo base_url() ?>css/sb-admin-2.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/custom.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
        <style>
            [ng\:cloak],
            [ng-cloak],
            [data-ng-cloak],
            [x-ng-cloak],
            .ng-cloak,
            .x-ng-cloak {
                display: none !important;
            }
        </style>

    </head>

    <body id="page-top" ng-app="reportApp" ng-controller="reportCtrl" ng-cloak>

        <!-- Page Wrapper -->
        <div id="wrapper">
            <?php $this->load->view("template/report/report_header"); ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->

                <div class="content-wrapper">
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="col-auto">
                                <div class="form-row">
                                    <div class="col-lg-3 mb-3"><label><b>Room Name</b> <span style="color: red">*</span></label>

                                        <select class="chzn-select form-control" tabindex="4" name="room_code" data-placeholder="Search Room Code" ng-change="getDispensingBooth()" ng-options="dataObj['room_code'] as (dataObj.room_code +             ' => ' +             dataObj.room_name +             ' => ' +             dataObj.area_id) for dataObj in sysRoomData"  ng-model="room_code" required chosen>
                                            <option value="">Select Room Name</option>
                                        </select>

                                    </div>
                                    <div class="col-lg-3 mb-3"><label>Dispensing Booth No <span style="color: red">*</span></label>
                                        <select class="chosen form-control" tabindex="4"  name="booth_code"  data-placeholder="Search Dispensing Booth No." ng-options="dataObj['equipment_code'] as (dataObj.equipment_name +     ' => ' +     dataObj.equipment_code) for dataObj in dispensingBoothData"  ng-model="booth_code" required chosen>
                                            <option value="">Select Dispensing Booth No.</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 mb-3"><label><b>Start Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-12 ng-isolate-scope" moment-picker="start_date" format="DD-MMM-YYYY" start-view="month" locale="en" today="true" max-date="current_date">

                                                <input ng-selected="getMonthName()" class="form-control ng-pristine ng-valid ng-scope moment-picker-input ng-empty ng-touched" name="start_date" placeholder="Select Start Date" ng-model="start_date" ng-model-options="{updateOn: 'blur'}" tabindex="0" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 mb-3"><label><b>End Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-12 ng-isolate-scope" moment-picker="end_date" format="DD-MMM-YYYY"  start-view="month" min-date="start_date" locale="en" today="true" max-date="current_date">

                                                <input class="form-control  ng-pristine ng-valid ng-scope moment-picker-input ng-empty ng-touched" name="end_date" placeholder="Select End Date" ng-model="end_date" ng-model-options="{updateOn: 'blur'}" tabindex="0" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-lg-2 mb-3"><label>Year</label>
                                        <select class=" form-control" tabindex="4" name="year" data-placeholder="Search year"  ng-model="year" >
                                            <option value="">Select Year</option>
                                    <?php for ($i = 2019; $i <= 2050; $i++) { ?>
                                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 mb-3"><label>Month</label>
                                        <select class=" form-control" tabindex="4" name="month" data-placeholder="Search month"  ng-model="month" >
                                            <option value="">Select Month</option>
                                    <?php
                                    $months = array("Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec");
                                    foreach ($months as $key => $month) {
                                        ?>
                                                        <option value="<?php echo $key + 1; ?>"><?php echo $month; ?></option>
                                    <?php }
                                    ?>
                                        </select>
                                    </div> -->
                                    <div class="col-lg-2 mb-3"><label></label>
                                        <button style="margin-top: 25px;"  class="btn btn-success" ng-click="getHeaderList()">Go</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="card card-default mt-4">
                    <div class="form-row">
                        <div class="col-lg-10 text-center"></div>
                        <?php if ($show) { ?>
                            <div class="col-lg-2" ng-if="prolog.length > 0"><button style="margin-top:5px;"  onclick="ExportPdf()"><i class="fas fa-file-pdf" style="font-size:24px"></i></button></div>
                        <?php } ?>
                    </div>
                    <div class="card-body" id="pressure-report">
                        
                        <!--<div class="form-row">
                            <div class="col-md-2">
                                <img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo">

                            </div>
                            <div class="col-md-10">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th><b>TITLE</b></th>
                                            <td colspan="4">Swab Sample</td>
                                        </tr>
                                        <tr>
                                        <tr>
                                            <th>Form No.</th>
                                            <th>Version No. &amp; Status</th>
                                            <th>Effective Date</th>
                                            <th>Retired Date</th>
                                            <th>Document Reference No.</th>
                                        </tr>
                                    </tbody>
                                    <tbody>
                                        <tr>
                                            <td>{{headerData.length > 0?reportHeader.form_no:'NA'}}</td>
                                            <td>{{headerData.length > 0?reportHeader.version_no+' & Effective':'NA'}} </td>
                                            <td>{{headerData.length > 0?(reportHeader.effective_date| date:'dd-MMM-yyyy'):'NA'}}</td>
                                            <td>{{headerData.length > 0?(reportHeader.retired_date| date:'dd-MMM-yyyy'):'NA'}}</td>
                                            <td>{{headerData.length > 0?reportHeader.document_no:'NA'}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>-->
                        <div class="form-row">
                            <div class="col-md-2">
                                <img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo">

                            </div>
                            <div class="col-lg-10">
                                <h2 class="text-center">LAF PRESSURE DIFFERENTIAL RECORD SHEET</h2>
                                <p style="text-align: right; padding-right: 150px;"><b>MONTH</b> : {{month}}<span ng-if="month != '' && year != ''">-</span>{{year}}</p>
                                <p style="text-align: right; padding-right: 150px;"><b>DISPENSING BOOTH NO.</b> : {{booth_code}} </p>


                            </div>
                        </div>

                        <div class="form-row scroll-table" style="margin-top:10px;">
                            <table class="table custom-table">
                                <thead style="border-top:10px">
                                    <tr>
                                        <th>DATE </th>
                                        <th>LAF START TIME</th>
                                        <th class="text-center" colspan="2">LIMIT IN(MM OF WC)</th>
                                        <th class="text-center" colspan="3">LIMIT IN(MM OF WC)</th>
                                        <th>SIGN WH TECH.</th>
                                        <th>CHECKED BY</th>
                                        <th>REMARKS</th>
                                        <th>ATTACHMENTS</th>
                                    </tr>
                                    <tr><th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th class="text-center">M-I (5 to 15)</th>
                                        <th class="text-center">M-II (0 to -5)</th>
                                        <th class="text-center">G-I (1.0 to 3)</th>
                                        <th class="text-center">G-II (1.5 to 5)</th>
                                        <th class="text-center">G-III (8 to 14)</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody >
                                    <tr ng-repeat="dataObj in prolog">
                                        <td>{{dataObj.startdate}}</td>
                                        <td>{{dataObj.starttime}}</td>
                                        <td>{{dataObj.m1}}</td>
                                        <td>{{dataObj.m2}}</td>
                                        <td>{{dataObj.g1}}</td>
                                        <td>{{dataObj.g2}}</td>
                                        <td>{{dataObj.g3}}</td>
                                        <td>{{dataObj.startoperator}}<br>({{dataObj.startremark}})</td>                                    
                                        <td>{{dataObj.stopcheckedby}}<br>({{dataObj.stopcheckedbyremark}})</td>
                                        <td><p ng-repeat="remarkObj in dataObj.remark track by $index">{{remarkObj}}</p><br></td>     
                                        <td><p ng-if="dataObj.logAttachments.length>0" ng-repeat="attachObj in dataObj.logAttachments track by $index"> <a href="<?php echo base_url() ?>assets/files/{{attachObj.act_id}}/{{attachObj.file_name}}" target="_blank"  >{{attachObj.file_name}}</a></p><br></td> 
                                    </tr>

                                </tbody>


                            </table>
                            
                            <div class="col-lg-12 text-center" ng-show="prolog.length == 0">
                                <h2>No Data Found</h2>
                            </div>
                            <div class="col-lg-12 text-center" ng-show="prolog.length == 0">
                                <h4>(Please Select the above parameters)</h4>
                            </div>
                        </div>
                        <div class="form-row" style="margin-top:10px;" ng-show="headerData.length > 0">
                            <b>FORM NO.</b>&nbsp;{{headerData.length > 0?reportHeader.footer_name:'NA'}}
                        </div>
                        <div class="form-row pb-2" id="pdf_download" style="display:none">
                            <div class="col-md-3"></div>
                            <div class="col-md-3">Print By : <?php echo $this->session->userdata('empname') ?></div>
                            <div class="col-md-3">Date : <?php echo date("d-M-Y"); ?></div>
                            <div class="col-md-3">Time : <?php echo date("H:i:s"); ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>SMHS. &copy; 2020 - 2021. All Rights Reserved.</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div> <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="<?php echo base_url() ?>user/logout">Logout</a>
            </div>
        </div>
    </div>
</div>
<script type="text/kendo-template" id="page-template">
    #
        var footer_name = foot_name;
    #
    <div class="page-template">
        <div class="footer">
            <table class="table">
                <tr>
                    <td>
                        <div class="form-row" style="margin-top:10px;">
                            <b>FORM NO.</b>&nbsp;<div id="footer_no1">#=footer_name#</div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"><b>Print By : <?php echo $this->session->userdata('empname') ?></b></div>
                            <div class="col-md-3"><b>Date : <?php echo date("d-M-Y"); ?></b></div>
                            <div class="col-md-3"><b>Time :<?php $timezone = "Asia/Kolkata";date_default_timezone_set($timezone);echo date("H:i:s");?></b></div>
                            <div class="col-md-3"><b>Page #: pageNum # of #: totalPages #</b></div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</script>
<div id="master_data_div" style="display: none;">
    <div class="card-body ht750" id="audit_trail2" style="margin-top:10px;padding-top:10px">                      
	<div class="form-row scroll-table" style="margin-top:10px;">
		<style>
			#tbldata {border-collapse:collapse; table-layout:fixed; width:310px; height: 100px !important;}
			#tbldata td {width:100px;word-wrap:break-word; height:100px;font-size: 15px;}
		</style>
		<table id="tbldata" border="1" style="width:100%;color:black;">
			<thead style="max-height:165px;">
				<tr style="max-height:80px;">
					<td colspan="11">
                                            <div class="form-row">
                                                <div class="col-md-2"><img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo"></div>
                                                <div class="col-lg-10">
                                                    <h2 class="text-center">LAF PRESSURE DIFFERENTIAL RECORD SHEET</h2>
                                                    <p style="text-align: right; padding-right: 150px;"><b>MONTH</b> : {{month}}<span ng-if="month != '' && year != ''">-</span>{{year}}</p>
                                                    <p style="text-align: right; padding-right: 150px;"><b>DISPENSING BOOTH NO.</b> : {{booth_code}} </p>
                                                </div>
                                            </div>
					</td>
				</tr>
				<tr style="background-color:#4e73df;color:#fff;height:60px;">
					<th>DATE </th>
					<th>LAF START TIME</th>
					<th class="text-center" colspan="2">LIMIT IN(MM OF WC)</th>
					<th class="text-center" colspan="3">LIMIT IN(MM OF WC)</th>
					<th>SIGN WH TECH.</th>
					<th>CHECKED BY</th>
					<th>REMARKS</th>
					<th>ATTACHMENTS</th>
				</tr>
				<tr style="background-color:#4e73df;color:#fff;height:40px;">
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th class="text-center">M-I (5 to 15)</th>
					<th class="text-center">M-II (0 to -5)</th>
					<th class="text-center">G-I (1.0 to 3)</th>
					<th class="text-center">G-II (1.5 to 5)</th>
					<th class="text-center">G-III (8 to 14)</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody >
				<tr style="max-height: 100px" ng-repeat="dataObj in prolog">
					<td>{{dataObj.startdate}}</td>
					<td>{{dataObj.starttime}}</td>
					<td>{{dataObj.m1}}</td>
					<td>{{dataObj.m2}}</td>
					<td>{{dataObj.g1}}</td>
					<td>{{dataObj.g2}}</td>
					<td>{{dataObj.g3}}</td>
					<td>{{dataObj.startoperator}}<br>({{dataObj.startremark}})</td>                                    
					<td>{{dataObj.stopcheckedby}}<br>({{dataObj.stopcheckedbyremark}})</td>
					<td><p ng-repeat="remarkObj in dataObj.remark track by $index">{{remarkObj}}</p><br></td>     
					<td><p ng-if="dataObj.logAttachments.length>0" ng-repeat="attachObj in dataObj.logAttachments track by $index"> <a href="<?php echo base_url() ?>assets/files/{{attachObj.act_id}}/{{attachObj.file_name}}" target="_blank"  >{{attachObj.file_name}}</a></p><br></td>
				</tr>
			</tbody>
		</table>
	</div>
    </div>
</div>
<style>
    /* Page Template for the exported PDF */
    .page-template {
      font-family: "DejaVu Sans", "Arial", "sans-serif";
      position: absolute;
      width: 100%;
      height: 100%;
      color: black;
      top: 0;
      left: 0;
    }
    .page-template .header {
      position: absolute;
      top: 0px;
      left: 30px;
      right: 30px;
      color: black;
      bottom:20px;
      margin-bottom: 10px;
      padding-bottom: 10px;
    }
    .page-template .footer {
      position: absolute;
      bottom: 0px;
      left: 30px;
      right: 30px;
      border-top: 1px solid black;
      text-align: center;
      color: #000000;
    }
    .k-grid-header .k-header {
        height: 10px;
        padding: 0;
      }

      .k-grid tbody tr {
        line-height: 14px !important;
      }

      .k-grid tbody td {
        padding: 0;
      }
</style>
<!-- Bootstrap core JavaScript-->

<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>js/lodash.min.js"></script>
<script src="<?php echo base_url() ?>js/angularjs-dropdown-multiselect.min.js"></script>
<script src="<?php echo base_url() ?>js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>js/kendo.all.min.js"></script>
<script src="<?php echo base_url() ?>js/moment-with-locales.js"></script>
<script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
<script type="text/javascript">
                            var foot_name = '';
                            var url_base_path = '<?php echo base_url() ?>';
                            var app = angular.module("reportApp", ['angular.chosen', 'moment-picker']);
                            app.directive('chosen', function ($timeout) {

                                var linker = function (scope, element, attr) {

                                    scope.$watch('sysRoomData', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    scope.$watch('dispensingBoothData', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    $timeout(function () {
                                        element.chosen();
                                    }, 0, false);
                                };
                                return {
                                    restrict: 'A',
                                    link: linker
                                };
                            });
                            app.controller("reportCtrl", function ($scope, $http, $filter) {
                                $scope.monthData = {"1": "Jan", "2": "Feb", "3": "Mar", "4": "Apr", "5": "May", "6": "June", "7": "July", "8": "Aug", "9": "Sep", "10": "Oct", "11": "Nov", "12": "Dec"};
                                // checking header
                                $scope.headerData = [];
                                $scope.reportHeader = {};
                                $scope.current_date = moment().format('DD-MMM-YYYY');
                                $scope.start_date = moment().format('DD-MMM-YYYY');
                                $scope.end_date = moment().format('DD-MMM-YYYY');
                                var abc = moment(moment($scope.start_date, 'DD-MMM-YYYY')).format('YYYY-MM-DD');
                                $scope.month = moment(abc).format('MMM');
                                $scope.year = moment(abc).format('YYYY');
                                $scope.getMonthName = function () {
                                    $scope.month = moment(abc).format('MMM');
                                    $scope.year = moment(abc).format('YYYY');
                                    if (!$scope.dateRangeCheck($scope.start_date, $scope.end_date)) {
                                        $scope.month = $scope.year = '';
                                    }
                                }
                                $scope.getHeaderList = function () {
                                    if ($scope.start_date == "" || $scope.start_date == undefined || $scope.end_date == "" || $scope.end_date == undefined)
                                    {
                                        alert("Please Select the date range first...!");
                                        return false;
                                    } else if (!$scope.dateRangeCheck($scope.start_date, $scope.end_date)) {
                                        alert('Date Range Should Be of Same Month');
                                        return false;
                                    }
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getHeaderByLogid',
                                        method: "POST",
                                        data: "header_id=" + <?php echo $_GET['mst_act_id'] ?> + "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date,
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.headerData = response.data.header_list;
                                        if ($scope.headerData.length > 0)
                                        {
                                            if ($scope.headerData.length > 1)
                                            {
                                                alert("There are more than one header in this date range please change it, Thank you...!");
                                                $scope.headerData = [];
                                                return false;
                                            } else
                                            {
                                                $scope.reportHeader = response.data.header_list[0];
                                                foot_name = $scope.reportHeader.footer_name;
                                                $scope.pressureDiffReportData();
                                            }
                                        } else
                                        {
                                            alert("No version is effective in selected date range, please change the dates or check with administrator for \"Report Header/Footer Master\"");
                                            $scope.headerData = [];
                                            return false;
                                        }
                                    }, function (error) { // optional
                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                //End
                                $scope.sysRoomData = [];
                                $scope.getDeviceRoomList = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.sysRoomData = response.data.room_list;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.getDeviceRoomList();
                                $scope.prolog = [];
                                $scope.dispensingBoothData = [];
                                $scope.dateRangeCheck = function (start_date, end_date) {
                                    var result = false;
                                    var a = new Date(start_date);
                                    var b = new Date(end_date);
                                    var start_year = a.getFullYear();
                                    var start_month = a.getMonth();
                                    var end_year = b.getFullYear();
                                    var end_month = b.getMonth();
                                    if (start_year == end_year && start_month == end_month) {
                                        result = true;
                                    }
                                    return result;
                                }
                                $scope.getDispensingBooth = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetDispensingBoothList?room_code=' + $scope.room_code,
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.dispensingBoothData = response.data.booth_list;
                                    }, function (error) { // optional

                                        console.log("Something went wrong.Please try again");

                                    });
                                }
                                $scope.pressureDiffData = [];
                                $scope.pressureDiffReportData = function () {
                                    var valid = true;
                                    if (($scope.booth_code == "" || $scope.booth_code == undefined) && ($scope.start_date == "" || $scope.start_date == undefined) && ($scope.end_date == "" || $scope.end_date == undefined)) {
                                        valid = false;
                                    }
                                    if (valid) {
                                        $scope.prolog = [];
                                        $scope.pressureDiffData = [];
                                        $http({
                                            url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/lafPressureDiffReport',
                                            method: "POST",
                                            data: "booth_no=" + $scope.booth_code + "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.pressureDiffData = response.data.row_data;
                                            var startoperator = "";
                                            var startremark = "";
                                            var startdate = "";
                                            var starttime = "";
                                            var stopoperator = "";
                                            var stopremark = "";
                                            var stopdate = "";
                                            var stoptime = "";
                                            var startcheckedby = "";
                                            var startcheckedbyremark = "";
                                            var stopcheckedby = "";
                                            var stopcheckedbyremark = "";
                                            var isworkflow = "no";
                                            var remark = "";
                                            var logAttachments = "";
                                            var qa = 'NA';
                                            var qaremark = 'NA';
                                            angular.forEach($scope.pressureDiffData, function (value, key) {

                                                if (value['workflow_type'] == 'start')
                                                {
                                                    if (value['approval_status'] == 'N/A')
                                                    {
                                                        startoperator = value['user_name'];
                                                        startdate = value['start_date'];
                                                        starttime = value['start_time'];
                                                        isworkflow = "no";
                                                        startremark = value['activity_time'];
                                                        remark = value['user_name'] + ':-' + value['activity_remarks'];
                                                    } else
                                                    {
                                                        startcheckedby = value['user_name'];
                                                        isworkflow = "yes";
                                                        startcheckedbyremark = value['activity_time'];
                                                        remark = remark + '~' + value['user_name'] + ':-' + value['activity_remarks'];
                                                    }
                                                } else if (value['workflow_type'] == 'stop')
                                                {
                                                    if (value['approval_status'] == 'N/A')
                                                    {
                                                        startoperator = value['user_name'];
                                                        startdate = value['start_date'];
                                                        starttime = value['start_time'];
                                                        isworkflow = "no";
                                                        stopremark = value['activity_time'];
                                                        remark = remark + "~" + value['user_name'] + ':-' + value['activity_remarks'];
                                                        startremark = value['activity_time'];
                                                        remark = value['user_name'] + ':-' + value['activity_remarks'];
                                                        stopcheckedby = "";
                                                        stopcheckedbyremark = "";
                                                        var tempStr = remark.split("~");
                                                        var uniqueNames = getUnique(tempStr);
                                                        qa = 'NA';
                                                        qaremark = 'NA';
                                                        var temp = {"startdate": startdate, "starttime": starttime, "stopdate": stopdate, "stoptime": stoptime, "activity": value['activity_name'], "startoperator": startoperator, "startremark": startremark, "startcheckedby": startcheckedby, "startcheckedbyremark": startcheckedbyremark, "stopoperator": stopoperator, "stopremark": stopremark, "stopcheckedby": stopcheckedby, "stopcheckedbyremark": stopcheckedbyremark, "remark": uniqueNames, "m1": value['m1'], "m2": value['m2'], "g1": value['g1'], "g2": value['g2'], "g3": value['g3'], "logAttachments": value['logAttachments'], "qa": qa, "qaremark": qaremark};
                                                        $scope.prolog[value.id] = temp;
                                                    } else if (value['approval_status'] == 'QAapprove') {
                                                        qa = value['user_name'];
                                                        qaremark = value['activity_time'];
                                                        remark = remark + "~" + value['user_name'] + ':-' + value['activity_remarks'];
                                                        var tempStr = remark.split("~");
                                                        var uniqueNames = getUnique(tempStr);
                                                        var temp = {"startdate": startdate, "starttime": starttime, "stopdate": stopdate, "stoptime": stoptime, "activity": value['activity_name'], "startoperator": startoperator, "startremark": startremark, "startcheckedby": startcheckedby, "startcheckedbyremark": startcheckedbyremark, "stopoperator": stopoperator, "stopremark": stopremark, "stopcheckedby": stopcheckedby, "stopcheckedbyremark": stopcheckedbyremark, "remark": uniqueNames, "m1": value['m1'], "m2": value['m2'], "g1": value['g1'], "g2": value['g2'], "g3": value['g3'], "logAttachments": value['logAttachments'], "qa": qa, "qaremark": qaremark};
                                                        $scope.prolog[value.id] = temp;
                                                    } else
                                                    {
                                                        stopcheckedby = value['user_name'];
                                                        isworkflow = "yes";
                                                        stopcheckedbyremark = value['activity_time'];
                                                        remark = remark + "~" + value['user_name'] + ':-' + value['activity_remarks'];
                                                        logAttachments = value['logAttachments'];
                                                        var tempStr = remark.split("~");
                                                        var uniqueNames = getUnique(tempStr);
                                                        if (value['approval_status'] == 'QAapprove') {
                                                            qa = value['user_name'];
                                                            qaremark = value['activity_time'];
                                                        }
                                                        var temp = {"startdate": startdate, "starttime": starttime, "stopdate": stopdate, "stoptime": stoptime, "activity": value['activity_name'], "startoperator": startoperator, "startremark": startremark, "startcheckedby": startcheckedby, "startcheckedbyremark": startcheckedbyremark, "stopoperator": stopoperator, "stopremark": stopremark, "stopcheckedby": stopcheckedby, "stopcheckedbyremark": stopcheckedbyremark, "remark": uniqueNames, "m1": value['m1'], "m2": value['m2'], "g1": value['g1'], "g2": value['g2'], "g3": value['g3'], "logAttachments": value['logAttachments'], "qa": qa, "qaremark": qaremark};
                                                        $scope.prolog[value.id] = temp;
                                                    }
                                                } else
                                                {
                                                    if (isworkflow == "no") {
                                                        startcheckedby = "";
                                                        stopcheckedby = "";
                                                    }
                                                    if (value['approval_status'] == 'QAapprove') {
                                                        qa = value['user_name'];
                                                        qaremark = value['activity_time'];
                                                    }
                                                    remark = remark + "~" + value['user_name'] + ':-' + value['activity_remarks'];
                                                    logAttachments = value['logAttachments'];
                                                    var tempStr = remark.split("~");
                                                    var uniqueNames = getUnique(tempStr);
                                                    var temp = {"startdate": startdate, "starttime": starttime, "stopdate": stopdate, "stoptime": stoptime, "activity": value['activity_name'], "startoperator": startoperator, "startremark": startremark, "startcheckedby": startcheckedby, "startcheckedbyremark": startcheckedbyremark, "stopoperator": stopoperator, "stopremark": stopremark, "stopcheckedby": stopcheckedby, "stopcheckedbyremark": stopcheckedbyremark, "remark": uniqueNames, "m1": value['m1'], "m2": value['m2'], "g1": value['g1'], "g2": value['g2'], "g3": value['g3'], "logAttachments": value['logAttachments'], "qa": qa, "qaremark": qaremark};
                                                    $scope.prolog[value.id] = temp;
                                                }
                                            });
                                            $scope.prolog = $scope.prolog.filter(function () {
                                                return true
                                            });
                                            //console.log($scope.prolog);
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");

                                        });
                                    } else {

                                        alert("Please Select Booth no/Year/Month");

                                    }
                                }
                                function getUnique(array) {
                                    var uniqueArray = [];

                                    // Loop through array values
                                    for (i = 0; i < array.length; i++) {
                                        if (uniqueArray.indexOf(array[i]) === -1) {
                                            uniqueArray.push(array[i]);
                                        }
                                    }
                                    return uniqueArray;
                                }

                            });
                            function ExportPdf() {
                                $("#master_data_div").show();
                                kendo.drawing.drawDOM("#master_data_div",{
                                    paperSize: "A3",
                                    landscape: true,
                                    pageable: true,
                                    repeatHeaders:true,
                                    template: $("#page-template").html(),
                                    margin: {top: "100px", bottom: "10px"},
                                    color:"black",
                                    //padding: {top: "40px", bottom: "40px"},
                                    scale: 0.7,
                                    height: 800
                                }).then(function (group) {
                                    kendo.drawing.pdf.saveAs(group,"laf_pressure_differential.pdf")
                                }).always(function () {
                                    //$("#pdf_download").hide();
                                    $("#master_data_div").hide();
                                });
                            }
</script>
<script src="<?php echo base_url(); ?>assets/js/disable_editor.js"></script>
</body>

</html>