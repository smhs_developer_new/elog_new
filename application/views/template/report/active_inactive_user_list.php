<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'report', "status" => 'active'))->row_array();
$show = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $show = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_edit'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1) {
            $show = true;
        } else {
            $show = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",3000);</script>";
    }
}

$this->db->order_by("id", "DESC");
$footerData = $this->db->get_where("pts_mst_report_footer", array("report_id" => $_GET['module_id'], "status" => "active"))->result_array();
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="smhs" content="">

        <title>E-Logbook</title>

        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url() ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>css/fontfamily.css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="<?php echo base_url() ?>css/sb-admin-2.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/custom.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
        <style>
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
        </style>

    </head>

    <body id="page-top" ng-app="userReportApp" ng-controller="userReportCtrl" ng-cloak>

        <!-- Page Wrapper -->
        <div id="wrapper">

            <?php $this->load->view("template/report/report_header"); ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->

                <div class="content-wrapper">
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="col-auto">
                                <div class="form-row scroll-table">
                                    <div class="col-lg-6 mb-3"><label><b>User List Status</b></label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="radio" name="status" ng-click="unsetuserData()" ng-model="status" value="active" required> <label class="pll5">Active </label>
                                            <input type="radio" name="status" ng-click="unsetuserData()"  ng-model="status" value="inactive"><label class="pll5">In Active</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- <div class="col-lg-2 mb-3"><label></label>
                                    <button style="margin-top: 30px;"  class="btn btn-success" ng-click="getProcesslogDetails('date-filter')">Filter Report</button>

                                </div>
                                <div class="col-lg-2 mb-3"><label></label><button style="margin-top: 10px;" ng-show="prolog.length > 0" class="btn btn-success" onclick="ExportPdf()">export to pdf</button></div> -->
                            </div>
                            <div class="col-auto">
                                <div class="form-row">
                                    <div class="col-lg-2 mb-3"><label><b>From Date</b><span style="color: red">*</span></label>
                                        <div class="row">
                                            <div class="input-group col-md-12 ng-isolate-scope" moment-picker="start_date" format="DD-MMM-YYYY" start-view="month" locale="en" today="true" max-date="current_date">

                                                <input class="form-control ng-pristine ng-valid ng-scope moment-picker-input ng-empty ng-touched" name="start_date" placeholder="Select From Date" ng-model="start_date" ng-model-options="{updateOn: 'blur'}" tabindex="0" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 mb-3"><label><b>To Date</b><span style="color: red">*</span></label>
                                        <div class="row">
                                            <div class="input-group col-md-12 ng-isolate-scope" moment-picker="end_date" format="DD-MMM-YYYY" start-view="month" min-date="start_date" locale="en" today="true" max-date="current_date">

                                                <input class="form-control  ng-pristine ng-valid ng-scope moment-picker-input ng-empty ng-touched" name="end_date" placeholder="Select To Date" ng-model="end_date" ng-model-options="{updateOn: 'blur'}" tabindex="0" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label><b>Plant</b> <span style="color: red">*</span></label>

                                        <select class="chzn-select form-control" tabindex="4" name="plant_code" data-placeholder="Search Plant Code" ng-change="getBlockList()" ng-options="dataObj['plant_code'] as (dataObj.plant_code +                 ' => ' +                 dataObj.plant_name) for dataObj in plantData"  ng-model="plant_code" required chosen>
                                            <option value="">Select Plant Name</option>
                                        </select>

                                    </div>
                                    <div class="col-lg-3 mb-3"><label><b>Block</b> <span style="color: red">*</span></label>
                                        <select class="chzn-select form-control" tabindex="4"  name="block_code" data-placeholder="Search Block" ng-chage="hideUserData()" ng-options="dataObj['block_code'] as (dataObj.block_code +                 ' => ' +                 dataObj.block_name) for dataObj in blockData"  ng-model="block_code" required chosen>
                                            <option value="">Select Block</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 mb-3"><label></label>
                                        <button class="btn btn-success" style="margin-top: 24px;" ng-click="getUserList('date-filter')">Go</button>
                                    </div>
                                </div>
                                <!-- <div class="col-lg-2 mb-3"><label></label>
                                    <button style="margin-top: 30px;"  class="btn btn-success" ng-click="getProcesslogDetails('date-filter')">Filter Report</button>

                                </div>
                                <div class="col-lg-2 mb-3"><label></label><button style="margin-top: 10px;" ng-show="prolog.length > 0" class="btn btn-success" onclick="ExportPdf()">export to pdf</button></div> -->
                            </div>
                        </div>
                    </div>
                </div>




                <div class="card card-default mt-4">
                    <div class="form-row">
                        <div class="col-lg-10 text-center"></div>
                        <?php if ($show) { ?>
                            <div class="col-lg-2" ng-if="userData.length > 0"><button style="margin-top:5px;"  onclick="ExportPdf()"><i class="fas fa-file-pdf" style="font-size:24px"></i></button></div>
                        <?php } ?>
                    </div>
                    

                    <!--            <div class="form-row">
                                            <div class="col-lg-10 text-center"></div>
                                               
                                            <div class="col-lg-2" ng-if="prolog.length > 0" style="margin-top:5px;"><button  ng-click="printToCart('process-log-report')"><span style="font-size:24px" class="glyphicon glyphicon-print"></span></button>&nbsp;&nbsp;<button  onclick="ExportPdf()"><i class="fas fa-file-pdf" style="font-size:24px"></i></button></div>
                                        </div>-->
                    <div class="card-body" id="process-log-report">
                        <div class="form-row scroll-table" style="margin-top:10px;">
                            <table class="table custom-table tableP runningacttable">
                                <thead>
                                    <tr>
                                        <td colspan="{{column_width}}">
                                            <div class="form-row">
                                                <div class="col-lg-4"></div>
                                                <div class="col-lg-4 text-center"><h2>Active/Inactive User List</h2></div>
                                            </div>
                                            <div class="form-row">
                                                    <table class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td>Date</td>
                                                                <td>{{start_date| date : "dd-MMM-y"}}&nbsp;&nbsp;{{end_date!=""?"To":''}}&nbsp;&nbsp;&nbsp;{{end_date| date : "dd-MMM-y"}}</td>
                                                                <td>Print Date</td>
                                                                <td><?php echo date('d-M-Y')?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Plant</td>
                                                                <td>{{plant_code}}</td>
                                                                <td>Print Time</td>
                                                                <td><?php date_default_timezone_set('Asia/Kolkata'); echo date('H:i')?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Block</td>
                                                                <td>{{block_code}}</td>
                                                                <td>User Id</td>
                                                                <td><?php echo $_SESSION['empname'];?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                            </div>
                                            <div class="form-row">
                                                <div class="col-md-12">
                                                    <div class="form-row">
                                                        <div class="col-lg-12 text-center"><h2><b>User Details</b></h2></div>

                                                    </div>

                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>User Id </th>
                                        <th>Emp Code</th>
                                        <th>Emp Name</th>
                                        <th>Creation Date</th>
                                        <th>Role</th>
                                        <th>Designation</th>
                                        <th ng-if="status == 'inactive'">Deactivate Date</th>
                                        <th>Email Id</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="dataObj in userData">
                                        <td>{{dataObj.emp_email}}</td>
                                        <td>{{dataObj.emp_code}}</td>
                                        <td>{{dataObj.emp_name}}</td>
                                        <td>{{dataObj.created_on | date:'dd-MMM-yyyy'}}</td>
                                        <td>{{dataObj.role_description}}</td>
                                        <td>{{dataObj.designation_code}}</td>
                                        <td ng-if="status == 'inactive'">{{dataObj.modified_on | format | date:'d-MMM-yyyy'}}</td>
                                        <td>{{dataObj.email2!=""?dataObj.email2:'NA'}}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="col-lg-12 text-center" ng-show="userData.length == 0"><h2>No Data Found</h2></div>
                            <div class="col-lg-12 text-center" ng-show="userData.length == 0"><h4>(Please Select the above parameters)</h4></div>
                        </div>
                    </div>
                </div>






            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>SMHS. &copy; 2020 - 2021. All Rights Reserved.</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="<?php echo base_url() ?>user/logout">Logout</a>
            </div>
        </div>
    </div>
</div>
<script type="text/kendo-template" id="page-template">
    <div class="page-template">
        <div class="footer">
            <table class="table">
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-md-3"><b>Page #: pageNum # of #: totalPages #</b></div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</script>
<style>
    /* Page Template for the exported PDF */
    .page-template {
      font-family: "DejaVu Sans", "Arial", "sans-serif";
      position: absolute;
      width: 100%;
      height: 100%;
      color: black;
      top: 0;
      left: 0;
    }
    .page-template .header {
      position: absolute;
      top: 0px;
      left: 30px;
      right: 30px;
      color: black;
      bottom:20px;
      margin-bottom: 10px;
      padding-bottom: 10px;
    }
    .page-template .footer {
      position: absolute;
      bottom: 0px;
      left: 30px;
      right: 30px;
      border-top: 1px solid black;
      text-align: center;
      color: #000000;
    }
    .k-grid-header .k-header {
        height: 10px;
        padding: 0;
    }

      .k-grid tbody tr {
        line-height: 14px !important;
      }

      .k-grid tbody td {
        padding: 0;
      }
</style>
<!-- Bootstrap core JavaScript-->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>js/lodash.min.js"></script>
<script src="<?php echo base_url() ?>js/angularjs-dropdown-multiselect.min.js"></script>
<script src="<?php echo base_url() ?>js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>js/kendo.all.min.js"></script>
<script src="<?php echo base_url() ?>js/moment-with-locales.js"></script>
<script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
<script src="<?php echo base_url() ?>js/dirPagination.js"></script> 
<script type="text/javascript">
                                
                                var app = angular.module("userReportApp", ['angular.chosen', 'moment-picker']);
                                app.directive('chosen', function ($timeout) {

                                    var linker = function (scope, element, attr) {

                                        scope.$watch('plantData', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);
                                        scope.$watch('blockData', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);
                                        $timeout(function () {
                                            element.chosen();
                                        }, 0, false);
                                    };
                                    return {
                                        restrict: 'A',
                                        link: linker
                                    };
                                });
                                app.controller("userReportCtrl", function ($scope, $http, $filter) {
                                    $scope.plantData = [];
                                    $scope.column_width =7;
                                    $scope.current_date = moment().format('DD-MMM-YYYY');
                                    $scope.start_date = moment().format('DD-MMM-YYYY');
                                    $scope.end_date = moment().format('DD-MMM-YYYY');
                                    $scope.hideUserData = function(){
                                        $scope.userData = [];
                                    }
                                    $scope.getPlantList = function () {
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getPlantList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.plantData = response.data.plant_list;
                                        }, function (error) { // optional

                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getPlantList();
                                    $scope.blockData = [];
                                    $scope.plant_code="";
                                    $scope.getBlockList = function () {
                                        $scope.block_code = "";
                                        $scope.blockData = [];
                                        $scope.hideUserData();
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getBlockList?plant_code=' + $scope.plant_code,
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.blockData = response.data.block_data;
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");

                                        });
                                    }
                                    $scope.getBlockList();
                                    $scope.status = "active";
                                   
                                    $scope.plant_code = "";
                                    $scope.block_code = "";
                                    $scope.userData = [];
                                    $scope.unsetuserData = function () {
                                        if($scope.status!='active'){
                                            $scope.column_width =8;
                                        }
                                        //$scope.start_date = "";
                                        //$scope.end_date = "";
                                        $scope.plant_code = "";
                                        $scope.block_code = "";
                                        $scope.userData = [];
                                    }
                                    $scope.getUserList = function () {
                                        var valid = true;
                                        
                                            if ($scope.start_date == "") {
                                                alert("Please select From Date");
                                                valid = false;
                                                return false;
                                            } else if ($scope.end_date == "") {
                                                alert("Please select To Date");
                                                valid = false;
                                                return false;
                                            } else if ($scope.plant_code == "") {
                                                alert("Please select Plant Code");
                                                valid = false;
                                                return false;
                                            } else if ($scope.block_code == "") {
                                                alert("Please select Block Code");
                                                valid = false;
                                                return false;
                                            } 

                                        if (valid) {
                                            $scope.userData = [];
                                            var datefilter = '';
                                            if (($scope.start_date != "" && $scope.start_date != undefined) && ($scope.end_date != "" && $scope.end_date != undefined)) {
                                                datefilter = "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date;
                                            }
                                            $http({
                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getUserList',
                                                method: "POST",
                                                data: "plant_code=" + $scope.plant_code + "&block_code=" + $scope.block_code + "&type=" + $scope.status + datefilter,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.userData = response.data.user_list;
                                                //console.log($scope.userData);
                                            }, function (error) { // optional
                                                console.log("Something went wrong.Please try again");

                                            });
                                    }
                                    }
                                    $scope.print = false;
                                    
                                });
                                app.filter('format', function () {
                                    return function (item) {
                                        var t = item.split(/[- :]/);
                                        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                        var time = d.getTime();
                                        return time;
                                    };
                                });
                                function ExportPdf() {
                                    kendo.drawing
                                            .drawDOM("#process-log-report",
                                                    {
                                                        paperSize: "A3",
                                                        landscape: true,
                                                        sortable: true,
                                                        pageable: true,
                                                        margin: {top: "1cm", bottom: "1cm"},
                                                        scale: 0.7,
                                                        repeatHeaders:true,
                                                        //height: 1200
                                                        template: $("#page-template").html(),
                                                        //margin: {top: "100px", bottom: "10px"},
                                                        color:"black",
                                                        //padding: {top: "40px", bottom: "40px"},
                                                        height: 800
                                                    })
                                            .then(function (group) {
                                                kendo.drawing.pdf.saveAs(group, "Active_Inactive_User_List.pdf")
                                            });
                                }
                                
</script>
<script src="<?php echo base_url(); ?>assets/js/disable_editor.js"></script>

</body>

</html>
