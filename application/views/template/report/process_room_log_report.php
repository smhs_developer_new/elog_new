<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'report', "status" => 'active'))->row_array();
$show = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $show = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_edit'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1) {
            $show = true;
        } else {
            $show = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",3000);</script>";
    }
}
$this->db->order_by("id", "DESC");
$footerData = $this->db->get_where("pts_mst_report_footer", array("report_id" => $_GET['module_id'], "status" => "active"))->result_array();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="smhs" content="">
        <title>E-Logbook</title>
        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url() ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>css/fontfamily.css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="<?php echo base_url() ?>css/sb-admin-2.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/custom.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
        <style>
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
        </style>
    </head>
    <body id="page-top" ng-app="processLogReportApp" ng-controller="processLogReportCtrl" ng-cloak>
        <!-- Page Wrapper -->
        <div id="wrapper">
            <?php $this->load->view("template/report/report_header"); ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="content-wrapper">
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="col-auto">
                                <div class="form-row">
                                    <div class="col-lg-3 mb-3"><label><b>Room Name</b> <span style="color: red">*</span></label>
                                        <select class="chzn-select form-control" tabindex="4" name="room_code" data-placeholder="Search Room Code" ng-change="getFixedEquimentList();getBlockDetails()" ng-options="dataObj['room_code'] as (dataObj.room_code +             ' => ' +             dataObj.room_name +             ' => ' +             dataObj.area_id) for dataObj in sysRoomData"  ng-model="room_code" required chosen>
                                            <option value="">Select Room Name</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label><b>Equipment Name</b> <span style="color: red">*</span></label>
                                        <!-- <select class="chzn-select form-control" tabindex="4"  name="equipment_code" data-placeholder="Search Equipment" ng-change="getProcesslogDetails()" ng-options="dataObj['equipment_code'] as dataObj.equipment_name for dataObj in fixedEquipmentData"  ng-model="equipment_code" required chosen>
                                            <option value="">Select Equipment</option>
                                        </select> -->
                                        <select class="chzn-select form-control" tabindex="4"  name="equipment_code" data-placeholder="Search Equipment" ng-options="dataObj['equipment_code'] as dataObj.equipment_name for dataObj in fixedEquipmentData"  ng-model="equipment_code" required chosen>
                                            <option value="">Select Equipment</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 mb-3"><label><b>Start Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-12 ng-isolate-scope" moment-picker="start_date" format="DD-MMM-YYYY" start-view="month" locale="en" today="true" max-date="current_date">
                                                <input class="form-control ng-pristine ng-valid ng-scope moment-picker-input ng-empty ng-touched" name="start_date" placeholder="Select Start Date" ng-model="start_date" ng-model-options="{updateOn: 'blur'}" tabindex="0" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 mb-3"><label><b>End Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-12 ng-isolate-scope" moment-picker="end_date" format="DD-MMM-YYYY" start-view="month" min-date="start_date" locale="en" today="true" max-date="current_date">
                                                <input class="form-control  ng-pristine ng-valid ng-scope moment-picker-input ng-empty ng-touched" name="end_date" placeholder="Select End Date" ng-model="end_date" ng-model-options="{updateOn: 'blur'}" tabindex="0" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 mb-3"><label></label>
                                        <button class="btn btn-success" style="margin-top: 24px;" ng-click="getHeaderList()">Go</button>
                                    </div>
                                </div>
                                <!-- <div class="col-lg-2 mb-3"><label></label>
                                    <button style="margin-top: 30px;"  class="btn btn-success" ng-click="getProcesslogDetails('date-filter')">Filter Report</button>
                                </div>
                                <div class="col-lg-2 mb-3"><label></label><button style="margin-top: 10px;" ng-show="prolog.length > 0" class="btn btn-success" onclick="ExportPdf()">export to pdf</button></div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-default mt-4">
                    <div class="form-row">
                        <div class="col-lg-10 text-center"></div>
                        <?php if ($show) { ?>
                            <div class="col-lg-2" ng-if="prolog.length > 0"><button style="margin-top:5px;"  onclick="ExportPdf()"><i class="fas fa-file-pdf" style="font-size:24px"></i></button></div>
                        <?php } ?>
                    </div>
                    <!--            <div class="form-row">
                                            <div class="col-lg-10 text-center"></div>
                                            <div class="col-lg-2" ng-if="prolog.length > 0" style="margin-top:5px;"><button  ng-click="printToCart('process-log-report')"><span style="font-size:24px" class="glyphicon glyphicon-print"></span></button>&nbsp;&nbsp;<button  onclick="ExportPdf()"><i class="fas fa-file-pdf" style="font-size:24px"></i></button></div>
                                        </div>-->
                    <div class="card-body" id="process-log-report">
                        <div class="form-row scroll-table">
                            <div class="col-md-2">
                                <img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo">

                            </div>
                            <div class="col-md-10">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th><b>TITLE</b></th>
                                            <td colspan="4">Process Room Log</td>
                                        </tr>
                                        <tr>
                                            <th>Form No.</th>
                                            <th>Version No. &amp; Status</th>
                                            <th>Effective Date</th>
                                            <th>Retired Date</th>
                                            <th>Document Reference No.</th>
                                        </tr>
                                    </tbody>
                                    <tbody>
                                        <tr>
                                            <td>{{headerData.length > 0?reportHeader.form_no:'NA'}}</td>
                                            <td>{{headerData.length > 0?reportHeader.version_no+' & Effective':'NA'}} </td>
                                            <td>{{headerData.length > 0?(reportHeader.effective_date| format | date:'dd-MMM-yyyy'):'NA'}}</td>
                                            <td>{{headerData.length > 0?(reportHeader.retired_date| format | date:'dd-MMM-yyyy'):'NA'}}</td>
                                            <td>{{headerData.length > 0?reportHeader.document_no:'NA'}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12">
                                <div class="form-row">
                                    <div class="col-lg-12 text-center"><h2><b>PROCESS ROOM LOG</b></h2></div>

                                </div>

                            </div>
                        </div>
                        <div class="form-row scroll-table">


                            <table style="border:0px;">
                                <tbody><tr>
                                        <th>EQUIPMENTS NAME<br>(Only fixed equipments):</th>
                                        <td style="border:0px;">{{equipment_name}}</td>
                                        <th>BLOCK:</th>
                                        <td style="border:0px;">{{blockData.length > 0?blockData[0].block_code:''}}</td>
                                    </tr>
                                    <tr>
                                        <th>EQUIPMENTS ID:</th>
                                        <td style="border:0px;">{{equipment_code}}</td>
                                        <th>ROOM NO:</th>
                                        <td style="border:0px;">{{room_name}}</td>
                                    </tr>
                                </tbody></table>

                        </div>
                        <div class="form-row scroll-table" style="margin-top:10px;">
                            <table class="table custom-table">
                                <thead>
                                    <tr>
                                        <th>DATE </th>
                                        <th>TIME</th>
                                        <th>ACTIVITY</th>
                                        <th>PRODUCT AND Batch No.</th>
                                        <th>OPERATOR<br>(SIGN/DATE)</th>
                                        <th>CHECKED BY PROD.<br>(SIGN/DATE)</th>
                                        <th>VERIFIED By<br>IPQA.<br>(SIGN/DATE)</th>
                                        <th>REMARK</th>
                                        <th>ATTACHMENTS</th>
                                    </tr>
                                </thead>
                                <tbody ng-repeat="dataObj in prolog" ng-show="prolog.length > 0">
                                    <tr>
                                        <td>{{dataObj.startdate| format | date:'dd-MMM-yyyy'}}</td>
                                        <td>From : <br/>{{dataObj.starttime}}</td>
                                        <td>
                                            <table class="table-bordered">
                                                <thead>
                                                    <tr>
                                                        <td><b>P</b></td>
                                                        <td><b>C</b></td>
                                                        <td><b>M</b></td>
                                                        <td><b>S</b></td>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </td>
                                        <td rowspan="2">{{dataObj.product + ' (' + dataObj.product_name + ") / " + dataObj.batch}}</td>
                                        <td>{{dataObj.startoperator}}<br>({{dataObj.startremark | date:'dd-MMM-yyyy HH:mm:ss'}})</td>
                                        <td>{{dataObj.startcheckedby}}<br>({{dataObj.startcheckedbyremark | date:'dd-MMM-yyyy HH:mm:ss'}})</td>
                                        <td rowspan="2">{{dataObj.qa!='' ? dataObj.qa:'' }}<br>({{dataObj.qaremark}})</td>
                                        <!--<td rowspan="2">{{dataObj.tempStr}}</td>-->
                                        <td rowspan="2"><p ng-repeat="remarkObj in dataObj.remark track by $index">{{remarkObj}}</p><br></td> 
                                        <td rowspan="2"><p ng-if="dataObj.logAttachments.length>0" ng-repeat="attachObj in dataObj.logAttachments track by $index"> <a href="<?php echo base_url() ?>assets/files/{{attachObj.act_id}}/{{attachObj.file_name}}" target="_blank"  >{{attachObj.file_name}}</a></p><br></td> 
                                    </tr>
                                    <tr>
                                        <td>{{dataObj.stopdate| format | date:'dd-MMM-yyyy'}}</td>
                                        <td>To : <br/>{{dataObj.stoptime}}</td>
                                        <td>
                                            <table class="table-bordered">
                                                <thead>
                                                    <tr ng-if="dataObj.activity == 'Type A Cleaning' || dataObj.activity == 'Type B Cleaning' || dataObj.activity == 'Type D Cleaning'">
                                                        <td>X</td>
                                                        <td ng-if="dataObj.activity == 'Type A Cleaning'"><b>A</b></td>
                                                        <td ng-if="dataObj.activity == 'Type B Cleaning'"><b>B</b></td>
                                                        <td ng-if="dataObj.activity == 'Type D Cleaning'"><b>D</b></td>
                                                        <td>X</td>
                                                        <td>X</td>
                                                    </tr>
                                                    <tr ng-if="dataObj.activity == 'Maintenance'">
                                                        <td>X</td>
                                                        <td>X</td>
                                                        <td><b>M</b></td>
                                                        <td>X</td>
                                                    </tr>
                                                    <tr ng-if="dataObj.activity == 'Production'">
                                                        <td><b>P</b></td>
                                                        <td>X</td>
                                                        <td>X</td>
                                                        <td>X</td>
                                                    </tr>
                                                    <tr ng-if="dataObj.activity == 'Sampling'">
                                                        <td>X</td>
                                                        <td>X</td>
                                                        <td>X</td>
                                                        <td><b>S</b></td>       
                                                    </tr>
                                                </thead>
                                            </table>
                                        </td>

                                        <td>{{dataObj.stopoperator}}<br>({{dataObj.stopremark | date:'dd-MMM-yyyy HH:mm:ss'}})</td>
                                        <td>{{dataObj.stopcheckedby}}<br>({{dataObj.stopcheckedbyremark | date:'dd-MMM-yyyy HH:mm:ss'}})</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="col-lg-12 text-center" ng-show="prolog.length == 0"><h2>No Data Found</h2></div>
                            <div class="col-lg-12 text-center" ng-show="prolog.length == 0"><h4>(Please Select the above parameters)</h4></div>
                        </div>
                        <div class="form-row" style="margin-top:10px;">
                            <b>Abbreviation:P=Process, C=Cleaning, M=Maintenance, S=Sampling</b>
                        </div>
                        <div class="form-row">
                            <p>Mark  <img style="height: 21px;" src="<?= base_url('img/icons/right_click.png') ?>" alt="Italian Trulli"> the activity column as per activity performed for 'P','M' and 'S'.For cleaning activity mention 'A'(For Type A cleaning),'B'(For Type B cleaning) and 'D'(For Day end/Shift end Cleaning) in activity column 'C'.</p>
                        </div>
                        <div class="form-row" style="margin-top:10px;" ng-show="headerData.length > 0">
                            <b>FORM NO.</b>&nbsp;{{reportHeader.footer_name!=''?reportHeader.footer_name:'NA'}}
                        </div>
                        <div class="form-row pb-2" id="pdf_download" style="display:none">
                            <div class="col-md-3"></div>
                            <div class="col-md-3">Print By : <?php echo $this->session->userdata('empname') ?></div>
                            <div class="col-md-3">Date : <?php echo date("d-M-Y"); ?></div>
                            <div class="col-md-3">Time : <?php echo date("H:i:s"); ?></div>
                        </div>
                    </div>
                </div>






            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>SMHS. &copy; 2020 - 2021. All Rights Reserved.</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->
    </div>
    <!-- End of Content Wrapper -->
    

</div>
<!-- End of Page Wrapper -->
<script type="text/kendo-template" id="page-template">
    #
        var footer_name = foot_name;
    #
    <div class="page-template">
        <div class="footer">
            <table class="table">
                <tr>
                    <td>
                        <div class="form-row" style="margin-top:0px;">
                            <b>Abbreviation:P=Process, C=Cleaning, M=Maintenance, S=Sampling</b>
                            <br>
                            <p>Mark  <img style="height: 21px;" src="<?= base_url('img/icons/right_click.png') ?>" alt="Italian Trulli"> the activity column as per activity performed for 'P','M' and 'S'.For cleaning activity mention 'A'(For Type A cleaning),'B'(For Type B cleaning) and 'D'(For Day end/Shift end Cleaning) in activity column 'C'.</p>
                        </div>
                        <div class="form-row" style="margin-top:10px;">
                            <b>FORM NO.</b>&nbsp;<div id="footer_no1">#=footer_name#</div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"><b>Print By : <?php echo $this->session->userdata('empname') ?></b></div>
                            <div class="col-md-3"><b>Date : <?php echo date("d-M-Y"); ?></b></div>
                            <div class="col-md-3"><b>Time :<?php $timezone = "Asia/Kolkata";date_default_timezone_set($timezone);echo date("H:i:s");?></b></div>
                            <div class="col-md-3"><b>Page #: pageNum # of #: totalPages #</b></div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</script>
<div id="master_data_div" style="display: none;">
    <div class="card-body ht750" id="audit_trail2" style="margin-top:10px;padding-top:10px">                      
	<div class="form-row scroll-table" style="margin-top:10px;">
            <style>
                #tbldata {border-collapse:collapse; table-layout:fixed; width:310px; height: 100px !important;}
                #tbldata td {width:100px;word-wrap:break-word; height:100px !important;font-size: 15px;}
            </style>
            <table id="tbldata" border="1" style="width:100%;color:black;">
                <thead style="max-height:165px;">
                    <tr style="max-height:80px;">
                        <td colspan="9">
                            <div class="form-row">
                                <div class="col-md-2"><img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo"></div>
                                <div class="col-md-10">
                                    <table class="table-bordered" style="width:100%!important">
                                        <tbody>
                                            <tr><th style="height:20px !important; width: 200px;">TITLE</th><td colspan="4" style="height:20px !important; width: 800px;">Process Room Log</td></tr>
                                            <tr><th style="height:20px !important; width: 200px;">Form No.</th><th style="height:20px !important; width: 200px;">Version No. &amp; Status</th><th style="height:20px !important; width: 200px;">Effective Date</th><th style="height:20px !important; width: 200px;">Retired Date</th><th style="height:20px !important; width: 200px;">Document Reference No.</th></tr>
                                            <tr>
                                                <td style="height:20px !important; width: 200px;">{{headerData.length > 0?reportHeader.form_no:'NA'}}</td>
                                                <td style="height:20px !important; width: 200px;">{{headerData.length > 0?reportHeader.version_no+' & Effective':'NA'}} </td>
                                                <td style="height:20px !important; width: 200px;">{{headerData.length > 0?(reportHeader.effective_date| format | date:'dd-MMM-yyyy'):'NA'}}</td>
                                                <td style="height:20px !important; width: 200px;">{{headerData.length > 0?(reportHeader.retired_date| format | date:'dd-MMM-yyyy'):'NA'}}</td>
                                                <td style="height:20px !important; width: 200px;">{{headerData.length > 0?reportHeader.document_no:'NA'}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-12">
                                    <div class="form-row">
                                        <div class="col-lg-12 text-center"><h2><b>PROCESS ROOM LOG</b></h2></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <table style="border:0px;">
                                    <tbody>
                                        <tr>
                                            <th style="width:200px;height:20px !important;">EQUIPMENTS NAME<br>(Only fixed equipments):</th>
                                            <td style="border:0px;height:20px !important;width:300px !important;">{{equipment_name}}</td>
                                            <th style="width:200px;height:20px !important;">BLOCK:</th>
                                            <td style="border:0px;height:20px !important;width:300px !important;">{{blockData.length > 0?blockData[0].block_code:''}}</td>
                                        </tr>
                                        <tr>
                                            <th style="width:200px;height:20px !important;">EQUIPMENTS ID:</th>
                                            <td style="border:0px;height:20px !important;width:300px !important;">{{equipment_code}}</td>
                                            <th style="width:150px;height:20px !important;">ROOM NO:</th>
                                            <td style="border:0px;height:20px !important;width:300px !important;">{{room_name}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color:#4e73df;color:#fff;height:60px;">
                        <th>DATE </th>
                        <th>TIME</th>
                        <th>ACTIVITY</th>
                        <th>PRODUCT AND Batch No.</th>
                        <th>OPERATOR<br>(SIGN/DATE)</th>
                        <th>CHECKED BY PROD.<br>(SIGN/DATE)</th>
                        <th>VERIFIED By<br>IPQA.<br>(SIGN/DATE)</th>
                        <th>REMARK</th>
                        <th>ATTACHMENTS</th>
                    </tr>
                </thead>
                <tbody ng-repeat="dataObj in prolog" ng-show="prolog.length > 0">
                    <tr style="max-height: 90px!important">
                        <td>{{dataObj.startdate| format | date:'dd-MMM-yyyy'}}</td>
                        <td>From : <br/>{{dataObj.starttime}}</td>
                        <td>
                            <table class="table-bordered">
                                <thead>
                                    <tr style="height:118px;">
                                        <td><b>P</b></td>
                                        <td><b>C</b></td>
                                        <td><b>M</b></td>
                                        <td><b>S</b></td>
                                    </tr>
                                </thead>
                            </table>
                        </td>
                        <td rowspan="2">{{dataObj.product + ' (' + dataObj.product_name + ") / " + dataObj.batch}}</td>
                        <td>{{dataObj.startoperator}}<br>({{dataObj.startremark  | date:'dd-MMM-yyyy HH:mm:ss'}})</td>
                        <td>{{dataObj.startcheckedby}}<br>({{dataObj.startcheckedbyremark  | date:'dd-MMM-yyyy HH:mm:ss'}})</td>
                        <td rowspan="2">{{dataObj.qa!='' ? dataObj.qa:'' }}<br>({{dataObj.qaremark}})</td>
                        <!--<td rowspan="2">{{dataObj.tempStr}}</td>-->
                        <td rowspan="2"><p ng-repeat="remarkObj in dataObj.remark track by $index">{{remarkObj}}</p></td> 
                        <td rowspan="2"><p ng-if="dataObj.logAttachments.length>0" ng-repeat="attachObj in dataObj.logAttachments track by $index"> <a href="<?php echo base_url() ?>assets/files/{{attachObj.act_id}}/{{attachObj.file_name}}" target="_blank"  >{{attachObj.file_name}}</a></p><br></td> 
                    </tr>
                    <tr style="max-height: 90px!important">
                        <td>{{dataObj.stopdate| format | date:'dd-MMM-yyyy'}}</td>
                        <td>To : <br/>{{dataObj.stoptime}}</td>
                        <td>
                            <table class="table-bordered">
                                <thead>
                                    <tr ng-if="dataObj.activity == 'Type A Cleaning' || dataObj.activity == 'Type B Cleaning' || dataObj.activity == 'Type D Cleaning'">
                                        <td>X</td>
                                        <td ng-if="dataObj.activity == 'Type A Cleaning'"><b>A</b></td>
                                        <td ng-if="dataObj.activity == 'Type B Cleaning'"><b>B</b></td>
                                        <td ng-if="dataObj.activity == 'Type D Cleaning'"><b>D</b></td>
                                        <td>X</td>
                                        <td>X</td>
                                    </tr>
                                    <tr ng-if="dataObj.activity == 'Maintenance'">
                                        <td>X</td>
                                        <td>X</td>
                                        <td><b>M</b></td>
                                        <td>X</td>
                                    </tr>
                                    <tr ng-if="dataObj.activity == 'Production'">
                                        <td><b>P</b></td>
                                        <td>X</td>
                                        <td>X</td>
                                        <td>X</td>
                                    </tr>
                                    <tr ng-if="dataObj.activity == 'Sampling'">
                                        <td>X</td>
                                        <td>X</td>
                                        <td>X</td>
                                        <td><b>S</b></td>       
                                    </tr>
                                </thead>
                            </table>
                        </td>
                        <td>{{dataObj.stopoperator}}<br>({{dataObj.stopremark | date:'dd-MMM-yyyy HH:mm:ss'}})</td>
                        <td>{{dataObj.stopcheckedby}}<br>({{dataObj.stopcheckedbyremark | date:'dd-MMM-yyyy HH:mm:ss'}})</td>
                    </tr>
                </tbody>
            </table>
	</div>
    </div>
</div>
<style>
    /* Page Template for the exported PDF */
    .page-template {
      font-family: "DejaVu Sans", "Arial", "sans-serif";
      position: absolute;
      width: 100%;
      height: 100%;
      color: black;
      top: 0;
      left: 0;
    }
    .page-template .header {
      position: absolute;
      top: 0px;
      left: 30px;
      right: 30px;
      color: black;
      bottom:20px;
      margin-bottom: 10px;
      padding-bottom: 10px;
    }
    .page-template .footer {
      position: absolute;
      bottom: 0px;
      left: 30px;
      right: 30px;
      border-top: 1px solid black;
      text-align: center;
      color: #000000;
    }
    .k-grid-header .k-header {
        height: 10px;
        padding: 0;
      }

      .k-grid tbody tr {
        line-height: 14px !important;
      }

      .k-grid tbody td {
        padding: 0;
      }
</style>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="<?php echo base_url() ?>user/logout">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->

<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>js/lodash.min.js"></script>
<script src="<?php echo base_url() ?>js/angularjs-dropdown-multiselect.min.js"></script>
<script src="<?php echo base_url() ?>js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>js/kendo.all.min.js"></script>
<script src="<?php echo base_url() ?>js/moment-with-locales.js"></script>
<script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
<script type="text/javascript">
                                var foot_name ='';
                                var app = angular.module("processLogReportApp", ['angular.chosen', 'moment-picker']);
                                app.directive('chosen', function ($timeout) {

                                    var linker = function (scope, element, attr) {

                                        scope.$watch('sysRoomData', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);
                                        scope.$watch('fixedEquipmentData', function () {
                                            $timeout(function () {
                                                element.trigger('chosen:updated');
                                            }, 0, false);
                                        }, true);
                                        $timeout(function () {
                                            element.chosen();
                                        }, 0, false);
                                    };
                                    return {
                                        restrict: 'A',
                                        link: linker
                                    };
                                });
                                app.filter('format', function () {
                                    return function (item) {
                                        var t = item.split(/[- :]/);
                                        var d = new Date(t[0], t[1] - 1, t[2]);
                                        var time = d.getTime();
                                        return time;
                                    };
                                });
                                app.filter('format1', function () {
                                    return function (item) {
                                        var t = item.split(/[- :]/);
                                        var d = new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
                                        var time = d.getTime();
                                        return time;
                                    };
                                });
                                app.controller("processLogReportCtrl", function ($scope, $http, $filter) {
                                    $scope.headerData = [];
                                    $scope.reportHeader = {};
                                    $scope.current_date = moment().format('DD-MMM-YYYY');
                                    $scope.start_date = moment().format('DD-MMM-YYYY');
        $scope.end_date = moment().format('DD-MMM-YYYY');
                                    $scope.getHeaderList = function () {
                                    if($scope.start_date == "" || $scope.start_date == undefined || $scope.end_date == "" || $scope.end_date == undefined)
                                    {
                                        alert("Please Select the date range first...!");
                                        return false;
                                    }
                                    $http({
                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getHeaderByLogid',
                                        method: "POST",
                                        data: "header_id=" + <?php echo $_GET['mst_act_id'] ?> + "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date,
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                        $scope.headerData = response.data.header_list;
                                        
                                        if($scope.headerData.length > 0)
                                        {
                                            if($scope.headerData.length > 1)
                                            {
                                                alert("There are more than one header in this date range please change it, Thank you...!");
                                                $scope.headerData = [];
                                                return false;
                                            }
                                            else
                                            {
                                                $scope.reportHeader = response.data.header_list[0];
                                                foot_name = $scope.reportHeader.footer_name;
                                                $scope.getProcesslogDetails();  
                                            }
                                        }
                                        else
                                        {
                                                alert("No version is effective in selected date range, please change the dates or check with administrator for \"Report Header/Footer Master\"");
                                                $scope.headerData = [];
                                                return false;
                                        } 
                                    }, function (error) { // optional
                                    console.log("Something went wrong.Please try again");
                                    });
                                    }

                                                    $scope.sysRoomData = [];
                                                    $scope.getDeviceRoomList = function () {
                                                        $http({
                                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist',
                                                            method: "GET",
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        }).then(function (response) {
                                                            $scope.sysRoomData = response.data.room_list;
                                                        }, function (error) { // optional

                                                            console.log("Something went wrong.Please try again");

                                                        });
                                                    }
                                                    $scope.getDeviceRoomList();
                                                    $scope.fixedEquipmentData = [];
                                                    $scope.getFixedEquimentList = function () {
                                                        $http({
                                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getfixedequiplistforreportnew?room_code=' + $scope.room_code,
                                                            method: "GET",
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        }).then(function (response) {
                                                            $scope.fixedEquipmentData = response.data.fixed_euip_list;
                                                        }, function (error) { // optional

                                                            console.log("Something went wrong.Please try again");

                                                        });
                                                    }
                                                    $scope.blockData = [];
                                                    $scope.getBlockDetails = function () {
                                                        var area_code = $filter('filter')($scope.sysRoomData, {room_code: $scope.room_code})[0].area_id;
                                                        $http({
                                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getBlockDetails?area_code=' + area_code,
                                                            method: "GET",
                                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                        }).then(function (response) {
                                                            $scope.blockData = response.data.block_list;
                                                        }, function (error) { // optional

                                                            console.log("Something went wrong.Please try again");

                                                        });
                                                    }
                                                    $scope.prolog = [];
                                                    $scope.processLogData = [];
                                                    $scope.getProcesslogDetails = function () {
                                                        var valid = true;
                                                        
                                                            if (($scope.room_code == "" || $scope.room_code == undefined) || ($scope.equipment_code == "" || $scope.equipment_code == undefined) || ($scope.start_date == "" || $scope.start_date == undefined)) {
                                                                valid = false;
                                                            }
                                                        
                                                        if (valid) {
                                                            $scope.prolog = [];
                                                            
                                                            $scope.room_name = $filter('filter')($scope.sysRoomData, {room_code: $scope.room_code})[0].room_name;
                                                            $scope.equipment_name = $filter('filter')($scope.fixedEquipmentData, {equipment_code: $scope.equipment_code})[0].equipment_name;
                                                            var datefilter = '';
                                                            if (($scope.start_date != "" && $scope.start_date != undefined) && ($scope.end_date != "" && $scope.end_date != undefined)) {
                                                                datefilter = "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date;
                                                            }
                                                            $http({
                                                                url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/GetProcessLogReport',
                                                                method: "POST",
                                                                data: "room_code=" + $scope.room_code + "&equip_code=" + $scope.equipment_code +"&mst_act_id=" + <?php echo $_GET['mst_act_id'] ?> + "&type=" + 'processlog_equipment' + datefilter,
                                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                                            }).then(function (response) {
                                                                $scope.processLogData = response.data.row_data;
                                                                console.log($scope.processLogData);
                                                                var batch = "";
                                                                        var product = "";
                                                                        var productname = "";
                                                                        var activityname = "";
                                                                        var startoperator = "";
                                                                        var startremark = "";
                                                                        var startdate = "";
                                                                        var starttime = "";
                                                                        var stopoperator = "";
                                                                        var stopremark = "";
                                                                        var stopdate = "";
                                                                        var stoptime = "";
                                                                        var startcheckedby = "";
                                                                        var startcheckedbyremark = "";
                                                                        var stopcheckedby = "";
                                                                        var stopcheckedbyremark = "";
                                                                        var checkedbyQA = "";
                                                                        var remarkbyQA = "";
                                                                        var activity_name = "";
                                                                        var isworkflow = "no";
                                                                        var remark = "";
                                                                        var logAttachments = "";
                                                                        var predoc = "";
                                                               
                                                                angular.forEach($scope.processLogData, function (value, key) {
                                                                    if(predoc != value["doc_id"])
                                                                    {
                                                                        //console.log(predoc);
                                                                        //console.log(value["doc_id"]);
                                                                        batch = "";
                                                                        product = "";
                                                                        productname = "";
                                                                        activityname = "";
                                                                        startoperator = "";
                                                                        startremark = "";
                                                                        startdate = "";
                                                                        starttime = "";
                                                                        stopoperator = "";
                                                                        stopremark = "";
                                                                        stopdate = "";
                                                                        stoptime = "";
                                                                        startcheckedby = "";
                                                                        startcheckedbyremark = "";
                                                                        stopcheckedby = "";
                                                                        stopcheckedbyremark = "";
                                                                        checkedbyQA = "";
                                                                        remarkbyQA = "";
                                                                        activity_name = "";
                                                                        isworkflow = "no";
                                                                        remark = "";
                                                                        logAttachments = "";
                                                                        //predoc = "";
                                                                        predoc = value["doc_id"];
                                                                    }

                                                                    if (value['workflow_type'] == 'start'){
                                                                        if (value['approval_status'] == 'N/A'){
                                                                            startoperator = value['user_name'];
                                                                            startdate = value['start_date'];
                                                                            starttime = value['start_time'];
                                                                            isworkflow = "no";
                                                                            startremark = value['activity_time'];
                                                                            remark = value['user_name'] + ':-' + value['activity_remarks'];
                                                                            batch =  value['batch_no'];
                                                                            product = value['product_code'];
                                                                            productname = value['product_name'];
                                                                            activityname = value['activity_name'];
                                                                        } else if (value['approval_status'] == 'QAapprove'){
                                                                            checkedbyQA = value['user_name'];
                                                                            isworkflow = "yes";
                                                                            remarkbyQA = value['activity_time'];
                                                                            remark = remark + '~' + value['user_name'] + ':-' + value['activity_remarks'];
                                                                        } else{
                                                                            startcheckedby = value['user_name'];
                                                                            isworkflow = "yes";
                                                                            startcheckedbyremark = value['activity_time'];
                                                                            remark = remark + '~' + value['user_name'] + ':-' + value['activity_remarks'];
                                                                        }
                                                                        var tempStr = remark.split("~");
                                                                        // var uniqueNames = getUnique(tempStr);


                                                                        //var temp={"startdate": startdate, "starttime": starttime, "stopdate": stopdate, "stoptime": stoptime, "activity": activityname, "product": product, "batch": batch, "startoperator": startoperator, "startremark": startremark, "startcheckedby": startcheckedby, "startcheckedbyremark": startcheckedbyremark, "stopoperator": stopoperator, "stopremark": stopremark, "stopcheckedby": stopcheckedby, "stopcheckedbyremark": stopcheckedbyremark, "qa": checkedbyQA, "remark": tempStr, "qaremark": remarkbyQA, "logAttachments": value['logAttachments'], "product_name":productname};

                                                                        //$scope.prolog[value.id]=temp;

                                                                    } else if (value['workflow_type'] == 'stop'){
                                                                        if (value['approval_status'] == 'N/A'){
                                                                            stopoperator = value['user_name'];
                                                                            stopdate = value['stop_date'];
                                                                            stoptime = value['stop_time'];
                                                                            isworkflow = "no";
                                                                            stopremark = value['activity_time'];
                                                                            remark = remark + "~" + value['user_name'] + ':-' + value['activity_remarks'];
                                                                        } else if (value['approval_status'] == 'QAapprove'){
                                                                            checkedbyQA = value['user_name'];
                                                                            isworkflow = "yes";
                                                                            remarkbyQA = value['activity_time'];
                                                                            remark = remark + '~' + value['user_name'] + ':-' + value['activity_remarks'];
                                                                        }else{
                                                                            stopcheckedby = value['user_name'];
                                                                            isworkflow = "yes";
                                                                            stopcheckedbyremark = value['activity_time'];
                                                                            remark = remark + "~" + value['user_name'] + ':-' + value['activity_remarks'];
                                                                            logAttachments = value['logAttachments'];
                                                                        }
                                                                        var tempStr = remark.split("~");
                                                                        // var uniqueNames = getUnique(tempStr);
                                                                        //var temp={"startdate": startdate, "starttime": starttime, "stopdate": stopdate, "stoptime": stoptime, "activity": activityname, "product": product, "batch": batch, "startoperator": startoperator, "startremark": startremark, "startcheckedby": startcheckedby, "startcheckedbyremark": startcheckedbyremark, "stopoperator": stopoperator, "stopremark": stopremark, "stopcheckedby": stopcheckedby, "stopcheckedbyremark": stopcheckedbyremark, "qa": checkedbyQA, "remark": tempStr, "qaremark": remarkbyQA, "logAttachments": value['logAttachments'], "product_name":productname};

                                                                        //$scope.prolog[value.id]=temp;
                                                                    } else{
                                                                        if (value['approval_status'] == 'N/A'){
                                                                            stopoperator = value['user_name'];
                                                                            stopdate = value['stop_date'];
                                                                            stoptime = value['stop_time'];
                                                                            isworkflow = "no";
                                                                            stopremark = value['activity_time'];
                                                                            remark = remark + "~" + value['user_name'] + ':-' + value['activity_remarks'];
                                                                        } else if (value['approval_status'] == 'QAapprove'){
                                                                            checkedbyQA = value['user_name'];
                                                                            isworkflow = "yes";
                                                                            remarkbyQA = value['activity_time'];
                                                                            remark = remark + '~' + value['user_name'] + ':-' + value['activity_remarks'];
                                                                        }else{
                                                                            stopcheckedby = value['user_name'];
                                                                            isworkflow = "yes";
                                                                            stopcheckedbyremark = value['activity_time'];
                                                                            remark = remark + "~" + value['user_name'] + ':-' + value['activity_remarks'];
                                                                             logAttachments = value['logAttachments'];
                                                                        }
                                                                        var tempStr = remark.split("~");
                                                                        //var uniqueNames = getUnique(tempStr);
                                                                        //var temp={"startdate": startdate, "starttime": starttime, "stopdate": stopdate, "stoptime": stoptime, "activity": activityname, "product": product, "batch": batch, "startoperator": startoperator, "startremark": startremark, "startcheckedby": startcheckedby, "startcheckedbyremark": startcheckedbyremark, "stopoperator": stopoperator, "stopremark": stopremark, "stopcheckedby": stopcheckedby, "stopcheckedbyremark": stopcheckedbyremark, "qa": checkedbyQA, "remark": tempStr, "qaremark": remarkbyQA, "logAttachments": value['logAttachments'], "product_name":productname};
                                                                        //$scope.prolog[value.id]=temp;
                                                                    }
                                                                    var temp={"startdate": startdate, "starttime": starttime, "stopdate": stopdate, "stoptime": stoptime, "activity": activityname, "product": product, "batch": batch, "startoperator": startoperator, "startremark": startremark, "startcheckedby": startcheckedby, "startcheckedbyremark": startcheckedbyremark, "stopoperator": stopoperator, "stopremark": stopremark, "stopcheckedby": stopcheckedby, "stopcheckedbyremark": stopcheckedbyremark, "qa": checkedbyQA, "remark": tempStr, "qaremark": remarkbyQA, "logAttachments": value['logAttachments'], "product_name":productname};
                                                                    $scope.prolog[value.id]=temp;
                                                                });                                                            
                                                                $scope.prolog = $scope.prolog.filter(function () {
                                                                        return true                                                                         
                                                                });
                                                                var prolog = $scope.prolog;
                                                                $scope.prolog = prolog.filter(prolog => prolog['batch'].length > 0);                                                               
                                                            }, function (error) { // optional

                                                                console.log("Something went wrong.Please try again");

                                                            });
                                                        } else {
                                                            alert("Please Select Room Code / Equipment Code");
                                                        }
                                                    }

                                                    $scope.printToCart = function (printSectionId) {
                                                        var innerContents = document.getElementById(printSectionId).innerHTML;
                                                        var popupWinindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
                                                        popupWinindow.document.open();
                                                        popupWinindow.document.write('<html><head>' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/brands.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/regular.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/solid.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/fontawesome.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/simple-line-icons/css/simple-line-icons.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/animate.css/animate.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/whirl/dist/whirl.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css" id="bscss">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>css/app.css" id="maincss">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>css/Ponta_style.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-slider/dist/css/bootstrap-slider.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/chosen-js/chosen.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/select2/dist/css/select2.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-wysiwyg/css/style.css">' +
                                                                '<link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />' +
                                                                '<link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />' +
                                                                '<link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">' +
                                                                '<body onload="window.print()">' + innerContents + '</html>');
                                                        popupWinindow.document.close();
                                                    }

                                                });
                            function ExportPdf() {
                                $("#master_data_div").show();
                                kendo.drawing.drawDOM("#master_data_div",{
                                    paperSize: "A3",
                                    landscape: true,
                                    pageable: true,
                                    repeatHeaders:true,
                                    template: $("#page-template").html(),
                                    margin: {top: "100px", bottom: "10px"},
                                    color:"black",
                                    //padding: {top: "40px", bottom: "40px"},
                                    scale: 0.7,
                                    height: 1200
                                }).then(function (group) {
                                    kendo.drawing.pdf.saveAs(group,"process_room_log.pdf")
                                }).always(function () {
                                    //$("#pdf_download").hide();
                                    $("#master_data_div").hide();
                                });
                            }
                                                </script>
<script src="<?php echo base_url(); ?>assets/js/disable_editor.js"></script>
</body>

</html>
