<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'report',"status"=>'active'))->row_array();
$show = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $show = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_edit']!=1) {
            $url=base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '".$url."';\",3000);</script>";
        } elseif ($res['is_edit'] == 1) {
            $show = true;
        } else {
            $show = false;
        }
    } else {
        $url=base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '".$url."';\",3000);</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="smhs" content="">

        <title>E-Logbook</title>

        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url() ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>css/fontfamily.css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="<?php echo base_url() ?>css/sb-admin-2.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/custom.css" rel="stylesheet">

        <link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
        <style>
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
            .pagination {
                display: inline-block;
                padding-left: 0;
                margin: 20px 0;
                border-radius: 4px;
            }
            .pagination > li {
                display: inline;
            }
        </style>


<!-- <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" /> -->
    </head>

    <body id="page-top" ng-app="reportApp" ng-controller="reportCtrl" ng-cloak>

        <!-- Page Wrapper -->
        <div id="wrapper">

            <?php $this->load->view("template/report/report_header"); ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->

                <div class="content-wrapper">
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="col-auto">
                                <div class="form-row">
                                    <div class="col-lg-3 mb-3"><label><b>Start Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-10"
                                                 moment-picker="start_date"
                                                 format="DD-MMM-YYYY"  start-view="month"    locale="en" today="true" max-date="current_date">

                                                <input class="form-control" name="start_date"
                                                       placeholder="Select Start Date"
                                                       ng-model="start_date"
                                                       ng-model-options="{updateOn: 'blur'}" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label><b>End Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-10"
                                                 moment-picker="end_date"
                                                 format="DD-MMM-YYYY"   start-view="month" min-date="start_date"  locale="en" today="true" max-date="current_date">

                                                <input class="form-control " name="end_date"
                                                       placeholder="Select End Date"
                                                       ng-model="end_date"
                                                       ng-model-options="{updateOn: 'blur'}" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label><b>Select Log</b></label>
                                        <select class="form-control" tabindex="4" name="type" data-placeholder="Search User"  ng-model="type">
                                            <option value="">Select Log</option>
                                            <option value="Process Log">Process Log</option>
                                            <option value="Portable Equipment Log">Portable Log</option>
                                            <option value="Line Log">Line Log</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label></label>
                                        <button style="margin-top: 25px;" type="button" class="btn btn-success" ng-click="resetReportData(1)">Go</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="card card-default mt-4">
                   
                    <!--                    <div class="form-row">
                                            <div class="col-lg-10 text-center"></div>
                                            <div class="col-lg-2" ng-if="auditTrailData.length > 0"><button style="margin-top:5px;"  onclick="ExportPdf()"><i class="fas fa-file-pdf" style="font-size:24px"></i></button></div>
                                        </div>-->
                    <div class="card-body" id="common_log_report">
                        <div class="form-row pb-2" id="pdf_download" style="display:none">
                            <div class="col-md-4"></div>
                            <div class="col-md-3">Print By : <?php echo $this->session->userdata('empname') ?></div>
                            <div class="col-md-3">Date : <?php echo date("d-m-Y");?></div>
                            <div class="col-md-2">Time : <?php echo date("H:i:s");?></div>
                        </div>
                        <!--<div class="form-row">
                            <div class="col-md-2">
                                <img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo">

                            </div>
                            <div class="col-md-10">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th><b>TITLE</b></th>
                                            <td colspan="4">Swab Sample</td>
                                        </tr>
                                        <tr>
                                        <tr>
                                            <th>Form No.</th>
                                            <th>Version No. &amp; Status</th>
                                            <th>Effective Date</th>
                                            <th>Retired Date</th>
                                            <th>Document Reference No.</th>
                                        </tr>
                                    </tbody>
                                    <tbody>
                                        <tr>
                                            <td>{{headerData.length > 0?reportHeader.form_no:'NA'}}</td>
                                            <td>{{headerData.length > 0?reportHeader.version_no+' & Effective':'NA'}} </td>
                                            <td>{{headerData.length > 0?(reportHeader.effective_date| date:'dd-MMM-yyyy'):'NA'}}</td>
                                            <td>{{headerData.length > 0?(reportHeader.retired_date| date:'dd-MMM-yyyy'):'NA'}}</td>
                                            <td>{{headerData.length > 0?reportHeader.document_no:'NA'}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>-->
                        <div class="form-row">
                            <div class="col-lg-4"></div>
                             <div class="col-lg-4 text-center"><h2>Process, Portable & Line Log report</h2></div>
                            <?php if ($show) { ?>
                            <div class="col-lg-4 text-right" style="margin-top:-15px;" ng-if="commonLogData.length > 0">
                                <!--<button class="btn_trns"  onclick="ExportPdf()"><i class="fas fa-file-pdf pdf_btn" style="font-size:24px"></i></button>-->
                            <!--<button style="margin-top:5px;"  onclick="exportTableToExcel('log_report', 'log_report')"><i class="fas fa-file-excel" style="font-size:24px"></i></button>-->
                                <a   href="<?php echo site_url()?>createxls?start_date={{start_date}}&end_date={{end_date}}&type={{type}}" target="_blank" ><i class="fas fa-file-excel pdf_btn"></i></a>
                            </div> 
                        <?php } ?>
                        </div>
                        <div class="form-row scroll-table" style="margin-top:10px;">
                            <table class="table custom-table runningacttable" id="log_report">
                                <thead>
                                    <tr>
                                        <th>Doc. No.</th>
                                        <th>Block</th>
                                        <th>Room No.</th>
                                        <th>Equipment Code/Line Code</th>
                                        <th>Equipment Name</th>
                                        <th>Batch No.</th>
                                        <th>Product Code</th>
                                        <th>Product Name</th>
                                        <th>Activity</th>
                                        <th>Start date time</th>
                                        <th>End Date time</th>
                                        <th>Operator</th>
                                        <th>Operator Date time</th>
                                        <th>Checked By</th>
                                        <th>Checked Date time</th>
                                        <th>Verified by</th>
                                        <th>Verified Date time</th>
                                        <th>Remark</th>
                                    </tr>
                                </thead>
                                <tbody dir-paginate="dataObj in commonLogData |itemsPerPage:itemsPerPage" total-items="total_count" ng-show="commonLogData.length > 0">
                                    <tr>
                                        <td>{{dataObj.doc_id}}</td> 
                                        <td>{{dataObj.block_code}}</td>    
                                        <td>{{dataObj.room_code}}</td>    
                                        <td>{{dataObj.equip_code}}</td>    
                                        <td>{{dataObj.equipment_name!=null?dataObj.equipment_name:dataObj.equip_code}}</td>    
                                        <td>{{dataObj.batch_no}}</td>    
                                        <td>{{dataObj.product_code}}</td>    
                                        <td>{{dataObj.product_name}}</td>    
                                        <td>{{dataObj.logname}}-{{dataObj.activity_name}}</td>    
                                        <td>{{dataObj.start_date_time}}</td>    
                                        <td>{{dataObj.stop_date_time}}</td>    
                                        <td> 
                                            <p ng-if="(dataObj.workflow_type=='start' || dataObj.workflow_type=='stop') && dataObj.approval_status=='N/A' ">{{dataObj.user_name}}</p>
                                            <p ng-if="(dataObj.workflow_type=='start' || dataObj.workflow_type=='stop') && dataObj.approval_status!='N/A' ">N/A</p>
                                        </td>    
                                        <td>
                                            <p ng-if="(dataObj.workflow_type=='start' || dataObj.workflow_type=='stop') && dataObj.approval_status=='N/A' ">{{dataObj.activity_time}}</p>
                                            <p ng-if="(dataObj.workflow_type=='start' || dataObj.workflow_type=='stop') && dataObj.approval_status!='N/A' ">N/A</p></td>  
                                        <td>
                                            <p ng-if="(dataObj.workflow_type=='start' || dataObj.workflow_type=='stop') && dataObj.approval_status=='approve' ">{{dataObj.user_name}}</p>
                                            <p ng-if="(dataObj.workflow_type=='start' || dataObj.workflow_type=='stop') && dataObj.approval_status!='approve' ">N/A</p>
                                        </td>    
                                        <td>
                                            <p ng-if="(dataObj.workflow_type=='start' || dataObj.workflow_type=='stop') && dataObj.approval_status=='approve' ">{{dataObj.activity_time}}</p>
                                            <p ng-if="(dataObj.workflow_type=='start' || dataObj.workflow_type=='stop') && dataObj.approval_status!='approve' ">N/A</p>
                                            
                                            </td>    
                                        <td>
                                            <p ng-if="(dataObj.workflow_type!='start' && dataObj.workflow_type!='stop')">{{dataObj.user_name}}</p>
                                            <p ng-if="(dataObj.workflow_type=='start' || dataObj.workflow_type=='stop')">N/A</p>
                                        </td>    
                                        <td>
                                            <p ng-if="(dataObj.workflow_type!='start' && dataObj.workflow_type!='stop')">{{dataObj.activity_time}}</p>
                                            <p ng-if="(dataObj.workflow_type=='start' || dataObj.workflow_type=='stop')">N/A</p>
                                        </td>    
                                        <td>{{dataObj.activity_remarks}}</td>    
                                    </tr>

                                </tbody>                                
                            </table>
                            <div class="col-lg-12" id="pagination-div">
                                <dir-pagination-controls
                                    max-size="8"
                                    direction-links="true"
                                    boundary-links="true" 
                                    on-page-change="commonLogReportData(newPageNumber)" >
                                </dir-pagination-controls>
                            </div>
                            <div id="no-data-div" class="col-lg-12 text-center" ng-show="commonLogData.length == 0"><h2>No Data Found</h2></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>SMHS. &copy; 2020 - 2021. All Rights Reserved.</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="<?php echo base_url() ?>user/logout">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->

<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>js/lodash.min.js"></script>
<script src="<?php echo base_url() ?>js/angularjs-dropdown-multiselect.min.js"></script>
<script src="<?php echo base_url() ?>js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>js/kendo.all.min.js"></script>
<script src="<?php echo base_url() ?>js/moment-with-locales.js"></script>
<script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
<script src="<?php echo base_url() ?>js/dirPagination.js"></script> <!-- Pagination link -->
<script type="text/javascript">
                                var url_base_path = '<?php echo base_url() ?>';
                                var app = angular.module("reportApp", ['angular.chosen', 'moment-picker', 'angularUtils.directives.dirPagination']);
                                app.controller("reportCtrl", function ($scope, $http, $filter, $sce) {
                                    $scope.commonLogData = [];
                                    $scope.pageno = 1; // initialize page no to 1
                                    $scope.total_count = 0;
                                    $scope.itemsPerPage = 10; //this could be a dynamic value from a drop down
                                    $scope.type = "";
                                    $scope.current_date = moment().format('DD-MMM-YYYY');
                                    $scope.start_date = moment().add(-30, 'days').format('DD-MMM-YYYY');
                                    $scope.end_date = moment().format('DD-MMM-YYYY');
                                    
                                                                        // checking header
                                    $scope.headerData = [];
                                    $scope.reportHeader = {};
                                    $scope.getHeaderList = function () {
                                    if($scope.start_date == "" || $scope.start_date == undefined || $scope.end_date == "" || $scope.end_date == undefined)
                                    {
                                        alert("Please Select the date range first...!");
                                        return false;
                                    }
                                    $http({
                                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getHeaderByLogid',
                                        method: "POST",
                                        data: "header_id=" + <?php echo $_GET['mst_act_id'] ?> + "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date,
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                        $scope.headerData = response.data.header_list;
                                        if($scope.headerData.length > 0)
                                        {
                                            if($scope.headerData.length > 1)
                                            {
                                                alert("There are more than one header in this date range please change it, Thank you...!");
                                                $scope.headerData = [];
                                                return false;
                                            }
                                            else
                                            {
                                                $scope.reportHeader = response.data.header_list[0];
                                                $scope.getProcesslogDetails('date-filter');  
                                            }
                                        }
                                        else
                                        {
                                                alert("No version is effective in selected date range, please change the dates or check with administrator for \"Report Header/Footer Master\"");
                                                $scope.headerData = [];
                                                return false;
                                        } 
                                    }, function (error) { // optional
                                    console.log("Something went wrong.Please try again");
                                    });
                                    }
                                    //End

                                    $scope.getUsersList = function () {
                                        $scope.userListData = [];
                                        $http({
                                            url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/getUsersList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.userListData = response.data.user_data;
                                        }, function (error) { // optional
                                            toaster.pop('error', "error", "Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getUsersList();
                                    
                                    
                                    $scope.commonLogReportData = function (pageno) {
                                        var valid = true;
                                        if (($scope.start_date == "") || ($scope.end_date == "")) {
                                            valid = true;
                                        }

                                        if (valid) {
                                            
                                            $http({
                                                url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/GetCommonLogReport?page_limit=' + $scope.itemsPerPage + "&page_no=" + pageno,
                                                method: "POST",
                                                data: "type=" + $scope.type + "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.commonLogData = [];
                                                $scope.commonLogData = response.data.row_data;
                                                $scope.total_count = response.data.total_count;
                                            }, function (error) { // optional
                                                toaster.pop('error', "error", "Something went wrong.Please try again");
                                            });
                                        } else {
                                            alert("Please Select Room No/Start Date/End Date");
                                        }
                                    }
                                    $scope.commonLogReportData($scope.pageno);
                                    $scope.resetReportData = function (pageno) {
                                        $scope.pageno = pageno; // initialize page no to 1
                                        $scope.commonLogReportData($scope.pageno);
                                    }

                                   

                                });
                                app.filter('removeUnderscores', [function() {
                                    return function(string) {
                                        if (!angular.isString(string)) {
                                            return string;
                                        }
                                        return string.replace(/[/_/]/g, ' ');
                                     };
                                    }])
                                app.filter('capitalize', function() {
                                    return function(input) {
                                      return (angular.isString(input) && input.length > 0) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : input;
                                    }
                                   });
                                       // add a custom filter to your module
                                app.filter('UserFilter', function() {
                                    // the filter takes an additional input filterIDs
                                    return function(inputArray, filterIDs) {
                                        // filter your original array to return only the objects that
                                        // have their ID in the filterIDs array
                                        var data = inputArray.filter(function (entry) {
                                            return this.indexOf(entry.id) !== -1;
                                        }, filterIDs);
                                        return data!=""?data[0].emp_name:"--"; // filterIDs here is what "this" is referencing in the line above
                                    };
                                });
                                function ExportPdf() {
                                    $("#pdf_download").show();
                                    kendo.drawing
                                            .drawDOM("#common_log_report",
                                                    {
                                                        paperSize: "A4",
                                                        landscape: true,
                                                        sortable: true,
                                                        pageable: true,
                                                        margin: {top: "1cm", bottom: "1cm"},
                                                        scale: 0.5,
                                                        height: 500
                                                    })
                                            .then(function (group) {
                                                kendo.drawing.pdf.saveAs(group, "log_report.pdf")
                                            }).always(function(){
                                                $("#pdf_download").hide();
                                            });
                                }
                                
                                function exportTableToExcel(tableID, filename){
                                    var downloadLink;
                                    var dataType = 'application/vnd.ms-excel';
                                    var tableSelect = document.getElementById(tableID);
                                    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

                                    // Specify file name
                                    filename = filename?filename+'.xls':'excel_data.xls';

                                    // Create download link element
                                    downloadLink = document.createElement("a");

                                    document.body.appendChild(downloadLink);

                                    if(navigator.msSaveOrOpenBlob){
                                        var blob = new Blob(['\ufeff', tableHTML], {
                                            type: dataType
                                        });
                                        navigator.msSaveOrOpenBlob( blob, filename);
                                    }else{
                                        // Create a link to the file
                                        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

                                        // Setting the file name
                                        downloadLink.download = filename;

                                        //triggering the function
                                        downloadLink.click();
                                    }
                                }
</script>
<script src="<?php echo base_url(); ?>assets/js/disable_editor.js"></script>
</body>

</html>
