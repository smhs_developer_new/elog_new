<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'report', "status" => 'active'))->row_array();
$show = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $show = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_edit'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1) {
            $show = true;
        } else {
            $show = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",3000);</script>";
    }
}
$this->db->order_by("id", "DESC");
$footerData = $this->db->get_where("pts_mst_report_footer", array("report_id" => $_GET['module_id'], "status" => "active"))->result_array();
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="smhs" content="">

    <title>E-Logbook</title>

    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url() ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url() ?>/css/fontfamily.css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="<?php echo base_url() ?>css/sb-admin-2.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
    <style>
        [ng\:cloak],
        [ng-cloak],
        [data-ng-cloak],
        [x-ng-cloak],
        .ng-cloak,
        .x-ng-cloak {
            display: none !important;
        }
    </style>

</head>

<body id="page-top" ng-app="reportApp" ng-controller="reportCtrl" ng-cloak>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <?php $this->load->view("template/report/report_header"); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">
            <!-- Page Heading -->

            <div class="content-wrapper">
                <div class="card card-default">
                    <div class="card-body">
                        <div class="col-auto">
                            <div class="form-row">
                                <div class="col-lg-3 mb-3"><label>Room Name <span style="color: red">*</span></label>
                                    <select class="chzn-select form-control" tabindex="4" name="room_code" data-placeholder="Search Room Code" ng-options="dataObj['room_code'] as (dataObj.room_code +              ' => ' +              dataObj.room_name +              ' => ' +              dataObj.area_id) for dataObj in sysRoomData" ng-model="room_no" ng-change="getRoomDetails();" required chosen>
                                        <option value="">Select Room Name</option>
                                    </select>
                                </div>
                                <div class="col-lg-3 mb-3"><label><b>Start Date</b></label>
                                    <div class="row">
                                        <div class="input-group col-md-12 ng-isolate-scope" moment-picker="start_date" format="DD-MMM-YYYY" start-view="month" locale="en" today="true" max-date="current_date">

                                            <input ng-selected="getMonthName()" class="form-control ng-pristine ng-valid ng-scope moment-picker-input ng-empty ng-touched" name="start_date" placeholder="Select Start Date" ng-model="start_date" ng-model-options="{updateOn: 'blur'}" tabindex="0" autocomplete="off" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 mb-3"><label><b>End Date</b></label>
                                    <div class="row">
                                        <div class="input-group col-md-12 ng-isolate-scope" moment-picker="end_date" format="DD-MMM-YYYY" start-view="month" min-date="start_date" locale="en" today="true" max-date="current_date">

                                            <input class="form-control  ng-pristine ng-valid ng-scope moment-picker-input ng-empty ng-touched" name="end_date" placeholder="Select End Date" ng-model="end_date" ng-model-options="{updateOn: 'blur'}" tabindex="0" autocomplete="off" readonly>
                                        </div>
                                    </div>
                                </div>

                                <!-- <div class="col-lg-3 mb-3"><label>Year</label>
                                        <select class=" form-control" tabindex="4" name="year"
                                                data-placeholder="Search year" ng-model="year">
                                            <option value="">Select Year</option>
                                            <?php for ($i = 2019; $i <= 2050; $i++) { ?>
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label>Month</label>
                                        <select class=" form-control" tabindex="4" name="month"
                                                data-placeholder="Search month" ng-model="month">
                                            <option value="">Select Month</option>
                                            <?php
                                            $months = array("Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec");
                                            foreach ($months as $key => $month) {
                                            ?>
                                                <option value="<?php echo $key + 1; ?>"><?php echo $month; ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    </div> -->
                                <div class="col-lg-3 mb-3"><label></label>
                                    <button style="margin-top: 25px;" class="btn btn-success" ng-click="getHeaderList()">Go</button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="card card-default mt-4">
                <div class="form-row">
                    <div class="col-lg-10 text-center"></div>
                    <?php if ($show) { ?>
                        <div class="col-lg-2" ng-if="envData.length > 0"><button style="margin-top:5px;" onclick="ExportPdf()"><i class="fas fa-file-pdf" style="font-size:24px"></i></button></div>
                    <?php } ?>
                </div>

                <!--                    <div class="form-row">
                                            
                    
                                            <div class="col-lg-2" ng-if="envData.length > 0"><button style="margin-top:5px;"
                                                                                                     onclick="ExportPdf()"><i class="fas fa-file-pdf"
                                                                         style="font-size:24px"></i></button></div>
                                        </div>-->
                <div class="card-body" id="pressure-report">
                    <div class="form-row scroll-table">
                        <div class="col-md-2">
                            <img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo">
                        </div>
                        <div class="col-md-10">
                            <table class="table table-bordered">
                                <tr>
                                    <th colspan="5">FORM</th>
                                </tr>
                                <tr>
                                    <th><b>TITLE</b></th>
                                    <td colspan="4"><b>ENVIRONMENTAL CONDITION/PRESSURE DIFFERENTIAL RECORD</b></td>
                                </tr>
                                <tr>
                                    <th>Form No.</th>
                                    <th>Version No. & Status</th>
                                    <th>Effective Date</th>
                                    <th>Retired Date</th>
                                    <th>Document Reference No.</th>
                                </tr>
                                <tbody>
                                    <tr>
                                        <td>{{headerData.length > 0?reportHeader.form_no:'NA'}}</td>
                                        <td>{{headerData.length > 0?reportHeader.version_no+' & Effective':'NA'}} </td>
                                        <td>{{headerData.length > 0?(reportHeader.effective_date| date:'dd-MMM-yyyy'):'NA'}}</td>
                                        <td>{{headerData.length > 0?(reportHeader.retired_date| date:'dd-MMM-yyyy'):'NA'}}</td>
                                        <td>{{headerData.length > 0?reportHeader.document_no:'NA'}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <br>
                    <div class="form-row scroll-table">
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-6">
                            <table class="table" style="border:1px;">
                                <tr>
                                    <th colspan="2" style="border:0px;">ENVIRONMENTAL CONDITION/PRESSURE DIFFERENTIAL RECORD LIMIT:</th>

                                </tr>
                                <tr>
                                    <th style="border:0px;">Temperature:</th>
                                    <td style="border:0px;">From : {{envPressureData[0].temp_from}} To: {{envPressureData[0].temp_to}} ℃</td>
                                </tr>
                                <tr>
                                    <th style="border:0px;">Relative Humidity:</th>
                                    <td style="border:0px;">From : {{envPressureData[0].rh_from}} To: {{envPressureData[0].rh_to}} %</td>
                                </tr>
                                <tr>
                                    <th style="border:0px;">Pressure Differential(+):</th>
                                    <td style="border:0px;">({{envPressureData[0].from_pressure+'-'+envPressureData[0].to_pressure}} {{envPressureData[0].globe_pressure_diff}})</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <table class="table table-bordered" style="border:1px;">
                                <tr>
                                    <th style="border:0px;">Block:</th>
                                    <td style="border:0px;">{{envData[0].block_code}}</td>
                                </tr>
                                <tr>
                                    <th style="border:0px;">Room/Line No:</th>
                                    <td style="border:0px;">{{envData[0].room_code}}</td>
                                </tr>
                                <tr>
                                    <th style="border:0px;">Month:</th>
                                    <td style="border:0px;"><div ng-if="envData.length>0">{{month_year}}</div></td>
                                </tr>
                                <tr>
                                    <th style="border:0px;">Hygrometer/Data logger ID:</th>
                                    <td style="border:0px;">{{envData[0].data_log_id}}</td>
                                </tr>
                                <tr>
                                    <th style="border:0px;">Magnehelic gauge ID:</th>
                                    <td style="border:0px;">{{envData[0].magnelic_id}}</td>
                                </tr>

                            </table>
                        </div>
                    </div>
                    <div class="form-row scroll-table" style="margin-top:10px;">
                        <table class="table custom-table">
                            <thead style="border-top:10px">
                                <tr>
                                    <th>Date </th>
                                    <th class="text-center" colspan="2">Time</th>
                                    <th class="text-center" colspan="2">Pressure Differential</th>
                                    <th>Temp(oC)</th>
                                    <th class="text-center" colspan="2">Temp. (oC)</th>
                                    <th>%RH</th>
                                    <th class="text-center" colspan="2">%RH</th>
                                    <th>Check By(Sign.)</th>
                                    <th>Review By(Sign.)</th>
                                    <th>Remarks</th>
                                    <th>ATTACHMENTS</th>
                                </tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th class="text-center" colspan="2"></th>
                                    <th class="text-center">Zero Ok</th>
                                    <th class="text-center">Reading</th>
                                    <th>&nbsp;</th>
                                    <th class="text-center">Min.</th>
                                    <th class="text-center">Max.</th>
                                    <th>&nbsp;</th>
                                    <th class="text-center">Min.</th>
                                    <th class="text-center">Max.</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="dataObj in envData track by $index">
                                    <td>{{dataObj.startdate}}</td>
                                    <td colspan="2">{{dataObj.starttime}}</td>
                                    <td>{{dataObj.pressure_diff}}</td>
                                    <td>{{dataObj.reading}}</td>
                                    <td>{{dataObj.temp}}</td>
                                    <td>{{dataObj.temp_min}}</td>
                                    <td>{{dataObj.temp_max}}</td>
                                    <td>{{dataObj.rh}}</td>
                                    <td>{{dataObj.rh_min}}</td>
                                    <td>{{dataObj.rh_max}}</td>
                                    <td>{{dataObj.startoperator}}<br>({{dataObj.startremark}})</td>
                                    <td>{{dataObj.stopcheckedby}}<br><span ng-if="dataObj.stopcheckedbyremark != ''">({{dataObj.stopcheckedbyremark}})</span></td>                                        
                                    <td>
                                        <span ng-repeat="remarkObj in dataObj.remark track by $index">{{remarkObj}}<br></span>
                                    </td>
                                    <td>
                                        <p ng-if="dataObj.logAttachments!=null" ng-repeat="attachObj in dataObj.logAttachments.split(',') track by $index"> <a href="<?php echo base_url() ?>assets/files/{{attachObj}}" target="_blank">{{attachObj}}</a></p><br>
                                    </td>
                                </tr>

                            </tbody>


                        </table>
                        <div class="col-lg-12 text-center" ng-show="envData.length == 0">
                            <h2>No Data Found</h2>
                        </div>
                        <div class="col-lg-12 text-center" ng-show="envData.length == 0">
                            <h4>(Please Select the above parameters)</h4>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <p>Note-Write NA where ever not applicable</p>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-6">
                            <p>Reviewed By Production</p>
                        </div>
                        <div class="col-md-6">
                            <p>Reviewed By IPQA</p>
                        </div>
                    </div>
                    <div class="form-row" style="margin-top:10px;" ng-show="headerData.length > 0">
                        <b>FORM NO.</b>&nbsp;{{headerData.length > 0?reportHeader.footer_name:'NA'}}
                    </div>
                    <div class="form-row pb-2" id="pdf_download" style="display:none">
                        <div class="col-md-4"></div>
                        <div class="col-md-3">Print By : <?php echo $this->session->userdata('empname') ?></div>
                        <div class="col-md-3">Date : <?php echo date("d-m-Y"); ?></div>
                        <div class="col-md-2">Time : <?php echo date("H:i:s"); ?></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

    <!-- Footer -->
    <footer class="sticky-footer bg-white">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
                <span>SMHS. &copy; 2020 - 2021. All Rights Reserved.</span>
            </div>
        </div>
    </footer>
    <!-- End of Footer -->

    </div> <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-primary" href="<?php echo base_url() ?>user/logout">Logout</a>
                </div>
            </div>
        </div>
    </div>
    <script type="text/kendo-template" id="page-template">
    #
        var footer_name = foot_name;
    #
    <div class="page-template">
        <div class="footer">
            <table class="table" style="color:black;">
                <tr>
                    <td>
                        <div class="form-row">
                            <div class="col-md-12">
                                <p>Note-Write NA where ever not applicable</p>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6">
                                <p>Reviewed By Production</p>
                            </div>
                            <div class="col-md-6">
                                <p>Reviewed By IPQA</p>
                            </div>
                        </div>
                        <div class="form-row" style="margin-top:10px;">
                            <b>FORM NO.</b>&nbsp;<div id="footer_no1">#=footer_name#</div>
                        </div>
                        <div class="row">
                            <div class="col-md-3"><b>Print By : <?php echo $this->session->userdata('empname') ?></b></div>
                            <div class="col-md-3"><b>Date : <?php echo date("d-M-Y"); ?></b></div>
                            <div class="col-md-3"><b>Time :<?php $timezone = "Asia/Kolkata";date_default_timezone_set($timezone);echo date("H:i:s");?></b></div>
                            <div class="col-md-3"><b>Page #: pageNum # of #: totalPages #</b></div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</script>

<div id="master_data_div" style="display: none;">
    <div class="card-body ht750" id="audit_trail2" style="margin-top:10px;padding-top:10px">                      
	<div class="form-row scroll-table" style="margin-top:10px;">
            <style>
                #tbldata { height: 20px !important;}
                #tbldata td {word-wrap:break-word; font-size: 15px;}
                .table th, table td, table td, table th {
                    font-size: 15px;
                    padding: 7px;
                    height: 25px!important;
                }
            </style>
            <table id="tbldata" border="1" style="width:100%;color:black;">
                <thead style="max-height:165px;">
                    <tr style="max-height:80px;">
                        <td colspan="15">
                            <div class="form-row">
                                <div class="col-md-2">
                                      <img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo">
                                </div>
                                <div class="col-md-10">
                                    <table class="table table-bordered" style="color:#000 !important">
                                        <tr>
                                            <td colspan="5"><b>FORM</b></td>
                                        </tr>
                                        <tr>
                                            <td><b>TITLE</b></td>
                                            <td colspan="4"><b>ENVIRONMENTAL CONDITION/PRESSURE DIFFERENTIAL RECORD</b></td>
                                        </tr>
                                        <tr>
                                            <th>Form No.</th>
                                            <th>Version No. & Status</th>
                                            <th>Effective Date</th>
                                            <th>Retired Date</th>
                                            <th>Document Reference No.</th>
                                        </tr>
                                        <tbody>
                                            <tr>
                                                <tr>
                                        <td>{{headerData.length > 0?reportHeader.form_no:'NA'}}</td>
                                        <td>{{headerData.length > 0?reportHeader.version_no+' & Effective':'NA'}} </td>
                                        <td>{{headerData.length > 0?(reportHeader.effective_date| date:'dd-MMM-yyyy'):'NA'}}</td>
                                        <td>{{headerData.length > 0?(reportHeader.retired_date| date:'dd-MMM-yyyy'):'NA'}}</td>
                                        <td>{{headerData.length > 0?reportHeader.document_no:'NA'}}</td>
                                    </tr>
                                           </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="form-row scroll-table" style="color:#000 !important;">
                                <div class="col-md-2">
                                </div>
                                <div class="col-md-6" style="color:#000 !important;">
                                    <table class="table" style="border:1px;">
                                <tr>
                                    <th colspan="2" style="border:0px;">ENVIRONMENTAL CONDITION/PRESSURE DIFFERENTIAL RECORD LIMIT:</th>

                                </tr>
                                <tr>
                                    <th style="border:0px;">Temperature:</th>
                                    <td style="border:0px;">From : {{envPressureData[0].temp_from}} To: {{envPressureData[0].temp_to}} ℃</td>
                                </tr>
                                <tr>
                                    <th style="border:0px;">Relative Humidity:</th>
                                    <td style="border:0px;">From : {{envPressureData[0].rh_from}} To: {{envPressureData[0].rh_to}} %</td>
                                </tr>
                                <tr>
                                    <th style="border:0px;">Pressure Differential(+):</th>
                                    <td style="border:0px;">({{envPressureData[0].from_pressure+'-'+envPressureData[0].to_pressure}} {{envPressureData[0].globe_pressure_diff}})</td>
                                </tr>
                            </table>
                                </div>
                                <div class="col-md-4">
                                   <table class="table table-bordered" style="border:1px; color:#000 !important;">
                                        <tbody>
                                            <tr>
                                                <th style="border:0px;">Block:</th>
                                                <td style="border:0px;">{{envData[0].block_code}}</td>
                                             </tr>
                                            <tr>
                                               <th style="border:0px;">Room/Line No:</th>
                                               <td style="border:0px;">{{envData[0].room_code}}</td>
                                            </tr>
                                            <tr>
                                               <th style="border:0px;">Month:</th>
                                               <td style="border:0px;"><div ng-if="envData.length>0">{{month_year}}</div></td>
                                            </tr>
                                            <tr>
                                               <th style="border:0px;">Hygrometer/Data logger ID:</th>
                                               <td style="border:0px;">{{envData[0].data_log_id}}</td>
                                            </tr>
                                            <tr>
                                               <th style="border:0px;">Magnehelic gauge ID:</th>
                                               <td style="border:0px;">{{envData[0].magnelic_id}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </td>
                     </tr>
                    <tr style="background-color:#4e73df;color:#fff;height:60px;">
                        <th>Date </th>
                        <th class="text-center" colspan="2">Time</th>
                        <th class="text-center" colspan="2">Pressure Differential</th>
                        <th>Temp(oC)</th>
                        <th class="text-center" colspan="2">Temp. (oC)</th>
                        <th>%RH</th>
                        <th class="text-center" colspan="2">%RH</th>
                        <th>Check By(Sign.)</th>
                        <th>Review By(Sign.)</th>
                        <th>Remarks</th>
                        <th>ATTACHMENTS</th>
                    </tr>
                    <tr style="background-color:#4e73df;color:#fff;height:60px;">
                        <th>&nbsp;</th>
                        <th class="text-center" colspan="2"></th>
                        <th class="text-center">Zero Ok</th>
                        <th class="text-center">Reading</th>
                        <th>&nbsp;</th>
                        <th class="text-center">Min.</th>
                        <th class="text-center">Max.</th>
                        <th>&nbsp;</th>
                        <th class="text-center">Min.</th>
                        <th class="text-center">Max.</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="max-height:100px" ng-repeat="dataObj in envData track by $index">
                        <td>{{dataObj.startdate}}</td>
                        <td colspan="2">{{dataObj.starttime}}</td>
                        <td>{{dataObj.pressure_diff}}</td>
                        <td>{{dataObj.reading}}</td>
                        <td>{{dataObj.temp}}</td>
                        <td>{{dataObj.temp_min}}</td>
                        <td>{{dataObj.temp_max}}</td>
                        <td>{{dataObj.rh}}</td>
                        <td>{{dataObj.rh_min}}</td>
                        <td>{{dataObj.rh_max}}</td>
                        <td>{{dataObj.startoperator}}<br>({{dataObj.startremark}})</td>
                        <td>{{dataObj.stopcheckedby}}<br><span ng-if="dataObj.stopcheckedbyremark != ''">({{dataObj.stopcheckedbyremark}})</span></td>                                        
                        <td style="height: 180px!important; text-overflow: ellipsis; overflow: hidden; display: block;">
                            <span ng-repeat="remarkObj in dataObj.remark track by $index">{{remarkObj}}<br></span>
                        </td>
                        <td>
                            <p ng-if="dataObj.logAttachments!=null" ng-repeat="attachObj in dataObj.logAttachments.split(',') track by $index"> <a href="<?php echo base_url() ?>assets/files/{{attachObj}}" target="_blank">{{attachObj}}</a></p><br>
                        </td>
                    </tr>
                </tbody>
            </table>
	</div>
    </div>
</div>

<style>
    /* Page Template for the exported PDF */
    .page-template {
      font-family: "DejaVu Sans", "Arial", "sans-serif";
      position: absolute;
      width: 100%;
      height: 100%;
      color: black;
      top: 0;
      left: 0;
    }
    .page-template .header {
      position: absolute;
      top: 0px;
      left: 30px;
      right: 30px;
      color: black;
      bottom:20px;
      margin-bottom: 10px;
      padding-bottom: 10px;
    }
    .page-template .footer {
      position: absolute;
      bottom: 0px;
      left: 30px;
      right: 30px;
      border-top: 1px solid black;
      text-align: center;
      color: #000;
    }
    .k-grid-header .k-header {
        height: 10px;
        padding: 0;
      }

      .k-grid tbody tr {
        line-height: 14px !important;
      }

      .k-grid tbody td {
        padding: 0;
      }
</style>
    <!-- Bootstrap core JavaScript-->
    
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
    <!-- <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->
<!--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
    <script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>js/angular.min.js"></script>
    <script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
    <script src="<?php echo base_url() ?>js/lodash.min.js"></script>
    <script src="<?php echo base_url() ?>js/angularjs-dropdown-multiselect.min.js"></script>
    <script src="<?php echo base_url() ?>js/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>js/kendo.all.min.js"></script>
    <script src="<?php echo base_url() ?>js/moment-with-locales.js"></script>
    <script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
    <script type="text/javascript">
        var foot_name = '';
        var url_base_path = '<?php echo base_url() ?>';
        var app = angular.module("reportApp", ['angular.chosen', 'moment-picker']);
        app.directive('chosen', function($timeout) {

            var linker = function(scope, element, attr) {

                scope.$watch('sysRoomData', function() {
                    $timeout(function() {
                        element.trigger('chosen:updated');
                    }, 0, false);
                }, true);

                $timeout(function() {
                    element.chosen();
                }, 0, false);
            };
            return {
                restrict: 'A',
                link: linker
            };
        });
        app.controller("reportCtrl", function($scope, $http, $filter) {
            $scope.sysRoomData = [];
            $scope.current_date = moment().format('DD-MMM-YYYY');
            $scope.start_date = moment().format('DD-MMM-YYYY');
            $scope.end_date = moment().format('DD-MMM-YYYY');
            $scope.month = $scope.year = '';$scope.month_year = '';
            var abc = moment(moment($scope.start_date, 'DD-MMM-YYYY')).format('YYYY-MM-DD');
            $scope.month = moment(abc).format('MMM');
            $scope.year = moment(abc).format('YYYY');
            $scope.getMonthName = function () {
                $scope.month = moment(abc).format('MMM');
                $scope.year = moment(abc).format('YYYY');
                $scope.month_year = $scope.month+'-'+$scope.year;
//                if (!$scope.dateRangeCheck($scope.start_date, $scope.end_date)) {
//                    $scope.month = $scope.year = $scope.month_year = '';
//                }
            }
            $scope.getDeviceRoomList = function() {
                $http({
                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/Getassignedroomlist',
                    method: "GET",
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).then(function(response) {
                    $scope.sysRoomData = response.data.room_list;
                }, function(error) { // optional

                    console.log("Something went wrong.Please try again");

                });
            }
            $scope.getDeviceRoomList();
            // $scope.headerData = [];
            // $scope.reportHeader = {};
            // $scope.getHeaderList = function () {
            //     $http({
            //         url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getHeaderList?report_id=<?php echo $_GET['module_id'] ?>',
            //         method: "GET",
            //         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            //     }).then(function (response) {
            //         $scope.headerData = response.data.header_list;
            //         $scope.reportHeader = response.data.header_list[0];
            //     }, function (error) { // optional
            //         console.log("Something went wrong.Please try again");
            //     });
            // }
            // $scope.getHeaderList();

            // checking header
            $scope.headerData = [];
            $scope.reportHeader = {};
            $scope.getHeaderList = function() {
                if ($scope.start_date == "" || $scope.start_date == undefined || $scope.end_date == "" || $scope.end_date == undefined) {
                    alert("Please Select the date range first...!");
                    return false;
                }
                $http({
                    url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/getHeaderByLogid',
                    method: "POST",
                    data: "header_id=" + <?php echo $_GET['mst_act_id'] ?> + "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date,
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                }).then(function(response) {
                    $scope.headerData = response.data.header_list;
                    if ($scope.headerData.length > 0) {
                        if ($scope.headerData.length > 1) {
                            alert("There are more than one header in this date range please change it, Thank you...!");
                            $scope.headerData = [];
                            return false;
                        } else {
                            $scope.reportHeader = response.data.header_list[0];
                            
                            foot_name = $scope.reportHeader.footer_name;
                            $scope.envPressureReportData();
                        }
                    } else {
                        alert("No version is effective in selected date range, please change the dates or check with administrator for \"Report Header/Footer Master\"");
                        $scope.headerData = [];
                        return false;
                    }
                }, function(error) { // optional
                    console.log("Something went wrong.Please try again");
                });
            }
            //End

            $scope.envPressureReportData = function() {
                var valid = true;
                if (($scope.room_no == "" || $scope.room_no == undefined) && ($scope.start_date == "" || $scope.start_date == undefined) && ($scope.end_date == "" || $scope.end_date == undefined)) {
                    valid = false;
                }
                if (valid) {
                    $scope.envData = [];
                    $http({
                        url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/envPressureReport',
                        method: "POST",
                        data: "room_no=" + $scope.room_no + "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date,
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        },
                    }).then(function(response) {
                        $scope.envPressureData = response.data.row_data;
                        var startoperator = "";
                        var startremark = "";
                        var startdate = "";
                        var starttime = "";
                        var stopoperator = "";
                        var stopremark = "";
                        var stopdate = "";
                        var stoptime = "";
                        var startcheckedby = "";
                        var startcheckedbyremark = "";
                        var stopcheckedby = "";
                        var stopcheckedbyremark = "";
                        var isworkflow = "no";
                        var remark = "";
                        var logAttachments = "";
                        var qa = 'NA';
                        var qaremark = 'NA';
                        var data_log_id = '';
                        var magnelic_id = '';
                        var room_code = '';
                        var block_code = '';
                        angular.forEach($scope.envPressureData, function(value, key) {

                            if (value['workflow_type'] == 'start') {
                                if (value['approval_status'] == 'N/A') {
                                    startoperator = value['user_name'];
                                    startdate = value['start_date'];
                                    starttime = value['start_time'];
                                    room_code = value['room_code'];
                                    block_code = value['block_code'];
                                    isworkflow = "no";
                                    data_log_id = value['data_log_id'];
                                    magnelic_id = value['magnelic_id'];
                                    startremark = value['activity_time'];
                                    remark = value['user_name'] + ':-' + value['activity_remarks'];
                                    logAttachments = value['logAttachments'];
                                    stopdate = "";
                                    stoptime = "";
                                    var tempStr = remark.split("~");
                                    var uniqueNames = getUnique(tempStr);
                                    var temp = {
                                        "room_code":room_code,
                                        "block_code":block_code,
                                        "data_log_id":data_log_id,
                                        "magnelic_id":magnelic_id,
                                        "startdate": startdate,
                                        "starttime": starttime,
                                        "stopdate": stopdate,
                                        "stoptime": stoptime,
                                        "activity": value['activity_name'],
                                        "startoperator": startoperator,
                                        "startremark": startremark,
                                        "startcheckedby": startcheckedby,
                                        "startcheckedbyremark": startcheckedbyremark,
                                        "stopoperator": stopoperator,
                                        "stopremark": stopremark,
                                        "stopcheckedby": stopcheckedby,
                                        "stopcheckedbyremark": stopcheckedbyremark,
                                        "remark": uniqueNames,
                                        "pressure_diff": value['pressure_diff'],
                                        "reading": value['reading'],
                                        "temp": value['temp'],
                                        "temp_min": value['temp_min'],
                                        "temp_max": value['temp_min'],
                                        "rh": value['rh'],
                                        "rh_min": value['rh_min'],
                                        "rh_max": value['rh_max'],
                                        "logAttachments": logAttachments,
                                        "qa": value['user_name']
                                    }
                                    $scope.envData[value['id']] = temp;
                                } else {
                                    startcheckedby = value['user_name'];
                                    isworkflow = "yes";
                                    startcheckedbyremark = value['activity_time'];
                                    remark = remark + '~' + value['user_name'] + ':-' + value['activity_remarks'];
                                }
                            } else if (value['workflow_type'] == 'stop') {
                                if (value['approval_status'] == 'N/A') {
                                    stopoperator = value['user_name'];
                                    stopdate = value['stop_date'];
                                    stoptime = value['stop_time'];
                                    isworkflow = "no";
                                    stopremark = value['activity_time'];
                                    logAttachments = value['logAttachments'];
                                    data_log_id = value['data_log_id'];
                                    magnelic_id = value['magnelic_id'];
                                    room_code = value['room_code'];
                                    block_code = value['block_code'];
                                    var tempStr = remark.split("~");
                                    var uniqueNames = getUnique(tempStr);
                                    var temp = {
                                        "room_code":room_code,
                                        "block_code":block_code,
                                        "startdate": startdate,
                                        "starttime": starttime,
                                        "data_log_id":data_log_id,
                                        "magnelic_id":magnelic_id,
                                        "stopdate": stopdate,
                                        "stoptime": stoptime,
                                        "activity": value['activity_name'],
                                        "startoperator": startoperator,
                                        "startremark": startremark,
                                        "startcheckedby": startcheckedby,
                                        "startcheckedbyremark": startcheckedbyremark,
                                        "stopoperator": stopoperator,
                                        "stopremark": stopremark,
                                        "stopcheckedby": stopcheckedby,
                                        "stopcheckedbyremark": stopcheckedbyremark,
                                        "remark": uniqueNames,
                                        "pressure_diff": value['pressure_diff'],
                                        "reading": value['reading'],
                                        "temp": value['temp'],
                                        "temp_min": value['temp_min'],
                                        "temp_max": value['temp_min'],
                                        "rh": value['rh'],
                                        "rh_min": value['rh_min'],
                                        "rh_max": value['rh_max'],
                                        "logAttachments": logAttachments,
                                        "qa": value['user_name']
                                    }
                                    $scope.envData[value['id']] = temp;
                                }else if (value['approval_status'] == 'QAapprove') {
                                    qa = value['user_name'];
                                    isworkflow = "no";
                                    stopremark = value['activity_time'];
                                    logAttachments = value['logAttachments'];
                                    remark = remark + "~" + value['user_name'] + ':-' + value['activity_remarks'];
                                    data_log_id = value['data_log_id'];
                                    magnelic_id = value['magnelic_id'];
                                    room_code = value['room_code'];
                                    block_code = value['block_code'];
                                    var tempStr = remark.split("~");
                                    var uniqueNames = getUnique(tempStr);
                                    var temp = {
                                        "room_code":room_code,
                                        "block_code":block_code,
                                        "magnelic_id":magnelic_id,
                                        "data_log_id":data_log_id,
                                        "startdate": startdate,
                                        "starttime": starttime,
                                        "stopdate": stopdate,
                                        "stoptime": stoptime,
                                        "activity": value['activity_name'],
                                        "startoperator": startoperator,
                                        "startremark": startremark,
                                        "startcheckedby": startcheckedby,
                                        "startcheckedbyremark": startcheckedbyremark,
                                        "stopoperator": stopoperator,
                                        "stopremark": stopremark,
                                        "stopcheckedby": stopcheckedby,
                                        "stopcheckedbyremark": stopcheckedbyremark,
                                        "remark": uniqueNames,
                                        "pressure_diff": value['pressure_diff'],
                                        "reading": value['reading'],
                                        "temp": value['temp'],
                                        "temp_min": value['temp_min'],
                                        "temp_max": value['temp_min'],
                                        "rh": value['rh'],
                                        "rh_min": value['rh_min'],
                                        "rh_max": value['rh_max'],
                                        "logAttachments": logAttachments,
                                        "qa": value['user_name']
                                    }
                                    $scope.envData[value['id']] = temp;
                                } else {
                                    stopcheckedby = value['user_name'];
                                    isworkflow = "yes";
                                    stopdate = value['stop_date'];
                                    stoptime = value['stop_time'];
                                    stopcheckedbyremark = value['activity_time'];
                                    remark = remark + "~" + value['user_name'] + ':-' + value['activity_remarks'];
                                    logAttachments = value['logAttachments'];
                                    data_log_id = value['data_log_id'];
                                    magnelic_id = value['magnelic_id'];
                                    room_code = value['room_code'];
                                    block_code = value['block_code'];
                                    var tempStr = remark.split("~");
                                    var uniqueNames = getUnique(tempStr);
                                    var temp = {
                                        "startdate": startdate,
                                        "starttime": starttime,
                                        "stopdate": stopdate,
                                        "stoptime": stoptime,
                                        "room_code":room_code,
                                        "block_code":block_code,
                                        "magnelic_id":magnelic_id,
                                        "data_log_id":data_log_id,
                                        "activity": value['activity_name'],
                                        "startoperator": startoperator,
                                        "startremark": startremark,
                                        "startcheckedby": startcheckedby,
                                        "startcheckedbyremark": startcheckedbyremark,
                                        "stopoperator": stopoperator,
                                        "stopremark": stopremark,
                                        "stopcheckedby": stopcheckedby,
                                        "stopcheckedbyremark": stopcheckedbyremark,
                                        "remark": uniqueNames,
                                        "pressure_diff": value['pressure_diff'],
                                        "reading": value['reading'],
                                        "temp": value['temp'],
                                        "temp_min": value['temp_min'],
                                        "temp_max": value['temp_min'],
                                        "rh": value['rh'],
                                        "rh_min": value['rh_min'],
                                        "rh_max": value['rh_max'],
                                        "logAttachments": logAttachments,
                                        "qa": value['user_name']
                                    }
                                    $scope.envData[value['id']] = temp;
                                }
                            } else {
                                if (isworkflow == "no") {
                                    startcheckedby = "";
                                    stopcheckedby = "";
                                    stopcheckedbyremark= "";
                                }
                                remark = remark + "~" + value['user_name'] + ':-' + value['activity_remarks'];
                                logAttachments = value['logAttachments'];
                                data_log_id = value['data_log_id'];
                                magnelic_id = value['magnelic_id'];
                                room_code = value['room_code'];
                                block_code = value['block_code'];
                                var tempStr = remark.split("~");
                                var uniqueNames = getUnique(tempStr);
                                var temp = {
                                    "data_log_id":data_log_id,
                                    "block_cde":block_code,
                                    "room_code":room_code,
                                    "magnelic_id":magnelic_id,
                                    "startdate": startdate,
                                    "starttime": starttime,
                                    "stopdate": stopdate,
                                    "stoptime": stoptime,
                                    "activity": value['activity_name'],
                                    "startoperator": startoperator,
                                    "startremark": startremark,
                                    "startcheckedby": startcheckedby,
                                    "startcheckedbyremark": startcheckedbyremark,
                                    "stopoperator": stopoperator,
                                    "stopremark": stopremark,
                                    "stopcheckedby": stopcheckedby,
                                    "stopcheckedbyremark": stopcheckedbyremark,
                                    "remark": uniqueNames,
                                    "pressure_diff": value['pressure_diff'],
                                    "reading": value['reading'],
                                    "temp": value['temp'],
                                    "temp_min": value['temp_min'],
                                    "temp_max": value['temp_min'],
                                    "rh": value['rh'],
                                    "rh_min": value['rh_min'],
                                    "rh_max": value['rh_max'],
                                    "logAttachments": logAttachments
                                }
                                $scope.envData[value['id']] = temp;
                            }
                        });

                        //console.log('===',$scope.envData);
                        $scope.envData = $scope.envData.filter(function() {
                            return true
                        });
                        console.log($scope.envData);
                        $scope.getMonthName();
                    }, function(error) { // optional

                        toaster.pop('error', "error", "Something went wrong.Please try again");

                    });
                } else {

                    alert("Please Select Room no/date range");

                }
            }

            $scope.envPressureData = [];

            $scope.envData = [];

            function getUnique(array) {
                var uniqueArray = [];

                // Loop through array values
                for (i = 0; i < array.length; i++) {
                    if (uniqueArray.indexOf(array[i]) === -1) {
                        uniqueArray.push(array[i]);
                    }
                }
                return uniqueArray;
            }
        });

        function ExportPdf() {
            $("#master_data_div").show();
            kendo.drawing.drawDOM("#master_data_div",{
                paperSize: "A3",
                landscape: true,
                pageable: true,
                repeatHeaders:true,
                template: $("#page-template").html(),
                margin: {top: "10px", bottom: "10px"},
                color:"black",
                //padding: {top: "40px", bottom: "40px"},
                scale: 0.7,
                height: 1200
            }).then(function (group) {
                kendo.drawing.pdf.saveAs(group,"environmental_pressure.pdf")
            }).always(function () {
                //$("#pdf_download").hide();
                $("#master_data_div").hide();
            });
        }
    </script>
    <script src="<?php echo base_url(); ?>assets/js/disable_editor.js"></script>
</body>

</html>