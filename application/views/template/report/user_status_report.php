<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'report', "status" => 'active'))->row_array();
$user_date = $this->session->userdata();
$show = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $show = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_edit'] != 1) {
            $url = base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '" . $url . "';\",1);</script>";
        } elseif ($res['is_edit'] == 1) {
            $show = true;
        } else {
            $show = false;
        }
    } else {
        $url = base_url() . 'User/home';
        echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
        echo "<script>setTimeout(\"location.href = '" . $url . "';\",3000);</script>";
    }
}
$this->db->order_by("id", "DESC");
$footerData = $this->db->get_where("pts_mst_report_footer", array("report_id" => $_GET['module_id'], "status" => "active"))->result_array();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="smhs" content="">
        <title>E-Logbook</title>
        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url() ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>css/fontfamily.css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="<?php echo base_url() ?>css/sb-admin-2.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/custom.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
        <style>
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
            .pagination {
                display: inline-block;
                padding-left: 0;
                margin: 20px 0;
                border-radius: 4px;
            }
            .pagination > li {
                display: inline;
            }
        </style>
    </head>
    <body id="page-top" ng-app="UserStatusReportApp" ng-controller="UserSatusReportCtrl" ng-cloak>
        <!-- Page Wrapper -->
        <div id="wrapper">
            <?php $this->load->view("template/report/report_header"); ?>
            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="content-wrapper">
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="col-auto">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-4 text-center"><h2>User Login & Logout report</h2></div><div class="col-lg-4"></div>
                                </div>
                                <div class="form-row">
                                    <div class="col-lg-3 mb-3">
                                        <label>Emp Name <span style="color: red"></span></label>
                                        <select class="chzn-select form-control" tabindex="4"  name="emp_name" ng-model="emp_name" data-placeholder="Search Emp name" id="emp_name" required chosen>
                                            <option value="" selected>Select Emp Name</option>
                                            <option value="{{dataObj.id}}" ng-repeat="dataObj in empDetailsArr">{{dataObj.emp_email}}=>{{dataObj.emp_name}}</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label>Block Code<span style="color: red"></span></label>
                                        <select class="chzn-select form-control" tabindex="4"  name="block_code" ng-model="block_code" data-placeholder="Search Block Code" id="block_codes" ng-options="dataObj['block_code'] as dataObj.block_code for dataObj in blockDetailsArr" required chosen>
                                            <option value="" selected>Select Block code</option>
                                        </select>
                                    </div>			
                                    <div class="col-lg-2 mb-3"><label><b>Start Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-12 ng-isolate-scope" moment-picker="start_date" set-on-select = "true" format="DD-MMM-YYYY" start-view="month" locale="en" today="true" max-date="current_date">
                                                <input class="form-control ng-pristine ng-valid ng-scope moment-picker-input ng-empty ng-touched" name="start_date" placeholder="Select Start Date" ng-model="start_date" ng-model-options="{updateOn: 'blur'}" tabindex="0" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 mb-2"><label><b>End Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-12 ng-isolate-scope" moment-picker="end_date" set-on-select = "true" format="DD-MMM-YYYY"  start-view="month" min-date="start_date" locale="en" today="true" max-date="current_date">
                                                <input class="form-control  ng-pristine ng-valid ng-scope moment-picker-input ng-empty ng-touched" name="end_date" placeholder="Select End Date" ng-model="end_date" ng-model-options="{updateOn: 'blur'}" tabindex="0" autocomplete="off" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 mb-3"><label></label>
                                        <button  class="btn btn-success" style="margin-top: 25px;" ng-click="getUserStatusDetails(pageno);getUserStatusDetailsAll()">Go</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card card-default mt-4">
                    <div class="form-row">
                        <div class="col-lg-10 text-center"></div>
                        <?php if ($show) { ?>
                            <div class="col-lg-2" ng-if="prolog_data.length > 0"><button style="margin-top:5px;"  onclick="ExportPdf()"><i class="fas fa-file-pdf" style="color:red;"></i></button></div>
                        <?php } ?>
                    </div>

                    <div class="card-body" id="user_status_report2" >
                        <div class="form-row">
                            <div class="col-lg-4"></div>
                            <div class="col-lg-4 text-center"><h2>User Login & Logout report</h2></div><div class="col-lg-4"></div>
                        </div>
                        <div class="form-row scroll-table" style="margin-top:10px;">
                            <table class="table custom-table" >
                                <thead>
                                    <tr>
                                        <th>EMP NAME</th>
                                        <th>USER ID</th>
                                        <th>ROOM CODE</th>
                                        <th>ROOM NAME</th>
                                        <th>LOGIN TIME</th>
                                        <th>LOGOUT TIME</th>
                                    </tr>
                                </thead>
                                <tbody >
                                    <tr dir-paginate="dataobj in prolog_data |itemsPerPage:itemsPerPage" total-items="total_count">
                                        <td>{{dataobj.emp_name}}</td>
                                        <td>{{dataobj.emp_email}}</td>
                                        <td>{{dataobj.room_code}}</td>
                                        <td>{{dataobj.room_name}}</td>
                                        <td>{{getDateConverter(dataobj.room_in_time)}}</td>
                                        <td>{{dataobj.room_out_time > 0 ? getDateConverter(dataobj.room_out_time) : 'NA'}}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="col-lg-12" id="pagination-div">
                                <dir-pagination-controls
                                    max-size="8"
                                    direction-links="true"
                                    boundary-links="true" 
                                    on-page-change="getUserStatusDetails(newPageNumber)" >
                                </dir-pagination-controls>
                            </div>
                            <div class="col-lg-12 text-center" ng-show="prolog_data.length == 0"><h2>No Data Found</h2></div>
                            <div class="col-lg-12 text-center" ng-show="prolog_data.length == 0"><h4>(Please Select the above parameters)</h4></div>  
                        </div>
                        <br><br>
                        <!--                        <div class="form-row" ng-if="prolog_data.length > 0">
                        <?php date_default_timezone_set("Asia/Kolkata"); ?>
                                                    <div class="col-md-3" id="pdf_download2" style="display:none"><b>User Name</b> : <?php //echo $this->session->userdata('empname')  ?></div>
                                                    <div class="col-md-4 text-center"><b>Printed By:</b> <?php echo $this->session->userdata('empcode') ?></div>
                                                    <div class="col-md-4 text-center"><b>Print Date:</b> <?php echo date('Y-m-d', $user_date['__ci_last_regenerate']) ?></div>
                                                    <div class="col-md-4 text-center"><b>Print Time:</b> <?php echo date('H:i:s', $user_date['__ci_last_regenerate']); ?></div>
                                                </div>-->
                        <?php if (!empty($footerData)) { ?>
                            <div class="form-row" style="margin-top:10px;"><b>FORM NO.</b>&nbsp;<?php echo $footerData[0]['footer_name'] ?></div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- End of Main Content -->
        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>SMHS. &copy; 2020 - 2021. All Rights Reserved.</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div1>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="<?php echo base_url() ?>user/logout">Logout</a>
            </div>
        </div>
    </div>
</div>
<div id="master_data_div" style="display: none">
    <div class="card-body ht750" id="audit_trail2" style="margin-top:10px;">
        <div class="form-row" style="margin-top:10px;">
            <style>
            #tbldata {border-collapse:collapse; table-layout:fixed; width:310px; height: 100px !important;}
                #tbldata td {width:100px;word-wrap:break-word; height:100px;font-size: 20px;color:black}
            </style>
            <table id="tbldata" border="1" style="width:100%;">
                <thead style="max-height:135px;">
                    <tr style="max-height:60px;">
                        <td colspan="6">
                            <div class="form-row">
                                <div class="col-lg-4" style="margin-left:40px;"><img src="<?= base_url('assets/images/sun-pharma-logo.jpg') ?>" class="report-logo"></div>
                                <div class="col-lg-4 text-center" style="margin-top:50px;"><h2>User Login & Logout report</h2></div><div class="col-lg-4"></div>
                            </div>
                        </td>
                    </tr>
                    <tr style="background-color: #4e73df;color: #fff; max-height: 60px;">
                        <th width="20%">EMP NAME</th>
                        <th width="10%">USER ID</th>
                        <th width="10%">ROOM CODE</th>
                        <th width="20%">ROOM NAME</th>
                        <th width="20%">LOGIN TIME</th>
                        <th width="20%">LOGOUT TIME</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="max-height:100px" ng-repeat="dataobj in prolog_data_all">
                        <td>{{dataobj.emp_name}}</td>
                        <td>{{dataobj.emp_email}}</td>
                        <td>{{dataobj.room_code}}</td>
                        <td>{{dataobj.room_name}}</td>
                        <td>{{getDateConverter(dataobj.room_in_time)}}</td>
                        <td>{{dataobj.room_out_time > 0 ? getDateConverter(dataobj.room_out_time) : 'NA'}}</td>
                    </tr>
                </tbody>                                
            </table>
        </div>
    </div>
</div>
<script type="x/kendo-template" id="page-template">
    <div class="page-template">
        <div class="footer">
            <div class="form-row">
                <div class="col-md-3"><b>Print By : <?php echo $this->session->userdata('empname') ?></b></div>
                <div class="col-md-3"><b>Date : <?php echo date("d-M-Y"); ?></b></div>
                <div class="col-md-3"><b>Time :<?php $timezone = "Asia/Kolkata";date_default_timezone_set($timezone);echo date("H:i:s"); ?></b></div>
                <div class="col-md-3"><b>Page #: pageNum # of #: totalPages #</b></div>
            </div>
                
        </div>
    </div>
</script>
<style>
    /* Page Template for the exported PDF */
    .page-template {
      font-family: "DejaVu Sans", "Arial", "sans-serif";
      position: absolute;
      width: 100%;
      height: 100%;
      color: black;
      top: 0;
      left: 0;
    }
    .page-template .footer {
      position: absolute;
      bottom: 20px;
      left: 30px;
      right: 30px;
      border-top: 1px solid black;
      text-align: center;
      color: #000000;
    }
    .k-grid-header .k-header {
        height: 0px;
        padding: 0;
      }

      .k-grid tbody tr {
        line-height: 14px !important;
      }

      .k-grid tbody td {
        padding: 0;
      }
</style>
<!-- Bootstrap core JavaScript-->

<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>-->
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="<?php echo base_url() ?>js/lodash.min.js"></script>
<script src="<?php echo base_url() ?>js/angularjs-dropdown-multiselect.min.js"></script>
<script src="<?php echo base_url() ?>js/jszip.min.js"></script>
<script src="<?php echo base_url() ?>js/kendo.all.min.js"></script>
<script src="<?php echo base_url() ?>js/moment-with-locales.js"></script>
<script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
<script src="<?php echo base_url('assets/js/dirPagination.js') ?>"></script>
<script type="text/javascript">
                            var app = angular.module("UserStatusReportApp", ['angular.chosen', 'moment-picker', 'angularUtils.directives.dirPagination']);
                            app.directive('chosen', function ($timeout) {
                                var linker = function (scope, element, attr) {
                                    scope.$watch('empDetailsArr', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    scope.$watch('blockDetailsArr', function () {
                                        $timeout(function () {
                                            element.trigger('chosen:updated');
                                        }, 0, false);
                                    }, true);
                                    $timeout(function () {
                                        element.chosen();
                                    }, 0, false);
                                };
                                return {
                                    restrict: 'A',
                                    link: linker
                                };
                            });
                            app.controller("UserSatusReportCtrl", function ($scope, $http, $filter) {
                                $scope.prolog_data = [];
                                $scope.prolog_data_all = [];
                                $scope.start_date = moment().format('DD-MMM-YYYY');
                                $scope.current_date = moment().format('DD-MMM-YYYY');
                                $scope.end_date = moment().format('DD-MMM-YYYY');
                                $scope.emp_name = "";
                                $scope.block_code = "";
                                $scope.blockDetailsArr = "";
                                $scope.empDetailsArr = "";
                                $scope.pageno = 1; // initialize page no to 1
                                $scope.total_count = 0;
                                $scope.itemsPerPage = 10; //this could be a dynamic value from a drop down

                                $scope.getBlockDetails = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/fetchBlockDetails',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.blockDetailsArr = response.data;
                                    }, function (error) {
                                        console.log("Something went wrong.Please try again");
                                    });
                                }
                                $scope.getBlockDetails();
                                $scope.getEmpDetails = function () {
                                    $http({
                                        url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/fetchEmpDetails',
                                        method: "GET",
                                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                    }).then(function (response) {
                                        $scope.empDetailsArr = response.data;
                                    }, function (error) {
                                        console.log("Something went wrong.Please try again");
                                    });
                                }

                                $scope.getEmpDetails();
                                $scope.getUserStatusDetails = function (pageno) {
                                    $scope.prolog_data = [];
                                    var valid = true;
                                    if ($scope.start_date == '' && $scope.end_date == '' && $scope.block_code == '' && $scope.emp_name == '') {
                                        alert('Please Select At Least One parameter');
                                        valid = false;
                                    }
                                    if (valid) {
                                        var datefilter = '';
                                        $scope.block_code;
                                        $scope.emp_name;
                                        datefilter = "start_date=" + $scope.start_date + "&end_date=" + $scope.end_date + "&block_code=" + $scope.block_code + "&emp_name=" + $scope.emp_name;
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/userStatusReport?page_limit='+ $scope.itemsPerPage + "&page_no=" + pageno,
                                            method: "POST",
                                            data: datefilter,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.prolog_data = response.data.row_data;
                                            $scope.total_count = response.data.total_count;
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");
                                        });
                                }
                                }
                                $scope.getUserStatusDetailsAll = function () {
                                    $scope.prolog_data_all = [];
                                    var valid = true;
                                    if ($scope.start_date == '' && $scope.end_date == '' && $scope.block_code == '' && $scope.emp_name == '') {
                                        alert('Please Select At Least One parameter');
                                        valid = false;
                                    }
                                    if (valid) {
                                        var datefilter = '';
                                        $scope.block_code;
                                        $scope.emp_name;
                                        datefilter = "start_date=" + $scope.start_date + "&end_date=" + $scope.end_date + "&block_code=" + $scope.block_code + "&emp_name=" + $scope.emp_name;
                                        $http({
                                            url: '<?php echo base_url() ?>Rest/Pontasahibelog/Pontasahib/userStatusReport',
                                            method: "POST",
                                            data: datefilter,
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.prolog_data_all = response.data.row_data;
                                            //$scope.total_count = response.data.total_count;
                                        }, function (error) { // optional
                                            console.log("Something went wrong.Please try again");
                                        });
                                }
                                }
                                $scope.getUserStatusDetails($scope.pageno);
                                $scope.getUserStatusDetailsAll();
                                $scope.getDateConverter = function (date) {
                                    var a = new Date(date * 1000);
                                    if (date == null) {
                                        return '';
                                    }
                                    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                                    var year = a.getFullYear();
                                    var month = months[a.getMonth()];
                                    var date = a.getDate();
                                    var hour = a.getHours();
                                    var min = a.getMinutes();
                                    var sec = a.getSeconds();
                                    var time = date + '-' + month + '-' + year + ' ' + ("0" + hour).slice(-2) + ':' + ("0" + min).slice(-2) + ':' + ("0" + sec).slice(-2);
                                    return time;
                                }
                            });
                            function ExportPdf() {
//        //$("#pdf_download2").show();
//        $("#user_status_report2").show();
//        kendo.drawing.drawDOM("#user_status_report2",{
//            paperSize: "A4",
//            landscape: true,
//            sortable: true,
//            pageable: true,
//            margin: {top: "1cm", bottom: "1cm"},
//            scale: 0.5,
//            height: 500
//        })
//        .then(function (group) {
//            kendo.drawing.pdf.saveAs(group, "user_status_report2.pdf")
//        }).always(function(){
//            //$("#pdf_download2").hide();
//        });
                                $("#master_data_div").show();
                                kendo.drawing.drawDOM("#master_data_div", {
                                    paperSize: "A3",
                                    landscape: true,
                                    pageable: true,
                                    repeatHeaders: true,
                                    template: $("#page-template").html(),
                                    margin: {top: "10px", bottom: "60px"},
                                    //padding: {top: "40px", bottom: "40px"},
                                    scale: 0.6,
                                    height: 1700
                                }).then(function (group) {
                                    kendo.drawing.pdf.saveAs(group, "user_login_logout_report.pdf")
                                }).always(function () {
                                    $("#master_data_div").hide();
                                });
                            }
</script>
<script src="<?php echo base_url(); ?>assets/js/disable_editor.js"></script>
<script>
                            function printDiv() {
                                $("#user_status_report2").show();
                                var divToPrint = document.getElementById('user_status_report2');
                                var newWin = window.open('', 'Print-Window');
                                newWin.document.open();
                                newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
                                newWin.document.close();
                                setTimeout(function () {
                                    newWin.close();
                                }, 1000);
                                $("#user_status_report2").hide();
                            }
</script>
</body>

</html>
