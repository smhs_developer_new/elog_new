<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$res = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $this->session->userdata('user_id'), "module_id" => $_GET['module_id'], "module_type" => 'report',"status"=>'active'))->row_array();
$show = false;
if ($this->session->userdata('role_description') == 'Super Admin') {
    $show = true;
} else {
    if (!empty($res)) {
        if ($res['is_view'] == 0 && $res['is_edit']!=1) {
            $url=base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '".$url."';\",1);</script>";
        } elseif ($res['is_edit'] == 1) {
            $show = true;
        } else {
            $show = false;
        }
    } else {
        $url=base_url() . 'User/home';
            echo '<script>alert("You have no Role and Responsibility to acces this Report")</script>';
            echo "<script>setTimeout(\"location.href = '".$url."';\",3000);</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="smhs" content="">

        <title>E-Logbook</title>

        <!-- Custom fonts for this template-->
        <link href="<?php echo base_url() ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="<?php echo base_url() ?>css/sb-admin-2.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>css/custom.css" rel="stylesheet">

        <link rel="stylesheet" href="<?php echo base_url() ?>css/chosen.min.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-datepicker.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>css/angular-moment-picker.min.css" rel="stylesheet">
        <style>
            [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
                display: none !important;
            }
            .pagination {
                display: inline-block;
                padding-left: 0;
                margin: 20px 0;
                border-radius: 4px;
            }
            .pagination > li {
                display: inline;
            }
        </style>


<!-- <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css" /> -->
    </head>

    <body id="page-top" ng-app="reportApp" ng-controller="reportCtrl" ng-cloak>

        <!-- Page Wrapper -->
        <div id="wrapper">

            <?php $this->load->view("template/report/report_header"); ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">
                <!-- Page Heading -->

                <div class="content-wrapper">
                    <div class="card card-default">
                        <div class="card-body">
                            <div class="col-auto">
                                <div class="form-row">
                                    <div class="col-lg-3 mb-3"><label><b>Start Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-10"
                                                 moment-picker="start_date"
                                                 format="DD-MMM-YYYY"  start-view="month"    locale="en" today="true" max-date="current_date">

                                                <input class="form-control" name="start_date"
                                                       placeholder="Select Start Date"
                                                       ng-model="start_date"
                                                       ng-model-options="{updateOn: 'blur'}" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label><b>End Date</b></label>
                                        <div class="row">
                                            <div class="input-group col-md-10"
                                                 moment-picker="end_date"
                                                 format="DD-MMM-YYYY"   start-view="month" min-date="start_date"  locale="en" today="true" max-date="current_date">

                                                <input class="form-control " name="end_date"
                                                       placeholder="Select End Date"
                                                       ng-model="end_date"
                                                       ng-model-options="{updateOn: 'blur'}" autocomplete="off">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label><b>User Name</b></label>
                                        <select class="form-control" tabindex="4" name="user_name" data-placeholder="Search User"  ng-model="user_name">
                                            <option value="">Select User Name</option>
                                            <option value="{{dataObj.id}}" ng-repeat="dataObj in userListData">{{dataObj.emp_name}}</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 mb-3"><label></label>
                                        <button style="margin-top: 25px;" type="button" class="btn btn-success" ng-click="resetReportData(1)">Go</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="card card-default mt-4">
                    <div class="form-row">
                        <div class="col-lg-10 text-center"></div>
                        <?php if ($show) { ?>
                            <div class="col-lg-2" ng-if="auditTrailData.length > 0"><button style="margin-top:5px;"  onclick="ExportPdf()"><i class="fas fa-file-pdf" style="font-size:24px"></i></button></div>
                        <?php } ?>
                    </div>
                    <!--                    <div class="form-row">
                                            <div class="col-lg-10 text-center"></div>
                                            <div class="col-lg-2" ng-if="auditTrailData.length > 0"><button style="margin-top:5px;"  onclick="ExportPdf()"><i class="fas fa-file-pdf" style="font-size:24px"></i></button></div>
                                        </div>-->
                    <div class="card-body" id="audit_trail">
                        <div class="form-row pb-2" id="pdf_download" style="display:none">
                            <div class="col-md-4"></div>
                            <div class="col-md-3">User Name : <?php echo $this->session->userdata('empname') ?></div>
                            <div class="col-md-3">Date : <?php echo date("d-m-Y");?></div>
                            <div class="col-md-2">Time : <?php echo date("h:i:sA");?></div>
                        </div>
                        <div class="form-row">
                            <div class="col-lg-12 text-center"><h2>Audit Trail Report</h2></div>
                        </div>
                        <div class="form-row" style="margin-top:10px;">
                            <div class=" col-lg-12 table-responsive noscroll" >
                            <table class="table custom-table runningacttable">
                                <thead>
                                    <tr>
                                        <th>Created On</th>
                                        <th>Action Text</th>
                                        <th>Activity Name</th>
                                        <th>Type</th>
                                        <th>Unique Field1</th>
                                        <th>Unique Field2</th>
                                        <th>Unique Field3</th>
                                        <!--<th>Record Type</th>-->
                                        <th>Update Count</th>
                                        <!--<th>Primary Id</th>-->
                                        <th>Table Name</th>
                                        <!--<th>Table Extra Field</th>-->
                                    </tr>
                                </thead>
                                <tbody dir-paginate="dataObj in auditTrailData |itemsPerPage:itemsPerPage" total-items="total_count" ng-show="auditTrailData.length > 0">
                                    <tr>
                                        <td>{{dataObj.created_on}}</td>
                                        <td>{{dataObj.action_text}}</td>
                                        <td>{{dataObj.activity_name}}</td>
                                        <td>{{dataObj.type}}</td>
                                        <td>{{dataObj.table_unique_field1}}</td>
                                        <td>{{dataObj.table_unique_field2}}</td>
                                        <td>{{dataObj.table_field3}}</td>
                                        <!--<td>{{dataObj.record_type}}</td>-->
                                        <td>{{dataObj.update_count}}</td>
                                        <!--<td>{{dataObj.primary_id}}</td>-->
                                        <td>{{dataObj.table_name}}</td>
                                        <!--<td>{{dataObj.table_extra_field}}</td>-->
                                        
                                    </tr>

                                </tbody>                                
                            </table>
                            </div>
                            <div class="col-lg-12">
                                <dir-pagination-controls
                                    max-size="8"
                                    direction-links="true"
                                    boundary-links="true" 
                                    on-page-change="auditTrailReportData(newPageNumber)" >
                                </dir-pagination-controls>
                            </div>
                            <div class="col-lg-12 text-center" ng-show="auditTrailData.length == 0"><h2>No Data Found</h2></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>SMHS. &copy; 2020 - 2021. All Rights Reserved.</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div1
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="<?php echo base_url() ?>user/logout">Logout</a>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/sb-admin-2.min.js"></script> 

<script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.slim.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/popper.min.js"></script>
<!-- <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="<?php echo base_url() ?>js/chosen.jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/angular.min.js"></script>
<script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/3.7.0/lodash.min.js"></script>
<script src="<?php echo base_url() ?>js/angularjs-dropdown-multiselect.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2017.2.621/js/jszip.min.js"></script>
<script src="https://kendo.cdn.telerik.com/2017.2.621/js/kendo.all.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment-with-locales.js"></script>
<script src="<?php echo base_url() ?>js/angular-moment-picker.min.js"></script>
<script src="<?php echo base_url() ?>js/dirPagination.js"></script> <!-- Pagination link -->
<script type="text/javascript">
                                var url_base_path = '<?php echo base_url() ?>';
                                var app = angular.module("reportApp", ['angular.chosen', 'moment-picker', 'angularUtils.directives.dirPagination']);
                                app.controller("reportCtrl", function ($scope, $http, $filter) {
                                    $scope.auditTrailData = [];
                                    $scope.pageno = 1; // initialize page no to 1
                                    $scope.total_count = 0;
                                    $scope.itemsPerPage = 10; //this could be a dynamic value from a drop down
                                    $scope.user_name = "";
                                    $scope.current_date = moment().format('DD-MMM-YYYY');
                                    $scope.start_date = moment().add(-30, 'days').format('YYYY-MM-DD');
                                    $scope.end_date = moment().format('YYYY-MM-DD');
                                    $scope.auditTrailReportData = function (pageno) {
                                        var valid = true;
                                        if (($scope.start_date == "") || ($scope.end_date == "")) {
                                            valid = true;
                                        }

                                        if (valid) {
                                            $scope.auditTrailData = [];
                                            $http({
                                                url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/auditTrailReportListNew?page_limit=' + $scope.itemsPerPage + "&page_no=" + pageno,
                                                method: "POST",
                                                data: "user_id=" + $scope.user_name + "&start_date=" + $scope.start_date + "&end_date=" + $scope.end_date,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                $scope.auditTrailData = response.data.row_data;
                                                $scope.total_count = response.data.total_count;
                                            }, function (error) { // optional
                                                toaster.pop('error', "error", "Something went wrong.Please try again");
                                            });
                                        } else {
                                            alert("Please Select Room No/Start Date/End Date");
                                        }
                                    }
                                    $scope.auditTrailReportData($scope.pageno);
                                    $scope.resetReportData = function (pageno) {
                                        $scope.pageno = pageno; // initialize page no to 1
                                        $scope.auditTrailReportData($scope.pageno);
                                    }

                                    $scope.getUsersList = function () {
                                        $scope.userListData = [];
                                        $http({
                                            url: url_base_path + 'Rest/Pontasahibelog/Pontasahib/getUsersList',
                                            method: "GET",
                                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                        }).then(function (response) {
                                            $scope.userListData = response.data.user_data;
                                            //console.log($scope.userListData);
                                        }, function (error) { // optional
                                            toaster.pop('error', "error", "Something went wrong.Please try again");
                                        });
                                    }
                                    $scope.getUsersList();

                                });
                                function ExportPdf() {
                                    $("#pdf_download").show();
                                    kendo.drawing
                                            .drawDOM("#audit_trail",
                                                    {
                                                        paperSize: "A4",
                                                        landscape: true,
                                                        sortable: true,
                                                        pageable: true,
                                                        margin: {top: "1cm", bottom: "1cm"},
                                                        scale: 0.5,
                                                        height: 500
                                                    })
                                            .then(function (group) {
                                                kendo.drawing.pdf.saveAs(group, "audit_trail.pdf")
                                            }).always(function(){
                                                $("#pdf_download").hide();
                                            });
                                }
</script>
<script src="<?php echo base_url(); ?>assets/js/disable_editor.js"></script>
</body>

</html>
