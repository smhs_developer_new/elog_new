<style>
#report tr td {
    width: 0px;
}
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/Bhupeestyle.css">
<script src="jquery.min.1.11.1.js" type="text/javascript"></script>  
    <script src="jquery.table2excel.min.js" type="text/javascript"></script>  
<style>

thead th {
  font-family: Source Sans Pro;
  font-size: 10px;
}


tbody tr td {
  font-family: Source Sans Pro;
  font-weight: 100;
  color: #5f6062;
  font-size: 10px;
  padding: 10px 10px 10px 10px;
  border-bottom: 1px solid #e0e0e0;
}

tbody tr:nth-child(2n) {
  background: #f0f3f5;
}

tbody tr:last-child td {
  border-bottom: none;
}
tbody tr:last-child td:first-child {
  -moz-border-radius-bottomleft: 5px;
  -webkit-border-bottom-left-radius: 5px;
  border-bottom-left-radius: 5px;
}
tbody tr:last-child td:last-child {
  -moz-border-radius-bottomright: 5px;
  -webkit-border-bottom-right-radius: 5px;
  border-bottom-right-radius: 5px;
}

tbody:hover > tr td {
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=50);
  opacity: 0.5;
  /* uncomment for blur effect */
  /* color:transparent;
  @include text-shadow(0px 0px 2px rgba(0,0,0,0.8));*/
}

tbody:hover > tr:hover td {
  text-shadow: none;
  color: #2d2d2d;
  filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
  opacity: 1;
}
</style>
  <!-- Main section-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-8 pl-0" id="head">Seq Log for Area & Equipment</div>
        <div class="col-sm-4 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>Report/Reports">Reports</a></li>
          </ol>
        </div>
      </div>
      <div class="card card-default">
        <div class="card-body">
          <div class="row">     
            <div class="col-sm-4">
              <select class="form-control" onchange="getroom();" id="area">
              <option value='' selected disabled>Select Area</option>
              <?php if($data['area']=="") { } else { foreach($data['area']->result() as $row) { ?>             
              <option data-sop="<?php echo $row->area_sop_no ?>" value="<?php echo $row->area_code ?>"><?php echo $row->area_name ?></option> 
            <?php } } ?>
              </select>
            </div>
            <div class="col-sm-4">
              <select class="form-control" id="roomdd1" onchange="getTime();">
              <option value='' selected disabled>Select Room</option>
              </select>
            </div>
            <div class="col-sm-3">
              <select class="form-control" id="timedd1" onchange="getcustom();">
              <option value='' selected disabled>Select Time</option>
              </select>
              <div id="cus" style="display: none;"><input class="form-control mt-1" type="date" id="from" name="from"><input class="form-control mt-1" type="date" id="to" name="to"><input type="button" id="btncus2" onclick="getcustomdata();" class="btn btn-success float-right mt-1" value="Submit" /></div>
            </div>
            <div class="col-sm-1">
              <div id="go"><input type="button" id="btncus" onclick="getdata();" class="btn btn-success float-right" value="Submit" /></div>
            </div>
          </div>
        </div>
      </div>
      <div class="card card-default" id="divreport">
      <div class="card-header p-5"><span id="an" class="p-5"></span><span id="as" class="p-5 pl60"></span><span id="rn" class="p-5 pl60"></span><span class="float-right"><input class="btn btn-success" type="button" id="btnExport1" value="Export to pdf" onclick="printDiv()" />
<!--<input type="button" id="btnExport2" onclick="exportToExcel('report', 'Sqnlog-report')" class="btn btn-success" value="Export to excel" />--></span></div>
        <table class="table table-dark" id="report" border="1">
          <thead><tr tabindex=0><th scope='col' width='100'>Product Description</th><th scope='col'>Document No.</th><th scope='col'>Batch</th><th scope='col'>Activity</th><th scope='col' width='200'>Start Time</th><th scope='col' width='200'>End Time</th><th scope='col' width='400'>Fixed Equipment</th><th scope='col' width='400'>Portable Equipment</th><th scope='col'>Started By</th><th scope='col'>Ended By</th><th scope='col'>Approved On</th><th scope='col'>Approved By</th></tr></thead>
          <tbody>

          </tbody>
          </table>
      </div><!-- START row-->     
      </div>
  </section>
<script type="text/javascript">
function exportToExcel(tableID, filename = ''){
    var downloadurl;
    var dataFileType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTMLData = tableSelect.outerHTML.replace(/ /g, '%20');
    
    // Specify file name
    filename = filename?filename+'.xls':'export_excel_data.xls';
    
    // Create download link element
    downloadurl = document.createElement("a");
    
    document.body.appendChild(downloadurl);
    
    if(navigator.msSaveOrOpenBlob){
        var blob = new Blob(['\ufeff', tableHTMLData], {
            type: dataFileType
        });
        navigator.msSaveOrOpenBlob( blob, filename);
    }else{
        // Create a link to the file
        downloadurl.href = 'data:' + dataFileType + ', ' + tableHTMLData;
    
        // Setting the file name
        downloadurl.download = filename;
        
        //triggering the function
        downloadurl.click();
    }
}
 
</script>
<script type="text/javascript">
function getroom()
{
    var areacode = $('#area').val();
    $.ajax({
      url: "<?= base_url()?>Report/getRoom",
      type: 'POST',
      data: {areacode:areacode},
      success: function(res) {
          console.log(res);
        var inv = "<option value='' selected disabled>Select Room</option>";
        $.each(res, function (index, value) {
        inv = inv.concat("<option value='"+value.room_code+"'>"+value.room_code+"</option>");
        });
        $("#roomdd1").html(inv);
      }
    });
}
function getTime()
{
  var cols = "<option value='' selected disabled>Select Time</option>";
  cols +="<option value='Today'>Today</option>";
  cols +="<option value='Lastweek'>Last 7 days</option>";
  cols +="<option value='Lastmonth'>Last 30 days</option>";
  cols +="<option value='Custom'>Custom</option>";
  $("#timedd1").html(cols);
} 

function getcustom()
{
    var time = $('#timedd1').val();
    if(time == "Custom")
    {
      $("#cus").show();
      //$("#report").html("");
      $("#btncus").hide();
    }
    else
    {
      $("#cus").hide();
      $("#btncus").show();
    }
} 

function getdata()
{
  debugger;

    var time = $('#timedd1').val();
    if(time != "Custom")
    {
    $("#from").val('');
    $("#to").val('');
    $("#cus").hide();
    $("#report").html("");
    var area_code = $('#area').val();
    var areasop = $("#area option:selected").attr('data-sop');
   var area_name = $("#area option:selected").text();
    $("#an").text("Area : "+area_name);
    $("#as").text("Area SOP : "+areasop);
    var room_code = $('#roomdd1').val();
    $("#rn").text("Room : "+room_code);
    $.ajax({
      url: "<?= base_url()?>Report/getdata",
      type: 'POST',
      data: {time:time,area_code:area_code,room_code:room_code},
      success: function(res) {

        console.log(res);
        if(res=="" || res == null)
        {
             var tbl = "<thead><tr tabindex=0><th colspan='10'><center>No result found</center></th></tr></thead><tbody>";
             $("#report").html(tbl);
        }
        else{
        var tbl = "<thead><tr tabindex=0><th scope='col' width='100'>Product Description</th><th scope='col'>Document No.</th><th scope='col'>Batch</th><th scope='col'>Activity</th><th scope='col' width='200'>Start Time</th><th scope='col' width='200'>End Time</th><th scope='col' width='400'>Fixed Equipment</th><th scope='col' width='400'>Portable Equipment</th><th scope='col'>Started By</th><th scope='col'>Ended By</th><th scope='col'>Approved On</th><th scope='col'>Approved By</th></tr></thead><tbody>";
        $.each(res, function (index, value) {
          var eqarray = value.equipments.split(',');
          var starray = value.start_time.split(' '); //starttime into an array
          var etarray = value.end_time.split(' '); //endtime into an array
          var approvedby = value.approvedby.split(',');
          var approvedon = value.approvedon.split(',');

        tbl = tbl.concat("<tr><td>"+value.product_description+"</td><td>"+value.document_no+"</td><td>"+value.batch_no+"</td><td>"+value.operation_type+"</td><td>"+starray[1]+ "<br/>" +starray[0]+"</td><td>"+etarray[1]+ "<br/>" +etarray[0]+"</td>");
            tbl = tbl.concat("<td width='400'>");
             for (var i = 0; i < eqarray.length; i++) {
                var array2 = eqarray[i].split('###');                                
                  if(array2[2]=="Fixed")
                  {
                      tbl = tbl.concat(array2[0]+"</br>");
                      tbl = tbl.concat(array2[1]+"</br>");
                      tbl = tbl.concat("SOP  : "+array2[3]+"</br>");
                  }                
                tbl = tbl.concat("</br>"); 
              }
              tbl = tbl.concat("</td>");
              tbl = tbl.concat("<td>");
              for (var i = 0; i < eqarray.length; i++) {
                var array2 = eqarray[i].split('###');                
                  if(array2[2]=="Portable")
                  {
                      tbl = tbl.concat(array2[0]+"</br>");
                      tbl = tbl.concat(array2[1]+"</br>");
                      tbl = tbl.concat("SOP  : "+array2[3]+"</br>");
                  }                 
                tbl = tbl.concat("</br>");
              }
              tbl = tbl.concat("</td>");       
          tbl = tbl.concat("<td>"+value.started_by+"</td><td>"+value.ended_by+"</td>");
          tbl = tbl.concat("<td>");
          for (var p = 0; p < approvedon.length; p++) {
              var approvedon2 = approvedon[p].split(" ");
              tbl = tbl.concat(approvedon2[1]+"</br>");
              tbl = tbl.concat(approvedon2[0]+"</br>");

          }
          tbl = tbl.concat("</td>");
          tbl = tbl.concat("<td>");
          for (var q = 0; q < approvedby.length; q++) {
              tbl = tbl.concat(approvedby[q]+"</br>");
          }
          tbl = tbl.concat("</td>");
          tbl = tbl.concat("</tr>");
        });
        tbl = tbl.concat("</tbody>")
        $("#report").html(tbl);
      }
    }
    });
  }
  else
  {
    $("#cus").show();
    $("#report").html("");
  }
} 
function getcustomdata()
{
  debugger;

    var time = $('#timedd1').val();
    if(time == "Custom")
    {
    var fromtime = $("#from").val();
    var totime = $("#to").val();
    $("#report").html("");
    var area_code = $('#area').val();
    var areasop = $("#area option:selected").attr('data-sop');
   var area_name = $("#area option:selected").text();
    $("#an").text("Area : "+area_name);
    $("#as").text("Area Sop : "+areasop);
    var room_code = $('#roomdd1').val();
    $("#rn").text("Room : "+room_code);
    $.ajax({
      url: "<?= base_url()?>Report/getcustomdata",
      type: 'POST',
      data: {fromtime:fromtime,totime:totime,area_code:area_code,room_code:room_code},
      success: function(res) {

        console.log(res);
        if(res=="" || res == null)
        {
             var tbl = "<thead><tr tabindex=0><th colspan='10'><center>No result found</center></th></tr></thead><tbody>";
             $("#report").html(tbl);
        }
        else{
        var tbl = "<thead><tr tabindex=0><th scope='col' width='100'>Product Description</th><th scope='col'>Document No.</th><th scope='col'>Batch</th><th scope='col'>Activity</th><th scope='col' width='200'>Start Time</th><th scope='col' width='200'>End Time</th><th scope='col' width='400'>Fixed Equipment</th><th scope='col' width='400'>Portable Equipment</th><th scope='col'>Started By</th><th scope='col'>Ended By</th><th scope='col'>Approved On</th><th scope='col'>Approved By</th></tr></thead><tbody>";
        $.each(res, function (index, value) {
          var eqarray = value.equipments.split(',');
          var starray = value.start_time.split(' '); //starttime into an array
          var etarray = value.end_time.split(' '); //endtime into an array
          var approvedby = value.approvedby.split(',');
          var approvedon = value.approvedon.split(',');

        tbl = tbl.concat("<tr><td>"+value.product_description+"</td><td>"+value.document_no+"</td><td>"+value.batch_no+"</td><td>"+value.operation_type+"</td><td>"+starray[1]+ "<br/>" +starray[0]+"</td><td>"+etarray[1]+ "<br/>" +etarray[0]+"</td>");
            tbl = tbl.concat("<td width='400'>");
             for (var i = 0; i < eqarray.length; i++) {
                var array2 = eqarray[i].split('###');                                
                  if(array2[2]=="Fixed")
                  {
                      tbl = tbl.concat(array2[0]+"</br>");
                      tbl = tbl.concat(array2[1]+"</br>");
                      tbl = tbl.concat("SOP  : "+array2[3]+"</br>");
                  }                
                tbl = tbl.concat("</br>"); 
              }
              tbl = tbl.concat("</td>");
              tbl = tbl.concat("<td>");
              for (var i = 0; i < eqarray.length; i++) {
                var array2 = eqarray[i].split('###');                
                  if(array2[2]=="Portable")
                  {
                      tbl = tbl.concat("Code : "+array2[0]+"</br>");
                      tbl = tbl.concat("Name : "+array2[1]+"</br>");
                      tbl = tbl.concat("SOP  : "+array2[3]+"</br>");
                  }                 
                tbl = tbl.concat("</br>");
              }
              tbl = tbl.concat("</td>");       
          tbl = tbl.concat("<td>"+value.started_by+"</td><td>"+value.ended_by+"</td>");
          tbl = tbl.concat("<td>");
          for (var p = 0; p < approvedon.length; p++) {
              var approvedon2 = approvedon[p].split(" ");
              tbl = tbl.concat(approvedon2[1]+"</br>");
              tbl = tbl.concat(approvedon2[0]+"</br>");

          }
          tbl = tbl.concat("</td>");
          tbl = tbl.concat("<td>");
          for (var q = 0; q < approvedby.length; q++) {
              tbl = tbl.concat(approvedby[q]+"</br>");
          }
          tbl = tbl.concat("</td>");
          tbl = tbl.concat("</tr>");
        });
        tbl = tbl.concat("</tbody>")
        $("#report").html(tbl);
      }
    }
    });
  }
} 
</script>
<script type="text/javascript">
        function printDiv() 
{

  var divToPrint=document.getElementById('divreport');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><style>table {border-collapse: separate;background: #fff;-moz-border-radius: 5px;-webkit-border-radius: 5px;border-radius: 5px;-moz-box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);-webkit-box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);}</style><body style="display: grid;grid-template-columns: 1fr 1fr 1fr;grid-template-rows: 50px 50px;" onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
    </script>
  