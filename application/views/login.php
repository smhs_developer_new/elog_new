<?php
if (isset($_GET["err"]) == 1) {
    echo "<script type='text/javascript'>alert('You Are Not Allowed to Access This URL Directly');</script>";
}
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Bootstrap Admin App">
        <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
        <link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>favicon.ico">
        <title>SMHS - eLog System</title><!-- =============== VENDOR STYLES ===============-->
        <!-- FONT AWESOME-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/brands.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/regular.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/solid.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/fontawesome.css"><!-- SIMPLE LINE ICONS-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/simple-line-icons/css/simple-line-icons.css"><!-- =============== BOOTSTRAP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css" id="bscss"><!-- =============== APP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/app.css" id="maincss">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
        <style>
            .txtPassword{
                -webkit-text-security:disc;
                text-security:disc;
            }

        </style>
    </head>

    <body oncopy="return false" oncut="return false" onpaste="return false">
        <input type="hidden" id="base" value="<?php echo base_url(); ?>">
        <div class="wrapper">
            <div class="block-center mt90 wd-xl30">
                <!-- START card-->
                <div class="card card-flat">
                    <div class="card-header text-center " style="background-color: #A9A9A9">
                        <div class="brand-logo font-weight-bold text-white block-center rounded">eLOGBOOK V1.0</div>
                    </div>
                    <div class="card-body">
                        <p class="text-center py-2">SIGN IN TO CONTINUE.</p>
                        <p class="text-center py-2"><font color="red"><?php echo isset($data['error']) ? $data['error'] : $this->session->flashdata('error') ?></font></p>
                        <?php echo form_open('login/userlogin') ?>
                        <div class="form-group">
                            <div class="input-group with-focus">
                                <?php echo form_input(['class' => 'form-control border-right-0', 'name' => 'emailid', 'id' => 'emailid', 'type' => 'text', 'placeholder' => 'Enter User Id', 'autocomplete' => 'off', 'required' => 'required']) ?>
                                <div class="input-group-append"><span class="input-group-text text-muted bg-transparent border-left-0"></span></div>
                            </div>
                        </div>
                        <!--<div class="fake-input"></div>-->
                        <div class="form-group">
                            <div class="input-group with-focus">
                                <?php echo form_input(['class' => 'form-control border-right-0 txtPassword', 'name' => 'pwd', 'id' => 'pwd', 'type' => 'text', 'placeholder' => 'Password', 'autocomplete' => 'off', 'required' => 'required']) ?>
                                <?php echo form_input(['class' => 'form-control border-right-0', 'name' => 'password', 'id' => 'password', 'type' => 'hidden', 'placeholder' => 'Password', 'autocomplete' => 'off']) ?>
                                <div class="input-group-append"><span class="input-group-text text-muted bg-transparent border-left-0"><em class="fa fa-lock"></em></span></div>
                            </div>
                        </div>
                        <div class="clearfix">
                            <!--<div class="checkbox c-checkbox float-left mt-0"><label><input type="checkbox" value="" name="remember"><span class="fa fa-check"></span> Remember Me</label></div>
                          <div class="float-right"><a class="text-muted" href="#">Forgot your password?</a></div>-->
                        </div>
                        <button class="btn btn-block btn-primary mt-3" id="subbtn" name="subbtn" type="submit">Login</button>
                        </form>
                        <!--<p class="pt-3 text-center">Need to Signup?</p><a class="btn btn-block btn-secondary" href="register.html">Register Now</a>-->
                    </div>
                </div><!-- END card-->
                <!-- Page footer-->
                <footer class="footer-container footercon text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
                <!-- Page footer End-->

            </div>
        </div><!-- =============== VENDOR SCRIPTS ===============-->
        <!--Reset password model-->
        <?php if (!empty($data) && isset($data['is_first']) && $data['is_first'] == 1) { ?>
            <div class="modal reset_popup" ng-app="resetApp" ng-controller="resetCtrl" style="display:block" id="loginModal"  role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">

                            <h4 class="modal-title login-label" id="exampleModalLabel">Reset Password</h4>
                            <button type="button" class="close" data-dismiss="modal" id="btnclose" onclick="cancilReset()"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <form name="loginForm" novalidate>
                            <div class="modal-body">

                                <div class="row mb-2">
                                    <div class="col-sm-4"><label for="user Id" class="control-label login-label">Password:</label></div>
                                    <div class="col-sm-8"><input type="password" ng-model="password" autocomplete="off" class="form-control" name="username" required></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4"><label for="Password" class="control-label login-label"> Confirm Password:</label></div>
                                    <div class="col-sm-8"><input type="password" name="password" autocomplete="off" ng-model="confpassword" class="form-control" required></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="reset" class="btn btn-danger" >Cancel</button>
                                <button type="button"  ng-disabled="loginForm.$invalid" class="btn btn-primary" ng-click="resetPassword();">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php } ?>
        <!--End Reset password model-->
        <!-- MODERNIZR-->
        <script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API-->
        <script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- i18next-->
        <script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script>
        <script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- JQUERY-->
        <script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script><!-- BOOTSTRAP-->
        <script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script>
        <script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- PARSLEY-->
        <script src="<?php echo base_url(); ?>vendor/parsleyjs/dist/parsley.js"></script><!-- =============== APP SCRIPTS ===============-->
        <script src="<?php echo base_url(); ?>js/app.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>
    </body>
    <script src="<?php echo base_url(); ?>js/angular.min.js"></script>
    <script src="<?php echo base_url() ?>js/angular-chosen.min.js"></script>
    <script data-require="angular-block-ui@*" data-semver="0.1.1" src="<?php echo base_url(); ?>js/angular-block-ui.min.js"></script>
    <?php if (!empty($data) && isset($data['is_first']) && $data['is_first'] == 1) { ?>
        <script type="text/javascript">
                                    var app = angular.module("resetApp", ['angular.chosen', 'blockUI']);
                                    app.controller("resetCtrl", function ($scope, $http, $filter, blockUI) {

                                        //*******************End Rahul's code***************************//
                                        $scope.resetPassword = function () {
                                            var newPassword = $scope.password;
                                            var upperCount = newPassword.replace(/[^A-Z]/g, "").length;
                                            var lowerCount = newPassword.replace(/[^a-z]/g, "").length;
                                            var numberCount = newPassword.replace(/[^0-9]/g, "").length;
                                            var minNumberofChars = 8;
                                            var maxNumberofChars = 16;
                                            var regularExpression = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,16}/;
                                            var empname = '<?php echo $data["emp_name"]; ?>';
                                            var cont = '<?php echo $data["emp_contact"]; ?>';
                                            var userid = '<?php echo $data["emp_email"]; ?>';
                                            var name = empname.toUpperCase().split(" ");
                                            var firstChar = newPassword.charAt(0);
                                            if (firstChar <= '9' && firstChar >= '0') {
                                                alert("Password should not Start with Numeric Value");
                                                return false;
                                            }

                                            for (var i = 0; i <= name.length; i++) {
                                                var tempnew = newPassword.toUpperCase();
                                                if (name[i] != '') {
                                                    if (tempnew.indexOf(name[i]) != -1) {
                                                        alert("Password should not contain First Name or Surname");
                                                        return false;
                                                    }
                                                }
                                            }
                                            if (newPassword.toUpperCase().indexOf(userid.toUpperCase()) != -1) {
                                                alert("Password should not contain Userid");
                                                return false;
                                            }

                                            if (newPassword.length < minNumberofChars) {
                                                alert("Password should be atleast 8 character");
                                                return false;
                                            }
                                            if (upperCount < 1) {
                                                alert("Password should contain at least one upper case letter: (A – Z)");
                                                return false;
                                            }
                                            if (lowerCount < 1) {
                                                alert("Password should contain at least one lower case letter: (a – z)");
                                                return false;
                                            }
                                            if (numberCount < 1) {
                                                alert("Password should contain at least one number: (0 – 9)");
                                                return false;
                                            }

                                            if (!regularExpression.test(newPassword)) {
                                                alert("Password should contain at least one Special Characters");
                                                return false;
                                            }
                                            if ($scope.password != $scope.confpassword) {
                                                alert("Confirm Password is not same as Password");
                                                return false;
                                            }

                                            var user_id = '<?php echo $data["id"]; ?>';
                                            var pwd = SHA256($scope.password);
                                            $http({
                                                url: '<?php echo base_url() ?>Login/resetPassword',
                                                method: "POST",
                                                data: "user_id=" + user_id + "&password=" + pwd,
                                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                            }).then(function (response) {
                                                alert(response.data.message);
                                                if (response.data.result) {
                                                    window.location.href = "<?php echo base_url() ?>login";
                                                }
                                            }, function (error) { // optional
                                            });
                                        }
                                        //Reset Login Form
                                        $scope.resetLoginForm = function () {
                                            $scope.username = "";
                                            $scope.password = "";
                                            $scope.remark = "";
                                        }
                                    });
        </script>
    <?php } ?>

    <script>
        $(function ()
        {
            $('#pwd').on('focus', function ()
            {
                $(this).prop("type", 'password');  // this stops pre-saved password offers
            });
            $('#loginForm').on('submit', function ()
            {
                
                if ($('#pwd').prop('type') == 'text') {
                    $('#pwd').prop('type', 'password');
                    $('#pwd').val('');
                    
                }
            });
        });
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $('#subbtn').click(function () {
                var value = jQuery("#pwd").val();
                jQuery("#password").val(btoa(btoa(value)));
                pwd2 = SHA256(value);
                jQuery("#pwd").val(pwd2);
                //alert(pwd2);
                return true;
            });
        });
        function cancilReset() {
            window.location.href = "<?php echo base_url() ?>login";
        }
    </script>
    <script>
        /**
         *  Secure Hash Algorithm (SHA256)
         *  http://www.webtoolkit.info/
         *  Original code by Angel Marin, Paul Johnston
         **/

        function SHA256(s) {
            var chrsz = 8;
            var hexcase = 0;
            function safe_add(x, y) {
                var lsw = (x & 0xFFFF) + (y & 0xFFFF);
                var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
                return (msw << 16) | (lsw & 0xFFFF);
            }

            function S(X, n) {
                return (X >>> n) | (X << (32 - n));
            }
            function R(X, n) {
                return (X >>> n);
            }
            function Ch(x, y, z) {
                return ((x & y) ^ ((~x) & z));
            }
            function Maj(x, y, z) {
                return ((x & y) ^ (x & z) ^ (y & z));
            }
            function Sigma0256(x) {
                return (S(x, 2) ^ S(x, 13) ^ S(x, 22));
            }
            function Sigma1256(x) {
                return (S(x, 6) ^ S(x, 11) ^ S(x, 25));
            }
            function Gamma0256(x) {
                return (S(x, 7) ^ S(x, 18) ^ R(x, 3));
            }
            function Gamma1256(x) {
                return (S(x, 17) ^ S(x, 19) ^ R(x, 10));
            }

            function core_sha256(m, l) {
                var K = new Array(0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786, 0xFC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA, 0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147, 0x6CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070, 0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2);
                var HASH = new Array(0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19);
                var W = new Array(64);
                var a, b, c, d, e, f, g, h, i, j;
                var T1, T2;
                m[l >> 5] |= 0x80 << (24 - l % 32);
                m[((l + 64 >> 9) << 4) + 15] = l;
                for (var i = 0; i < m.length; i += 16) {
                    a = HASH[0];
                    b = HASH[1];
                    c = HASH[2];
                    d = HASH[3];
                    e = HASH[4];
                    f = HASH[5];
                    g = HASH[6];
                    h = HASH[7];
                    for (var j = 0; j < 64; j++) {
                        if (j < 16)
                            W[j] = m[j + i];
                        else
                            W[j] = safe_add(safe_add(safe_add(Gamma1256(W[j - 2]), W[j - 7]), Gamma0256(W[j - 15])), W[j - 16]);
                        T1 = safe_add(safe_add(safe_add(safe_add(h, Sigma1256(e)), Ch(e, f, g)), K[j]), W[j]);
                        T2 = safe_add(Sigma0256(a), Maj(a, b, c));
                        h = g;
                        g = f;
                        f = e;
                        e = safe_add(d, T1);
                        d = c;
                        c = b;
                        b = a;
                        a = safe_add(T1, T2);
                    }

                    HASH[0] = safe_add(a, HASH[0]);
                    HASH[1] = safe_add(b, HASH[1]);
                    HASH[2] = safe_add(c, HASH[2]);
                    HASH[3] = safe_add(d, HASH[3]);
                    HASH[4] = safe_add(e, HASH[4]);
                    HASH[5] = safe_add(f, HASH[5]);
                    HASH[6] = safe_add(g, HASH[6]);
                    HASH[7] = safe_add(h, HASH[7]);
                }
                return HASH;
            }

            function str2binb(str) {
                var bin = Array();
                var mask = (1 << chrsz) - 1;
                for (var i = 0; i < str.length * chrsz; i += chrsz) {
                    bin[i >> 5] |= (str.charCodeAt(i / chrsz) & mask) << (24 - i % 32);
                }
                return bin;
            }

            function Utf8Encode(string) {
                string = string.replace(/\r\n/g, "\n");
                var utftext = "";
                for (var n = 0; n < string.length; n++) {

                    var c = string.charCodeAt(n);
                    if (c < 128) {
                        utftext += String.fromCharCode(c);
                    } else if ((c > 127) && (c < 2048)) {
                        utftext += String.fromCharCode((c >> 6) | 192);
                        utftext += String.fromCharCode((c & 63) | 128);
                    } else {
                        utftext += String.fromCharCode((c >> 12) | 224);
                        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }

                }

                return utftext;
            }

            function binb2hex(binarray) {
                var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
                var str = "";
                for (var i = 0; i < binarray.length * 4; i++) {
                    str += hex_tab.charAt((binarray[i >> 2] >> ((3 - i % 4) * 8 + 4)) & 0xF) +
                            hex_tab.charAt((binarray[i >> 2] >> ((3 - i % 4) * 8)) & 0xF);
                }
                return str;
            }

            s = Utf8Encode(s);
            return binb2hex(core_sha256(str2binb(s), s.length * chrsz));
        }
    </script>
    <script>
        function Encrypt(str) {
            if (!str)
                str = "";
            str = (str == "undefined" || str == "null") ? "" : str;
            try {
                var key = 146;
                var pos = 0;
                ostr = '';
                while (pos < str.length) {
                    ostr = ostr + String.fromCharCode(str.charCodeAt(pos) ^ key);
                    pos += 1;
                }

                return ostr;
            } catch (ex) {
                return '';
            }
        }

        function Decrypt(str) {
            if (!str)
                str = "";
            str = (str == "undefined" || str == "null") ? "" : str;
            try {
                var key = 146;
                var pos = 0;
                ostr = '';
                while (pos < str.length) {
                    ostr = ostr + String.fromCharCode(key ^ str.charCodeAt(pos));
                    pos += 1;
                }

                return ostr;
            } catch (ex) {
                return '';
            }
        }
    </script>
    <script type="text/javascript">
        <!--
        
        var keyStr = "ABCDEFGHIJKLMNOP" +
                "QRSTUVWXYZabcdef" +
                "ghijklmnopqrstuv" +
                "wxyz0123456789+/" +
                "=";
        function encode64(input) {
            input = escape(input);
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                        keyStr.charAt(enc1) +
                        keyStr.charAt(enc2) +
                        keyStr.charAt(enc3) +
                        keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);
            return output;
        }

        function decode64(input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;
            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                alert("There were invalid base64 characters in the input text.\n" +
                        "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                        "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            do {
                enc1 = keyStr.indexOf(input.charAt(i++));
                enc2 = keyStr.indexOf(input.charAt(i++));
                enc3 = keyStr.indexOf(input.charAt(i++));
                enc4 = keyStr.indexOf(input.charAt(i++));
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
                output = output + String.fromCharCode(chr1);
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);
            return unescape(output);
        }

        //--></script>
<script src="<?php echo base_url(); ?>assets/js/disable_editor.js"></script>
<script src="<?php echo base_url(); ?>js/network.js"></script>

</html>