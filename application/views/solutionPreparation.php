<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="Bootstrap Admin App">
<meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
<link rel="icon" type="image/x-icon" href="favicon.ico">
<title>SMHS - eLog System</title>
<!-- =============== VENDOR STYLES ===============-->
<!-- FONT AWESOME-->
<link rel="stylesheet" href="<?= base_url()?>vendor/@fortawesome/fontawesome-free/css/brands.css">
<link rel="stylesheet" href="<?= base_url()?>vendor/@fortawesome/fontawesome-free/css/regular.css">
<link rel="stylesheet" href="<?= base_url()?>vendor/@fortawesome/fontawesome-free/css/solid.css">
<link rel="stylesheet" href="<?= base_url()?>vendor/@fortawesome/fontawesome-free/css/fontawesome.css">
<!-- SIMPLE LINE ICONS-->
<link rel="stylesheet" href="<?= base_url()?>vendor/simple-line-icons/css/simple-line-icons.css">
<!-- ANIMATE.CSS-->
<link rel="stylesheet" href="<?= base_url()?>vendor/animate.css/animate.css">
<!-- WHIRL (spinners)-->
<link rel="stylesheet" href="<?= base_url()?>vendor/whirl/dist/whirl.css">
<!-- =============== PAGE VENDOR STYLES ===============-->
<!-- =============== BOOTSTRAP STYLES ===============-->
<link rel="stylesheet" href="<?= base_url()?>css/bootstrap.css" id="bscss">
<!-- =============== APP STYLES ===============-->
<link rel="stylesheet" href="<?= base_url()?>css/app.css" id="maincss">
<link rel="stylesheet" href="<?= base_url()?>css/style.css">
<style type="text/css">
  .ui-autocomplete {
    position: absolute;
    z-index: 1000;
    cursor: default;
    padding: 0;
    margin-top: 2px;
    list-style: none;
    background-color: #ffffff;
    border: 1px solid #ccc;
    -webkit-border-radius: 5px;
       -moz-border-radius: 5px;
            border-radius: 5px;
    -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
       -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
            box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
}
.ui-autocomplete > li {
  padding: 3px 20px;
}
.ui-autocomplete > li.ui-state-focus {
  background-color: #DDD;
}
.ui-helper-hidden-accessible {
  display: none;
}
</style>

<script type="text/javascript">
$(document).ready(function(){
$("#Fstep").click(function(){
	if(this.checked){
	alert("hi");
	//document.getElementById("incresswidth").style.width = "60%";
	}
	});
});
</script>
</head>

<body>
<div class="wrapper"> 
  <!-- top navbar-->
  <header class="topnavbar-wrapper"> 
    <!-- START Top Navbar-->
    <nav class="navbar topnavbar"> 
      <!-- START navbar header-->
      <div class="navbar-header"><a class="navbar-brand" href="logo">
        <div class="brand-logo font-weight-bold text-white">SMHS - eLog System</div>
        <div class="brand-logo-collapsed font-weight-bold text-white">SMHS</div>
        </a></div>
      <!-- END navbar header--> 
      <!-- START Left navbar-->
      <ul class="navbar-nav mr-auto flex-row">
        <li class="nav-item"> 
          <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link d-none d-md-block d-lg-block d-xl-block" href="#" data-trigger-resize="" data-toggle-state="aside-collapsed"><em class="fas fa-bars"></em></a><!-- Button to show/hide the sidebar on mobile. Visible on mobile only.--><a class="nav-link sidebar-toggle d-md-none" href="#" data-toggle-state="aside-toggled" data-no-persist="true"><em class="fas fa-bars"></em></a></li>
        <!-- START User avatar toggle-->
        <li class="nav-item d-none d-md-block"> 
          <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link" id="user-block-toggle" href="#user-block" data-toggle="collapse"><em class="icon-user"></em></a></li>
        <!-- END User avatar toggle--> 
        <!-- START lock screen-->
        <li class="nav-item d-none d-md-block"><a class="nav-link" href="#" title="Lock screen"><em class="icon-lock"></em></a></li>
        <!-- END lock screen-->
      </ul>
      <!-- END Left navbar--> 
      <!-- START Right Navbar-->
      <ul class="navbar-nav flex-row">
        <!-- Search icon-->
        <li class="nav-item"><a class="nav-link" href="#" data-search-open=""><em class="icon-magnifier"></em></a></li>
        <!-- Fullscreen (only desktops)-->
        <li class="nav-item d-none d-md-block"><a class="nav-link" href="#" data-toggle-fullscreen=""><em class="fas fa-expand"></em></a></li>
        <!-- START Alert menu-->
        <li class="nav-item dropdown dropdown-list"><a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="#" data-toggle="dropdown"><em class="icon-bell"></em><span class="badge badge-danger">11</span></a><!-- START Dropdown menu-->
          <div class="dropdown-menu dropdown-menu-right animated flipInX">
            <div class="dropdown-item"> 
              <!-- START list group-->
              <div class="list-group">
                <div class="list-group-item list-group-item-action">
                  <div class="media">
                    <div class="align-self-start mr-2"><em class="fas fa-envelope fa-2x text-warning"></em></div>
                    <div class="media-body">
                      <p class="m-0">New e-mails</p>
                      <p class="m-0 text-muted text-sm">You have 10 new emails</p>
                    </div>
                  </div>
                </div>
                <!-- list item-->
                <div class="list-group-item list-group-item-action">
                  <div class="media">
                    <div class="align-self-start mr-2"><em class="fas fa-tasks fa-2x text-success"></em></div>
                    <div class="media-body">
                      <p class="m-0">Pending Tasks</p>
                      <p class="m-0 text-muted text-sm">11 pending task</p>
                    </div>
                  </div>
                </div>
                <!-- last list item-->
                <div class="list-group-item list-group-item-action"><span class="d-flex align-items-center"><span class="text-sm">More notifications</span><span class="badge badge-danger ml-auto">14</span></span></div>
              </div>
              <!-- END list group--> 
            </div>
          </div>
          <!-- END Dropdown menu--> 
        </li>
        <!-- END Alert menu--> 
        <!-- START Offsidebar button-->
        <li class="nav-item"><a href="#" class="nav-link"></a></li>
        <li class="dropdown langs text-normal ng-scope" uib-dropdown="" is-open="status.isopenLang" data-ng-controller="LangCtrl" style=""> <a href="javascript:;" class="dropdown-toggle  dropdown-toggleactive-flag" uib-dropdown-toggle="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <div class="flag flags-american" style=""></div>
          </a>
          <ul class="dropdown-menu with-arrow  pull-right list-langs dropdown-menu-scaleIn animated flipInX" role="menu">
            <li data-ng-show="lang !== 'English' " aria-hidden="true" class="ng-hide" style=""> <a href="javascript:;" data-ng-click="setLang('English')">
              <div class="flag flags-american"></div>
              English</a></li>
            <li data-ng-show="lang !== 'Español' " aria-hidden="false" class="" style=""> <a href="javascript:;" data-ng-click="setLang('Español')">
              <div class="flag flags-spain"></div>
              Español</a></li>
            <li data-ng-show="lang !== 'Portugal' " aria-hidden="false" class="" style=""> <a href="javascript:;" data-ng-click="setLang('Portugal')">
              <div class="flag flags-portugal"></div>
              Portugal</a></li>
            <li data-ng-show="lang !== '中文' " aria-hidden="false" class=""> <a href="javascript:;" data-ng-click="setLang('中文')">
              <div class="flag flags-china"></div>
              中文</a></li>
            <li data-ng-show="lang !== '日本語' " aria-hidden="false" class="" style=""> <a href="javascript:;" data-ng-click="setLang('日本語')">
              <div class="flag flags-japan"></div>
              日本語</a></li>
            <li data-ng-show="lang !== 'Русский язык' " aria-hidden="false" class="" style=""> <a href="javascript:;" data-ng-click="setLang('Русский язык')">
              <div class="flag flags-russia"></div>
              Русский язык</a></li>
          </ul>
        </li>
        
        <!-- END Offsidebar menu-->
      </ul>
      <!-- END Right Navbar--> 
      <!-- START Search form-->
      <form class="navbar-form" role="search" action="search.html">
        <div class="form-group">
          <input class="form-control" type="text" placeholder="Type and hit enter ...">
          <div class="fas fa-times navbar-form-close" data-search-dismiss=""></div>
        </div>
        <button class="d-none" type="submit">Submit</button>
      </form>
      <!-- END Search form--> 
    </nav>
    <!-- END Top Navbar--> 
  </header>
  <!-- sidebar-->
  <aside class="aside-container"> 
    <!-- START Sidebar (left)-->
    <div class="aside-inner">
      <nav class="sidebar" data-sidebar-anyclick-close=""> 
        <!-- START sidebar nav-->
        <ul class="sidebar-nav">
          <!-- START user info-->
          <li class="has-user-block">
            <div class="collapse" id="user-block">
              <div class="item user-block"> 
                <!-- User picture-->
                <div class="user-block-picture">
                  <div class="user-block-status"><img class="img-thumbnail rounded-circle" src="<?= base_url()?>img/user/02.jpg" alt="Avatar" width="60" height="60">
                    <div class="circle bg-success circle-lg"></div>
                  </div>
                </div>
                <!-- Name and Job-->
                <div class="user-block-info"><span class="user-block-name">Hello, Mike</span><span class="user-block-role">Designer</span></div>
              </div>
            </div>
          </li>
          <!-- END user info--> 
          <!-- Iterates over all sidebar items-->
          <li class="nav-heading text-bold"><span data-localize="sidebar.heading.HEADER">Main Menu</span></li>
          <li ><a href="dashboard.html" title="Plant master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Plant master">My Tasks</span></a></li>
          <li class=" active"><a href="area.html" title="Plant master"><em class="fas fa-cogs"></em><span data-localize="sidebar.nav.Plant master">Execute SOP</span></a></li>
          <li><a href="#submenu" title="Menu" data-toggle="collapse"><em class="fas fa-chalkboard-teacher"></em><span data-localize="sidebar.nav.menu.MENU">Master Data</span></a>
            <ul class="sidebar-nav sidebar-subnav collapse" id="submenu">
              <li class="sidebar-subnav-header">Menu</li>
              <li><a href="#" title="Plant master"><span data-localize="sidebar.nav.Plant master">Plant master</span></a></li>
              <li><a href="#" title="Block master"><span data-localize="sidebar.nav.Plant master">Block master</span></a></li>
              <li><a href="#" title="Area master"><span data-localize="sidebar.nav.Plant master">Area master</span></a></li>
              <li><a href="#" title="Rooms master"><span data-localize="sidebar.nav.Plant master">Rooms master</span></a></li>
              <li><a href="#" title="SOP master"><span data-localize="sidebar.nav.Plant master">SOP master</span></a></li>
              <li><a href="#" title="Product master"><span data-localize="sidebar.nav.Plant master">product master</span></a></li>
              <li><a href="#" title="Critical Parts master"><span data-localize="sidebar.nav.Critical Parts master">Critical Parts master</span></a></li>
              <li><a href="#" title="Accessory Parts"><span data-localize="sidebar.nav.Accessory Parts">Accessory Parts</span></a></li>
              <li><a href="#" title="Equipments master"><span data-localize="sidebar.nav.Equipments master">Equipments master</span></a></li>
              <li><a href="#" title="Employee master"><span data-localize="sidebar.nav.Employee master">Employee master</span></a></li>
              <li><a href="#" title="Holiday master"><span data-localize="sidebar.nav.Holiday master">Holiday master</span></a></li>
            </ul>
          </li>
        </ul>
        <!-- END sidebar nav--> 
      </nav>
    </div>
    <!-- END Sidebar (left)--> 
  </aside>
  <!-- offsidebar-->
  <aside class="offsidebar d-none"> 
    <!-- START Off Sidebar (right)-->
    <nav>
      <div role="tabpanel"> 
        <!-- Nav tabs-->
        <ul class="nav nav-tabs nav-justified" role="tablist">
          <li class="nav-item" role="presentation"><a class="nav-link active" href="#app-settings" aria-controls="app-settings" role="tab" data-toggle="tab"><em class="icon-equalizer fa-lg"></em></a></li>
          <li class="nav-item" role="presentation"><a class="nav-link" href="#app-chat" aria-controls="app-chat" role="tab" data-toggle="tab"><em class="icon-user fa-lg"></em></a></li>
        </ul>
        <!-- Tab panes-->
        <div class="tab-content">
          <div class="tab-pane fade active show" id="app-settings" role="tabpanel">
            <h3 class="text-center text-thin mt-4">Settings</h3>
            <div class="p-2">
              <h4 class="text-muted text-thin">Themes</h4>
              <div class="row row-flush mb-2">
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-a.css">
                      <input type="radio" name="setting-theme" checked="checked">
                      <span class="icon-check"></span><span class="split"><span class="color bg-info"></span><span class="color bg-info-light"></span></span><span class="color bg-white"></span></label>
                  </div>
                </div>
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-b.css">
                      <input type="radio" name="setting-theme">
                      <span class="icon-check"></span><span class="split"><span class="color bg-green"></span><span class="color bg-green-light"></span></span><span class="color bg-white"></span></label>
                  </div>
                </div>
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-c.css">
                      <input type="radio" name="setting-theme">
                      <span class="icon-check"></span><span class="split"><span class="color bg-purple"></span><span class="color bg-purple-light"></span></span><span class="color bg-white"></span></label>
                  </div>
                </div>
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-d.css">
                      <input type="radio" name="setting-theme">
                      <span class="icon-check"></span><span class="split"><span class="color bg-danger"></span><span class="color bg-danger-light"></span></span><span class="color bg-white"></span></label>
                  </div>
                </div>
              </div>
              <div class="row row-flush mb-2">
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-e.css">
                      <input type="radio" name="setting-theme">
                      <span class="icon-check"></span><span class="split"><span class="color bg-info-dark"></span><span class="color bg-info"></span></span><span class="color bg-gray-dark"></span></label>
                  </div>
                </div>
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-f.css">
                      <input type="radio" name="setting-theme">
                      <span class="icon-check"></span><span class="split"><span class="color bg-green-dark"></span><span class="color bg-green"></span></span><span class="color bg-gray-dark"></span></label>
                  </div>
                </div>
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-g.css">
                      <input type="radio" name="setting-theme">
                      <span class="icon-check"></span><span class="split"><span class="color bg-purple-dark"></span><span class="color bg-purple"></span></span><span class="color bg-gray-dark"></span></label>
                  </div>
                </div>
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-h.css">
                      <input type="radio" name="setting-theme">
                      <span class="icon-check"></span><span class="split"><span class="color bg-danger-dark"></span><span class="color bg-danger"></span></span><span class="color bg-gray-dark"></span></label>
                  </div>
                </div>
              </div>
            </div>
            <div class="p-2">
              <h4 class="text-muted text-thin">Layout</h4>
              <div class="clearfix">
                <p class="float-left">Fixed</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-fixed" type="checkbox" data-toggle-state="layout-fixed">
                    <span></span></label>
                </div>
              </div>
              <div class="clearfix">
                <p class="float-left">Boxed</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-boxed" type="checkbox" data-toggle-state="layout-boxed">
                    <span></span></label>
                </div>
              </div>
              <div class="clearfix">
                <p class="float-left">RTL</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-rtl" type="checkbox">
                    <span></span></label>
                </div>
              </div>
            </div>
            <div class="p-2">
              <h4 class="text-muted text-thin">Aside</h4>
              <div class="clearfix">
                <p class="float-left">Collapsed</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-collapsed" type="checkbox" data-toggle-state="aside-collapsed">
                    <span></span></label>
                </div>
              </div>
              <div class="clearfix">
                <p class="float-left">Collapsed Text</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-collapsed-text" type="checkbox" data-toggle-state="aside-collapsed-text">
                    <span></span></label>
                </div>
              </div>
              <div class="clearfix">
                <p class="float-left">Float</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-float" type="checkbox" data-toggle-state="aside-float">
                    <span></span></label>
                </div>
              </div>
              <div class="clearfix">
                <p class="float-left">Hover</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-hover" type="checkbox" data-toggle-state="aside-hover">
                    <span></span></label>
                </div>
              </div>
              <div class="clearfix">
                <p class="float-left">Show Scrollbar</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-scroll" type="checkbox" data-toggle-state="show-scrollbar" data-target=".sidebar">
                    <span></span></label>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="app-chat" role="tabpanel">
            <h3 class="text-center text-thin mt-4">Connections</h3>
            <div class="list-group"> 
              <!-- START list title-->
              <div class="list-group-item border-0"><small class="text-muted">ONLINE</small></div>
              <!-- END list title-->
              <div class="list-group-item list-group-item-action border-0">
                <div class="media"><img class="align-self-center mr-3 rounded-circle thumb48" src="<?= base_url()?>img/user/05.jpg" alt="Image">
                  <div class="media-body text-truncate"><a href="#"><strong>Juan Sims</strong></a><br>
                    <small class="text-muted">Designeer</small></div>
                  <div class="ml-auto"><span class="circle bg-success circle-lg"></span></div>
                </div>
              </div>
              <div class="list-group-item list-group-item-action border-0">
                <div class="media"><img class="align-self-center mr-3 rounded-circle thumb48" src="<?= base_url()?>img/user/06.jpg" alt="Image">
                  <div class="media-body text-truncate"><a href="#"><strong>Maureen Jenkins</strong></a><br>
                    <small class="text-muted">Designeer</small></div>
                  <div class="ml-auto"><span class="circle bg-success circle-lg"></span></div>
                </div>
              </div>
              <div class="list-group-item list-group-item-action border-0">
                <div class="media"><img class="align-self-center mr-3 rounded-circle thumb48" src="<?= base_url()?>img/user/07.jpg" alt="Image">
                  <div class="media-body text-truncate"><a href="#"><strong>Billie Dunn</strong></a><br>
                    <small class="text-muted">Designeer</small></div>
                  <div class="ml-auto"><span class="circle bg-danger circle-lg"></span></div>
                </div>
              </div>
              <div class="list-group-item list-group-item-action border-0">
                <div class="media"><img class="align-self-center mr-3 rounded-circle thumb48" src="<?= base_url()?>img/user/08.jpg" alt="Image">
                  <div class="media-body text-truncate"><a href="#"><strong>Tomothy Roberts</strong></a><br>
                    <small class="text-muted">Designeer</small></div>
                  <div class="ml-auto"><span class="circle bg-warning circle-lg"></span></div>
                </div>
              </div>
              <!-- START list title-->
              <div class="list-group-item border-0"><small class="text-muted">OFFLINE</small></div>
              <!-- END list title-->
              <div class="list-group-item list-group-item-action border-0">
                <div class="media"><img class="align-self-center mr-3 rounded-circle thumb48" src="<?= base_url()?>img/user/09.jpg" alt="Image">
                  <div class="media-body text-truncate"><a href="#"><strong>Lawrence Robinson</strong></a><br>
                    <small class="text-muted">Designeer</small></div>
                  <div class="ml-auto"><span class="circle bg-warning circle-lg"></span></div>
                </div>
              </div>
              <div class="list-group-item list-group-item-action border-0">
                <div class="media"><img class="align-self-center mr-3 rounded-circle thumb48" src="<?= base_url()?>img/user/10.jpg" alt="Image">
                  <div class="media-body text-truncate"><a href="#"><strong>Tyrone Owens</strong></a><br>
                    <small class="text-muted">Designeer</small></div>
                  <div class="ml-auto"><span class="circle bg-warning circle-lg"></span></div>
                </div>
              </div>
            </div>
            <div class="px-3 py-4 text-center"> 
              <!-- Optional link to list more users--><a class="btn btn-purple btn-sm" href="#" title="See more contacts"><strong>Load more..</strong></a></div>
            <!-- Extra items-->
            <div class="px-3 py-2">
              <p><small class="text-muted">Tasks completion</small></p>
              <div class="progress progress-xs m-0">
                <div class="progress-bar bg-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"><span class="sr-only">80% Complete</span></div>
              </div>
            </div>
            <div class="px-3 py-2">
              <p><small class="text-muted">Upload quota</small></p>
              <div class="progress progress-xs m-0">
                <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"><span class="sr-only">40% Complete</span></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
    <!-- END Off Sidebar (right)--> 
  </aside>
  <!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        
        <div class="col-sm-3 pl-0">Solution Preparation</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-9 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="#">Solid Drug </a></li>
            <li class="breadcrumb-item"><a href="area.html">Granulation</a></li>
            <li class="breadcrumb-item"><a href="room.html">TAB-19, TAB-21</a></li>
			<li class="breadcrumb-item"><a href="SelectActivity.html">Solution Preparation</a></li>
      <li class=""> <button class="btn btn-danger btn-sm"> View Existing Solutions</button></li>
            
          </ol>
        </div>


      
</div>

	  
<form id="submit">

	<div class="card card-default">
        <div class="card-body">
			<div class="row">
            <div class="col-sm-6">
              <div class="listexecutesop"><img src="<?= base_url()?>img/icons/document-icon.png"><?= $docid?></div>
              <input type="text" name="doc_id" value="<?= $docid?>" hidden required>
            </div>
			<div class="col-sm-6"><div class="row"><div class="col-sm-3 mt-2 pl00">Solution Name</div><div class="col-sm-9 ui-widget"><input class="form-control autocomplete" type="text" placeholder="Solution Name" name="solutionname" required ></div>
            </div>
            </div>
            </div>
        </div>
      </div>

	  <div class="card card-default">
        <div class="card-body">
          
                     <fieldset>
                              <div class="form-row">
                                 <div class="col-lg-6 mb-3"><label for="validationServer01">Previous Batch No.</label><input class="form-control" id="validationServer01" type="text" placeholder="Batch No" name="batchno" required>
                                    
                                 </div>
                                 <div class="col-lg-6 mb-3"><label for="validationServer02">Previous QCAR No.</label><input class="form-control" id="validationServer02" type="text" placeholder="QCAR No" name="qcarno" required>
                                 </div>                                 
                              </div>
                        
                     </fieldset>
        </div>             
                 
      </div>
	  

			<div class="row">
               <div class="col-md-6">
                  <!-- START card-->
                  <div class="card card-default">                     
                     <div class="card-body">                        
                           <div class="form-group"><label>Standard Solution Qty</label><input class="form-control" type="text" placeholder="ex: 2000 ml" readonly name="stdsolqty" id="stdsolqty" required></div>
                           <div class="form-group"><label>Purified Water</label><input class="form-control" type="text" placeholder="ex: 500 ml" readonly name="strpurewater" id="strpurewater" required></div>
                           <div class="form-group"><label>Makeup Volume</label><input class="form-control" type="text" placeholder="ex : 2500 cubic ml" readonly name="markupvol" id="markupvol" required></div>                        
                     </div>
                  </div><!-- END card-->
               </div>
               <div class="col-md-6">
                   <!-- START card-->
                  <div class="card card-default">
                     <div class="card-body">
                      <div class="form-group row">
                      <div class="col-sm-8 col-md-8">
                           <label>Actual Solution Qty</label><input class="form-control" type="number" step="0.01" placeholder="Quantity" name="actsolqty" required>
                      </div>
                      <div class="col-sm-4 col-md-4">
                        <label>UoM</label>
                        <select class="form-control" name="actsoluom" required>
                          <option value="" selected disabled>Select UoM</option>
                          <option value="ml">ml</option>
                          <option value="l">l</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-sm-8 col-md-8">
                           <label>Actual Purified Water</label><input class="form-control" type="number" step="0.01" placeholder="Quantity" name="actpurewater" required>
                      </div>
                      <div class="col-sm-4 col-md-4">
                        <label>UoM</label>
                        <select class="form-control" name="actpureuom">
                          <option value="" selected disabled>Select UoM</option>
                          <option value="ml">ml</option>
                          <option value="l">l</option>
                        </select>
                      </div>
                    </div>

                           <!--<div class="form-group"><label>Actual Purified Water</label><input class="form-control" type="" placeholder=""></div>-->
                    <div class="row">
                    <div class="col-sm-12 col-md-12">       
                           <div class="form-group"><label>Solution Valid Upto</label><input class="form-control" type="number" min="1" placeholder="Number of Hours" name="validupto"></div>
                    </div>       
                    </div>                               
                     </div>
                  </div><!-- END card-->
               </div>
            </div><!-- END row-->	
			
			 <div class="card card-default" id="btn1">
                     
                     <div class="card-body" >
                        
                           <div class="form-group row" >
                              <!--<div class="col-xl-4">&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;</div>-->
							  <div class="col-xl-12 text-center">
                  <!--<button id="submitbtn" onClick="showmsg();" class="btn btn-danger btn-lg" type="button" >&nbsp;&nbsp;&nbsp;&nbsp;Submit Form &nbsp;&nbsp;&nbsp;&nbsp;</button>-->
                  <button type="submit" class="btn btn-danger btn-lg" >Submit Form</button>
                  </div>
                           </div>
                        
                     </div>
                  </div><!-- END card-->
				  
				  </form>

<form id="submit2">
<div class="card card-default">
        <div class="card-body">
          <input type="text" name="doc_id" value="<?= $docid?>" hidden required>
                     <fieldset>
                              <div class="form-row">
                                 <div class="col-lg-6 mb-3"><label for="remark">Check Remark</label><input class="form-control" type="text" placeholder="Check Remark" name="checkremark" required id="remark">
                                    
                                 </div>
                                 <div class="col-lg-6 mb-3"><label for="destory">Solution Destroyed By</label><input class="form-control" type="text" placeholder="Solution Destroyed By" name="soldesby" required id="destory">
                                 </div>                                 
                              </div>
                        
                     </fieldset>
        </div>             
                 
      </div>

      <div class="card card-default">
                     
                     <div class="card-body" >
                        
                           <div class="form-group row" >
                          
                <div class="col-xl-12 text-center">
                  <button type="button" class="btn btn-danger btn-lg" onclick="viewmore()">View More</button>
                  <button type="submit" class="btn btn-danger btn-lg">Destroy</button>
                  <button type="button" class="btn btn-danger btn-lg" onclick="mainscreen()">Add New Solution</button>
                  </div>
                           </div>
                        
                     </div>
                  </div>

</form>

        </div>
      </div>
    </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?= base_url()?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?= base_url()?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?= base_url()?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?= base_url()?>vendor/i18next/i18next.js"></script> 
<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?= base_url()?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?= base_url()?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?= base_url()?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- Modal Large-->
<div class="modal fade" id="myModalLarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabelLarge" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelLarge">Add portable euipments in your euipment list</h4>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>		
      </div>
	  
      <div class="modal-body">
		<ul class="task-list list-unstyled">
        <li class="bg-white pt10 pb10 pl15"> 
        <span class="col-sm-3 pl-0"><b>Area :</b> </span><span class="col-sm-3 pl-0" id="area">Granulation</span>
        <select class="custom-select custom-select-sm col-sm-6" id="areaddl" onchange="araechange();">
        <option selected="">Select Another Area</option>
        <option value="Blending">Blending</option>
        <option value="Coating">Coating</option>
        <option value="Packaging">Packaging</option>
        </select>
                              <button class="btn btn-success col-sm-2 mr-2" type="button" value="Show all" onclick="showpequip();" name="showall" id="showall" style="float:right;">Show All</button>
							  <button class="btn btn-success col-sm-2 mr-2" style="display:none;float:right;" type="button" value="Show less" onclick="hidepequip();" name="showless" id="showless">Show less</button>
                              
        </li>
        </ul>
        <section class="page-tasks">
		
          <ul class="task-list list-unstyled" id="peqlist">
            <li id="pli1"> <span class="view" id="p1">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 1" id="check1" name="check1">
                  <label>Portable euipment 1</label>
                </div>
                <!-- <div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli2"> <span class="view" id="p2">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 2" id="check2" name="check2">
                  <label>Portable euipment 2</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli3"> <span class="view" id="p3">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 3" id="check3" name="check3">
                  <label>Portable euipment 3</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli4" style="display:none;"> <span class="view" id="p4">
              <div class="row">
                <div class="col-sm-12 pl0" onClick="Fstep">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 4" id="check4" name="check4">
                  <label>Portable euipment 4</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli5" style="display:none;"> <span class="view" id="p5">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 5" id="check5" name="check5">
                  <label>Portable euipment 5</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli6" style="display:none;"> <span class="view" id="p6">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 6" id="check6" name="check6">
                  <label>Portable euipment 6</label>
                </div>
                <!-- <div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
          </ul>
        </section>
      </div>
	  
    </div>
  </div>
</div>
<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="js/app.js"></script> 
<script>
function showmsg()
{
	alert("Form submited successfully to QC for approval.");
}
function araechange()
{
	var area = $("#areaddl").val();
	$("#area").text(area);
}

$(document).ready(function(){
	$("#show1 .btn-danger").click(function(){
		$(".showcountdown1").show();
    	});
		
		$("#show1 .btn-success").click(function(){
		$(".showcountdown1").hide();
		});
		
		$("#show2 .btn-danger").click(function(){
		$(".showcountdown2").show();
    	});
		
		$("#show2 .btn-success").click(function(){
		$(".showcountdown2").hide();
		});
		
		$("#show3 .btn-danger").click(function(){
		$(".showcountdown").show();
    	});
		
		$("#show3 .btn-success").click(function(){
		$(".showcountdown").hide();
		});
		
		$("#show4 .btn-danger").click(function(){
		$(".showcountdown").show();
    	});
		
		$("#show4 .btn-success").click(function(){
		$(".showcountdown").hide();
		});
		
		$("#show5 .btn-danger").click(function(){
		$(".showcountdown").show();
    	});
		
		$("#show5 .btn-success").click(function(){
		$(".showcountdown").hide();
		});
		
		$("#show6 .btn-danger").click(function(){
		$(".showcountdown").show();
    	});
		
		$("#show6 .btn-success").click(function(){
		$(".showcountdown").hide();
		});
	
	});

function chngclr(obj,divid)
{
	debugger;
	/*if($("#"+obj).prop('checked') == true)
	{
		$("#"+divid).css("background-color","#b0be3c");
	}
	else
	{
	
	}*/
	
	var check = document.getElementById(obj);
		if(check.checked == true){
	document.getElementById(divid).style.backgroundColor = "#b0be3c";
	document.getElementById(divid).style.color = "white";
	}else{
		document.getElementById(divid).style.backgroundColor = "white";
		document.getElementById(divid).style.color = "#000";
		}
}
function showpequip()
{
debugger;
	$("#pli4").show(100);
	$("#pli5").show(200);
	$("#pli6").show(300);
	$("#showall").hide();
	$("#showless").show();
}
function hidepequip()
{
debugger;
	$("#pli4").hide(100);
	$("#pli5").hide(200);
	$("#pli6").hide(300);
	$("#showall").show();
	$("#showless").hide();
}
function brkdown(obj)
{
debugger;
	for(i=1;i<=12;i++)
	{
		if(i==obj)
		{
			$("#btnbd"+i).attr("disabled", true);
			$("#end"+i).show();
		}
		else{
			$("#btnbd"+i).attr("disabled", true);
		}
	}
	$("#fbtn").attr("disabled", true);
	$("#sbtn").hide();
	$("#rsbtn").show();
	$("#btnrs").attr("disabled", true);
}
function brkdownover(obj)
{
debugger;
	for(i=1;i<=12;i++)
	{
		if(i==obj)
		{
			$("#btnbd"+i).attr("disabled", false);
			$("#end"+i).hide();
		}
		else{
			$("#btnbd"+i).attr("disabled", false);
		}
	}
	$("#fbtn").attr("disabled", false);
	$("#sbtn").hide();
	$("#btnrs").attr("disabled", false);
}
function addpequip()
{
	debugger;
	$("#porteqlist").html("");
	var newRow = $("<li>");
	var counter = 6;
	$("#peqlist").find('input[name^="check"]').each(function () {	
	if($(this).prop('checked') == true)
	{	
	counter++;
	var name = $(this).attr("data-name");
    var cols = "";
    cols += "<span class='view' id='eqdiv"+counter+"'><div class='row'><div class='col-sm-12 pl0'><div class='row'>";
    cols += "<div class='col-sm-9 listexecutesop'><img class='mb-4 mt-4 wt42' src='<?= base_url()?>img/icons/machine.png'><span class='f15'>"+name+"</span><span class='f15 pl-8'>SOP/DC/011-3 Operation & Cleaning  of vertical deduster</span></div>";
    cols += "<div class='col-sm-3 disable-button-color float-right text-right' id='show"+counter+"'><button class='btn btn-labeled btn-danger mt-4 mb-4' type='button' onClick='brkdown("+counter+")' name='btnbd"+counter+"' id='btnbd"+counter+"'><span class='btn-label'><i class='fa fa-times'></i></span>Breakdown</button>";
	cols += "&nbsp;<button class='btn btn-labeled btn-success mb-2 mt-2 pl10' type='button' onClick='brkdownover("+counter+")' name='end"+counter+"' id='end"+counter+"' style='display:none;'><span class='btn-label'><i class='fa fa-check'></i></span>End</button></div></div></div></div></span></li>";
    newRow.append(cols);        
	}
	});
	$("#porteqlist").append(newRow);
}
function stepOne(){
	var check = document.getElementById("check1");
	if(check.checked == true){
	document.getElementById("incresswidth").style.width = "20%";
	document.getElementById("greencolor").style.backgroundColor = "#b0be3c";
	document.getElementById("greencolor").style.color = "white";
	}else{
		document.getElementById("incresswidth").style.width = "0%";
		document.getElementById("greencolor").style.backgroundColor = "white";
		document.getElementById("greencolor").style.color = "#000";
		}
}
function stepTwo(){
	var check = document.getElementById("check2");
	if(check.checked == true){
	document.getElementById("incresswidth").style.width = "40%";
	document.getElementById("greencolor2").style.backgroundColor = "#b0be3c";
	document.getElementById("greencolor2").style.color = "white";
	}else{
		document.getElementById("incresswidth").style.width = "20%";
		document.getElementById("greencolor2").style.backgroundColor = "white";
		document.getElementById("greencolor2").style.color = "#000";
		}
}
function stepThree(){
	var check = document.getElementById("check3");
	if(check.checked == true){
	document.getElementById("incresswidth").style.width = "55%";
	document.getElementById("greencolor3").style.backgroundColor = "#b0be3c";
	document.getElementById("greencolor3").style.color = "white";
	}else{
		document.getElementById("incresswidth").style.width = "40%";
		document.getElementById("greencolor3").style.backgroundColor = "white";
		document.getElementById("greencolor3").style.color = "#000";
		}
}
function stepFour(){
	var check = document.getElementById("check4");
	if(check.checked == true){
	document.getElementById("incresswidth").style.width = "70%";
	document.getElementById("greencolor4").style.backgroundColor = "#b0be3c";
	document.getElementById("greencolor4").style.color = "white";
	}else{
		document.getElementById("incresswidth").style.width = "55%";
		document.getElementById("greencolor4").style.backgroundColor = "white";
		document.getElementById("greencolor4").style.color = "#000";
		}
}
function stepFive(){
	var check = document.getElementById("check5");
	if(check.checked == true){
	document.getElementById("incresswidth").style.width = "85%";
	document.getElementById("greencolor5").style.backgroundColor = "#b0be3c";
	document.getElementById("greencolor5").style.color = "white";
	}else{
		document.getElementById("incresswidth").style.width = "70%";
		document.getElementById("greencolor5").style.backgroundColor = "white";
		document.getElementById("greencolor5").style.color = "#000";
		}
}
function stepSix(){
	var check = document.getElementById("check6");
	if(check.checked == true){
	document.getElementById("incresswidth").style.width = "100%";
	document.getElementById("greencolor6").style.backgroundColor = "#b0be3c";
	document.getElementById("greencolor6").style.color = "white";
	}else{
		document.getElementById("incresswidth").style.width = "85%";
		document.getElementById("greencolor6").style.backgroundColor = "white";
		document.getElementById("greencolor6").style.color = "#000";
		}
}

var onload = setTimeout(onload, 1000)
var onload = setTimeout(onload1, 2000)
function onload(){
	document.getElementById("show-hexagoan").style.display = "block";
	}
	function onload1(){
	document.getElementById("show-hexagoan1").style.display = "block";
	}
	
	
// Set the date we're counting down to
var countDownDate = new Date("Jan 5, 2021 00:00:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
 // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 60)) / (1000 * 60 * 60 ));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("demo").innerHTML = hours + "h "
  + minutes + "m " + seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);

// Set the date we're counting down to
var countDownDate = new Date("Jan 5, 2021 00:00:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
 // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 60)) / (1000 * 60 * 60 ));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("demo3").innerHTML = hours + "h "
  + minutes + "m " + seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo3").innerHTML = "EXPIRED";
  }
}, 1000);

// Set the date we're counting down to
var countDownDate = new Date("Jan 5, 2021 00:00:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
 // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 60)) / (1000 * 60 * 60 ));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("demo4").innerHTML = hours + "h "
  + minutes + "m " + seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo4").innerHTML = "EXPIRED";
  }
}, 1000);




	

</script>
<script type="text/javascript" src="js/script.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>

<script type="text/javascript">

$(document).ready(function(){

$(".autocomplete").change(function() {

var str = $(this).val();

$.ajax({
    url: "<?= base_url()?>user/solnamegetval",
    type: 'POST',
    data: {str:str},
    success: function(res) {
        //console.log(res);
        $("#stdsolqty").val(res.sol_qty);
        $("#strpurewater").val(res.water_qty);
        $("#markupvol").val(res.tot_qty);
    }
});

});  


$(".autocomplete").keyup(function() {
var str = $(this).val();
if(str!="" && str.length>=1){
  
$.ajax({
    url: "<?= base_url()?>user/solname",
    type: 'POST',
    data: {str:str},
    success: function(res) {
        console.log(res);
        availableTags = res; 
        //alert(res);
        /*var availableTags = [
    "ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++",
    "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran",
    "Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl",
    "PHP", "Python", "Ruby", "Scala", "Scheme"
  ];*/
  
  $(".autocomplete").autocomplete({
    source: availableTags
  });
    }
});

  
}

});

});

  function mainscreen(){
    //$("#submit2")[0].reset();
    //$("#submit2").hide();
    //$("#submit").show();
    location.reload(true);
  }

  function viewmore(){
    $("#btn1").hide();
    $("#submit").show();
  }

  $(document).ready(function(){
    $("#submit2").hide();
  $('#submit').submit(function(){ 
    $.ajax({
      type: 'POST',
      url: '<?= base_url()?>user/solpre',
      data: $(this).serialize() // getting filed value in serialize form
    })
    .done(function(details){ // if getting done then call.
      // show the response
      if(details.status==1){ 
        console.log(details.data);
        alert("Form submitted successfully");
        //$("#submit")[0].reset();
        
        //$("#submit").hide();
        //$("#submit2").show();
        location.reload(true);
      }
      else{
        alert("No");    
      }
    })
    .fail(function(details) { // if fail then getting message
      // just in case posting your form failed
      alert( "Posting failed." );
    });

// to prevent refreshing the whole page page
return false;

});


  $('#submit2').submit(function(){ 
    $.ajax({
      type: 'POST',
      url: '<?= base_url()?>user/solpre2',
      data: $(this).serialize() // getting filed value in serialize form
    })
    .done(function(details){ // if getting done then call.
      // show the response
      if(details.status==1){ 
        alert("Remaining Solution Destroyed Successfully");
        location.reload(true);
        //$("#submit2")[0].reset();
        //$("#submit2").hide();
        //$("#submit").show();

      }
      else{
        alert("No");    
      }
    })
    .fail(function(details) { // if fail then getting message
      // just in case posting your form failed
      alert( "Posting failed." );
    });

// to prevent refreshing the whole page page
return false;

});
});  
</script>

</body>
</html>