         <!-- Main section-->
         <section class="section-container">
            <!-- Page content-->
            <div class="content-wrapper">
               <div class="content-heading executesop-heading">
                <?php if($this->session->userdata("roleid")==2) { $role = "AS";?>
                  <div class="col-sm-3 pl-0">AS Pending Approvals</div>
                <?php } elseif($this->session->userdata("roleid")==3) { $role = "QA";?>
                  <div class="col-sm-3 pl-0">QA Pending Approvals</div>
                <?php } elseif ($this->session->userdata("roleid")==4) { $role = "PM";?>
                  <div class="col-sm-3 pl-0">PM Pending Approvals</div>
                <?php } ?>




                  <form class="search-form col-sm-5 pl-0">
                     <!--<em class="icon-magnifier"></em>
                        <input class="form-control" type="email" placeholder="Enter room code or number on name">-->
                  </form>
                  <div class="col-sm-4 pr-0">
                     <ol class="breadcrumb ml-auto">
                        <li class="breadcrumb-item"><a href="#">Solid Drug </a></li>
                        <li class="breadcrumb-item"><a href="#">Granulation</a></li>
                        <li class="breadcrumb-item active"><a href="room.html">Room 4</a></li>
                        <!--<li class="breadcrumb-item">Execute SOP</li>-->
                     </ol>
                  </div>
               </div>
               <?php
               $op = 0;
               $opA =0;
               $opB =0;
                if($data['result']=="")
                {

                }
                else
                { ?>
                
                <?php foreach($data['result']->result() as $row) { if($row->operation_type=="operation") { ?> <?php if($op==0) { ?> <h4>Operations</h4> <?php } $op=1; ?>
               <div class="card card-default" id="<?php echo 'mainrecorddiv'.$row->id ?>">
                  <div class="card-body">
                     <div class="row mt-6">
                        <div class="col-sm-2">
                           <p>Document No.</p>
                        </div>
                        <div class="col-sm-2">
                           <p>Batch</p>
                        </div>
                        <div class="col-sm-1">
                           <p>Area</p>
                        </div>
                        <div class="col-sm-3">
                           <p>Date</p>
                        </div>
                        <div class="col-sm-4">
                        </div>
                     </div>
                     <div class="row mt-3">
                        <div class="col-sm-2">
                           <p class="text-border"><?php echo $row->document_no ?></p>
                        </div>
                        <div class="col-sm-2">
                           <p class="text-border"><?php echo $row->batch_no ?></p>
                        </div>
                        <div class="col-sm-1">
                           <p class="text-border"><?php echo $row->area_code ?></p>
                        </div>
                        <div class="col-sm-3">
                           <p class="float-left mr-3 pr-3 text-border"><?php echo $row->created_on ?></p>
                        </div>
                        <div class="col-sm-4">
                           <div class="row">
                              <div class="col">
                                 <span class="fa-stack float-left mt-1" style="" name="<?php echo 't'. $row->id ?>" id="<?php echo 't'. $row->id ?>" data-flag="0" onclick="getheaderdtl('<?php echo $row->id ?>')">
                                 <em class="fa fa-circle fa-stack-2x text-success bgred"></em>
                                 <em class="far fa-clock fa-stack-1x fa-inverse text-white"></em>
                                 </span>
                                 <button class="mb-1 btn btn-success float-left mr-3" type="button" data-actid="<?php echo $row->activity_code ?>" data-headerid="<?php echo $row->id ?>" onclick="getapproved(this)" id="<?php echo 'Approvebtn'. $row->id ?>">Approve</button>
                                 <i class="fa fa-clock-o" aria-hidden="true"></i>
                                 <button class="mb-1 btn btn-danger float-left mr-4" type="button"  data-actid="<?php echo $row->activity_code ?>" data-headerid="<?php echo $row->id ?>" onclick="getreject(this)" id="<?php echo 'Rejectbtn'. $row->id ?>">Reject</button>
                                 <div class="float-left pointer"><img src="<?php echo base_url(); ?>img/icons/Fluid-Bed-Dryer.png" width="40" data-toggle="modal" onclick="getheaderequipmentdtl('<?php echo $row->id ?>')" data-target="#myModalLarge"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row mt-3 mb-3">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-1"></div>
                        <div class="col">
                           <div class="table-responsive table-bordered watch-table" id="<?php echo 'headertbldiv'.$row->id ?>" style="display:none;">
                              <table class="table" id="<?php echo 'headertbl'.$row->id ?>">
                                 
                              </table>
                           </div>
                           <div class="card card-default text-center remark-box" id="<?php echo 'rejectdiv'.$row->id ?>" style="display:none;">
                              <div class="card-body">
                                 <form>
                                    <div class="form-group">
                                       <label class="text-left float-left mt-1">Remarks</label>
                                       <div class="file float-left ml-5 mb-1">
                                          <label class="file-label">
                                          <input class="file-input" type="file" id="<?php echo 'rejectfile'.$row->id ?>" name="<?php echo 'rejectfile'.$row->id ?>">
                                          <span class="file-cta">
                                          <span class="file-icon">
                                          <i class="fas fa-upload"></i>
                                          </span>
                                          <span class="file-label">
                                          Upload file…
                                          </span>
                                          </span>
                                          </label>
                                       </div>
                                       <input class="form-control height70" type="text" id="<?php echo 'remark'.$row->id ?>" placeholder="">
                                    </div>
                                    <button class="btn btn-primary mb-2" type="submit">Save</button>
                                 </form>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-1"></div>
                     </div>
                  </div>
                  <div class="col-sm-1"></div>
               </div>
            <?php } } } ?>

                           <?php
                if($data['result']=="")
                {

                }
                else
                { ?>
                <?php foreach($data['result']->result() as $row) { if($row->operation_type=="typeA_operation") { ?>
                <?php if($opA==0) { ?> <h4>Type_A operations</h4> <?php } $opA=1; ?> 
               <div class="card card-default" id="<?php echo 'mainrecorddiv'.$row->id ?>">
                  <div class="card-body">
                     <div class="row mt-6">
                        <div class="col-sm-2">
                           <p>Document No.</p>
                        </div>
                        <div class="col-sm-2">
                           <p>Batch</p>
                        </div>
                        <div class="col-sm-1">
                           <p>Area</p>
                        </div>
                        <div class="col-sm-3">
                           <p>Date</p>
                        </div>
                        <div class="col-sm-4">
                        </div>
                     </div>
                     <div class="row mt-3">
                        <div class="col-sm-2">
                           <p class="text-border"><?php echo $row->document_no ?></p>
                        </div>
                        <div class="col-sm-2">
                           <p class="text-border"><?php echo $row->batch_no ?></p>
                        </div>
                        <div class="col-sm-1">
                           <p class="text-border"><?php echo $row->area_code ?></p>
                        </div>
                        <div class="col-sm-3">
                           <p class="float-left mr-3 pr-3 text-border"><?php echo $row->created_on ?></p>
                        </div>
                        <div class="col-sm-4">
                           <div class="row">
                              <div class="col">
                                 <span class="fa-stack float-left mt-1" style="" name="<?php echo 't'. $row->id ?>" id="<?php echo 't'. $row->id ?>" data-flag="0" onclick="getheaderdtl('<?php echo $row->id ?>')">
                                 <em class="fa fa-circle fa-stack-2x text-success bgred"></em>
                                 <em class="far fa-clock fa-stack-1x fa-inverse text-white"></em>
                                 </span>
                                 <button class="mb-1 btn btn-success float-left mr-3" type="button" data-actid="<?php echo $row->activity_code ?>" data-headerid="<?php echo $row->id ?>" onclick="getapproved(this)" id="<?php echo 'Approvebtn'. $row->id ?>">Approve</button>
                                 <i class="fa fa-clock-o" aria-hidden="true"></i>
                                 <button class="mb-1 btn btn-danger float-left mr-4" type="button"  data-actid="<?php echo $row->activity_code ?>" data-headerid="<?php echo $row->id ?>" onclick="getreject(this)" id="<?php echo 'Rejectbtn'. $row->id ?>">Reject</button>
                                 <div class="float-left pointer"><img src="<?php echo base_url(); ?>img/icons/Fluid-Bed-Dryer.png" width="40" data-toggle="modal" onclick="getheaderequipmentdtl('<?php echo $row->id ?>')" data-target="#myModalLarge"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row mt-3 mb-3">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-1"></div>
                        <div class="col">
                           <div class="table-responsive table-bordered watch-table" id="<?php echo 'headertbldiv'.$row->id ?>" style="display:none;">
                              <table class="table" id="<?php echo 'headertbl'.$row->id ?>">
                                 
                              </table>
                           </div>
                           <div class="card card-default text-center remark-box" id="<?php echo 'rejectdiv'.$row->id ?>" style="display:none;">
                              <div class="card-body">
                                 <form>
                                    <div class="form-group">
                                       <label class="text-left float-left mt-1">Remarks</label>
                                       <div class="file float-left ml-5 mb-1">
                                          <label class="file-label">
                                          <input class="file-input" type="file" id="<?php echo 'rejectfile'.$row->id ?>" name="<?php echo 'rejectfile'.$row->id ?>">
                                          <span class="file-cta">
                                          <span class="file-icon">
                                          <i class="fas fa-upload"></i>
                                          </span>
                                          <span class="file-label">
                                          Upload file…
                                          </span>
                                          </span>
                                          </label>
                                       </div>
                                       <input class="form-control height70" type="text" id="<?php echo 'remark'.$row->id ?>" placeholder="">
                                    </div>
                                    <button class="btn btn-primary mb-2" type="submit">Save</button>
                                 </form>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-1"></div>
                     </div>
                  </div>
                  <div class="col-sm-1"></div>
               </div>
            <?php } } } ?>

                           <?php
                if($data['result']=="")
                {

                }
                else
                { ?>
                <?php foreach($data['result']->result() as $row) { if($row->operation_type=="typeB_operation") { ?>
                <?php if($opB==0) { ?> <h4>Type_B operations</h4> <?php } $opB=1; ?> 
               <div class="card card-default" id="<?php echo 'mainrecorddiv'.$row->id ?>">
                  <div class="card-body">
                     <div class="row mt-6">
                        <div class="col-sm-2">
                           <p>Document No.</p>
                        </div>
                        <div class="col-sm-2">
                           <p>Batch</p>
                        </div>
                        <div class="col-sm-1">
                           <p>Area</p>
                        </div>
                        <div class="col-sm-3">
                           <p>Date</p>
                        </div>
                        <div class="col-sm-4">
                        </div>
                     </div>
                     <div class="row mt-3">
                        <div class="col-sm-2">
                           <p class="text-border"><?php echo $row->document_no ?></p>
                        </div>
                        <div class="col-sm-2">
                           <p class="text-border"><?php echo $row->batch_no ?></p>
                        </div>
                        <div class="col-sm-1">
                           <p class="text-border"><?php echo $row->area_code ?></p>
                        </div>
                        <div class="col-sm-3">
                           <p class="float-left mr-3 pr-3 text-border"><?php echo $row->created_on ?></p>
                        </div>
                        <div class="col-sm-4">
                           <div class="row">
                              <div class="col">
                                 <span class="fa-stack float-left mt-1" style="" name="<?php echo 't'. $row->id ?>" id="<?php echo 't'. $row->id ?>" data-flag="0" onclick="getheaderdtl('<?php echo $row->id ?>')">
                                 <em class="fa fa-circle fa-stack-2x text-success bgred"></em>
                                 <em class="far fa-clock fa-stack-1x fa-inverse text-white"></em>
                                 </span>
                                 <button class="mb-1 btn btn-success float-left mr-3" type="button" data-actid="<?php echo $row->activity_code ?>" data-headerid="<?php echo $row->id ?>" onclick="getapproved(this)" id="<?php echo 'Approvebtn'. $row->id ?>">Approve</button>
                                 <i class="fa fa-clock-o" aria-hidden="true"></i>
                                 <button class="mb-1 btn btn-danger float-left mr-4" type="button"  data-actid="<?php echo $row->activity_code ?>" data-headerid="<?php echo $row->id ?>" onclick="getreject(this)" id="<?php echo 'Rejectbtn'. $row->id ?>">Reject</button>
                                 <div class="float-left pointer"><img src="<?php echo base_url(); ?>img/icons/Fluid-Bed-Dryer.png" width="40" data-toggle="modal" onclick="getheaderequipmentdtl('<?php echo $row->id ?>')" data-target="#myModalLarge"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row mt-3 mb-3">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-1"></div>
                        <div class="col">
                           <div class="table-responsive table-bordered watch-table" id="<?php echo 'headertbldiv'.$row->id ?>" style="display:none;">
                              <table class="table" id="<?php echo 'headertbl'.$row->id ?>">
                                 
                              </table>
                           </div>
                           <div class="card card-default text-center remark-box" id="<?php echo 'rejectdiv'.$row->id ?>" style="display:none;">
                              <div class="card-body">
                                 <form>
                                    <div class="form-group">
                                       <label class="text-left float-left mt-1">Remarks</label>
                                       <div class="file float-left ml-5 mb-1">
                                          <label class="file-label">
                                          <input class="file-input" type="file" id="<?php echo 'rejectfile'.$row->id ?>" name="<?php echo 'rejectfile'.$row->id ?>">
                                          <span class="file-cta">
                                          <span class="file-icon">
                                          <i class="fas fa-upload"></i>
                                          </span>
                                          <span class="file-label">
                                          Upload file…
                                          </span>
                                          </span>
                                          </label>
                                       </div>
                                       <input class="form-control height70" type="text" id="<?php echo 'remark'.$row->id ?>" placeholder="">
                                    </div>
                                    <button class="btn btn-primary mb-2" type="submit">Save</button>
                                 </form>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-1"></div>
                     </div>
                  </div>
                  <div class="col-sm-1"></div>
               </div>
            <?php } } } ?>

            <h5>Sanitization - Solution Preparation</h5>
            <div class="card card-default">
              <div class="card-body">
                  <div class="row mt-6">
                  <div class="col-sm-12">
                    <table style="border:0px">
              <tr><th class="col-sm-2">Document No.</th><th class="col-sm-3">Solution Name</th><th class="col-sm-1">Area</th><th class="col-sm-3">Date</th><th class="col-sm-3"></th></tr>
                      <?php
                      if($data['result2']!=null){

                      
                      foreach($data['result2']->result_array() as $row2) {
?>
<tr><td><?= $row2["doc_id"]?></td><td><?= $row2["solutionname"]?></td><td><?= $row2["area_code"]?></td><td><?= $row2["created_on"]?></td><td><span class='fa-stack float-left mt-1' style='' name='st<?= $row2["id"]?>' id='st<?= $row2["id"]?>' data-flag='0' onclick='getLogsview(this.value="<?= $row2["doc_id"]?>_27")'> <em class='fa fa-circle fa-stack-2x text-success bgred'></em><em class='far fa-clock fa-stack-1x fa-inverse text-white'></em></span> <button class='mb-1 btn btn-success float-left mr-3' onclick='sapprove(this.value="<?= $row2["id"]?>_27")'>Approve</button> <button class='mb-1 btn btn-danger float-left mr-4' onclick='sreject(this.value="<?= $row2["id"]?>_27")'>Reject</button></td></tr>
<?php
                      }
                      }
                      ?>
                    
                    </table>
                  </div>
                  </div>
              </div>
            </div>

            <h5>Sanitization - Drain Point Cleaning</h5>
            <div class="card card-default">
              <div class="card-body">
                  <div class="row mt-6">
                  <div class="col-sm-12">
                    <table style="border:0px">
              <tr><th class="col-sm-2">Document No.</th><th class="col-sm-3">Sanitization Used</th><th class="col-sm-1">Area</th><th class="col-sm-3">Date</th><th class="col-sm-3"></th></tr>
                      <?php
                      if($data['result3']!=null){

                      
                      foreach($data['result3']->result_array() as $row3) {
?>
<tr><td><?= $row3["document_no"]?></td><td><?= $row3["sanitizationused"]?></td><td><?= $row3["area_code"]?></td><td><?= $row3["created_on"]?></td><td><span class='fa-stack float-left mt-1' style='' name='stt<?= $row3["id"]?>' id='stt<?= $row3["id"]?>' data-flag='0' onclick='getLogsview(this.value="<?= $row3["document_no"]?>_29")'> <em class='fa fa-circle fa-stack-2x text-success bgred'></em><em class='far fa-clock fa-stack-1x fa-inverse text-white'></em></span> <button class='mb-1 btn btn-success float-left mr-3' onclick='sapprove(this.value="<?= $row3["id"]?>_29")'>Approve</button> <button class='mb-1 btn btn-danger float-left mr-4' onclick='sreject(this.value="<?= $row3["id"]?>_29")'>Reject</button></td></tr>
<?php

                      }
                      }
                      ?>
                    
                    </table>
                  </div>
                  </div>
              </div>
            </div>

            <h5>Sanitization - Daily Cleaning</h5>
            <div class="card card-default">
              <div class="card-body">
                  <div class="row mt-6">
                  <div class="col-sm-12">
                    <table style="border:0px">
              <tr><th class="col-sm-2">Document No.</th><th class="col-sm-3">Drain Points</th><th class="col-sm-1">Area</th><th class="col-sm-3">Date</th><th class="col-sm-3"></th></tr>
                      <?php
                      if($data['result4']!=null){

                      
                      foreach($data['result4']->result_array() as $row4) {
?>
<tr><td><?= $row4["document_no"]?></td><td><?= $row4["drain_points"]?></td><td><?= $row4["area_code"]?></td><td><?= $row4["created_on"]?></td><td><span class='fa-stack float-left mt-1' style='' name='sttt<?= $row4["id"]?>' id='sttt<?= $row4["id"]?>' data-flag='0' onclick='getLogsview(this.value="<?= $row4["document_no"]?>_30")'> <em class='fa fa-circle fa-stack-2x text-success bgred'></em><em class='far fa-clock fa-stack-1x fa-inverse text-white'></em></span> <button class='mb-1 btn btn-success float-left mr-3' onclick='sapprove(this.value="<?= $row4["id"]?>_30")'>Approve</button> <button class='mb-1 btn btn-danger float-left mr-4' onclick='sreject(this.value="<?= $row4["id"]?>_30")'>Reject</button></td></tr>
<?php

                      }
                      }
                      ?>
                    
                    </table>
                  </div>
                  </div>
              </div>
            </div>


            <h5>Production - InProcess Container Cleaning</h5>
            <div class="card card-default">
              <div class="card-body">
                  <div class="row mt-6">
                  <div class="col-sm-12">
                    <table style="border:0px">
              <tr><th class="col-sm-2">Document No.</th><th class="col-sm-3">Product Name</th><th class="col-sm-1">Area</th><th class="col-sm-3">Date</th><th class="col-sm-3"></th></tr>
                      <?php
                      if($data['result5']!=null){

                      
                      foreach($data['result5']->result_array() as $row5) {
?>
<tr><td><?= $row5["document_no"]?></td><td><?= $row5["product_name"]?></td><td><?= $row5["area_code"]?></td><td><?= $row5["created_on"]?></td><td><span class='fa-stack float-left mt-1' style='' name='stttt<?= $row5["id"]?>' id='sttt<?= $row5["id"]?>' data-flag='0' onclick='getLogsview(this.value="<?= $row5["document_no"]?>_4")'> <em class='fa fa-circle fa-stack-2x text-success bgred'></em><em class='far fa-clock fa-stack-1x fa-inverse text-white'></em></span> <button class='mb-1 btn btn-success float-left mr-3' onclick='sapprove(this.value="<?= $row5["id"]?>_4")'>Approve</button> <button class='mb-1 btn btn-danger float-left mr-4' onclick='sreject(this.value="<?= $row5["id"]?>_4")'>Reject</button></td></tr>
<?php

                      }
                      }
                      ?>
                    
                    </table>
                  </div>
                  </div>
              </div>
            </div>


            <h5>Production - Accessory Cleaning</h5>
            <div class="card card-default">
              <div class="card-body">
                  <div class="row mt-6">
                  <div class="col-sm-12">
                    <table style="border:0px">
              <tr><th class="col-sm-2">Document No.</th><th class="col-sm-3">Product Name</th><th class="col-sm-1">Area</th><th class="col-sm-3">Date</th><th class="col-sm-3"></th></tr>
                      <?php
                      if($data['result6']!=null){

                      
                      foreach($data['result6']->result_array() as $row6) {
?>
<tr><td><?= $row6["document_no"]?></td><td><?= $row6["product_name"]?></td><td><?= $row6["area_code"]?></td><td><?= $row6["created_on"]?></td><td><span class='fa-stack float-left mt-1' style='' name='sttttt<?= $row6["id"]?>' id='stttt<?= $row6["id"]?>' data-flag='0' onclick='getLogsview(this.value="<?= $row6["document_no"]?>_26")'> <em class='fa fa-circle fa-stack-2x text-success bgred'></em><em class='far fa-clock fa-stack-1x fa-inverse text-white'></em></span> <button class='mb-1 btn btn-success float-left mr-3' onclick='sapprove(this.value="<?= $row6["id"]?>_26")'>Approve</button> <button class='mb-1 btn btn-danger float-left mr-4' onclick='sreject(this.value="<?= $row6["id"]?>_26")'>Reject</button></td></tr>
<?php
//echo "<tr><td>".$row6["document_no"]."</td><td>".$row6["product_name"]."</td><td>".$row6["area_code"]."</td><td>".$row6["created_on"]."</td><td><span class='fa-stack float-left mt-1' style='' name='sttttt'".$row6["id"]."' id='sttttt'".$row6["id"]."' data-flag='0' onclick='getheaderdtl1tttt(".$row6["id"].")'> <em class='fa fa-circle fa-stack-2x text-success bgred'></em><em class='far fa-clock fa-stack-1x fa-inverse text-white'></em></span> <button class='mb-1 btn btn-success float-left mr-3' onclick='acapprove(".$row6["id"].")'>Approve</button> <button class='mb-1 btn btn-danger float-left mr-4' onclick='acreject(".$row6["id"].")'>Reject</button></td></tr>";
//print_r($row);
                      }
                      }
                      ?>
                    
                    </table>
                  </div>
                  </div>
              </div>
            </div>

            <h5>Production - Portable Equipment Cleaning</h5>
            <div class="card card-default">
              <div class="card-body">
                  <div class="row mt-6">
                  <div class="col-sm-12">
                    <table style="border:0px">
              <tr><th class="col-sm-2">Document No.</th><th class="col-sm-3">Batch Code</th><th class="col-sm-1">Area</th><th class="col-sm-3">Date</th><th class="col-sm-3"></th></tr>
                      <?php
                      if($data['result7']!=null){

                      
                      foreach($data['result7']->result_array() as $row7) {
?>
<tr><td><?= $row7["document_no"]?></td><td><?= $row7["batch_code"]?></td><td><?= $row7["area_code"]?></td><td><?= $row7["created_on"]?></td><td><span class='fa-stack float-left mt-1' style='' name='stttttt<?= $row7["id"]?>' id='sttttt<?= $row7["id"]?>' data-flag='0' onclick='getLogsview(this.value="<?= $row7["document_no"]?>_5")'> <em class='fa fa-circle fa-stack-2x text-success bgred'></em><em class='far fa-clock fa-stack-1x fa-inverse text-white'></em></span> <button class='mb-1 btn btn-success float-left mr-3' onclick='sapprove(this.value="<?= $row7["id"]?>_5")'>Approve</button> <button class='mb-1 btn btn-danger float-left mr-4' onclick='sreject(this.value="<?= $row7["id"]?>_5")'>Reject</button><div class="float-left pointer"><img src="<?php echo base_url(); ?>img/icons/Fluid-Bed-Dryer.png" width="30" data-toggle="modal" onclick='getequipmentdtlnow(this.value="<?= $row7["id"]?>_5")'></div></td></tr>
<?php

                      }
                      }
                      ?>
                    
                    </table>
                  </div>
                  </div>
              </div>
            </div>

            
            </div>
      </div>
      </div>   
      </section>
      <!-- Page footer--> 
      <!-- Page footer-->
      <footer class="footer-container text-center"><span>  SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
      </div>
      <!-- =============== VENDOR SCRIPTS ===============--> 
      <!-- MODERNIZR--> 
      <script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API-->
      <script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL-->
      <script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next-->
      <script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script>
      <script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script>
      <script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script>
      <script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script>
      <script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script>
      <!-- =============== APP SCRIPTS ===============-->
      <!-- Modal Large-->
      <div class="modal fade" id="myModalLarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabelLarge" style="display: none;" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabelLarge">Used equipments.</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
               </div>
               <div class="modal-body">
                  <section class="page-tasks" id="Listofequip">
                     <ul class="task-list list-unstyled" id="eqlist">
                        
                     </ul>
                     <ul class="task-list list-unstyled" id="porteqlist">
                     </ul>
                  </section>
               </div>
            </div>
         </div>
      </div>
      <script src="js/app.js"></script> 
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script>
        function getheaderdtl(obj)
        {
          debugger;
          var headerid = obj;
          $.ajax({
            url: "<?= base_url()?>User/getheaderdetail",
            type: 'POST',
            data: {headerid:headerid},
            success: function(res) {
              //console.log(res);
                var logrec = "<thead><tr><th scope='col'>Start</th><th scope='col'>End</th><th scope='col'>Performed By</th><th scope='col'>Change over</th></tr></thead>";
                $.each(res, function (index, value) 
                {
                    logrec = logrec.concat('<tr><td>'+value.start_time+'</td><td>'+value.end_time+'</td><td>'+value.performed_by+'</td><td>'+value.checked_by+'</td></tr>');
                });
                $("#headertbl"+obj).html(logrec);

                    if($("#t"+obj).attr("data-flag")=="0")
                    {
                        $("#t"+obj).attr("data-flag", "1");
                        $("#headertbldiv"+obj).show(1000);
                    }
                    else
                    {
                        $("#t"+obj).attr("data-flag", "0");
                        $("#headertbldiv"+obj).hide(1000);    
                    }
          }
        });          
        }

        function getheaderequipmentdtl(obj)
        {
          debugger;
          var headerid = obj;
          $.ajax({
            url: "<?= base_url()?>User/getequipmentdetail",
            type: 'POST',
            data: {headerid:headerid},
            success: function(res) {
              //console.log(res);
                var logrec = "";
                $.each(res, function (index, value) 
                {
                  logrec = logrec.concat("<li><span class='view' id='eqdiv"+obj+"' ><div class='row'><div class='col-sm-12 pl0'><div class='row'><div class='col-sm-4 listexecutesop2'><img class='mb-2 mt-2 wt42' src='http://localhost/SMHS_REPOSITORIES/SMHS/img/icons/"+value.equipment_icon+"'><span class='f15'>"+value.equipment_name+"</span></div><div class='col-sm-7 mt-4'><p class='f15 mt-2'>"+value.sop_code+" "+value.sop_name+"</p></div><div class='col-sm-1 disable-button-color float-right text-right pl00'><span class='fa-stack mt30 pointer' id='tt"+value.equipment_code+"' name='tt"+value.equipment_code+"' data-flag='0' data-id='"+obj+"' data-equipcode='"+value.equipment_code+"' onclick='getequiplog(this);'><em class='fa fa-circle fa-stack-2x text-success bgred'></em><em class='far fa-clock fa-stack-1x fa-inverse text-white'></em></span><div class='card card-default tableleftalign' style='display:none;'' id='bdtbldiv"+value.equipment_code+"'><div class='card-header p-5'><center>Equipment Log Record</center></div><table class='table table-dark' id='bdtbl"+value.equipment_code+"'></table></div></div></div></div></div></span></li>");
                });
                $("#eqlist").html(logrec);
          }
        });          
        }

        function getequiplog(element)
        {
          debugger;
          var headerid = $(element).data("id");          
          var obj = $(element).data("equipcode");
          var equipmentcode = obj;
          $.ajax({
            url: "<?= base_url()?>User/getEachequipmentlogdetail",
            type: 'POST',
            data: {headerid:headerid,equipmentcode:equipmentcode},
            success: function(res) {
              console.log(res);
                var logrec = "<thead><tr><th scope='col'>Start time</th><th scope='col'>End time</th><th scope='col'>Performed By</th><th scope='col'>End By</th></tr></thead>";
                $.each(res, function (index, value) 
                {
                    logrec = logrec.concat('<tr><td>'+value.strattime+'</td><td>'+value.endtime+'</td><td>'+value.performed_by+'</td><td>'+value.checked_by+'</td></tr>');
                });
                $("#bdtbl"+obj).html(logrec);

                    if($("#tt"+obj).attr("data-flag")=="0")
                    {
                        $("#tt"+obj).attr("data-flag", "1");
                        $("#bdtbldiv"+obj).show(1000);
                    }
                    else
                    {
                        $("#tt"+obj).attr("data-flag", "0");
                        $("#bdtbldiv"+obj).hide(1000);    
                    }
          }
        });          
        }

        function getapproved(element)
        {
          debugger;
          var headerid = $(element).data("headerid");
          var actid = $(element).data("actid");          
          $.ajax({
            url: "<?= base_url()?>User/senttoapproval",
            type: 'POST',
            data: {headerid:headerid,actid:actid},
            success: function(res) {
              console.log(res);
                alert("Successfully approved.")
                $("#mainrecorddiv"+headerid).hide(1000);
          }
        });          
        }

        /*function getreject(element)
        {
          debugger;
          var headerid = $(element).data("headerid");
          var actid = $(element).data("actid");          
          $.ajax({
            url: "<?= base_url()?>User/senttoreject",
            type: 'POST',
            data: {headerid:headerid,actid:actid},
            success: function(res) {
              console.log(res);
                alert("Successfully approved.")
                $("#mainrecorddiv"+headerid).hide(1000);
          }
        });          
        }*/
      </script>


      <script type="text/javascript">
        function sapprove(rid){
          res = rid.split("_");
          $.ajax({
            url: "<?= base_url()?>User/sentforQAapprovalsol",
            type: 'POST',
            data: {rid:res[0],aid:res[1]},
            success: function(res) {
              //console.log(res);
              if(res.status==1){
                alert("Approved Successfully.");
                location.reload(true);
              }
              else{
                alert("Something went wrong!");
              }
              
          }
        }); 
        }

        function sreject(rid){
          res = rid.split("_");
          $.ajax({
            url: "<?= base_url()?>User/rejectedQAapprovalsol",
            type: 'POST',
            data: {rid:res[0],aid:res[1]},
            success: function(res) {
              //console.log(res);
              if(res.status==1){
                alert("Rejected Successfully.");
                location.reload(true);
              }
              else{
                alert("Something went wrong!");
              }
              
            }
          }); 
        }
      </script>

      <script type="text/javascript">
        function getLogsview(rid){
          //alert(dev);
          //$("#myb").click();
          res = rid.split("_");
          $.ajax({
            url: "<?= base_url()?>User/getLogsview",
            type: 'POST',
            data: {rid:res[0],aid:res[1]},
            success: function(res) {
              console.log(res);
              var tres ="";
              $.each(res, function(key,val) {
                  //alert(val.created_on);
                  var ckon = val.checked_on!=null ? val.checked_on : "";
                  var ckby = val.checked_by!=null ? val.checked_by : "";
                  tres = tres.concat("<tr><td>"+val.created_on+"</td><td>"+ckon+"</td><td>"+val.created_by+"</td><td>"+ckby+"</td></tr>");
              });
              $("#tdata").html(tres);
              $("#myb").click();
            }
          }); 
        }
      </script>


      <script type="text/javascript">
         function getequipmentdtlnow(rid){
            res = rid.split("_");
            //alert(rid);
            $.ajax({
            url: "<?= base_url()?>User/getequipmentdtlnow",
            type: 'POST',
            data: {rid:res[0],aid:res[1]},
            success: function(res) {
              //console.log(res);
              
              var tres2 ="";
              $.each(res, function(key,val) {
                  //alert(val.created_on);
                  tres2 = tres2.concat("<tr><td><img src='<?= base_url()?>img/icons/"+val.equipment_icon+"' style='width:80px'></td><td>"+val.equipment_name+"</td><td>"+val.sop_code+"  | "+val.sop_name+"</td><td>"+val.changepart+"</td></tr>");
              });
              $("#tdata2").html(tres2);
              $("#myb2").click();
               
            }
          }); 

         }
      </script>


      <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalLarge1" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

      <div class="modal fade" id="myModalLarge1" tabindex="-1" role="dialog" aria-labelledby="myModalLabelLarge1" style="display: none;" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabelLarge1">Activity Logs</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
               </div>
               <div class="modal-body">
                  <table>
                    <thead>
                    <tr><th>Start</th><th>End</th><th>Performed By</th><th>Change Over</th></tr>
                    </thead>
                    <tbody id="tdata">
                      
                    </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>

      <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalLarge2" hidden id="myb2" data-backdrop="static" data-keyboard="false"></button>

      <div class="modal fade" id="myModalLarge2" tabindex="-1" role="dialog" aria-labelledby="myModalLabelLarge2" style="display: none;" aria-hidden="true">
         <div class="modal-dialog modal-lg">
            <div class="modal-content">
               <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabelLarge2">Used Equipment</h4>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
               </div>
               <div class="modal-body">
                  <table>
                    <thead class="hide">
                    <tr><th></th><th></th><th></th><th></th></tr>
                    </thead>
                    <tbody id="tdata2">
                      
                    </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>