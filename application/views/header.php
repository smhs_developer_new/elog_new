<?php
// Turn off all error reporting
error_reporting(0);
?>
<?php
//$diff = time() - $_SESSION['__ci_last_regenerate'];
//
//if ($diff > 300) {//converted to 300 as new session out is 5 minutes
//    redirect(base_url() . 'User/logout');
//} else {
//    $_SESSION['__ci_last_regenerate'] = time(); //set new timestamp
//}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="Bootstrap Admin App">
        <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
        <link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>favicon.ico">
        <title>SMHS - eLog System</title>
        <!-- =============== VENDOR STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/brands.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/regular.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/solid.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/fontawesome.css">
        <!-- SIMPLE LINE ICONS-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/simple-line-icons/css/simple-line-icons.css">
        <!-- ANIMATE.CSS-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/animate.css/animate.css">
        <!-- WHIRL (spinners)-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/whirl/dist/whirl.css">
        <!-- =============== PAGE VENDOR STYLES ===============-->
        <!-- =============== BOOTSTRAP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css" id="bscss">
        <!-- =============== APP STYLES ===============-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/app.css" id="maincss">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">



        <!-- TAGS INPUT-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.css"><!-- SLIDER CTRL-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-slider/dist/css/bootstrap-slider.css"><!-- CHOSEN-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/chosen-js/chosen.css"><!-- DATETIMEPICKER-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.css"><!-- COLORPICKER-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css"><!-- SELECT2-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/select2/dist/css/select2.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@ttskch/select2-bootstrap4-theme/dist/select2-bootstrap4.css"><!-- WYSIWYG-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap-wysiwyg/css/style.css"><!-- =============== BOOTSTRAP STYLES ===============-->
    </head>

    <body>
        <div class="wrapper"> 
            <!-- top navbar-->
            <header class="topnavbar-wrapper"> 
                <!-- START Top Navbar-->
                <nav class="navbar topnavbar"> 
                    <!-- START navbar header-->
                    <div class="navbar-header"><a class="navbar-brand" href="<?php echo base_url(); ?>logo">
                            <div class="brand-logo font-weight-bold text-white">SMHS - eLog System</div>
                            <div class="brand-logo-collapsed font-weight-bold text-white">SMHS</div>
                        </a></div>
                    <!-- END navbar header--> 
                    <!-- START Left navbar-->
                    <ul class="navbar-nav mr-auto flex-row">
                        <li class="nav-item"> 
                            <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link d-none d-md-block d-lg-block d-xl-block" href="#" data-trigger-resize="" data-toggle-state="aside-collapsed"><em class="fas fa-bars"></em></a><!-- Button to show/hide the sidebar on mobile. Visible on mobile only.--><a class="nav-link sidebar-toggle d-md-none" href="#" data-toggle-state="aside-toggled" data-no-persist="true"><em class="fas fa-bars"></em></a></li>
                        <!-- START User avatar toggle-->
                        <li class="nav-item d-none d-md-block"> 
                            <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link" id="user-block-toggle" href="#user-block" data-toggle="collapse"><em class="icon-user"></em></a></li>
                        <!-- END User avatar toggle--> 
                    </ul>
                    <!-- END Left navbar--> 
                    <!-- START Right Navbar-->
                    <ul class="navbar-nav flex-row">
                        <!-- Fullscreen (only desktops)-->
                        <li class="nav-item d-none d-md-block"><a class="nav-link" href="<?php echo base_url(); ?>#" data-toggle-fullscreen=""><em class="fas fa-expand"></em></a></li>
                        <!-- START Offsidebar button-->
                        <li class="nav-item"><a href="<?php echo base_url(); ?>#" class="nav-link"></a></li>
                        <li class="dropdown langs text-normal ng-scope" uib-dropdown="" is-open="status.isopenLang" data-ng-controller="LangCtrl" style=""> <a href="<?php echo base_url(); ?>javascript:;" class="dropdown-toggle  dropdown-toggleactive-flag" uib-dropdown-toggle="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <div class="flag flags-american" style=""></div>
                            </a>
                            <ul class="dropdown-menu with-arrow  pull-right list-langs dropdown-menu-scaleIn animated flipInX" role="menu">
                                <li data-ng-show="lang !== 'English'" aria-hidden="true" class="ng-hide" style=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('English')">
                                        <div class="flag flags-american"></div>
                                        English</a></li>
                                <li data-ng-show="lang !== 'Español'" aria-hidden="false" class="" style=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('Español')">
                                        <div class="flag flags-spain"></div>
                                        Español</a></li>
                                <li data-ng-show="lang !== 'Portugal'" aria-hidden="false" class="" style=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('Portugal')">
                                        <div class="flag flags-portugal"></div>
                                        Portugal</a></li>
                                <li data-ng-show="lang !== '中文'" aria-hidden="false" class=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('中文')">
                                        <div class="flag flags-china"></div>
                                        中文</a></li>
                                <li data-ng-show="lang !== '日本語'" aria-hidden="false" class="" style=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('日本語')">
                                        <div class="flag flags-japan"></div>
                                        日本語</a></li>
                                <li data-ng-show="lang !== 'Русский язык'" aria-hidden="false" class="" style=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('Русский язык')">
                                        <div class="flag flags-russia"></div>
                                        Русский язык</a></li>
                            </ul>
                        </li>

                        <!-- END Offsidebar menu-->
                    </ul>
                    <!-- END Right Navbar--> 

                </nav>
                <!-- END Top Navbar--> 
            </header>
            <!-- sidebar-->
            <aside class="aside-container"> 
                <!-- START Sidebar (left)-->
                <div class="aside-inner">
                    <nav class="sidebar" data-sidebar-anyclick-close=""> 
                        <!-- START sidebar nav-->
                        <ul class="sidebar-nav">
                            <!-- START user info-->
                            <li class="has-user-block">
                                <div class="collapse" id="user-block">
                                    <div class="item user-block"> 
                                        <!-- User picture-->
                                        <div class="user-block-picture">
                                            <div class="user-block-status"><img class="img-thumbnail rounded-circle" src="<?php echo base_url(); ?>img/user/02.jpg" alt="Avatar" width="60" height="60">
                                                <div class="circle bg-success circle-lg"></div>
                                            </div>
                                        </div>
                                        <!-- Name and Job-->
                                        <div class="user-block-info"><span class="user-block-name">Hello, <?php echo $this->session->userdata('empname') ?></span><span class="user-block-role"><?php if ($this->session->userdata('empname')) { ?><a href="<?php echo base_url() ?>user/logout" >Logout </a><?php } ?></span></div>
                                    </div>
                                </div>
                            </li>
                            <!-- END user info--> 
                            <!-- Iterates over all sidebar items-->
                            <li class="nav-heading text-bold"><span data-localize="sidebar.heading.HEADER">Main Navigation</span>
                            <li><a href="<?php echo base_url(); ?>User/home" title="Plant master"><em class="fas fa-home"></em><span data-localize="sidebar.nav.Plant master">Home</span></a></li>
                            <?php if ($this->session->userdata('roleid') == 6) { ?>
                                <li ><a href="<?php echo base_url(); ?>manage_room" title="Plant master"><em class="fas fa-home"></em><span data-localize="sidebar.nav.Paonta Shahib Master">Master</span></a></li>
                                <li ><a href="<?php echo base_url(); ?>audit_trail_report" title="Plant master"><em class="fas fa-home"></em><span data-localize="sidebar.nav.Paonta Shahib Master">Report</span></a></li>
                            <?php } ?>
                            <?php
                            $name = "";
                            foreach ($result->result() as $row) {
                                if ($name != $row->activitytype_name) {
                                    $name = $row->activitytype_name;
                                    ?> 
                                    <li class=""><a href="" title="Plant master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Plant master"><?php echo $row->activitytype_name ?></span></a></li>
                                            <?php }
                                        } ?>

                            <!-- <?php if ($this->session->userdata('roleid') == 1) { ?>
                       
                                 <li class=""><a href="<?php echo base_url(); ?>Production/area" title="Plant master"><em class="fas fa-cogs"></em><span data-localize="sidebar.nav.Plant master">Production</span></a></li>
                                 <li ><a href="<?php echo base_url(); ?>Sanitization/area" title="Plant master"><em class="fas fa-broom"></em><span data-localize="sidebar.nav.Plant master">Sanitization</span></a></li>
<?php } else { ?>
                                 <li><a href="<?php echo base_url(); ?>User/AS" title="Plant master"><em class="fas fa-wrench"></em><span data-localize="sidebar.nav.Plant master">Approvals</span></a></li>
                                 <li><a href="<?php echo base_url(); ?>Report/Reports" title="Plant master"><em class="fas fa-file"></em><span data-localize="sidebar.nav.Plant master">Reports</span></a></li>
                                 </li>
<?php } ?>
                            -->
                        </ul>
                        </li>
                        </ul>
                        <!-- END sidebar nav--> 
                    </nav>
                </div>
                <!-- END Sidebar (left)--> 
            </aside>