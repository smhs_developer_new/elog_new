  <!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Bulk Upload</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item active"><a href="<?= base_url()?>Admin/SopList">SOP</a></li>
            
          </ol>
        </div>
      </div>
	  


<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">

<a class="btn btn-primary btn-lg" href="<?= base_url()?>admin/SopList" > Back To list</a>  
&nbsp;&nbsp;&nbsp;
<a class="btn btn-primary btn-lg" href="<?= base_url()?>admin/AddSop"> Add New SOP</a>

</div>
</div>
</div>
</div>

<form enctype="multipart/form-data" id="batchsop">
    <div class="card card-default">
        <div class="card-body">

	  <fieldset>
	  <div class="col-auto">
      <div class="form-row">
	  <div class="col-lg-6 mb-3"><label for="edso">Upload File <span style="color: red">Allowed Format : xls, xlsx, csv </span></label><br><input class="" id="uploadFile" type="file" placeholder="" name="uploadFile" accept=".xlsx, .xls, .csv" required>
      </div>
	  <div class="col-lg-6 mb-3"><label for="edso"><a class="btn btn-primary btn-lg" href="<?= base_url()?>samplexls/sop.xlsx">Click Here To Download Sample File</a></label><br>
      </div>
	  </fieldset>
	  </div>
	  </div>
	
	<!--Upload excel file : 
    <input type="file" name="uploadFile" value="" /><br><br>
	<input type="hidden" name ="tablename" value="userdetails">-->


    <!--<input type="submit" name="submit" value="Upload" />-->
<div class="card card-default">
                     
                     <div class="card-body" >
                        
                          
<div class="row disable-button-color">
      <div class="col-sm-12 text-center" id="sbtn"><button id="sbbtn" class="btn btn-success btn-lg" type="submit" name="submit">&nbsp;&nbsp;&nbsp;&nbsp;Upload File &nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      </div>

                        
                     </div>
                  </div><!-- END card-->



</form>

        </div>
      </div>
      
    </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?= base_url()?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?= base_url()?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?= base_url()?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?= base_url()?>vendor/i18next/i18next.js"></script> 
<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?= base_url()?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?= base_url()?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?= base_url()?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 

<!-- =============== APP SCRIPTS ===============--> 

<script src="<?= base_url()?>js/app.js"></script> 
<script type="text/javascript" src="js/script.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>

<script type="text/javascript">
$(document).ready(function(){

 $('#batchsop').submit(function () {

	 var file_data = $('#uploadFile').prop('files')[0];
     var filename = file_data.name;
	 var extension = filename.substring(filename.lastIndexOf('.') + 1);
     //console.log( "The file extension is " + extension );
	 if(extension == "xls" || extension == "xlsx" || extension == "csv"){
	  $.ajax({
        url: '<?= base_url()?>Admin/uploadSopExcel',
        type: 'POST',
        //data: $(this).serialize(),
		//data: form_data,
		data:new FormData(this),
		processData: false,
        contentType: false,
        cache: false,
	    async:false,
        success: function (res) {
          //console.log(data);
          if(res.status==1){
           // $("#validationnow21").attr("disabled",true);
           // $("#validationnow22").attr("disabled",false);
           
			alert("File Uploaded And Processed successfully");
            window.location.href = "<?= base_url()?>Admin/SopList";
          }
		  else{
			  alert("Error Occured In File, Please check And Retry Again");
		  }
        }
      });
	  }
else{
 alert("You cann't upload this file.");
  $("#uploadFile").val(null);
}
      return false;
    });


});  
</script>