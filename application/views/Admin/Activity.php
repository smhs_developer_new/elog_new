<!-- Main section-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <?php if($data['acttypeid']==15) { ?>
        <div class="col-sm-3 pl-0">First time Plant Set-up</div>
      <?php } else { ?>
        <div class="col-sm-3 pl-0">Create Master Data</div>
      <?php } ?>
        <form class="search-form col-sm-5 pl-0">
          <!--<em class="icon-magnifier"></em>
          <input class="form-control" type="email" placeholder="Enter room code or number on name">-->
        </form>
        <div class="col-sm-4 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
          </ol>
        </div>
      </div>
          
			      <?php foreach($data['activity']->result() as $row) { ?>
            <div class="card card-default">
            <div class="card-body">

            <span class="view" id='<?php echo "eqdiv" .$row->id ?>'>
            <div class="row">
            <div class="col-sm-12 pl0">
            <div class="row">
            <div class="col-sm-6">
            <img class="wt42" src="<?php echo base_url(); ?>img/<?php echo $row->activity_icon ?>">
            <span class="f15"><?php echo $row->activity_name ?></span></div>
            <div class="col-sm-6 disable-button-color float-right text-right pl00" id='<?php echo "show" .$row->id ?>'>             
            <a class="btn btn-labeled btn-success mt-3 pl10" href="<?= base_url()?><?php echo $row->activityfunc_tocall ?>" type="button"><span class="btn-label"><i class="fa fa-check"></i></span>Go</a>          
            </div>
            </div>
            </div>
            </div>
            </span> 
            </div>
            </div>
            <?php } ?>			
                   		  
          <!--<div class="row mt30 mb-5">
            <div class="col-sm-12 text-center"> <a href="<?php echo base_url(); ?>#"><img src="<?php echo base_url(); ?>img/down-arrow.png" width="50" class="bounce"></a> </div>
          </div>-->
    </div>
  </section>
<!-- Page footer-->