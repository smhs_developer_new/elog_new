



<style type="text/css">
  #loading {
   width: 100%;
   height: 100%;
   top: 0;
   left: 0;
   position: fixed;
   display: block;
   opacity: 0.7;
   background-color: #fff;
   z-index: 1000;
   text-align: center;
}

#loading-image {
  position: absolute;
  top: 35%;
  left: 45%;
  z-index: 100;
}
.select2-container--default .select2-selection--multiple {
    background-color: white;
    border: 1px solid #aaa;
    border-radius: 4px;
    cursor: text;
    max-height: 62px;
    overflow: auto;
}
</style>
  <!-- Main section-->
  <div id="loading" style="display: none;">
  <img id="loading-image" src="<?= base_url()?>img/ajax-loader.gif" alt="Loading..." />
</div>
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Activity and Area Mapping</div>
        <!--<form class="search-form col-sm-5 pl-0">
          <em class="icon-magnifier"></em>
          <input class="form-control" type="email" placeholder="Enter room code or number on name">
        </form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto hide-on-mob">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Admin/activityMapping">Activity and Area Mapping</a></li>
          </ol>
          <div class="humburger" id="myTopnav"> <em class="fa-2x icon-menu mr-2" id="icon"></em>
            <div class="main-menu"> <a href="Home2.html">Home</a> <a href="#">Products</a> <a href="#">XYZ</a> <a href="#">Features</a> </div>
          </div>
        </div>
      </div>
      <div class="card2 card card-default mt-4" hidden>
        <div class="card-body">
          <div class="row">
            <div class="col-6 offset-6 text-right">
              <label class="pr-3">Select Role</label>
              <div class="btn-group">
                <button class="btn btn-secondary" type="button"><em class="f18 mr-2 fas fa-plus"></em></button>
                <button class="btn dropdown-toggle btn-secondary" type="button" data-toggle="dropdown" aria-expanded="false"> <span class="sr-only">secondary</span></button>
                <div class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(67px, 33px, 0px);"> <a class="dropdown-item" href="#" id="optr1">Operator</a> <a class="dropdown-item" href="#" id="area">Area</a> <a class="dropdown-item" href="#" id="prodct1">Production</a> <a class="dropdown-item" href="#" id="quty1">Quality</a> <a class="dropdown-item" href="#" id="qtlcheck">Quality Check</a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <form id="activity-map">
      <div class="card2 card card-default mt-4">
        <div class="card-body">
        <div class="form-row mt-3">
<div class="col-lg-2 mb-3"><label for="block">Select Block <span style="color: red">*</span></label></div>
<div class="col-lg-4 mb-3"><select class="form-control" id="block" name="block" required>
  <option value="" disabled selected>Select Block</option>
  <?php
  foreach ($blocks as $block) {
  
    $ss = $block['block_code']== $blockselected?"Selected":"" ;
    
  
    ?>
    <option value="<?= $block['block_code']?>" <?=$ss?>><?= $block["block_code"]?></option>
    <?php
  }
  ?>
</select></div>
                                 
</div>

<?php
//print_r($selected["rid"]);
$this->load->model('Activitymodel');
//print_r($activitySelect);
?>
          <?php 
          $i=1;
          foreach($activity as $as){
           ?>
          <div class="row mt-3">
            <input type="text" name="activity[]" value="<?php echo $as['activity_code']."##".$as['activity_name']."##".$as['activitytype_id']."##".$as['activity_type']."##".$as['activityfunc_tocall']."##".$as['activity_icon']."##".$as['activity_remark'] ?>" hidden>
            <div class="col-3 f16"><?php echo $as['activity_type']."/".$as['activity_name']; ?></div>
            <div class="col-8">
              <div class="form-group row">
                <div class="col-md-12">
                  <select class="nselect form-control" multiple="multiple" id="area<?= $i?>" name="area<?= $i?>[]" style="width: 100%">
                  
                    <?php
                    //foreach ($select as $s) {
                    $activitySelect = $this->Adminmodel->sA($as['activity_code']);  
                    foreach ($area as $ar) {
                      //$ss = "selected";
                       $ss = "";
                       foreach ($activitySelect["ndata"] as $kv) {
                      if($kv["area_code"]==$ar['area_code']){
                      $ss = "selected";
                     }
                     }

                    ?>
                    <option value="<?php echo $ar['area_code']."##".$as['area_name'] ?>" <?= $ss?>><?php echo $ar['area_code']." / ".$ar['area_name']; ?></option>
                    <?php
                    
                    }
                     //}
                    //}
                    //}
                    
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <!--<div class="col-1.5">

                <input class="btn btn-primary" onclick="getallchked('area<?= $i?> option');" type="button" name="SelectALLbtn<?= $i?>" id="SelectALLbtn<?= $i?>" value="Select All" />
            </div>-->
            
            <div class="col-1 mt-2"><label class="switch"><input type="checkbox" data-selid="area<?= $i?>" data-att="area<?= $i?> option"  name="ALLchk<?= $i?>" id="ALLbtn<?= $i?>"><span></span></label></div>
         <!-- <div class="col-1"><em class="fa-2x mr-2 far fa-window-close pointer redd" id="clse"></em></div> -->
          </div>
                <?php 
                $i++;
                } 
                ?>

        
        </div>
      </div>
      
      <div class="card2 card card-default mt-5">
        <div class="card-body">
          <div class="row">
            <div class="col-12 offset-5">
              <button class="btn btn-square btn-primary mr-3" type="submit">Submit</button>
              <!--<button class="btn btn-square btn-danger" type="button">Cancel</button>-->
            </div>
          </div>
        </div>
      </div>

      </form>


    </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>

<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 
<script src="<?= base_url()?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?= base_url()?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?= base_url()?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?= base_url()?>vendor/i18next/i18next.js"></script> 
<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?= base_url()?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?= base_url()?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?= base_url()?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<script src="<?= base_url()?>vendor/chosen-js/chosen.jquery.js"></script><!-- SLIDER CTRL--> 
<script src="<?= base_url()?>vendor/jquery/dist/jquery.redirect.js"></script>
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?= base_url()?>js/app.js"></script>
<script src="<?php echo base_url(); ?>vendor/chosen-js/chosen.jquery.js"></script><!-- SLIDER CTRL--> 
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript" src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>
<link rel="stylesheet" type="text/css" href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css">
<script type="text/javascript">

$('input[type="checkbox"]').change(function() { 
  debugger;
  if ($(this).is(':checked')) 
  {
    var selecteid=$(this).attr('data-selid');
    $("#"+selecteid).html('');
    var block = $("#block").val();
    $.ajax({
        url: "<?= base_url()?>Admin/getareabyblockcode",
        type: 'POST',
        data: {block:block},
        success: function(res) {
        console.log(res);
        var inv = "";
        $.each(res, function (index, value) {
        //inv = inv.concat('<option selected value="as">a</option>');
        inv = inv.concat("<option selected value='"+value.area_code+"##"+value.area_name+"'>"+value.area_code+" / "+value.area_name+"</option>");
        });
        $("#"+selecteid).html(inv);
      }
    });
  }
  else
  {
    var selecteid=$(this).attr('data-selid');
    $("#"+selecteid).html('');
    var block = $("#block").val();
    $.ajax({
        url: "<?= base_url()?>Admin/getareabyblockcode",
        type: 'POST',
        data: {block:block},
        success: function(res) {
        console.log(res);
        var inv = "";
        $.each(res, function (index, value) {
         inv = inv.concat("<option value='"+value.area_code+"##"+value.area_name+"'>"+value.area_code+" / "+value.area_name+"</option>");
        });
        $("#"+selecteid).html(inv);
      }
    });
  }
});
</script>
<script type="text/javascript">
$(document).ready(function(){

$("#block").change(function(){

$.redirect("<?= base_url()?>Admin/activityMapping",
  {
    block:$("#block").val()
  }, "POST","_self");

});

$('.nselect').select2({
    placeholder: 'If you want to select all areas, please enable the button on the right ->'
});

//$(".chosen-select").chosen().val();


//$("select").trigger('chosen:updated');

  $('#activity-map').submit(function(){

  //$("select").trigger('chosen:updated');
  //$('option').prop('selected', true); 
  $('#loading').show();
    $.ajax({
      type: 'POST',
      url: '<?= base_url()?>Admin/activityareaMap',
      data: $(this).serialize() // getting filed value in serialize form
    })
    .done(function(details){ // if getting done then call.
    console.log(details);
      if(details.status==1){
        $('#loading').hide();
        $("#myb").click();
        $("#head").html(details.msgheader);
        $("#message").html(details.msg);
      }
      else{
        alert("No");    
      }
      
    })
    .fail(function(details) {
        console.log(details);
     // if fail then getting message
      // just in case posting your form failed
      alert( "Posting failed." );
    });

// to prevent refreshing the whole page page
return false;

});




});  
</script>

<script>
$(".chosen-select").chosen();
$('button').click(function(){
        $(".chosen-select").val('').trigger("chosen:updated");
});
// Show div
$("#optr1").click(function(){
	$("#optr").show();
	});
	$("#area1").click(function(){
	$("#area").show();
	});
	$("#prodct1").click(function(){
	$("#prodtn").show();
	});
	$("#quty1").click(function(){
	$("#qlty").show();
	});
	$("#qtlcheck").click(function(){
	$("#qltycheck").show();
	});
// Hide Div	
$("#clse").click(function(){
	$("#optr").hide();
	});
	$("#clse1").click(function(){
	$("#area").hide();
	});
	$("#clse2").click(function(){
	$("#prodtn").hide();
	});
	$("#clse3").click(function(){
	$("#qlty").hide();
	});
	$("#clse4").click(function(){
	$("#qltycheck").hide();
	});
</script>

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="head"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="message"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <a href="" class="btn btn-primary" id="disclose">Ok</a>
      </div>
    </div>

  </div>
</div>

</body>
</html>