<!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Add Status</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            
            
          </ol>
        </div>
      </div>

<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">

<a class="btn btn-primary btn-lg" href="<?= base_url()?>Admin/RoleStatusMappinglist" > Back To list</a>  

</div>
</div>
</div>
</div>
<form id="Rolestatus"> 
<div class="card card-default">
  <div class="card-header text-white bg-danger">Enter a status for role</div>
  <div class="card-body">
	<fieldset>                      
  <div class="col-md-12">
  <div class="form-row">
  <div class="col-lg-6 mb-3"><label >Role :</label>
      <Select class="form-control" name="role" id="role" required>
      <option value="0" selected disabled>Select Role</option>
        <?php if(count($data['role']->result())) { ?>
        <?php foreach($data['role']->result() as $row) { ?>
        <option value="<?php echo $row->id ?>"><?php echo $row->role_description ?></option>
        <?php } } ?> 
      </Select>                                
      </div> 
<div class="col-lg-4 mb-3"><label >Status :</label>
  <Select class="form-control" type="text" name="status" id="status" required>
      <option value="0" selected disabled>Select Status</option>
      <?php if(count($data['status']->result())) { ?>
        <?php foreach($data['status']->result() as $row) { if(($row->status_name != "START") && ($row->id != 1)) { ?>
      <option id="<?php echo $row->id ?>" name="<?php echo $row->status_name ?>" value="<?php echo $row->id ?>"><?php echo $row->status_name ?></option>
      <?php } } } ?> 
  </Select>                                    
  </div>
  <div class="col-lg-2 mb-3"><label>&nbsp;</label>
  <input class="form-control btn-success"  id="subbtn" type="submit" value="Submit">                                
  </div>                        
  </div>
  </div>
  </fieldset>
	</div>
</div>
</form>
  </div>
  </div>
  </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?php echo base_url(); ?>js/app.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="js/script.js"></script>-->
<script type="text/javascript">
  $(document).ready(function () {
  $('#Rolestatus').submit(function () {
    debugger;

     $.ajax({
       url: '<?= base_url()?>Admin/AddNewRolestatusMapping',
       type: 'POST',
       data: $(this).serialize(),
       success: function (res) {
        debugger;
         console.log(res);
          if(res.status==1)
          {
              alert("successfully saved.");
              location.reload(true);
              //window.location.href = "<?= base_url()?>Admin/RoleStatusMappinglist";
          }
          else
          {
            alert("Not saved.");
          }
       }
     });
     return false;
   });

});
</script>

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="head"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="message"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <a href="" class="btn btn-primary" id="disclose">Ok</a>
      </div>
    </div>

  </div>
</div>

</body>
</html>