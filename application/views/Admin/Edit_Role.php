<!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Edit Role</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            
            
          </ol>
        </div>
      </div>

<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">

<a class="btn btn-primary btn-lg" href="<?= base_url()?>Admin/Rolelist" > Back To list</a>  

</div>
</div>
</div>
</div>
	  
<div class="card card-default">
  <div class="card-header text-white bg-danger">Update Role Details</div>
  <div class="card-body">
	<fieldset>                      
  <div class="col-md-12">
  <?php if(count($data['roledtl']->result())) { 
  $res = $data['roledtl']->row_array();
  $id=$res['id'];
  $roletype = $res['role_description'];
  $roleremark = $res['role_remark'];
  } ?>
  <div class="form-row">
  <input type="hidden" id="hdd" name="hdd" value="<?php echo $id ?>">
  <div class="col-lg-4 mb-3"><label >Role Description :</label>
  <input class="form-control" type="text" name="roletype" id="roletype" value="<?php echo $roletype ?>" required>                                
  </div>
   <div class="col-lg-6 mb-3"><label >Remark :</label>
  <input class="form-control" type="text" name="remark" id="remark" value="<?php echo $roleremark ?>">                                
  </div>
  <div class="col-lg-2 mb-3"><label>&nbsp;</label>
  <input class="form-control btn-success"  id="subbtn" type="submit" value="Update">                                
  </div>                        
  </div>
  </div>
  </fieldset>
	</div>
</div>
  </div>
  </div>
  </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?php echo base_url(); ?>js/app.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="js/script.js"></script>-->
<script type="text/javascript">
  $(document).ready(function () {
  $("#subbtn").click(function(){
      debugger;
    var roletype = $("#roletype").val();
    var remark = $("#remark").val();
    var id = $("#hdd").val();

      $.ajax({
      url: "<?= base_url()?>Admin/Updaterole",
      type: 'POST',
      data: {id:id,roletype:roletype,remark:remark},
      success: function(res) {
      console.log(res);        
      if(res.status==1)
      {
          alert("successfully Updated.");
          //location.reload(true);
          window.location.href = "<?= base_url()?>Admin/Rolelist";
      }
      else
      {
        alert("Not saved.");
      }
    }
    });      
  });
});
</script>

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="head"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="message"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <a href="" class="btn btn-primary" id="disclose">Ok</a>
      </div>
    </div>

  </div>
</div>

</body>
</html>