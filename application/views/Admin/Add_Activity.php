<!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Add Activity</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            
            
          </ol>
        </div>
      </div>

<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">

<a class="btn btn-primary btn-lg" href="<?= base_url()?>Admin/ActivityList" > Back To list</a>  

</div>
</div>
</div>
</div>

<form id="activity" action="#" method="post" enctype="multipart/form-data"> 
<div class="card card-default">
  <div class="card-header text-white bg-danger">Enter Activity Details</div>
  <div class="card-body">
                    
  <div class="col-md-12">
  <div class="form-row">
      <div class="col-lg-3 mb-3"><label >Activity code :</label>
      <input class="form-control" type="text" name="actcode" id="actcode" required>                                
      </div>
      <div class="col-lg-3 mb-3"><label >Activity Name :</label>
      <input class="form-control" type="text" name="actname" id="actname" required>                                
      </div>
      <div class="col-lg-3 mb-3"><label >Type :</label>
      <Select class="form-control" type="text" name="type" id="type" required>
      <option value="0" selected disabled>Select here </option>
      <?php if(count($data['Acttype']->result())) { ?>
        <?php foreach($data['Acttype']->result() as $row) { ?>
      <option id="<?php echo $row->id ?>" name="<?php echo $row->activitytype_name ?>" value="<?php echo $row->id ?>"><?php echo $row->activitytype_name ?></option>
      <?php } } ?> 
      </Select>
      <input type="hidden" id="typename" name="typename" value="0" />
      </div>
      <div class="col-lg-3 mb-3"><label >Function to call :</label>
      <input class="form-control" type="text" name="functocall" id="functocall" required>                                
      </div>
  </div>
  <div class="form-row">
      <div class="col-lg-6 mb-3"><label >Icon :</label>
      <input class="form-control" type="file" name="icon" id="icon" required accept="image/x-png,image/gif,image/jpeg,image/jpg">                                
      </div>                     
      <div class="col-lg-6 mb-3"><label>Remark :</label>
      <input class="form-control"  id="rmk" name="rmk" type="text" >                                
      </div>                      
  </div>
  <div class="form-row">
    <div class="col-lg-12"> 
      <center><input class="btn btn-success"  id="subbtn" type="submit" value="Submit"></center> 
    </div>              
  </div>

  </div>

	</div>
</div>
</form>
  </div>
  </div>
  </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 
<script type="text/javascript" src="<?php echo base_url().'js/jquery-3.2.1.js'?>"></script>
<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?php echo base_url(); ?>js/app.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="js/script.js"></script>-->
<script type="text/javascript">
  $(document).ready(function () {

   $("#actcode").change(function(){
    debugger;
    var tblname = "mst_activity";
    var col = "activity_code";
    var code = $(this).val();
    $.ajax({
        url: '<?= base_url()?>Admin/chkcode',
        type: 'POST',
        data: {tblname:tblname,col:col,code:code},
        success: function (res) {
          //console.log(res);
          if(res.status==1){
           // $("#validationnow21").attr("disabled",true);
           // $("#validationnow22").attr("disabled",false);
            alert("Record Already Exists at this code.");
            $("#actcode").val("");
            $("#actcode").focus();
          }
        }
      });
  });

  $('#activity').submit(function(e){
        e.preventDefault(); 
    debugger;
    var acttypename = $('#type option:selected').text();
    $("#typename").val(acttypename);

             $.ajax({
                 url:'<?php echo base_url();?>Admin/add_new_acat',
                 type:"post",
                 data:new FormData(this), //this is formData
                 processData:false,
                 contentType:false,
                 cache:false,
                 async:false,
                  success: function(result){
                    console.log(result);
                    /*if(result.status==1)
                    {
                        $("#Msgmyb").click();
                        $("#Msghead").html(result.msgheader);
                        $("#Msgmessage").html(result.msg);
                    }
                    else if(result.status==0)
                    {
                        $("#Msgmyb").click();
                        $("#Msghead").html(result.msgheader);
                        $("#Msgmessage").html(result.msg);
                    }
                    else if(result.status==2)
                    {
                        $("#Msgmyb").click();
                        $("#Msghead").html(result.msgheader);
                        $("#Msgmessage").html(result.msg);
                    }*/
                    alert(result);
                    location.reload(true);
                    //window.location.href = "<?= base_url()?>Admin/ActivityList";
                    //alert(JSON.parse(data));
                      //alert("Upload Image Successful.");
               }
             });
    });
});
function disabledpopup()
{
$("#Msgmyb").click();
}
</script>
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="head"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="message"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <a href="" class="btn btn-primary" id="disclose" onclick="disabledpopup();">Ok</a>
      </div>
    </div>

  </div>
</div>

</body>
</html>