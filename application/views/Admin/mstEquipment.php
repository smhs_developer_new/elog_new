  <!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Equipment Details</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item active"><a href="<?= base_url()?>Admin/mstEquipmentview">Equipment</a></li>
            
          </ol>
        </div>
      </div>
	  


<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">

<a class="btn btn-primary btn-lg" href="<?= base_url()?>admin/mstEquipmentView" > Back To list</a>  
&nbsp;&nbsp;&nbsp;
<a class="btn btn-primary btn-lg" href="<?= base_url()?>admin/batchEquipment"> Excel Upload</a>

</div>
</div>
</div>
</div>

<form id="equipment" enctype='multipart/form-data' novalidate>
    <div class="card card-default">
        <div class="card-body">

	  <fieldset>            
      <div class="col-auto">
        <div class="form-row">   
          <div class="col-lg-3 mb-3"><label for="aqty">Equipment Code <span style="color: red">*</span></label><input class="form-control" id="ecode" type="text" placeholder="Equipment Code" name="ecode" required >
          </div>
          <div class="col-lg-3 mb-3"><label for="edso">Equipment Name <span style="color: red">*</span></label><input class="form-control" id="ename" type="text" placeholder="Equipment Name" name="ename" required >
          </div>
          <div class="col-lg-3 mb-3"><label for="edso">Equipment Type <span style="color: red">*</span></label>
          <select id="etype" name="etype" onchange="divchng();" class="form-control" required>
          <option value="" selected disabled>Select Equipment Type </option>
					<option value="Fixed">Fixed</option>
					<option value="Portable">Portable</option>
          <option value="Vaccum">Vaccum Cleanner</option>
          <option value="Booth">Dispensing Booth</option>
          </select>
          </div>
          <div class="col-lg-3 mb-3"><label for="validationServer01">Block<span style="color: red">* </span></label>
          <select id="blockcode" name="blockcode" onchange="getarea();" class="form-control" required>
          <option value="0" selected disabled>Select Block</option>
          <?php if($data['block']->num_rows()>0) { ?>
          <?php foreach($data['block']->result() as $row) {  if($row->block_code !="Block-000") {?>
          <option value="<?php echo $row->block_code ?>"><?php echo $row->block_code." | ".$row->block_name ?> </option>
          <?php } } } ?> 
          </select>
          </div>
        </div>
        <div class="form-row" id="fixeq" style="display: none;">
          <div class="col-lg-6 mb-3"><label for="validationServer01">Area<span style="color: red">* </span></label>
          <select id="areacode" onchange="getroom();" name="areacode" class="form-control" required>
                
          </select>
          </div>
          <div class="col-lg-6 mb-3"><label for="validationServer01">Room<span style="color: red">* </span></label>
          <select id="rcode" name="rcode" class="form-control" required>
                
          </select>
          </div>
          </div>

          <div class="form-row">
					<div class="col-lg-6 mb-3"><label for="validationServer01">Drain Point Code </label>
          <!--<select id="scode" name="scode" class="form-control" required>
              <option value="" selected disabled>Select Drain Point </option>
              <?php if($data['sop']->num_rows()>0) { ?>
              <?php foreach($data['sop']->result() as $row) { ?>
              <option value="<?php echo $row->sop_code ?>"><?php echo $row->sop_code ?> | <?php echo $row->sop_name ?> </option>
              <?php } } ?> 
              </select>  autocomplete -->
          <input class="form-control" type="text" placeholder="Drain Point Code" name="dpcode"  id="dpcode">
          </div>
          <div class="col-lg-6 mb-3"><label for="validationServer01">Sop Code <span style="color: red">* </span></label>
          <!--<select id="scode" name="scode" class="form-control" required>
              <option value="" selected disabled>Select Sop Code </option>
              <?php if($data['sop']->num_rows()>0) { ?>
              <?php foreach($data['sop']->result() as $row) { ?>
              <option value="<?php echo $row->sop_code ?>"><?php echo $row->sop_code ?> | <?php echo $row->sop_name ?> </option>
              <?php } } ?> 
              </select>-->
          <input class="form-control" type="text" placeholder="Sop Code" name="spcode" id="spcode" required>                      
          </div>                    
          </div>

                        <div class="form-row">
                         <div class="col-lg-6 mb-3"><label for="validationServer01">Remarks </label><textarea class="form-control" aria-label="With textarea" name="eremarks" placeholder="Enter Remarks" ></textarea>
                         </div>
                         <div class="col-lg-6 mb-3"><label for="edso">Equipment Icon <span style="color: red"> (less than 1MB & png/jpg/gif)</span></label><br><input class="" id="eicon" type="file" placeholder="Equipment Type" name="eicon" accept="image/x-png,image/gif,image/jpeg">
                         </div>
                        </div>

                     </fieldset>
					 </div>
					 </div>
					  <div class="card card-default">
                     
                     <div class="card-body" >
                        
                          
<div class="row disable-button-color">
      <div class="col-sm-12 text-center" id="sbtn"><button id="sbbtn" class="btn btn-success btn-lg" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Submit &nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      </div>

                        
                     </div>
                  </div><!-- END card-->
					 
</form>

        </div>
      </div>
      
    </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?= base_url()?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?= base_url()?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?= base_url()?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?= base_url()?>vendor/i18next/i18next.js"></script> 
<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?= base_url()?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?= base_url()?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?= base_url()?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- Modal Large-->
<div class="modal fade" id="myModalLarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabelLarge" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelLarge">Add portable euipments in your euipment list</h4>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>		
      </div>
	  
      <div class="modal-body">
		<ul class="task-list list-unstyled">
        <li class="bg-white pt10 pb10 pl15"> 
        <span class="col-sm-3 pl-0"><b>Area :</b> </span><span class="col-sm-3 pl-0" id="area">Granulation</span>
        <select class="custom-select custom-select-sm col-sm-6" id="areaddl" onchange="araechange();">
        <option selected="">Select Another Area</option>
        <option value="Blending">Blending</option>
        <option value="Coating">Coating</option>
        <option value="Packaging">Packaging</option>
        </select>
                              <button class="btn btn-success col-sm-2 mr-2" type="button" value="Show all" onclick="showpequip();" name="showall" id="showall" style="float:right;">Show All</button>
							  <button class="btn btn-success col-sm-2 mr-2" style="display:none;float:right;" type="button" value="Show less" onclick="hidepequip();" name="showless" id="showless">Show less</button>
                              
        </li>
        </ul>
        <section class="page-tasks">
		
          <ul class="task-list list-unstyled" id="peqlist">
            <li id="pli1"> <span class="view" id="p1">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 1" id="check1" name="check1">
                  <label>Portable euipment 1</label>
                </div>
                <!-- <div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli2"> <span class="view" id="p2">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 2" id="check2" name="check2">
                  <label>Portable euipment 2</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli3"> <span class="view" id="p3">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 3" id="check3" name="check3">
                  <label>Portable euipment 3</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli4" style="display:none;"> <span class="view" id="p4">
              <div class="row">
                <div class="col-sm-12 pl0" onClick="Fstep">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 4" id="check4" name="check4">
                  <label>Portable euipment 4</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli5" style="display:none;"> <span class="view" id="p5">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 5" id="check5" name="check5">
                  <label>Portable euipment 5</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli6" style="display:none;"> <span class="view" id="p6">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 6" id="check6" name="check6">
                  <label>Portable euipment 6</label>
                </div>
                <!-- <div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
          </ul>
        </section>
      </div>
	  
    </div>
  </div>
</div>
<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 

<script src="<?= base_url()?>js/app.js"></script> 
<script type="text/javascript" src="js/script.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>

<script type="text/javascript">
$(document).ready(function(){

$("#dpcode").keyup(function() {
var str = $(this).val();
if(str!="" && str.length>=1){
  
$.ajax({
    url: "<?= base_url()?>Admin/getDrainpointlist",
    type: 'POST',
    data: {str:str},
    success: function(res) {
        console.log(res);
        availableTags = res; 
  
  $("#dpcode").autocomplete({
    source: availableTags
  });
    }
}); 
}
});

$("#spcode").keyup(function() {
var str = $(this).val();
if(str!="" && str.length>=1){
  
$.ajax({
    url: "<?= base_url()?>Admin/getSoplist",
    type: 'POST',
    data: {str:str},
    success: function(res) {
        console.log(res);
        availableTags = res; 
  
  $("#spcode").autocomplete({
    source: availableTags
  });
    }
}); 
}
});

 $("#ecode").change(function(){
    var epcode = $(this).val();
    $.ajax({
        url: '<?= base_url()?>Admin/checkEquipmentcode',
        type: 'POST',
        data: {ecode:epcode},
        success: function (res) {
          //console.log(res);
          if(res.status==0){
           // $("#validationnow21").attr("disabled",true);
           // $("#validationnow22").attr("disabled",false);
            alert("Equipment Code Already Exists");
            $("#ecode").val("");
          }
        }
      });
  });


   $('#equipment').submit(function () {
var form_data = new FormData(this);
if($("#eicon").val()!=""){
var file_data = $('#eicon').prop('files')[0];
//console.log(file_data);
var filename = file_data.name;
var extension = filename.substring(filename.lastIndexOf('.') + 1);
var size = file_data.size/1048576;
size = size.toFixed(2);
//console.log(filename+" - "+size.toFixed(2)+" MB - "+extension);

if(size<=1 && (extension == "png" || extension == "jpg" || extension == "gif" || extension == "jpeg")){




$.ajax({
                 url: '<?= base_url()?>Admin/mstEquipmentSubmit',
                 type: "POST",
                 data: form_data,
                 
                 processData: false,
                 contentType: false,
                 cache: false,
         
                 success: function(data) {  
                     //alert("hi : <?= $urid?> : "+data.results+" - "+data.da);             
                 //console.log(data);
                 //alert("Image Uploaded");
                 if(data.status==1){
                  alert("Equipment Added successfully");
                  window.location.href = "<?= base_url()?>Admin/mstEquipment";
                 }
                 }
         
             });

}
else{
  alert("You cann't upload this file.");
  $("#eicon").val(null);
}


}
else{
$.ajax({
                 url: '<?= base_url()?>Admin/mstEquipmentSubmit',
                 type: "POST",
                 data: form_data,
                 
                 processData: false,
                 contentType: false,
                 cache: false,
         
                 success: function(data) {  
                     //alert("hi : <?= $urid?> : "+data.results+" - "+data.da);             
                 //console.log(data);
                 //alert("Image Uploaded");
                 if(data.status==1){
                  alert("Equipment Added successfully");
                  //window.location.href = "<?= base_url()?>Admin/mstEquipmentview";
				  window.location.reload();
                 }
                 }
         
             });
}
 return false;
    });

});  
</script>
<script type="text/javascript">
function getarea()
{
  debugger;
  $("#areacode").html('');
  var block = $("#blockcode").val();
  $.ajax({
    url: "<?= base_url()?>Admin/getareabyblockcode",
    type: 'POST',
    data: {block:block},
    success: function(res) {
      debugger;
        var inv = "<option value='' selected disabled>Select Area</option>";
        $.each(res, function (index, value) {
        inv = inv.concat("<option value='"+value.area_code+"'>"+value.area_code+" | "+value.area_name+"</option>");
        });
        $("#areacode").html(inv);
    }
  });

}
function getroom()
{
  debugger;
  var area = $("#areacode").val();
  $("#rcode").html('');
  $.ajax({
    url: "<?= base_url()?>Production/getRoomnow",
    type: 'POST',
    data: {area:area},
    success: function(res) {
        //console.log(res);
        var inv = "<option value='' selected disabled>Select Room</option>";
        $.each(res, function (index, value) {
        inv = inv.concat("<option value='"+value.room_code+"'>"+value.room_code+" | "+value.room_name+"</option>");
        });
        $("#rcode").html(inv);

    }
});

}

function divchng()
{
  debugger;

  $("#blockcode").val('0');

  var etype = $("#etype").val();
  $("#areacode").html('');
  $("#rcode").html('');
  if(etype=="Fixed" || etype=="Booth")
  {
      $("#fixeq").show();
  }
  else
  {
      $("#fixeq").hide(); 
  }
}
</script>


<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="head"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="message"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <a href="" class="btn btn-primary" id="disclose">Ok</a>
      </div>
    </div>

  </div>
</div>

</body>
</html>