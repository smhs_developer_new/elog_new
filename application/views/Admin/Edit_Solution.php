<!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Update Solution</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            
            
          </ol>
        </div>
      </div>

<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">

<a class="btn btn-primary btn-lg" href="<?= base_url()?>Admin/SolutionList" > Back To list</a>  

</div>
</div>
</div>
</div>

<form id="solution">  
<div class="card card-default">
  <div class="card-header text-white bg-danger">Edit Solution Details</div>
  <div class="card-body">
  <?php if(count($data['Soldtl']->result())) { 
  $res = $data['Soldtl']->row_array(); ?>                   
  <div class="col-md-12">
  <div class="form-row">
    <input type="hidden" id="hdd" name="hdd" value="<?php echo $res['id'] ?>">
      <div class="col-lg-3 mb-3"><label >Solution code :</label>
      <input class="form-control" type="text" name="solcode" id="solcode" value="<?php echo $res['solution_code'] ?>" required>                                
      </div>
      <div class="col-lg-5 mb-3"><label >Solution Name :</label>
      <input class="form-control" type="text" name="solname" id="solname" value="<?php echo $res['solution_name'] ?>" required>                                
      </div>
      <div class="col-lg-4 mb-3"><label>Block :</label>
      <Select class="form-control" name="block" id="block" required>
      <option value="0">Select Here</option>
        <?php if(count($data['block']->result())) { ?>
        <?php foreach($data['block']->result() as $row) {
          $chk = $row->block_code == $res['Block_Code'] ? "Selected" : "";
         ?>
        <option value="<?php echo $row->block_code ?>" <?= $chk ?>><?php echo $row->block_code ?></option>
        <?php } } ?> 
      </Select>                               
      </div>                      
  </div>
    
  <div class="form-row">
      <div class="col-lg-3 mb-3"><label >Short name. :</label>
      <input class="form-control" type="text" name="shortname" id="shortname" value="<?php echo $res['Short_Name'] ?>" required>                                
      </div>  
      <div class="col-lg-2 mb-3"><label >Solution Quantity :</label>
      <input class="form-control" type="text" name="solquantity" id="solquantity" value="<?php echo $res['sol_qty'] ?>" required>                                
      </div>
      <div class="col-lg-2 mb-3"><label>Water Quantity :</label>
      <input class="form-control"  id="waterquantity" name="waterquantity" type="text" value="<?php echo $res['water_qty'] ?>" required>                              
      </div>  
      <div class="col-lg-2 mb-3"><label >Total Quantity :</label>
      <input class="form-control" type="text" name="totquantity" id="totquantity" value="<?php echo $res['tot_qty'] ?>" required>                                
      </div>
      <div class="col-lg-3 mb-3"><label >Unit of Measure :</label>
      <Select class="form-control" type="text" name="uom" id="uom">
        <?php if($res['UoM']=="ml") { ?>
        <option value="ml">ml</option>
        <?php } else if($res['UoM']=="l") { ?>          
        <option value="l">l</option>
      <?php } else { ?>
        <option value="ml">ml</option>
        <option value="l">l</option>
      <?php } ?>
      </Select>                                
      </div>                      
  </div>
    <div class="form-row">
    <div class="col-lg-12"> 
      <label>Remark :</label>
      <input class="form-control"  id="Precaution" name="Precaution" type="text" value="<?php echo $res['Precautions'] ?>">                                
    </div>              
  </div>
  <div class="form-row">
    <div class="col-lg-12"> 
      <center><input class="btn btn-success"  id="subbtn" type="submit" value="Update"></center> 
    </div>              
  </div>

  </div>
  <?php } else { ?>
  <div class="col-md-12">
  <div class="form-row">
      <div class="col-lg-3 mb-3"><center><label >There is nothing to update</label></center></div>
  </div>
  </div>
  <?php } ?>
	</div>
</div>
</form>
  </div>
  </div>
  </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?php echo base_url(); ?>js/app.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="js/script.js"></script>-->
<script type="text/javascript">
  $(document).ready(function () {

  $("#solcode").change(function(){
    debugger;
    var tblname = "mst_solution";
    var col = "solution_code";
    var code = $(this).val();
    $.ajax({
        url: '<?= base_url()?>Admin/chkcode',
        type: 'POST',
        data: {tblname:tblname,col:col,code:code},
        success: function (res) {
          //console.log(res);
          if(res.status==1){
           // $("#validationnow21").attr("disabled",true);
           // $("#validationnow22").attr("disabled",false);
            alert("Record Already Exists at this code.");
            $("#solcode").val("");
            $("#solcode").focus();
          }
        }
      });
  });

  $('#solution').submit(function () {
    debugger;
     $.ajax({
       url: '<?= base_url()?>Admin/UpdateSolution',
       type: 'POST',
       data: $(this).serialize(),
       success: function (res) {
        debugger;
         console.log(res);
          if(res.status==1)
          {
              alert("successfully Updated.");
              //location.reload(true);
              window.location.href = "<?= base_url()?>Admin/SolutionList";
          }
          else
          {
            alert("Not saved.");
          }
       }
     });
     return false;
   });

});
</script>
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="head"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="message"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <a href="" class="btn btn-primary" id="disclose">Ok</a>
      </div>
    </div>

  </div>
</div>

</body>
</html>