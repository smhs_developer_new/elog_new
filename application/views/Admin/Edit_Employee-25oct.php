<style type="text/css">
  .select2-container--default .select2-selection--multiple {
    background-color: white !important;
    border: 1px solid #dde6e9 !important;
    border-radius: 4px !important;
    cursor: text !important;
}
</style>
<!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Update Employee</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            
            
          </ol>
        </div>
      </div>

<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">

<a class="btn btn-primary btn-lg" href="<?= base_url()?>Admin/EmployeeList" > Back To list</a>  

</div>
</div>
</div>
</div>
<form id="employee">  
<div class="card card-default">
  <div class="card-header text-white bg-danger">Edit Employee Details</div>
  <div class="card-body">
  <?php if(count($data['empdtl']->result())) { 
  $res = $data['empdtl']->row_array(); ?>                     
  <div class="col-md-12">
  <div class="form-row">
    <input type="hidden" id="hdd" name="hdd" value="<?php echo $res['id'] ?>">
      <div class="col-lg-4 mb-3"><label >Emp. code :</label>
      <input class="form-control" type="text" name="code" id="code" value="<?php echo $res['emp_code'] ?>" required>
      </div>
      <div class="col-lg-4 mb-3"><label >Emp. Name :</label>
      <input class="form-control" type="text" name="empname" id="empname" value="<?php echo $res['emp_name'] ?>" required>
      </div>                              
     <div class="col-lg-4 mb-3"><label>Email-Id :</label>
      <input class="form-control"  id="email2" name="email2" type="email" value="<?php echo $res['emp_name'] ?>" >                                
      </div>                      
  </div>

  <div class="form-row">
      <div class="col-lg-12 mb-3"><label>Address :</label>
      <input class="form-control"  id="add" name="add" type="text" value="<?php echo $res['emp_address'] ?>" >                                
      </div>
  </div>
    
  <div class="form-row">
      <div class="col-lg-3 mb-3"><label >Contact no. :</label>
      <input class="form-control cc" type="text" maxlength="10" minlength="10" pattern="(.){10,10}" name="cont" id="cont" value="<?php echo $res['emp_contact'] ?>" >                                
      </div>  
      <div class="col-lg-3 mb-3"><label >Username :</label>
      <input class="form-control" type="text" name="email" id="email" value="<?php echo $res['emp_email'] ?>" required>                                
      </div>
      <div class="col-lg-3 mb-3"><label>Password :</label>      
      <div class="input-group">
      <input class="form-control"  id="pwd" name="pwd" type="password" value="<?php echo $res['emp_password'] ?>" readonly required>
      <div class="input-group-prepend"><span class="input-group-text" id="inputGroupPrepend3"><em class="fa fa-edit fa-fw"></em></span></div></div>
      </div>  
      <div class="col-lg-3 mb-3"><label >Designation :</label>
      <input class="form-control" type="text" name="desg" id="desg" value="<?php echo $res['designation_code'] ?>" required>                                
      </div>                     
  </div>

  <div class="form-row">
      <div class="col-lg-6 mb-3"><label >Role :</label>
      <Select class="form-control" name="role" id="role" required>
      <option value="0" selected disabled>Select Here</option>
        <?php if(count($data['role']->result())) { ?>
        <?php foreach($data['role']->result() as $row) { 
        $chk = $row->id == $res['role_id'] ? "selected":"";
        ?>
        <option value="<?php echo $row->id ?>" <?= $chk ?> ><?php echo $row->role_description ?></option>
        <?php } } ?> 
      </Select>                                
      </div>    
      <div class="col-lg-6 mb-3"><label>Block :</label>
      <Select class="form-control" onchange="getarea();" name="block" id="block" required>
      <option value="0" selected disabled>Select Here</option>
        <?php if(count($data['block']->result())) { ?>
        <?php foreach($data['block']->result() as $row) { 
        $chk = $row->block_code == $res['block_code'] ? "selected":"";
        ?>
        <option value="<?php echo $row->block_code ?>" <?= $chk ?> ><?php echo $row->block_code ?></option>
        <?php } } ?> 
      </Select>                             
      </div>      
  </div>
  <div class="form-row">
    <div class="col-lg-12 mb-3"><label>Sub block :</label>
    <Select class="nselect form-control" multiple="multiple" onchange="getarea();" name="subblock[]" id="subblock" required>
      <?php $myArray = explode(',', $res['Sub_block_code']);
       if(count($data['subblock']->result())) { ?>
        <?php foreach($data['subblock']->result() as $row) { 
              $chk="";
                foreach($myArray as $ar) {
                    $chk = $row->department_code == $ar ? "selected":""; } ?>
        <option value="<?php echo $row->department_code ?>" <?= $chk ?> ><?php echo $row->department_code ?>|<?php echo $row->department_name ?></option>
        <?php } } ?> 
      </Select>                              
    </div>
  </div>
    <div class="form-row">
      <div class="col-lg-12 mb-3"><label>Area :</label>
      <Select class="nselect form-control" multiple="multiple" name="area[]" id="area" required>

      <?php $myArray = explode(',', $res['area_code']);
       if(count($data['area']->result())) { ?>
        <?php foreach($data['area']->result() as $row) { 
          $chk="";
                foreach($myArray as $ar) {
                    $chk = $row->area_code == $ar ? "selected":""; } ?>
        <option value="<?php echo $row->area_code ?>" <?= $chk ?> ><?php echo $row->area_code ?>|<?php echo $row->area_name ?></option>
        <?php } } ?> 
      </Select>                             
      </div> 
  </div>
  <div class="form-row">
    <div class="col-lg-12 mb-3"><label>Remark :</label>
      <input class="form-control"  id="remark" name="remark" type="text" value="<?php echo $res['designation_code'] ?>">                               
      </div>
  </div>
  <div class="form-row">
    <div class="col-lg-12"> 
      <center><input class="btn btn-success"  id="subbtn" type="submit" value="Submit"></center> 
    </div>              
  </div>

  </div>
<?php } else { ?>
  <div class="col-md-12">
  <div class="form-row">
      <div class="col-lg-3 mb-3"><center><label >There is nothing to update</label></center></div>
  </div>
  </div>
<?php } ?>
	</div>
</div>
</form>
  </div>
  </div>
  </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?php echo base_url(); ?>js/app.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="js/script.js"></script>-->

<script src="<?php echo base_url(); ?>vendor/chosen-js/chosen.jquery.js"></script><!-- SLIDER CTRL--> 

<script src="<?php echo base_url(); ?>js/1.11.2-jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/select2.js"></script>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>js/select2.min.css">

<script type="text/javascript">
  $(document).ready(function () {

  $('#subblock').select2({
    placeholder: 'Select Sub block'
});

  $('#area').select2({
    placeholder: 'Select area'
});

  $('.cc').bind('keyup paste', function(){
        this.value = this.value.replace(/[^0-9]/g, '');
  });

    $("#code").change(function(){
    debugger;
    var tblname = "mst_employee";
    var col = "emp_code";
    var code = $(this).val();
    $.ajax({
        url: '<?= base_url()?>Admin/chkcode',
        type: 'POST',
        data: {tblname:tblname,col:col,code:code},
        success: function (res) {
          //console.log(res);
          if(res.status==1){
           // $("#validationnow21").attr("disabled",true);
           // $("#validationnow22").attr("disabled",false);
            alert("Record Already Exists at this code.");
            $("#code").val("");
            $("#code").focus();
          }
        }
      });
  });

  $("#email").change(function(){
    debugger;
    var tblname = "mst_employee";
    var col = "emp_email";
    var code = $(this).val();
    $.ajax({
        url: '<?= base_url()?>Admin/chkcode',
        type: 'POST',
        data: {tblname:tblname,col:col,code:code},
        success: function (res) {
          //console.log(res);
          if(res.status==1){
           // $("#validationnow21").attr("disabled",true);
           // $("#validationnow22").attr("disabled",false);
            alert("Record already exists at this email_id.");
            $("#email").val("");
            $("#email").focus();
          }
        }
      });
  });

  $('#employee').submit(function () {
    debugger;
        var newPassword = document.getElementById('pwd').value;
        var minNumberofChars = 8;
        var maxNumberofChars = 16;
        var regularExpression  = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,16}/;
        if(newPassword.length < minNumberofChars || newPassword.length > maxNumberofChars){
            alert("Password should be atleast 8 character and max 16 character");
            return false;
        }
        if(!regularExpression.test(newPassword)) {
            alert("Password should contain:-\nAt least one upper case letter: (A – Z) \nAt least one lower case letter: (a - z) \nAt least one number: (0 - 9) \nAt least one Special Characters");
          return false;
        }
        else
        {
            //var value = jQuery("#pwd").val();
            //pwd2 = SHA256(value);
            //jQuery("#pwd").val(pwd2);
            $.ajax({
            url: '<?= base_url()?>Admin/UpdateEmployee',
            type: 'POST',
            data: $(this).serialize(),
            success: function (res) {
              if(res.status==1)
              {
                  alert("successfully Updated.");
                  //location.reload(true);
                  window.location.href = "<?= base_url()?>Admin/EmployeeList";
              }
              else
              {
                  alert("Not saved.");
              }
            }
          });
        }
     return false;
   });
});
</script>
<script type="text/javascript">
function getarea()
{
  debugger;
  $("#area").html('');
  var selected=[];
  $('#subblock :selected').each(function(i, sel){ 
    selected.push($(sel).val()); 
  });

  $.ajax({
    url: "<?= base_url()?>Admin/getarea",
    type: 'POST',
    data: {subblock:selected},
    success: function(res) {
      debugger;
        var inv = "";
        $.each(res, function (index, value) {
        inv = inv.concat("<option selected value='"+value.area_code+"'>"+value.area_code+" | "+value.area_name+"</option>");
        });
        $("#area").html(inv);
    }
  });

}

function getsubblock()
{
  debugger;
  var block = $("#block").val();
  $.ajax({
    url: "<?= base_url()?>Admin/getdept",
    type: 'POST',
    data: {block:block},
    success: function(res) {
        //console.log(res);
        var inv = "";
        $.each(res, function (index, value) {
        inv = inv.concat("<option selected value='"+value.department_code+"'>"+value.department_code+" | "+value.department_name+"</option>");
        });
        $("#subblock").html(inv);
         getarea();
    }
});
 
}
</script>
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="head"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="message"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <a href="" class="btn btn-primary" id="disclose">Ok</a>
      </div>
    </div>

  </div>
</div>

</body>
<script>
    /**
*  Secure Hash Algorithm (SHA256)
*  http://www.webtoolkit.info/
*  Original code by Angel Marin, Paul Johnston
**/

function SHA256(s){
  var chrsz   = 8;
  var hexcase = 0;

 function safe_add (x, y) {
   var lsw = (x & 0xFFFF) + (y & 0xFFFF);
   var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
   return (msw << 16) | (lsw & 0xFFFF);
 }

 function S (X, n) { return ( X >>> n ) | (X << (32 - n)); }
 function R (X, n) { return ( X >>> n ); }
 function Ch(x, y, z) { return ((x & y) ^ ((~x) & z)); }
 function Maj(x, y, z) { return ((x & y) ^ (x & z) ^ (y & z)); }
 function Sigma0256(x) { return (S(x, 2) ^ S(x, 13) ^ S(x, 22)); }
 function Sigma1256(x) { return (S(x, 6) ^ S(x, 11) ^ S(x, 25)); }
 function Gamma0256(x) { return (S(x, 7) ^ S(x, 18) ^ R(x, 3)); }
 function Gamma1256(x) { return (S(x, 17) ^ S(x, 19) ^ R(x, 10)); }

 function core_sha256 (m, l) {
   var K = new Array(0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5, 0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5, 0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3, 0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174, 0xE49B69C1, 0xEFBE4786, 0xFC19DC6, 0x240CA1CC, 0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA, 0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7, 0xC6E00BF3, 0xD5A79147, 0x6CA6351, 0x14292967, 0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13, 0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85, 0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3, 0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070, 0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5, 0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3, 0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208, 0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2);
   var HASH = new Array(0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A, 0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19);
   var W = new Array(64);
   var a, b, c, d, e, f, g, h, i, j;
   var T1, T2;

   m[l >> 5] |= 0x80 << (24 - l % 32);
   m[((l + 64 >> 9) << 4) + 15] = l;

   for ( var i = 0; i<m.length; i+=16 ) {
     a = HASH[0];
     b = HASH[1];
     c = HASH[2];
     d = HASH[3];
     e = HASH[4];
     f = HASH[5];
     g = HASH[6];
     h = HASH[7];

     for ( var j = 0; j<64; j++) {
       if (j < 16) W[j] = m[j + i];
       else W[j] = safe_add(safe_add(safe_add(Gamma1256(W[j - 2]), W[j - 7]), Gamma0256(W[j - 15])), W[j - 16]);

       T1 = safe_add(safe_add(safe_add(safe_add(h, Sigma1256(e)), Ch(e, f, g)), K[j]), W[j]);
       T2 = safe_add(Sigma0256(a), Maj(a, b, c));

       h = g;
       g = f;
       f = e;
       e = safe_add(d, T1);
       d = c;
       c = b;
       b = a;
       a = safe_add(T1, T2);
     }

     HASH[0] = safe_add(a, HASH[0]);
     HASH[1] = safe_add(b, HASH[1]);
     HASH[2] = safe_add(c, HASH[2]);
     HASH[3] = safe_add(d, HASH[3]);
     HASH[4] = safe_add(e, HASH[4]);
     HASH[5] = safe_add(f, HASH[5]);
     HASH[6] = safe_add(g, HASH[6]);
     HASH[7] = safe_add(h, HASH[7]);
   }
   return HASH;
 }

 function str2binb (str) {
   var bin = Array();
   var mask = (1 << chrsz) - 1;
   for(var i = 0; i < str.length * chrsz; i += chrsz) {
     bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (24 - i%32);
   }
   return bin;
 }

 function Utf8Encode(string) {
   string = string.replace(/\r\n/g,"\n");
   var utftext = "";

   for (var n = 0; n < string.length; n++) {

     var c = string.charCodeAt(n);

     if (c < 128) {
       utftext += String.fromCharCode(c);
     }
     else if((c > 127) && (c < 2048)) {
       utftext += String.fromCharCode((c >> 6) | 192);
       utftext += String.fromCharCode((c & 63) | 128);
     }
     else {
       utftext += String.fromCharCode((c >> 12) | 224);
       utftext += String.fromCharCode(((c >> 6) & 63) | 128);
       utftext += String.fromCharCode((c & 63) | 128);
     }

   }

   return utftext;
 }

 function binb2hex (binarray) {
   var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
   var str = "";
   for(var i = 0; i < binarray.length * 4; i++) {
     str += hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8+4)) & 0xF) +
     hex_tab.charAt((binarray[i>>2] >> ((3 - i%4)*8  )) & 0xF);
   }
   return str;
 }

 s = Utf8Encode(s);
 return binb2hex(core_sha256(str2binb(s), s.length * chrsz));
}
</script>
<script>
    function Encrypt(str) {
    if (!str) str = "";
    str = (str == "undefined" || str == "null") ? "" : str;
    try {
        var key = 146;
        var pos = 0;
        ostr = '';
        while (pos < str.length) {
            ostr = ostr + String.fromCharCode(str.charCodeAt(pos) ^ key);
            pos += 1;
        }

        return ostr;
    } catch (ex) {
        return '';
    }
}

function Decrypt(str) {
    if (!str) str = "";
    str = (str == "undefined" || str == "null") ? "" : str;
    try {
        var key = 146;
        var pos = 0;
        ostr = '';
        while (pos < str.length) {
            ostr = ostr + String.fromCharCode(key ^ str.charCodeAt(pos));
            pos += 1;
        }

        return ostr;
    } catch (ex) {
        return '';
    }
}
</script>
<script type="text/javascript">
  <!--

  var keyStr = "ABCDEFGHIJKLMNOP" +
               "QRSTUVWXYZabcdef" +
               "ghijklmnopqrstuv" +
               "wxyz0123456789+/" +
               "=";

  function encode64(input) {
     input = escape(input);
     var output = "";
     var chr1, chr2, chr3 = "";
     var enc1, enc2, enc3, enc4 = "";
     var i = 0;

     do {
        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
           enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
           enc4 = 64;
        }

        output = output +
           keyStr.charAt(enc1) +
           keyStr.charAt(enc2) +
           keyStr.charAt(enc3) +
           keyStr.charAt(enc4);
        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";
     } while (i < input.length);

     return output;
  }

  function decode64(input) {
     var output = "";
     var chr1, chr2, chr3 = "";
     var enc1, enc2, enc3, enc4 = "";
     var i = 0;

     // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
     var base64test = /[^A-Za-z0-9\+\/\=]/g;
     if (base64test.exec(input)) {
        alert("There were invalid base64 characters in the input text.\n" +
              "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
              "Expect errors in decoding.");
     }
     input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

     do {
        enc1 = keyStr.indexOf(input.charAt(i++));
        enc2 = keyStr.indexOf(input.charAt(i++));
        enc3 = keyStr.indexOf(input.charAt(i++));
        enc4 = keyStr.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
           output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
           output = output + String.fromCharCode(chr3);
        }

        chr1 = chr2 = chr3 = "";
        enc1 = enc2 = enc3 = enc4 = "";

     } while (i < input.length);

     return unescape(output);
  }

  //--></script>
</html>