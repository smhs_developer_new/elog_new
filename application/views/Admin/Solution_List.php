<!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Solution List</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            
            
          </ol>
        </div>
      </div>

<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">

<a class="btn btn-primary btn-lg" href="<?= base_url()?>Admin/AddSolution" > Add New Solution</a>  

</div>
</div>
</div>
</div>
	  
<div class="card card-default" id="divftbl"   >
    <div class="card-header p-5"><center>Solution List</center></div>
    <table class="table table-dark" id="recordtbl">
    <thead>
    <tr tabindex=0>                        
      <th scope="col">S.No.</th>
      <th scope="col">Solution Code</th>
      <th scope="col">Solution Name</th>
      <th scope="col">Block</th>
      <th scope="col">Short Name</th>
      <th scope="col">Sol. Qty</th>
      <th scope="col">Water Qty</th>
      <th scope="col">Tot. Qty</th>
      <th scope="col">Created By</th>
      <th scope="col">Created On</th>
      <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody> 
     <?php if(count($data['Soldtl']->result())) { ?>
        <?php $N=0; foreach($data['Soldtl']->result() as $row) { $N++; ?>
        <tr><td><?php echo $N ?></td><td><?php echo $row->solution_code ?></td><td><?php echo $row->solution_name ?></td><td><?php echo $row->Block_Code ?></td><td><?php echo $row->Short_Name ?></td><td><?php echo $row->sol_qty."".$row->UoM ?></td><td><?php echo $row->water_qty."".$row->UoM ?></td><td><?php echo $row->tot_qty."".$row->UoM ?></td><td><?php echo $row->created_by ?></td><td><?php echo $row->created_on ?></td>
          <td class="text-left" style="">
            <a type="button" href="<?= base_url()?>Admin/EditSolution/<?= $row->id ?>" class="btn btn-sm btn-info mr-2 command-edit" data-id="10243"><em class="fa fa-edit fa-fw"></em></a>
            <a type="button" class="btn btn-sm btn-danger command-delete" onclick="CommonDelete('<?php echo $row->id ?>')" data-id="10243"><em class="fa fa-eye-slash fa-fw"></em></a>
            </td>
        </tr>
        <?php } } else { ?>  
          <tr><td colspan="5"><center>No record exist.</center></td></tr>
        <?php } ?>                  
    </tbody>
    </table>
    </div><!-- START row-->

  </div>
  </div>
  </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?php echo base_url(); ?>js/app.js"></script> 
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>
<!--<script type="text/javascript" src="js/script.js"></script>-->
<script type="text/javascript">
  function CommonDelete(id)
  {
    debugger;
    var r = confirm("Do You Want To Deactivate This Record ?");
    if(r == true)
    {
      var id=id;
      var tblname = "mst_solution";
      var colname = "id";
      $.ajax({
          url: "<?= base_url()?>Admin/CommonDelete",
          type: 'POST',
          data: {id:id,tblname:tblname,colname:colname},
          success: function(res) {

          if(res>0){
            alert("Record successfully deactivated...");
            window.location.href = "<?= base_url()?>Admin/SolutionList";
          }
          else
          {
            alert("Something went wrong");
          }
         }
      }); 
    }
  }
</script>

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="head"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="message"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <a href="" class="btn btn-primary" id="disclose">Ok</a>
      </div>
    </div>

  </div>
</div>

</body>
</html>