  <!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Drain Point Details</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item active"><a href="<?= base_url()?>Admin/mstDrainpontview">Drain point</a></li>
            
          </ol>
        </div>
      </div>
	  


<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">

<a class="btn btn-primary btn-lg" href="<?= base_url()?>admin/mstDrainpointView" > Back To list</a>  
&nbsp;&nbsp;&nbsp;
<a class="btn btn-primary btn-lg" href="<?= base_url()?>admin/mstDrainpoint"> Add New Drain Point</a>

</div>
</div>
</div>
</div>

<form id="drainpoint">
<input type="text" name="dpid" value="<?= $data['dpdetails']['id']?>" required id="dpid" hidden> 
    <div class="card card-default">
        <div class="card-body">

	  <fieldset>
                        
                           <div class="col-auto">
                             <!-- <div class="form-row">
                                 <div class="col-lg-12 mb-3"><label for="validationServer01">Sanitization Used <span style="color: red">*</span></label><input class="form-control autocomplete" type="text" placeholder="Sanitization Used" name="solutionname" id="solutionname" required> 
                                 </div>
                              </div>   -->
                              <div class="form-row">   
                                 <div class="col-lg-4 mb-3"><label for="aqty">Darin Point Code <span style="color: red">*</span></label><input class="form-control" id="dpcode" type="text" placeholder="" name="dpcode" value = "<?=$data['dpdetails']['drainpoint_code']?>" readonly >
                                 </div>
                                 <div class="col-lg-4 mb-3"><label for="edso">Drain point Name <span style="color: red">*</span></label><input class="form-control" id="dpname" type="text" placeholder="Drain Point Name" name="dpname"  value = "<?=$data['dpdetails']['drainpoint_name']?>">
                                 </div>
                                 <div class="col-lg-4 mb-3"><label for="validationServer01">Remarks </label><textarea class="form-control" aria-label="With textarea" name="dpremarks" placeholder="Enter Remarks" ><?=$data['dpdetails']['drainpoint_remark']?></textarea>
                                    
                                 </div>
                                 
                              </div>

                              <div class="form-row">
                                                                  
                                 
                                <div class="col-lg-6 mb-3"><label for="validationServer01">Room Code </label>
                  <select id="rcode" name="rcode" class="form-control">
  <option value="" selected disabled>Select Room Code </option>
 <?php if($data['rcode']->num_rows()>0) { ?>
        <?php foreach($data['rcode']->result() as $row) { 
		$ss = $row->room_code == $data['dpdetails']['room_code'] ? "selected":"";		
		?>
        <option value="<?php echo $row->room_code ?>"<?= $ss?>><?php echo $row->room_code ?> </option>
        <?php } } ?> 
</select>
                                 
                              </div>
                               <!--  <div class="col-lg-4 mb-3"><label for="validationServer01">Remarks <span style="color: red">*</span></label><textarea class="form-control" aria-label="With textarea" name="rremarks" placeholder="Enter Remarks" required></textarea>
                                    
                                 </div>-->

								<div class="col-lg-6 mb-3"><label for="validationServer01">Solution Code </label>
                  <select id="scode" name="scode" class="form-control">
  <option value="" selected disabled>Select Solution Code </option>
 <?php if($data['scode']->num_rows()>0) { ?>
        <?php foreach($data['scode']->result() as $row) { 
		 $ss = $row->solution_code == $data['dpdetails']['solution_code'] ? "selected":"";	
		?>
        <option value="<?php echo $row->solution_code ?>"<?= $ss?>><?php echo $row->solution_code ?> | <?php echo $row->solution_name ?> </option>
        <?php } } ?> 
</select>
                                 
                              </div>

							  

                              
                              
                              
                        </div>
                     </fieldset>
					 </div>
					 </div>
					  <div class="card card-default">
                     
                     <div class="card-body" >
                        
                          
<div class="row disable-button-color">
      <div class="col-sm-12 text-center" id="sbtn"><button id="sbbtn" class="btn btn-success btn-lg" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Save &nbsp;&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;<button id="upbtn" class="btn btn-danger btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Deactivate &nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      </div>

                        
                     </div>
                  </div><!-- END card-->
					 
</form>

        </div>
      </div>
      
    </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?= base_url()?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?= base_url()?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?= base_url()?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?= base_url()?>vendor/i18next/i18next.js"></script> 
<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?= base_url()?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?= base_url()?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?= base_url()?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- Modal Large-->
<div class="modal fade" id="myModalLarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabelLarge" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelLarge">Add portable euipments in your euipment list</h4>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>		
      </div>
	  
      <div class="modal-body">
		<ul class="task-list list-unstyled">
        <li class="bg-white pt10 pb10 pl15"> 
        <span class="col-sm-3 pl-0"><b>Area :</b> </span><span class="col-sm-3 pl-0" id="area">Granulation</span>
        <select class="custom-select custom-select-sm col-sm-6" id="areaddl" onchange="araechange();">
        <option selected="">Select Another Area</option>
        <option value="Blending">Blending</option>
        <option value="Coating">Coating</option>
        <option value="Packaging">Packaging</option>
        </select>
                              <button class="btn btn-success col-sm-2 mr-2" type="button" value="Show all" onclick="showpequip();" name="showall" id="showall" style="float:right;">Show All</button>
							  <button class="btn btn-success col-sm-2 mr-2" style="display:none;float:right;" type="button" value="Show less" onclick="hidepequip();" name="showless" id="showless">Show less</button>
                              
        </li>
        </ul>
        <section class="page-tasks">
		
          <ul class="task-list list-unstyled" id="peqlist">
            <li id="pli1"> <span class="view" id="p1">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 1" id="check1" name="check1">
                  <label>Portable euipment 1</label>
                </div>
                <!-- <div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli2"> <span class="view" id="p2">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 2" id="check2" name="check2">
                  <label>Portable euipment 2</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli3"> <span class="view" id="p3">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 3" id="check3" name="check3">
                  <label>Portable euipment 3</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli4" style="display:none;"> <span class="view" id="p4">
              <div class="row">
                <div class="col-sm-12 pl0" onClick="Fstep">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 4" id="check4" name="check4">
                  <label>Portable euipment 4</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli5" style="display:none;"> <span class="view" id="p5">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 5" id="check5" name="check5">
                  <label>Portable euipment 5</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli6" style="display:none;"> <span class="view" id="p6">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 6" id="check6" name="check6">
                  <label>Portable euipment 6</label>
                </div>
                <!-- <div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
          </ul>
        </section>
      </div>
	  
    </div>
  </div>
</div>
<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 

<script src="<?= base_url()?>js/app.js"></script> 
<script type="text/javascript" src="js/script.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>

<script type="text/javascript">
$(document).ready(function(){

 $("#upbtn").click(function(){
    var condel = confirm("Do You Really Want To Deactivate This Record?"); 
	  if(condel == true)
	  {
	var drainid = $("#dpid").val();
    $.ajax({
        url: '<?= base_url()?>Admin/deleteDrainpoint',
        type: 'POST',
        data: {drainid:drainid},
        success: function (res) {
          //console.log(res);
          if(res.status==1){
           // $("#validationnow21").attr("disabled",true);
           // $("#validationnow22").attr("disabled",false);
            alert("Drain Point Deactivated Successfully");
            window.location.href = "<?= base_url()?>Admin/mstDrainpointview";
          }
        }
      });
  }});

   $('#drainpoint').submit(function () {
      $.ajax({
        url: '<?= base_url()?>Admin/mstDrainpointUpdate',
        type: 'POST',
        data: $(this).serialize(),
        success: function (res) {
          //console.log(res);
          if(res.status==1){
           // $("#validationnow21").attr("disabled",true);
           // $("#validationnow22").attr("disabled",false);
            alert("Drain Point Updated successfully");
            window.location.href = "<?= base_url()?>Admin/mstDrainpointview";
          }
        }
      });
      return false;
    });


});  
</script>


<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="head"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="message"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <a href="" class="btn btn-primary" id="disclose">Ok</a>
      </div>
    </div>

  </div>
</div>

</body>
</html>