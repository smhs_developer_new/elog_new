  <!-- Main section-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-3 pl-0">Reports</div>
        <form class="search-form col-sm-5 pl-0">
        </form>
        <div class="col-sm-4 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
          </ol>
        </div>
      </div>
      <div class="row mt-14">
        <div class="col-sm-6 text-center second-bg-color" style="background-color:#ff6f61 !important;"> <a href="<?php echo base_url(); ?>Report" class="roomhover" id="show-hexagoan1" >
          <div class="rectangle-box"><img src="<?php echo base_url(); ?>img/reports-h135px.png" width="90">
            <p class="mt-2">Seq Log for Area & Equipment                                                                                                                     </p>
          </div>
          </a> </div>
        <div class="col-sm-6 text-center second-bg-color" style="background-color:#88b04b !important;"> <a href="<?php echo base_url(); ?>Report/StatusReport" class="roomhover" id="show-hexagoan1" >
          <div class="rectangle-box"><img src="<?php echo base_url(); ?>img/reports-h135px.png" width="90">
            <p class="mt-2">Process status report                                                                                                                     </p>
          </div>
          </a> </div>
      </div>       
    </div>
  </section>
  <!-- Page footer-->
  