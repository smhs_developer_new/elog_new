<!-- Main section-->
      <section class="section-container">
         <!-- Page content-->
         <div class="content-wrapper">
            <div class="content-heading">
               <div>Workflow</div>
            </div>
			<div class="row">
				<div class="col-xl-12">
				<input class="btn btn-lg btn-info float-right mb-2 text-right" type="button" name="addbtn" onclick="addflow();" id="addbtn" value="Add workflow" />
				</div>
			</div>
			<div class="card card-default" id="flow" data-flag="0" style="display: none;">
        <div class="card-body" style="margin: 0 auto;">
          <div class="row">
		      <div class="col-sm-12"><div><span class="float-left"><h4>List of Activities</h4></span></div></div>
		      <div class="modal-body">
          <section class="page-tasks" id="Listofequip">
          <ul class="task-list list-unstyled" id="eqlist">
          <?php if($data['activities']=="")
          {

          }
          else
          {
            foreach($data['activities']->result() as $row) { ?>
            <li> 
            <span class="view bg-info"><div class="row"><div class="col-sm-12 pl0"><div class="row">
            <div class="col-sm-8"><img class="mb-2 mt-2 wt42" src="<?php echo base_url(); ?>img/<?php echo $row->activity_icon ?>"><span class="f15 pl60"><?php echo $row->activity_name ?></span></div>
            <div class="col-sm-4 disable-button-color float-right text-right pl00" id="show19">             
            <a class="btn btn-labeled btn-danger mt-3 mb-2" type="button" name="btndb19" id="btnbd19" href="<?= base_url()?>User/addstep/<?= $row->activity_code ?>"><span class="btn-label"><i class="fa fa-check"></i></span>Go</a>                
            </div>
            </div>
            </div>
            </div>
            </span> 
            </li>
        <?php } } ?>
          </ul>
        </section>
		
		</div>
          </div>		
		  
        </div>
      </div>
			<div class="card">
               <div class="card-body">
					<div>
                     <table class="table table-striped bootgrid-table" id="bootgrid-command" aria-busy="false">
                        <thead>
                           <tr>
						   <th>S.no.</th><th>Workflow</th><th>Created By</th><th>Created On</th><th>Action</th>
						   </tr>
                        </thead>
                        <tbody>
            <?php if(count($data['wf']->result())) { ?>
            <?php $i=0; foreach($data['wf']->result() as $row) { $i++;?> 
						<tr data-row-id="0">
						<td class="text-left" style=""><?php echo $i ?></td>
						<td class="text-left" style=""><?php echo $row->activity_name ?></td>
            <td class="text-left" style=""><?php echo $row->emp_name ?></td>
						<td class="text-left" style=""><?php echo $row->created_on ?></td>
						<td class="text-left" style="">
						<a type="button" href="<?= base_url()?>User/editstep/<?= $row->activity_id ?>" class="btn btn-sm btn-info mr-2 command-edit" data-id="10243"><em class="fa fa-edit fa-fw"></em></a>
						<a type="button" class="btn btn-sm btn-danger command-delete" onclick="deleteworkflow('<?php echo $row->activity_id ?>')" data-id="10243"><em class="fa fa-trash fa-fw"></em></a>
						</td>
						</tr>
            <?php } } else { ?>
              <tr data-row-id="0"><td colspan="5"><center>No record exist.</center></td></tr>
            <?php } ?>
          </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </section><!-- Page footer-->
      <footer class="footer-container"><span>&copy; 2019 - Angle</span></footer>
   </div><!-- =============== VENDOR SCRIPTS ===============-->
   <!-- MODERNIZR-->
   <script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API-->
   <script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL-->
   <script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next-->
   <script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script>
   <script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script>
   <script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script>
   <script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script>
   <script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============-->
   <!-- Bootgrid-->
   <script src="<?php echo base_url(); ?>vendor/jquery-bootgrid/dist/jquery.bootgrid.js"></script>
   <script src="<?php echo base_url(); ?>vendor/jquery-bootgrid/dist/jquery.bootgrid.fa.js"></script><!-- =============== APP SCRIPTS ===============-->
   <script src="<?php echo base_url(); ?>js/app.js"></script>

   <script type="text/javascript">
     function addflow()
     {
        if($("#flow").attr('data-flag')=='0')
        {
        $("#flow").show(1000);
        $("#addbtn").val('Less workflow');
        $("#flow").attr('data-flag','1');
        }
        else
        {
        $("#flow").hide(1000);
        $("#addbtn").val('Add workflow');
        $("#flow").attr('data-flag','0');
        }

     }
     function go(act)
     {

     }

     function deleteworkflow(actid)
     {
         debugger;
         var act=actid;
         $.ajax({
            url: "<?= base_url()?>User/deleteworkflow",
            type: 'POST',
            data: {act:act},
            success: function(res) {
            if(res>0){
               alert("Workflow successfully deleted...");
               window.location.href = "<?= base_url()?>User/workflow";
            }
            else
            {
               alert("Something went wrong");
            }
            
         }
         }); 
      }
   </script>
</body>

</html>