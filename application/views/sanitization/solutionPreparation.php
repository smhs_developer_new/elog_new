
  <!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        
        <div class="col-sm-3 pl-0">Solution Preparation</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-9 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Sanitization/area"><?php echo $this->session->userdata('area_code') ?></a></li>
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Sanitization/room?area=<?php echo $this->session->userdata('area_code') ?>"><?php echo $this->session->userdata('room_code') ?></a></li>
      <li class="breadcrumb-item active"><a href="<?= base_url()?>Sanitization/solutionPreparationview">Solution Preparation</a></li>
          </ol>
        </div>


      
</div>


<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">
<div style="padding-right: 5px">
<a class="btn btn-primary btn-lg" href="<?= base_url()?>Sanitization/solutionPreparationview" > Back To list</a>  
&nbsp;&nbsp;&nbsp;
<a class="btn btn-primary btn-lg" href="<?= base_url()?>Sanitization/solutionPreparation"> Add Solution</a>
</div>
</div>
</div>
</div>
</div> 


<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-6">
<span class="listexecutesop"><img src="<?= base_url()?>img/icons/area-operation-page.png"><b>Area : </b><?= $this->session->userdata("area_code")." , ".$this->session->userdata("area_name")?> </span> 
</div>
<div class="col-sm-6">
<span class="listexecutesop"><img src="<?= base_url()?>img/icons/area-operation-page.png"><b>Room : </b><?= $this->session->userdata("room_code")?></span>
</div>
</div>
</div>
</div>
	  
<form id="submit">

	<div class="card card-default">
        <div class="card-body">
			<div class="row">
            <div class="col-sm-6">
              <div class="listexecutesop"><img src="<?= base_url()?>img/icons/document-icon.png"><b>Document No. : </b><?= $docid?></div>
              <input type="text" name="doc_id" value="<?= $docid?>" hidden required>
            </div>

            
            <div class="col-sm-6"><div class="row"><div class="col-sm-12 ui-widget">
              <div class="listexecutesop"><img src="<?= base_url()?>img/icons/department.png"><b>Department Name : </b><?= $this->session->userdata('empdepartment_name')?></div>
              <input class="form-control autocomplete" type="text" placeholder="Department Name" name="departmentname" required value="<?= $this->session->userdata('empdepartment_name')?>" readonly hidden></div>
            </div>
            </div>

            </div>
        </div>
      </div>

	  <div class="card card-default">
        <div class="card-body">
          
                     
                              <div class="form-row">
                                 <div class="col-lg-6 mb-3"><label for="validationServer01">Previous Batch No. <span style="color: red">*</span></label><input class="form-control" id="validationServer01" type="text" placeholder="Batch No" name="batchno" required>
                                    
                                 </div>
                                 <div class="col-lg-6 mb-3"><label for="validationServer02">Previous QCAR No. <span style="color: red">*</span></label><input class="form-control" id="validationServer02" type="text" placeholder="QCAR No" name="qcarno" required>
                                 </div>                                 
                              </div>
                        
                     


<div class="form-row">            
<div class="col-sm-6">
<label for="validationServer02">Solution Name <span style="color: red">*</span></label>  
<input class="form-control autocomplete" type="text" placeholder="Solution Name" name="solutionname" required >
</div>

<div class="col-sm-6">
<label for="requiredquantity">Required Quantity <span style="color: red">*</span></label>  
<input class="form-control" type="number" min="0" placeholder="Required Quantity" name="requiredquantity" required id="requiredquantity">
</div>

</div>


        </div>             
                 
      </div>
	  

			<div class="row">
               <div class="col-md-6">
                  <!-- START card-->
                  <div class="card card-default">                     
                     <div class="card-body">                        
                           <div class="form-group"><label>Standard Solution Qty</label><input class="form-control" type="text" placeholder="ex: 2000 ml" readonly name="stdsolqty" id="stdsolqty" required></div>
                           <div class="form-group"><label>Purified Water</label><input class="form-control" type="text" placeholder="ex: 500 ml" readonly name="strpurewater" id="strpurewater" required></div>
                           <div class="form-group"><label>Makeup Volume</label><input class="form-control" type="text" placeholder="ex : 2500 cubic ml" readonly name="markupvol" id="markupvol" required></div>                        
                     </div>
                  </div><!-- END card-->
               </div>
               <div class="col-md-6">
                   <!-- START card-->
                  <div class="card card-default">
                     <div class="card-body">
                      <div class="form-group row">
                      <div class="col-sm-8 col-md-8">
                           <label>Actual Solution Qty <span style="color: red">*</span></label><input class="form-control" type="number" step="0.01" placeholder="Quantity" name="actsolqty" required id="actsolqty">
                      </div>
                      <div class="col-sm-4 col-md-4">
                        <label>UoM <span style="color: red">*</span></label>
                        <select class="form-control" name="actsoluom" required>
                          <option value="" selected disabled>Select UoM</option>
                          <option value="ml">ml</option>
                          <option value="l">l</option>
                        </select>
                      </div>
                    </div>

                    <div class="form-group row">
                      <div class="col-sm-8 col-md-8">
                           <label>Actual Purified Water <span style="color: red">*</span></label><input class="form-control" type="number" step="0.01" placeholder="Quantity" name="actpurewater" required id="actpurewater">
                      </div>
                      <div class="col-sm-4 col-md-4">
                        <label>UoM <span style="color: red">*</span></label>
                        <select class="form-control" name="actpureuom" required>
                          <option value="" selected disabled>Select UoM</option>
                          <option value="ml">ml</option>
                          <option value="l">l</option>
                        </select>
                      </div>
                    </div>

                           <!--<div class="form-group"><label>Actual Purified Water</label><input class="form-control" type="" placeholder=""></div>-->
                    <div class="row">
                    <div class="col-sm-12 col-md-12">       
                           <div class="form-group"> <label>Solution Valid Upto  (in hours) <span style="color: red">*</span></label><input class="form-control" type="number" min="1" placeholder="Number of Hours" name="validupto" required></div>
                    </div>       
                    
                    

                    </div>                               
                     </div>
                  </div><!-- END card-->
               </div>
            </div><!-- END row-->	
			
			 <div class="card card-default" id="btn1">
                     
                     <div class="card-body" >
                      


<div class="row disable-button-color">
      <div class="col-sm-4 text-right" id="sbtn"><button id="sbbtn" class="btn btn-success btn-lg" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Start&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      <div class="col-sm-4 text-right" id="rsbtn" style="display:none;"><span id="demo3" class="pr-2 f16"></span><button id="btnrs" class="btn btn-success btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Restart&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      <div class="col-sm-4"><button id="fbtn" disabled="disabled" onclick="stopoperation();" class="btn btn-danger btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Destroy&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      <div class="col-sm-4"><button id="chkby" disabled="disabled" onclick="checkedbyoperation();" class="btn btn-danger btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Shift Change &nbsp;&nbsp;&nbsp;&nbsp;</button>
      <span class="fa-stack mt-2 float-right" style="display:none;" name="FTR" id="FTR" data-flag="1" onclick="FTRshowtme();">
          <em class="fa fa-circle fa-stack-2x text-success bgred"></em>
          <em class="far fa-clock fa-stack-1x fa-inverse text-white"></em>
      </span>
      </div>
      <div class="col-sm-12 mt-2">

      <div class="card card-default" id="divftbl" style="display:none;">
               <div class="card-header p-5"><center>Activity Log Record</center></div>
               <table class="table table-dark" id="bdftbl">
                  <thead>
                     <tr tabindex=0>                        
                        <th scope="col">Start</th>
                        <th scope="col">End</th>
            <th scope="col">performed By</th>
            <th scope="col">Change Over</th>
            <th scope="col">Activity</th>
                     </tr>
                  </thead>
                  <tbody>
                  
                  </tbody>
               </table>
            </div><!-- START row-->
      
      </div>
      
      </div>


                        
                     </div>
                  </div><!-- END card-->
				  
				  </form>

<form id="submit2" hidden>
<div class="card card-default">
        <div class="card-body">
          <input type="text" name="doc_id" value="<?= $docid?>" hidden required>
                     <fieldset>
                              <div class="form-row">
                                 <div class="col-lg-6 mb-3"><label for="remark">Check Remark</label><input class="form-control" type="text" placeholder="Check Remark" name="checkremark" required id="remark">
                                    
                                 </div>
                                 <div class="col-lg-6 mb-3"><label for="destory">Solution Destroyed By</label><input class="form-control" type="text" placeholder="Solution Destroyed By" name="soldesby" required id="destory">
                                 </div>                                 
                              </div>
                        
                     </fieldset>
        </div>             
                 
      </div>

      <div class="card card-default" hidden>
                     
                     <div class="card-body" >
                        
                           <div class="form-group row" >
                          
                <div class="col-xl-12 text-center">
                  <button type="button" class="btn btn-danger btn-lg" onclick="viewmore()">View More</button>
                  <button type="submit" class="btn btn-danger btn-lg">Destroy</button>
                  <button type="button" class="btn btn-danger btn-lg" onclick="mainscreen()">Add New Solution</button>
                  </div>
                           </div>
                        
                     </div>
                  </div>

</form>

        </div>
      </div>
    </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?= base_url()?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?= base_url()?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?= base_url()?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?= base_url()?>vendor/i18next/i18next.js"></script> 
<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?= base_url()?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?= base_url()?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?= base_url()?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 

<script type="text/javascript">
  
  $(document).ready(function(){


    $("#requiredquantity").change(function(){
      tval = this.value;
      stdsolqty = $("#stdsolqty").val();
      strpurewater = $("#strpurewater").val();
      markupvol = $("#markupvol").val();
      var a1 = (stdsolqty/markupvol)*tval;
      var a2 = (strpurewater/markupvol)*tval;
      
      //alert(a1+"_"+a2*a1);
      $("#actsolqty").val(a1.toFixed(2));
      $("#actpurewater").val(a2.toFixed(2));
      
    });

  })
</script>

<!-- Modal Large-->
<div class="modal fade" id="myModalLarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabelLarge" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelLarge">Add portable euipments in your euipment list</h4>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>		
      </div>
	  
      <div class="modal-body">
		<ul class="task-list list-unstyled">
        <li class="bg-white pt10 pb10 pl15"> 
        <span class="col-sm-3 pl-0"><b>Area :</b> </span><span class="col-sm-3 pl-0" id="area">Granulation</span>
        <select class="custom-select custom-select-sm col-sm-6" id="areaddl" onchange="araechange();">
        <option selected="">Select Another Area</option>
        <option value="Blending">Blending</option>
        <option value="Coating">Coating</option>
        <option value="Packaging">Packaging</option>
        </select>
                              <button class="btn btn-success col-sm-2 mr-2" type="button" value="Show all" onclick="showpequip();" name="showall" id="showall" style="float:right;">Show All</button>
							  <button class="btn btn-success col-sm-2 mr-2" style="display:none;float:right;" type="button" value="Show less" onclick="hidepequip();" name="showless" id="showless">Show less</button>
                              
        </li>
        </ul>
        <section class="page-tasks">
		
          <ul class="task-list list-unstyled" id="peqlist">
            <li id="pli1"> <span class="view" id="p1">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 1" id="check1" name="check1">
                  <label>Portable euipment 1</label>
                </div>
                <!-- <div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli2"> <span class="view" id="p2">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 2" id="check2" name="check2">
                  <label>Portable euipment 2</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli3"> <span class="view" id="p3">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 3" id="check3" name="check3">
                  <label>Portable euipment 3</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli4" style="display:none;"> <span class="view" id="p4">
              <div class="row">
                <div class="col-sm-12 pl0" onClick="Fstep">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 4" id="check4" name="check4">
                  <label>Portable euipment 4</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli5" style="display:none;"> <span class="view" id="p5">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 5" id="check5" name="check5">
                  <label>Portable euipment 5</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli6" style="display:none;"> <span class="view" id="p6">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 6" id="check6" name="check6">
                  <label>Portable euipment 6</label>
                </div>
                <!-- <div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
          </ul>
        </section>
      </div>
	  
    </div>
  </div>
</div>
<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?= base_url()?>js/app.js"></script> 

<!--<script type="text/javascript" src="<?= base_url()?>js/script.js"></script>-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>



<script type="text/javascript">

$(document).ready(function(){

$(".autocomplete").change(function() {

var str = $(this).val();

$.ajax({
    url: "<?= base_url()?>Sanitization/solnamegetval",
    type: 'POST',
    data: {str:str},
    success: function(res) {
        //console.log(res);
        $("#stdsolqty").val(res.sol_qty);
        $("#strpurewater").val(res.water_qty);
        $("#markupvol").val(res.tot_qty);

        $("#requiredquantity").val("");
        $("#actsolqty").val("");
        $("#actpurewater").val("");
    }
});

});  


$(".autocomplete").keyup(function() {
var str = $(this).val();
if(str!="" && str.length>=1){
  
$.ajax({
    url: "<?= base_url()?>Sanitization/solname",
    type: 'POST',
    data: {str:str},
    success: function(res) {
        console.log(res);
        availableTags = res; 
        //alert(res);
        /*var availableTags = [
    "ActionScript", "AppleScript", "Asp", "BASIC", "C", "C++",
    "Clojure", "COBOL", "ColdFusion", "Erlang", "Fortran",
    "Groovy", "Haskell", "Java", "JavaScript", "Lisp", "Perl",
    "PHP", "Python", "Ruby", "Scala", "Scheme"
  ];*/
  
  $(".autocomplete").autocomplete({
    source: availableTags
  });
    }
});

  
}

});

});

  function mainscreen(){
    //$("#submit2")[0].reset();
    //$("#submit2").hide();
    //$("#submit").show();
    location.reload(true);
  }

  function viewmore(){
    $("#btn1").hide();
    $("#submit").show();
  }

  $(document).ready(function(){
    $("#submit2").hide();
  
  $('#submit').submit(function(){
	  var conform = confirm("Do You Really Want To Start This?"); 
	  if(conform == true)
	  {
    $.ajax({
      type: 'POST',
      url: '<?= base_url()?>Sanitization/solpre',
      data: $(this).serialize() // getting filed value in serialize form
    })
    .done(function(details){ // if getting done then call.
      // show the response
      if(details.status==1){ 
        //console.log(details);
        
        $("#myb").click();
        $("#head").html(details.msgheader);
        $("#message").html(details.msg);

        //alert("Solution preparation successfully submitted");
        //window.location.href = "<?= base_url()?>Sanitization/solutionPreparationview";
        //window.location.href = "<?= base_url()?>Sanitization/solutionPreparationviewnow/"+details.id;

        $("#disclose").attr("href","<?= base_url()?>Sanitization/solutionPreparationviewnow/"+details.id);

      }
      else{
        alert("No");    
      }
    })
    .fail(function(details) { // if fail then getting message
      // just in case posting your form failed
      alert( "Workflow for this activitymay not be defined or connectivity to the server may be lost. Please contact the administartor." );
    });

// to prevent refreshing the whole page page
return false;
}
	  else
	  {
		  return false;
	  
	  }

});


  $('#submit2').submit(function(){ 
    $.ajax({
      type: 'POST',
      url: '<?= base_url()?>Sanitization/solpre2',
      data: $(this).serialize() // getting filed value in serialize form
    })
    .done(function(details){ // if getting done then call.
      // show the response
      if(details.status==1){ 
        alert("Remaining Solution Destroyed Successfully");
        location.reload(true);
        //$("#submit2")[0].reset();
        //$("#submit2").hide();
        //$("#submit").show();

      }
      else{
        alert("No");    
      }
    })
    .fail(function(details) { // if fail then getting message
      // just in case posting your form failed
      alert( "Posting failed." );
    });

// to prevent refreshing the whole page page
return false;

});
});  
</script>



<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="head"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="message"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <a href="" class="btn btn-primary" id="disclose">Ok</a>
      </div>
    </div>

  </div>
</div>

</body>
</html>