  <!-- Main section-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-3 pl-0">Select Room</div>
        
          
          <input class="form-control autocomplete" list="roomlist" type="text" id="roomcodetxt" placeholder="Enter room code">
          <datalist id="roomlist">
            <?php if(count($data['room']->result())) { ?>
            <?php foreach($data['room']->result() as $row) { ?>  
                <option value="<?php echo $row->room_code."/".$row->room_name ?>">
            <?php } } ?>
          </datalist>
        
        <div class="col-sm-4 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>sanitization/area"><?php echo $this->session->userdata('area_code') ?></a></li>
            <!--<li class="breadcrumb-item">Execute SOP</li>-->
          </ol>
        </div>
      </div>
      <div class="row mt-14" id="roomdiv">
        <?php 
            $this->session->set_userdata('area_code', $data['area_code']);
        ?>
        <?php if(count($data['room']->result())) { ?>
        <?php foreach($data['room']->result() as $row) {
        if($row->in_use != '1') { ?>
        <div class="col-sm-3 text-center first-bg-color mt-1"> <a href="<?php echo base_url(); ?>sanitization/activity?room=<?php echo $row->room_code ?>" class="roomhover" id="<?php echo $row->room_code ?>">
          <div class="rectangle-box"><img class="door" src="<?php echo base_url(); ?>img/room-icon.png"><img class="opendoor" src="<?php echo base_url(); ?>img/room-icon.png">
            <p><?php echo $row->room_code ?></p>
          </div>
          </a> </div>
        <?php } else { ?>
        <div class="col-sm-3 text-center first-bg-color mt-1" style="background-color:#676464 !important;"><a href="<?php echo base_url(); ?>sanitization/activity?room=<?php echo $row->room_code ?>" class="roomhover" id="<?php echo $row->room_code ?>"> 
        <div class="rectangle-box"><img class="door" src="<?php echo base_url(); ?>img/room-icon.png"><img class="opendoor" src="<?php echo base_url(); ?>img/room-icon.png"><p><?php echo $row->inuse_activity ?></p></div></a>
        </div>
        <?php } } ?> 
        <?php } else { ?>
          <div class="col-sm-3 text-center first-bg-color mt-1">
          <div class="rectangle-box">
            <p>There is no room in this area</p>
          </div>
          </div>
        <?php } ?>         
      </div>      
      <!--<div class="row mt50">
        <div class="col-sm-12 text-center"> <a href="#"><img src="<?php echo base_url(); ?>img/down-arrow.png" width="50" class="bounce"></a> </div>
      </div>-->
    </div>
  </section>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script>
$(document).ready(function()
{   
  if($("#roomcodetxt").val()!="" && $("#roomcodetxt").val()!=null && $("#roomcodetxt").length>0)
  {
    getroom();
  }

  $(".autocomplete").change(function() 
  {
        getroom();
  }); 

  $(".autocomplete").keyup(function() 
  {
    debugger;
    var rcode = $(this).val();
    if(rcode!="" && rcode.length>=1){  
      /*$.ajax({
        url: "<?= base_url()?>Production/roomcode",
        type: 'POST',
        data: {rcode:rcode},
        success: function(res) {
          debugger;
          console.log(res);            
          availableTags = res;   
          $(".autocomplete").autocomplete({
          source: availableTags
          });
        }
      });*/
    }
    else
    {
      location.reload(true);
    }
  });
});

function getroom()
{
  var roomcode = $("#roomcodetxt").val();
    var ararray = roomcode.split("/");
    var flag=0;
    var inv="";
    rcode = ararray[0];
    $.ajax({
      url: "<?= base_url()?>Production/roomdata",
      type: 'POST',
      data: {rcode:rcode},
      success: function(res) {
          console.log(res);
          if(res!=null && res != "")
          {
            inv = inv.concat('<div class="col-sm-3 text-center first-bg-color mt-1"> <a href="<?php echo base_url(); ?>sanitization/activity?room='+res.room_code+'" class="roomhover" id="<?php echo $row->room_code ?>"><div class="rectangle-box"><img class="door" src="<?php echo base_url(); ?>img/room-icon.png"><img class="opendoor" src="<?php echo base_url(); ?>img/room-icon.png"><p>'+res.room_code+'</p></div></a> </div>');
          }
          else
          {
              inv = inv.concat('<div class="col-sm-3 text-center first-bg-color mt-1"><div class="rectangle-box"><p>There is no room at this code, please Enter valid room code</p></div></div>');
          }
        $("#roomdiv").html(inv);
      }
    });
}
</script>