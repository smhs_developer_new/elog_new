
  <!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Daily Cleaning</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Sanitization/area"><?php echo $this->session->userdata('area_code') ?></a></li>
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Sanitization/room?area=<?php echo $this->session->userdata('area_code') ?>"><?php echo $this->session->userdata('room_code') ?></a></li>
      <li class="breadcrumb-item active"><a href="<?= base_url()?>Sanitization/dailyCleaning">Daily Cleaning</a></li>
            
          </ol>
        </div>
      </div>

            <div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">

<a class="btn btn-primary btn-lg" href="<?= base_url()?>Sanitization/dailyCleaningview" > Back To list</a>  
&nbsp;&nbsp;&nbsp;
<a class="btn btn-primary btn-lg" href="<?= base_url()?>Sanitization/dailyCleaning"> Add Daily Cleaning</a>

</div>
</div>
</div>
</div>

<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-6">
<span class="listexecutesop"><img src="<?= base_url()?>img/icons/area-operation-page.png"><b>Area : </b><?= $this->session->userdata("area_code")." , ".$this->session->userdata("area_name")?> </span> 
</div>
<div class="col-sm-6">
<span class="listexecutesop"><img src="<?= base_url()?>img/icons/area-operation-page.png"><b>Room : </b><?= $this->session->userdata("room_code")?></span>
</div>
</div>
</div>
</div>
	  
<form id="daily">
     <div class="card card-default">
        <div class="card-body">
          
            <div class="row">
            <div class="col-sm-6">
              <div class="listexecutesop"><img src="<?= base_url()?>img/icons/document-icon.png"><b>Document No. : </b><?= $docid?></div>
              <input type="text" name="doc_id" value="<?= $docid?>" hidden required>
            </div>

            
            <div class="col-sm-6"><div class="row"><div class="col-sm-12 ui-widget">
              <div class="listexecutesop"><img src="<?= base_url()?>img/icons/department.png"><b>Department Name : </b><?= $this->session->userdata('empdepartment_name')?></div>
              <input class="form-control autocomplete" type="text" placeholder="Department Name" name="departmentname" required value="<?= $this->session->userdata('empdepartment_name')?>" readonly hidden></div>
            </div>
            </div>

            </div>
                 
      </div>
	  </div>

	  <div class="card card-default">
        <div class="card-body">

	  <fieldset>
                        
                           <div class="col-md-12">
                            <div class="form-row">
                                 <div class="col-lg-12 mb-3"><label for="validationServer01">Sanitization Used <span style="color: red">*</span></label><input class="form-control autocomplete" type="text" placeholder="Sanitization Used" name="solutionname" id="solutionname" required> 
                                 </div>
                              </div>   
                              <div class="form-row">   
                                 <div class="col-lg-4 mb-3"><label for="aqty">Available Quantity <span style="color: red">*</span></label><input class="form-control" id="aqty" type="text" placeholder="Available Quantity" name="aqty" required readonly><input class="form-control" id="currentqty" type="text" name="currentqty" value="" hidden>
                                 </div>
                                 <div class="col-lg-4 mb-3"><label for="edso">Expiry Date of Solution <span style="color: red">*</span></label><input class="form-control" id="edso" type="text" placeholder="Expiry Date of Solution" name="edso" required readonly>
                                 </div>
                                 <div class="col-lg-4 mb-3"><label for="rtc">Required to Clean <span style="color: red">*</span></label><input class="form-control" id="rtc" type="number" placeholder="Required to Clean" name="rtc" step="0.01" required>
                                 </div>
                                 
                              </div>
                              <div class="form-row">
                                 <div class="col-lg-6 mb-3"><label for="validationServer01">Room No/ Corridor No.</label><input class="form-control" id="validationServer01" type="text" placeholder="Room no/ Corridor no." name="roomno" value="<?= $this->session->userdata('room_code')?>" readonly required>
                                    
                                 </div>
                                 <!--
                                 <div class="col-lg-6 mb-3"><label for="validationServer02">Drain point</label><input class="form-control" id="validationServer02" type="text" placeholder="Drain Point" name="drainpoint" required>
                                    
                                 </div>-->


                                 <div class="col-lg-6 mb-3">
                                  <label for="validationServer02">Drain point </label>
                                  <select id="example" multiple="multiple" class="form-control" name="drainpoint[]">
                                    <?php
                                    foreach ($items as $it) {
                                    ?>
                                    <option value="<?= $it?>"><?= $it?></option>
                                    <?php
                                    }
                                    ?>
                                  </select>
                                 </div>
                                 
                              </div>
                              <div class="form-row">
                                 <div class="col-lg-6 mb-3"><label for="validationServer03">Waste Bin : </label>

                                  
								 <label ><input id="inlineradio1" type="radio" name="wastebin" value="1" checked="checked"></span> Yes</label> <label class=""><input id="inlineradio2" type="radio" name="wastebin" value="0"> N/A</label>
                                    
                                 </div>
                                 
                                 <div class="col-lg-6 mb-3"><label for="validationServer03">Floor Coving : </label>
								 <label class=""><input id="inlineradio3" type="radio" name="floorcovering" value="1"> Yes</label> <label class=""><input id="inlineradio4" type="radio" name="floorcovering" value="0" checked="checked"> No</label>
                                    
                                 </div>

                              </div>
                              
                              
                        </div>
                     </fieldset>
					 </div>
					 </div>
					  <div class="card card-default">
                     
                     <div class="card-body" >
                        
                           <div class="row disable-button-color">
      <div class="col-sm-4 text-right" id="sbtn"><button id="sbbtn" class="btn btn-success btn-lg" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Start&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      
      <div class="col-sm-4"><button id="finish" class="btn btn-danger btn-lg" type="button" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Stop&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      <div class="col-sm-4"><button id="checkedby" class="btn btn-warning btn-lg" type="button" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Shift Change&nbsp;&nbsp;&nbsp;&nbsp;</button>
      
      </div>
      <div class="col-sm-12 mt-2" hidden>

      <div class="card card-default" id="divftbl" style="display:none;">
               <div class="card-header p-5"><center>Activity Log Record</center></div>
               <table class="table table-dark" id="bdftbl">
                  <thead>
                     <tr tabindex=0>                        
                        <th scope="col">Start</th>
                        <th scope="col">End</th>
            <th scope="col">performed By</th>
            <th scope="col">Shift Change</th>
            <th scope="col">Activity</th>
                     </tr>
                  </thead>
                  <tbody>
                  
                  </tbody>
               </table>
            </div><!-- START row-->
      
      </div>
      
      </div>
                        
                     </div>
                  </div><!-- END card-->
					 </form>


        </div>
      </div>
    </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?= base_url()?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?= base_url()?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?= base_url()?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?= base_url()?>vendor/i18next/i18next.js"></script> 
<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?= base_url()?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?= base_url()?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?= base_url()?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- Modal Large-->
<div class="modal fade" id="myModalLarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabelLarge" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelLarge">Add portable euipments in your euipment list</h4>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>		
      </div>
	  
      <div class="modal-body">
		<ul class="task-list list-unstyled">
        <li class="bg-white pt10 pb10 pl15"> 
        <span class="col-sm-3 pl-0"><b>Area :</b> </span><span class="col-sm-3 pl-0" id="area">Granulation</span>
        <select class="custom-select custom-select-sm col-sm-6" id="areaddl" onchange="araechange();">
        <option selected="">Select Another Area</option>
        <option value="Blending">Blending</option>
        <option value="Coating">Coating</option>
        <option value="Packaging">Packaging</option>
        </select>
                              <button class="btn btn-success col-sm-2 mr-2" type="button" value="Show all" onclick="showpequip();" name="showall" id="showall" style="float:right;">Show All</button>
							  <button class="btn btn-success col-sm-2 mr-2" style="display:none;float:right;" type="button" value="Show less" onclick="hidepequip();" name="showless" id="showless">Show less</button>
                              
        </li>
        </ul>
        <section class="page-tasks">
		
          <ul class="task-list list-unstyled" id="peqlist">
            <li id="pli1"> <span class="view" id="p1">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 1" id="check1" name="check1">
                  <label>Portable euipment 1</label>
                </div>
                <!-- <div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli2"> <span class="view" id="p2">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 2" id="check2" name="check2">
                  <label>Portable euipment 2</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli3"> <span class="view" id="p3">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 3" id="check3" name="check3">
                  <label>Portable euipment 3</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli4" style="display:none;"> <span class="view" id="p4">
              <div class="row">
                <div class="col-sm-12 pl0" onClick="Fstep">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 4" id="check4" name="check4">
                  <label>Portable euipment 4</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli5" style="display:none;"> <span class="view" id="p5">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 5" id="check5" name="check5">
                  <label>Portable euipment 5</label>
                </div>
                <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
            <li id="pli6" style="display:none;"> <span class="view" id="p6">
              <div class="row">
                <div class="col-sm-12 pl0">
                  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 6" id="check6" name="check6">
                  <label>Portable euipment 6</label>
                </div>
                <!-- <div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
              </div>
              </span> </li>
          </ul>
        </section>
      </div>
	  
    </div>
  </div>
</div>
<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?= base_url()?>js/app.js"></script> 

<!--<script type="text/javascript" src="js/script.js"></script>-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>
<script type="text/javascript" src="https://rawgit.com/select2/select2/master/dist/js/select2.js"></script>
      <link rel="stylesheet" type="text/css" href="https://rawgit.com/select2/select2/master/dist/css/select2.min.css">

<script type="text/javascript">
  $(document).ready(function(){

    $("#solutionname").keyup(function() {
var str = $(this).val();
if(str!="" && str.length>=1){
  
$.ajax({
    url: "<?= base_url()?>Sanitization/solUsed",
    type: 'POST',
    data: {str:str},
    success: function(res) {
        console.log(res);
        availableTags = res;         
  
  $("#solutionname").autocomplete({
    source: availableTags
  });
    }
});

  
}

});

$("#solutionname").change(function(){
//alert("hi.... : "+this.value);
var solname = this.value;
res = solname.split("|");
docid= res[0];
$.ajax({
    url: "<?= base_url()?>Sanitization/solUseddata",
    type: 'POST',
    data: {docid:docid},
    success: function(res) {
        console.log(res);
        $("#aqty").val(res.available_qty);
        $("#edso").val(res.expiry_date);
        $("#rtc").attr("max",res.available_qty); 
    }
});

});    


  $('#example').select2({
    placeholder: 'Select Drain Point'
});

    
	$("#sbbtn").click(function(){

//alert("hi.... : " +daily.solutionname.value);
var solname = daily.solutionname.value;

res = solname.split("|");
docid= res[0];
$.ajax({
    url: "<?= base_url()?>Sanitization/solUseddata",
    type: 'POST',
    data: {docid:docid},
    success: function(res) {
        console.log(res);
        $("#currentqty").val(res.available_qty);
		return formsubmit();
    }
});

}); 
	
	
	
	
	$('#daily').submit(function(){
	
	 var conform = confirm("Do You Really Want To Submit This?"); 
	  if(conform == true )
	  { 
   
	$.ajax({
      type: 'POST',
      url: '<?= base_url()?>Sanitization/dailyCleaningdetails',
      data: $(this).serialize() // getting filed value in serialize form
    })
    .done(function(details){ // if getting done then call.
      // show the response
      //console.log(details);
      
      if(details.status==1){ 
        $("#myb").click();
        $("#head").html(details.msgheader);
        $("#message").html(details.msg);
        //console.log(details.data);
        //alert("Daily Cleaning Started");
        //window.location.href = "<?= base_url()?>Sanitization/dailyCleaningview";
        //window.location.href = "<?= base_url()?>Sanitization/dailyCleaningviewnow/"+details.id;
		if(details.id!=0)
		  {
        $("#disclose").attr("href","<?= base_url()?>Sanitization/dailyCleaningviewnow/"+details.id);
		  }
		  else{
			  return false;
		  }
      }
      else{
        alert("Workflow for this activitymay not be defined or connectivity to the server may be lost. Please contact the administartor." ); 
		return false;
      }
      
    //})
    //.fail(function(details) { // if fail then getting message
      // just in case posting your form failed
     // alert( "Workflow for this activitymay not be defined or connectivity to the server may be lost. Please contact the administartor." );
  });

// to prevent refreshing the whole page page
return false;
	}
	else
	{
		//alert("Sufficients Solution Quantity Required is not available");
		return false;
	}

	});

	}); 
	
	

</script>

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="head"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="message"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <a href="" class="btn btn-primary" id="disclose">Ok</a>
      </div>
    </div>

  </div>
</div>

</body>
</html>