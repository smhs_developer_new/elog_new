<!-- Main section-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-3 pl-0">Select activity</div>
        <form class="search-form col-sm-5 pl-0">
          <!--<em class="icon-magnifier"></em>
          <input class="form-control" type="email" placeholder="Enter room code or number on name">-->
        </form>
        <div class="col-sm-4 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Sanitization/area"><?php echo $this->session->userdata('area_code') ?></a></li>
      <li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>Sanitization/room?area=<?php echo $this->session->userdata('area_code') ?>"><?php echo $this->session->userdata('room_code') ?></a></li>
            <!--<li class="breadcrumb-item">Execute SOP</li>-->
          </ol>
        </div>
      </div>
          <div class="row mt-14">
          <?php $this->session->set_userdata('room_code', $data['room_code']); ?>
			     <?php foreach($data['activity']->result() as $row) { ?> 
            <div class="col-sm-3 text-center first-bg-color mt-1" id="show-hexagoan"> <a href="<?php echo base_url(); ?>Sanitization/batch?activitycode=<?php echo $row->activity_code ?>" class="roomhover">
              <div class="rectangle-box"><img src="<?php echo base_url(); ?>img/<?php echo $row->activity_icon ?>">
                <p><?php echo $row->activity_name ?></p>
              </div>
              </a> </div>
            <?php } ?>			
          </div>          		  
          <div class="row mt30 mb-5" hidden>
            <div class="col-sm-12 text-center"> <a href="<?php echo base_url(); ?>#"><img src="<?php echo base_url(); ?>img/down-arrow.png" width="50" class="bounce"></a> </div>
          </div>
    </div>
  </section>
<!-- Page footer-->
  <footer class="footer-container text-center"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>


<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 
<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/flot/jquery.flot.time.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?php echo base_url(); ?>js/app.js"></script>

<script>
var onload = setTimeout(onload, 1000)
var onload = setTimeout(onload1, 1200)
var onload = setTimeout(onload2, 1400)
var onload = setTimeout(onload3, 1600)
var onload = setTimeout(onload4, 1800)
var onload = setTimeout(onload5, 2000)
var onload = setTimeout(onload6, 2200)
var onload = setTimeout(onload7, 2400)
var onload = setTimeout(onload8, 2600)
var onload = setTimeout(onload9, 2800)
var onload = setTimeout(onload10, 3000)
var onload = setTimeout(onload11, 3200)
var onload = setTimeout(onload12, 3400)

function onload(){
	document.getElementById("show-hexagoan").style.display = "block";
	}
	function onload1(){
	document.getElementById("show-hexagoan1").style.display = "block";
	}
	function onload2(){
	document.getElementById("show-hexagoan2").style.display = "block";
	}
	function onload3(){
	document.getElementById("show-hexagoan3").style.display = "block";
	}
	function onload4(){
	document.getElementById("show-hexagoan4").style.display = "block";
	}
	function onload5(){
	document.getElementById("show-hexagoan5").style.display = "block";
	}
	function onload6(){
	document.getElementById("show-hexagoan6").style.display = "block";
	}
	function onload7(){
	document.getElementById("show-hexagoan7").style.display = "block";
	}
	function onload8(){
	document.getElementById("show-hexagoan8").style.display = "block";
	}
	function onload9(){
	document.getElementById("show-hexagoan9").style.display = "block";
	}
	function onload10(){
	document.getElementById("show-hexagoan10").style.display = "block";
	}
	function onload11(){
	document.getElementById("show-hexagoan11").style.display = "block";
	}
	function onload12(){
	document.getElementById("show-hexagoan12").style.display = "block";
	}
</script>

</body>
</html>