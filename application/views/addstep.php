<!-- Main section-->
      <section class="section-container">
         <!-- Page content-->
         <div class="content-wrapper">
            <div class="content-heading">Add Step</div><!-- START row-->           
            <div id="steps">   
            <?php if($data['wfdtl']==""){ } else { foreach($data['wfdtl']->result() as $row) { ?>
            <div class='row' id='<?php echo "stepdiv".$row->id ?>'><input type='hidden' name='role' id='role' value='<?php echo $row->role_id ?>' data-id='<?php echo $row->role_id ?>' data-role='<?php echo $row->role_description ?>' /><div class='col-xl-3'></div><div class='col-xl-6'><div class='card' style='background-color: transparent;border-color:transparent;margin-bottom:0rem;'><div class='card-body bg-info'><div class='d-flex align-items-center'><div class='text-center'><img class='img-thumbnail circle img-fluid thumb64' src='<?php echo base_url(); ?>img/user/06.jpg' alt='Image'></div><div class='text-center'><h3 class='m-0'><?php echo $row->role_description ?></h3></div><div class='ml-auto align-self-start mt-3'><a name='role' value='<?php echo $row->role_id ?>' class='btn btn-danger' href='#' onclick="getremove('<?php echo $row->id ?>');"><em class='mr-2 fas fa-trash'></em><span>Delete</span></a></div></div></div><center><em class='fa-2x mt-2  fas fa-arrow-down'></em></center></div></div><div class='col-xl-3'></div></div>			
            <?php } } ?>
            </div>

			<div class="row">
			<div class="col-xl-3">
			</div>
               <div class="col-xl-6">
				  <!-- START card-->
                  <div class="card">                  
					 <!-- START card footer-->
                  <div class="card-footer">
                  <div>
						<center>
                  <input type="hidden" id="actid" name="actid" value="<?php echo $data['actid']; ?>" />
						<input class="btn btn-primary btn-sm" type="button" onclick="addstep()" id="addbtn" name="addbtn" value="Add step" />&nbsp;&nbsp;&nbsp;&nbsp;
						<input class="btn btn-success btn-sm" type="button" onclick="addworkflow();" name="Save" value="Save" />
						</div>
                  </div><!-- END card-footer-->
                  </div><!-- END card-->
				</div>
			<div class="col-xl-3">
			</div>
            </div><!-- END row-->
			
			<div class="row" id="flowstep" data-flag="0" style="text-align: center;display: none;">
			<div class="col-xl-8" style="margin:0 auto;">
			<div class="card card-default">
                <div class="card-footer"><div class="card-title">Select Role</div></div>
				<div class="card-body">
                  <div style="display: inline-flex;">
                  <?php if($data['role']=="")
                  {

                  }
                  else
                  {
                  foreach($data['role']->result() as $row) { ?>
                  <div class="col-xl-2"><img class="img-thumbnail circle img-fluid thumb64" src="<?php echo base_url(); ?>img/user/06.jpg" alt="Image">
						<p><a href="#" onclick="getrole('<?php echo $row->id ?>','<?php echo $row->role_description ?>');"><?php echo $row->role_description ?></a></p>
						</div>
                  <?php } } ?>
                  </div>
                </div>
				<!-- START card footer--><div class="card-footer"></div><!-- END card-footer--></div>
			</div>
			</div>
			
		 </div>
      </section>
<script type="text/javascript">
   var divid=0;
     function addstep()
     {
        if($("#flowstep").attr('data-flag')=='0')
        {
        $("#flowstep").show(1000);
        $("#addbtn").val('Less step');
        $("#flowstep").attr('data-flag','1');
        }
        else
        {
        $("#flowstep").hide(1000);
        $("#addbtn").val('Add step');
        $("#flowstep").attr('data-flag','0');
        }

     }
     function getrole(id,role)
     {
      divid++;
         cols="";
         cols += "<div class='row' id='stepdiv"+divid+"' style='display:none;'><input type='hidden' name='role' id='role' value='"+id+"' data-id='"+id+"' data-role='"+role+"' /><div class='col-xl-3'></div><div class='col-xl-6'><div class='card' style='background-color: transparent;border-color:transparent;margin-bottom:0rem;'><div class='card-body bg-info'><div class='d-flex align-items-center'><div class='text-center'><img class='img-thumbnail circle img-fluid thumb64' src='<?php echo base_url(); ?>img/user/06.jpg' alt='Image'></div><div class='text-center'><h3 class='m-0'>"+role+"</h3></div><div class='ml-auto align-self-start mt-3'><a name='role' value='"+id+"' class='btn btn-danger' href='#' onclick='getremove("+divid+");'><em class='mr-2 fas fa-trash'></em><span>Delete</span></a></div></div></div><center><em class='fa-2x mt-2  fas fa-arrow-down'></em></center></div></div><div class='col-xl-3'></div></div>";
         $("#steps").append(cols);
         $("#stepdiv"+divid).show(1000);
         $("#flowstep").hide(1000);
        $("#addbtn").val('Add step');
        $("#flowstep").attr('data-flag','0');
     }
     function getremove(obj)
     {
         debugger;
         $('#stepdiv'+obj).remove();
     }

     function addworkflow()
     {
         debugger;
         var obj=0;
         var act = $("#actid").val();
         var myArray = [];
         $("#steps").find('input[name^="role"]').each(function () {
         obj++;
         var role = $(this).val();
         myArray.push( {
         role: role,
         });
         });
         console.log(myArray);

         if(myArray.length>1)
         {
            $.ajax({
                url: "<?= base_url()?>User/makeworkflow",
                type: 'POST',
                data: {act:act,myArray:myArray},
                success: function(res) {
                if(res=="-1")
                {
                  alert("Please select at-least 2 level to create workflow...");
                }
                else if(res>0){
                  alert("Workflow successfully added...");
                  window.location.href = "<?= base_url()?>User/workflow";
                }
                else
                {
                  alert("Something went wrong");
                }            
                }
            }); 
          }
          else
          {
              alert("Please select at-least 2 level to create workflow...");
          }
      }
   </script>