

 <!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
  <section class="section-container">
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Sequential Logs</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
						<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>injectable/activities">Activity</a></li>
          </ol>
        </div>
      </div>
	  
<form id="selectwork">
<div class="card card-default">
        <div class="card-body">
			<div class="form-row">
			<div class="col-lg-6 mb-3"><label for="equipmentcode">Equipment Code <span style="color: red">*</span></label><input class="form-control" id="equipmentcode" type="text" placeholder="Equipment Code" name="equipmentcode" required>
			</div>
			<div class="col-lg-6 mb-3"><label for="sopno">Reference SOP No <span style="color: red">*</span></label><input class="form-control" id="sopno" type="text" placeholder="PAR-064" value="PAR-064" name="sopno" readonly required>
			</div>
	 		</div>
		</div>
</div>
<div class="card card-default" id="workdiv">
        <div class="card-body">
			<div class="form-row">
			
			<div class="col-lg-4 offset-lg-4 mb-4"><center>
			<label for="equipmentcode">Select Work To Perform </label><br>
			<button type="button" class="btn btn-lg btn-info" id="cleaningwork"><img width="50" src="<?php echo base_url(); ?>img/daily-cleaning-h100px.png" alt="Save icon"/><br/>Cleaning</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-lg btn-info" id="opwork"><img width="33" src="<?php echo base_url(); ?>img/operations-h100px.png" alt="Save icon"/><br/>Operation</button>
			</center>
        <!--<select id="equipddl" name="equipddl" class="custom-select custom-select-sm col-sm-12">
        <option value="" selected disabled> --- Select Work to Perform --- </option>
			  <option value="cleaning"> Cleaning </option>
        <option value="operation"> Operation </option>
			  </select>-->
			</div>
			
			</div>
		</div>
</div>
</form>

<form id="cleaning">
<!--<div class="card card-default">
        <div class="card-body">
			<div class="form-row">
			<div class="col-lg-6 mb-3"><label for="equipmentcode">Equipment Code <span style="color: red">*</span></label><input class="form-control" id="equipmentcode" type="text" placeholder="Equipment Code" name="equipmentcode" required>
			</div>
			<div class="col-lg-6 mb-3"><label for="sopno">Reference SOP No <span style="color: red">*</span></label><input class="form-control" id="sopno" type="text" placeholder="PAR-064" value="PAR-064" name="sopno" readonly required>
			</div>
	 		</div>
		</div>
</div>-->

<h4> Cleaning </h4>

<div class="card card-default">
        <div class="card-body">
			<div class="form-row">
			<!--<div class="col-lg-6 mb-3"><label for="cleaningequipmentcode">Cleaning Equipment Code</label><input class="form-control" id="cleaningequipmentcode" type="text" placeholder="" name="cleaningequipmentcode" readonly>
			</div>-->
			<div class="col-lg-12 mb-3" id="cleancheckbydiv"><label for="cleanremarks">Remarks</label><input class="form-control" id="cleanremarks" type="text" placeholder="" name="cleanremarks" disabled><input class="form-control" id="cleaningequipmentcode" type="text" placeholder="" name="cleaningequipmentcode" hidden readonly>
			</div>
	 		</div>

			<div class="form-row">
               <div class="col-md-12 text-center">
               	<button class="btn btn-lg btn-success" id="cleanstart" type="submit">Start</button> &nbsp; &nbsp;<button class="btn btn-lg btn-danger" id="cleanstop" type="button" disabled>Stop</button> &nbsp; &nbsp;<button class="btn btn-lg btn-warning" id="cleancheckby" type="button" disabled>Check By</button>
               </div>
            </div>   
		</div>
</div>
</form>

<!---- Cleaning Log Table Starts Here ---->
<div class="card card-default" id="cleaninglog">
<div class="card-header p-5"><center>Cleaning Log</center></div>
        <!--<div class="card-body">-->
             <table class="table table-dark" >
              <thead> 
               <tr>                        
                <th scope="col">Document Number</th>
				<th scope="col">Start</th>
                <th scope="col">End</th>
                <th scope="col">Done By</th>
                <th scope="col">Remarks</th>
                <th scope="col">Check By</th>
                </tr>
                </thead>
                <tbody id="clogs">
                  
                </tbody>
               </table>

		<!--</div>-->
</div>

<!---- Cleaning Log Table Ends Here ---->

<form id="operation">

<!--<div class="card card-default">
        <div class="card-body">
			<div class="form-row">
			<div class="col-lg-6 mb-3"><label for="equipmentcode1">Equipment Code <span style="color: red">*</span></label><input class="form-control" id="equipmentcode1" type="text" placeholder="Equipment Code" name="equipmentcode" required>
			</div>
			<div class="col-lg-6 mb-3"><label for="sopno">Reference SOP No <span style="color: red">*</span></label><input class="form-control" id="sopno" type="text" placeholder="PAR-064" value="PAR-064" name="sopno" readonly required>
			</div>
	 		</div>
		</div>
</div>-->

<h4> Operations </h4>

<div class="card card-default">
        <div class="card-body">
			<div class="form-row">
			<div class="col-lg-6 mb-3"><label for="productname">Product Name</label><input class="form-control" id="productname" type="text" placeholder="Product Name" name="productname" required><input class="form-control" id="proequipmentcode" type="text" placeholder="" name="proequipmentcode" hidden readonly>
			</div>
			<div class="col-lg-6 mb-3"><label for="productlotno">Batch/ Lot No.</label><input class="form-control" id="productlotno" type="text" placeholder="Batch/ Lot No." name="productlotno" required>
			</div>
	 		</div>

			<div class="form-row">
			<div class="col-lg-5 mb-3">
      <label for="opoutput">Output</label>
      <input class="form-control" type="text" placeholder="" id="opoutput" name="opoutput" disabled>
      </div>
      <div class="col-lg-1 mb-3">
      <label >&nbsp;</label>
      <button class="form-control btn btn-info" style="padding-right: 0px !important;padding-left: 0px !important;" type="button" id="btopoutput" disabled>Confirm</button>
			</div>

			<div class="col-lg-6 mb-3"><label for="opremarks">Remarks</label><input class="form-control" id="opremarks" type="text" placeholder="" name ="opremarks" disabled>
			</div>
	 		</div>

			<div class="form-row">
               <div class="col-md-12 text-center">
               	<button class="btn btn-lg btn-success" id="opstart" type="submit">Start</button> &nbsp; &nbsp;&nbsp; &nbsp;<button class="btn btn-lg btn-danger" id="opstop" type="button" disabled>Stop</button> &nbsp; &nbsp; &nbsp; &nbsp;<button class="btn btn-lg btn-warning" id="opcheckby" type="button" disabled>Check By</button>
               </div>
            </div>   
		</div>
</div>


<h4> BreakDown </h4>

<div class="card card-default">
        <div class="card-body">
			<!--<div class="form-row">
			<div class="col-lg-6 mb-3"><label for="validationServer01">Product Name</label><input class="form-control" id="validationServer01" type="text" placeholder="Auto Suggestion For Product">
			</div>
			<div class="col-lg-6 mb-3"><label for="validationServer01">Batch/ Lot No.</label><input class="form-control" id="validationServer01" type="text" placeholder="Auto Populated based on Product Selection">
			</div>
	 		</div>-->

			<!--<div class="form-row">
			<div class="col-lg-6 mb-3"><label for="breakformtime">BreakDown From Date & Time</label><input class="form-control" id="breakfromtime" type="datetime-local" placeholder="date & time" name="breakfromtime" disabled></div>
                           
			
			<div class="col-lg-6 mb-3"><label for="breaktotime">BreakDown To Date & Time</label><input class="form-control" id="breaktotime" type="datetime-local" placeholder="date & time" name="breaktotime" disabled></div>
	 		</div> -->

			<div class="form-row">
			<div class="col-lg-6 mb-3"><label for="breakremarks">Remarks</label><input class="form-control" id="breakremarks" type="text" placeholder="" name="breakremarks" disabled>
			</div>
			<div class="col-lg-6 mb-3"><label for="breakcheckby">&nbsp; &nbsp;</label><br><button class="btn btn-success" id="breakstart" type="button" disabled>Start</button>&nbsp;&nbsp;&nbsp;<button class="btn btn-warning" id="breakcheckby" type="button" disabled>Check By</button>
			</div>
	 		</div>

			
		</div>
</div>
</form>

<!---- operation Log Table Starts Here ---->
<div class="card card-default" id="operationlog">
        <!--<div class="card-body">-->
		<div class="card-header p-5"><center>Operation Log</center></div>
             <table class="table table-dark" id="bdftbl">
              <thead> 
               <tr> 
			   <th scope="col">Product Name</th>
                <th scope="col">Batch No</th>
                <th scope="col">Operation Start Time</th>
                <th scope="col">Operation End Time</th>
                <th scope="col">Output</th>
                <th scope="col">Operation Done By</th>
				<th scope="col">Operation Remarks</th>
				<th scope="col">Operation Check By</th>
                <th scope="col">Breakdown From</th>
				<th scope="col">Breakdown To</th>
				<th scope="col">Breakdown Remarks</th>
                <th scope="col">Breakdown Check By</th>
                </tr>
                </thead>
                <tbody id="oplogs">
                  
                </tbody>
               </table>

		<!--</div>-->
</div>

<!---- operation Log Table Ends Here ---->
		</div>


<!--</div>






        </div>
      </div>
    </div>-->
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============-->
<!-- MODERNIZR-->

<script src="<?= base_url()?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API-->
<script src="<?= base_url()?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL-->
<script src="<?= base_url()?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next-->
<script src="<?= base_url()?>vendor/i18next/i18next.js"></script>
<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script>
<script src="<?= base_url()?>vendor/jquery/dist/jquery.js"></script>
<script src="<?= base_url()?>vendor/popper.js/dist/umd/popper.js"></script>
<script src="<?= base_url()?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============-->

<script src="<?= base_url()?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============-->
<!-- =============== APP SCRIPTS ===============-->
<script src="<?= base_url()?>js/app.js"></script>

<!--<script type="text/javascript" src="<?= base_url()?>js/script.js"></script>-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>

<script type="text/javascript">
  
  $(document).ready(function(){
  $("#cleaning").hide();
  $("#operation").hide();
  $("#cleaninglog").hide();
  $("#operationlog").hide();
  $("#cleancheckbydiv").hide();
  //$("#maindocno").hide();

    $("#cleaningwork").click(function(){
       $("#operation").hide();
	   $("#cleaning").show();
	   $("#equipmentcode").attr("disabled",true);

    });

	$("#opwork").click(function(){
       $("#operation").show();
	   $("#cleaning").hide();
	   $("#equipmentcode").attr("disabled",true);

    });



    
	$("#equipddl").change(function(){
      //alert(this.value);
      if(this.value=="cleaning"){
        $("#operation").hide();
      }
      if(this.value=="operation"){
        $("#cleaning").hide();
      }
      $("#"+this.value).show();
	  //$("#workdiv").hide();
	  $("#equipmentcode").attr("disabled",true);

    });

   // $("#preparation").hide();
   // $("#sterilization").hide();
   // $("#filtration").hide();

    
    $("#equipmentcode").keyup(function() {
      var str = $(this).val();
      if(str!="" && str.length>0){
        
      $.ajax({
          url: "<?= base_url()?>Injectable/getEquipments",
          type: 'POST',
          data: {str:str},
          success: function(res) {
              console.log(res);
              availableTags = res; 
        $("#equipmentcode").autocomplete({
          source: availableTags
        });
          }
      });

      }
      });

    $("#equipmentcode").change(function() {
    var str = $(this).val();

    $.ajax({
        url: "<?= base_url()?>Injectable/getEquipmentsval",
        type: 'POST',
        data: {str:str},
        success: function(res) {
            //console.log(res);
            $("#equipmentcode").val(res.equipment_code);
            $("#cleaningequipmentcode").val(res.equipment_code);
			$("#proequipmentcode").val(res.equipment_code);
                
        }
    });

    });

    $("#equipmentcode1").keyup(function() {
      var str = $(this).val();
      if(str!="" && str.length>0){
        
      $.ajax({
          url: "<?= base_url()?>Injectable/getEquipments",
          type: 'POST',
          data: {str:str},
          success: function(res) {
              console.log(res);
              availableTags = res; 
        $("#equipmentcode1").autocomplete({
          source: availableTags
        });
          }
      });

      }
      });

    $("#equipmentcode1").change(function() {
    var str = $(this).val();

    $.ajax({
        url: "<?= base_url()?>Injectable/getEquipmentsval",
        type: 'POST',
        data: {str:str},
        success: function(res) {
            //console.log(res);
            $("#equipmentcode1").val(res.equipment_code);    
        }
    });

    });

    $("#productname").keyup(function() {
      var str = $(this).val();
      if(str!="" && str.length>0){
        
      $.ajax({
          url: "<?= base_url()?>Injectable/getProductnames",
          type: 'POST',
          data: {str:str},
          success: function(res) {
              console.log(res);
              availableTags = res; 
        $("#productname").autocomplete({
          source: availableTags
        });
          }
      });

      }
      });

    $("#productname").change(function() {

    var str = $(this).val();
    if(str!=""){
    $.ajax({
        url: "<?= base_url()?>Injectable/getProductlotno",
        type: 'POST',
        data: {str:str},
        success: function(res) {
            //console.log(res);
            $("#productlotno").val(res.batch_code);    
        }
    });
    }
    });



    $('#cleaning').submit(function () {
      $.ajax({
        url: '<?= base_url()?>Injectable/submitCleaning',
        type: 'POST',
        data: $(this).serialize(),
        success: function (res) {
          //console.log(res);
          if(res.status==1){
            $("#cleanstart").attr("disabled",true);
            $("#cleanstop").attr("disabled",false);
			$("#equipddl").attr("disabled",true);
			//$("#selectwork").hide();
			var tbl = '<tr><td scope="col"> PAR064 - '+res.logs.id+'</td><td scope="col">'+res.logs.cleaning_start+'</td><td scope="col">'+res.logs.cleaning_stop+'</td><td scope="col">'+res.logs.cleanstop_by+'</td><td scope="col">'+res.logs.clean_remarks+'</td><td scope="col">'+res.logs.clean_checkby+'</td></tr>';
			$("#clogs").html(tbl);
            alert("Cleaning is started successfully");
			$("#cleaninglog").show();
          }
        }
      });
      return false;
    });

    
	$("#cleanstop").click(function(){
      $.ajax({
        url: '<?= base_url()?>Injectable/stopCleaning',
        type: 'GET',
        //data: $(this).serialize(),
        success: function (res) {
          $("#cleanstop").attr("disabled",true);
          $("#cleancheckby").attr("disabled",false);
		  $("#cleanremarks").attr("disabled",false);
		  $("#cleancheckbydiv").show();
		  //$("#equipddl").attr("disabled",false);
		  //$("#selectwork").show();
         var tbl = '<tr><td scope="col"> PAR064 - '+res.logs.id+'</td><td scope="col">'+res.logs.cleaning_start+'</td><td scope="col">'+res.logs.cleaning_stop+'</td><td scope="col">'+res.logs.cleanstop_by+'</td><td scope="col">'+res.logs.clean_remarks+'</td><td scope="col">'+res.logs.clean_checkby+'</td></tr>';
			$("#clogs").html(tbl);
            alert("Cleaning is stopped successfully");
			$("#cleaninglog").show();

          //$("#sterilization").show();
		  //$("#filtration").show();
        }
      });
      
    });  
	
	
	
	
	
	$('#cleancheckby').click(function () {
          $("#myb").click();
          return false;
    });

    $('#loginform').submit(function () {
          $.ajax({
            url: '<?= base_url()?>Injectable/checkAuth',
            type: 'POST',
            data: $(this).serialize(),
            success: function (res) {
              //console.log(res);
              //alert('form was submitted');
              if(res.status == 1){
                //$("#msg").show();
                //$("#msg").html("Validate User ok..........");
               $("#cnow").click();
               $('#loginform')[0].reset();
               cleaningnow(res.data.emp_code+' - '+res.data.emp_name);
               alert("Checked By successful"); 
              }
              else{
                $('#loginform')[0].reset();
                $("#msg").show();
                $("#msg").html(res.msg);
                $("#msg").css("color","red");
                setTimeout(function() { $("#msg").hide(); }, 5000);
              }
            }
          });
          return false;

    });

    
	
	$('#operation').submit(function () {
      $.ajax({
        url: '<?= base_url()?>Injectable/submitOperation',
        type: 'POST',
        data: $(this).serialize(),
        success: function (res) {
          //console.log(res);
          if(res.status==1){
            $("#opstart").attr("disabled",true);
            $("#opstop").attr("disabled",false);
			//$("#breakstart").attr("disabled",false);
			//$("#breaktotime").attr("disabled",false);
			//$("#breakremarks").attr("disabled",false);
			$("#breakstart").attr("disabled",false);
			$("#breakcheckby").attr("disabled",false);
            alert("Operation is started successfully");
			$("#operationlog").show();
              
			  
			  var optbl = '<tr><td scope="col">'+res.logs.op_productname+'</td><td scope="col">'+res.logs.op_lotno+'</td><td scope="col">'+res.logs.op_start+'</td><td scope="col">'+res.logs.op_stop+'</td><td scope="col">'+res.logs.op_output+'</td><td scope="col">'+res.logs.op_by+'</td><td scope="col">'+res.logs.op_remarks+'</td><td scope="col">'+res.logs.op_checkby+'</td><td scope="col">'+res.logs.break_fromtime+'</td><td scope="col">'+res.logs.break_totime+'</td><td scope="col">'+res.logs.break_remarks+'</td><td scope="col">'+res.logs.break_checkby+'</td></tr>';
			  $("#oplogs").html(optbl);
          }
        }
      });
      return false;
    });

    
	$("#opstop").click(function(){
      $.ajax({
        url: '<?= base_url()?>Injectable/stopOperation',
        type: 'GET',
        //data: $(this).serialize(),
        success: function (res) {
          $("#opstop").attr("disabled",true);
		  //$("#opremarks").attr("disabled",false);
          //$("#opcheckby").attr("disabled",false);
		  $("#opoutput").attr("disabled", false);
          $("#btopoutput").attr("disabled", false);
		  alert("Operation is stopped successfully");
          
		  //$("#operationlog").show();
              
			  
			  var optbl = '<tr><td scope="col">'+res.logs.op_productname+'</td><td scope="col">'+res.logs.op_lotno+'</td><td scope="col">'+res.logs.op_start+'</td><td scope="col">'+res.logs.op_stop+'</td><td scope="col">'+res.logs.op_output+'</td><td scope="col">'+res.logs.op_by+'</td><td scope="col">'+res.logs.op_remarks+'</td><td scope="col">'+res.logs.op_checkby+'</td><td scope="col">'+res.logs.break_fromtime+'</td><td scope="col">'+res.logs.break_totime+'</td><td scope="col">'+res.logs.break_remarks+'</td><td scope="col">'+res.logs.break_checkby+'</td></tr>';
			  $("#oplogs").html(optbl);
		  
		  //$("#sterilization").show();
		  //$("#filtration").show();
        }
      });
      
    });
	

	$("#btopoutput").click(function(){

		$.ajax({
        url: '<?= base_url()?>Injectable/confirmOutput',
        type: 'POST',
        data: $('#operation').serialize(),
        success: function (res) {
          //$("#opstop").attr("disabled",true);
		  $("#opremarks").attr("disabled",false);
          $("#opcheckby").attr("disabled",false);
		  $("#opoutput").attr("disabled", true);
          $("#btopoutput").attr("disabled", true);
		  alert("Output Value Confirmed successfully");

		  //$("#operationlog").show();
              
			  
			  var optbl = '<tr><td scope="col">'+res.logs.op_productname+'</td><td scope="col">'+res.logs.op_lotno+'</td><td scope="col">'+res.logs.op_start+'</td><td scope="col">'+res.logs.op_stop+'</td><td scope="col">'+res.logs.op_output+'</td><td scope="col">'+res.logs.op_by+'</td><td scope="col">'+res.logs.op_remarks+'</td><td scope="col">'+res.logs.op_checkby+'</td><td scope="col">'+res.logs.break_fromtime+'</td><td scope="col">'+res.logs.break_totime+'</td><td scope="col">'+res.logs.break_remarks+'</td><td scope="col">'+res.logs.break_checkby+'</td></tr>';
			  $("#oplogs").html(optbl);

          //$("#sterilization").show();
		  //$("#filtration").show();
        }
      });
      
    });

   $("#breakstart").click(function(){
      $.ajax({
        url: '<?= base_url()?>Injectable/breakStart',
        type: 'GET',
        //data: $(this).serialize(),
        success: function (res) {
          $("#breakstart").attr("disabled",true);
		  $("#breakremarks").attr("disabled",false);
          //$("#opcheckby").attr("disabled",false);
		  //$("#opoutput").attr("disabled", false);
          //$("#btopoutput").attr("disabled", false);
		  alert("Breakdown is started successfully");
		  var optbl = '<tr><td scope="col">'+res.logs.op_productname+'</td><td scope="col">'+res.logs.op_lotno+'</td><td scope="col">'+res.logs.op_start+'</td><td scope="col">'+res.logs.op_stop+'</td><td scope="col">'+res.logs.op_output+'</td><td scope="col">'+res.logs.op_by+'</td><td scope="col">'+res.logs.op_remarks+'</td><td scope="col">'+res.logs.op_checkby+'</td><td scope="col">'+res.logs.break_fromtime+'</td><td scope="col">'+res.logs.break_totime+'</td><td scope="col">'+res.logs.break_remarks+'</td><td scope="col">'+res.logs.break_checkby+'</td></tr>';
			  $("#oplogs").html(optbl);
          //$("#sterilization").show();
		  //$("#filtration").show();
        }
      });
      
    });

	
	$('#opcheckby').click(function () {
          $("#myb1").click();
          return false;
    });

   $('#loginform_steri').submit(function () {
          $.ajax({
            url: '<?= base_url()?>Injectable/checkAuth',
            type: 'POST',
            data: $(this).serialize(),
            success: function (res) {
              //console.log(res);
              //alert('form was submitted');
              if(res.status == 1){
                //$("#msg").show();
                //$("#msg").html("Validate User ok..........");
               $("#cnow_steri").click();
               $('#loginform_steri')[0].reset();
               opnow(res.data.emp_code+' - '+res.data.emp_name);
               alert("Checked By successful"); 
              }
              else{
                $('#loginform_steri')[0].reset();
                $("#msg3").show();
                $("#msg3").html(res.msg);
                $("#msg3").css("color","red");
                setTimeout(function() { $("#msg3").hide(); }, 5000);
              }
            }
          });
          return false;

    });
	

	$('#breakcheckby').click(function () {
          $("#myb2").click();
          return false;
    });

   $('#loginform_filtration').submit(function () {
          $.ajax({
            url: '<?= base_url()?>Injectable/checkAuth',
            type: 'POST',
            data: $(this).serialize(),
            success: function (res) {
              //console.log(res);
              //alert('form was submitted');
              if(res.status == 1){
                //$("#msg").show();
                //$("#msg").html("Validate User ok..........");
               $("#cnow_filtration").click();
               $('#loginform_filtration')[0].reset();
               breaknow(res.data.emp_code+' - '+res.data.emp_name);
               alert("Checked By successful"); 
              }
              else{
                $('#loginform_filtration')[0].reset();
                $("#msg2").show();
                $("#msg2").html(res.msg);
                $("#msg2").css("color","red");
                setTimeout(function() { $("#msg2").hide(); }, 5000);
              }
            }
          });
          return false;

    });

	
	
	
	$("#solutionname").keyup(function() {
      var str = $(this).val();
      if(str!="" && str.length>0){
        
      $.ajax({
          url: "<?= base_url()?>Injectable/solutionNames",
          type: 'POST',
          data: {str:str},
          success: function(res) {
              console.log(res);
              availableTags = res; 
        $("#solutionname").autocomplete({
          source: availableTags
        });
          }
      });

      }
      });

    
    $("#solutionname").change(function() {
    var str = $(this).val();
    $.ajax({
        url: "<?= base_url()?>Injectable/getSolutionval",
        type: 'POST',
        data: {str:str},
        success: function(res) {
            //console.log(res);
            //$("#codeno").val(res.equipment_code);
            //sqwfi sqhp sqqs  
            $("#sqwfi").val(res.water_qty);
            $("#sqhp").val(res.sol_qty);
            $("#sqqs").val(res.wfi_qs);
                
        }
    });

    });

    
    


	

	
   $("#prelimit").keyup(function(){
    var vv = $(this).val();
    if(vv.length==2 && vv<"38"){
      alert("Value should be equal/more than 38");
      $(this).val("");
    }
    
   });

   $("#postlimit").keyup(function(){
    var vv = $(this).val();
    if(vv.length==2 && vv<"38"){
      alert("Value should be equal/more than 38");
      $(this).val("");
    }
    
   });

   $("#requiredquantity").change(function() {
   var tval = $(this).val();
   if(tval!="0" || tval!=""){
    sqwfi = $("#sqwfi").val();
    sqhp = $("#sqhp").val();
    sqqs = $("#sqqs").val();

    var a1 = (sqwfi/sqqs)*tval;
    var a2 = (sqhp/sqqs)*tval;

    $("#wati").val(a1.toFixed(2));
    $("#hpq").val(a2.toFixed(2));
    $("#wfiq").val(tval);
    
   }
   });

  });


  function cleaningnow(ucode){
    var data = $('#cleaning').serializeArray();
    data.push({name: 'ucode', value: ucode});
    $.ajax({
            url: '<?= base_url()?>Injectable/cleaningCheckby',
            type: 'POST',
            data: data,
            success: function (res) {
              console.log(res);
              $("#cleancheckby").attr("disabled",true);
			  $("#cleanremarks").attr("disabled",true);
			  $("#equipddl").attr("disabled",false);
		      //$("#operation").show();
			  $("#cleaninglog").show();
			  $("#cleaning").hide();
				
			  var tbl = '<tr><td scope="col"> PAR064 - '+res.logs.id+'</td><td scope="col">'+res.logs.cleaning_start+'</td><td scope="col">'+res.logs.cleaning_stop+'</td><td scope="col">'+res.logs.cleanstop_by+'</td><td scope="col">'+res.logs.clean_remarks+'</td><td scope="col">'+res.logs.clean_checkby+'</td></tr>';
			  $("#clogs").html(tbl); 
              //$("#preparation").show();
              //$("#fid").val(res.id); 
            }
           });   
  }

  function opnow(ucode){
    var data = $('#operation').serializeArray();
    data.push({name: 'ucode_steri', value: ucode});
    $.ajax({
            url: '<?= base_url()?>Injectable/operationCheckby',
            type: 'POST',
            data: data,
            success: function (res) {
              console.log(res);
			  $("#opremarks").attr("disabled",true);
              $("#opcheckby").attr("disabled",true);
			  $("#operation").hide();
			  $("#operationlog").show();
              
			  
			  var optbl = '<tr><td scope="col">'+res.logs.op_productname+'</td><td scope="col">'+res.logs.op_lotno+'</td><td scope="col">'+res.logs.op_start+'</td><td scope="col">'+res.logs.op_stop+'</td><td scope="col">'+res.logs.op_output+'</td><td scope="col">'+res.logs.op_by+'</td><td scope="col">'+res.logs.op_remarks+'</td><td scope="col">'+res.logs.op_checkby+'</td><td scope="col">'+res.logs.break_fromtime+'</td><td scope="col">'+res.logs.break_totime+'</td><td scope="col">'+res.logs.break_remarks+'</td><td scope="col">'+res.logs.break_checkby+'</td></tr>';
			  $("#oplogs").html(optbl);
              //$("#preparation").show();
              //$("#fid").val(res.id); 
            }
           });   
  }

  function breaknow(ucode){
    var data = $('#operation').serializeArray();
    data.push({name: 'ucode_break', value: ucode});
    $.ajax({
            url: '<?= base_url()?>Injectable/breakdownCheckby',
            type: 'POST',
            data: data,
            success: function (res) {
              console.log(res);
              $("#breakcheckby").attr("disabled","true");
			  var optbl = '<tr><td scope="col">'+res.logs.op_productname+'</td><td scope="col">'+res.logs.op_lotno+'</td><td scope="col">'+res.logs.op_start+'</td><td scope="col">'+res.logs.op_stop+'</td><td scope="col">'+res.logs.op_output+'</td><td scope="col">'+res.logs.op_by+'</td><td scope="col">'+res.logs.op_remarks+'</td><td scope="col">'+res.logs.op_checkby+'</td><td scope="col">'+res.logs.break_fromtime+'</td><td scope="col">'+res.logs.break_totime+'</td><td scope="col">'+res.logs.break_remarks+'</td><td scope="col">'+res.logs.break_checkby+'</td></tr>';
			  $("#oplogs").html(optbl);
              //alert("Checked By successful");
              //$("#preparation").show();
              //$("#fid").val(res.id); 
            }
           });   
  }

</script>


<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
     <div class="modal-content">
        <div class="modal-header">
           <h4 class="modal-title" id="myModalLabel">Login To Validate</h4>
        </div>
        <div class="modal-body">

          <form id="loginform">
            <div class="form-row">
              <div class="form-group col-md-12"><label>User Id <span style="color: red">*</span></label><input class="form-control" type="text" placeholder="User Id" required name="userid" id="userid"></div>
              <div class="form-group col-md-12"><label>Password <span style="color: red">*</span></label><input class="form-control" type="Password" placeholder="Password" required name="password" id="password"></div>
              <div class="form-group col-md-6">
                <p id="msg"></p>
              </div>
              <div class="form-group col-md-6 text-right">
                <center><button class="btn btn-success" type="submit">Submit</button> &nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-danger" type="button" data-dismiss="modal" id="cnow">Close</button></center>
              </div>
            </div>
          </form>
       

        </div>
        <!--<div class="modal-footer" ><button class="btn btn-green btn-secondary" type="button" data-dismiss="modal" id="cnow">Close</button></div>-->
     </div>
  </div>
</div>

<!----  STERILIZATION  ---->

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModa2" hidden id="myb1" data-backdrop="static" data-keyboard="false"></button>

<div class="modal fade" id="myModa2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
     <div class="modal-content">
        <div class="modal-header">
           <h4 class="modal-title" id="myModalLabel">Login To Validate</h4>
        </div>
        <div class="modal-body">

          <form id="loginform_steri">
            <div class="form-row">
              <div class="form-group col-md-12"><label>User Id <span style="color: red">*</span></label><input class="form-control" type="text" placeholder="User Id" required name="userid" id="userid"></div>
              <div class="form-group col-md-12"><label>Password <span style="color: red">*</span></label><input class="form-control" type="Password" placeholder="Password" required name="password" id="password"></div>
              <div class="form-group col-md-6">
                <p id="msg3"></p>
              </div>
              <div class="form-group col-md-6 text-right">
                <button class="btn btn-success" type="submit">Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-danger btn-secondary" type="button" data-dismiss="modal" id="cnow_steri">Close</button>
              </div>
            </div>
          </form>
       

        </div>
        <!--<div class="modal-footer" ><button class="btn btn-green btn-secondary" type="button" data-dismiss="modal" id="cnow_steri">Close</button></div>-->
     </div>
  </div>
</div>


<!----  FILTRATION  ---->

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModa3" hidden id="myb2" data-backdrop="static" data-keyboard="false"></button>

<div class="modal fade" id="myModa3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
     <div class="modal-content">
        <div class="modal-header">
           <h4 class="modal-title" id="myModalLabel">Login To Validate</h4>
        </div>
        <div class="modal-body">

          <form id="loginform_filtration">
            <div class="form-row">
              <div class="form-group col-md-12"><label>User Id <span style="color: red">*</span></label><input class="form-control" type="text" placeholder="User Id" required name="userid" id="userid"></div>
              <div class="form-group col-md-12"><label>Password <span style="color: red">*</span></label><input class="form-control" type="Password" placeholder="Password" required name="password" id="password"></div>
              <div class="form-group col-md-6">
                <p id="msg2"></p>
              </div>
              <div class="form-group col-md-6 text-right">
                <button class="btn btn-success" type="submit">Submit</button>&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-danger btn-secondary" type="button" data-dismiss="modal" id="cnow_filtration">Close</button>
              </div>
            </div>
          </form>
       

        </div>
        <!--<div class="modal-footer" ><button class="btn btn-green btn-secondary" type="button" data-dismiss="modal" id="cnow_filtration">Close</button></div>-->
     </div>
  </div>
</div>



</body>
</html>
