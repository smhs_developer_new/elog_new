
  <!-- Main section-->
<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Portable Equipment Cleaning</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/area"><?php echo $this->session->userdata('area_code') ?></a></li>
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/room?area=<?php echo $this->session->userdata('area_code') ?>"><?php echo $this->session->userdata('room_code') ?></a></li>
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/activity?room=<?php echo $this->session->userdata('room_code') ?>"><?php echo $this->session->userdata('activity_code') ?></a></li>
      <li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>Production/batch?activitycode=<?php echo $this->session->userdata('activity_code') ?>"><?php echo $this->session->userdata('batch_code') ?></a></li>
          </ol>
        </div>
      </div>

<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">
<div style="padding-right: 5px">
<a class="btn btn-primary btn-lg" href="<?= base_url()?>Production/portableEquipmentcleaningview" > Back To list</a>  
&nbsp;&nbsp;&nbsp;
<a class="btn btn-primary btn-lg" href="<?= base_url()?>Production/portableEquipmentcleaning"> Add New</a>
</div>
</div>
</div>
</div>
</div>

<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-6">
<span class="listexecutesop"><img src="<?= base_url()?>img/icons/area-operation-page.png"><b>Area : </b><?= $this->session->userdata("area_code")." , ".$this->session->userdata("area_name")?> </span> 
</div>
<div class="col-sm-6">
<span class="listexecutesop"><img src="<?= base_url()?>img/icons/area-operation-page.png"><b>Room : </b><?= $this->session->userdata("room_code")?></span>
</div>
</div>
</div>
</div>


    
    <div class="card card-default">
        <div class="card-body">
      <div class="row">
      <div class="col-sm-6">
      <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/document-icon.png"><b>Document No. : </b><?php echo $data['docid'] ?><input type="hidden" name="docid" id="docid" value="<?php echo $data['docid'] ?>" /></div>
      </div>
      <div class="col-sm-6">
      <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/department.png"><b>Department Name : </b><?php echo $data['departmentname'] ?></div>
      <input type="hidden" name="departmentname" id="departmentname" value="<?php echo $data['departmentname'] ?>" />
      </div>
      </div>
        </div>
      </div>
    
      <div class="card card-default">
        <div class="card-body">
          <div class="row mt-4">
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/code-p.png"><b>Product code : </b><?php echo $this->session->userdata('pcode'); ?><input type="hidden" name="pcode" id="pcode" value="<?php echo $this->session->userdata('pcode'); ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/mdicine-red.png"><b>Description : </b><?php echo $this->session->userdata('pdesc'); ?><input type="hidden" name="pdesc" id="pdesc" value="<?php echo $this->session->userdata('pdesc'); ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/market.png"><b>Market : </b><?php echo $this->session->userdata('market'); ?><input type="hidden" name="market" id="market" value="<?php echo $this->session->userdata('market'); ?>" /></div>
            </div>
          </div>
          <div class="row mt-4 mb-2">
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/code-bred.png"><b>Batch Code : </b><?php echo $this->session->userdata('bcode'); ?><input type="hidden" name="bcode" id="bcode" value="<?php echo $this->session->userdata('bcode'); ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/mg-red.png"><b>UOM : </b><?php echo $this->session->userdata('UOM'); ?><input type="hidden" name="UOM" id="UOM" value="<?php echo $this->session->userdata('UOM'); ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/40-red.png"><b>Batch Size : </b><?php echo $this->session->userdata('bsizeUOM'); ?><input type="hidden" name="bsizeUOM" id="bsizeUOM" value="<?php echo $this->session->userdata('bsizeUOM'); ?>" /></div>
            </div>
          </div>
        </div>
      </div>
      <div class="card card-default">
        <div class="card-body">
          <div class="row mb-3">
      <div class="col-sm-12">

              <div><span class="ml18 float-left pt11"><h4>Equipment list</h4></span></div>
        <div class="float-right"><span class="ml18 float-left pt11 mr15"><h4>Add portable equipments </h4></span><i data-toggle="modal" data-target="#myModalLarge" class="fa fa-plus default-icon-color default-icon-style float-right" aria-hidden="true"></i></div>
            </div>
      <div class="modal-body">
                    <section class="page-tasks" id="Listofequip">
          <ul class="task-list list-unstyled" id="eqlist">
            <?php 
			
			if($data['fixequipment'] != NuLL)
			{
			$counter=0; foreach($data['fixequipment']->result() as $row) { if($row->equipment_type == "Portable") { $counter++; ?> 
  <li id='<?php echo "eqli" .$row->id ?>'> 
  <input type ="hidden" id ="fe" name="fe" value="1">
  <input type="hidden" name='<?php echo "hddeqdtl" .$row->id ?>' id='<?php echo "hddeqdtl" .$row->id ?>' data-id='<?php echo $row->id ?>' data-eqcode='<?php echo $row->equipment_code ?>' data-sopcode='<?php echo $row->sop_code ?>' data-eqtype='<?php echo $row->equipment_type ?>' />
  <span class="view" id='<?php echo "eqdiv" .$row->id ?>'>
    <div class="row"><div class="col-sm-12 pl0">

    <div class="row">
  <div class="col-sm-4 listexecutesop2"><img class="mb-2 mt-2 wt42" src="<?php echo base_url(); ?>img/icons/sifter.png"><span class="f15"><?php echo $row->equipment_name ?></span></div>
  <div class="col-sm-4 mt-4"><p class="f15 mt-2"><?php echo $row->sop_code ?> <?php echo $row->sop_name ?></p></div>
  <div class="col-sm-4 mt-4">
                  <div class="form-group">
                    <input class="form-control" type="text" placeholder="Change part" name="changepart<?= $counter?>" id="changepart<?= $counter?>" disabled>
                </div>
                </div>
  </div>
  </div>
  </div>
  </span> 
  </li>
  <?php } }} 
	?>
            
          </ul>
      <ul class="task-list list-unstyled" id="porteqlist">
      </ul>
        </section>
    </form>
    </div>
          </div>    
      
      <div class="row disable-button-color">
      <div class="col-sm-4 text-right" id="sbtn"><span id="demo3" class="pr-2 f16"></span><button id="sbbtn" class="btn btn-success btn-lg" type="button" onClick="ftrrshow();">&nbsp;&nbsp;&nbsp;&nbsp;Start&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      <div class="col-sm-4 text-right" id="rsbtn" style="display:none;"><span id="demo3" class="pr-2 f16"></span><button id="btnrs" class="btn btn-success btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Restart&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      <div class="col-sm-4"><button id="fbtn" disabled="disabled" onclick="stopoperation();" class="btn btn-danger btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Stop&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      <div class="col-sm-4"><button id="chkby" disabled="disabled" onclick="checkedbyoperation();" class="btn btn-warning btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Shift Change &nbsp;&nbsp;&nbsp;&nbsp;</button>
      <span class="fa-stack mt-2 float-right" style="display:none;" name="FTR" id="FTR" data-flag="1" onclick="FTRshowtme();">
          <em class="fa fa-circle fa-stack-2x text-success bgred"></em>
          <em class="far fa-clock fa-stack-1x fa-inverse text-white"></em>
      </span>
      </div>
      <div class="col-sm-12 mt-2">

      <div class="card card-default" id="divftbl" style="display:none;">
               <div class="card-header p-5"><center>Activity Log Record</center></div>
               <table class="table table-dark" id="bdftbl">
                  <thead>
                     <tr tabindex=0>                        
                        <th scope="col">Start</th>
                        <th scope="col">End</th>
            <th scope="col">performed By</th>
            <th scope="col">Shift Change</th>
            <th scope="col">Activity</th>
                     </tr>
                  </thead>
                  <tbody>
                  
                  </tbody>
               </table>
            </div><!-- START row-->
      
      </div>
      
      </div>
      
        </div>
      </div>
    </div>
  </section>
  



  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 


 
<!--My Modal-->
<div class="modal fade show" id="myModal11" data-id="0"  style="display: none; padding-right: 17px;">
  <div class="modal-dialog modal-md">
    <div class="modal-content mt120">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelLarge">Submit memo number & remark</h4>
        <!--<button class="close" type="button" data-dismiss="modal" aria-label="Close" onclick="hidememo();"><span aria-hidden="true">×</span></button>-->   
      </div>
    
      <div class="modal-body">
    <div class="col-md-12">
                  <!-- START card-->
                  <div class="card card-default">
                     <div class="card-body">
                        <form>
                           <div class="form-group"><label>Memo Number</label><input name="memonumber" id="memonumber" class="form-control" type="text" placeholder="Enter Memo Number"></div>
                           <div class="form-group"><label>Remark</label><textarea name="memoremark" id="memoremark" class="form-control" row="3" placeholder="Enter Remark"></textarea></div>
                        </form>
                     </div>
           
            </div><!-- END card-->
        </div>    
      </div>
    <div class="modal-footer" style="justify-content: center;">       
      <div class="modal-button-container"><button type="button" onclick="hidememo();" class="btn btn-success btn-lg">Submit</button></div>
    </div>
    </div>
  </div>
</div>
<!--End My Modal-->
<div class="modal fade show" id="myModalLarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabelLarge" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelLarge">Add portable equipments in your equipment list</h4>
      </div>    
      <div class="modal-body">
        <section class="page-tasks" style="max-height: 250px;overflow-y: auto;">    
          <ul class="task-list list-unstyled" id="peqlist">
  <?php if($data['portequipment']->num_rows() > 0) { ?>
  <?php foreach($data['portequipment']->result() as $row) { if($row->equipment_type == "Portable") {?> 
      <li id="<?php echo 'pli' .$row->id ?>"><span class="view" id="<?php echo 'p' .$row->id ?>"><div class="row"><div class="col-sm-12 pl0"><div class="row">
  <div class="col-sm-10 listexecutesop1">
  <img class="mb-2 mt-2 wt42" src="<?php echo base_url(); ?>img/icons/<?php echo $row->equipment_icon ?>">    
    <span class="f15"><?php echo $row->equipment_name ?></span></div>
  <div class="col-md-2 float-right"><label class="switch switch-lg mt-2">
  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onchange="addpequip2();"  data-name="<?php echo $row->equipment_name ?>" data-eqcode="<?php echo $row->equipment_code ?>" data-sopcode="<?php echo $row->sop_code ?>" data-eqtype="<?php echo $row->equipment_type ?>" 
  <?php if($row->filepath != null && $row->filepath != "") { ?>
  data-sopdtl="<a href='#' data-file='<?php echo $row->filepath ?>' onclick='showmodel(this)'><?php echo $row->sop_code ?> <?php echo $row->sop_name ?></a>"
  <?php } else { ?>
  data-sopdtl="<?php echo $row->sop_code ?> <?php echo $row->sop_name ?>"
  <?php } ?>
  data-id="<?php echo $row->id ?>" data-imgurl="<?php echo base_url(); ?>img/icons/<?php echo $row->equipment_icon ?>" id="<?php echo 'check' .$row->id ?>" name="<?php echo 'check' .$row->id ?>">
  <span></span></label></div></div></div></div></span>
  </li><?php } } ?> 
<?php } else { ?>
    <div class="col-sm-12 listexecutesop1"><span class="f15">All the portable equipment are in use in another area or room</span></div>
  <?php } ?>
          </ul>
        </section></div>
    <div class="modal-footer" style="justify-content: center;">
      <div class="modal-button-container pl15"><button type="button" id="btnclose" data-dismiss="modal" aria-label="Close" class="btn btn-success btn-lg">Close</button><button type="button" id="btnsub" style="display: none;" onclick="addpequip('<?php echo $row->id ?>');" data-dismiss="modal" aria-label="Close" class="btn btn-success btn-lg">Submit</button></div>
    </div></div></div></div>

<script>
var id='';
function memo(obj)
{ 
  $("#myModal").show(1000);
  $("#myModal").attr("data-id", obj);
}
function hidememo()
{
  
  $("#myModal").hide(1000);
  var id = $("#myModal").attr("data-id");
  brkdownover(id);
  
}
</script>
<script>
      $("#proreg1 a").click(function () {
      debugger;
          if($(this).attr("data-contact")=="Contact")
          {
          //debugger;
            console.log('inside');
            if ($(this).attr("data-flag1") === "false") {
                //debugger;
                $("#proreg a").each(function (prop, val) {
                    var el = $(this).attr("data-proreg");
                    $("#" + el).hide(1000);
                    $(this).attr("data-flag1", "false");
                });

                var el = $(this).attr("data-proreg");
                $("#" + el).show(1000);
                $(this).attr("data-flag1", "true");
            }
            return false;
          }

        });
</script>

<script>
function ftrrshow()
{
  var docid = $("#docid").val();
  var pcode = $("#pcode").val();
  var pdesc = $("#pdesc").val();
  var bcode = $("#bcode").val();
  var departmentname = $("#departmentname").val();
  var market = $("#market").val();
  var UOM = $("#UOM").val();
  var bsizeUOM = $("#bsizeUOM").val();
  var operation_type = "Portable";
  var fixedequip = $("#fe").val();

  var postlist = {
       docid: docid,
       pcode: pcode,
       pdesc: pdesc,
       bcode: bcode,
       market: market,
       uom: UOM,
       bsizeuom: bsizeUOM,
       operation_type: operation_type,
       departmentname: departmentname
     };
  //postlist.push( );


  var myArray = [];
  i=1;
  $("#eqlist").find('input[name^="hddeqdtl"]').each(function () {
    var sopcode = $(this).attr("data-sopcode");
    var eqcode = $(this).attr("data-eqcode");
    var eqtype = $(this).attr("data-eqtype");
    var chgprt = $("#changepart"+i).val();
    myArray.push( {
       sopcode: sopcode,
       eqcode: eqcode,
       eqtype: eqtype,
       chgprt: chgprt
     });
    i++;
  });


  //x=1;
  $("#porteqlist").find('input[name^="hddeqdtl"]').each(function () {
  var sopcode = $(this).attr("data-sopcode");
  var eqcode = $(this).attr("data-eqcode");
  var eqtype = $(this).attr("data-eqtype");
  var cid = $(this).attr("data-id");
  var chgprt = $("#cpart1"+cid).val();
  
  myArray.push( {
     sopcode: sopcode,
     eqcode: eqcode,
     eqtype: eqtype,
     chgprt: chgprt
   });

  });


console.log(myArray);

var conform = confirm("Do You Really Want To Start This?"); 
	  if(conform == true)
	  {
		  //alert("value of fe is" +fixedequip);
		  
		  if(myArray.length>0)
		  {
			  

$.ajax({
      url: "<?= base_url()?>Production/portableEquipmentcleaningsubmit",
      type: 'POST',
      data: {postlist:postlist,myArray:myArray},
      success: function(details) {
          console.log(details);
          $("#myb").click();
          $("#head").html(details.msgheader);
          $("#message").html(details.msg);
          $("#disclose").attr("href","<?= base_url()?>Production/portableEquipmentcleaningviewnow/"+details.id);
          
          //alert(res);
          //console.log(res);
          //alert("Portable Equipment Cleaning is Succesfully Started");
          //window.location.href = "<?= base_url()?>Production/portableEquipmentcleaningviewnow/"+res.id;
          //location.reload(true);
          //$("#myModal2").hide();
          //$("#bdftbl tr[tabindex=0]").focus();
          //$("#divftbl").show(1000);
          //$("#FTR").show();
          //$("#sbbtn").attr("disabled", true);
          
          //window.location.href="<?= base_url()?>Production/operation";

      },

error :function(details) { // if fail then getting message
      // just in case posting your form failed
      alert( "Workflow for this activitymay not be defined or connectivity to the server may be lost. Please contact the administartor." );
    }}); 


}else
		 {
			alert("No Equipment to start this Cleaning ");
			return false;
		  }
		  }}

function ftrrshow22()
{
  alert("Hi..");
  var docid = $("#docid").val();
  var pcode = $("#pcode").val();
  var pdesc = $("#pdesc").val();
  var bcode = $("#bcode").val();
  var market = $("#market").val();
  var lotno = $("#lotno").val();
  var UOM = $("#UOM").val();
  var bsizeUOM = $("#bsizeUOM").val();
  var operation_type = "operation";


  var myArray = [];
$("#eqlist").find('input[name^="hddeqdtl"]').each(function () {
  var sopcode = $(this).attr("data-sopcode");
  var eqcode = $(this).attr("data-eqcode");
  var eqtype = $(this).attr("data-eqtype");
  myArray.push( {
     sopcode: sopcode,
     eqcode: eqcode,
     eqtype: eqtype
   });

});

$("#porteqlist").find('input[name^="hddeqdtl"]').each(function () {
  var sopcode = $(this).attr("data-sopcode");
  var eqcode = $(this).attr("data-eqcode");
  var eqtype = $(this).attr("data-eqtype");
  

  myArray.push( {
     sopcode: sopcode,
     eqcode: eqcode,
     eqtype: eqtype
   });

});

  //var pcode = $("#pcode").val();
  //var pcode = $("#pcode").val();
    $.ajax({
      url: "<?= base_url()?>Production/makeinitiallog",
      type: 'POST',
      data: {docid:docid,bcode:bcode,lotno:lotno,myArray:myArray,operation_type:operation_type},
      success: function(res) {
          //alert(res);
          
          alert("Operation is succesfully started...");
          location.reload(true);
          $("#myModal2").hide();
          $("#bdftbl tr[tabindex=0]").focus();
          $("#divftbl").show(1000);
          $("#FTR").show();
          $("#sbbtn").attr("disabled", true);
          
          //window.location.href="<?= base_url()?>Production/operation";
      }
    }); 
}

function stopoperation2()
{
    var bcode = $("#bcode").val();
    var lotno = $("#lotno").val();
    var operation_type = "operation";
    $.ajax({
      url: "<?= base_url()?>Production/makefinishlog",
      type: 'POST',
      data: {bcode:bcode,lotno:lotno,operation_type:operation_type},
      success: function(res) {
          alert(res);        
      }
    }); 
}

function checkedbyoperation2()
{
  var bcode = $("#bcode").val();
  var lotno = $("#lotno").val();
  var operation_type = "operation";
    $.ajax({
      url: "<?= base_url()?>Production/makechkbylog",
      type: 'POST',
      data: {bcode:bcode,lotno:lotno,operation_type:operation_type},
      success: function(res) {
          alert(res);        
      }
    }); 
}

function FTRshowtme()
{ 
debugger;
  if($("#FTR").attr("data-flag")=="0")
  {
    $("#FTR").attr("data-flag", "1");
    $("#divftbl").show(1000);
  }
  else
  {
    $("#FTR").attr("data-flag", "0");
    $("#divftbl").hide(1000);
    
  }
}
function showtme(obj)
{ 
debugger;
  if($("#t"+obj).attr("data-flag")=="0")
  {
    $("#t"+obj).attr("data-flag", "1");
    $("#divbdtbl"+obj).show(1000);
  }
  else
  {
    $("#t"+obj).attr("data-flag", "0");
    $("#divbdtbl"+obj).hide(1000);
    
  }
}
function araechange()
{
  var area = $("#areaddl").val();
  //$("#area").text(area);

  $.ajax({
    url: "<?= base_url()?>Production/getRoomnow",
    type: 'POST',
    data: {area:area},
    success: function(res) {
        //console.log(res);
        var inv = "<option value='' selected disabled>Select Room</option>";
        $.each(res, function (index, value) {
        inv = inv.concat("<option value='"+value.room_code+"'>"+value.room_code+"</option>");
        });
        $("#roomdd1").html(inv);

    }
});

}

function roomchange(){
  $("#peqlist").html();
  var room = $("#roomdd1").val();
  $.ajax({
    url: "<?= base_url()?>Production/getEqm",
    type: 'POST',
    data: {room:room},
    success: function(res) {
        console.log(res);
        
        var inv = "";
        $.each(res, function (index, value) {
        inv = inv.concat('<li id="pli'+value.id+'"><span class="view" id="p'+value.id+'"><div class="row"><div class="col-sm-12 pl0"><div class="row"><div class="col-sm-10 listexecutesop1"><img class="mb-2 mt-2 wt42" src="<?php echo base_url(); ?>img/icons/Sifter.png"><span class="f15">'+value.equipment_name+'</span></div><div class="col-md-2 float-right"><label class="switch switch-lg mt-2"><input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip('+value.id+');" data-name="'+value.equipment_name+'" data-eqcode="'+value.equipment_code+'" data-sopcode="'+value.sop_code+'" data-eqtype="'+value.equipment_type+'" data-sopdtl="'+value.sop_code+' '+value.sop_name+'" data-id="'+value.id+'" data-imgurl="<?php echo base_url(); ?>img/icons/Sifter.png" id="check'+value.id+'" name="check'+value.id+'"><span></span></label><input type ="hidden" id ="fe" name="fe" value="1"></div></div></div></div></span></li>');
        });
        




$("#peqlist").html(inv);




    }
});
}

function chngclr(obj,divid)
{
  debugger;
  var check = document.getElementById(obj);
    if(check.checked == true){
  document.getElementById(divid).style.backgroundColor = "#b0be3c";
  document.getElementById(divid).style.color = "white";
  }else{
    document.getElementById(divid).style.backgroundColor = "white";
    document.getElementById(divid).style.color = "#000";
    }
}
function showpequip()
{
debugger;
  $("#pli4").show(100);
  $("#pli5").show(200);
  $("#pli6").show(300);
  $("#showall").hide();
  $("#showless").show();
}
function hidepequip()
{
debugger;
  $("#pli4").hide(100);
  $("#pli5").hide(200);
  $("#pli6").hide(300);
  $("#showall").show();
  $("#showless").hide();
}
</script>
<script>
var dt='';
var dt1='';
var stt='';
var ft='';
var ct='';
function brkdown2(obj)
{
debugger; 
var memonumber = $("#memonumber").val();
  var memoremark = $("#memoremark").val();
  var sopcode = $("#hddeqdtl"+obj).attr("data-sopcode");
  var eqcode = $("#hddeqdtl"+obj).attr("data-eqcode");
  var eqtype = $("#hddeqdtl"+obj).attr("data-eqtype");
  var bdstarttime = "";
  var bdendtime = "";
  var bcode = $("#bcode").val();
  var lotno = $("#lotno").val();
  var operation_type = "operation";
  $.ajax({
      url: "<?= base_url()?>Production/makestartbrkdwnlog",
      type: 'POST',
      data: {sopcode:sopcode,eqcode:eqcode,eqtype:eqtype,memonumber:memonumber,memoremark:memoremark,bdstarttime:bdstarttime,bdendtime:bdendtime,bcode:bcode,lotno:lotno,operation_type:operation_type},
      success: function(res) {

    $("#btnbd"+obj).hide();
    $("#end"+obj).show();
    //$("#t"+obj).show();
    $("#fbtn").attr("disabled", true);
    $("#sbtn").hide();
    $("#rsbtn").show();
    $("#btnrs").attr("disabled", true);
    var dt = new Date();
    stt = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
    }
  });
}
function brkdownover2(obj)
{
  var dt1 = new Date();
  ft = dt1.getHours() + ":" + dt1.getMinutes() + ":" + dt1.getSeconds();
  var memonumber = $("#memonumber").val();
  var memoremark = $("#memoremark").val();
  var sopcode = $("#hddeqdtl"+obj).attr("data-sopcode");
  var eqcode = $("#hddeqdtl"+obj).attr("data-eqcode");
  var eqtype = $("#hddeqdtl"+obj).attr("data-eqtype");
  var bdstarttime = "";
  var bdendtime = "";
  var bcode = $("#bcode").val();
  var lotno = $("#lotno").val();
  var operation_type = "operation";
  $.ajax({
      url: "<?= base_url()?>Production/makeendbrkdwnlog",
      type: 'POST',
      data: {sopcode:sopcode,eqcode:eqcode,eqtype:eqtype,memonumber:memonumber,memoremark:memoremark,bdstarttime:bdstarttime,bdendtime:bdendtime,bcode:bcode,lotno:lotno,operation_type:operation_type},
      success: function(res) {
          //alert(res);
          //alert("Operation is succesfully started...");
          $("#btnbd"+obj).show();
          $("#end"+obj).hide();
          $("#t"+obj).show();
          $("#t"+obj).attr("data-flag", "1");
          $("#fbtn").attr("disabled", false);
          $("#sbtn").hide();
          $("#btnrs").attr("disabled", false);
          $("#divbdtbl"+obj).show(1000);
          //$("#bdtbl"+obj).html("");
          
          $("#bdtbl"+obj).append("<tr><td>"+stt+"</td><td>"+ft+"</td><td>00:00:00</td><td>Bhupendra kumar</td></tr>");
      }
    });
}
function addpequip2()
{
  var chknum = 0;
  $("#peqlist").find('input[name^="check"]').each(function () { 
  if($(this).prop('checked') == true)
  { 
    chknum++;
  }
  });
  if(chknum<1)
  {
    $("#btnsub").hide();
    $("#btnclose").show();
  }
  else
  {
    $("#btnsub").show();
    $("#btnclose").hide();
  }
}
function addpequip()
{
      var r = confirm("Do You Want To Save This Record?");
      if(r == true)
      {
         $("#porteqlist").html("");
         var newRow = $("<li>");
         $("#peqlist").find('input[name^="check"]').each(function () { var i=0;  
           if($(this).prop('checked') == true)
           {  
            i++;
              var name = $(this).attr("data-name");
              var imgurl = $(this).attr("data-imgurl");
              var counter = $(this).attr("data-id");
              var sopdtl = $(this).attr("data-sopdtl");
              var sopcode = $(this).attr("data-sopcode");
              var eqcode = $(this).attr("data-eqcode");
              var eqtype = $(this).attr("data-eqtype");
              var cols = "";
  cols += "<input type='hidden' name='hddeqdtl"+i+"' id='hddeqdtl"+i+"' data-id='"+counter+"' data-eqcode='"+eqcode+"' data-sopcode='"+sopcode+"' data-eqtype='"+eqtype+"' /> ";
  cols += "<span class='view' id='eqdiv"+counter+"'><div class='row'><div class='col-sm-12 pl0'><div class='row'>";
    cols += "<div class='col-sm-4 listexecutesop2'><img class='mb-2 mt-2 wt42' src='"+imgurl+"'><span class='f15'>"+name+"</span></div>";
  cols += "<div class='col-sm-4 mt-4'><p class='f15 mt-2'>"+sopdtl+"</p></div>";
  cols += "<div class='col-sm-4 mt-4'><p class='f15 mt-2'><input type='text' name='cpart1"+counter+"' id='cpart1"+counter+"' class='form-control' placeholder='Change part'></p></div>";
  cols += "</div></div></div></span></li>";
  newRow.append(cols);     
             }
           });
           $("#porteqlist").append(newRow);
        }
}
</script>
<script src="<?php echo base_url(); ?>js/app.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>

<!--
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
-->

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="head"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="message"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <a href="" class="btn btn-primary" id="disclose">Ok</a>
      </div>
    </div>

  </div>
</div>
</body>
</html>