  <!-- Main section-->
<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <?php if($this->session->userdata('activity_code') == 2) { ?>
             <div class="col-sm-5 pl-0">Type A Cleaning</div>
            <?php } else { ?>
             <div class="col-sm-5 pl-0">Type B Cleaning</div>
            <?php } ?>
       
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/area"><?php echo $this->session->userdata('area_code') ?></a></li>
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/room?area=<?php echo $this->session->userdata('area_code') ?>"><?php echo $this->session->userdata('room_code') ?></a></li>
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/activity?room=<?php echo $this->session->userdata('room_code') ?>"><?php echo $this->session->userdata('activity_code') ?></a></li>
      <li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>Production/batch?activitycode=<?php echo $this->session->userdata('activity_code') ?>"><?php echo $this->session->userdata('bcode') ?></a></li>
          </ol>
        </div>
      </div>
	  
    <div class="card card-default">
      <div class="card-body">
      <div class="row">
      <div class="col-sm-4">
      <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/area-operation-page.png"><b>Area : </b><?php echo $this->session->userdata('area_code') ?>
      </div>
      </div>      
      <div class="col-sm-4">
      <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/area-operation-page.png"><b>Room : </b><?php echo $this->session->userdata('room_code') ?></div>
      </div>
      </div>
    </div>
  </div>

	  <div class="card card-default">
        <div class="card-body">
			<div class="row">
      <div class="col-sm-4">
      <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/document-icon.png"><b>Document No. : </b><?php echo $data['docid'] ?><input type="hidden" name="docid" id="docid" value="<?php echo $data['docid'] ?>" /></div>
      </div>
      <div class="col-sm-4">
      <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/department.png"><b>Department Name : </b><?php echo $data['departmentname'] ?></div>
      </div>
      </div>
        </div>
      </div>
	  
      <div class="card card-default">
        <div class="card-body">
          <div class="row mt-4">
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/code-p.png"><b>Product code : </b><?php echo $this->session->userdata('pcode'); ?><input type="hidden" name="pcode" id="pcode" value="<?php echo $this->session->userdata('pcode'); ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/mdicine-red.png"><b>Description : </b><?php echo $this->session->userdata('pdesc'); ?><input type="hidden" name="pdesc" id="pdesc" value="<?php echo $this->session->userdata('pdesc'); ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/market.png"><b>Market : </b><?php echo $this->session->userdata('market'); ?><input type="hidden" name="market" id="market" value="<?php echo $this->session->userdata('market'); ?>" /></div>
            </div>
          </div>
          <div class="row mt-4 mb-2">
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/code-bred.png"><b>Batch : </b><?php echo $this->session->userdata('bcode'); ?><input type="hidden" name="bcode" id="bcode" value="<?php echo $this->session->userdata('bcode'); ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/mg-red.png"><b>Unit : </b><?php echo $this->session->userdata('UOM'); ?><input type="hidden" name="UOM" id="UOM" value="<?php echo $this->session->userdata('UOM'); ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/40-red.png"><b>Batch size : </b><?php echo $this->session->userdata('bsizeUOM'); ?><input type="hidden" name="bsizeUOM" id="bsizeUOM" value="<?php echo $this->session->userdata('bsizeUOM'); ?>" /></div>
            </div>
          </div>
        </div>
      </div>
      <div class="card card-default">
        <div class="card-body">
          <div class="row mb-3">
		  <div class="col-sm-12">		  
              <div><span class="ml18 float-left pt11"><h4>Equipments list of Granulation</h4></span></div>
			  <div class="float-right"><span class="ml18 float-left pt11 mr15"><h4>Add portable equipments </h4></span><i data-toggle="modal" data-target="#myModalLarge" class="fa fa-plus default-icon-color default-icon-style float-right" aria-hidden="true"></i></div>
            </div>
		  <div class="modal-body">
                    <section class="page-tasks" id="Listofequip">
          <ul class="task-list list-unstyled" id="eqlist">
            <?php $counter=0; foreach($data['fixequipment']->result() as $row) { if($row->equipment_type == "Fixed") { $counter++; ?> 
  <li id='<?php echo "eqli" .$row->id ?>'> 
  <input type="hidden" name='<?php echo "hddeqdtl" .$row->id ?>' id='<?php echo "hddeqdtl" .$row->id ?>' data-id='<?php echo $row->id ?>' data-eqcode='<?php echo $row->equipment_code ?>' data-sopcode='<?php echo $row->sop_code ?>' data-eqtype='<?php echo $row->equipment_type ?>' />
  <span class="view" id='<?php echo "eqdiv" .$row->id ?>'><div class="row"><div class="col-sm-12 pl0"><div class="row">
  <div class="col-sm-4 listexecutesop2"><img class="mb-2 mt-2 wt42" src="<?php echo base_url(); ?>img/icons/<?php echo $row->equipment_icon ?>"><span class="f15"><?php echo $row->equipment_name ?></span></div>
  <div class="col-sm-8 mt-4"><p class="f15 mt-2"><?php echo $row->sop_code ?> <?php echo $row->sop_name ?></p></div>  
  </div>
  </div>
  </div>
  </span> 
  </li>
  <?php } } ?>
            
          </ul>
		  <ul class="task-list list-unstyled" id="porteqlist">
		  </ul>
        </section>
		</form>
		</div>
          </div>		
		  
		  <div class="row disable-button-color">
			<div class="col-sm-4 text-right" id="sbtn"><span id="demo3" class="pr-2 f16"></span><button id="sbbtn" class="btn btn-success btn-lg" type="button" onClick="ftrrshow();">&nbsp;&nbsp;&nbsp;&nbsp;Start&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
			<div class="col-sm-4 text-right" id="rsbtn" style="display:none;"><span id="demo3" class="pr-2 f16"></span><button id="btnrs" class="btn btn-success btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Restart&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
			<div class="col-sm-4"><button id="fbtn" disabled="disabled" onclick="stopoperation();" class="btn btn-danger btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Stop&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      <div class="col-sm-4"><button id="chkby" disabled="disabled" onclick="checkedbyoperation();" class="btn btn-warning btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Shift Change&nbsp;&nbsp;&nbsp;&nbsp;</button>
			<span class="fa-stack mt-2 float-right" style="display:none;" name="FTR" id="FTR" data-flag="1" onclick="FTRshowtme();">
				  <em class="fa fa-circle fa-stack-2x text-success bgred"></em>
				  <em class="far fa-clock fa-stack-1x fa-inverse text-white"></em>
			</span>
			</div>
			<div class="col-sm-12 mt-2">

			<div class="card card-default" id="divftbl" style="display:none;">
               <div class="card-header p-5"><center>Activity Log Record</center></div>
               <table class="table table-dark" id="bdftbl">
                  <thead>
                     <tr tabindex=0>                        
                        <th scope="col">Start</th>
                        <th scope="col">End</th>
						<th scope="col">Performed By</th>
						<th scope="col">Shift Change</th>
						<th scope="col">Activity</th>
                     </tr>
                  </thead>
                  <tbody>
          				
                  </tbody>
               </table>
            </div><!-- START row-->
			
			</div>
			
		  </div>
		  
        </div>
      </div>
    </div>
  </section>
  
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="js/app.js"></script> 

<!-- Modal Large-->
<div class="modal fade" id="myModalLarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabelLarge" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelLarge">Add portable euipments in your euipment list</h4>
        <!--<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
      </div>
    
      <div class="modal-body">
    <ul class="task-list list-unstyled">
    
        <li class="bg-white pt10 pb10 pl15">
        <span><b>Area :</b> </span><span class="col-sm-3 pl-0" id="area"></span>
        <select class="custom-select custom-select-sm col-sm-3" id="areaddl" onchange="araechange();">
          <option selected="">Select Another Area</option>
          <?php
          foreach ($data["area"] as $a) {
            if($a["area_code"]!== $this->session->userdata("area_code")){
            ?>
            <option value="<?= $a["area_code"]?>"><?= $a["area_name"]?></option>
            <?php
            }
          }
          ?>   
        
        </select>

        <select class="custom-select custom-select-sm col-sm-3" id="roomdd1" onchange="roomchange();">
          <option selected="">Select Room</option>
           
        
        </select>
                              <button class="btn btn-success col-sm-2 mr-2" type="button" value="Show all" onclick="showpequip();" name="showall" id="showall" style="float:right;">Show All</button>
                <button class="btn btn-success col-sm-2 mr-2" style="display:none;float:right;" type="button" value="Show less" onclick="hidepequip();" name="showless" id="showless">Show less</button>
                              
        </li>

        </ul>
        <section class="page-tasks">
    
          <ul class="task-list list-unstyled" id="peqlist">
  <?php foreach($data['fixequipment']->result() as $row) { if($row->equipment_type == "Portable") {?> 
      <li id="<?php echo 'pli' .$row->id ?>"><span class="view" id="<?php echo 'p' .$row->id ?>"><div class="row"><div class="col-sm-12 pl0"><div class="row">
  <div class="col-sm-10 listexecutesop1">
  <img class="mb-2 mt-2 wt42" src="<?php echo base_url(); ?>img/icons/<?php echo $row->equipment_icon ?>">    
    <span class="f15"><?php echo $row->equipment_name ?></span></div>
  <div class="col-md-2 float-right"><label class="switch switch-lg mt-2">
  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip('<?php echo $row->id ?>');" data-name="<?php echo $row->equipment_name ?>" data-eqcode="<?php echo $row->equipment_code ?>" data-sopcode="<?php echo $row->sop_code ?>" data-eqtype="<?php echo $row->equipment_type ?>" data-sopdtl="<?php echo $row->sop_code ?> <?php echo $row->sop_name ?>" data-id="<?php echo $row->id ?>" data-imgurl="<?php echo base_url(); ?>img/icons/Sifter.png" id="<?php echo 'check' .$row->id ?>" name="<?php echo 'check' .$row->id ?>">
  <span></span></label></div>
    </div></div></div></span>
  </li>
  <?php } } ?>
  
          </ul>
        </section>
      </div>
    <div class="modal-footer" style="justify-content: center;">
      <div class="modal-button-container" id="cancel"><button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger btn-lg bg-danger">Cancel</button></div>
      <div class="modal-button-container pl15"><button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-success btn-lg">Insert</button></div>
    </div>
    </div>
  </div>
</div>

<script>
function ftrrshow()
{
  debugger;
  var r = confirm("Do you really want to start this ?");
  if(r == true)
  {
  var docid = $("#docid").val();
  var pcode = $("#pcode").val();
  var pdesc = $("#pdesc").val();
  var bcode = $("#bcode").val();
  var market = $("#market").val();
  var lotno = "";
  var UOM = $("#UOM").val();
  var bsizeUOM = $("#bsizeUOM").val();
  var operation_type = '<?php echo $this->session->userdata('activityType'); ?>';

  var myArray = [];
$("#eqlist").find('input[name^="hddeqdtl"]').each(function () {
  var sopcode = $(this).attr("data-sopcode");
  var eqcode = $(this).attr("data-eqcode");
  var eqtype = $(this).attr("data-eqtype");
  myArray.push( {
     sopcode: sopcode,
     eqcode: eqcode,
     eqtype: eqtype
   });

});
$("#porteqlist").find('input[name^="hddeqdtl"]').each(function () {
  var sopcode = $(this).attr("data-sopcode");
  var eqcode = $(this).attr("data-eqcode");
  var eqtype = $(this).attr("data-eqtype");
  myArray.push( {
     sopcode: sopcode,
     eqcode: eqcode,
     eqtype: eqtype
   });

});

  //var pcode = $("#pcode").val();
  //var pcode = $("#pcode").val();
    $.ajax({
      url: "<?= base_url()?>Production/makeinitiallog",
      type: 'POST',
      data: {docid:docid,bcode:bcode,lotno:lotno,myArray:myArray,operation_type:operation_type},
      success: function(res) {
          //alert(res);
          //alert("Operation is succesfully started...");
          //location.reload(true);

          $("#Msgmyb").click();
          $("#Msghead").html("Start");
          $("#Msgmessage").html("operation is started successfully.");

          $("#myModal2").hide();
          $("#bdftbl tr[tabindex=0]").focus();
          $("#divftbl").show(1000);
          $("#FTR").show();
          $("#sbbtn").attr("disabled", true);
          
          //window.location.href="<?= base_url()?>Production/operation";
      }
    });
  }	
}

function stopoperation()
{
    var bcode = $("#bcode").val();
    var lotno = "";
    var operation_type = '<?php echo $this->session->userdata('activityType'); ?>';
    $.ajax({
      url: "<?= base_url()?>Production/makefinishlog",
      type: 'POST',
      data: {bcode:bcode,lotno:lotno,operation_type:operation_type},
      success: function(res) {
          alert(res);        
      }
    }); 
}

function checkedbyoperation()
{
    var bcode = $("#bcode").val();
    var lotno = "";
    var operation_type = '<?php echo $this->session->userdata('activityType'); ?>';
    $.ajax({
      url: "<?= base_url()?>Production/makechkbylog",
      type: 'POST',
      data: {bcode:bcode,lotno:lotno,operation_type:operation_type},
      success: function(res) {
          alert(res);        
      }
    }); 
}

function FTRshowtme()
{	
debugger;
	if($("#FTR").attr("data-flag")=="0")
	{
		$("#FTR").attr("data-flag", "1");
		$("#divftbl").show(1000);
	}
	else
	{
		$("#FTR").attr("data-flag", "0");
		$("#divftbl").hide(1000);
		
	}
}
function showtme(obj)
{	
debugger;
	if($("#t"+obj).attr("data-flag")=="0")
	{
		$("#t"+obj).attr("data-flag", "1");
		$("#divbdtbl"+obj).show(1000);
	}
	else
	{
		$("#t"+obj).attr("data-flag", "0");
		$("#divbdtbl"+obj).hide(1000);
		
	}
}

function disabledpopup()
{
location.reload(true);
}

function araechange()
{
  var area = $("#areaddl").val();
  //$("#area").text(area);

  $.ajax({
    url: "<?= base_url()?>Production/getRoomnow",
    type: 'POST',
    data: {area:area},
    success: function(res) {
        //console.log(res);
        var inv = "<option value='' selected disabled>Select Room</option>";
        $.each(res, function (index, value) {
        inv = inv.concat("<option value='"+value.room_code+"'>"+value.room_code+"</option>");
        });
        $("#roomdd1").html(inv);

    }
});

}

function roomchange(){
  $("#peqlist").html();
  var room = $("#roomdd1").val();
  $.ajax({
    url: "<?= base_url()?>Production/getEqm",
    type: 'POST',
    data: {room:room},
    success: function(res) {
        console.log(res);
        
        var inv = "";
        $.each(res, function (index, value) {
        inv = inv.concat('<li id="pli'+value.id+'"><span class="view" id="p'+value.id+'"><div class="row"><div class="col-sm-12 pl0"><div class="row"><div class="col-sm-10 listexecutesop1"><img class="mb-2 mt-2 wt42" src="<?php echo base_url(); ?>img/icons/'+value.equipment_icon+'"><span class="f15">'+value.equipment_name+'</span></div><div class="col-md-2 float-right"><label class="switch switch-lg mt-2"><input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip('+value.id+');" data-name="'+value.equipment_name+'" data-eqcode="'+value.equipment_code+'" data-sopcode="'+value.sop_code+'" data-eqtype="'+value.equipment_type+'" data-sopdtl="'+value.sop_code+' '+value.sop_name+'" data-id="'+value.id+'" data-imgurl="<?php echo base_url(); ?>img/icons/'+value.equipment_icon+'" id="check'+value.id+'" name="check'+value.id+'"><span></span></label></div></div></div></div></span></li>');
        });
        




$("#peqlist").html(inv);




    }
});
}

</script>
<script>
var dt='';
var dt1='';
var stt='';
var ft='';
var ct='';

function addpequip()
{
	debugger;
	$("#porteqlist").html("");
	var newRow = $("<li>");
	//counter = 6;
	$("#peqlist").find('input[name^="check"]').each(function () {	
	if($(this).prop('checked') == true)
	{	
	var name = $(this).attr("data-name");
	var imgurl = $(this).attr("data-imgurl");
  var counter = $(this).attr("data-id");
  var sopdtl = $(this).attr("data-sopdtl");
  var sopcode = $(this).attr("data-sopcode");
  var eqcode = $(this).attr("data-eqcode");
  var eqtype = $(this).attr("data-eqtype");
  //var counter++;
    var cols = "";
	cols += "<input type='hidden' name='hddeqdtl"+counter+"' id='hddeqdtl"+counter+"' data-id='"+counter+"' data-eqcode='"+eqcode+"' data-sopcode='"+sopcode+"' data-eqtype='"+eqtype+"' /> ";
	cols += "<span class='view' id='eqdiv"+counter+"'><div class='row'><div class='col-sm-12 pl0'><div class='row'>";
    cols += "<div class='col-sm-4 listexecutesop2'><img class='mb-2 mt-2 wt42' src='"+imgurl+"'><span class='f15'>"+name+"</span></div>";
	cols += "<div class='col-sm-8 mt-4'><p class='f15 mt-2'>"+sopdtl+"</p></div></div></div></div></span></li>";
    newRow.append(cols);      
	}
	
	});
	$("#porteqlist").append(newRow);
}
</script>
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myMsgModal" hidden id="Msgmyb" data-backdrop="static" data-keyboard="false"></button>

<div id="myMsgModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="Msghead"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="Msgmessage"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <button class="btn btn-success" id="yes" onclick="disabledpopup();">ok</button>
      </div>
    </div>

  </div>
</div>
</body>
</html>