<!-- Main section-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-3 pl-0">Select activity</div>
        <form class="search-form col-sm-5 pl-0">
          <!--<em class="icon-magnifier"></em>
          <input class="form-control" type="email" placeholder="Enter room code or number on name">-->
        </form>
        <div class="col-sm-4 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/area"><?php echo $this->session->userdata('area_code') ?></a></li>
			<li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>Production/room?area=<?php echo $this->session->userdata('area_code') ?>"><?php echo $this->session->userdata('room_code') ?></a></li>
            <!--<li class="breadcrumb-item">Execute SOP</li>-->
          </ol>
        </div>
      </div>
          <div class="row mt-14">
          <?php $this->session->set_userdata('room_code', $data['room_code']); ?>        
			     <?php foreach($data['activity']->result() as $row) { ?> 
            <div class="col-sm-3 text-center first-bg-color mt-1" id="show-hexagoan"> <a onclick="chkcleaning('<?php echo $row->activity_code ?>','<?php echo base_url(); ?>Production/batch?activitycode=<?php echo $row->activity_code ?>')" class="roomhover">
              <div class="rectangle-box"><img src="<?php echo base_url(); ?>img/<?php echo $row->activity_icon ?>">
                <p><?php echo $row->activity_name ?></p>
              </div>
              </a> </div>
            <?php } ?>			
          </div>          		  
          <!--<div class="row mt30 mb-5">
            <div class="col-sm-12 text-center"> <a href="<?php echo base_url(); ?>#"><img src="<?php echo base_url(); ?>img/down-arrow.png" width="50" class="bounce"></a> </div>
          </div>-->
    </div>
  </section>
<!-- Page footer-->
<script type="text/javascript">
function chkcleaning(code,link)
{
  if(code=="1")
  {
  $.ajax({
    url: "<?= base_url()?>Production/chkcleaning",
    type: 'POST',
    data: {},
    success: function(res) {
        console.log(res);
        if(res>=0 && res<=48)
        {
            window.location.href="<?php echo base_url(); ?>Production/batch?activitycode="+code;
        }
        else
        {
            alert("Cleaning has been over 48 hours");
        }
    }
});
}
else
{
  window.location.href="<?php echo base_url(); ?>Production/batch?activitycode="+code;
}

}
</script>