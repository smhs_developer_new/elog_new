  <!-- Main section-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-3 pl-0">Select Area</div>
        
          
          <input class="form-control autocomplete" list="arealist" id="areacodetxt" type="text" placeholder="Enter area code">
          <datalist id="arealist">
            <?php //foreach($array as $ar) { ?>
            <?php foreach($area->result() as $ar) { ?>  
                <option value="<?php echo $ar->area_code."/".$ar->area_name ?>">
            <?php } ?>
          </datalist>
        
        <div class="col-sm-4 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <!--<li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>area.html">Granulation</a></li>
            <li class="breadcrumb-item">Execute SOP</li>-->
          </ol>
        </div>
      </div>
      <form action="#" method="post" id="submitform" enctype="multipart/form-data">
      <div class="row mt-14" id="areadiv">
        <?php foreach($area->result() as $row) { ?> 
        <div class="col-sm-3 text-center first-bg-color mt-1"> <a href="<?php echo base_url() ?>Production/room?area=<?php echo $row->area_code ?>" class="roomhover" id="<?php echo "area" .$row->id ?>"  data-areacode="<?php echo $row->area_code ?>">
          <div class="rectangle-box"><img class="door" src="<?php echo base_url(); ?>img/area-w90px.png"><img onclick="aad();" class="opendoor" src="<?php echo base_url(); ?>img/area-w90px.png">
            <p><?php echo $row->area_name ?></p>
          </div>
          </a> </div>
        <?php } ?>        
      </div>
    </form>
      <!--<div class="row mt50">
        <div class="col-sm-12 text-center"> <a href="<?php echo base_url(); ?>#"><img src="<?php echo base_url(); ?>img/down-arrow.png" width="50" class="bounce"></a> </div>
      </div>-->
    </div>
  </section>

<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script>
$(document).ready(function()
{   
  if($("#areacodetxt").val()!="" && $("#areacodetxt").val()!=null && $("#areacodetxt").length>0)
  {
    getarea();
  }

  $(".autocomplete").change(function() 
  {
        getarea();
  }); 

  $(".autocomplete").keyup(function() 
  {
    debugger;
    var arcode = $(this).val();
    var myarray = [];
    if(arcode!="" && arcode.length>=1){  
      /*$.ajax({
        url: "<?= base_url()?>Production/areacode",
        type: 'POST',
        data: {arcode:arcode},
        success: function(res) {
          debugger;
          console.log(res);
            for(i=0;i<res.length;i++)
            {
               var ac=res[i].split("/");
               var empareas = "<?php echo $this->session->userdata('emparea_code'); ?>";
               var empareaarray = empareas.split(",");
                  for(j=0; j<empareaarray.length; j++)
                  {
                        if(empareaarray[j]==ac[0])
                        {
                           myarray.push(res[i]);
                           //return false;
                        }
                  }
            }

          availableTags = myarray;   
          $(".autocomplete").autocomplete({
          source: availableTags
          });
        }
      });*/
    }
    else
    {
      location.reload(true);
    }
  });
});

function getarea()
{
  var areacode = $("#areacodetxt").val();
    var ararray = areacode.split("/");
    var flag=0;
    var inv="";
    arcode = ararray[0];
    $.ajax({
      url: "<?= base_url()?>Production/areadata",
      type: 'POST',
      data: {arcode:arcode},
      success: function(res) {
          console.log(res);
      var empareas = "<?php echo $this->session->userdata('emparea_code'); ?>";
      var empareaarray = empareas.split(",");
      for(i=0; i<empareaarray.length; i++)
      {
        if(empareaarray[i]==arcode)
        {
            flag=1;
        }
      }
          if(flag==1)
          {
            inv = inv.concat('<div class="col-sm-3 text-center first-bg-color mt-1"> <a href="<?php echo base_url() ?>Production/room?area='+res.area_code+'" class="roomhover" id="area'+res.id+'"  data-areacode="'+res.area_code+'"><div class="rectangle-box"><img class="door" src="<?php echo base_url(); ?>img/area-w90px.png"><img onclick="aad();" class="opendoor" src="<?php echo base_url(); ?>img/area-w90px.png"><p>'+res.area_name+'</p></div></a></div>');
          }
          else
          {
              inv = inv.concat('<div class="col-sm-3 text-center first-bg-color mt-1"> <div class="rectangle-box"><p>There is no area at this code, please enter a valid area code</p></div></a></div>');
          }
        $("#areadiv").html(inv);
      }
    });
}
</script>
<script>
function gotoroom(obj)
{
  var area_code = $("#area"+obj).attr("data-areacode");
    $.ajax({
      url: "<?= base_url()?>Production/room",
      type: 'POST',
      data: {area_code:area_code},
    });
}
</script>
