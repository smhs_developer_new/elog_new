<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="Bootstrap Admin App">
<meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
<link rel="icon" type="image/x-icon" href="favicon.ico">
<title>SMHS - eLog System</title>
<!-- =============== VENDOR STYLES ===============-->
<!-- FONT AWESOME-->
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/brands.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/regular.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/solid.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/fontawesome.css">
<!-- SIMPLE LINE ICONS-->
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/simple-line-icons/css/simple-line-icons.css">
<!-- ANIMATE.CSS-->
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/animate.css/animate.css">
<!-- WHIRL (spinners)-->
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/whirl/dist/whirl.css">
<!-- =============== PAGE VENDOR STYLES ===============-->
<!-- =============== BOOTSTRAP STYLES ===============-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css" id="bscss">
<!-- =============== APP STYLES ===============-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/app.css" id="maincss">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
</head>

<body>
<div class="wrapper"> 
  <!-- top navbar-->
  <header class="topnavbar-wrapper"> 
    <!-- START Top Navbar-->
    <nav class="navbar topnavbar"> 
      <!-- START navbar header-->
      <div class="navbar-header"><a class="navbar-brand" href="logo">
        <div class="brand-logo font-weight-bold text-white">SMHS - eLog System</div>
        <div class="brand-logo-collapsed font-weight-bold text-white">SMHS</div>
        </a></div>
      <!-- END navbar header--> 
      <!-- START Left navbar-->
      <ul class="navbar-nav mr-auto flex-row">
        <li class="nav-item"> 
          <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link d-none d-md-block d-lg-block d-xl-block" href="#" data-trigger-resize="" data-toggle-state="aside-collapsed"><em class="fas fa-bars"></em></a><!-- Button to show/hide the sidebar on mobile. Visible on mobile only.--><a class="nav-link sidebar-toggle d-md-none" href="#" data-toggle-state="aside-toggled" data-no-persist="true"><em class="fas fa-bars"></em></a></li>
        <!-- START User avatar toggle-->
        <li class="nav-item d-none d-md-block"> 
          <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link" id="user-block-toggle" href="#user-block" data-toggle="collapse"><em class="icon-user"></em></a></li>
        <!-- END User avatar toggle--> 
        <!-- START lock screen-->
        <li class="nav-item d-none d-md-block"><a class="nav-link" href="#" title="Lock screen"><em class="icon-lock"></em></a></li>
        <!-- END lock screen-->
      </ul>
      <!-- END Left navbar--> 
      <!-- START Right Navbar-->
      <ul class="navbar-nav flex-row">
        <!-- Fullscreen (only desktops)-->
        <li class="nav-item d-none d-md-block"><a class="nav-link" href="#" data-toggle-fullscreen=""><em class="fas fa-expand"></em></a></li>
        <!-- START Offsidebar button-->
        <li class="nav-item"><a href="#" class="nav-link"></a></li>
        <li class="dropdown langs text-normal ng-scope" uib-dropdown="" is-open="status.isopenLang" data-ng-controller="LangCtrl" style=""> <a href="javascript:;" class="dropdown-toggle  dropdown-toggleactive-flag" uib-dropdown-toggle="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <div class="flag flags-american" style=""></div>
          </a>
          <ul class="dropdown-menu with-arrow  pull-right list-langs dropdown-menu-scaleIn animated flipInX" role="menu">
            <li data-ng-show="lang !== 'English' " aria-hidden="true" class="ng-hide" style=""> <a href="javascript:;" data-ng-click="setLang('English')">
              <div class="flag flags-american"></div>
              English</a></li>
            <li data-ng-show="lang !== 'Español' " aria-hidden="false" class="" style=""> <a href="javascript:;" data-ng-click="setLang('Español')">
              <div class="flag flags-spain"></div>
              Español</a></li>
            <li data-ng-show="lang !== 'Portugal' " aria-hidden="false" class="" style=""> <a href="javascript:;" data-ng-click="setLang('Portugal')">
              <div class="flag flags-portugal"></div>
              Portugal</a></li>
            <li data-ng-show="lang !== '中文' " aria-hidden="false" class=""> <a href="javascript:;" data-ng-click="setLang('中文')">
              <div class="flag flags-china"></div>
              中文</a></li>
            <li data-ng-show="lang !== '日本語' " aria-hidden="false" class="" style=""> <a href="javascript:;" data-ng-click="setLang('日本語')">
              <div class="flag flags-japan"></div>
              日本語</a></li>
            <li data-ng-show="lang !== 'Русский язык' " aria-hidden="false" class="" style=""> <a href="javascript:;" data-ng-click="setLang('Русский язык')">
              <div class="flag flags-russia"></div>
              Русский язык</a></li>
          </ul>
        </li>
        
        <!-- END Offsidebar menu-->
      </ul>
      <!-- END Right Navbar--> 
      <!-- START Search form-->
      <form class="navbar-form" role="search" action="search.html">
        <div class="form-group">
          <input class="form-control" type="text" placeholder="Type and hit enter ...">
          <div class="fas fa-times navbar-form-close" data-search-dismiss=""></div>
        </div>
        <button class="d-none" type="submit">Submit</button>
      </form>
      <!-- END Search form--> 
    </nav>
    <!-- END Top Navbar--> 
  </header>
  <!-- sidebar-->
  <aside class="aside-container"> 
    <!-- START Sidebar (left)-->
    <div class="aside-inner">
      <nav class="sidebar" data-sidebar-anyclick-close=""> 
        <!-- START sidebar nav-->
        <ul class="sidebar-nav">
          <!-- START user info-->
          <li class="has-user-block">
            <div class="collapse" id="user-block">
              <div class="item user-block"> 
                <!-- User picture-->
                <div class="user-block-picture">
                  <div class="user-block-status"><img class="img-thumbnail rounded-circle" src="img/user/02.jpg" alt="Avatar" width="60" height="60">
                    <div class="circle bg-success circle-lg"></div>
                  </div>
                </div>
                <!-- Name and Job-->
                <div class="user-block-info"><span class="user-block-name">Hello, Mike</span><span class="user-block-role">Designer</span></div>
              </div>
            </div>
          </li>
          <!-- END user info--> 
          <!-- Iterates over all sidebar items-->
          <li class="nav-heading text-bold"><span data-localize="sidebar.heading.HEADER">Main Menu</span></li>
          <li ><a href="dashboard.html" title="Plant master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Plant master">My Tasks</span></a></li>
          <li class=" active"><a href="area.html" title="Plant master"><em class="fas fa-cogs"></em><span data-localize="sidebar.nav.Plant master">Execute SOP</span></a></li>
          <li><a href="#submenu" title="Menu" data-toggle="collapse"><em class="fas fa-chalkboard-teacher"></em><span data-localize="sidebar.nav.menu.MENU">Master Data</span></a>
            <ul class="sidebar-nav sidebar-subnav collapse" id="submenu">
              <li class="sidebar-subnav-header">Menu</li>
              <li><a href="#" title="Plant master"><span data-localize="sidebar.nav.Plant master">Plant master</span></a></li>
              <li><a href="#" title="Block master"><span data-localize="sidebar.nav.Plant master">Block master</span></a></li>
              <li><a href="#" title="Area master"><span data-localize="sidebar.nav.Plant master">Area master</span></a></li>
              <li><a href="#" title="Rooms master"><span data-localize="sidebar.nav.Plant master">Rooms master</span></a></li>
              <li><a href="#" title="SOP master"><span data-localize="sidebar.nav.Plant master">SOP master</span></a></li>
              <li><a href="#" title="Product master"><span data-localize="sidebar.nav.Plant master">product master</span></a></li>
              <li><a href="#" title="Critical Parts master"><span data-localize="sidebar.nav.Critical Parts master">Critical Parts master</span></a></li>
              <li><a href="#" title="Accessory Parts"><span data-localize="sidebar.nav.Accessory Parts">Accessory Parts</span></a></li>
              <li><a href="#" title="Equipments master"><span data-localize="sidebar.nav.Equipments master">Equipments master</span></a></li>
              <li><a href="#" title="Employee master"><span data-localize="sidebar.nav.Employee master">Employee master</span></a></li>
              <li><a href="#" title="Holiday master"><span data-localize="sidebar.nav.Holiday master">Holiday master</span></a></li>
            </ul>
          </li>
        </ul>
        <!-- END sidebar nav--> 
      </nav>
    </div>
    <!-- END Sidebar (left)--> 
  </aside>
  <!-- offsidebar-->
  <!-- Main section-->
<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Production</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="Home2.html">Home</a></li>
            <li class="breadcrumb-item"><a href="area2.html">Granulation</a></li>
            <li class="breadcrumb-item"><a href="room.html">TAB-32</a></li>
			      <li class="breadcrumb-item"><a href="SelectActivity.html">Operation</a></li>
            <li class="breadcrumb-item active"><a href="batch.html">B2101</a></li>
          </ol>
        </div>
      </div>
	  
	  <div class="card card-default">
        <div class="card-body">
			<div class="row">
      <div class="col-sm-4">
      <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/document-icon.png"><?php echo $data['docid'] ?><input type="hidden" name="docid" id="docid" value="<?php echo $data['docid'] ?>" /></div>
      </div>
      <div class="col-sm-4">
      <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/document-icon.png"><?php echo $data['lotno'] ?><input type="hidden" name="lotno" id="lotno" value="<?php echo $data['lotno'] ?>" /></div>
      </div>
      <div class="col-sm-4">
      <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/document-icon.png"><?php echo $data['departmentname'] ?></div>
      </div>
      </div>
        </div>
      </div>
	  
      <div class="card card-default">
        <div class="card-body">
          <div class="row mt-4">
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/code-p.png"><?php echo $data['pcode'] ?><input type="hidden" name="pcode" id="pcode" value="<?php if($data['pcode']!='' && $data['pcode']!=null) { echo $data['pcode']; } else { set_value('pcode'); } ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/mdicine-red.png"><?php echo $data['pdesc'] ?><input type="hidden" name="pdesc" id="pdesc" value="<?php echo $data['pdesc'] ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/market.png"><?php echo $data['market'] ?><input type="hidden" name="market" id="market" value="<?php echo $data['market'] ?>" /></div>
            </div>
          </div>
          <div class="row mt-4 mb-2">
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/code-bred.png"><?php echo $data['bcode'] ?><input type="hidden" name="bcode" id="bcode" value="<?php echo $data['bcode'] ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/mg-red.png"><?php echo $data['UOM'] ?><input type="hidden" name="UOM" id="UOM" value="<?php echo $data['UOM'] ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/40-red.png"><?php echo $data['bsizeUOM'] ?><input type="hidden" name="bsizeUOM" id="bsizeUOM" value="<?php echo $data['bsizeUOM'] ?>" /></div>
            </div>
          </div>
        </div>
      </div>
      <div class="card card-default">
        <div class="card-body">
          <div class="row mb-3">
		  <div class="col-sm-12">		  
              <div><span class="ml18 float-left pt11"><h4>Equipments list of Granulation</h4></span></div>
        <?php if($data['header_id'] !='' && $data['header_id']!=null) { ?>
      
      <?php } else { ?>
      <div class="float-right"><span class="ml18 float-left pt11 mr15"><h4>Add portable equipments </h4></span><i data-toggle="modal" data-target="#myModalLarge" class="fa fa-plus default-icon-color default-icon-style float-right" aria-hidden="true"></i></div>
      <?php } ?>			  
            </div>
		  <div class="modal-body">
                    <section class="page-tasks" id="Listofequip">
          <ul class="task-list list-unstyled" id="eqlist">
            <?php $counter=0; foreach($data['fixequipment']->result() as $row) { if($row->equipment_type == "Fixed") { $counter++; ?> 
  <li id='<?php echo "eqli" .$row->id ?>'> 
  <input type="hidden" name='<?php echo "hddeqdtl" .$row->id ?>' id='<?php echo "hddeqdtl" .$row->id ?>' data-id='<?php echo $row->id ?>' data-eqcode='<?php echo $row->equipment_code ?>' data-sopcode='<?php echo $row->sop_code ?>' data-eqtype='<?php echo $row->equipment_type ?>' />
  <span class="view" id='<?php echo "eqdiv" .$row->id ?>'><div class="row"><div class="col-sm-12 pl0"><div class="row">
  <div class="col-sm-4 listexecutesop2"><img class="mb-2 mt-2 wt42" src="<?php echo base_url(); ?>img/icons/Octoganal-Blende.png"><span class="f15"><?php echo $row->equipment_name ?></span></div>
  <div class="col-sm-5 mt-4"><p class="f15 mt-2"><?php echo $row->sop_code ?> <?php echo $row->sop_name ?></p></div>
  <div class="col-sm-3 disable-button-color float-right text-right pl00" id='<?php echo "show" .$row->id ?>'>       
  <span class="fa-stack mt-3" style="display:none;" name='<?php echo "t" .$row->id ?>' id='<?php echo "t" .$row->id ?>' data-flag="<?php echo $row->id ?>" onclick="showtme('<?php echo $row->id ?>');">
  <em class="fa fa-circle fa-stack-2x text-success bgred"></em>
  <em class="far fa-clock fa-stack-1x fa-inverse text-white"></em>
  </span>       
  <button class="btn btn-labeled btn-danger mt-4 mb-2" type="button" name="<?php echo 'btndb' .$row->id ?>" id="<?php echo 'btnbd' .$row->id ?>" onClick="brkdown('<?php echo $row->id ?>')" ><span class="btn-label"><i class="fa fa-times"></i></span>Breakdown</button>&nbsp;
  <button class="btn btn-labeled btn-success mb-2 mt-4 pl10" type="button" onClick="memo('<?php echo $row->id ?>')" name='<?php echo "end" .$row->id ?>' id='<?php echo "end" .$row->id ?>' style="display:none;"><span class="btn-label"><i class="fa fa-check"></i></span>End</button>          
  
  <div class="card card-default tableleftalign" id='<?php echo "divbdtbl" .$row->id ?>' style="display:none;">
  <div class="card-header p-5"><center>Breakdown Time Record</center></div>
  <table class="table table-dark" id='<?php echo "bdtbl" .$row->id ?>'>
  <thead><tr><th scope="col">Start</th><th scope="col">End</th><th scope="col">Elapsed</th><th scope="col">Operated By</th></tr></thead>
  <tbody>
  </tbody>
  </table>
  </div><!-- START row-->
          
  </div>
  </div>
  </div>
  </div>
  </span> 
  </li>
  <?php } } ?>
            
          </ul>
		  <ul class="task-list list-unstyled" id="porteqlist">
		  </ul>
        </section>
		</form>
		</div>
          </div>		
		  
		  <div class="row">
      <?php if($data['header_id'] !='' && $data['header_id']!=null) { ?>
      <div class="col-sm-4 text-right" id="sbtn"><span id="demo3" class="pr-2 f16"></span><button id="sbbtn" class="btn btn-success btn-lg" type="button" disabled="disabled" onClick="ftrrshow();">&nbsp;&nbsp;&nbsp;&nbsp;Start&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      <?php } else { ?>
      <div class="col-sm-4 text-right" id="sbtn"><span id="demo3" class="pr-2 f16"></span><button id="sbbtn" class="btn btn-success btn-lg" type="button" onClick="ftrrshow();">&nbsp;&nbsp;&nbsp;&nbsp;Start&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      <?php } ?>  
			
			<div class="col-sm-4 text-right" id="rsbtn" style="display:none;"><span id="demo3" class="pr-2 f16"></span><button id="btnrs" class="btn btn-success btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Restart&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
			<div class="col-sm-4"><button id="fbtn" onclick="stopoperation();" class="btn btn-danger btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Stop&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      <div class="col-sm-4"><button id="chkby" onclick="checkedbyoperation();" class="btn btn-danger btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Checked by&nbsp;&nbsp;&nbsp;&nbsp;</button>
        <?php if($data['header_id'] !='' && $data['header_id']!=null) { ?>
      <span class="fa-stack mt-2 float-right" name="FTR" id="FTR" data-flag="1" onclick="FTRshowtme();">
      <?php } else { ?>
      <span class="fa-stack mt-2 float-right" style="display:none;" name="FTR" id="FTR" data-flag="1" onclick="FTRshowtme();">
      <?php } ?>			
				  <em class="fa fa-circle fa-stack-2x text-success bgred"></em>
				  <em class="far fa-clock fa-stack-1x fa-inverse text-white"></em>
			</span>
			</div>
			<div class="col-sm-12 mt-2">
      <?php if($data['header_id'] !='' && $data['header_id']!=null) { ?>
      <div class="card card-default" id="divftbl">
      <?php } else { ?>
      <div class="card card-default" id="divftbl" style="display:none;">
      <?php } ?>
			
               <div class="card-header p-5"><center>Activity Log Record</center></div>
               <table class="table table-dark" id="bdftbl">
                  <thead>
                     <tr tabindex=0>                        
                        <th scope="col">Start</th>
                        <th scope="col">End</th>
						<th scope="col">performed By</th>
						<th scope="col">Checked By</th>
						<th scope="col">Activity</th>
                     </tr>
                  </thead>
                  <tbody>
          				<?php foreach($data['headerlogdtl']->result() as $row) { ?> 
          <tr>
            <td><?php echo $row->start_time ?></td>
            <td><?php echo $row->end_time ?></td>
            <td><?php echo $row->performed_by ?></td>
            <td><?php echo $row->checked_by ?></td>
            <td>Production</td>
          </tr> 
          <?php } ?>
                  </tbody>
               </table>
            </div><!-- START row-->
			
			</div>
			
		  </div>
		  
        </div>
      </div>
    </div>
  </section>
  
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="js/app.js"></script> 


<!--My Modal-->
<div class="modal fade show" id="myModal" data-id="0"  style="display: none; padding-right: 17px;">
  <div class="modal-dialog modal-md">
    <div class="modal-content mt120">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelLarge">Submit memo number & remark</h4>
        <!--<button class="close" type="button" data-dismiss="modal" aria-label="Close" onclick="hidememo();"><span aria-hidden="true">×</span></button>-->   
      </div>
    
      <div class="modal-body">
    <div class="col-md-12">
                  <!-- START card-->
                  <div class="card card-default">
                     <div class="card-body">
                        <form>
                           <div class="form-group"><label>Memo Number</label><input name="memonumber" id="memonumber" class="form-control" type="text" placeholder="Enter Memo Number"></div>
                           <div class="form-group"><label>Remark</label><textarea name="memoremark" id="memoremark" class="form-control" row="3" placeholder="Enter Remark"></textarea></div>
                        </form>
                     </div>
           
            </div><!-- END card-->
        </div>    
      </div>
    <div class="modal-footer" style="justify-content: center;">       
      <div class="modal-button-container"><button type="button" onclick="hidememo();" class="btn btn-success btn-lg">Submit</button></div>
    </div>
    </div>
  </div>
</div>
<!--End My Modal-->
<!-- Modal Large-->
<div class="modal fade" id="myModalLarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabelLarge" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabelLarge">Add portable euipments in your euipment list</h4>
        <!--<button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
      </div>
    
      <div class="modal-body">
    <ul class="task-list list-unstyled">
    
        <li class="bg-white pt10 pb10 pl15">
        <span><b>Area :</b> </span><span class="col-sm-3 pl-0" id="area">Granulation</span>
        <select class="custom-select custom-select-sm col-sm-6" id="areaddl" onchange="araechange();">
        <option selected="">Select Another Area</option>
        <option value="Blending">Blending</option>
        <option value="Coating">Coating</option>
        <option value="Packaging">Packaging</option>
        </select>
                              <button class="btn btn-success col-sm-2 mr-2" type="button" value="Show all" onclick="showpequip();" name="showall" id="showall" style="float:right;">Show All</button>
                <button class="btn btn-success col-sm-2 mr-2" style="display:none;float:right;" type="button" value="Show less" onclick="hidepequip();" name="showless" id="showless">Show less</button>
                              
        </li>

        </ul>
        <section class="page-tasks">
    
          <ul class="task-list list-unstyled" id="peqlist">
  <?php foreach($data['fixequipment']->result() as $row) { if($row->equipment_type == "Portable") {?> 
      <li id="<?php echo 'pli' .$row->id ?>"><span class="view" id="<?php echo 'p' .$row->id ?>"><div class="row"><div class="col-sm-12 pl0"><div class="row">
  <div class="col-sm-10 listexecutesop1">
  <img class="mb-2 mt-2 wt42" src="<?php echo base_url(); ?>img/icons/Sifter.png">    
    <span class="f15"><?php echo $row->equipment_name ?></span></div>
  <div class="col-md-2 float-right"><label class="switch switch-lg mt-2">
  <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip('<?php echo $row->id ?>');" data-name="<?php echo $row->equipment_name ?>" data-eqcode="<?php echo $row->equipment_code ?>" data-sopcode="<?php echo $row->sop_code ?>" data-eqtype="<?php echo $row->equipment_type ?>" data-sopdtl="<?php echo $row->sop_code ?> <?php echo $row->sop_name ?>" data-id="<?php echo $row->id ?>" data-imgurl="<?php echo base_url(); ?>img/icons/Sifter.png" id="<?php echo 'check' .$row->id ?>" name="<?php echo 'check' .$row->id ?>">
  <span></span></label></div>
    </div></div></div></span>
  </li>
  <?php } } ?>
  
          </ul>
        </section>
      </div>
    <div class="modal-footer" style="justify-content: center;">
      <div class="modal-button-container" id="cancel"><button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger btn-lg bg-danger">Cancel</button></div>
      <div class="modal-button-container pl15"><button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-success btn-lg">Insert</button></div>
    </div>
    </div>
  </div>
</div>

<script>
var id='';
function memo(obj)
{	
	$("#myModal").show(1000);
	$("#myModal").attr("data-id", obj);
}
function hidememo()
{
	
	$("#myModal").hide(1000);
	var id = $("#myModal").attr("data-id");
	brkdownover(id);
	
}
</script>
<script>
	    $("#proreg1 a").click(function () {
			debugger;
	        if($(this).attr("data-contact")=="Contact")
	        {
	        //debugger;
            console.log('inside');
            if ($(this).attr("data-flag1") === "false") {
                //debugger;
                $("#proreg a").each(function (prop, val) {
                    var el = $(this).attr("data-proreg");
                    $("#" + el).hide(1000);
                    $(this).attr("data-flag1", "false");
                });

                var el = $(this).attr("data-proreg");
                $("#" + el).show(1000);
                $(this).attr("data-flag1", "true");
            }
            return false;
	        }

        });
</script>

<script>
function ftrrshow()
{
  var docid = $("#docid").val();
  var pcode = $("#pcode").val();
  var pdesc = $("#pdesc").val();
  var bcode = $("#bcode").val();
  var market = $("#market").val();
  var lotno = $("#lotno").val();
  var UOM = $("#UOM").val();
  var bsizeUOM = $("#bsizeUOM").val();

  var myArray = [];
$("#eqlist").find('input[name^="hddeqdtl"]').each(function () {
  var sopcode = $(this).attr("data-sopcode");
  var eqcode = $(this).attr("data-eqcode");
  var eqtype = $(this).attr("data-eqtype");
  myArray.push( {
     sopcode: sopcode,
     eqcode: eqcode,
     eqtype: eqtype
   });

});
$("#porteqlist").find('input[name^="hddeqdtl"]').each(function () {
  var sopcode = $(this).attr("data-sopcode");
  var eqcode = $(this).attr("data-eqcode");
  var eqtype = $(this).attr("data-eqtype");
  myArray.push( {
     sopcode: sopcode,
     eqcode: eqcode,
     eqtype: eqtype
   });

});

  //var pcode = $("#pcode").val();
  //var pcode = $("#pcode").val();
    $.ajax({
      url: "<?= base_url()?>Production/makeinitiallog",
      type: 'POST',
      data: {docid:docid,bcode:bcode,lotno:lotno,myArray:myArray},
      success: function(res) {
          alert(res);
          alert("Operation is succesfully started...");
          $("#myModal2").hide();
          $("#bdftbl tr[tabindex=0]").focus();
          $("#divftbl").show(1000);
          $("#FTR").show();
          $("#sbbtn").attr("disabled", true);
      }
    });	
}

function stopoperation()
{
    $.ajax({
      url: "<?= base_url()?>Production/makefinishlog",
      type: 'POST',
      data: {},
      success: function(res) {
          alert(res);        
      }
    }); 
}

function checkedbyoperation()
{
    $.ajax({
      url: "<?= base_url()?>Production/makechkbylog",
      type: 'POST',
      data: {},
      success: function(res) {
          alert(res);        
      }
    }); 
}

function FTRshowtme()
{	
debugger;
	if($("#FTR").attr("data-flag")=="0")
	{
		$("#FTR").attr("data-flag", "1");
		$("#divftbl").show(1000);
	}
	else
	{
		$("#FTR").attr("data-flag", "0");
		$("#divftbl").hide(1000);
		
	}
}
function showtme(obj)
{	
debugger;
	if($("#t"+obj).attr("data-flag")=="0")
	{
		$("#t"+obj).attr("data-flag", "1");
		$("#divbdtbl"+obj).show(1000);
	}
	else
	{
		$("#t"+obj).attr("data-flag", "0");
		$("#divbdtbl"+obj).hide(1000);
		
	}
}
function araechange()
{
	var area = $("#areaddl").val();
	$("#area").text(area);
}

function chngclr(obj,divid)
{
	debugger;
	var check = document.getElementById(obj);
		if(check.checked == true){
	document.getElementById(divid).style.backgroundColor = "#b0be3c";
	document.getElementById(divid).style.color = "white";
	}else{
		document.getElementById(divid).style.backgroundColor = "white";
		document.getElementById(divid).style.color = "#000";
		}
}
function showpequip()
{
debugger;
	$("#pli4").show(100);
	$("#pli5").show(200);
	$("#pli6").show(300);
	$("#showall").hide();
	$("#showless").show();
}
function hidepequip()
{
debugger;
	$("#pli4").hide(100);
	$("#pli5").hide(200);
	$("#pli6").hide(300);
	$("#showall").show();
	$("#showless").hide();
}
</script>
<script>
var dt='';
var dt1='';
var stt='';
var ft='';
var ct='';
function brkdown(obj)
{
debugger;	
var memonumber = $("#memonumber").val();
  var memoremark = $("#memoremark").val();
  var sopcode = $("#hddeqdtl"+obj).attr("data-sopcode");
  var eqcode = $("#hddeqdtl"+obj).attr("data-eqcode");
  var eqtype = $("#hddeqdtl"+obj).attr("data-eqtype");
  var bdstarttime = "";
  var bdendtime = "";
  $.ajax({
      url: "<?= base_url()?>Production/makestartbrkdwnlog",
      type: 'POST',
      data: {sopcode:sopcode,eqcode:eqcode,eqtype:eqtype,memonumber:memonumber,memoremark:memoremark,bdstarttime:bdstarttime,bdendtime:bdendtime},
      success: function(res) {

    $("#btnbd"+obj).hide();
    $("#end"+obj).show();
    //$("#t"+obj).show();
    $("#fbtn").attr("disabled", true);
    $("#sbtn").hide();
    $("#rsbtn").show();
    $("#btnrs").attr("disabled", true);
    var dt = new Date();
    stt = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
    }
  });
}
function brkdownover(obj)
{
  var dt1 = new Date();
  ft = dt1.getHours() + ":" + dt1.getMinutes() + ":" + dt1.getSeconds();
  var memonumber = $("#memonumber").val();
  var memoremark = $("#memoremark").val();
  var sopcode = $("#hddeqdtl"+obj).attr("data-sopcode");
  var eqcode = $("#hddeqdtl"+obj).attr("data-eqcode");
  var eqtype = $("#hddeqdtl"+obj).attr("data-eqtype");
  var bdstarttime = "";
  var bdendtime = "";
  $.ajax({
      url: "<?= base_url()?>Production/makeendbrkdwnlog",
      type: 'POST',
      data: {sopcode:sopcode,eqcode:eqcode,eqtype:eqtype,memonumber:memonumber,memoremark:memoremark,bdstarttime:bdstarttime,bdendtime:bdendtime},
      success: function(res) {
          //alert(res);
          //alert("Operation is succesfully started...");
          $("#btnbd"+obj).show();
          $("#end"+obj).hide();
          $("#t"+obj).show();
          $("#t"+obj).attr("data-flag", "1");
          $("#fbtn").attr("disabled", false);
          $("#sbtn").hide();
          $("#btnrs").attr("disabled", false);
          $("#divbdtbl"+obj).show(1000);
          //$("#bdtbl"+obj).html("");
          
          $("#bdtbl"+obj).append("<tr><td>"+stt+"</td><td>"+ft+"</td><td>00:00:00</td><td>Bhupendra kumar</td></tr>");
      }
    });
}
function addpequip()
{
	debugger;
	$("#porteqlist").html("");
	var newRow = $("<li>");
	//counter = 6;
	$("#peqlist").find('input[name^="check"]').each(function () {	
	if($(this).prop('checked') == true)
	{	
	var name = $(this).attr("data-name");
	var imgurl = $(this).attr("data-imgurl");
  var counter = $(this).attr("data-id");
  var sopdtl = $(this).attr("data-sopdtl");
  var sopcode = $(this).attr("data-sopcode");
  var eqcode = $(this).attr("data-eqcode");
  var eqtype = $(this).attr("data-eqtype");
  //var counter++;
    var cols = "";
	cols += "<input type='hidden' name='hddeqdtl"+counter+"' id='hddeqdtl"+counter+"' data-id='"+counter+"' data-eqcode='"+eqcode+"' data-sopcode='"+sopcode+"' data-eqtype='"+eqtype+"' /> ";
	cols += "<span class='view' id='eqdiv"+counter+"'><div class='row'><div class='col-sm-12 pl0'><div class='row'>";
    cols += "<div class='col-sm-4 listexecutesop2'><img class='mb-2 mt-2 wt42' src='"+imgurl+"'><span class='f15'>"+name+"</span></div>";
	cols += "<div class='col-sm-5 mt-4'><p class='f15 mt-2'>"+sopdtl+"</p></div>";
    cols += "<div class='col-sm-3 disable-button-color float-right text-right pl00' id='show"+counter+"'>";
	cols += "<span class='fa-stack mt-3' style='display:none;' name='t"+counter+"' id='t"+counter+"' data-flag='1' onclick='showtme("+counter+");'><em class='fa fa-circle fa-stack-2x text-success bgred'></em><em class='far fa-clock fa-stack-1x fa-inverse text-white'></em></span>";			
	cols += "<button class='btn btn-labeled btn-danger mt-4 mb-2' type='button' onClick='brkdown("+counter+")' name='btnbd"+counter+"' id='btnbd"+counter+"'><span class='btn-label'><i class='fa fa-times'></i></span>Breakdown</button>";
	cols += "&nbsp;<button class='btn btn-labeled btn-success mb-2 mt-4 pl10' type='button' onClick='memo("+counter+")' name='end"+counter+"' id='end"+counter+"' style='display:none;'><span class='btn-label'><i class='fa fa-check'></i></span>End</button>";
	cols += "<div class='card card-default' id='divbdtbl"+counter+"' style='display:none;'><div class='card-header p-5'><center>Breakdown Time Record</center></div><table class='table table-dark' id='bdtbl"+counter+"'><thead><tr><th scope='col'>Start</th><th scope='col'>End</th><th scope='col'>Elapsed</th><th scope='col'>Operated By</th></tr></thead><tbody></tbody></table></div></div></div></div></div></span></li>";
    newRow.append(cols);      
	}
	
	});
	$("#porteqlist").append(newRow);
}
</script>
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>
</body>
</html>