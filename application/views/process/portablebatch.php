<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="Bootstrap Admin App">
<meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
<link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>favicon.ico">
<title>SMHS - eLog System</title>
<!-- =============== VENDOR STYLES ===============-->
<!-- FONT AWESOME-->
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/brands.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/regular.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/solid.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/fontawesome.css">
<!-- SIMPLE LINE ICONS-->
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/simple-line-icons/css/simple-line-icons.css">
<!-- ANIMATE.CSS-->
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/animate.css/animate.css">
<!-- WHIRL (spinners)-->
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/whirl/dist/whirl.css">
<!-- =============== PAGE VENDOR STYLES ===============-->
<!-- =============== BOOTSTRAP STYLES ===============-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css" id="bscss">
<!-- =============== APP STYLES ===============-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/app.css" id="maincss">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/Bhupeestyle.css">

</head>

<body>
<div class="wrapper"> 
  <!-- top navbar-->
  <header class="topnavbar-wrapper"> 
    <!-- START Top Navbar-->
    <nav class="navbar topnavbar"> 
      <!-- START navbar header-->
      <div class="navbar-header"><a class="navbar-brand" href="<?php echo base_url(); ?>logo">
        <div class="brand-logo font-weight-bold text-white">SMHS - eLog System</div>
        <div class="brand-logo-collapsed font-weight-bold text-white">SMHS</div>
        </a></div>
      <!-- END navbar header--> 
      <!-- START Left navbar-->
      <ul class="navbar-nav mr-auto flex-row">
        <li class="nav-item"> 
          <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link d-none d-md-block d-lg-block d-xl-block" href="<?php echo base_url(); ?>#" data-trigger-resize="" data-toggle-state="aside-collapsed"><em class="fas fa-bars"></em></a><!-- Button to show/hide the sidebar on mobile. Visible on mobile only.--><a class="nav-link sidebar-toggle d-md-none" href="<?php echo base_url(); ?>#" data-toggle-state="aside-toggled" data-no-persist="true"><em class="fas fa-bars"></em></a></li>
        <!-- START User avatar toggle-->
        <li class="nav-item d-none d-md-block"> 
          <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link" id="user-block-toggle" href="<?php echo base_url(); ?>#user-block" data-toggle="collapse"><em class="icon-user"></em></a></li>
        <!-- END User avatar toggle--> 
        <!-- START lock screen-->
        <li class="nav-item d-none d-md-block"><a class="nav-link" href="<?php echo base_url(); ?>#" title="Lock screen"><em class="icon-lock"></em></a></li>
        <!-- END lock screen-->
      </ul>
      <!-- END Left navbar--> 
      <!-- START Right Navbar-->
      <ul class="navbar-nav flex-row">
        <!-- Search icon-->
        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>#" data-search-open=""><em class="icon-magnifier"></em></a></li>
        <!-- Fullscreen (only desktops)-->
        <li class="nav-item d-none d-md-block"><a class="nav-link" href="<?php echo base_url(); ?>#" data-toggle-fullscreen=""><em class="fas fa-expand"></em></a></li>
        <!-- START Alert menu-->
        <li class="nav-item dropdown dropdown-list"><a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="<?php echo base_url(); ?>#" data-toggle="dropdown"><em class="icon-bell"></em><span class="badge badge-danger">11</span></a><!-- START Dropdown menu-->
          <div class="dropdown-menu dropdown-menu-right animated flipInX">
            <div class="dropdown-item"> 
              <!-- START list group-->
              <div class="list-group">
                <div class="list-group-item list-group-item-action">
                  <div class="media">
                    <div class="align-self-start mr-2"><em class="fas fa-envelope fa-2x text-warning"></em></div>
                    <div class="media-body">
                      <p class="m-0">New e-mails</p>
                      <p class="m-0 text-muted text-sm">You have 10 new emails</p>
                    </div>
                  </div>
                </div>
                <!-- list item-->
                <div class="list-group-item list-group-item-action">
                  <div class="media">
                    <div class="align-self-start mr-2"><em class="fas fa-tasks fa-2x text-success"></em></div>
                    <div class="media-body">
                      <p class="m-0">Pending Tasks</p>
                      <p class="m-0 text-muted text-sm">11 pending task</p>
                    </div>
                  </div>
                </div>
                <!-- last list item-->
                <div class="list-group-item list-group-item-action"><span class="d-flex align-items-center"><span class="text-sm">More notifications</span><span class="badge badge-danger ml-auto">14</span></span></div>
              </div>
              <!-- END list group--> 
            </div>
          </div>
          <!-- END Dropdown menu--> 
        </li>
        <!-- END Alert menu--> 
        <!-- START Offsidebar button-->
        <li class="nav-item"><a href="<?php echo base_url(); ?>#" class="nav-link"></a></li>
        <li class="dropdown langs text-normal ng-scope" uib-dropdown="" is-open="status.isopenLang" data-ng-controller="LangCtrl" style=""> <a href="<?php echo base_url(); ?>javascript:;" class="dropdown-toggle  dropdown-toggleactive-flag" uib-dropdown-toggle="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <div class="flag flags-american" style=""></div>
          </a>
          <ul class="dropdown-menu with-arrow  pull-right list-langs dropdown-menu-scaleIn animated flipInX" role="menu">
            <li data-ng-show="lang !== 'English' " aria-hidden="true" class="ng-hide" style=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('English')">
              <div class="flag flags-american"></div>
              English</a></li>
            <li data-ng-show="lang !== 'Español' " aria-hidden="false" class="" style=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('Español')">
              <div class="flag flags-spain"></div>
              Español</a></li>
            <li data-ng-show="lang !== 'Portugal' " aria-hidden="false" class="" style=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('Portugal')">
              <div class="flag flags-portugal"></div>
              Portugal</a></li>
            <li data-ng-show="lang !== '中文' " aria-hidden="false" class=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('中文')">
              <div class="flag flags-china"></div>
              中文</a></li>
            <li data-ng-show="lang !== '日本語' " aria-hidden="false" class="" style=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('日本語')">
              <div class="flag flags-japan"></div>
              日本語</a></li>
            <li data-ng-show="lang !== 'Русский язык' " aria-hidden="false" class="" style=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('Русский язык')">
              <div class="flag flags-russia"></div>
              Русский язык</a></li>
          </ul>
        </li>
        
        <!-- END Offsidebar menu-->
      </ul>
      <!-- END Right Navbar--> 
      <!-- START Search form-->
      <form class="navbar-form" role="search" action="search.html">
        <div class="form-group">
          <input class="form-control" type="text" placeholder="Type and hit enter ...">
          <div class="fas fa-times navbar-form-close" data-search-dismiss=""></div>
        </div>
        <button class="d-none" type="submit">Submit</button>
      </form>
      <!-- END Search form--> 
    </nav>
    <!-- END Top Navbar--> 
  </header>
  <!-- sidebar-->
  <aside class="aside-container"> 
    <!-- START Sidebar (left)-->
    <div class="aside-inner">
      <nav class="sidebar" data-sidebar-anyclick-close=""> 
        <!-- START sidebar nav-->
        <ul class="sidebar-nav">
          <!-- START user info-->
          <li class="has-user-block">
            <div class="collapse" id="user-block">
              <div class="item user-block"> 
                <!-- User picture-->
                <div class="user-block-picture">
                  <div class="user-block-status"><img class="img-thumbnail rounded-circle" src="<?php echo base_url(); ?>img/user/02.jpg" alt="Avatar" width="60" height="60">
                    <div class="circle bg-success circle-lg"></div>
                  </div>
                </div>
                <!-- Name and Job-->
                <div class="user-block-info"><span class="user-block-name">Hello, Mike</span><span class="user-block-role">Designer</span></div>
              </div>
            </div>
          </li>
          <!-- END user info--> 
          <!-- Iterates over all sidebar items-->
          <li class="nav-heading text-bold"><span data-localize="sidebar.heading.HEADER">Main Menu</span></li>
          <li><a href="<?php echo base_url(); ?>dashboard.html" title="Plant master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Plant master">My Tasks</span></a></li>
          <li class=" active"><a href="<?php echo base_url(); ?>area.html" title="Plant master"><em class="fas fa-cogs"></em><span data-localize="sidebar.nav.Plant master">Execute SOP</span></a></li>
          <li><a href="<?php echo base_url(); ?>#submenu" title="Menu" data-toggle="collapse"><em class="fas fa-chalkboard-teacher"></em><span data-localize="sidebar.nav.menu.MENU">Master Data</span></a>
            <ul class="sidebar-nav sidebar-subnav collapse" id="submenu">
              <li class="sidebar-subnav-header">Menu</li>
              <li><a href="<?php echo base_url(); ?>#" title="Plant master"><span data-localize="sidebar.nav.Plant master">Plant master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Block master"><span data-localize="sidebar.nav.Plant master">Block master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Area master"><span data-localize="sidebar.nav.Plant master">Area master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Rooms master"><span data-localize="sidebar.nav.Plant master">Rooms master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="SOP master"><span data-localize="sidebar.nav.Plant master">SOP master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Product master"><span data-localize="sidebar.nav.Plant master">product master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Critical Parts master"><span data-localize="sidebar.nav.Critical Parts master">Critical Parts master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Accessory Parts"><span data-localize="sidebar.nav.Accessory Parts">Accessory Parts</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Equipments master"><span data-localize="sidebar.nav.Equipments master">Equipments master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Employee master"><span data-localize="sidebar.nav.Employee master">Employee master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Holiday master"><span data-localize="sidebar.nav.Holiday master">Holiday master</span></a></li>
            </ul>
          </li>
        </ul>
        <!-- END sidebar nav--> 
      </nav>
    </div>
    <!-- END Sidebar (left)--> 
  </aside>
  <!-- offsidebar-->
  <aside class="offsidebar d-none"> 
    <!-- START Off Sidebar (right)-->
    <nav>
      <div role="tabpanel"> 
        <!-- Nav tabs-->
        <ul class="nav nav-tabs nav-justified" role="tablist">
          <li class="nav-item" role="presentation"><a class="nav-link active" href="<?php echo base_url(); ?>#app-settings" aria-controls="app-settings" role="tab" data-toggle="tab"><em class="icon-equalizer fa-lg"></em></a></li>
          <li class="nav-item" role="presentation"><a class="nav-link" href="<?php echo base_url(); ?>#app-chat" aria-controls="app-chat" role="tab" data-toggle="tab"><em class="icon-user fa-lg"></em></a></li>
        </ul>
        <!-- Tab panes-->
        <div class="tab-content">
          <div class="tab-pane fade active show" id="app-settings" role="tabpanel">
            <h3 class="text-center text-thin mt-4">Settings</h3>
            <div class="p-2">
              <h4 class="text-muted text-thin">Themes</h4>
              <div class="row row-flush mb-2">
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-a.css">
                      <input type="radio" name="setting-theme" checked="checked">
                      <span class="icon-check"></span><span class="split"><span class="color bg-info"></span><span class="color bg-info-light"></span></span><span class="color bg-white"></span></label>
                  </div>
                </div>
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-b.css">
                      <input type="radio" name="setting-theme">
                      <span class="icon-check"></span><span class="split"><span class="color bg-green"></span><span class="color bg-green-light"></span></span><span class="color bg-white"></span></label>
                  </div>
                </div>
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-c.css">
                      <input type="radio" name="setting-theme">
                      <span class="icon-check"></span><span class="split"><span class="color bg-purple"></span><span class="color bg-purple-light"></span></span><span class="color bg-white"></span></label>
                  </div>
                </div>
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-d.css">
                      <input type="radio" name="setting-theme">
                      <span class="icon-check"></span><span class="split"><span class="color bg-danger"></span><span class="color bg-danger-light"></span></span><span class="color bg-white"></span></label>
                  </div>
                </div>
              </div>
              <div class="row row-flush mb-2">
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-e.css">
                      <input type="radio" name="setting-theme">
                      <span class="icon-check"></span><span class="split"><span class="color bg-info-dark"></span><span class="color bg-info"></span></span><span class="color bg-gray-dark"></span></label>
                  </div>
                </div>
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-f.css">
                      <input type="radio" name="setting-theme">
                      <span class="icon-check"></span><span class="split"><span class="color bg-green-dark"></span><span class="color bg-green"></span></span><span class="color bg-gray-dark"></span></label>
                  </div>
                </div>
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-g.css">
                      <input type="radio" name="setting-theme">
                      <span class="icon-check"></span><span class="split"><span class="color bg-purple-dark"></span><span class="color bg-purple"></span></span><span class="color bg-gray-dark"></span></label>
                  </div>
                </div>
                <div class="col mb-2">
                  <div class="setting-color">
                    <label data-load-css="css/theme-h.css">
                      <input type="radio" name="setting-theme">
                      <span class="icon-check"></span><span class="split"><span class="color bg-danger-dark"></span><span class="color bg-danger"></span></span><span class="color bg-gray-dark"></span></label>
                  </div>
                </div>
              </div>
            </div>
            <div class="p-2">
              <h4 class="text-muted text-thin">Layout</h4>
              <div class="clearfix">
                <p class="float-left">Fixed</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-fixed" type="checkbox" data-toggle-state="layout-fixed">
                    <span></span></label>
                </div>
              </div>
              <div class="clearfix">
                <p class="float-left">Boxed</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-boxed" type="checkbox" data-toggle-state="layout-boxed">
                    <span></span></label>
                </div>
              </div>
              <div class="clearfix">
                <p class="float-left">RTL</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-rtl" type="checkbox">
                    <span></span></label>
                </div>
              </div>
            </div>
            <div class="p-2">
              <h4 class="text-muted text-thin">Aside</h4>
              <div class="clearfix">
                <p class="float-left">Collapsed</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-collapsed" type="checkbox" data-toggle-state="aside-collapsed">
                    <span></span></label>
                </div>
              </div>
              <div class="clearfix">
                <p class="float-left">Collapsed Text</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-collapsed-text" type="checkbox" data-toggle-state="aside-collapsed-text">
                    <span></span></label>
                </div>
              </div>
              <div class="clearfix">
                <p class="float-left">Float</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-float" type="checkbox" data-toggle-state="aside-float">
                    <span></span></label>
                </div>
              </div>
              <div class="clearfix">
                <p class="float-left">Hover</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-hover" type="checkbox" data-toggle-state="aside-hover">
                    <span></span></label>
                </div>
              </div>
              <div class="clearfix">
                <p class="float-left">Show Scrollbar</p>
                <div class="float-right">
                  <label class="switch">
                    <input id="chk-scroll" type="checkbox" data-toggle-state="show-scrollbar" data-target=".sidebar">
                    <span></span></label>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="app-chat" role="tabpanel">
            <h3 class="text-center text-thin mt-4">Connections</h3>
            <div class="list-group"> 
              <!-- START list title-->
              <div class="list-group-item border-0"><small class="text-muted">ONLINE</small></div>
              <!-- END list title-->
              <div class="list-group-item list-group-item-action border-0">
                <div class="media"><img class="align-self-center mr-3 rounded-circle thumb48" src="<?php echo base_url(); ?>img/user/05.jpg" alt="Image">
                  <div class="media-body text-truncate"><a href="<?php echo base_url(); ?>#"><strong>Juan Sims</strong></a><br>
                    <small class="text-muted">Designeer</small></div>
                  <div class="ml-auto"><span class="circle bg-success circle-lg"></span></div>
                </div>
              </div>
              <div class="list-group-item list-group-item-action border-0">
                <div class="media"><img class="align-self-center mr-3 rounded-circle thumb48" src="<?php echo base_url(); ?>img/user/06.jpg" alt="Image">
                  <div class="media-body text-truncate"><a href="<?php echo base_url(); ?>#"><strong>Maureen Jenkins</strong></a><br>
                    <small class="text-muted">Designeer</small></div>
                  <div class="ml-auto"><span class="circle bg-success circle-lg"></span></div>
                </div>
              </div>
              <div class="list-group-item list-group-item-action border-0">
                <div class="media"><img class="align-self-center mr-3 rounded-circle thumb48" src="<?php echo base_url(); ?>img/user/07.jpg" alt="Image">
                  <div class="media-body text-truncate"><a href="<?php echo base_url(); ?>#"><strong>Billie Dunn</strong></a><br>
                    <small class="text-muted">Designeer</small></div>
                  <div class="ml-auto"><span class="circle bg-danger circle-lg"></span></div>
                </div>
              </div>
              <div class="list-group-item list-group-item-action border-0">
                <div class="media"><img class="align-self-center mr-3 rounded-circle thumb48" src="<?php echo base_url(); ?>img/user/08.jpg" alt="Image">
                  <div class="media-body text-truncate"><a href="<?php echo base_url(); ?>#"><strong>Tomothy Roberts</strong></a><br>
                    <small class="text-muted">Designeer</small></div>
                  <div class="ml-auto"><span class="circle bg-warning circle-lg"></span></div>
                </div>
              </div>
              <!-- START list title-->
              <div class="list-group-item border-0"><small class="text-muted">OFFLINE</small></div>
              <!-- END list title-->
              <div class="list-group-item list-group-item-action border-0">
                <div class="media"><img class="align-self-center mr-3 rounded-circle thumb48" src="<?php echo base_url(); ?>img/user/09.jpg" alt="Image">
                  <div class="media-body text-truncate"><a href="<?php echo base_url(); ?>#"><strong>Lawrence Robinson</strong></a><br>
                    <small class="text-muted">Designeer</small></div>
                  <div class="ml-auto"><span class="circle bg-warning circle-lg"></span></div>
                </div>
              </div>
              <div class="list-group-item list-group-item-action border-0">
                <div class="media"><img class="align-self-center mr-3 rounded-circle thumb48" src="<?php echo base_url(); ?>img/user/10.jpg" alt="Image">
                  <div class="media-body text-truncate"><a href="<?php echo base_url(); ?>#"><strong>Tyrone Owens</strong></a><br>
                    <small class="text-muted">Designeer</small></div>
                  <div class="ml-auto"><span class="circle bg-warning circle-lg"></span></div>
                </div>
              </div>
            </div>
            <div class="px-3 py-4 text-center"> 
              <!-- Optional link to list more users--><a class="btn btn-purple btn-sm" href="<?php echo base_url(); ?>#" title="See more contacts"><strong>Load more..</strong></a></div>
            <!-- Extra items-->
            <div class="px-3 py-2">
              <p><small class="text-muted">Tasks completion</small></p>
              <div class="progress progress-xs m-0">
                <div class="progress-bar bg-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"><span class="sr-only">80% Complete</span></div>
              </div>
            </div>
            <div class="px-3 py-2">
              <p><small class="text-muted">Upload quota</small></p>
              <div class="progress progress-xs m-0">
                <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"><span class="sr-only">40% Complete</span></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
    <!-- END Off Sidebar (right)--> 
  </aside>
<!-- Main section-->
  <!-- Main section-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
	<div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Select batch</div>
        <!--<form class="search-form col-sm-5 pl-0">
          <em class="icon-magnifier"></em>
          <input class="form-control" type="email" placeholder="Enter room code or number on name">
        </form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Home2.html">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>area2.html">Granulation</a></li>
			<li class="breadcrumb-item"><a href="<?php echo base_url(); ?>room.html">TAB-32</a></li>
			<li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>SelectActivity.html">Operation</a></li>
            <!--<li class="breadcrumb-item">Execute SOP</li>-->
          </ol>
        </div>
      </div>	  
      <div class="card2 card card-default mt-4">		
        <div class="card-body mt-4">
		    <div class="form-group row mt-4">
          <?php $this->session->set_userdata('activity_code', $data['activity_code']); ?>
				<div class="col-md-1"></div>
				<div class="col-md-4"><div class="card-header fa-2x pt-1 pb-1 pl00 mb-15 mt-4">Enter lot no.</div></div>
        <div class="col-md-6"><div class="card-header fa-2x pt-1 pb-1 pl00 mb-15 mt-4">Enter a batch no.</div></div>
        <div class="col-md-1">
        </div>
        </div>
        <form action="<?php echo base_url() ?>Production/operation" method="post" id="submit2" enctype="multipart/form-data">
        <div class="form-row align-items-center">
			   <div class="col-md-12">
					<div class="form-group row">
					<div class="col-md-1"></div>
					<!--<div class="col-md-4">					
					<input class="form-control" type="text" id="lotno" name="lotno" placeholder="Enter lot number" />
					</div>-->
          <div class="col-md-6">          
          <input class="form-control autocomplete" type="text" id="bcode" name="bcode" placeholder="Enter a batch number" />
          </div>
					<div class="col-md-1"></div>
					</div>
			   </div>
        </div>  
            
            <div class="clearfix"></div>
            

			<div  class="row mt10" style="display:none;" id="showpr">
            <div class="col-md-8 offset-2">
          <div class="row">
            <div class="col-sm-4">
              <div class="listexecutesop f15"><img src="<?php echo base_url(); ?>img/icons/code-p.png" class="wt42"><span id="pro-code"></span><input type="hidden" id="hddpro_code" name="hddpro_code" value="" /></div>
            </div>
            <div class="col-sm-4"> 
              <div class="listexecutesop f15"><img src="<?php echo base_url(); ?>img/icons/mdicine-red.png" class="wt42"><span id="pro-desc"></span><input type="hidden" id="hddpro_desc" name="hddpro_desc" value="" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop f15"><img src="<?php echo base_url(); ?>img/icons/market.png" class="wt42"><span id="market"></span><input type="hidden" id="hddmarket" name="hddmarket" value="" /></div>
            </div>
          </div>
          <div class="row mt-4 mb-2">
            <div class="col-sm-4">
              <div class="listexecutesop f15"><img src="<?php echo base_url(); ?>img/icons/code-bred.png" class="wt42"><span id="batchcode"></span><input type="hidden" id="hddbatchcode" name="hddbatchcode" value="" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop f15"><img src="<?php echo base_url(); ?>img/icons/mg-red.png" class="wt42"><span id="UOM"></span><input type="hidden" id="hddUOM" name="hddUOM" value="" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop f15"><img src="<?php echo base_url(); ?>img/icons/40-red.png" class="wt42"><span id="batchsizeUOM"></span><input type="hidden" id="hddbatchsizeUOM" name="hddbatchsizeUOM" value="" /></div>
            </div>
          </div>
          </div>
      </div>
			
            <div class="row mt90 mb15">
            <div class="col-sm-12 text-center"><a href="#"  id="btnbounce" style="display: none;"><img id="imgbounce" src="<?php echo base_url(); ?>img/right-arrow.png" width="40" class="bounce"></a></div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </section>
 <!-- Page footer-->
  <footer class="footer-container text-center"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>


<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 
<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/flot/jquery.flot.time.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?php echo base_url(); ?>js/app.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script>
$(document).ready(function()
{
  $(".autocomplete").change(function() 
  {
    var bcode = $(this).val();
    $.ajax({
      url: "<?= base_url()?>Production/batchdata",
      type: 'POST',
      data: {bcode:bcode},
      success: function(res) {
          //alert(res);
          $("#showpr").hide(1000);
          $("#btnbounce").hide(1000);
          $("#pro-code").text(res.product_code);
          $("#pro-desc").text(res.product_description);
          $("#market").text(res.market);
          $("#batchcode").text(res.batch_code);
          $("#batchsizeUOM").text(res.batch_size+" "+res.UOM);
          $("#UOM").text(res.UOM);
          $("#hddpro_code").val(res.product_code);
          $("#hddpro_desc").val(res.product_description);
          $("#hddmarket").val(res.market);
          $("#hddbatchcode").val(res.batch_code);
          $("#hddbatchsizeUOM").val(res.batch_size+" "+res.UOM);
          $("#hddUOM").val(res.UOM);
          $("#showpr").show(1000);
          $("#btnbounce").show(1000);
      }
    });
  });  

  $(".autocomplete").keyup(function() 
  {
    var bcode = $(this).val();
    if(bcode!="" && bcode.length>=1){  
      $.ajax({
        url: "<?= base_url()?>Production/batchcode",
        type: 'POST',
        data: {bcode:bcode},
        success: function(res) {
          //alert(res);
          availableTags = res;   
          $(".autocomplete").autocomplete({
          source: availableTags
          });
        }
      });
    }
  });
});
</script>
<script type="text/javascript">

$( "img.bounce" ).click(function() {
  alert("Hi");
$('#submit2').submit();/*function(){ 
  debugger;
    var pcode = $("#hddpro_code").val();
    var pdesc = $("#hddpro_desc").val();
    var market = $("#hddmarket").val();
    var bcode = $("#hddbatchcode").val();
    var bsizeUOM = $("#hddbatchsizeUOM").val();
    var UOM = $("#hddUOM").val();
    $.ajax({
      url: "<?= base_url()?>Production/operation",
      type: 'POST',
      data: {pcode:pcode, pdesc:pdesc, market:market, bcode:bcode, bsizeUOM:bsizeUOM, UOM:UOM},
      success: function(res) {
          //alert(res);          
      }
    });
})*/
});


</script>
</body>
</html>