
<style type="text/css">
    .ui-autocomplete {
        position: absolute;
        z-index: 1000;
        cursor: default;
        padding: 0;
        margin-top: 2px;
        list-style: none;
        background-color: #ffffff;
        border: 1px solid #ccc;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;
        -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
        box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
    }
    .ui-autocomplete > li {
        padding: 3px 20px;
    }
    .ui-autocomplete > li.ui-state-focus {
        background-color: #DDD;
    }
    .ui-helper-hidden-accessible {
        display: none;
    }

    .subject-info-box-1,
    .subject-info-box-2 {
        float: left;
        width: 45%;
    }
    .subject-info-box-1 select,
    .subject-info-box-2 select {
        height: 400px;
        padding: 0;
    }
    .subject-info-box-1 select option,
    .subject-info-box-2 select option {
        padding: 4px 10px 4px 10px;
    }
    .subject-info-box-1 select option:hover,
    .subject-info-box-2 select option:hover {
        background: #EEEEEE;
    }

    .subject-info-arrows {
        float: left;
        width: 10%;
    }
    .subject-info-arrows input {
        width: 70%;
        margin-bottom: 5px;
    }
</style>


<!-- Main section-->
<!--<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">-->
<section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
        <div class="content-heading executesop-heading">
            <div class="col-sm-5 pl-0">Accessory Cleaning</div>
            <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
            <div class="col-sm-7 pr-0">
                <ol class="breadcrumb ml-auto">
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/area"><?php echo $this->session->userdata('area_code') ?></a></li>
                    <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/room?area=<?php echo $this->session->userdata('area_code') ?>"><?php echo $this->session->userdata('room_code') ?></a></li>
                    <li class="breadcrumb-item active"><a href="<?= base_url() ?>Production/accessoryCleaning">Accessory Cleaning</a></li>

                </ol>
            </div>
        </div>


        <div class="card card-default">
            <div class="card-body">                     
                <div class="row">
                    <div class="col-sm-12 text-right">

                        <a class="btn btn-primary btn-lg" href="<?= base_url() ?>Production/accessoryCleaningview" > Back To list</a>  
                        &nbsp;&nbsp;&nbsp;
                        <a class="btn btn-primary btn-lg" href="<?= base_url() ?>Production/accessoryCleaning"> Add Accessory Cleaning</a>

                    </div>
                </div>
            </div>
        </div>

        <div class="card card-default">
            <div class="card-body">                     
                <div class="row">
                    <div class="col-sm-6">
                        <span class="listexecutesop"><img src="<?= base_url() ?>img/icons/area-operation-page.png"><b>Area : </b><?= $this->session->userdata("area_code") . " , " . $this->session->userdata("area_name") ?> </span> 
                    </div>
                    <div class="col-sm-6">
                        <span class="listexecutesop"><img src="<?= base_url() ?>img/icons/area-operation-page.png"><b>Room : </b><?= $this->session->userdata("room_code") ?></span>
                    </div>
                </div>
            </div>
        </div>

        <form id="cleaning">
            <div class="card card-default">
                <div class="card-body">



                    <div class="row">
                        <div class="col-sm-6">
                            <div class="listexecutesop"><img src="<?= base_url() ?>img/icons/document-icon.png"><b>Document No. : </b><?= $docid ?></div>
                            <input type="text" name="doc_id" value="<?= $docid ?>" hidden required>
                        </div>


                        <div class="col-sm-6"><div class="row"><div class="col-sm-12 ui-widget">
                                    <div class="listexecutesop"><img src="<?= base_url() ?>img/icons/department.png"><b>Department Name : </b><?= $this->session->userdata('empdepartment_name') ?></div>
                                    <input class="form-control autocomplete" type="text" placeholder="Department Name" name="departmentname" required value="<?= $this->session->userdata('empdepartment_name') ?>" readonly hidden></div>
                            </div>
                        </div>

                    </div>


                </div>

            </div>


            <div class="card card-default">
                <div class="card-body">

                    <fieldset>

                        <div class="col-auto">


                            <div class="form-row">
                                <div class="col-lg-6 mb-3"><label for="validationServer01">Previous Batch No <span style="color: red">*</span></label><input class="form-control" id="validationServer01" type="text" placeholder="Batch No" name="batchno" required>

                                </div>
                                <div class="col-lg-6 mb-3"><label for="validationServer02">Previous Product Name <span style="color: red">*</span></label><input class="form-control autocomplete" id="productname" type="text" placeholder="Product Name" name="productname" required>

                                </div>



                            </div>

                            <div class="form-row">


                                <div class="subject-info-box-1" >
                                    <label for="validationServer01">Select Accessory</label>  
                                    <select multiple="multiple" id='lstBox1' class="form-control" style="min-height: 250px">

                                        <?php
                                        foreach ($accessory as $as) {
                                            ?>
                                            <option value="<?= $as ?>"><?= $as ?></option>
                                            <?php
                                        }
                                        ?>

                                    </select>
                                </div>
                                <div class="subject-info-arrows text-center">
                                    <label for="validationServer01"> </label>  
                                    <br><br>
                                    <input type='button' id='btnAllRight' value='>>' class="btn btn-primary" /><br />
                                    <input type='button' id='btnRight' value='>' class="btn btn-primary" /><br />
                                    <input type='button' id='btnLeft' value='<' class="btn btn-primary" /><br />
                                    <input type='button' id='btnAllLeft' value='<<' class="btn btn-primary" />
                                </div>
                                <div class="subject-info-box-2">
                                    <label for="validationServer01">Selected Accessory <span style="color: red">*</span></label>  
                                    <select multiple="multiple" id='lstBox2' class="form-control" name="accessorylist[]" style="min-height: 250px">

                                    </select>

                                </div>




                            </div>




                        </div>
                    </fieldset>
                </div>
            </div>


            <div class="card card-default">
                <div class="card-body">
                    <div class="col-lg-10 mb-3"><label for="validationServer01">Remarks <span style="color: red">*</span></label><textarea class="form-control" aria-label="With textarea" name="remarks" placeholder="Enter Remarks" required></textarea>

                    </div>

                </div>
            </div>   



            <div class="card card-default">

                <div class="card-body" >

                    <div class="row disable-button-color">
                        <div class="col-sm-4 text-right" id="sbtn"><button id="sbbtn" class="btn btn-success btn-lg" type="submit">&nbsp;&nbsp;&nbsp;&nbsp;Start&nbsp;&nbsp;&nbsp;&nbsp;</button></div>

                        <div class="col-sm-4"><button id="finish" class="btn btn-danger btn-lg" type="button" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Stop&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
                        <div class="col-sm-4"><button id="checkedby" class="btn btn-warning btn-lg" type="button" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Shift Change &nbsp;&nbsp;&nbsp;&nbsp;</button>

                        </div>
                        <div class="col-sm-12 mt-2" hidden>

                            <div class="card card-default" id="divftbl" style="display:none;">
                                <div class="card-header p-5"><center>Activity Log Record</center></div>
                                <table class="table table-dark" id="bdftbl">
                                    <thead>
                                        <tr tabindex=0>                        
                                            <th scope="col">Start</th>
                                            <th scope="col">End</th>
                                            <th scope="col">performed By</th>
                                            <th scope="col">Shift Change </th>
                                            <th scope="col">Activity</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div><!-- START row-->

                        </div>

                    </div>

                </div>
            </div><!-- END card-->

        </form>

    </div>
</div>

</div>
</section>
<!-- Page footer-->
<footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?= base_url() ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?= base_url() ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?= base_url() ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?= base_url() ?>vendor/i18next/i18next.js"></script> 
<script src="<?= base_url() ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?= base_url() ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?= base_url() ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?= base_url() ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- Modal Large-->
<div class="modal fade" id="myModalLarge" tabindex="-1" role="dialog" aria-labelledby="myModalLabelLarge" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabelLarge">Add portable euipments in your euipment list</h4>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>		
            </div>

            <div class="modal-body">
                <ul class="task-list list-unstyled">
                    <li class="bg-white pt10 pb10 pl15"> 
                        <span class="col-sm-3 pl-0"><b>Area :</b> </span><span class="col-sm-3 pl-0" id="area">Granulation</span>
                        <select class="custom-select custom-select-sm col-sm-6" id="areaddl" onchange="araechange();">
                            <option selected="">Select Another Area</option>
                            <option value="Blending">Blending</option>
                            <option value="Coating">Coating</option>
                            <option value="Packaging">Packaging</option>
                        </select>
                        <button class="btn btn-success col-sm-2 mr-2" type="button" value="Show all" onclick="showpequip();" name="showall" id="showall" style="float:right;">Show All</button>
                        <button class="btn btn-success col-sm-2 mr-2" style="display:none;float:right;" type="button" value="Show less" onclick="hidepequip();" name="showless" id="showless">Show less</button>

                    </li>
                </ul>
                <section class="page-tasks">

                    <ul class="task-list list-unstyled" id="peqlist">
                        <li id="pli1"> <span class="view" id="p1">
                                <div class="row">
                                    <div class="col-sm-12 pl0">
                                        <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 1" id="check1" name="check1">
                                        <label>Portable euipment 1</label>
                                    </div>
                                    <!-- <div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
                                </div>
                            </span> </li>
                        <li id="pli2"> <span class="view" id="p2">
                                <div class="row">
                                    <div class="col-sm-12 pl0">
                                        <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 2" id="check2" name="check2">
                                        <label>Portable euipment 2</label>
                                    </div>
                                    <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
                                </div>
                            </span> </li>
                        <li id="pli3"> <span class="view" id="p3">
                                <div class="row">
                                    <div class="col-sm-12 pl0">
                                        <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 3" id="check3" name="check3">
                                        <label>Portable euipment 3</label>
                                    </div>
                                    <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
                                </div>
                            </span> </li>
                        <li id="pli4" style="display:none;"> <span class="view" id="p4">
                                <div class="row">
                                    <div class="col-sm-12 pl0" onClick="Fstep">
                                        <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 4" id="check4" name="check4">
                                        <label>Portable euipment 4</label>
                                    </div>
                                    <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
                                </div>
                            </span> </li>
                        <li id="pli5" style="display:none;"> <span class="view" id="p5">
                                <div class="row">
                                    <div class="col-sm-12 pl0">
                                        <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 5" id="check5" name="check5">
                                        <label>Portable euipment 5</label>
                                    </div>
                                    <!--<div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
                                </div>
                            </span> </li>
                        <li id="pli6" style="display:none;"> <span class="view" id="p6">
                                <div class="row">
                                    <div class="col-sm-12 pl0">
                                        <input type="checkbox" class="toggle-task ng-valid ng-dirty ng-touched" onclick="addpequip();" data-name="Portable euipment 6" id="check6" name="check6">
                                        <label>Portable euipment 6</label>
                                    </div>
                                    <!-- <div class="col-sm-2 float-right editdetete-icon text-right"> <em class="fa-2x mr-2 far fa-edit"></em> <i class="fa fa-times" aria-hidden="true"></i> </div>--> 
                                </div>
                            </span> </li>
                    </ul>
                </section>
            </div>

        </div>
    </div>
</div>
<script src="<?= base_url() ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?= base_url() ?>js/app.js"></script> 

<!--
<script>

function araechange()
{
        var area = $("#areaddl").val();
        $("#area").text(area);
}

$(document).ready(function(){
        $("#show1 .btn-danger").click(function(){
                $(".showcountdown1").show();
        });
                
                $("#show1 .btn-success").click(function(){
                $(".showcountdown1").hide();
                });
                
                $("#show2 .btn-danger").click(function(){
                $(".showcountdown2").show();
        });
                
                $("#show2 .btn-success").click(function(){
                $(".showcountdown2").hide();
                });
                
                $("#show3 .btn-danger").click(function(){
                $(".showcountdown").show();
        });
                
                $("#show3 .btn-success").click(function(){
                $(".showcountdown").hide();
                });
                
                $("#show4 .btn-danger").click(function(){
                $(".showcountdown").show();
        });
                
                $("#show4 .btn-success").click(function(){
                $(".showcountdown").hide();
                });
                
                $("#show5 .btn-danger").click(function(){
                $(".showcountdown").show();
        });
                
                $("#show5 .btn-success").click(function(){
                $(".showcountdown").hide();
                });
                
                $("#show6 .btn-danger").click(function(){
                $(".showcountdown").show();
        });
                
                $("#show6 .btn-success").click(function(){
                $(".showcountdown").hide();
                });
        
        });

function chngclr(obj,divid)
{
        debugger;
        /*if($("#"+obj).prop('checked') == true)
        {
                $("#"+divid).css("background-color","#b0be3c");
        }
        else
        {
        
        }*/
        
        var check = document.getElementById(obj);
                if(check.checked == true){
        document.getElementById(divid).style.backgroundColor = "#b0be3c";
        document.getElementById(divid).style.color = "white";
        }else{
                document.getElementById(divid).style.backgroundColor = "white";
                document.getElementById(divid).style.color = "#000";
                }
}
function showpequip()
{
debugger;
        $("#pli4").show(100);
        $("#pli5").show(200);
        $("#pli6").show(300);
        $("#showall").hide();
        $("#showless").show();
}
function hidepequip()
{
debugger;
        $("#pli4").hide(100);
        $("#pli5").hide(200);
        $("#pli6").hide(300);
        $("#showall").show();
        $("#showless").hide();
}
function brkdown(obj)
{
debugger;
        for(i=1;i<=12;i++)
        {
                if(i==obj)
                {
                        $("#btnbd"+i).attr("disabled", true);
                        $("#end"+i).show();
                }
                else{
                        $("#btnbd"+i).attr("disabled", true);
                }
        }
        $("#fbtn").attr("disabled", true);
        $("#sbtn").hide();
        $("#rsbtn").show();
        $("#btnrs").attr("disabled", true);
}
function brkdownover(obj)
{
debugger;
        for(i=1;i<=12;i++)
        {
                if(i==obj)
                {
                        $("#btnbd"+i).attr("disabled", false);
                        $("#end"+i).hide();
                }
                else{
                        $("#btnbd"+i).attr("disabled", false);
                }
        }
        $("#fbtn").attr("disabled", false);
        $("#sbtn").hide();
        $("#btnrs").attr("disabled", false);
}
function addpequip()
{
        debugger;
        $("#porteqlist").html("");
        var newRow = $("<li>");
        var counter = 6;
        $("#peqlist").find('input[name^="check"]').each(function () {	
        if($(this).prop('checked') == true)
        {	
        counter++;
        var name = $(this).attr("data-name");
    var cols = "";
    cols += "<span class='view' id='eqdiv"+counter+"'><div class='row'><div class='col-sm-12 pl0'><div class='row'>";
    cols += "<div class='col-sm-9 listexecutesop'><img class='mb-4 mt-4 wt42' src='img/icons/machine.png'><span class='f15'>"+name+"</span><span class='f15 pl-8'>SOP/DC/011-3 Operation & Cleaning  of vertical deduster</span></div>";
    cols += "<div class='col-sm-3 disable-button-color float-right text-right' id='show"+counter+"'><button class='btn btn-labeled btn-danger mt-4 mb-4' type='button' onClick='brkdown("+counter+")' name='btnbd"+counter+"' id='btnbd"+counter+"'><span class='btn-label'><i class='fa fa-times'></i></span>Breakdown</button>";
        cols += "&nbsp;<button class='btn btn-labeled btn-success mb-2 mt-2 pl10' type='button' onClick='brkdownover("+counter+")' name='end"+counter+"' id='end"+counter+"' style='display:none;'><span class='btn-label'><i class='fa fa-check'></i></span>End</button></div></div></div></div></span></li>";
    newRow.append(cols);        
        }
        });
        $("#porteqlist").append(newRow);
}
function stepOne(){
        var check = document.getElementById("check1");
        if(check.checked == true){
        document.getElementById("incresswidth").style.width = "20%";
        document.getElementById("greencolor").style.backgroundColor = "#b0be3c";
        document.getElementById("greencolor").style.color = "white";
        }else{
                document.getElementById("incresswidth").style.width = "0%";
                document.getElementById("greencolor").style.backgroundColor = "white";
                document.getElementById("greencolor").style.color = "#000";
                }
}
function stepTwo(){
        var check = document.getElementById("check2");
        if(check.checked == true){
        document.getElementById("incresswidth").style.width = "40%";
        document.getElementById("greencolor2").style.backgroundColor = "#b0be3c";
        document.getElementById("greencolor2").style.color = "white";
        }else{
                document.getElementById("incresswidth").style.width = "20%";
                document.getElementById("greencolor2").style.backgroundColor = "white";
                document.getElementById("greencolor2").style.color = "#000";
                }
}
function stepThree(){
        var check = document.getElementById("check3");
        if(check.checked == true){
        document.getElementById("incresswidth").style.width = "55%";
        document.getElementById("greencolor3").style.backgroundColor = "#b0be3c";
        document.getElementById("greencolor3").style.color = "white";
        }else{
                document.getElementById("incresswidth").style.width = "40%";
                document.getElementById("greencolor3").style.backgroundColor = "white";
                document.getElementById("greencolor3").style.color = "#000";
                }
}
function stepFour(){
        var check = document.getElementById("check4");
        if(check.checked == true){
        document.getElementById("incresswidth").style.width = "70%";
        document.getElementById("greencolor4").style.backgroundColor = "#b0be3c";
        document.getElementById("greencolor4").style.color = "white";
        }else{
                document.getElementById("incresswidth").style.width = "55%";
                document.getElementById("greencolor4").style.backgroundColor = "white";
                document.getElementById("greencolor4").style.color = "#000";
                }
}
function stepFive(){
        var check = document.getElementById("check5");
        if(check.checked == true){
        document.getElementById("incresswidth").style.width = "85%";
        document.getElementById("greencolor5").style.backgroundColor = "#b0be3c";
        document.getElementById("greencolor5").style.color = "white";
        }else{
                document.getElementById("incresswidth").style.width = "70%";
                document.getElementById("greencolor5").style.backgroundColor = "white";
                document.getElementById("greencolor5").style.color = "#000";
                }
}
function stepSix(){
        var check = document.getElementById("check6");
        if(check.checked == true){
        document.getElementById("incresswidth").style.width = "100%";
        document.getElementById("greencolor6").style.backgroundColor = "#b0be3c";
        document.getElementById("greencolor6").style.color = "white";
        }else{
                document.getElementById("incresswidth").style.width = "85%";
                document.getElementById("greencolor6").style.backgroundColor = "white";
                document.getElementById("greencolor6").style.color = "#000";
                }
}

var onload = setTimeout(onload, 1000)
var onload = setTimeout(onload1, 2000)
function onload(){
        document.getElementById("show-hexagoan").style.display = "block";
        }
        function onload1(){
        document.getElementById("show-hexagoan1").style.display = "block";
        }
        
        
// Set the date we're counting down to
var countDownDate = new Date("Jan 5, 2021 00:00:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
 // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 60)) / (1000 * 60 * 60 ));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("demo").innerHTML = hours + "h "
  + minutes + "m " + seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);

// Set the date we're counting down to
var countDownDate = new Date("Jan 5, 2021 00:00:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
 // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 60)) / (1000 * 60 * 60 ));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("demo3").innerHTML = hours + "h "
  + minutes + "m " + seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo3").innerHTML = "EXPIRED";
  }
}, 1000);

// Set the date we're counting down to
var countDownDate = new Date("Jan 5, 2021 00:00:00").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();
    
  // Find the distance between now and the count down date
  var distance = countDownDate - now;
    
  // Time calculations for days, hours, minutes and seconds
 // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 60)) / (1000 * 60 * 60 ));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
  // Output the result in an element with id="demo"
  document.getElementById("demo4").innerHTML = hours + "h "
  + minutes + "m " + seconds + "s ";
    
  // If the count down is over, write some text 
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo4").innerHTML = "EXPIRED";
  }
}, 1000);




        

</script>
-->
<script type="text/javascript" src="js/script.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.js"></script>



<script type="text/javascript">
                                            $(document).ready(function () {

                                                /*
                                                 $(".autocomplete").change(function() {
                                                 
                                                 var str = $(this).val();
                                                 
                                                 $.ajax({
                                                 url: "<?= base_url() ?>Sanitization/solnamegetval",
                                                 type: 'POST',
                                                 data: {str:str},
                                                 success: function(res) {
                                                 //console.log(res);
                                                 $("#stdsolqty").val(res.sol_qty);
                                                 $("#strpurewater").val(res.water_qty);
                                                 $("#markupvol").val(res.tot_qty);
                                                 }
                                                 });
                                                 
                                                 });  */


                                                $(".autocomplete").keyup(function () {
                                                    var str = $(this).val();
                                                    if (str != "" && str.length >= 1) {

                                                        $.ajax({
                                                            url: "<?= base_url() ?>Production/productName",
                                                            type: 'POST',
                                                            data: {str: str},
                                                            success: function (res) {
                                                                console.log(res);
                                                                availableTags = res;

                                                                $(".autocomplete").autocomplete({
                                                                    source: availableTags
                                                                });
                                                            }
                                                        });


                                                    }

                                                });

                                            });
</script>

<script type="text/javascript">
    $(document).ready(function () {

        $('#cleaning').submit(function () {
            var conform = confirm("Do You Really Want To Start This?");
            if (conform == true)
            {
                $.ajax({
                    type: 'POST',
                    url: '<?= base_url() ?>Production/cleaningDetails',
                    data: $(this).serialize() // getting filed value in serialize form
                })
                        .done(function (details) { // if getting done then call.
                            // show the response
                            //console.log(details);
                            //var nn = $("#lstBox2").val();
                            //alert(nn);
                            if (details.status == 1) {
                                //console.log(details.data);
                                //alert("Accessory Cleaning started");
                                //window.location.href = "<?= base_url() ?>production/accessoryCleaningview";
                                //location.reload(true);
                                //window.location.href = "<?= base_url() ?>production/accessoryCleaningviewnow/"+details.id;
                                $("#myb").click();
                                $("#head").html(details.msgheader);
                                $("#message").html(details.msg);
                                $("#disclose").attr("href", "<?= base_url() ?>Production/accessoryCleaningviewnow/" + details.id);
                            } else {
                                alert("No");
                            }

                        })
                        .fail(function (details) { // if fail then getting message
                            // just in case posting your form failed
                            alert("Workflow for this activitymay not be defined or connectivity to the server may be lost. Please contact the administartor.");
                        });

// to prevent refreshing the whole page page
                return false;
            } else
            {
                return false;

            }

        });




    });
</script>

<script id="rendered-js">
    /*
     * Original example found here: http://www.jquerybyexample.net/2012/05/how-to-move-items-between-listbox-using.html
     * Modified by Esau Silva to support 'Move ALL items to left/right' and add better stylingon on Jan 28, 2016.
     * 
     */
    function strDes(a, b) {
        if (a.value > b.value)
            return 1;
        else
        if (a.value < b.value)
            return -1;
        else
            return 0;
    }

    console.clear();
    (function () {
        $('#btnRight').click(function (e) {
            var selectedOpts = $('#lstBox1 option:selected');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#lstBox2').append($(selectedOpts).clone());
            $('#lstBox2 option').prop('selected', true);
            $(selectedOpts).remove();

            /* -- Uncomment for optional sorting --
             var box2Options = $('#lstBox2 option');
             var box2OptionsSorted;
             box2OptionsSorted = box2Options.toArray().sort(strDes);
             $('#lstBox2').empty();
             box2OptionsSorted.forEach(function(opt){
             $('#lstBox2').append(opt);
             })
             */

            e.preventDefault();
        });

        $('#btnAllRight').click(function (e) {
            var selectedOpts = $('#lstBox1 option');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#lstBox2').append($(selectedOpts).clone());
            $(selectedOpts).remove();

            $('#lstBox2 option').prop('selected', true);
            e.preventDefault();
        });

        $('#btnLeft').click(function (e) {
            var selectedOpts = $('#lstBox2 option:selected');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#lstBox1').append($(selectedOpts).clone());
            $(selectedOpts).remove();
            e.preventDefault();
        });

        $('#btnAllLeft').click(function (e) {
            var selectedOpts = $('#lstBox2 option');
            if (selectedOpts.length == 0) {
                alert("Nothing to move.");
                e.preventDefault();
            }

            $('#lstBox1').append($(selectedOpts).clone());
            $(selectedOpts).remove();
            e.preventDefault();
        });
    })(jQuery);
    //# sourceURL=pen.js
</script>


<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title pull-left" id="head"></h4>
                <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
            </div>
            <div class="modal-body">
                <p id="message"></p>
            </div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
                <!--data-dismiss="modal"-->
                <a href="" class="btn btn-primary" id="disclose">Ok</a>
            </div>
        </div>

    </div>
</div>
</body>
</html>