
  <!-- Main section-->

  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Portable Equipment Cleaning</div>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/area"><?php echo $this->session->userdata('area_code') ?></a></li>
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/room?area=<?php echo $this->session->userdata('area_code') ?>"><?php echo $this->session->userdata('room_code') ?></a></li>
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/activity?room=<?php echo $this->session->userdata('room_code') ?>"><?php echo $this->session->userdata('activity_code') ?></a></li>
      <li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>Production/batch?activitycode=<?php echo $this->session->userdata('activity_code') ?>"><?php echo $this->session->userdata('batch_code') ?></a></li>
          </ol>
        </div>
      </div>


<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-12 text-right">
<div style="padding-right: 5px">
<a class="btn btn-primary btn-lg" href="<?= base_url()?>Production/portableEquipmentcleaningview" > Back To list</a>  
&nbsp;&nbsp;&nbsp;
<a class="btn btn-primary btn-lg" href="<?= base_url()?>Production/portableEquipmentcleaning"> Add New</a>
</div>
</div>
</div>
</div>
</div>

<div class="card card-default">
<div class="card-body">                     
<div class="row">
<div class="col-sm-6">
<span class="listexecutesop"><img src="<?= base_url()?>img/icons/area-operation-page.png"><b>Area : </b><?= $this->session->userdata("area_code")." , ".$this->session->userdata("area_name")?> </span> 
</div>
<div class="col-sm-6">
<span class="listexecutesop"><img src="<?= base_url()?>img/icons/area-operation-page.png"><b>Room : </b><?= $this->session->userdata("room_code")?></span>
</div>
</div>
</div>
</div>


<?php
$cb = $row["response1"]["created_by"];
$finishbuttonstatus = $row["response1"]["status"]==2? "disabled":"";
?>
  <form id="sub">  
    <div class="card card-default">
        <div class="card-body">
      <div class="row">
      <div class="col-sm-6">
      <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/document-icon.png"><b>Document No. : </b><?php echo $row["response1"]["document_no"] ?><input type="hidden" name="docid" id="docid" value="<?php echo $row["response1"]["document_no"] ?>" /></div>

      <input type="text" name="id" id="id" value="<?= $row["response1"]["id"]?>" hidden required>
      <input type="text" name="status" id="status" value="<?= $row["response1"]['status']?>" hidden required>
      <input type="text" name="next_step" id="next_step" value="<?= $row["response1"]['next_step']?>" hidden required>

      </div>
      <div class="col-sm-6">
      <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/department.png"><b>Department Name : </b><?php echo $row["response1"]['department_name'] ?></div>
      <input type="hidden" name="departmentname" id="departmentname" value="<?php echo $row["response1"]['department_name'] ?>" />
      </div>
      </div>
        </div>
      </div>
    

      <div class="card card-default">
        <div class="card-body">
          <div class="row mt-4">
            <div class="col-sm-4">

              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/code-p.png"><b>Product Code : </b><?php echo $edata["product_code"] ?><input type="hidden" name="pcode" id="pcode" value="<?php echo $edata["product_code"] ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/mdicine-red.png"><b>Description : </b><?php echo $edata["product_description"] ?><input type="hidden" name="pdesc" id="pdesc" value="<?php $edata["product_description"] ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/market.png"><b>Market : </b><?php echo $edata["market"]?><input type="hidden" name="market" id="market" value="<?php echo $edata["market"] ?>" /></div>
            </div>
          </div>
          <div class="row mt-4 mb-2">
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/code-bred.png"><b>Batch Code : </b><?php echo $edata["batch_code"]; ?><input type="hidden" name="bcode" id="bcode" value="<?php echo $edata["batch_code"]; ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/mg-red.png"><b>UOM : </b><?php echo $edata["UOM"]; ?><input type="hidden" name="UOM" id="UOM" value="<?php echo $edata["UOM"] ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/40-red.png"><b>Batch Size : </b><?php echo $edata["batch_size"]; ?><input type="hidden" name="bsizeUOM" id="bsizeUOM" value="<?php echo $edata["batch_size"]; ?>" /></div>
            </div>
          </div>
        </div>
      </div>


      <div class="card card-default">
        <div class="card-body">
          <div class="row mb-3">
      <div class="col-sm-12">

              <div><span class="ml18 float-left pt11"><h4>Equipment list</h4></span></div>
        <div class="float-right" hidden><span class="ml18 float-left pt11 mr15"><h4>Add portable equipment</h4></span><i data-toggle="modal" data-target="#myModalLarge" class="fa fa-plus default-icon-color default-icon-style float-right" aria-hidden="true"></i></div>
         </div>

         <section class="page-tasks" id="Listofequip">
          <ul class="task-list list-unstyled" id="eqlist">


            <?php $counter=0; foreach($row["response2"] as $row) { if($row->equipment_type == "Portable") { $counter++; ?> 

<input type="text" name="ids[]" value="<?= $row->ids?>" hidden>
  <li id='<?php echo "eqli" .$row->id ?>'> 
  <input type="hidden" name='<?php echo "hddeqdtl" .$row->id ?>' id='<?php echo "hddeqdtl" .$row->id ?>' data-id='<?php echo $row->id ?>' data-eqcode='<?php echo $row->equipment_code ?>' data-sopcode='<?php echo $row->sop_code ?>' data-eqtype='<?php echo $row->equipment_type ?>' />
  <span class="view" id='<?php echo "eqdiv" .$row->id ?>'>
    <div class="row"><div class="col-sm-12 pl0">

    <div class="row">
  <div class="col-sm-4 listexecutesop2"><img class="mb-2 mt-2 wt42" src="<?php echo base_url(); ?>img/icons/sifter.png"><span class="f15"><?php echo $row->equipment_name ?></span></div>
  <div class="col-sm-4 mt-4"><p class="f15 mt-2"><?php echo $row->sop_code ?> <?php echo $row->sop_name ?></p></div>
  <div class="col-sm-4 mt-4">
                  <div class="form-group">
                    <input class="form-control" type="text" placeholder="Change part" name="changepart[]" id="changepart<?= $counter?>" value="<?= $row->changepart?>">
                </div>
                </div>
  </div>
  </div>
  </div>
  </span> 
  </li>
  <?php } } ?>
            
          </ul>
      <ul class="task-list list-unstyled" id="porteqlist">
      </ul>
        </section>
        

      </div>
        </div>
          </div> 
    </form>
       
   


<div class="card card-default">
        <div class="card-body">

                  <div class="row disable-button-color">
      <div class="col-sm-4 text-right" id="sbtn"><button id="sbbtn" class="btn btn-success btn-lg" type="submit" disabled>&nbsp;&nbsp;&nbsp;&nbsp;Start&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      
      <div class="col-sm-4"><button id="finish" class="btn btn-danger btn-lg" type="button" <?=$finishbuttonstatus?>>&nbsp;&nbsp;&nbsp;&nbsp;Stop&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      <div class="col-sm-4"><button id="checkedby" class="btn btn-warning btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Shift Change&nbsp;&nbsp;&nbsp;&nbsp;</button>
      
      </div>
      <div class="col-sm-12 mt-2" hidden>

      <div class="card card-default" id="divftbl" style="display:none;">
               <div class="card-header p-5"><center>Activity Log Record</center></div>
               <table class="table table-dark" id="bdftbl">
                  <thead>
                     <tr tabindex=0>                        
                        <th scope="col">Start</th>
                        <th scope="col">End</th>
            <th scope="col">performed By</th>
            <th scope="col">Shift Change</th>
            <th scope="col">Activity</th>
                     </tr>
                  </thead>
                  <tbody>
                  
                  </tbody>
               </table>
            </div><!-- START row-->
      
      </div>
      </div>
</div>
</div>
     


<div class="card card-default" id="btn1">                     
            <div class="card-body" >
                <table class="table table-dark">
                        <thead>
                      <tr><th>Document No</th><th>Start Time</th><th>Stop Time</th><th>Performed By</th><th>Shift Change</th></tr>  
                      </thead>
					  <tbody>
                      <?php
                      foreach ($logs as $key => $log) {
                        ?>
                        <tr><td><?= $log["document_no"]?></td><td><?= $log["created_on"]?></td><td><?= $log["stoptime"]?></td><td><?= $log["performed_name"]?></td><td><?= $log["checked_name"]?></td></tr> 
                        <?php
                      }
                      ?>
					  </tbody>
                </table>
            </div>
          </div> 

        
    </div>
  </section>
  



  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 

<script>
function ftrrshow()
{
  var docid = $("#docid").val();
  var pcode = $("#pcode").val();
  var pdesc = $("#pdesc").val();
  var bcode = $("#bcode").val();
  var departmentname = $("#departmentname").val();
  var market = $("#market").val();
  var UOM = $("#UOM").val();
  var bsizeUOM = $("#bsizeUOM").val();
  var operation_type = "Portable";

  var postlist = {
       docid: docid,
       pcode: pcode,
       pdesc: pdesc,
       bcode: bcode,
       market: market,
       uom: UOM,
       bsizeuom: bsizeUOM,
       operation_type: operation_type,
       departmentname: departmentname
     };
  //postlist.push( );


  var myArray = [];
  i=1;
  $("#eqlist").find('input[name^="hddeqdtl"]').each(function () {
    var sopcode = $(this).attr("data-sopcode");
    var eqcode = $(this).attr("data-eqcode");
    var eqtype = $(this).attr("data-eqtype");
    var chgprt = $("#changepart"+i).val();
    myArray.push( {
       sopcode: sopcode,
       eqcode: eqcode,
       eqtype: eqtype,
       chgprt: chgprt
     });
    i++;
  });


  //x=1;
  $("#porteqlist").find('input[name^="hddeqdtl"]').each(function () {
  var sopcode = $(this).attr("data-sopcode");
  var eqcode = $(this).attr("data-eqcode");
  var eqtype = $(this).attr("data-eqtype");
  var cid = $(this).attr("data-id");
  var chgprt = $("#cpart1"+cid).val();
  myArray.push( {
     sopcode: sopcode,
     eqcode: eqcode,
     eqtype: eqtype,
     chgprt: chgprt
   });

  });


console.log(myArray);


$.ajax({
      url: "<?= base_url()?>Production/portableEquipmentcleaningsubmit",
      type: 'POST',
      data: {postlist:postlist,myArray:myArray},
      success: function(res) {
          //alert(res);
          //console.log(res);
          alert("Portable Equipment Cleaning is Succesfully Started");
          window.location.href = "<?= base_url()?>Production/portableEquipmentcleaningviewnow/"+res.id;
          //location.reload(true);
          //$("#myModal2").hide();
          //$("#bdftbl tr[tabindex=0]").focus();
          //$("#divftbl").show(1000);
          //$("#FTR").show();
          //$("#sbbtn").attr("disabled", true);
          
          //window.location.href="<?= base_url()?>Production/operation";

      }
    }); 


}


</script>
<script src="<?php echo base_url(); ?>js/app.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>



<script type="text/javascript">
  
  $(document).ready(function(){
        var performed_by = '<?= $cb?>';
    var actual_user = '<?= $this->session->userdata("empcode")?>';
    if(performed_by==actual_user){
      $("#checkedby").attr("disabled",true);
    }


  $("#finish").click(function(){
    debugger;
    var performed_by = '<?= $cb?>';
    var actual_user = '<?= $this->session->userdata("empcode")?>';
    var conform = confirm("Do You Really Want To Finish This?"); 
	  if(conform == true)
	  {
    if(performed_by!=actual_user){
      alert("Please press Shift Change first to takeover the task.");
    }
    else{

		
      $.ajax({
    url: "<?= base_url()?>Production/portableEquipmentcleaningfinish",
    type: 'POST',
    data: $("#sub").serialize(),
    success: function(res) {
        console.log(res);
        
        if(res.status==1){
          $("#myb").click();
          $("#head").html(res.msgheader);
          $("#message").html(res.msg);
          $("#disclose").attr("href","javascript:window.location.href=window.location.href");
          //alert("Portable Equipment Cleaning Completed");
          //window.location.href = "<?= base_url()?>Sanitization/dailyCleaningview";
          //location.reload(true);
          //$("#finish").attr("disabled",true);
        }
        
    }
    });
    }

	}});

  $("#checkedby").click(function(){


var docid = $("#docid").val();
  var pcode = $("#pcode").val();
  var pdesc = $("#pdesc").val();
  var bcode = $("#bcode").val();
  var departmentname = $("#departmentname").val();
  var market = $("#market").val();
  var UOM = $("#UOM").val();
  var bsizeUOM = $("#bsizeUOM").val();
  var operation_type = "Portable";
  var id = $("#id").val();
  var status = $("#status").val();
  var next_step = $("#next_step").val();


  var postlist = {
       id: id,
       status: status,
       next_step: next_step, 
       docid: docid,
       pcode: pcode,
       pdesc: pdesc,
       bcode: bcode,
       market: market,
       uom: UOM,
       bsizeuom: bsizeUOM,
       operation_type: operation_type,
       departmentname: departmentname
     };
  //postlist.push( );


  var myArray = [];
  i=1;
  $("#eqlist").find('input[name^="hddeqdtl"]').each(function () {
    var sopcode = $(this).attr("data-sopcode");
    var eqcode = $(this).attr("data-eqcode");
    var eqtype = $(this).attr("data-eqtype");
    var chgprt = $("#changepart"+i).val();
    myArray.push( {
       sopcode: sopcode,
       eqcode: eqcode,
       eqtype: eqtype,
       chgprt: chgprt
     });
    i++;
  });

var conform = confirm("Do You Really Want To Takeover This?"); 
	  if(conform == true)
	  {

    $.ajax({
    url: "<?= base_url()?>Production/portableEquipmentcleaningtakeover",
    type: 'POST',
    data: {postlist:postlist,myArray:myArray},
    //data: $("#sub").serialize(),
    success: function(res) {
        //console.log(res);
        
        if(res.status==1){
          $("#myb").click();
          $("#head").html(res.msgheader);
          $("#message").html(res.msg);
          $("#disclose").attr("href","<?= base_url()?>Production/portableEquipmentcleaningviewnow/"+res.id);
          //alert("Successfully taken over");
          //$(location).attr('href', '<?= base_url()?>Production/portableEquipmentcleaningviewnow/'+res.id);
        }
        
    }
    });


  }}); 
  });
</script>

<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" hidden id="myb" data-backdrop="static" data-keyboard="false"></button>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="head"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="message"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <a href="" class="btn btn-primary" id="disclose">Ok</a>
      </div>
    </div>

  </div>
</div>

</body>
</html>