<!-- Main section-->
  <!-- Main section-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
	<div class="content-heading executesop-heading">
        <div class="col-sm-5 pl-0">Select batch</div>
        <!--<form class="search-form col-sm-5 pl-0">
          <em class="icon-magnifier"></em>
          <input class="form-control" type="email" placeholder="Enter room code or number on name">
        </form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
             <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/area"><?php echo $this->session->userdata('area_code') ?></a></li>
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/room?area=<?php echo $this->session->userdata('area_code') ?>"><?php echo $this->session->userdata('room_code') ?></a></li>
			<li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>Production/activity?room=<?php echo $this->session->userdata('room_code') ?>"><?php echo $this->session->userdata('activity_code') ?></a></li>
            <!--<li class="breadcrumb-item">Execute SOP</li>-->
          </ol>
        </div>
      </div>	  
      <div class="card2 card card-default mt-1">		
        <div class="card-body mt-1">
		    <div class="form-group row mt-1">
          <?php $this->session->set_userdata('activity_code', $data['activity_code']); ?>
				<div class="col-md-1"></div>
        <div class="col-md-7"><div class="card-header fa-2x pt-1 pb-1 pl00 mb-15 mt-4">Enter a batch no. <font color="red">*</font></div></div>
				<div class="col-md-3"><div class="card-header fa-2x pt-1 pb-1 pl00 mb-15 mt-4">Enter lot no.</div></div>
        <div class="col-md-1">
        </div>
        </div>
        <form action="<?php echo base_url() ?>Production/operation" method="post" id="submit2" enctype="multipart/form-data">
        <div class="form-row align-items-center">
			   <div class="col-md-12">
					<div class="form-group row">
					<div class="col-md-1"></div>
          <div class="col-md-7">          
          <input class="form-control autocomplete" type="text" id="bcode" name="bcode" placeholder="Enter a batch number" />
          </div>
					<div class="col-md-3">					
					<input class="form-control" type="text" id="lotno" name="lotno" placeholder="Enter lot number" />
					</div>
          
					<div class="col-md-1"></div>
					</div>
			   </div>
        </div>  
            
            <div class="clearfix"></div>
            

			<div  class="row mt10" style="display:none;" id="showpr">
            <div class="col-md-11 offset-1">
          <div class="row">
            <div class="col-sm-3">
              <div class="listexecutesop f15"><img src="<?php echo base_url(); ?>img/icons/code-p.png" class="wt42"><b>Product code : </b><span id="pro-code"></span><input type="hidden" id="hddpro_code" name="hddpro_code" value="" /></div>
            </div>
            <div class="col-sm-5"> 
              <div class="listexecutesop f15"><img src="<?php echo base_url(); ?>img/icons/mdicine-red.png" class="wt42"><b>Description : </b><span id="pro-desc"></span><input type="hidden" id="hddpro_desc" name="hddpro_desc" value="" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop f15"><img src="<?php echo base_url(); ?>img/icons/market.png" class="wt42"><b>Market : </b><span id="market"></span><input type="hidden" id="hddmarket" name="hddmarket" value="" /></div>
            </div>
          </div>
          <div class="row mt-4 mb-2">
            <div class="col-sm-3">
              <div class="listexecutesop f15"><img src="<?php echo base_url(); ?>img/icons/code-bred.png" class="wt42"><b>Batch : </b><span id="batchcode"></span><input type="hidden" id="hddbatchcode" name="hddbatchcode" value="" /></div>
            </div>
            <div class="col-sm-5">
              <div class="listexecutesop f15"><img src="<?php echo base_url(); ?>img/icons/mg-red.png" class="wt42"><b>Unit : </b><span id="UOM"></span><input type="hidden" id="hddUOM" name="hddUOM" value="" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop f15"><img src="<?php echo base_url(); ?>img/icons/40-red.png" class="wt42"><b>Batch size : </b><span id="batchsizeUOM"></span><input type="hidden" id="hddbatchsizeUOM" name="hddbatchsizeUOM" value="" /></div>
            </div>
          </div>
          </div>
      </div>

      <div  class="row mt10" style="display:none;" id="showpr2">
        <form id="batchdtl"> 
        <div class="col-md-10 offset-1">
          <div class="row">
            <div class="col-sm-3">
            <div class="listexecutesop f15"><b>Unit : </b><br/>
            <select class="form-control" id="UOMinput" name="UOMinput">
              <option selected disabled value="0">Select Unit Here</option>
              <option value="mg">mg</option>
              <option value="gram">gram</option>
              <option value="ml">ml</option>
              <option value="litre">litre</option>
            </select>
            <input type="hidden" id="hddUOM2" name="hddUOM2" value="" />
            <!--<input type="text" class="form-control" id="UOMinput" name="UOMinput" />-->
            </div>             
            </div>
            <div class="col-sm-5">
            <input type="hidden" id="hddbatchcode2" name="hddbatchcode2" value="" />
              <div class="listexecutesop f15"><b>Batch Description : </b><br/><input type="text" class="form-control" id="BatchDesc" name="BatchDesc" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop f15"><b>Batch size : </b><br/><input type="text" class="form-control" id="Batchsize" name="Batchsize" /><input type="hidden" id="hddbatchsizeUOM2" name="hddbatchsizeUOM2" value="" /></div>
            </div>
          </div>
          <div class="row mt-4 mb-2">
            <div class="col-sm-3">
              <div class="listexecutesop f15">                
                <b>Product code : </b><span id="pro-code"></span>
                <Select class="form-control" name="product" id="product" required>
                <option selected disabled value="0">Select Product Here</option>
                <?php if(count($data['product']->result())) { ?>
                <?php foreach($data['product']->result() as $row) { ?>
                <option value="<?php echo $row->product_code ?>"><?php echo $row->product_code ?></option>
                <?php } } ?> 
                </Select>
                <input type="hidden" id="hddpro_code2" name="hddpro_code2" value="" />  
              </div>
            </div>
            <div class="col-sm-5"> 
              <div class="listexecutesop f15"><img src="<?php echo base_url(); ?>img/icons/mdicine-red.png" class="wt42"><b>Description : </b><span id="pro-desc2"></span><input type="hidden" id="hddpro_desc2" name="hddpro_desc2" value="" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop f15"><img src="<?php echo base_url(); ?>img/icons/market.png" class="wt42"><b>Market : </b><span id="market2"></span><input type="hidden" id="hddmarket2" name="hddmarket2" value="" /></div>
            </div>
          </div>
          <div class="row mt50 mb15">
            <div class="col-sm-12 text-center"><a href="#"  id="btnbounce2"><img id="imgbounce2" src="<?php echo base_url(); ?>img/right-arrow.png" width="40"></a></div>
          </div>
          </div>
        </form>
      </div>

			    <div class="row mt50 mb15">
            <div class="col-sm-12 text-center"><a href="#"  id="btnbounce" style="display: none;"><img id="imgbounce" src="<?php echo base_url(); ?>img/right-arrow.png" width="40"></a></div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </section>
   <!-- Page footer-->
  <footer class="footer-container text-center"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>


<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 
<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/flot/jquery.flot.time.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="<?php echo base_url(); ?>js/app.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script>
$(document).ready(function()
{
  $('#btnbounce2').click(function() {
    debugger;
    var hddbatchcode2 = $('#hddbatchcode2').val();
    var BatchDesc = $('#BatchDesc').val();
    var UOMinput = $('#UOMinput').val();
    var Batchsize = $('#Batchsize').val();
    var hddpro_code2 = $('#hddpro_code2').val();
    var hddpro_desc2 = $('#hddpro_desc2').val();
    var hddmarket2 = $('#hddmarket2').val();
     $.ajax({
       url: '<?= base_url()?>Production/AddBatch',
       type: 'POST',
       data: {hddbatchcode2:hddbatchcode2,BatchDesc:BatchDesc,UOMinput:UOMinput,Batchsize:Batchsize,hddpro_code2:hddpro_code2,hddpro_desc2:hddpro_desc2,hddpro_desc2:hddpro_desc2,hddmarket2:hddmarket2},
       success: function (res) {
        debugger;
         console.log(res);
          if(res.status==0)
          {
              alert("successfully saved.");
              var bcode=$("#bcode").val();
              $("#hddbatchcode").val($("#bcode").val());
              $("#hddbatchcode2").val($("#bcode").val());
              $("#hddbatchsizeUOM").val($("#Batchsize").val()+" "+$("#UOMinput").val());
              $("#hddbatchsizeUOM2").val($("#Batchsize").val()+" "+$("#UOMinput").val());
              $("#hddUOM").val($("#UOMinput").val());

              //chk cleaning for diff batch
              $.ajax({
              url: "<?= base_url()?>Production/chkcleaning2",
              type: 'POST',
              data: {bcode:bcode},
              success: function(res) {
              console.log(res);
              if(res=="-1")
              {
                alert("Please do Type A or Type B cleaning First, Because you are running different batch."); 
              }
              else
              {
                $('#submit2').submit();
              }
              }
              });
              //end cleaning chk
          }
          else
          {
            alert("Not saved.");
          }
       }
     });
     return false;
  });



  $("#product").change(function() 
  {
    debugger;
    var productcode = $(this).val();

    $("#hddbatchcode2").val($("#bcode").val());
    $.ajax({
      url: "<?= base_url()?>Production/Getproduct_Bycode",
      type: 'POST',
      data: {productcode:productcode},
      success: function(res) {
        debugger;
          console.log(res);
          $("#hddpro_code").val(res.product_code);
          $("#hddpro_code2").val(res.product_code);
          $("#pro-desc").text(res.product_name);
          $("#pro-desc2").text(res.product_name);
          $("#hddpro_desc").val(res.product_name);
          $("#hddpro_desc2").val(res.product_name);
          $("#market").text(res.product_market);
          $("#market2").text(res.product_market);
          $("#hddmarket").val(res.product_market);
          $("#hddmarket2").val(res.product_market);
      }
    });
  }); 

  $(".autocomplete").change(function() 
  {
    var bcode = $(this).val();
    $.ajax({
      url: "<?= base_url()?>Production/batchdata",
      type: 'POST',
      data: {bcode:bcode},
      success: function(res) {
          console.log(res);
          if(res != null && res !="")
          {
          $("#showpr").hide(1000);
          $("#showpr2").hide(1000);
          $("#btnbounce").hide(1000);
          $("#pro-code").text(res.product_code);
          $("#pro-desc").text(res.product_description);
          $("#market").text(res.market);
          $("#batchcode").text(res.batch_code);
          $("#batchsizeUOM").text(res.batch_size+" "+res.UOM);
          $("#UOM").text(res.UOM);
          $("#hddpro_code").val(res.product_code);
          $("#hddpro_desc").val(res.product_description);
          $("#hddmarket").val(res.market);
          $("#hddbatchcode").val(res.batch_code);
          $("#hddbatchsizeUOM").val(res.batch_size+" "+res.UOM);
          $("#hddUOM").val(res.UOM);
          $("#showpr").show(1000);
          $("#btnbounce").show(1000);
          }
          else
          {
            $("#btnbounce").hide(1000);
            $("#showpr").hide(1000);
            $("#showpr2").show(1000);

            $("#hddpro_code").val("");
            $("#hddpro_code2").val("");
            $("#pro-desc").text("");
            $("#pro-desc2").text("");
            $("#hddpro_desc").val("");
            $("#hddpro_desc2").val("");
            $("#market").text("");
            $("#market2").text("");
            $("#hddmarket").val("");
            $("#hddmarket2").val("");

            $("#hddbatchcode").val("");
            $("#hddbatchcode2").val("");
            $("#hddbatchsizeUOM").val("");
            $("#hddbatchsizeUOM2").val("");
            $("#hddUOM").val("");
          }
      }
    });
  });  

  $(".autocomplete").keyup(function() 
  {
    var bcode = $(this).val();
    if(bcode!="" && bcode.length>=1){  
      $.ajax({
        url: "<?= base_url()?>Production/batchcode",
        type: 'POST',
        data: {bcode:bcode},
        success: function(res) {
          //alert(res);
          availableTags = res;   
          $(".autocomplete").autocomplete({
          source: availableTags
          });
        }
      });
    }
  });
});
</script>
<script type="text/javascript">

$( "#imgbounce" ).click(function() {

  //alert("Hi");

  if($("#bcode").val()<=0){
    alert("Please enter batch code");
    $("#showpr").hide(1000);
          $("#btnbounce").hide(1000);
    $("#bcode").focus();
  }
  else
  {
              var bcode=$("#bcode").val();
              //chk cleaning for diff batch
              $.ajax({
              url: "<?= base_url()?>Production/chkcleaning2",
              type: 'POST',
              data: {bcode:bcode},
              success: function(res) {
              console.log(res);
              if(res=="-1")
              {
                alert("Please do Type A or Type B cleaning First, Because you are running different batch."); 
              }
              else
              {                
                if($("#lotno").val()<=0){
                $("#lotno").val('N/A');
                }
                $('#submit2').submit();
              }
              }
              });
              //end cleaning chk    
}

/*function(){ 
  debugger;
    var pcode = $("#hddpro_code").val();
    var pdesc = $("#hddpro_desc").val();
    var market = $("#hddmarket").val();
    var bcode = $("#hddbatchcode").val();
    var bsizeUOM = $("#hddbatchsizeUOM").val();
    var UOM = $("#hddUOM").val();
    $.ajax({
      url: "<?= base_url()?>Production/operation",
      type: 'POST',
      data: {pcode:pcode, pdesc:pdesc, market:market, bcode:bcode, bsizeUOM:bsizeUOM, UOM:UOM},
      success: function(res) {
          //alert(res);          
      }
    });
})*/
});


</script>
</body>
</html>
