  <!-- Main section-->
<form action="#" method="post" autocomplete="off" enctype="multipart/form-data">
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <?php if($this->session->userdata('activity_code') == 2) { ?>
             <div class="col-sm-5 pl-0">Type A Cleaning</div>
            <?php } else { ?>
             <div class="col-sm-5 pl-0">Type B Cleaning</div>
            <?php } ?>
        <!-- <form class="search-form col-sm-5 pl-0"> <em class="icon-magnifier"></em><input class="form-control" type="email" placeholder="Enter room code or number on name"></form>-->
        <div class="col-sm-7 pr-0">
          <ol class="breadcrumb ml-auto">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>User/home">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/area"><?php echo $this->session->userdata('area_code') ?></a></li>
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/room?area=<?php echo $this->session->userdata('area_code') ?>"><?php echo $this->session->userdata('room_code') ?></a></li>
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>Production/activity?room=<?php echo $this->session->userdata('room_code') ?>"><?php echo $this->session->userdata('activity_code') ?></a></li>
      <li class="breadcrumb-item active"><a href="<?php echo base_url(); ?>Production/batch?activitycode=<?php echo $this->session->userdata('activity_code') ?>"><?php echo $this->session->userdata('bcode') ?></a></li>
          </ol>
        </div>
      </div>
	  <div class="card card-default">
      <div class="card-body">
      <div class="row">
      <div class="col-sm-4">
      <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/area-operation-page.png"><b>Area : </b><?php echo $this->session->userdata('area_code') ?>
      </div>
      </div>      
      <div class="col-sm-4">
      <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/area-operation-page.png"><b>Room : </b><?php echo $this->session->userdata('room_code') ?></div>
      </div>
      </div>
    </div>
  </div>
	  <div class="card card-default">
        <div class="card-body">
			<div class="row">
      <div class="col-sm-4">
      <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/document-icon.png"><b>Document No. : </b><?php echo $data['docid'] ?><input type="hidden" name="docid" id="docid" value="<?php echo $data['docid'] ?>" /></div>
      </div>
      <div class="col-sm-4">
      <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/department.png"><b>Department Name : </b><?php echo $data['departmentname'] ?></div>
      </div>
      </div>
        </div>
      </div>
	  
      <div class="card card-default">
        <div class="card-body">
          <div class="row mt-4">
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/code-p.png"><b>Product code : </b><?php echo $this->session->userdata('pcode'); ?><input type="hidden" name="pcode" id="pcode" value="<?php echo $this->session->userdata('pcode'); ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/mdicine-red.png"><b>Description : </b><?php echo $this->session->userdata('pdesc'); ?><input type="hidden" name="pdesc" id="pdesc" value="<?php echo $this->session->userdata('pdesc'); ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/market.png"><b>Market : </b><?php echo $this->session->userdata('market'); ?><input type="hidden" name="market" id="market" value="<?php echo $this->session->userdata('market'); ?>" /></div>
            </div>
          </div>
          <div class="row mt-4 mb-2">
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/code-bred.png"><b>Batch : </b><?php echo $this->session->userdata('bcode'); ?><input type="hidden" name="bcode" id="bcode" value="<?php echo $this->session->userdata('bcode'); ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/mg-red.png"><b>Unit : </b><?php echo $this->session->userdata('UOM'); ?><input type="hidden" name="UOM" id="UOM" value="<?php echo $this->session->userdata('UOM'); ?>" /></div>
            </div>
            <div class="col-sm-4">
              <div class="listexecutesop"><img src="<?php echo base_url(); ?>img/icons/40-red.png"><b>Batch size : </b><?php echo $this->session->userdata('bsizeUOM'); ?><input type="hidden" name="bsizeUOM" id="bsizeUOM" value="<?php echo $this->session->userdata('bsizeUOM'); ?>" /></div>
            </div>
          </div>
        </div>
      </div>
      <div class="card card-default">
        <div class="card-body">
          <div class="row mb-3">
		  <div class="col-sm-12">		  
              <div><span class="ml18 float-left pt11"><h4>Equipment list</h4></span></div>
        			  
            </div>
		  <div class="modal-body">
                    <section class="page-tasks" id="Listofequip">
          <ul class="task-list list-unstyled" id="eqlist">
            <?php $counter=0; foreach($data['fixequipment']->result() as $row) { $counter++; ?> 
  <li id='<?php echo "eqli" .$row->id ?>'> 
  <input type="hidden" name='<?php echo "hddeqdtl" .$row->id ?>' id='<?php echo "hddeqdtl" .$row->id ?>' data-id='<?php echo $row->id ?>' data-eqcode='<?php echo $row->equipment_code ?>' data-sopcode='<?php echo $row->sop_code ?>' data-eqtype='<?php echo $row->equipment_type ?>' />
  <span class="view" id='<?php echo "eqdiv" .$row->id ?>'><div class="row"><div class="col-sm-12 pl0"><div class="row">
  <div class="col-sm-4 listexecutesop2"><img class="mb-2 mt-2 wt42" src="<?php echo base_url(); ?>img/icons/<?php echo $row->equipment_icon ?>"><span class="f15"><?php echo $row->equipment_name ?></span></div>
  <?php if($row->filepath != null && $row->filepath != "") { ?>
  <div class="col-sm-8 mt-4"><p class="f15 mt-2"><a href="#" onclick="showmodel('<?php echo $row->filepath; ?>');"><?php echo $row->sop_code ?> <?php echo $row->sop_name ?></a></p></div>
  <?php } else { ?>
  <div class="col-sm-8 mt-4"><p class="f15 mt-2"><?php echo $row->sop_code ?> <?php echo $row->sop_name ?></p></div>
  <?php } ?> 
  </div>
  
  </div>
  </div>
  </span> 
  </li>
  <?php } ?>
            
          </ul>
		  <ul class="task-list list-unstyled" id="porteqlist">
		  </ul>
        </section>
		</form>
		</div>
          </div>		
		  
		  <div class="row disable-button-color">
      <div class="col-sm-4 text-right" id="sbtn"><span id="demo3" class="pr-2 f16"></span><button id="sbbtn" class="btn btn-success btn-lg" type="button" disabled="disabled" onClick="ftrrshow();">&nbsp;&nbsp;&nbsp;&nbsp;Start&nbsp;&nbsp;&nbsp;&nbsp;</button></div> 
			<div class="col-sm-4 text-right" id="rsbtn" style="display:none;"><span id="demo3" class="pr-2 f16"></span><button id="btnrs" class="btn btn-success btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Restart&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
			<div class="col-sm-4"><button id="fbtn" onclick="stopoperation();" class="btn btn-danger btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Stop&nbsp;&nbsp;&nbsp;&nbsp;</button></div>
      <div class="col-sm-4"><button id="chkby" onclick="checkedbyoperation();" class="btn btn-warning btn-lg" type="button">&nbsp;&nbsp;&nbsp;&nbsp;Shift Change&nbsp;&nbsp;&nbsp;&nbsp;</button>
      <span class="fa-stack mt-2 float-right" name="FTR" id="FTR" data-flag="1" onclick="FTRshowtme();">			
				  <em class="fa fa-circle fa-stack-2x text-success bgred"></em>
				  <em class="far fa-clock fa-stack-1x fa-inverse text-white"></em>
			</span>
			</div>
			<div class="col-sm-12 mt-2">
      <div class="card card-default" id="divftbl">
			
               <div class="card-header p-5"><center>Activity Log Record</center></div>
               <table class="table table-dark" id="bdftbl">
                  <thead>
                     <tr tabindex=0>                        
                        <th scope="col">Start</th>
                        <th scope="col">End</th>
						<th scope="col">Performed By</th>
						<th scope="col">Change over</th>
						<th scope="col">Activity</th>
                     </tr>
                  </thead>
                  <tbody>
          				<?php foreach($data['headerlogdtl']->result() as $row) { ?> 
          <tr>
            <td><?php echo $row->start_time ?></td>
            <td><?php echo $row->end_time ?></td>
            <td><?php echo $row->performed_by ?></td>
            <td><?php echo $row->checked_by ?></td>
            <?php if($this->session->userdata('activity_code') == 2) { ?>
            <td>type A</td>
            <?php } else{ ?>
            <td>type B</td>
            <?php } ?>
            
          </tr> 
          <?php } ?>
                  </tbody>
               </table>
            </div><!-- START row-->
			
			</div>
			
		  </div>
		  
        </div>
      </div>
    </div>
  </section>
  
  <!-- Page footer-->
  <footer class="footer-container text-center pb-1"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>
<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 

<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script> 
<script src="<?php echo base_url(); ?>vendor/jquery/dist/jquery.js"></script> 
<script src="<?php echo base_url(); ?>vendor/popper.js/dist/umd/popper.js"></script> 
<script src="<?php echo base_url(); ?>vendor/bootstrap/dist/js/bootstrap.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="js/app.js"></script> 
<!--PDF Model code-->
  <div class="modal fade show" id="mypdfModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabelLarge" aria-modal="true" style="top:2%;left:5%;padding-right: 17px;display: none;width:90%;min-height: 500px;height: 100%;">
  <div class="modal-dialog modal-lg" style="display:contents;">
    <div class="modal-content">
      <div class="modal-body">
    <iframe src="" style="width: 100%;height: 460px;border: none;" toolbar="true" id="pdfid"></iframe>
      </div>
    <div class="modal-footer" style="justify-content: center;">
      <div class="modal-button-container" id="cancel"><button type="button" data-dismiss="modal" aria-label="Close" class="btn btn-danger btn-lg bg-danger" onclick="$('#mypdfModal').hide();">Close</button></div>
    </div>
    </div>
  </div>
</div>
<!-- model end -->

<script>
function showmodel(obj)
{
    $('#pdfid').attr('src','<?php echo base_url(); ?>uploads/'+obj);
    $('#mypdfModal').show();
}
function test_ftrrshow()
{
  var docid = $("#docid").val();
  var pcode = $("#pcode").val();
  var pdesc = $("#pdesc").val();
  var bcode = $("#bcode").val();
  var market = $("#market").val();
  var lotno = "";
  var UOM = $("#UOM").val();
  var bsizeUOM = $("#bsizeUOM").val();
  var operation_type = '<?php echo $this->session->userdata('activityType'); ?>';

  var myArray = [];
$("#eqlist").find('input[name^="hddeqdtl"]').each(function () {
  var sopcode = $(this).attr("data-sopcode");
  var eqcode = $(this).attr("data-eqcode");
  var eqtype = $(this).attr("data-eqtype");
  myArray.push( {
     sopcode: sopcode,
     eqcode: eqcode,
     eqtype: eqtype
   });

});
$("#porteqlist").find('input[name^="hddeqdtl"]').each(function () {
  var sopcode = $(this).attr("data-sopcode");
  var eqcode = $(this).attr("data-eqcode");
  var eqtype = $(this).attr("data-eqtype");
  myArray.push( {
     sopcode: sopcode,
     eqcode: eqcode,
     eqtype: eqtype
   });

});

  //var pcode = $("#pcode").val();
  //var pcode = $("#pcode").val();
    $.ajax({
      url: "<?= base_url()?>Production/makeinitiallog",
      type: 'POST',
      data: {docid:docid,bcode:bcode,lotno:lotno,myArray:myArray,operation_type:operation_type},
      success: function(res) {
          //alert(res);
          alert("Operation is succesfully started...");
          location.reload(true);
          $("#myModal2").hide();
          $("#bdftbl tr[tabindex=0]").focus();
          $("#divftbl").show(1000);
          $("#FTR").show();
          $("#sbbtn").attr("disabled", true);
      }
    });	
}

function stopoperation()
{
  var r = confirm("Do you really want to stop this ?");
  if(r == true)
  {
    var bcode = $("#bcode").val();
    var lotno = "";
    var operation_type = '<?php echo $this->session->userdata('activityType'); ?>';
    $.ajax({
      url: "<?= base_url()?>Production/makefinishlog",
      type: 'POST',
      data: {bcode:bcode,lotno:lotno,operation_type:operation_type},
      success: function(result) {
        debugger;
        var logrecrd="";
        console.log(result);
          if(result=="0")
          {
            alert("Please takeover this operation first...");
          }
          else
          {
            var logrecrd = "<thead><tr tabindex=0><th scope='col'>Start</th><th scope='col'>End</th><th scope='col'>Performed By</th><th scope='col'>Change over</th><th scope='col'>Activity</th></tr></thead>";
            $.each(result.query,function(index,value) 
            {
                logrecrd = logrecrd.concat('<tr><td>'+value.start_time+'</td><td>'+value.end_time+'</td><td>'+value.performed_by+'</td><td>'+value.checked_by+'</td><?php if($this->session->userdata('activity_code') == 2) { ?><td>type A</td><?php } else{ ?><td>type B</td><?php } ?></tr>');
            });
            $("#bdftbl").html(logrecrd);
          //alert(res);
          //location.reload(true); 
          $("#fbtn").attr("disabled", true); 
          $("#chkby").attr("disabled", true); 
          $("#Msgmyb").click();
          $("#Msghead").html(result.msgheader);
          $("#Msgmessage").html(result.msg); 


          //location.reload(true); 
        }
          //window.location.href="<?= base_url()?>User/home";
      }
    }); 
  }
}

function checkedbyoperation()
{
  debugger;
  var r = confirm("Do you really want to takeover this ?");
  if(r == true)
  {
    var bcode = $("#bcode").val();
    var lotno = "";
    var operation_type = '<?php echo $this->session->userdata('activityType'); ?>';
    $.ajax({
      url: "<?= base_url()?>Production/makechkbylog",
      type: 'POST',
      data: {bcode:bcode,lotno:lotno,operation_type:operation_type},
      success: function(res) {
          console.log(res);
          if(res=="0")
          {
            alert("This operation can't be performed.");
          }
          else
          {
            var logrec = "<thead><tr tabindex=0><th scope='col'>Start</th><th scope='col'>End</th><th scope='col'>Performed By</th><th scope='col'>Change over</th><th scope='col'>Activity</th></tr></thead>";
            $.each(res.query, function (index, value) 
            {
                logrec = logrec.concat('<tr><td>'+value.start_time+'</td><td>'+value.end_time+'</td><td>'+value.performed_by+'</td><td>'+value.checked_by+'</td><?php if($this->session->userdata('activity_code') == 2) { ?><td>type A</td><?php } else{ ?><td>type B</td><?php } ?></tr>');
            });
            $("#bdftbl").html(logrec);
            $("#chkby").attr("disabled", true);

            $("#Msgmyb").click();
           $("#Msghead").html(res.msgheader);
           $("#Msgmessage").html(res.msg);
          } 
      }
    }); 
  }
}

function FTRshowtme()
{	
debugger;
	if($("#FTR").attr("data-flag")=="0")
	{
		$("#FTR").attr("data-flag", "1");
		$("#divftbl").show(1000);
	}
	else
	{
		$("#FTR").attr("data-flag", "0");
		$("#divftbl").hide(1000);
		
	}
}
function showtme(obj)
{	
debugger;
	if($("#t"+obj).attr("data-flag")=="0")
	{
		$("#t"+obj).attr("data-flag", "1");
		$("#divbdtbl"+obj).show(1000);
	}
	else
	{
		$("#t"+obj).attr("data-flag", "0");
		$("#divbdtbl"+obj).hide(1000);
		
	}
}
function araechange()
{
	var area = $("#areaddl").val();
	$("#area").text(area);
}

function chngclr(obj,divid)
{
	debugger;
	var check = document.getElementById(obj);
		if(check.checked == true){
	document.getElementById(divid).style.backgroundColor = "#b0be3c";
	document.getElementById(divid).style.color = "white";
	}else{
		document.getElementById(divid).style.backgroundColor = "white";
		document.getElementById(divid).style.color = "#000";
		}
}
function showpequip()
{
debugger;
	$("#pli4").show(100);
	$("#pli5").show(200);
	$("#pli6").show(300);
	$("#showall").hide();
	$("#showless").show();
}
function hidepequip()
{
debugger;
	$("#pli4").hide(100);
	$("#pli5").hide(200);
	$("#pli6").hide(300);
	$("#showall").show();
	$("#showless").hide();
}
function disabledpopup()
{
debugger;
//location.reload(true);
//$('#myMsgModal').hide(); 
//$('.modal-backdrop').hide();
$("#Msgmyb").click();
}
</script>
<script>
var dt='';
var dt1='';
var stt='';
var ft='';
var ct='';
function brkdown(obj)
{
debugger;	
var memonumber = $("#memonumber").val();
  var memoremark = $("#memoremark").val();
  var sopcode = $("#hddeqdtl"+obj).attr("data-sopcode");
  var eqcode = $("#hddeqdtl"+obj).attr("data-eqcode");
  var eqtype = $("#hddeqdtl"+obj).attr("data-eqtype");
  var bdstarttime = "";
  var bdendtime = "";
  $.ajax({
      url: "<?= base_url()?>Production/makestartbrkdwnlog",
      type: 'POST',
      data: {sopcode:sopcode,eqcode:eqcode,eqtype:eqtype,memonumber:memonumber,memoremark:memoremark,bdstarttime:bdstarttime,bdendtime:bdendtime},
      success: function(res) {
      
    $("#btnbd"+obj).hide();
    $("#end"+obj).show();
    //$("#t"+obj).show();
    $("#fbtn").attr("disabled", true);
    $("#sbtn").hide();
    $("#rsbtn").show();
    $("#btnrs").attr("disabled", true);
    var dt = new Date();
    stt = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
    location.reload(true);
    }
  });
  
}
function brkdownover(obj)
{
  var dt1 = new Date();
  ft = dt1.getHours() + ":" + dt1.getMinutes() + ":" + dt1.getSeconds();
  var memonumber = $("#memonumber").val();
  var memoremark = $("#memoremark").val();
  var sopcode = $("#hddeqdtl"+obj).attr("data-sopcode");
  var eqcode = $("#hddeqdtl"+obj).attr("data-eqcode");
  var eqtype = $("#hddeqdtl"+obj).attr("data-eqtype");
  var bdstarttime = "";
  var bdendtime = "";
  $.ajax({
      url: "<?= base_url()?>Production/makeendbrkdwnlog",
      type: 'POST',
      data: {sopcode:sopcode,eqcode:eqcode,eqtype:eqtype,memonumber:memonumber,memoremark:memoremark,bdstarttime:bdstarttime,bdendtime:bdendtime},
      success: function(res) {
          //alert(res);
          //alert("Operation is succesfully started...");
          //location.reload();
          $("#btnbd"+obj).show();
          $("#end"+obj).hide();
          $("#t"+obj).show();
          $("#t"+obj).attr("data-flag", "1");
          $("#fbtn").attr("disabled", false);
          $("#sbtn").hide();
          $("#btnrs").attr("disabled", false);
          $("#divbdtbl"+obj).show(1000);
          //$("#bdtbl"+obj).html("");
          location.reload(true);
          //$("#bdtbl"+obj).append("<tr><td>"+stt+"</td><td>"+ft+"</td><td>00:00:00</td><td>Bhupendra kumar</td></tr>");
      }



    });
  //location.reload();
}
function addpequip()
{
	debugger;
	$("#porteqlist").html("");
	var newRow = $("<li>");
	//counter = 6;
	$("#peqlist").find('input[name^="check"]').each(function () {	
	if($(this).prop('checked') == true)
	{	
	var name = $(this).attr("data-name");
	var imgurl = $(this).attr("data-imgurl");
  var counter = $(this).attr("data-id");
  var sopdtl = $(this).attr("data-sopdtl");
  var sopcode = $(this).attr("data-sopcode");
  var eqcode = $(this).attr("data-eqcode");
  var eqtype = $(this).attr("data-eqtype");
  //var counter++;
    var cols = "";
	cols += "<input type='hidden' name='hddeqdtl"+counter+"' id='hddeqdtl"+counter+"' data-id='"+counter+"' data-eqcode='"+eqcode+"' data-sopcode='"+sopcode+"' data-eqtype='"+eqtype+"' /> ";
	cols += "<span class='view' id='eqdiv"+counter+"'><div class='row'><div class='col-sm-12 pl0'><div class='row'>";
    cols += "<div class='col-sm-4 listexecutesop2'><img class='mb-2 mt-2 wt42' src='"+imgurl+"'><span class='f15'>"+name+"</span></div>";
	cols += "<div class='col-sm-5 mt-4'><p class='f15 mt-2'>"+sopdtl+"</p></div>";
    cols += "<div class='col-sm-3 disable-button-color float-right text-right pl00' id='show"+counter+"'>";
	cols += "<span class='fa-stack mt-3' style='display:none;' name='t"+counter+"' id='t"+counter+"' data-flag='1' onclick='showtme("+counter+");'><em class='fa fa-circle fa-stack-2x text-success bgred'></em><em class='far fa-clock fa-stack-1x fa-inverse text-white'></em></span>";			
	cols += "<button class='btn btn-labeled btn-danger mt-4 mb-2' type='button' onClick='brkdown("+counter+")' name='btnbd"+counter+"' id='btnbd"+counter+"'><span class='btn-label'><i class='fa fa-times'></i></span>Breakdown</button>";
	cols += "&nbsp;<button class='btn btn-labeled btn-success mb-2 mt-4 pl10' type='button' onClick='memo("+counter+")' name='end"+counter+"' id='end"+counter+"' style='display:none;'><span class='btn-label'><i class='fa fa-check'></i></span>End</button>";
	cols += "<div class='card card-default' id='divbdtbl"+counter+"' style='display:none;'><div class='card-header p-5'><center>Breakdown Time Record</center></div><table class='table table-dark' id='bdtbl"+counter+"'><thead><tr><th scope='col'>Start</th><th scope='col'>End</th><th scope='col'>Elapsed</th><th scope='col'>Operated By</th></tr></thead><tbody></tbody></table></div></div></div></div></div></span></li>";
    newRow.append(cols);      
	}
	
	});
	$("#porteqlist").append(newRow);
}
</script>
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myMsgModal" hidden id="Msgmyb" data-backdrop="static" data-keyboard="false"></button>

<div id="myMsgModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title pull-left" id="Msghead"></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>
      <div class="modal-body">
        <p id="Msgmessage"></p>
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>-->
        <!--data-dismiss="modal"-->
        <button class="btn btn-success" id="yes" onclick="disabledpopup();">ok</button>
      </div>
    </div>

  </div>
</div>
</body>
</html>