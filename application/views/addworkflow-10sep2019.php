<?php
// Turn off all error reporting
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="Bootstrap Admin App">
<meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
<link rel="icon" type="image/x-icon" href="<?php echo base_url(); ?>favicon.ico">
<title>SMHS - eLog System</title>
<!-- =============== VENDOR STYLES ===============-->
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/brands.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/regular.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/solid.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/@fortawesome/fontawesome-free/css/fontawesome.css">
<!-- SIMPLE LINE ICONS-->
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/simple-line-icons/css/simple-line-icons.css">
<!-- ANIMATE.CSS-->
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/animate.css/animate.css">
<!-- WHIRL (spinners)-->
<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/whirl/dist/whirl.css">
<!-- =============== PAGE VENDOR STYLES ===============-->
<!-- =============== BOOTSTRAP STYLES ===============-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap.css" id="bscss">
<!-- =============== APP STYLES ===============-->
<link rel="stylesheet" href="<?php echo base_url(); ?>css/app.css" id="maincss">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>css/Bhupeestyle.css">
</head>

<body>
<div class="wrapper"> 
  <!-- top navbar-->
  <header class="topnavbar-wrapper"> 
    <!-- START Top Navbar-->
    <nav class="navbar topnavbar"> 
      <!-- START navbar header-->
      <div class="navbar-header"><a class="navbar-brand" href="<?php echo base_url(); ?>logo">
        <div class="brand-logo font-weight-bold text-white">SMHS - eLog System</div>
        <div class="brand-logo-collapsed font-weight-bold text-white">SMHS</div>
        </a></div>
      <!-- END navbar header--> 
      <!-- START Left navbar-->
      <ul class="navbar-nav mr-auto flex-row">
        <li class="nav-item"> 
          <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link d-none d-md-block d-lg-block d-xl-block" href="#" data-trigger-resize="" data-toggle-state="aside-collapsed"><em class="fas fa-bars"></em></a><!-- Button to show/hide the sidebar on mobile. Visible on mobile only.--><a class="nav-link sidebar-toggle d-md-none" href="#" data-toggle-state="aside-toggled" data-no-persist="true"><em class="fas fa-bars"></em></a></li>
        <!-- START User avatar toggle-->
        <li class="nav-item d-none d-md-block"> 
          <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops--><a class="nav-link" id="user-block-toggle" href="#user-block" data-toggle="collapse"><em class="icon-user"></em></a></li>
        <!-- END User avatar toggle--> 
      </ul>
      <!-- END Left navbar--> 
      <!-- START Right Navbar-->
      <ul class="navbar-nav flex-row">
        <!-- Fullscreen (only desktops)-->
        <li class="nav-item d-none d-md-block"><a class="nav-link" href="<?php echo base_url(); ?>#" data-toggle-fullscreen=""><em class="fas fa-expand"></em></a></li>
        <!-- START Offsidebar button-->
        <li class="nav-item"><a href="<?php echo base_url(); ?>#" class="nav-link"></a></li>
        <li class="dropdown langs text-normal ng-scope" uib-dropdown="" is-open="status.isopenLang" data-ng-controller="LangCtrl" style=""> <a href="<?php echo base_url(); ?>javascript:;" class="dropdown-toggle  dropdown-toggleactive-flag" uib-dropdown-toggle="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <div class="flag flags-american" style=""></div>
          </a>
          <ul class="dropdown-menu with-arrow  pull-right list-langs dropdown-menu-scaleIn animated flipInX" role="menu">
            <li data-ng-show="lang !== 'English' " aria-hidden="true" class="ng-hide" style=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('English')">
              <div class="flag flags-american"></div>
              English</a></li>
            <li data-ng-show="lang !== 'Español' " aria-hidden="false" class="" style=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('Español')">
              <div class="flag flags-spain"></div>
              Español</a></li>
            <li data-ng-show="lang !== 'Portugal' " aria-hidden="false" class="" style=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('Portugal')">
              <div class="flag flags-portugal"></div>
              Portugal</a></li>
            <li data-ng-show="lang !== '中文' " aria-hidden="false" class=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('中文')">
              <div class="flag flags-china"></div>
              中文</a></li>
            <li data-ng-show="lang !== '日本語' " aria-hidden="false" class="" style=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('日本語')">
              <div class="flag flags-japan"></div>
              日本語</a></li>
            <li data-ng-show="lang !== 'Русский язык' " aria-hidden="false" class="" style=""> <a href="<?php echo base_url(); ?>javascript:;" data-ng-click="setLang('Русский язык')">
              <div class="flag flags-russia"></div>
              Русский язык</a></li>
          </ul>
        </li>
        
        <!-- END Offsidebar menu-->
      </ul>
      <!-- END Right Navbar--> 
     
    </nav>
    <!-- END Top Navbar--> 
  </header>
  <!-- sidebar-->
  <aside class="aside-container"> 
    <!-- START Sidebar (left)-->
    <div class="aside-inner">
      <nav class="sidebar" data-sidebar-anyclick-close=""> 
        <!-- START sidebar nav-->
        <ul class="sidebar-nav">
          <!-- START user info-->
          <li class="has-user-block">
            <div class="collapse" id="user-block">
              <div class="item user-block"> 
                <!-- User picture-->
                <div class="user-block-picture">
                  <div class="user-block-status"><img class="img-thumbnail rounded-circle" src="<?php echo base_url(); ?>img/user/02.jpg" alt="Avatar" width="60" height="60">
                    <div class="circle bg-success circle-lg"></div>
                  </div>
                </div>
                <!-- Name and Job-->
                <div class="user-block-info"><span class="user-block-name">Hello, <?php echo $this->session->userdata('empname') ?></span><span class="user-block-role"><?php if($this->session->userdata('empname')) { ?><a href="<?php echo base_url() ?>user/logout" >Logout </a><?php } ?></span></div>
              </div>
            </div>
          </li>
          <!-- END user info--> 
          <!-- Iterates over all sidebar items-->
          <li class="nav-heading text-bold"><span data-localize="sidebar.heading.HEADER">Main Navigation</span></li>
          <li><a href="<?php echo base_url(); ?>User/home" title="Plant master"><em class="fas fa-home"></em><span data-localize="sidebar.nav.Plant master">Home</span></a></li>
          <li class=" active"><a href="<?php echo base_url(); ?>Production/area" title="Plant master"><em class="fas fa-cogs"></em><span data-localize="sidebar.nav.Plant master">Production</span></a></li>
          <li ><a href="<?php echo base_url(); ?>Sanitization/area" title="Plant master"><em class="fas fa-broom"></em><span data-localize="sidebar.nav.Plant master">Sanitization</span></a></li>
          <li><a href="#submenu" title="Menu" data-toggle="collapse"><em class="fas fa-wrench"></em><span data-localize="sidebar.nav.menu.MENU">Preventive Maintenance</span></a>
          <li><a href="#" title="Menu" data-toggle="collapse"><em class="fas fa-file"></em><span data-localize="sidebar.nav.menu.MENU">Reports</span></a>
          </li>


          <!--<li class="nav-heading text-bold"><span data-localize="sidebar.heading.HEADER">Main Menu</span></li>
          <li><a href="<?php echo base_url(); ?>dashboard.html" title="Plant master"><em class="fas fa-tasks"></em><span data-localize="sidebar.nav.Plant master">Production</span></a></li>
          <li class=" active"><a href="<?php echo base_url(); ?>area.html" title="Plant master"><em class="fas fa-cogs"></em><span data-localize="sidebar.nav.Plant master">Execute SOP</span></a></li>
          <li><a href="#submenu" title="Menu" data-toggle="collapse"><em class="fas fa-chalkboard-teacher"></em><span data-localize="sidebar.nav.menu.MENU">Master Data</span></a>
            <ul class="sidebar-nav sidebar-subnav collapse" id="submenu">
              <li class="sidebar-subnav-header">Menu</li>
              <li><a href="<?php echo base_url(); ?>#" title="Plant master"><span data-localize="sidebar.nav.Plant master">Plant master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Block master"><span data-localize="sidebar.nav.Plant master">Block master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Area master"><span data-localize="sidebar.nav.Plant master">Area master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Rooms master"><span data-localize="sidebar.nav.Plant master">Rooms master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="SOP master"><span data-localize="sidebar.nav.Plant master">SOP master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Product master"><span data-localize="sidebar.nav.Plant master">product master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Critical Parts master"><span data-localize="sidebar.nav.Critical Parts master">Critical Parts master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Accessory Parts"><span data-localize="sidebar.nav.Accessory Parts">Accessory Parts</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Equipments master"><span data-localize="sidebar.nav.Equipments master">Equipments master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Employee master"><span data-localize="sidebar.nav.Employee master">Employee master</span></a></li>
              <li><a href="<?php echo base_url(); ?>#" title="Holiday master"><span data-localize="sidebar.nav.Holiday master">Holiday master</span></a></li>-->
            </ul>
          </li>
        </ul>
        <!-- END sidebar nav--> 
      </nav>
    </div>
    <!-- END Sidebar (left)--> 
  </aside>
  <!-- Main section-->
  <section class="section-container"> 
    <!-- Page content-->
    <div class="content-wrapper">
      <div class="content-heading executesop-heading">
        <div class="col-sm-8 pl-0" id="head">Add workflow</div>
      </div>
      <div class="card-default wfcard">
        <div class="card-body">
          <div class="col-sm-12">     
            <div><span class="ml18 float-left mt-3"><h4>Workflow not added</h4></span></div>
            <div class="float-right"><span class="ml18 float-left mt-3 mr15"><h4>Add workflow</h4></span><i data-toggle="modal" data-target="#myModalLarge" onclick="showact();" class="fa fa-plus default-icon-color default-icon-style float-right" aria-hidden="true"></i></div>
          </div>
        </div>
      </div>
      <div class="card-default wfcard" id="actdiv" style="display: none;">
        <div class="card-body">
          <div class="col-sm-12">     
            <div><span class="ml18 float-left mt-3"><h4>Select Activity</h4></span></div>
            <div class="form-group float-right mt-3"><select id="act" onchange="showaddstep();" class="form-control">
              <option>Select</option>
            <?php if($data['activities']=="")
                {

                }
                else
                {
                foreach($data['activities']->result() as $row) { ?> 
              <option value="<?php echo $row->activity_code ?>"><?php echo $row->activity_name ?></option><?php } } ?></select></div>
          </div>
        </div>
      </div>
      <div class="card-default wfcard" id="addstepdive" style="display: none;">
        <div class="card-body">
          <div class="col-sm-12">     
            <div><span class="ml18 float-left mt-3"><h4>Add step</h4></span></div>
            <div class="float-right"><span class="ml18 float-left mt-3 mr15"><h4>Add more</h4></span><i data-toggle="modal" data-target="#myModalLarge" onclick="showrole();" class="fa fa-plus default-icon-color default-icon-style float-right" aria-hidden="true"></i></div>
          </div>
        </div>
      </div>
      <div class="card-default wfcard" id="rolediv">

      </div>
      <div class="card-default wfcard">
        <div class="card-body">
          <div class="col-sm-12">     
            
      <div class="text-right" id="sbtn"><span id="demo3" class="pr-2 f16"></span><button id="sbbtn" class="btn btn-success btn-lg" type="button" onClick="addworkflow();">&nbsp;&nbsp;&nbsp;&nbsp;Submit&nbsp;&nbsp;&nbsp;&nbsp;</button></div>      
      </div>
          
        </div>
      </div>
      

      </div>
  </section>
  <!-- Page footer-->
  <footer class="footer-container text-center"><span> SMHS. &copy; 2019 - 2020. All Rights Reserved.</span></footer>
</div>


<!-- =============== VENDOR SCRIPTS ===============--> 
<!-- MODERNIZR--> 
<script src="<?php echo base_url(); ?>vendor/modernizr/modernizr.custom.js"></script><!-- STORAGE API--> 
<script src="<?php echo base_url(); ?>vendor/js-storage/js.storage.js"></script><!-- SCREENFULL--> 
<script src="<?php echo base_url(); ?>vendor/screenfull/dist/screenfull.js"></script><!-- i18next--> 
<script src="<?php echo base_url(); ?>vendor/i18next/i18next.js"></script> 
<script src="<?php echo base_url(); ?>vendor/flot/jquery.flot.time.js"></script> 
<script src="<?php echo base_url(); ?>vendor/i18next-xhr-backend/i18nextXHRBackend.js"></script><!-- =============== PAGE VENDOR SCRIPTS ===============--> 
<!-- =============== APP SCRIPTS ===============--> 
<script src="js/app.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
<script>
  var role = 0;
function showact()
{
  $("#actdiv").show(1000);
}
function showaddstep()
{
  $("#addstepdive").show(1000);
}
function showrole()
{
  //$("#rolediv").show(1000);
  role ++;
  var cols = "";
  cols += '<div class="card-body"><div class="col-sm-12"><div><span class="ml18 float-left mt-3"><h4>Step '+role+'</h4></span></div><div class="form-group float-right mt-3">Select Role<select name="role" class="form-control"><?php if($data['role']=="") { } else { foreach($data['role']->result() as $row) { ?> <option value="<?php echo $row->id ?>"><?php echo $row->role_description ?></option><?php } } ?></select></div></div></div>';
  $("#rolediv").append(cols);
}
</script>
<script>
function addworkflow()
{
  debugger;
  var obj=0;
  var act = $("#act").val();
  var myArray = [];
  $("#rolediv").find('select[name^="role"]').each(function () {
    obj++;
    var role = $(this).val();
    myArray.push( {
    role: role,
    });
  });
  console.log(myArray);
    $.ajax({
      url: "<?= base_url()?>User/makeworkflow",
      type: 'POST',
      data: {act:act,myArray:myArray},
      success: function(res) {
        if(res>0){
          alert("Workflow successfully added...");
        }
        else
        {
          alert("Something went wrong");
        }
                  location.reload(true);
      }
    }); 
}
</script>
</body>
</html>