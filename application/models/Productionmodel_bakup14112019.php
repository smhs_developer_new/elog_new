<?php

class Productionmodel extends CI_Model {
	
	public function get_production_areadtl()
	{
		$this->db->where('is_active',1);
		$q = $this->db->get('mst_area');
		return $q;
	}

	public function get_production_roomdtl()
	{
		$this->db->where('is_active',1);
		$q = $this->db->get('mst_room');
		return $q;	
	}

	public function get_production_batchdtl()
	{
		$this->db->where('is_active',1);
		$q = $this->db->get('trn_batch');
		return $q;	
	}

	public function get_Header4($colname,$colval,$colname2,$colval2,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where('is_active',1);
		$this->db->limit(1);
		return $this->db->get($tblname);		
	}

	public function get_Header12($colname,$colval,$colname2,$colval2,$colname3,$colval3,$colname4,$colval4,$colname5,$colval5,$colname6,$colval6,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where($colname3, $colval3);
		$this->db->where($colname4, $colval4);
		$this->db->where($colname5, $colval5);
		$this->db->where($colname6, $colval6);
		$this->db->where('is_active',1);
		$this->db->where('status','1');
		$this->db->where('next_step','2');
		$this->db->limit(1);
		$this->db->order_by("id","ASC");
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function get_HeaderID12($colname,$colval,$colname2,$colval2,$colname3,$colval3,$colname4,$colval4,$colname5,$colval5,$colname6,$colval6,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where($colname3, $colval3);
		$this->db->where($colname4, $colval4);
		$this->db->where($colname5, $colval5);
		$this->db->where($colname6, $colval6);
		$this->db->where('is_active',1);
		$this->db->where('status','1');
		$this->db->where('next_step','2');
		$this->db->limit(1);
		$this->db->order_by("id","ASC");
		$q = $this->db->get($tblname);
		if($q->num_rows() > 0)
		{
			$res = $q->row_array();
			$id = $res["id"];
			$docid = $res["document_no"];

			$response = array(
			'id' => $id,
			'docid' => $docid,
			);

			return $response;

		}		
	}

	public function getLastHeader_dtl3($colname,$colval,$tblname)
	{
		$this->db->where($colname, $colval);		
		$this->db->where('is_active',1);
		$this->db->limit(1);
		$this->db->order_by("id","DESC");
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function operationfinish($header_id,$headerlast_id,$endtime,$checked_by,$next_step){

	$arr1 = array("checked_by"=>$checked_by,"end_time"=>$endtime,"checked_by_time"=>$endtime,"modified_by"=>$checked_by,"modified_on"=>$endtime);		
    $this->db->where("id",$headerlast_id);  
    $this->db->update("trn_operationlogdetail",$arr1);
        
  	$arr2 = array("status"=>'2',"next_step"=>$next_step,"modified_by"=>$checked_by,"modified_on"=>$endtime);		
    $this->db->where("id",$header_id);  
    if ($this->db->update("trn_operationlogheader",$arr2)) {

        $this->db->where('header_id',$header_id);
		$res22 = $this->db->get('trn_operationlogdetail');
		$response22 = $res22->result_array();
		return $response22;
     } 
  	}

  	public function operationcheckby($header_id,$headerlast_id,$endtime,$checked_by){

	$arr1 = array("checked_by"=>$this->session->userdata('empname'),"end_time"=>$endtime,"checked_by_time"=>$endtime,"modified_by"=>$checked_by,"modified_on"=>$endtime);		
    $this->db->where("id",$headerlast_id);  
    $this->db->update("trn_operationlogdetail",$arr1);
        
    	$this->load->model('Logmaintainmodel');
		date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
		$now = date('Y-m-d H:i:s');
		$this->Logmaintainmodel->insert15para_with_tblnama('trn_operationlogdetail','header_id',$header_id,'start_time',$now,'end_time','','checked_by_time','','performed_by',$this->session->userdata('empname'),'checked_by','','created_by',$checked_by);
		$inserted_row_id = $this->db->insert_id();

		$this->db->where('header_id',$header_id);
		$res = $this->db->get('trn_operationlogdetail');
		$response = $res->result_array();
		return $response;
  	}

  	public function brkdownfinish($header_id,$equipment_code,$endtime,$checked_by,$memo_number,$remark,$equiptblid)
  	{
		$arr1 = array("checked_by"=>$this->session->userdata('empname'),"endtime"=>$endtime,"memo_number"=>$memo_number,"remark"=>$remark,"modified_by"=>$checked_by,"modified_on"=>$endtime);		
    	
    	$this->db->where("header_id",$header_id);
    	$this->db->where("equipment_code",$equipment_code);
    	$this->db->where("equipmentheader_table_id",$equiptblid);
    	$this->db->where("endtime",0);
    	if($this->db->update("trn_operationequipmentdetaillog",$arr1))
    	{

    		/*$this->db->Select("*,emp.emp_name as emp_name");
	    	$this->db->from("trn_operationequipmentdetaillog as opeqdtl");
			$this->db->join("mst_employee as emp","emp.emp_code=opeqdtl.performed_by","INNER");

			$this->db->where("equipment_code",$equipment_code);
    		$this->db->where("equipmentheader_table_id",$equiptblid);
			$this->db->where('header_id', $header_id);
			$response = $this->db->get();*/


			$this->db->where("equipment_code",$equipment_code);
    		$this->db->where("equipmentheader_table_id",$equiptblid);
			$this->db->where('header_id', $header_id);
			$response = $this->db->get('trn_operationequipmentdetaillog');			
			return $response->result_array();
			//return $response = array("status"=>1);
     	} 
     	else 
     	{
        	return $response = array("status"=>0);
  		}
  	}


	public function get_dtl4($colname,$colval,$colname2,$colval2,$tblname,$activityByRole)
	{
		$activityRoles = [];
		foreach($activityByRole as $row){
			$activityRoles[] = $row['activity_id'];
		}

		$this->db->where_in('activity_code', $activityRoles);
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		return $q;
	}

	// getting Activities
	public function getActivities()
	{
		$this->db->select("acttype.id as id,acttype.activitytype_name as activitytype_name,acttype.activitytype_icon as activitytype_icon,acttype.activityfunction_tocall as activityfunction_tocall,acttype.activity_blockcolor as activity_blockcolor");
		$this->db->from("mst_activitytype as acttype");
		$this->db->join("trn_activityrole as actrole","acttype.id=actrole.activity_typeid","INNER");
		$this->db->where("actrole.role_id",$this->session->userdata("roleid"));
		$this->db->where('actrole.is_active',1);
		$q= $this->db->get();
		return $q;		
	}

	public function getnext_step($status_id,$role_id,$activity_id)
	{		
		$this->db->where("status_id",$status_id);
		$this->db->where("role_id",$role_id);
		$this->db->where("activity_id",$activity_id);
		$this->db->where('is_active',1);		
		$q= $this->db->get('trn_workflowsteps');
		return $q;		
	}

	public function getnext_step2($role_id)
	{		
		$this->db->where("role_id",$role_id);
		$this->db->where('is_active',1);		
		$q= $this->db->get('trn_workflowsteps');
		return $q;		
	}

	public function get_dtl6($colname,$colval,$colname2,$colval2,$colname3,$colval3,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where($colname3, $colval3);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function get_dtl3($id,$colname,$colval,$tblname)
	{
		$this->db->where('id',$id);
		$this->db->where($colname, $colval);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		return $q;
	}

	public function get_dtl2($colname,$colval,$tblname,$groupby='Null')
	{
		if($groupby!='Null')
		{
         $this->db->group_by($groupby);
		}
		$this->db->where($colname, $colval);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function get_logdtl($colname,$colval,$tblname)
	{
		$this->db->Select("opdtl.start_time,opdtl.end_time,opdtl.performed_by,opdtl.checked_by,emp.emp_name as emp_name");
    	$this->db->from("trn_operationlogdetail as opdtl");
		$this->db->join("mst_employee as emp","emp.emp_code=opdtl.performed_by","INNER");
        $this->db->where($colname, $colval);
		$res = $this->db->get();
		return $q;	
	}

	public function getdtl2($colval,$tblname)
	{
		$this->db->where($colval);
		$this->db->where('is_active',1);
		$this->db->order_by("created_on","DESC");
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function get_dtl5($colname,$colval,$colname2,$colval2,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		return $q;
	}

	public function getlike_dtl2($colname,$colval,$tblname)
	{
		$this->db->like($colname, $colval);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function get2like_dtl($colname,$colname2,$colval,$tblname)
	{
		$where = "($colname LIKE '%$colval%' OR $colname2 LIKE '%$colval%')";
		//$this->db->like($colname, $colval);
		//$this->db->like($colname2, $colval);
		$this->db->where($where);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function getlike_dtl4($colname,$colval,$colname1,$colval1,$tblname)
	{
		$this->db->like($colname, $colval);
		$this->db->where($colname1, $colval1);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function get4like_dtl4($colname,$colname2,$colval,$colname1,$colval1,$tblname)
	{
		$where = "($colname LIKE '%$colval%' OR $colname2 LIKE '%$colval%')";
		//$this->db->like($colname, $colval);
		//$this->db->like($colname2, $colval);
		$this->db->where($where);
		$this->db->where($colname1, $colval1);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function getlike_areadtl2($colname,$colval,$tblname)
	{
		$this->db->like($colname, $colval);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function get_dtl1($tblname)
	{
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		return $q;
	}



	public function checkedBycount($tablename,$tablecolumn){
	$this->db->where("area_code",$this->session->userdata("area_code"));
	$this->db->where("room_code",$this->session->userdata("room_code"));
	$this->db->where($tablecolumn,null);
	$this->db->where("is_active",1);
	$query = $this->db->get($tablename);
	if($query->num_rows() > 0)
	{
		$count = $query->num_rows();
		return $count;
	}	
	else
	{
		return 0;
	}
	}

	public function getEquipmentsdtl(){
		$this->db->select("eq.id as id,eq.equipment_code as equipment_code,eq.equipment_name as equipment_name,eq.equipment_icon as equipment_icon,eq.equipment_type as equipment_type,sop.sop_code as sop_code,sop.sop_name as sop_name,sop.filepath as filepath");
		$this->db->from("mst_equipment as eq");
		$this->db->join("mst_sop as sop","sop.sop_code=eq.sop_code","INNER");
		$this->db->where("eq.room_code",$this->session->userdata("room_code"));
		$this->db->where('eq.is_active',1);
		$this->db->where('sop.is_active',1);
		$q= $this->db->get();
		if($q->num_rows() > 0)
		{
			return $q;
		}	

		/*$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$res[] = $r["equipment_name"]."-".$r["sop_name"];
		}
		return $res;*/
	}

	public function getpendingtaskEquipmentsdtl($header_id){
		$this->db->select("opeql.id as id,eq.equipment_code as equipment_code,eq.equipment_name as equipment_name,eq.equipment_icon as equipment_icon,eq.equipment_type as equipment_type,sop.sop_code as sop_code,sop.sop_name as sop_name,sop.filepath as filepath");
		$this->db->from("mst_equipment as eq");
		$this->db->join("trn_operationequipmentheaderlog as opeql","opeql.equipment_code=eq.equipment_code","INNER");
		$this->db->join("mst_sop as sop","sop.sop_code=opeql.sop_code","INNER");
		$this->db->where("opeql.header_id",$header_id);
		//$this->db->where("opeql.strattime",0);
		//$this->db->order_by("opeql.equipment_code","DESC");
		$q= $this->db->get();
		if($q->num_rows() > 0)
		{
			return $q;
		}	
	}

	public function getresultset_of_eachEuipment_of_Pendingtask($header_id,$equipment_code){
		$this->db->select("id,header_id,equipment_code,equipment_type,strattime,endtime,checked_by,performed_by");
		$this->db->where("header_id",$header_id);
		$this->db->where("equipment_code",$equipment_code);
		$this->db->order_by("strattime","DESC");
		$this->db->order_by("endtime","DESC");
		$q= $this->db->get("trn_operationequipmentdetaillog");
		if($q->num_rows() > 0)
		{
			return $q->result_array();
		}	
	}

	public function getProductname($str){
	    $this->db->group_start();
	    $this->db->like('product_name', $str);
	    $this->db->group_end();
	    $this->db->where("is_active",1);
	    $dev = $this->db->get("mst_product");
	    foreach ($dev->result_array() as $sn) {
	      $response[] = $sn["product_name"];
	    }
	    return $response;
	}

	public function getAccessory(){
		$this->db->select("eq.equipment_name as equipment_name,sop.sop_name as sop_name,eq.equipment_code as equipment_code");
		$this->db->from("mst_equipment as eq");
		$this->db->join("mst_sop as sop","sop.sop_code=eq.sop_code","INNER");
		$this->db->where("eq.room_code",$this->session->userdata("room_code"));
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$res[] = $r["equipment_code"]." | ".$r["equipment_name"]." | ".$r["sop_name"];
		}
		return $res;
	}

	public function insertAccessorycleaning($data){
		$this->db->where("activity_id",$this->session->userdata('activity_code'));
		$this->db->where("status_id","1");
		$query = $this->db->get("trn_workflowsteps");
		$res = $query->row_array();

	$aa="";
		foreach ($data["accessorylist"] as $k) {

			$aa=$aa."".$k."@";
		}
		$acc = rtrim($aa,"@");
		$arr = array("document_no"=>$data["doc_id"],"batch_no"=>$data["batchno"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"department_name"=>$data["departmentname"],"product_name"=>$data["productname"],"accessorylist"=>$acc,"remarks"=>$data["remarks"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$this->session->userdata('empcode'),"performed_by"=>$this->session->userdata('empcode'),"status"=>'1',"next_step"=>$res["next_step"]);

    if ($this->db->insert("trn_accessorycleaning",$arr)) {

    	$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("document_no",$data["doc_id"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_accessorycleaning");
		$response = $query->row_array();
        $this->db->where("id",25);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}

	public function accessoryCleaningtakeover($data){
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
	$arr1 = array("checked_by"=>$this->session->userdata('empcode'),"stop_time"=>$date,"is_active"=>0);		
    $this->db->where("id",$data["id"]);
    $this->db->update("trn_accessorycleaning",$arr1);
    /*$aa="";
		foreach ($data["accessorylist"] as $k) {

			$aa=$aa."".$k."@";
		}
		$acc = rtrim($aa,"@");
		*/
		
		$arr = array("document_no"=>$data["doc_id"],"batch_no"=>$data["batchno"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"department_name"=>$data["departmentname"],"product_name"=>$data["productname"],"accessorylist"=>$data["accessorylist"],"remarks"=>$data["remarks"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$this->session->userdata('empcode'),"performed_by"=>$this->session->userdata('empcode'),"status"=>$data["status"],"next_step"=>$data["next_step"]);

    if ($this->db->insert("trn_accessorycleaning",$arr)) {

    	$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("document_no",$data["doc_id"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_accessorycleaning");
		$response = $query->row_array();
        $this->db->where("id",27);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}

	public function accessoryCleaningfinish($data){
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
		$this->db->where("activity_id",$this->session->userdata('activity_code'));
		$this->db->where("status_id","2");
		$query = $this->db->get("trn_workflowsteps");
		$res = $query->row_array();

	$arr1 = array("role_id"=>$this->session->userdata('roleid'),"checked_by"=>$this->session->userdata('empcode'),"stop_time"=>$date,"status"=>'2',"next_step"=>$res["next_step"]);		
    $this->db->where("id",$data["id"]);
  
    if ($this->db->update("trn_accessorycleaning",$arr1)) {
        $this->db->where("id",26);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}

	public function getaccessoryCleaninglogs($did){
		$this->db->where("document_no",$did);
		$this->db->where("role_id",$this->session->userdata("roleid"));
		$query = $this->db->get("trn_accessorycleaning");
		$response = $query->result_array();
		return $response;
	}

	public function getaccessoryCleaningview(){
		$this->db->group_by('document_no');
		$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("checked_by",null);
		$this->db->where("role_id",$this->session->userdata("roleid"));
		$query = $this->db->get("trn_accessorycleaning");
		$response = $query->result_array();
		return $response;
	}

	public function getaccessoryCleaningviewnow($id){
		$this->db->where("id",$id);
		$this->db->where("role_id",$this->session->userdata("roleid"));
		$query = $this->db->get("trn_accessorycleaning");
		$response = $query->row_array();
		return $response;
	}

	public function getportableEquipmentcleaning(){
		$this->db->select("eq.equipment_name as equipment_name,eq.room_code as room_code,sop.sop_name as sop_name");
		$this->db->from("mst_equipment as eq");
		$this->db->join("mst_sop as sop","sop.sop_code=eq.sop_code","INNER");
		$this->db->where("eq.room_code",$this->session->userdata("room_code"));
		$this->db->where("eq.equipment_type","Portable");
		$res = $this->db->get();
		return $res->result_array();
	}

	public function portableEquipmentcleaningbatchno(){
		$this->db->select("batch_code");
		$query = $this->db->get("trn_batch");
		foreach ($query->result_array() as $res) {
			$response[]=$res["batch_code"];
		}
		return $response;
	}

	public function getbatchcodeDetails($data){
		$this->db->where("batch_code",$data["nbatch"]);
		$query = $this->db->get("trn_batch");
		return $query->row_array();
	}

	public function inprocesscleaningDetails($data){
		$this->db->where("activity_id",$this->session->userdata('activity_code'));
		$this->db->where("status_id","1");
		$query = $this->db->get("trn_workflowsteps");
		$res = $query->row_array();

		$arr = array("document_no"=>$data["doc_id"],"batch_no"=>$data["batchno"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"department_name"=>$data["departmentname"],"product_name"=>$data["productname"],"sop_no"=>$data["sopno"],"remarks"=>$data["remarks"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$this->session->userdata('empcode'),"created_by"=>$this->session->userdata('empcode'),"equipment_code"=>$data["equipmentcode"],"status"=>'1',"next_step"=>$res["next_step"]);

    if ($this->db->insert("trn_inprocesscontainercleaning",$arr)) {

    	$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("document_no",$data["doc_id"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_inprocesscontainercleaning");
		$response = $query->row_array();
		$this->db->where("id",13);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}

	public function getsopList($str){
		$this->db->group_start();
	    $this->db->like('sop_code', $str);
	    $this->db->group_end();
	    //$this->db->where("area_code",$this->session->userdata("area_code"));
	    $this->db->where("is_active",1);
	    $dev = $this->db->get("mst_sop");
	    foreach ($dev->result_array() as $sn) {
	      $response[] = $sn["sop_code"];
	    }
	    return $response;
	}


	public function getinprocessEquipment(){
		
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$dev = $this->db->get("mst_equipment");
	    foreach ($dev->result_array() as $sn) {
	      $response[] = $sn["equipment_code"];
	    }
	    return $response;
	}

	public function getinprocessContainercleaningviewnow($id){
		$this->db->where("id",$id);
		$this->db->where("role_id",$this->session->userdata("roleid"));
		$query = $this->db->get("trn_inprocesscontainercleaning");
		$response = $query->row_array();
		return $response;
	}

	public function getinprocessContainercleaninglogs($did){
		$this->db->where("document_no",$did);
		$this->db->where("role_id",$this->session->userdata("roleid"));
		$query = $this->db->get("trn_inprocesscontainercleaning");
		$response = $query->result_array();
		return $response;
	}

	public function inprocesscontainerCleaningfinish($data){
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
		$this->db->where("activity_id",$this->session->userdata('activity_code'));
		$this->db->where("status_id","2");
		$query = $this->db->get("trn_workflowsteps");
		$res = $query->row_array();


	$arr1 = array("role_id"=>$this->session->userdata('roleid'),"checked_by"=>$this->session->userdata('empcode'),"end_time"=>$date,"status"=>'2',"next_step"=>$res["next_step"]);		
    $this->db->where("id",$data["id"]);
  
    if ($this->db->update("trn_inprocesscontainercleaning",$arr1)) {
        $this->db->where("id",14);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}

	public function inprocesscontainerCleaningtakeover($data){
	date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
	$arr1 = array("checked_by"=>$this->session->userdata('empcode'),"end_time"=>$date,"is_active"=>0);		
    $this->db->where("id",$data["id"]);
    $this->db->update("trn_inprocesscontainercleaning",$arr1);


    $this->db->where("activity_id",$this->session->userdata('activity_code'));
		$this->db->where("status_id","1");
		$query = $this->db->get("trn_workflowsteps");
		$res = $query->row_array();

    $arr = array("document_no"=>$data["doc_id"],"batch_no"=>$data["batchno"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"department_name"=>$data["departmentname"],"product_name"=>$data["productname"],"sop_no"=>$data["sopno"],"remarks"=>$data["remarks"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$this->session->userdata('empcode'),"created_by"=>$this->session->userdata('empcode'),"equipment_code"=>$data["equipmentcode"],"status"=>$data["status"],"next_step"=>$data["next_step"]);

    if ($this->db->insert("trn_inprocesscontainercleaning",$arr)) {

    	$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("document_no",$data["doc_id"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_inprocesscontainercleaning");
		$response = $query->row_array();
		$this->db->where("id",15);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;	
	}

	public function getinprocessContainercleaningview(){
		$this->db->group_by('document_no');
		$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("checked_by",null);
		$this->db->where("role_id",$this->session->userdata("roleid"));
		$query = $this->db->get("trn_inprocesscontainercleaning");
		$response = $query->result_array();
		return $response;
	}

	public function portableEquipmentcleaningsubmit($data){ 

		$this->db->where("activity_id",$this->session->userdata('activity_code'));
		$this->db->where("status_id","1");
		$query = $this->db->get("trn_workflowsteps");
		$res = $query->row_array();

		$arr = array("document_no"=>$data["postlist"]["docid"],"department_name"=>$data["postlist"]["departmentname"],"batch_code"=>$data["postlist"]["bcode"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"product_code"=>$data["postlist"]["pcode"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$this->session->userdata('empcode'),"status"=>'1',"next_step"=>$res["next_step"]);

    if ($this->db->insert("trn_portabledetail",$arr)) {
    	$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("document_no",$data["postlist"]["docid"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_portabledetail");
		$response = $query->row_array();
    	
    	foreach ($data["myArray"] as $av) {

       $arrn = array("document_portable_id"=>$response["id"],"document_no"=>$data["postlist"]["docid"],"equipment_code"=>$av["eqcode"],"equipment_type"=>$av["eqtype"],"sop_code"=>$av["sopcode"],"changepart"=>$av["chgprt"]);
		$this->db->insert("trn_portableequipmentdetail",$arrn);
    	} 

    	$this->db->where("id",10);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}

		return $response;
	}

	public function getEqm($room_code){
		
		$this->db->from("mst_equipment as eq");
		$this->db->join("mst_sop as sop","sop.sop_code=eq.sop_code","INNER");
		$this->db->where("eq.room_code",$room_code);
		$this->db->where("eq.equipment_type","Portable");
		$res = $this->db->get();
		return $res->result_array();

	}

	public function get_area_name($area){
		$this->db->where("area_code",$area);
		$query = $this->db->get("mst_area");
		$response = $query->row_array();
		return $response["area_name"];
	}

	public function countPortable(){
		$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_portabledetail");
		return $query->num_rows();
	}

	public function getportableEquipmentcleaningview(){
		$this->db->group_by('document_no');
		$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("checked_by",null);
		$this->db->where("role_id",$this->session->userdata("roleid"));
		$query = $this->db->get("trn_portabledetail");
		return $query->result_array();
	}

	public function getportableEquipmentcleaningviewnow($id){
		$this->db->where("id",$id);
		$this->db->where("role_id",$this->session->userdata("roleid"));
		$query = $this->db->get("trn_portabledetail");
		$response1 = $query->row_array();

		$this->db->select("*,peq.id as ids");
		$this->db->from("trn_portableequipmentdetail as peq");
		$this->db->join("mst_equipment as eq","eq.equipment_code=peq.equipment_code","INNER");
		$this->db->join("mst_sop as sop","sop.sop_code=peq.sop_code","INNER");

		$this->db->where("peq.document_portable_id",$id);
		$query = $this->db->get();
		$response2 = $query->result();


		$response = array("response1"=>$response1,"response2"=>$response2); 
		return $response;
	}

	public function getportableEquipmentcleaninglogs($did){
		$this->db->where("document_no",$did);
		$this->db->where("role_id",$this->session->userdata("roleid"));
		$query = $this->db->get("trn_portabledetail");
		$response = $query->result_array();
		return $response;
	}

	public function getEdata($bcode){
		$this->db->where("batch_code",$bcode);
		$query = $this->db->get("trn_batch");
		$response = $query->row_array();
		return $response;
	}

	public function portableEquipmentcleaningfinish($data){
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
		$this->db->where("activity_id",$this->session->userdata('activity_code'));
		$this->db->where("status_id","2");
		$query = $this->db->get("trn_workflowsteps");
		$res = $query->row_array();


	$arr1 = array("role_id"=>$this->session->userdata('roleid'),"checked_by"=>$this->session->userdata('empcode'),"checked_on"=>$date,"status"=>'2',"next_step"=>$res["next_step"]);		
    $this->db->where("id",$data["id"]);
  
    if ($this->db->update("trn_portabledetail",$arr1)) {
    	$i=0;
    	foreach ($data["ids"] as $d) {
    		$nid = $data["ids"][$i];
    		$arrd = array("changepart"=>$data["changepart"][$i]);
    		$this->db->where("id",$nid);
    		$this->db->update("trn_portableequipmentdetail",$arrd);
    	$i++;	
    	}

        $this->db->where("id",11);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}


	public function portableEquipmentcleaningtakeover($data){
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
	$arr1 = array("checked_by"=>$this->session->userdata('empcode'),"checked_on"=>$date);		
    $this->db->where("id",$data["postlist"]["id"]);
    $this->db->update("trn_portabledetail",$arr1);

    $arr = array("document_no"=>$data["postlist"]["docid"],"department_name"=>$data["postlist"]["departmentname"],"batch_code"=>$data["postlist"]["bcode"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"product_code"=>$data["postlist"]["pcode"],"created_by"=>$this->session->userdata('empcode'),"status"=>$data["postlist"]["status"],"next_step"=>$data["postlist"]["next_step"],"role_id"=>$this->session->userdata('roleid'));


    if ($this->db->insert("trn_portabledetail",$arr)) {

    	$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("document_no",$data["postlist"]["docid"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_portabledetail");
		$response = $query->row_array();
    	
    	foreach ($data["myArray"] as $av) {

       $arrn = array("document_portable_id"=>$response["id"],"document_no"=>$data["postlist"]["docid"],"equipment_code"=>$av["eqcode"],"equipment_type"=>$av["eqtype"],"sop_code"=>$av["sopcode"],"changepart"=>$av["chgprt"]);
		$this->db->insert("trn_portableequipmentdetail",$arrn);
    	} 
        $this->db->where("id",12);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}

}