<?php

class Adminmodel extends CI_Model {

	public function insertActivityarea($data){
		$i=1;
		//$this->db->empty_table('mst_activity');

		//$rcount = count($data['roles']);
		/*$j=1;
		foreach ($data['activity'] as $act) {
			if(isset($data['area'.$j])){
				$actdata1 = explode("##",$act);
				$activity_code1 = $actdata1[0];
				$this->db->where('activity_code', $activity_code1);
				$this->db->where('area_code !=', "A-000");
   				$this->db->delete('mst_activity'); 
   			}
   			else
   			{

   			}
   			$j++;
		}*/	


		

		foreach ($data['activity'] as $a) {
		if(isset($data['area'.$i])){	
		$actdata = explode("##",$a);
		$activity_code = $actdata[0];
		$activity_name = $actdata[1];
		$activitytype_id = $actdata[2];
		$activity_type = $actdata[3];
		$activityfunc_tocall = $actdata[4];
		$activity_icon = $actdata[5];
		$activity_remark = $actdata[6];

		$mstarea = $data['area'.$i];
		$cby = $this->session->userdata('empcode');
		$error = 0;
				
				//Deleting previous Records
				$this->db->where('activity_code', $activity_code);
				$this->db->where('area_code !=', "A-000");
   				$this->db->delete('mst_activity'); 
   				//End Deleting

		foreach($data['area'.$i] as $area){
			$areadata = explode("##",$area);
			$areacode = $areadata[0];
			$areaname = $areadata[1];

			// Inserting New Records area vise
			$arr = array('activity_code'=>$activity_code,'activity_name'=>$activity_name,'activitytype_id'=>$activitytype_id,'activity_type'=>$activity_type,'activityfunc_tocall'=>$activityfunc_tocall,'activity_icon'=>$activity_icon,'activity_remark'=>$activity_remark,'area_code'=>$areacode,'created_by'=>$cby,'modified_by'=>$cby);

			if ($this->db->insert('mst_activity',$arr)) {
				$error = 1;				
			} 
			//End Inserting
		}
		}
		else
		{
			$actdata = explode("##",$a);
		$activity_code = $actdata[0];
		$activity_name = $actdata[1];
		$activitytype_id = $actdata[2];
		$activity_type = $actdata[3];
		$activityfunc_tocall = $actdata[4];
		$activity_icon = $actdata[5];
		$activity_remark = $actdata[6];
		$cby = $this->session->userdata('empcode');
				//Deleting previous Records
				$this->db->where('activity_code', $activity_code);
				$this->db->where('area_code !=', "A-000");
   				$this->db->delete('mst_activity'); 
   				//End Deleting
   				// Inserting New Records area vise
				$arr2 = array('activity_code'=>$activity_code,'activity_name'=>$activity_name,'activitytype_id'=>$activitytype_id,'activity_type'=>$activity_type,'activityfunc_tocall'=>$activityfunc_tocall,'activity_icon'=>$activity_icon,'activity_remark'=>$activity_remark,'area_code'=>" ",'created_by'=>$cby,'modified_by'=>$cby);

				if ($this->db->insert('mst_activity',$arr2)) {
				$error = 1;				
				} 
				//End Inserting
		}
		$i++;
		}
		

		if($error==1){
			$response = array("status"=>1,"msg"=>"Successfully Inserted","msgheader"=>"Successfully Inserted");
		}else{
			$response = array("status"=>0);
		}	
		
		return $response;
	}

	public function getActivities()
	{
		$where = '(activitytype_id=14 or activitytype_id = 17 or activitytype_id=18 or activitytype_id=20)';
      
		$this->db->group_by('activity_code');
		 $this->db->where($where);
		$q = $this->db->get('mst_activity');
		if($q->num_rows() > 0)
		{	
			return $q->result_array();
		}	
	}

	public function getAreaformap()
	{
		$this->db->where("area_code !=","A-000");
		$q = $this->db->get('mst_area');
		if($q->num_rows() > 0)
		{	
			return $q->result_array();
		}		
	}

	public function getArea()
	{
		$q = $this->db->get('mst_area');
		if($q->num_rows() > 0)
		{	
			return $q->result_array();
		}		
	}

	public function sA($actcode){
		$this->db->where("activity_code",$actcode);
		$dd2 = $this->db->get("mst_activity");
		$response = array("ndata"=>$dd2->result_array());
		return $response;
	}

	public function get_activityBytypeid($col1,$val1,$tblname)
	{
		$this->db->where($col1,$val1);
		$this->db->where('is_active',1);
		$this->db->order_by("activity_code","ASC");
		$q = $this->db->get($tblname);
		return $q;
	}

	/*Common Delete function*/
	public function deleterecords($col,$val,$deleteby,$deletetime,$tblname)
	{
		$arr2 = array("is_active"=>0,"modified_by"=>$deleteby,"modified_on"=>$deletetime);
    	$this->db->where($col,$val);
    	if ($this->db->update($tblname,$arr2)) 
    	{
        	$response = $this->db->affected_rows();
     	} 
    	else 
    	{
        	$response = $this->db->affected_rows();
  		}

    	return $response;

	}
	/* End Common Delete Function */
	//Function to insert records by Bhupendra
	public function insert19para_with_tblnama($tblname,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7,$col8,$val8,$col9,$val9)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7,$col8=>$val8,$col9=>$val9);
			$this->db->insert($tblname,$arr);
			$last_id = $this->db->insert_id();
		return $last_id;	
	}

	public function insert5para_with_tblnama($tblname,$col1,$val1,$col2,$val2)
	{
		$arr = array($col1=>$val1,$col2=>$val2);
			$this->db->insert($tblname,$arr);
			$last_id = $this->db->insert_id();
		return $last_id;	
	}

	public function insert7para_with_tblnama($tblname,$col1,$val1,$col2,$val2,$col3,$val3)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3);
			$this->db->insert($tblname,$arr);
			$last_id = $this->db->insert_id();
		return $last_id;	
	}

	public function insert9para_with_tblnama($tblname,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4);
			$this->db->insert($tblname,$arr);
			$last_id = $this->db->insert_id();
		return $last_id;	
	}

	public function insert11para_with_tblnama($tblname,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5);
			$this->db->insert($tblname,$arr);
			$last_id = $this->db->insert_id();
		return $last_id;	
	}

	public function insert13para_with_tblnama($tblname,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6);
			$this->db->insert($tblname,$arr);
			$last_id = $this->db->insert_id();
		return $last_id;	
	}

	public function insert15para_with_tblnama($tblname,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7);
			$this->db->insert($tblname,$arr);
			$last_id = $this->db->insert_id();
		return $last_id;	
	}

	public function insert21para_with_tblnama($tblname,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7,$col8,$val8,$col9,$val9,$col10,$val10)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7,$col8=>$val8,$col9=>$val9,$col10=>$val10);
			$this->db->insert($tblname,$arr);
			$last_id = $this->db->insert_id();
		return $last_id;	
	}

	public function insert23para_with_tblnama($tblname,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7,$col8,$val8,$col9,$val9,$col10,$val10,$col11,$val11)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7,$col8=>$val8,$col9=>$val9,$col10=>$val10,$col11=>$val11);
			$this->db->insert($tblname,$arr);
			$last_id = $this->db->insert_id();
		return $last_id;	
	}

	public function insert25para_with_tblnama($tblname,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7,$col8,$val8,$col9,$val9,$col10,$val10,$col11,$val11,$col12,$val12)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7,$col8=>$val8,$col9=>$val9,$col10=>$val10,$col11=>$val11,$col12=>$val12);
			$this->db->insert($tblname,$arr);
			$last_id = $this->db->insert_id();
		return $last_id;	
	}

	public function insert27para_with_tblnama($tblname,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7,$col8,$val8,$col9,$val9,$col10,$val10,$col11,$val11,$col12,$val12,$col13,$val13)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7,$col8=>$val8,$col9=>$val9,$col10=>$val10,$col11=>$val11,$col12=>$val12,$col13=>$val13);
			$this->db->insert($tblname,$arr);
			$last_id = $this->db->insert_id();
		return $last_id;	
	}

	public function insert29para_with_tblnama($tblname,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7,$col8,$val8,$col9,$val9,$col10,$val10,$col11,$val11,$col12,$val12,$col13,$val13,$col14,$val14)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7,$col8=>$val8,$col9=>$val9,$col10=>$val10,$col11=>$val11,$col12=>$val12,$col13=>$val13,$col14=>$val14);
			$this->db->insert($tblname,$arr);
			$last_id = $this->db->insert_id();
		return $last_id;	
	}
	//End function to insert records

	
	//Function to get records by Bhupendra
	public function get_dtl6($colname,$colval,$colname2,$colval2,$colname3,$colval3,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where($colname3, $colval3);
		$this->db->where('is_active',1);
		$this->db->order_by("created_on","DESC");
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function get_dtl3($id,$colname,$colval,$tblname)
	{
		$this->db->where('id',$id);
		$this->db->where($colname, $colval);
		$this->db->where('is_active',1);
		$this->db->order_by("created_on","DESC");
		$q = $this->db->get($tblname);
		return $q;
	}

	public function get_dtl2($colname,$colval,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where('is_active',1);
		$this->db->order_by("created_on","DESC");
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function getdtl2($colval,$tblname)
	{
		$this->db->where($colval);
		$this->db->where('is_active',1);
		$this->db->order_by("created_on","DESC");
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function getdeptbyblock($colval)
	{

		$this->db->select();
		$this->db->from("mst_department as dept");
		$this->db->join("mst_area as ar","dept.department_code=ar.department_code","INNER");
		$this->db->where('dept.block_code', $colval);
		$this->db->where('dept.is_active',1);
		$this->db->group_by('dept.department_code');
		$this->db->order_by("dept.created_on","DESC");
		$q = $this->db->get();
		return $q;	
	}

	public function getareabysubblock($val,$tblname)
	{
		$this->db->where($val);
		$this->db->where('is_active',1);
		$this->db->order_by("created_on","DESC");
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function get_dtl5($colname,$colval,$colname2,$colval2,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where('is_active',1);
		$this->db->order_by("created_on","DESC");
		$q = $this->db->get($tblname);
		return $q;
	}

	public function getlike_dtl2($colname,$colval,$tblname)
	{
		$this->db->like($colname, $colval);
		$this->db->where('is_active',1);
		$this->db->order_by("created_on","DESC");
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function get_dtl1($tblname)
	{
		$this->db->where('is_active',1);
		$this->db->order_by("created_on","DESC");
		$q = $this->db->get($tblname);
		return $q;
	}

	public function getblock($tblname)
	{
		$this->db->where('is_active',1);
		$this->db->where('block_code !=','Block-000');
		$this->db->order_by("created_on","DESC");
		$q = $this->db->get($tblname);
		return $q;
	}

	public function getRoleStatusMapping()
	{
		$this->db->select("rwm.id as id,st.status_name as status,r.role_description as role,rwm.created_by,rwm.created_on");
		$this->db->from("mst_status as st");
		$this->db->join("mst_roleid_workflow_mapping as rwm","st.id=rwm.status_id","INNER");
		$this->db->join("mst_role as r","r.id=rwm.role_id","INNER");
		$this->db->where('st.is_active',1);
		$this->db->where('rwm.is_active',1);
		$this->db->order_by("created_on","DESC");
		$q= $this->db->get();
		return $q;		
	}
	//end function to get records

	//function for update records by Bhupendra
	public function update33_15para_with_1where_and_tbl($tblname,$wherecol,$whereval,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7,$col8,$val8,$col9,$val9,$col10,$val10,$col11,$val11,$col12,$val12,$col13,$val13,$col14,$val14,$col15,$val15)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7,$col8=>$val8,$col9=>$val9,$col10=>$val10,$col11=>$val11,$col12=>$val12,$col13=>$val13,$col14=>$val14,$col15=>$val15);
		$this->db->where($wherecol,$whereval);
		$this->db->update($tblname,$arr);
		$AR = $this->db->affected_rows();
		return $AR;		
	}

	public function update31_14para_with_1where_and_tbl($tblname,$wherecol,$whereval,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7,$col8,$val8,$col9,$val9,$col10,$val10,$col11,$val11,$col12,$val12,$col13,$val13,$col14,$val14)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7,$col8=>$val8,$col9=>$val9,$col10=>$val10,$col11=>$val11,$col12=>$val12,$col13=>$val13,$col14=>$val14);
		$this->db->where($wherecol,$whereval);
		$this->db->update($tblname,$arr);
		$AR = $this->db->affected_rows();
		return $AR;		
	}

	public function update29_13para_with_1where_and_tbl($tblname,$wherecol,$whereval,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7,$col8,$val8,$col9,$val9,$col10,$val10,$col11,$val11,$col12,$val12,$col13,$val13)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7,$col8=>$val8,$col9=>$val9,$col10=>$val10,$col11=>$val11,$col12=>$val12,$col13=>$val13);
		$this->db->where($wherecol,$whereval);
		$this->db->update($tblname,$arr);
		$AR = $this->db->affected_rows();
		return $AR;		
	}

	public function update27_12para_with_1where_and_tbl($tblname,$wherecol,$whereval,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7,$col8,$val8,$col9,$val9,$col10,$val10,$col11,$val11,$col12,$val12)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7,$col8=>$val8,$col9=>$val9,$col10=>$val10,$col11=>$val11,$col12=>$val12);
		$this->db->where($wherecol,$whereval);
		$this->db->update($tblname,$arr);
		$AR = $this->db->affected_rows();
		return $AR;		
	}

	public function update25_11para_with_1where_and_tbl($tblname,$wherecol,$whereval,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7,$col8,$val8,$col9,$val9,$col10,$val10,$col11,$val11)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7,$col8=>$val8,$col9=>$val9,$col10=>$val10,$col11=>$val11);
		$this->db->where($wherecol,$whereval);
		$this->db->update($tblname,$arr);
		$AR = $this->db->affected_rows();
		return $AR;		
	}

	public function update23_10para_with_1where_and_tbl($tblname,$wherecol,$whereval,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7,$col8,$val8,$col9,$val9,$col10,$val10)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7,$col8=>$val8,$col9=>$val9,$col10=>$val10);
		$this->db->where($wherecol,$whereval);
		$this->db->update($tblname,$arr);
		$AR = $this->db->affected_rows();
		return $AR;		
	}

	public function update21_9para_with_1where_and_tbl($tblname,$wherecol,$whereval,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7,$col8,$val8,$col9,$val9)
	{
		$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7,$col8=>$val8,$col9=>$val9);
		$this->db->where($wherecol,$whereval);
		$this->db->update($tblname,$arr);
		$AR = $this->db->affected_rows();
		return $AR;		
	}

	public function update19_8para_with_1where_and_tbl($tblname,$wherecol,$whereval,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7,$col8,$val8)
	{
	$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7,$col8=>$val8);
		$this->db->where($wherecol,$whereval);
		$this->db->update($tblname,$arr);
		$AR = $this->db->affected_rows();
		return $AR;	
	}

	public function update17_7para_with_1where_and_tbl($tblname,$wherecol,$whereval,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6,$col7,$val7)
	{
	$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6,$col7=>$val7);
		$this->db->where($wherecol,$whereval);
		$this->db->update($tblname,$arr);
		$AR = $this->db->affected_rows();
		return $AR;	
	}

	public function update15_6para_with_1where_and_tbl($tblname,$wherecol,$whereval,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5,$col6,$val6)
	{
	$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5,$col6=>$val6);
		$this->db->where($wherecol,$whereval);
		$this->db->update($tblname,$arr);
		$AR = $this->db->affected_rows();
		return $AR;	
	}

	public function update13_5para_with_1where_and_tbl($tblname,$wherecol,$whereval,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4,$col5,$val5)
	{
	$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4,$col5=>$val5);
		$this->db->where($wherecol,$whereval);
		$this->db->update($tblname,$arr);
		$AR = $this->db->affected_rows();
		return $AR;	
	}

	public function update11_4para_with_1where_and_tbl($tblname,$wherecol,$whereval,$col1,$val1,$col2,$val2,$col3,$val3,$col4,$val4)
	{
	$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3,$col4=>$val4);
		$this->db->where($wherecol,$whereval);
		$this->db->update($tblname,$arr);
		$AR = $this->db->affected_rows();
		return $AR;	
	}

	public function update9_3para_with_1where_and_tbl($tblname,$wherecol,$whereval,$col1,$val1,$col2,$val2,$col3,$val3)
	{
	$arr = array($col1=>$val1,$col2=>$val2,$col3=>$val3);
		$this->db->where($wherecol,$whereval);
		$this->db->update($tblname,$arr);
		$AR = $this->db->affected_rows();
		return $AR;	
	}

	
	//End function for update records
public function checkPlantcode($data){
$this->db->where("plant_code",$data["pcode"]);
$query = $this->db->get("mst_plant");
if($query->num_rows()==0){
$response = array("status"=>1);
}
else{
$response = array("status"=>0);
}
return $response;
}

public function mstPlantview(){
$this->db->where("is_active",1);
$this->db->order_by("id","DESC");
$query = $this->db->get("mst_plant");
$response = $query->result_array();
return $response;
}

public function mstPlantviewnow($id){
$this->db->where("id",$id);	
$query = $this->db->get("mst_plant");
$response = $query->row_array();
return $response;	
}

public function mstPlantUpdate($data){
date_default_timezone_set('Asia/Kolkata');	
$date = date("Y-m-d H:i:s");
$array = array("plant_code"=>$data["pcode"],"plant_name"=>$data["pname"],"plant_address"=>$data["paddress"],"plant_remarks"=>$data["premarks"],"plant_contact"=>$data["pcontact"],"number_of_blocks"=>$data["pblock"],"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
$this->db->where("id",$data["plantid"]);
if($this->db->update("mst_plant",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}


public function deletePlant($data){
date_default_timezone_set('Asia/Kolkata');	
$date = date("Y-m-d H:i:s");
$array = array("is_active"=>0,"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
$this->db->where("id",$data["plantid"]);
if($this->db->update("mst_plant",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;	
}


public function mstPlantSubmit($data){
$array = array("plant_code"=>$data["pcode"],"plant_name"=>$data["pname"],"plant_address"=>$data["paddress"],"plant_remarks"=>$data["premarks"],"plant_contact"=>$data["pcontact"],"number_of_blocks"=>$data["pblock"],"created_by"=>$this->session->userdata('empcode'));

if($this->db->insert("mst_plant",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}


public function checkBlockcode($data){
$this->db->where("block_code",$data["bcode"]);
$query = $this->db->get("mst_block");
if($query->num_rows()==0){
$response = array("status"=>1);
}
else{
$response = array("status"=>0);
}
return $response;
}

public function mstBlockview(){
$this->db->where("is_active",1);
$this->db->order_by("id","DESC");
$query = $this->db->get("mst_block");
$response = $query->result_array();
return $response;
}

public function mstBlockSubmit($data){
$array = array("block_code"=>$data["bcode"],"block_name"=>$data["bname"],"block_contact"=>$data["bcontact"],"number_of_areas"=>$data["barea"],"block_remarks"=>$data["bremarks"],"plant_code"=>$data["pcode"],"created_by"=>$this->session->userdata('empcode'));

if($this->db->insert("mst_block",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

public function mstBlockviewnow($id){
$this->db->where("id",$id);	
$query = $this->db->get("mst_block");
$response = $query->row_array();
return $response;
}

public function mstBlockUpdate($data){
date_default_timezone_set('Asia/Kolkata');	
$date = date("Y-m-d H:i:s");
$array = array("block_code"=>$data["bcode"],"block_name"=>$data["bname"],"block_contact"=>$data["bcontact"],"number_of_areas"=>$data["barea"],"block_remarks"=>$data["bremarks"],"plant_code"=>$data["pcode"],"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
$this->db->where("id",$data["blockid"]);
if($this->db->update("mst_block",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

public function deleteBlock($data){
date_default_timezone_set('Asia/Kolkata');	
$date = date("Y-m-d H:i:s");
$array = array("is_active"=>0,"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
$this->db->where("id",$data["blockid"]);
if($this->db->update("mst_block",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;	
}

public function checkAreacode($data){
$this->db->where("area_code",$data["acode"]);
$query = $this->db->get("mst_area");
if($query->num_rows()==0){
$response = array("status"=>1);
}
else{
$response = array("status"=>0);
}
return $response;
}

public function mstAreaSubmit($data){
$response = array("status"=>0);
$subblockcode = explode("##",$data["bcode"]);
$subblock = $subblockcode[0];
$block = $subblockcode[1];
$array = array("area_code"=>$data["acode"],"area_name"=>$data["aname"],"area_contact"=>$data["acontact"],"area_remarks"=>$data["aremarks"],"area_sop_no"=>$data["asop"],"number_of_rooms"=>$data["aroom"],"block_code"=>$block,"department_code"=>$subblock,"created_by"=>$this->session->userdata('empcode'));

if($this->db->insert("mst_area",$array)){
	
/* checking for Sub Block where Area is Null */
    $this->db->where("is_active",1);
	$this->db->where("area_code",NULL);
	$this->db->where("department_code",$subblock);
	$query = $this->db->get("mst_department");
	
	if($query->num_rows()>0){
    $array = array("area_code"=>$data["acode"]);
	$this->db->where("department_code",$subblock);
	if($this->db->update("mst_department",$array)){
		$response = array("status"=>1);
	}
	}
	else
	{
		$this->db->where("is_active",1);
	    $this->db->where("department_code",$subblock);
	    $query = $this->db->get("mst_department");
		$result = $query->row_array();
		$array = array("department_code"=>$result["department_code"],"department_name"=>$result["department_name"],"department_contact"=>$result["department_contact"],"department_remark"=>$result["department_remark"],"area_code"=>$data["acode"],"block_code"=>$result["block_code"],"created_by"=>$this->session->userdata('empcode'));
		if($this->db->insert("mst_department",$array)){
			$response = array("status"=>1);
		}


	}

	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

public function mstAreaview(){
$this->db->where("is_active",1);
$this->db->order_by("id","DESC");
$query = $this->db->get("mst_area");
$response = $query->result_array();
return $response;
}

public function mstAreaviewnow($id){
$this->db->where("id",$id);	
$query = $this->db->get("mst_area");
$response = $query->row_array();
return $response;
}

public function mstAreaupdate($data){
date_default_timezone_set('Asia/Kolkata');	
$date = date("Y-m-d H:i:s");
$response = array("status"=>0);
$subblockcode = explode("##",$data["bcode"]);
$subblock = $subblockcode[0];
$block = $subblockcode[1];
$array = array("area_code"=>$data["acode"],"area_name"=>$data["aname"],"area_contact"=>$data["acontact"],"area_remarks"=>$data["aremarks"],"area_sop_no"=>$data["asop"],"number_of_rooms"=>$data["aroom"],"block_code"=>$block,"department_code"=>$subblock,"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
$this->db->where("id",$data["areaid"]);
if($this->db->update("mst_area",$array)){

	$this->db->where("department_code",$subblock);
	$this->db->where("area_code",$data["acode"]);
	$this->db->where("is_active",1);
	$query = $this->db->get("mst_department");
	if($query->num_rows()>0){
    $response = array("status"=>1);
	}
	else
	{
		/* checking for Sub Block where Area is Null */
    $this->db->where("is_active",1);
	$this->db->where("area_code",NULL);
	$this->db->where("department_code",$subblock);
	$query = $this->db->get("mst_department");
	
	if($query->num_rows()>0){
    $array = array("area_code"=>$data["acode"]);
	$this->db->where("department_code",$subblock);
	
	if($this->db->update("mst_department",$array)){
		$response = array("status"=>1);
	}
	}
	else
		{

		
		//$this->db->where("department_code",$subblock);
	    $this->db->where("area_code",$data["acode"]);
		$this->db->where("is_active",1);
		$array = array("is_active"=>0);
	    $query = $this->db->update("mst_department",$array);
		
		$this->db->where("is_active",1);
	    $this->db->where("department_code",$subblock);
	    $query = $this->db->get("mst_department");
		$result = $query->row_array();
		$array=array("department_code"=>$result["department_code"],"department_name"=>$result["department_name"],"department_contact"=>$result["department_contact"],"department_remark"=>$result["department_remark"],"area_code"=>$data["acode"],"block_code"=>$result["block_code"],"created_by"=>$this->session->userdata('empcode'));
		if($this->db->insert("mst_department",$array)){
			$response = array("status"=>1);
		}

		//}

	}


	
	/*$response = array("status"=>1);
	$array = array("area_code"=>$data["acode"]);
	$this->db->where("department_code",$subblock);
	$this->db->update("mst_department",$array);*/
	
	}}
	else{
	$response = array("status"=>0);	
	}

	return $response;
}

public function deleteArea($data){
date_default_timezone_set('Asia/Kolkata');	
$date = date("Y-m-d H:i:s");
$array = array("is_active"=>0,"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
$this->db->where("id",$data["areaid"]);
if($this->db->update("mst_area",$array)){
	$response = array("status"=>1);	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;	
}

public function checkRoomcode($data){
$this->db->where("room_code",$data["rcode"]);
$query = $this->db->get("mst_room");
if($query->num_rows()==0){
$response = array("status"=>1);
}
else{
$response = array("status"=>0);
}
return $response;
}

public function mstRoomSubmit($data){
$array = array("room_code"=>$data["rcode"],"room_name"=>$data["rname"],"room_remarks"=>$data["rremarks"],"area_code"=>$data["acode"],"created_by"=>$this->session->userdata('empcode'));

if($this->db->insert("mst_room",$array)){

	if(isset($data['drainpoint']))
	{
	$this->db->where_in("drainpoint_code",$data['drainpoint']);
	$array = array("room_code"=>$data["rcode"]);
	if($this->db->update("mst_drainpoint",$array)){
		$response = array("status"=>1);
	}
	else{
	  $response = array("status"=>0);
	}
	}
	else{
	  $response = array("status"=>1);
	}

	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;  
}

public function mstRoomview(){
$this->db->where("is_active",1);
$this->db->order_by("id","DESC");
$query = $this->db->get("mst_room");
$response = $query->result_array();
return $response;
}

public function mstRoomviewnow($id){
$this->db->where("id",$id);	
$query = $this->db->get("mst_room");
$response = $query->row_array();
return $response;
}

public function mstRoomupdate($data){
date_default_timezone_set('Asia/Kolkata');	
$date = date("Y-m-d H:i:s");
$array = array("room_code"=>$data["rcode"],"room_name"=>$data["rname"],"room_remarks"=>$data["rremarks"],"area_code"=>$data["acode"],"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
$this->db->where("id",$data["roomid"]);
if($this->db->update("mst_room",$array)){
	
	$this->db->where("room_code",$data["rcode"]);
	$array = array("room_code"=>'');
	$this->db->update("mst_drainpoint",$array);
	
	if(isset($data['drainpoint']))
	{
	$this->db->where_in("drainpoint_code",$data['drainpoint']);
	$array = array("room_code"=>$data["rcode"]);
	if($this->db->update("mst_drainpoint",$array)){
	$response = array("status"=>1);
	}
	else{
	  $response = array("status"=>0);
	}
	}
	else{
	  $response = array("status"=>1);
	}	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

public function deleteRoom($data){
date_default_timezone_set('Asia/Kolkata');	
$date = date("Y-m-d H:i:s");
$array = array("is_active"=>0,"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
$this->db->where("id",$data["roomid"]);
if($this->db->update("mst_room",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;	
}

public function checkDocumentcode($data){
$this->db->where("doc_id",$data["dcode"]);
$query = $this->db->get("mst_document");
if($query->num_rows()==0){
$response = array("status"=>1);
}
else{
$response = array("status"=>0);
}
return $response;
}

public function mstDocumentSubmit($data){
$array = array("doc_id"=>$data["dcode"],"remarks"=>$data["dremarks"],"created_by"=>$this->session->userdata('empcode'));

if($this->db->insert("mst_document",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

public function mstDocumentview(){
$this->db->where("is_active",1);
$this->db->order_by("id","DESC");
$query = $this->db->get("mst_document");
$response = $query->result_array();
return $response;
}


public function mstDocumentviewnow($id){
$this->db->where("id",$id);	
$query = $this->db->get("mst_document");
$response = $query->row_array();
return $response;
}

public function mstDocumentupdate($data){
date_default_timezone_set('Asia/Kolkata');	
$date = date("Y-m-d H:i:s");
$array = array("remarks"=>$data["dremarks"],"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
$this->db->where("id",$data["docid"]);
if($this->db->update("mst_document",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}


public function deleteDocument($data){
date_default_timezone_set('Asia/Kolkata');	
$date = date("Y-m-d H:i:s");
$array = array("is_active"=>0,"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
$this->db->where("id",$data["docid"]);
if($this->db->update("mst_document",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;	
}


public function checkDraincode($data){
$this->db->where("drainpoint_code",$data["dpcode"]);
$query = $this->db->get("mst_drainpoint");
if($query->num_rows()==0){
$response = array("status"=>1);
}
else{
$response = array("status"=>0);
}
return $response;
}


public function mstDrainpointSubmit($data){
$array = array("drainpoint_code"=>$data["dpcode"],"drainpoint_name"=>$data["dpname"],"drainpoint_remark"=>$data["dpremarks"],"solution_code"=>$data["scode"],"created_by"=>$this->session->userdata('empcode'));

if($this->db->insert("mst_drainpoint",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

public function mstDrainpointview(){
$this->db->where("is_active",1);
$this->db->order_by("id","DESC");
$query = $this->db->get("mst_drainpoint");
$response = $query->result_array();
return $response;
}

public function mstDrainpointviewnow($id){
$this->db->where("id",$id);	
$query = $this->db->get("mst_drainpoint");
$response = $query->row_array();
return $response;
}

public function mstDrainpointupdate($data){
date_default_timezone_set('Asia/Kolkata');	
$date = date("Y-m-d H:i:s");
$array = array("drainpoint_name"=>$data["dpname"],"drainpoint_remark"=>$data["dpremarks"],"room_code"=>$data["rcode"],"solution_code"=>$data["scode"],"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
$this->db->where("id",$data["dpid"]);
if($this->db->update("mst_drainpoint",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}




public function deleteDrainpoint($data){
date_default_timezone_set('Asia/Kolkata');	
$date = date("Y-m-d H:i:s");
$array = array("is_active"=>0,"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
$this->db->where("id",$data["drainid"]);
if($this->db->update("mst_drainpoint",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;	
}


public function checkDepartmentcode($data){
$this->db->where("department_code",$data["dcode"]);
$query = $this->db->get("mst_department");
if($query->num_rows()==0){
$response = array("status"=>1);
}
else{
$response = array("status"=>0);
}
return $response;
}

public function checkAreasbyblock($data){
$this->db->where("block_code",$data["bcode"]);
$query = $this->db->get("mst_area");
if($query->num_rows()>0){
$response = array("status"=>1,"data"=>$query->result_array());
}
else{
$response = array("status"=>0);
}
return $response;
}


public function mstDepartmentSubmit($data){
$array = array("department_code"=>$data["dcode"],"department_name"=>$data["dname"],"department_contact"=>$data["dcontact"],"department_remark"=>$data["dremarks"],"block_code"=>$data["bcode"],"created_by"=>$this->session->userdata('empcode'));

if($this->db->insert("mst_department",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}


public function mstDepartmentview(){
$this->db->where("is_active",1);
$this->db->order_by("id","DESC");
$query = $this->db->get("mst_department");
$response = $query->result_array();
return $response;
}

public function mstDepartmentviewnow($id){
$this->db->where("id",$id);	
$query = $this->db->get("mst_department");
$response = $query->row_array();
return $response;
}

public function mstGetareanow($departmentdata){
$this->db->where("is_active",1);
$this->db->where("block_code",$departmentdata["block_code"]);
$this->db->order_by("id","DESC");
$response = $this->db->get("mst_area");
return $response;	
}

public function mstDepartmentupdate($data){
date_default_timezone_set('Asia/Kolkata');	
$date = date("Y-m-d H:i:s");
$array = array("department_code"=>$data["dcode"],"department_name"=>$data["dname"],"department_contact"=>$data["dcontact"],"department_remark"=>$data["dremarks"],"block_code"=>$data["bcode"],"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
$this->db->where("department_code",$data["dcode"]);
$this->db->where("is_active",1);
if($this->db->update("mst_department",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;	
}

public function deleteDepartment($data){
date_default_timezone_set('Asia/Kolkata');	
$date = date("Y-m-d H:i:s");
$array = array("is_active"=>0,"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
$this->db->where("id",$data["departmentid"]);
if($this->db->update("mst_department",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;	
}

public function checkPlantlimit($data){
$this->db->where("is_active",1);
$this->db->where("plant_code",$data["pcode"]);
$query = $this->db->get("mst_plant");
$res1 = $query->row_array();
$maxcount = $res1["number_of_blocks"];

$this->db->where("is_active",1);
$this->db->where("plant_code",$data["pcode"]);
$query2 = $this->db->get("mst_block");
$actcount = $query2->num_rows();

if($actcount==$maxcount){
$response = array("status"=>0);
}
else{
$response = array("status"=>1);
}

return $response;
}


public function checkBlocklimit($data){
$this->db->where("is_active",1);
$this->db->where("block_code",$data["bcode"]);
$query = $this->db->get("mst_block");
$res1 = $query->row_array();
$maxcount = $res1["number_of_areas"];

$this->db->where("is_active",1);
$this->db->where("block_code",$data["bcode"]);
$query2 = $this->db->get("mst_department");
$actcount = $query2->num_rows();

if($actcount==$maxcount){
$response = array("status"=>0);
}
else{
$response = array("status"=>1);
}

return $response;
}	


public function checkArealimit($data){
$this->db->where("is_active",1);
$this->db->where("area_code",$data["acode"]);
$query = $this->db->get("mst_area");
$res1 = $query->row_array();
$maxcount = $res1["number_of_rooms"];

$this->db->where("is_active",1);
$this->db->where("area_code",$data["acode"]);
$query2 = $this->db->get("mst_room");
$actcount = $query2->num_rows();

if($actcount==$maxcount){
$response = array("status"=>0);
}
else{
$response = array("status"=>1);
}

return $response;
}


public function checkEquipmentcode($data){
$this->db->where("equipment_code",$data["ecode"]);
$query = $this->db->get("mst_equipment");
if($query->num_rows()==0){
$response = array("status"=>1);
}
else{
$response = array("status"=>0);
}
return $response;
}

public function getDrainpointlist($keyword){
    $this->db->group_start();
    $this->db->like('drainpoint_code', $keyword);
    $this->db->group_end();
    $this->db->where("is_active",1);
    //$this->db->limit(5);
    $this->db->group_by('drainpoint_code');
    $dev = $this->db->get("mst_drainpoint");
    foreach ($dev->result_array() as $sn) {
      $response[] = $sn["drainpoint_code"];
    }
    return $response;
}	

public function getSoplist($keyword){
    $this->db->group_start();
    $this->db->like('sop_code', $keyword);
    $this->db->group_end();
    $this->db->where("is_active",1);
    //$this->db->limit(5);
    $this->db->group_by('sop_code');
    $dev = $this->db->get("mst_sop");
    foreach ($dev->result_array() as $sn) {
      $response[] = $sn["sop_code"];
    }
    return $response;
}

public function mstEquipmentview(){
$this->db->where("is_active",1);
$this->db->order_by("id","DESC");
$query = $this->db->get("mst_equipment");
$response = $query->result_array();
return $response;
}

public function mstEquipmentviewnow($id){
$this->db->where("id",$id);	
$query = $this->db->get("mst_equipment");
$response = $query->row_array();
return $response;
}

public function deleteEquipment($data){
date_default_timezone_set('Asia/Kolkata');	
$date = date("Y-m-d H:i:s");
$array = array("is_active"=>0,"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
$this->db->where("id",$data["equipmentid"]);
if($this->db->update("mst_equipment",$array)){
	$response = array("status"=>1);
	
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;	
}


public function checkcode($tblname,$col,$val){
$this->db->where($col,$val);
$this->db->where("is_active",1);
$query = $this->db->get($tblname);
if($query->num_rows()>0){
$response = array("status"=>1);
}
else{
$response = array("status"=>0);
}
return $response;
}


public function importDataExcel($data,$tablename) {
        $res = $this->db->insert_batch($tablename,$data);
        if($res){
            return TRUE;
        }else{
            return FALSE;
        }
    }






}