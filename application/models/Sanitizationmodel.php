<?php

class Sanitizationmodel extends CI_Model {

	public function get_dtl1($tblname)
	{
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		if($q->num_rows() > 0)
		{
			return $q;
		}	
		else
		{
			return false;
		}	
	}

	public function get_dtl2($colname,$colval,$tblname,$groupby='Null')
	{
		if($groupby!='Null')
		{
         $this->db->group_by($groupby);
		}
		$this->db->where($colname, $colval);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		return $q;	
	}

	public function get_dtl4($colname,$colval,$colname2,$colval2,$tblname,$activityByRole)
	{
		$activityRoles = [];
		foreach($activityByRole as $row){
			$activityRoles[] = $row['activity_id'];
		}

		$this->db->where_in('activity_code', $activityRoles);
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		if($q->num_rows() > 0)
		{
			return $q;
		}	
		else
		{
			return false;
		}	
	}

	public function checkedBycount($tablename,$tablecolumn){
	$this->db->where("area_code",$this->session->userdata("area_code"));
	$this->db->where("room_code",$this->session->userdata("room_code"));
	$this->db->where($tablecolumn,null);
	$this->db->where("is_active",1);
	$query = $this->db->get($tablename);
	if($query->num_rows() > 0)
	{
		$count = $query->num_rows();
		return $count;
	}	
	else
	{
		return 0;
	}
	}

	public function solutionNames($keyword){
    $this->db->group_start();
    $this->db->like('solution_name', $keyword);
    $this->db->or_like('Short_Name', $keyword);
    $this->db->group_end();
    $this->db->where("Block_Code",$this->session->userdata('empblock_code'));
    $this->db->where("is_active",1);
    //$this->db->limit(5);
    $dev = $this->db->get("mst_solution");
    foreach ($dev->result_array() as $sn) {
      $response[] = $sn["solution_name"];
    }
    return $response;
	}

	public function solnamegetval($keyword){
	$this->db->where('solution_name', $keyword);
    $this->db->where("Block_Code",$this->session->userdata('empblock_code'));
    $this->db->where("is_active",1);
    $dev = $this->db->get("mst_solution");
    $response = $dev->row_array();
	return $response;
	}

	public function solUsed($keyword){
	$response = array();		
	//$this->db->group_start();
    $this->db->like('solutionname', $keyword);
    //$this->db->group_end();
    $this->db->where("expiry_date >",date("Y-m-d H:i:s"));
    $this->db->where("is_active",1);
    $this->db->limit(5);
    $dev = $this->db->get("trn_solutionstock");
    foreach ($dev->result_array() as $sn) {
      $response[] = $sn["document_no"]."|".$sn["solutionname"];
    }
    return $response;
	}

	public function solUseddata($docid){
		$this->db->where("document_no",$docid);
		$this->db->where("is_active",1);
		$dev = $this->db->get("trn_solutionstock");
		$response = $dev->row_array();
		return $response;
	}

	public function getnext_step2($role_id,$aid)
	{		
		$this->db->where("role_id",$role_id);
		$this->db->where("activity_id",$aid);
		$this->db->where('is_active',1);		
		$q= $this->db->get('trn_workflowsteps');
		if($q->num_rows() > 0)
		{	
			return $q;
		}		
	}

	public function getdocsoldetails($rid,$tname){
		$this->db->where("id",$rid);
		$this->db->where('is_active',1);		
		$q= $this->db->get($tname);
		if($q->num_rows() > 0)
		{	
			$res = $q->row_array();
			return $res;
		}
	}



	public function submitSolutionPreparation($data){
		date_default_timezone_set('Asia/Kolkata');
		$this->db->where("activity_id",$this->session->userdata('activity_code'));
		$this->db->where("status_id","2");
		$query = $this->db->get("trn_workflowsteps");
		$res = $query->row_array();
		$datetime = date("Y-m-d H:i:s", strtotime('+'.$data["validupto"].' hours'));
		$arr = array("doc_id"=>$data["doc_id"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"solutionname"=>$data["solutionname"],"batch_no"=>$data["batchno"],"qcar_no"=>$data["qcarno"],"department_name"=>$data["departmentname"],"standard_solution_qty"=>$data["stdsolqty"],"purified_water"=>$data["strpurewater"],"makeup_volume"=>$data["markupvol"],"actual_solution_qty"=>$data["actsolqty"]." ".$data["actsoluom"],"actual_purified_water"=>$data["actpurewater"]." ".$data["actpureuom"],"solution_valid_upto"=>$data["validupto"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$this->session->userdata('empcode'),"created_name"=>$this->session->userdata('empname'),"status"=>'1',"next_step"=>$res["next_step"],"required_quantity"=>$data["requiredquantity"],"expiry_datatime"=>$datetime);

    if ($this->db->insert("trn_solutiondetails",$arr)) {
        $arr_stock = array("document_no"=>$data["doc_id"],"solutionname"=>$data["solutionname"],"available_qty"=>$data["requiredquantity"],"expiry_date"=>$datetime);
	}
	if ($this->db->insert("trn_solutionstock",$arr_stock)) {
        $this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("doc_id",$data["doc_id"]);
		$this->db->where("solution_destroy_checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_solutiondetails");
		$response = $query->row_array();
        
		$room_array= array("in_use"=>'1',"inuse_activity"=>"Solution Preparation In Progress");
		$this->db->where("room_code", $this->session->userdata("room_code"));
		$this->db->update("mst_room", $room_array);

        
		$this->db->where("id",19);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}
  	return $response;
	}

	public function getdrainPoints(){
		$res = array();
		$res1 = array();
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("is_active",1);
		$query = $this->db->get("mst_drainpoint");
		foreach ($query->result_array() as $dp) {
			$res[] = $dp["drainpoint_code"];
		}
		return $res;
	}

	public function errMsg($header){
		$this->db->where("msg-header",$header);
		$this->db->where("is_active",1);
		$query = $this->db->get("mst_messages");
		$res = $query->row_array();
		return $res;
	}

	public function getsolNames(){
		$this->db->where("Block_Code",$this->session->userdata('empblock_code'));
	    $this->db->where("is_active",1);
	    $dev = $this->db->get("mst_solution");
		$res = $dev->num_rows();
		return $res;
	}

	public function getSolused(){
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
		$this->db->where("expiry_date >",$date);
    	$this->db->where("is_active",1);
    	$query = $this->db->get("trn_solutionstock");
    	$res = $query->num_rows();
		return $res;
	}

	public function insertDailycleaning($data){
		
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
		$solused=explode('|',$data["solutionname"]);
		$sol_doc=$solused[0];
		$this->db->where("document_no",$sol_doc);
		//$this->db->where("status_id","1");
		$query = $this->db->get("trn_solutionstock");
		$res_stock = $query->row_array();
		if($res_stock['available_qty'] >= $data["rtc"] and $res_stock['expiry_date'] >= $date)
		{		
		$this->db->where("activity_id",$this->session->userdata('activity_code'));
		$this->db->where("status_id","1");
		$query = $this->db->get("trn_workflowsteps");
		$res = $query->row_array();

		$dp="";
		
		if(isset($data["drainpoint"]))
		{
		foreach ($data["drainpoint"] as $k) {

			$dp=$dp."".$k.",";
		}
		$dp = rtrim($dp,",");
		}

		$roomno = $data["roomno"]!="" ? $data["roomno"]:"";
		$arr = array("document_no"=>$data["doc_id"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"department_name"=>$data["departmentname"],"roomno"=>$roomno,"wastebin"=>$data["wastebin"],"floorcovering"=>$data["floorcovering"],"drain_points"=>$dp,"role_id"=>$this->session->userdata('roleid'),"created_by"=>$this->session->userdata('empcode'),"created_name"=>$this->session->userdata('empname'),"status"=>'1',"next_step"=>$res["next_step"],"available_quantity"=>$data["aqty"],"expiry_date_solution"=>$data["edso"],"required_to_clean"=>$data["rtc"],"sanitizationused"=>$data["solutionname"]);

    if ($this->db->insert("trn_dailycleaning",$arr)) {

    	$room_array= array("in_use"=>'1',"inuse_activity"=>"Daily Cleaning In Progress");
		$this->db->where("room_code", $this->session->userdata("room_code"));
		$this->db->update("mst_room", $room_array);
		
		
		$available_quantity = $data["aqty"] - $data["rtc"];
    	if($available_quantity==0){
    		$arr1 = array("available_qty"=>$available_quantity,"is_active"=>0);
    	}
    	else{
    		$arr1 = array("available_qty"=>$available_quantity);
    	}
    	$solutionname = explode("|", $data["solutionname"]);
    	$this->db->where("document_no",$solutionname[0]);
    	$this->db->update("trn_solutionstock",$arr1);

        $this->db->where("is_active",1);
	    $this->db->where("area_code",$this->session->userdata("area_code"));
	    $this->db->where("room_code",$this->session->userdata("room_code"));
	    $this->db->where("document_no",$data["doc_id"]);
	    $this->db->where("checked_by",null);
	    $query = $this->db->get("trn_dailycleaning");
        $response = $query->row_array();
        $this->db->where("id",17);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}
	}
	else
		{
		$response = array("status"=>1,"id"=>0,"msg"=>"Either not enough solution to start cleaning OR Solution may have just expired ","msgheader"=>"Error Message");
		}

    return $response;

	}

	public function insertDrainpoint($data){
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
		$solused=explode('|',$data["solutionname"]);
		$sol_doc=$solused[0];
		$this->db->where("document_no",$sol_doc);
		//$this->db->where("status_id","1");
		$query = $this->db->get("trn_solutionstock");
		$res_stock = $query->row_array();
		if($res_stock['available_qty'] >= $data["rtc"] and $res_stock['expiry_date'] >= $date)
		{	
		$this->db->where("activity_id",$this->session->userdata('activity_code'));
		$this->db->where("status_id","1");
		$query = $this->db->get("trn_workflowsteps");
		$res = $query->row_array();
    	$arr = array("document_no"=>$data["doc_id"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"department_name"=>$data["departmentname"],"sanitizationused"=>$data["solutionname"],"batch_no"=>$data["batchno"],"drain_points"=>$data["drainpoints"],"identification_drainpoints"=>$data["nodrainpoints"],"check_remark"=>$data["remarks"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$this->session->userdata('empcode'),"created_name"=>$this->session->userdata('empname'),"status"=>'1',"next_step"=>$res["next_step"],"available_quantity"=>$data["aqty"],"expiry_date_solution"=>$data["edso"],"required_to_clean"=>$data["rtc"]);


    if ($this->db->insert("trn_drainpointdetails",$arr)) {
    	
    	$room_array= array("in_use"=>'1',"inuse_activity"=>"Drain Point Cleaning In Progress");
		$this->db->where("room_code", $this->session->userdata("room_code"));
		$this->db->update("mst_room", $room_array);
		
		
		$available_quantity = $data["aqty"] - $data["rtc"];
    	if($available_quantity==0){
    		$arr1 = array("available_qty"=>$available_quantity,"is_active"=>0);
    	}
    	else{
    		$arr1 = array("available_qty"=>$available_quantity);
    	}
    	$solutionname = explode("|", $data["solutionname"]);
    	$this->db->where("document_no",$solutionname[0]);
    	$this->db->update("trn_solutionstock",$arr1);

    	$this->db->where("is_active",1);
	    $this->db->where("area_code",$this->session->userdata("area_code"));
	    $this->db->where("room_code",$this->session->userdata("room_code"));
	    $this->db->where("document_no",$data["doc_id"]);
	    $this->db->where("checked_by",null);
	    $query = $this->db->get("trn_drainpointdetails");
        $response = $query->row_array();
        $this->db->where("id",22);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}}
	else
		{
		$response = array("status"=>1,"id"=>0,"msg"=>"Either not enough solution to start cleaning OR Solution may have just expired ","msgheader"=>"Error Message");
		}


    return $response;
	}

	public function solutionPreparationview(){
		
	$this->db->group_by('doc_id');
	$this->db->where("role_id",$this->session->userdata("roleid"));
    $this->db->where("area_code",$this->session->userdata("area_code"));
    $this->db->where("room_code",$this->session->userdata("room_code"));
    $this->db->where("solution_destroy_checked_by",null);
    $this->db->order_by("created_on","DESC");
    $data = $this->db->get("trn_solutiondetails");
    $response = $data->result_array();
    return $response;
	}

	public function getsolutionPreparationviewnow($id){
		
		$this->db->where("id",$id);
		$this->db->where("role_id",$this->session->userdata("roleid"));
		$query = $this->db->get("trn_solutiondetails");
		$response = $query->row_array();
		return $response;
	}

	public function getsolutionPreparationlogs($did){
		$this->db->where("doc_id",$did);
		$this->db->where("role_id",$this->session->userdata("roleid"));
		$query = $this->db->get("trn_solutiondetails");
		$response = $query->result_array();
		return $response;
	}

	public function solutionPreparationtakeover($data){

		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
	$arr1 = array("solution_destroy_checked_by"=>$this->session->userdata('empcode'),"solution_destroy_checked_name"=>$this->session->userdata('empname'),"checked_on"=>$date,"check_remark"=>$data["remarks"],"is_active"=>0);		
    //$arr1 = array("checked_on"=>$date,"check_remark"=>$data["remarks"],"is_active"=>0);
	$this->db->where("id",$data["id"]);
    $this->db->update("trn_solutiondetails",$arr1);
	$arr01 = array("is_active"=>0);
	$this->db->where("doc_id",$data["doc_id"]);
    $this->db->update("trn_solutiondetails",$arr01);


    	/*$this->db->where("activity_id",$this->session->userdata('activity_code'));
		$this->db->where("status_id","1");
		$query = $this->db->get("trn_workflowsteps");
		$res = $query->row_array();*/

    
		$arr = array("doc_id"=>$data["doc_id"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"solutionname"=>$data["solutionname"],"batch_no"=>$data["batchno"],"qcar_no"=>$data["qcarno"],"department_name"=>$data["departmentname"],"standard_solution_qty"=>$data["stdsolqty"],"purified_water"=>$data["strpurewater"],"makeup_volume"=>$data["markupvol"],"actual_solution_qty"=>$data["actsolqty"]." ".$data["actsoluom"],"actual_purified_water"=>$data["actpurewater"]." ".$data["actpureuom"],"solution_valid_upto"=>$data["validupto"],"role_id"=>$this->session->userdata("roleid"),"created_by"=>$this->session->userdata('empcode'),"created_name"=>$this->session->userdata('empname'),"status"=>$data["status"],"next_step"=>$data["next_step"],"required_quantity"=>$data["requiredquantity"],"expiry_datatime"=>$data["expiry_datatime"],"check_remark"=>$data["remarks"]);

    if ($this->db->insert("trn_solutiondetails",$arr)) {

    	$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("doc_id",$data["doc_id"]);
		$this->db->where("solution_destroy_checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_solutiondetails");
		$response = $query->row_array();

		$this->db->where("id",20);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
    
	}

	public function solutionPreparationdesrory($data){
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
	$arr1 = array("solution_destroy_checked_by"=>$this->session->userdata('empcode'),"solution_destroy_checked_name"=>$this->session->userdata('empname'),"checked_on"=>$date,"check_remark"=>$data["remarks"],"is_active"=>0, "is_destroy"=>1);		
    $this->db->where("id",$data["id"]);
  
    if ($this->db->update("trn_solutiondetails",$arr1)) {
    
	$arr01 = array("is_active"=>0);
	$this->db->where("doc_id",$data["doc_id"]);
    $this->db->update("trn_solutiondetails",$arr01);
	 
	 $arr_stock = array("is_active"=>0);
     $this->db->where("document_no",$data["doc_id"]);
	}
	 if ($this->db->update("trn_solutionstock",$arr_stock)) {

    	$this->db->where("id",21);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}


	public function drainPointview(){

	$this->db->group_by('document_no');
	$this->db->where("role_id",$this->session->userdata("roleid"));
    $this->db->where("area_code",$this->session->userdata("area_code"));
    $this->db->where("room_code",$this->session->userdata("room_code"));
    $this->db->where("checked_by",null);
    $data = $this->db->get("trn_drainpointdetails");
    $response = $data->result_array();
    return $response;	
	}

	public function getdrainPointviewnow($id){
		$this->db->where("id",$id);
		$this->db->where("role_id",$this->session->userdata("roleid"));

		$query = $this->db->get("trn_drainpointdetails");
		$response = $query->row_array();
		return $response;
	}

	public function getdrainPointviewlogs($did){
		$this->db->where("document_no",$did);
		$this->db->where("role_id",$this->session->userdata("roleid"));

		$query = $this->db->get("trn_drainpointdetails");
		$response = $query->result_array();
		return $response;
	}

	public function drainPointfinish($data){
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
		$this->db->where("activity_id",$this->session->userdata('activity_code'));
		$this->db->where("status_id","2");
		$query = $this->db->get("trn_workflowsteps");
		$res = $query->row_array();

	$arr1 = array("role_id"=>$this->session->userdata('roleid'),"checked_by"=>$this->session->userdata('empcode'),"checked_name"=>$this->session->userdata('empname'),"end_time"=>$date,"status"=>'2',"next_step"=>$res["next_step"]);		
    $this->db->where("id",$data["id"]);
  
    if ($this->db->update("trn_drainpointdetails",$arr1)) {
        
       $room_array= array("in_use"=>'0',"inuse_activity"=>"");
		$this->db->where("room_code", $this->session->userdata("room_code"));
		$this->db->update("mst_room", $room_array);
		
		$this->db->where("id",23);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}

	public function drainPointtakeover($data){

		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
	$arr1 = array("checked_by"=>$this->session->userdata('empcode'),"checked_name"=>$this->session->userdata('empname'),"end_time"=>$date,"is_active"=>0);		
    $this->db->where("id",$data["id"]);
    $this->db->update("trn_drainpointdetails",$arr1);

    	/*$this->db->where("activity_id",$this->session->userdata('activity_code'));
		$this->db->where("status_id","1");
		$query = $this->db->get("trn_workflowsteps");
		$res = $query->row_array();*/
    
		$arr = array("document_no"=>$data["doc_id"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"department_name"=>$data["departmentname"],"sanitizationused"=>$data["solutionname"],"batch_no"=>$data["batchno"],"drain_points"=>$data["drainpoints"],"identification_drainpoints"=>$data["nodrainpoints"],"check_remark"=>$data["remarks"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$this->session->userdata('empcode'),"created_name"=>$this->session->userdata('empname'),"status"=>$data["status"],"next_step"=>$data["next_step"],"available_quantity"=>$data["aqty"],"expiry_date_solution"=>$data["edso"],"required_to_clean"=>$data["rtc"]);

    if ($this->db->insert("trn_drainpointdetails",$arr)) {

    	$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("document_no",$data["doc_id"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_drainpointdetails");
		$response = $query->row_array();
		$this->db->where("id",24);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;

	}


	public function dailyCleaningview(){
	
	$this->db->group_by('document_no');
	$this->db->where("role_id",$this->session->userdata("roleid"));

    $this->db->where("area_code",$this->session->userdata("area_code"));
    $this->db->where("room_code",$this->session->userdata("room_code"));
    $this->db->where("checked_by",null);
    $data = $this->db->get("trn_dailycleaning");
    $response = $data->result_array();
    return $response;	
	}

	public function dailyCleaningviewnow($id){
	$this->db->where("id",$id);
	$this->db->where("role_id",$this->session->userdata("roleid"));

	$query = $this->db->get("trn_dailycleaning");
	$response = $query->row_array();
	return $response;		
	}

	public function dailyCleaninglogs($did){
	$this->db->where("document_no",$did);
	$this->db->where("role_id",$this->session->userdata("roleid"));
	$query = $this->db->get("trn_dailycleaning");
	$response = $query->result_array();
	return $response;	
	}

	public function dailyCleaningtakeover($data){
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
	$arr1 = array("checked_by"=>$this->session->userdata('empcode'),"checked_name"=>$this->session->userdata('empname'),"end_time"=>$date,"is_active"=>0);		
    $this->db->where("id",$data["id"]);
    $this->db->update("trn_dailycleaning",$arr1);

    	
    
		$roomno = $data["roomno"]!="" ? $data["roomno"]:"";
		$arr = array("document_no"=>$data["doc_id"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"department_name"=>$data["departmentname"],"roomno"=>$roomno,"wastebin"=>$data["wastebin"],"floorcovering"=>$data["floorcovering"],"drain_points"=>$data["drainpoint"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$this->session->userdata('empcode'),"created_name"=>$this->session->userdata('empname'),"status"=>$data["status"],"next_step"=>$data["next_step"],"available_quantity"=>$data["aqty"],"expiry_date_solution"=>$data["edso"],"required_to_clean"=>$data["rtc"],"sanitizationused"=>$data["solutionname"]);

    if ($this->db->insert("trn_dailycleaning",$arr)) {

    	$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("document_no",$data["doc_id"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_dailycleaning");
		$response = $query->row_array();
        $this->db->where("id",18);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}


	public function dailyCleaningfinish($data){
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
        $this->db->where("activity_id",$this->session->userdata('activity_code'));
		$this->db->where("status_id","2");
		$query = $this->db->get("trn_workflowsteps");
		$res = $query->row_array();


	$arr1 = array("role_id"=>$this->session->userdata('roleid'),"checked_by"=>$this->session->userdata('empcode'),"checked_name"=>$this->session->userdata('empname'),"end_time"=>$date,"status"=>'2',"next_step"=>$res["next_step"]);		
    $this->db->where("id",$data["id"]);
  
    if ($this->db->update("trn_dailycleaning",$arr1)) {
		
		$room_array= array("in_use"=>'0',"inuse_activity"=>"");
		$this->db->where("room_code", $this->session->userdata("room_code"));
		$this->db->update("mst_room", $room_array);

        $this->db->where("id",16);
		$mres = $this->db->get("mst_messages");
		$msg = $mres->row_array();
        $response = array("status"=>1,"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}

	public function get_area_name($area){
		$this->db->where("area_code",$area);
		$query = $this->db->get("mst_area");
		$response = $query->row_array();
		return $response["area_name"];
	}

	public function get_view($ac){
		$this->db->select("activityfunc_tocall");
		$this->db->where("activity_code",$ac);
		$this->db->group_by("activity_code");
		$query = $this->db->get("mst_activity");
		$response = $query->row_array();
		return $response["activityfunc_tocall"];	
	}

	public function geteqdetails($rid,$tablename){
		$this->db->where("document_portable_id",$rid);
		$query = $this->db->get($tablename);
		$response = $query->result_array();
		return $response;
	}

}


