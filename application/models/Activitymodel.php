<?php

class Activitymodel extends CI_Model {
	
	public function getActivities($data)
	{
		//$this->db->group_by('activity_code');
		
	    $this->db->where("area_code",$data["areaselected"]);
		$q = $this->db->get('mst_activity');
		if($q->num_rows() > 0)
		{	
			return $q->result_array();
		}		
	}

	public function getActivitiesByBlock($data)
	{
		//$this->db->group_by('activity_code');
		//$areacode="";
		$this->db->where("block_code",$data["blockselected"]);
		$this->db->where("is_active",1);
		$q = $this->db->get('mst_area');
		$result = $q->result_array();
		foreach ($result as $act) {
		$areacode[] = $act["area_code"];
		}
			//$areaselected = rtrim($areacode,",");	    
		
		$this->db->select('activity_code,activity_name,activitytype_id,activity_type,group_concat(area_code) As area_code');
		$this->db->where_in("area_code",$areacode);
		$this->db->where("is_active",1);
		$this->db->group_by("activity_code");
        $this->db->order_by("activity_type", 'ASC');
		$q = $this->db->get('mst_activity');
		if($q->num_rows() > 0)
		{	
			return $q->result_array();
		}		
	}

	public function getRole()
	{
		$this->db->where("is_active",1);
		$q = $this->db->get('mst_role');
		if($q->num_rows() > 0)
		{	
			return $q->result_array();
		}		
	}

	public function getBlocks(){
		$this->db->where("block_code !=","Block-000");
		$this->db->where("is_active",1);
		$this->db->select("block_code");
		$query = $this->db->get('mst_block');
		return $query->result_array();
	}

	public function getSubblocks($data){
		$this->db->select("department_code");
		$this->db->where("block_code",$data["block"]);
		$this->db->group_by("department_code");
		$query = $this->db->get('mst_department');
		return $query->result_array();
	}

	public function getAreas($data){
		$this->db->select("area_code");
		$this->db->where("department_code",$data["subblock"]);
		$query = $this->db->get('mst_area');
		return $query->result_array();
	}
	
	public function insertActivityRole($data){

		$this->db->where("block_code",$data['block']);
		//$this->db->where("area_code",$data['area']);
		$this->db->delete('trn_activityrole');

		//$rcount = count($data['roles']);
		$i=1;
		foreach ($data['roles'] as $a) {
		if(isset($data['activity'.$i])){	
		$mstRole = $a;
		$mstActivity = $data['activity'.$i];
		$cby = $this->session->userdata('empcode');
		$error = 0;
		
		foreach($data['activity'.$i] as $activity){
			
			//$sepvalue = explode(",",$activity);
		    //foreach($sepvalue as $activity1)
			//{
			$sepActivity = explode("##",$activity);
			$activityCode = $sepActivity[0];
			$activityTypeId = $sepActivity[1];
			$activityareacode = $sepActivity[2];
			$separeavalue = explode(",",$activityareacode);
			foreach($separeavalue as $actualareacode)
			{

			/*$this->db->select("ar.block_code as block_code");
			$this->db->from("mst_activity as ac");
			$this->db->join("mst_area as ar","ar.area_code=ac.area_code","INNER");
			$this->db->where("ac.activity_code",$activityCode);
			$this->db->where("ac.activitytype_id",$activityTypeId);
			$this->db->where("ac.area_code",$activityareacode);
			$query = $this->db->get();
			$res = $query->row_array();*/
			$arr = array('role_id'=>$mstRole,'activity_typeid'=>$activityTypeId,'activity_id'=>$activityCode,'created_by'=>$cby,'modified_by'=>$cby,'block_code'=>$data["block"],'area_code'=>$actualareacode);
			//$this->db->insert('trn_activityrole',$arr);
			if ($this->db->insert('trn_activityrole',$arr)) {
				$error = 1;				
			} 
		}
		}
		}
		$i++;
		}
		

		if($error==1){
			$response = array("status"=>1,"msg"=>"Successfully Inserted","msgheader"=>"Successfully Inserted");
		}else{
			$response = array("status"=>0);
		}	
		
		return $response;
	}
	
	public function insertActivityRoleOld($data){
		$mstRole = $data['roles'];
		$mstActivity = $data['activity'];
		$sepActivity = explode("##",$mstActivity);
		$activityCode = $sepActivity[0];
		$activityTypeId = $sepActivity[1];
		$cby = $this->session->userdata('empcode');
		$error = 0;
		foreach($mstRole as $role){
			$arr = array('role_id'=>$role,'activity_typeid'=>$activityTypeId,'activity_id'=>$activityCode,'created_by'=>$cby,'modified_by'=>$cby);
			//$this->db->insert('trn_activityrole',$arr);
			if ($this->db->insert('trn_activityrole',$arr)) {
				$error = 1;				
			} 
		}
		
		if($error==1){
			$response = array("status"=>1,"msg"=>"Successfully Inserted","msgheader"=>"Successfully Inserted");
		}else{
			$response = array("status"=>0);
		}	
		
		return $response;
	}
	
	public function getActivityByRoles($role_id)
	{
		$this->db->where('role_id', $role_id);
		$this->db->where('is_active',1);
		$q = $this->db->get('trn_activityrole');
		if($q->num_rows() > 0)
		{
			return $q->result_array();
		}	
		else
		{
			return false;
		}	
	}
	
	public function getTrnActivity()
	{
		$this->db->select('trn_activityrole.id,trn_activityrole.role_id,trn_activityrole.activity_typeid,trn_activityrole.activity_id,trn_activityrole.created_by,mst_role.role_description,mst_activitytype.activitytype_name,mst_activity.activity_name');
		$this->db->from('trn_activityrole');
		$this->db->join('mst_role', 'mst_role.id = trn_activityrole.role_id','Left');
		$this->db->join('mst_activitytype', 'mst_activitytype.id = trn_activityrole.activity_typeid','Left');
		$this->db->join('(SELECT activity_code,activity_name FROM `mst_activity` GROUP BY activity_code) as mst_activity', 'mst_activity.activity_code = trn_activityrole.activity_id','Left');
		$query=$this->db->get();

		if($query->num_rows() > 0)
		{
			return $query->result_array();
		}	
		else
		{
			return false;
		}	
	}
	
	public function deleteTrnActivityById($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('trn_activityrole');
		return true;
	}

	public function selectedActivity(){
		$this->db->group_by("role_id");
		$dd = $this->db->get("trn_activityrole");
		foreach ($dd->result_array() as $k) {
			$this->db->where("role_id",$k["role_id"]);
			$dd2 = $this->db->get("trn_activityrole");
			$response[] = array("rid"=>$k["role_id"],"ndata"=>$dd2->result_array());
		 	
		}
		return $response;
	}

	public function sA($role_id){
		$this->db->where("role_id",$role_id);
        $this->db->where("is_active",1);
        $dd2 = $this->db->get("trn_activityrole");
		$response = array("ndata"=>$dd2->result_array());
		return $response;
	}

}