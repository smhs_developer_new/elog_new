<?php

class Productionmodel extends CI_Model {
	
	public function get_production_areadtl()
	{
		$this->db->where('is_active',1);
		$q = $this->db->get('mst_area');
		if($q->num_rows() > 0)
		{
			return $q;
		}	
		else
		{
			return false;
		}	
	}

	public function get_production_roomdtl()
	{
		$this->db->where('is_active',1);
		$q = $this->db->get('mst_room');
		if($q->num_rows() > 0)
		{
			return $q;
		}	
		else
		{
			return false;
		}	
	}

	public function get_production_batchdtl()
	{
		$this->db->where('is_active',1);
		$q = $this->db->get('trn_batch');
		if($q->num_rows() > 0)
		{
			return $q;
		}	
		else
		{
			return false;
		}	
	}

	public function get_dtl4($colname,$colval,$colname2,$colval2,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		if($q->num_rows() > 0)
		{
			return $q;
		}	
		else
		{
			return false;
		}	
	}

	public function get_dtl3($id,$colname,$colval,$tblname)
	{
		$this->db->where('id',$id);
		$this->db->where($colname, $colval);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		if($q->num_rows() > 0)
		{
			return $q;
		}	
		else
		{
			return false;
		}	
	}

	public function get_dtl2($colname,$colval,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		if($q->num_rows() > 0)
		{
			return $q;
		}	
		else
		{
			return false;
		}	
	}

	public function getlike_dtl2($colname,$colval,$tblname)
	{
		$this->db->like($colname, $colval);
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		if($q->num_rows() > 0)
		{
			return $q;
		}	
		else
		{
			return false;
		}	
	}

	public function get_dtl1($tblname)
	{
		$this->db->where('is_active',1);
		$q = $this->db->get($tblname);
		if($q->num_rows() > 0)
		{
			return $q;
		}	
		else
		{
			return false;
		}	
	}

	public function getProductname($str){
	    $this->db->group_start();
	    $this->db->like('product_name', $str);
	    $this->db->group_end();
	    $this->db->where("is_active",1);
	    $dev = $this->db->get("mst_product");
	    foreach ($dev->result_array() as $sn) {
	      $response[] = $sn["product_name"];
	    }
	    return $response;
	}

	public function getAccessory(){
		$this->db->select("eq.equipment_name as equipment_name,sop.sop_name as sop_name");
		$this->db->from("mst_equipment as eq");
		$this->db->join("mst_sop as sop","sop.sop_code=eq.sop_code","INNER");
		$this->db->where("eq.room_code",$this->session->userdata("room_code"));
		$query = $this->db->get();
		foreach ($query->result_array() as $r) {
			$res[] = $r["equipment_name"]."-".$r["sop_name"];
		}
		return $res;
	}

	public function insertAccessorycleaning($data){
	$aa="";
		foreach ($data["accessorylist"] as $k) {

			$aa=$aa."".$k."@";
		}
		$acc = rtrim($aa,"@");
		$arr = array("document_no"=>$data["doc_id"],"batch_no"=>$data["batchno"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"department_name"=>$data["departmentname"],"product_name"=>$data["productname"],"accessorylist"=>$acc,"remarks"=>$data["remarks"],"created_by"=>$this->session->userdata('empcode'),"performed_by"=>$this->session->userdata('empcode'));

    if ($this->db->insert("trn_accessorycleaning",$arr)) {

    	$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("document_no",$data["doc_id"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_accessorycleaning");
		$response = $query->row_array();
        $response = array("status"=>1,"id"=>$response["id"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}

	public function accessoryCleaningtakeover($data){
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
	$arr1 = array("checked_by"=>$this->session->userdata('empcode'),"stop_time"=>$date);		
    $this->db->where("id",$data["id"]);
    $this->db->update("trn_accessorycleaning",$arr1);
    /*$aa="";
		foreach ($data["accessorylist"] as $k) {

			$aa=$aa."".$k."@";
		}
		$acc = rtrim($aa,"@");
		*/
		$arr = array("document_no"=>$data["doc_id"],"batch_no"=>$data["batchno"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"department_name"=>$data["departmentname"],"product_name"=>$data["productname"],"accessorylist"=>$data["accessorylist"],"remarks"=>$data["remarks"],"created_by"=>$this->session->userdata('empcode'),"performed_by"=>$this->session->userdata('empcode'));

    if ($this->db->insert("trn_accessorycleaning",$arr)) {

    	$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("document_no",$data["doc_id"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_accessorycleaning");
		$response = $query->row_array();
        $response = array("status"=>1,"id"=>$response["id"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}

	public function accessoryCleaningfinish($data){
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
	$arr1 = array("checked_by"=>$this->session->userdata('empcode'),"stop_time"=>$date);		
    $this->db->where("id",$data["id"]);
  
    if ($this->db->update("trn_accessorycleaning",$arr1)) {
        $response = array("status"=>1);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}

	public function getaccessoryCleaninglogs($did){
		$this->db->where("document_no",$did);
		$query = $this->db->get("trn_accessorycleaning");
		$response = $query->result_array();
		return $response;
	}

	public function getaccessoryCleaningview(){
		$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_accessorycleaning");
		$response = $query->result_array();
		return $response;
	}

	public function getaccessoryCleaningviewnow($id){
		$this->db->where("id",$id);
		$query = $this->db->get("trn_accessorycleaning");
		$response = $query->row_array();
		return $response;
	}

	public function getportableEquipmentcleaning(){
		$this->db->select("eq.equipment_name as equipment_name,eq.room_code as room_code,sop.sop_name as sop_name");
		$this->db->from("mst_equipment as eq");
		$this->db->join("mst_sop as sop","sop.sop_code=eq.sop_code","INNER");
		$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("eq.equipment_type","Portable");
		$res = $this->db->get();
		return $res->result_array();
	}

	public function inprocesscleaningDetails($data){

		$arr = array("document_no"=>$data["doc_id"],"batch_no"=>$data["batchno"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"department_name"=>$data["departmentname"],"product_name"=>$data["productname"],"sop_no"=>$data["sopno"],"remarks"=>$data["remarks"],"created_by"=>$this->session->userdata('empcode'),"created_by"=>$this->session->userdata('empcode'));

    if ($this->db->insert("trn_inprocesscontainercleaning",$arr)) {

    	$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("document_no",$data["doc_id"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_inprocesscontainercleaning");
		$response = $query->row_array();
        $response = array("status"=>1,"id"=>$response["id"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}

	public function getsopList($str){
		$this->db->group_start();
	    $this->db->like('sop_code', $str);
	    $this->db->group_end();
	    $this->db->where("area_code",$this->session->userdata("area_code"));
	    $this->db->where("is_active",1);
	    $dev = $this->db->get("mst_sop");
	    foreach ($dev->result_array() as $sn) {
	      $response[] = $sn["sop_code"];
	    }
	    return $response;
	}

	public function getinprocessContainercleaningviewnow($id){
		$this->db->where("id",$id);
		$query = $this->db->get("trn_inprocesscontainercleaning");
		$response = $query->row_array();
		return $response;
	}

	public function getinprocessContainercleaninglogs($did){
		$this->db->where("document_no",$did);
		$query = $this->db->get("trn_inprocesscontainercleaning");
		$response = $query->result_array();
		return $response;
	}

	public function inprocesscontainerCleaningfinish($data){
		date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
	$arr1 = array("checked_by"=>$this->session->userdata('empcode'),"end_time"=>$date);		
    $this->db->where("id",$data["id"]);
  
    if ($this->db->update("trn_inprocesscontainercleaning",$arr1)) {
        $response = array("status"=>1);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;
	}

	public function inprocesscontainerCleaningtakeover($data){
	date_default_timezone_set('Asia/Calcutta');
		$date = date("Y-m-d H:i:s");
	$arr1 = array("checked_by"=>$this->session->userdata('empcode'),"end_time"=>$date);		
    $this->db->where("id",$data["id"]);
    $this->db->update("trn_inprocesscontainercleaning",$arr1);

    $arr = array("document_no"=>$data["doc_id"],"batch_no"=>$data["batchno"],"area_code"=>$this->session->userdata('area_code'),"room_code"=>$this->session->userdata("room_code"),"department_name"=>$data["departmentname"],"product_name"=>$data["productname"],"sop_no"=>$data["sopno"],"remarks"=>$data["remarks"],"created_by"=>$this->session->userdata('empcode'),"created_by"=>$this->session->userdata('empcode'));

    if ($this->db->insert("trn_inprocesscontainercleaning",$arr)) {

    	$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("document_no",$data["doc_id"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_inprocesscontainercleaning");
		$response = $query->row_array();
        $response = array("status"=>1,"id"=>$response["id"]);
     } 
     else {
        $response = array("status"=>0);
  	}

    return $response;	
	}

	public function getinprocessContainercleaningview(){
		$this->db->where("area_code",$this->session->userdata("area_code"));
		$this->db->where("room_code",$this->session->userdata("room_code"));
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_inprocesscontainercleaning");
		$response = $query->result_array();
		return $response;
	}

	public function getEquipmentsdtl(){
		$this->db->select("eq.id as id,eq.equipment_code as equipment_code,eq.equipment_name as equipment_name,eq.equipment_icon as equipment_icon,eq.equipment_type as equipment_type,sop.sop_code as sop_code,sop.sop_name as sop_name");
		$this->db->from("mst_equipment as eq");
		$this->db->join("mst_sop as sop","sop.sop_code=eq.sop_code","INNER");
		$this->db->where("eq.room_code",$this->session->userdata("room_code"));
		$this->db->where('eq.is_active',1);
		$this->db->where('sop.is_active',1);
		$q= $this->db->get();
		if($q->num_rows() > 0)
		{
			return $q;
		}	
		else
		{
			return false;
		}
	}

	public function getpendingtaskEquipmentsdtl($header_id){
		$this->db->select("eq.id as id,opeql.equipment_code as equipment_code,eq.equipment_name as equipment_name,eq.equipment_icon as equipment_icon,opeql.equipment_type as equipment_type,opeql.brkdwn_strattime,opeql.brkdwn_endtime,opeql.performed_by,sop.sop_code as sop_code,sop.sop_name as sop_name");
		$this->db->from("mst_equipment as eq");
		$this->db->join("trn_operationequipmentlog as opeql","opeql.equipment_code=eq.equipment_code","INNER");
		$this->db->join("mst_sop as sop","sop.sop_code=opeql.sop_code","INNER");
		$this->db->where("opeql.header_id",$header_id);
		$this->db->where("opeql.brkdwn_strattime",0);
		//$this->db->order_by("opeql.equipment_code","DESC");
		$q= $this->db->get();
		if($q->num_rows() > 0)
		{
			return $q;
		}	
	}

	public function getresultset_of_eachEuipment_of_Pendingtask($header_id,$equipment_code){
		$this->db->select("id,header_id,equipment_code,equipment_type,sop_code,brkdwn_strattime,brkdwn_endtime,brkdwn_checked_by,performed_by");
		$this->db->where("header_id",$header_id);
		$this->db->where("equipment_code",$equipment_code);
		$this->db->order_by("brkdwn_strattime","DESC");
		$this->db->order_by("brkdwn_endtime","DESC");
		$q= $this->db->get("trn_operationequipmentlog");
		if($q->num_rows() > 0)
		{
			return $q->result_array();
		}	
	}

	public function get_HeaderID10($colname,$colval,$colname2,$colval2,$colname3,$colval3,$colname4,$colval4,$colname5,$colval5,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where($colname3, $colval3);
		$this->db->where($colname4, $colval4);
		$this->db->where($colname5, $colval5);
		$this->db->where('is_active',1);
		$this->db->limit(1);
		$this->db->order_by("id","ASC");
		$q = $this->db->get($tblname);
		if($q->num_rows() > 0)
		{
			$res = $q->row_array();
			$response = $res["id"]; 
			return $response;
		}		
	}

	public function brkdownfinish($header_id,$equipment_code,$endtime,$checked_by,$memo_number,$remark){

		$arr1 = array("checked_by"=>$checked_by,"brkdwn_endtime"=>$endtime,"memo_number"=>$memo_number,"remark"=>$remark,"brkdwn_checked_by"=>$checked_by,"modified_by"=>$checked_by,"modified_on"=>$endtime);		
		$this->db->where("header_id",$header_id); 
		$this->db->where("equipment_code",$equipment_code);  
		if($this->db->update("trn_operationequipmentlog",$arr1)){
			$response = array("status"=>1);
		 } 
		 else {
			$response = array("status"=>0);
		  }
		  }
}