<?php

//error_reporting(1);
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ptscommon extends CI_Model {

    const records_per_page = 10;

    function __construct() {
        parent::__construct();
        $this->load->database('');
    }

    //get the no of total records from mysql table
    public function getRowsCount($db, $table, $param, $order = NULL) {
        $CI = &get_instance();
        $this->db = $CI->load->database($db, TRUE);
        $this->db->from($table);
        if (!empty($param)) {
            $this->db->where($param);
        }
        if ($table == 'pts_mst_server_detail') {
            $this->db->order_by("id", "DESC");
        }
        if ($table == 'pts_mst_activity' || $table == 'pts_mst_master' || $table == 'pts_mst_report') {
            $this->db->order_by("id", "ASC");
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    // get the data fom mysql table
    public function getMultipleRowswithgroupbyCount($db, $table, $param, $group) {
        $CI = &get_instance();
        $this->db = $CI->load->database($db, TRUE);
        $this->db->from($table);
        if (!empty($param)) {
            $this->db->where($param);
        }

        if ($table == 'pts_trn_room_log_activity') {
            $this->db->join("pts_mst_sys_id", "pts_trn_room_log_activity.room_id=pts_mst_sys_id.room_id");
            $this->db->where("pts_mst_sys_id.status", 'active');
            $this->db->group_by('pts_trn_room_log_activity.room_id');
        } else {
            $this->db->group_by($group);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

    // get the data fom mysql table
    public function getMultipleRows($db, $table, $param = [], $order = NULL, $page = null) {
        $CI = &get_instance();
        $this->db = $CI->load->database($db, TRUE);
        $this->db->from($table);
//        print_r($param);exit;
        if (!empty($param) && !isset($param['room_array'])) {
            $this->db->where($param);
        }
        if (isset($param['room_array'])) {
            $this->db->where_in("room_code", $param['room_array']);
            $this->db->where("status", "active");
        }
        if ($table == 'pts_mst_server_detail') {
            $this->db->order_by("id", "DESC");
        }
        if ($table == 'pts_mst_activity' || $table == 'pts_mst_master' || $table == 'pts_mst_report') {
            $this->db->order_by("id", "ASC");
        }

        //$this->db->order_by('id', 'DESC');

        if (!empty($page)) {
            $offset = ($page - 1) * (self::records_per_page);
            $this->db->limit(self::records_per_page, $offset);
        }
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        } else {
            return array();
        }
    }

    // get the data fom mysql table
    public function getMultipleRowsNewCreate($db, $table, $param = [], $order = NULL, $page = null) {
        $CI = &get_instance();
        $this->db = $CI->load->database($db, TRUE);
        $this->db->from($table);

        if (!empty($param) && !isset($param['room_array'])) {
            $equiptype = ['Fixed', 'Booth'];
            $this->db->where($param);
            $this->db->where_in('equipment_type', $equiptype);
        }
        if (isset($param['room_array'])) {
            $this->db->where_in("room_code", $param['room_array']);
            $this->db->where("status", "active");
        }
        if ($table == 'pts_mst_server_detail') {
            $this->db->order_by("id", "DESC");
        }
        if ($table == 'pts_mst_activity' || $table == 'pts_mst_master' || $table == 'pts_mst_report') {
            $this->db->order_by("id", "ASC");
        }
        //$this->db->order_by('id', 'DESC');
        if (!empty($page)) {
            $offset = ($page - 1) * (self::records_per_page);
            $this->db->limit(self::records_per_page, $offset);
        }
        $query = $this->db->get();
//        echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        } else {
            return array();
        }
    }

    // get the data fom mysql table
    public function getMultipleRowswithgroupby($db, $table, $param, $group, $page = null,$orderby=NULL,$order='DESC') {
        $CI = &get_instance();
        $this->db = $CI->load->database($db, TRUE);
        $this->db->from($table);
        if (!empty($param)) {
            $this->db->where($param);
        }
        if ($table == 'pts_trn_room_log_activity') {
            $this->db->join("pts_mst_sys_id", "pts_trn_room_log_activity.room_id=pts_mst_sys_id.room_id");
            $this->db->where("pts_mst_sys_id.status", 'active');
            $this->db->group_by('pts_trn_room_log_activity.room_id');
        } else {
            $this->db->group_by($group);
        }
        if(!empty($orderby)){
            $this->db->order_by($orderby,$order);
        }
        if (!empty($page)) {
            $offset = ($page - 1) * (self::records_per_page);
            $this->db->limit(self::records_per_page, $offset);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        } else {
            return array();
        }
    }

    /**
     * add single entry in table
     * $tablename=table name
     * $post=data in array formats like array('id'=>$val['id'],'name'=>$name)
     */
    public function addToTable($db, $tablename, $post = '', $param = '') {
        $CI = &get_instance();
        $this->db2 = $CI->load->database($db, TRUE);
        if (!empty($param)) {
            $insert = $this->db2->query($param);
            if (!$insert) {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            $insert = $this->db2->insert($tablename, $post) or die(" error" . mysqli_error($this->db2));
            if (!$insert) {
                return FALSE;
            } else {
                return $this->db2->insert_id();
            }
        }
    }

    /**
     * update single entry in table
     * $tablename=table name
     * $post=data in array formats like array('id'=>$val['id'],'name'=>$name)
     */
    public function updateToTable($db, $tablename, $post = '', $param = '') {
        $CI = &get_instance();
        $this->db2 = $CI->load->database($db, TRUE);
        $this->db2->where($param);
        $insert = $this->db2->update($tablename, $post) or die(" error" . mysqli_error($this->db2));
        if (!$insert) {
            return FALSE;
        } else {
            return $this->db2->insert_id();
        }
    }

    /**
     * Get Inprogress Activity List
     * Created By :Rahul Chauhan
     * Date:27-01-2020
     */
    public function GetInprogressActivityList($system_id) {
        $actid = isset($_GET['actid']) ? $_GET['actid'] : '';
        $docid = isset($_GET['doc_id']) ? $_GET['doc_id'] : '';
        $result = array();
        $this->db->select("pts_trn_workflowsteps.next_step,pts_trn_workflowsteps.role_id,pts_trn_user_activity_log.activity_stop,pts_trn_user_activity_log.workflowstatus,pts_trn_user_activity_log.workflownextstep,pts_trn_user_activity_log.id as act_id,pts_trn_user_activity_log.activity_id as activity_id,pts_trn_user_activity_log.doc_id,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.activity_remarks,pts_mst_activity.activity_name,pts_trn_user_activity_log.roomlogactivity as processid,pts_mst_activity.type,pts_mst_activity.activity_url,pts_trn_user_activity_log.equip_code,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_start, '%Y-%m-%d') as start_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,from_unixtime(pts_trn_user_activity_log.activity_start, '%Y-%m-%d') as start_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%Y-%m-%d') as stop_date,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.workflownextstep,pts_trn_user_activity_log.user_id,pts_trn_user_activity_log.stop_by,pts_trn_user_activity_log.stop_by_id,pts_trn_user_activity_log.dies_no,pts_trn_user_activity_log.sampling_rod_id,pts_trn_user_activity_log.act_name,pts_trn_user_activity_log.id as log_id,pts_trn_user_activity_log.user_id,pts_mst_activity.activity_name as home_activity,pts_mst_report_header.form_no,pts_mst_report_header.user_version_no as version_no,pts_mst_report_header.effective_date,pts_mst_report_header.retired_date,pts_mst_report_header.id as headerid,pts_mst_sys_id.room_type,pts_trn_user_activity_log.selection_status");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->join("pts_trn_room_log_activity", "pts_trn_room_log_activity.mst_act_id=pts_trn_user_activity_log.roomlogactivity", 'left');
        $this->db->join("pts_trn_workflowsteps", "pts_trn_user_activity_log.activity_id=pts_trn_workflowsteps.activity_id", 'left');
        $this->db->join("pts_mst_activity", "pts_mst_activity.id=pts_trn_user_activity_log.activity_id", 'left');
        $this->db->join("pts_mst_report_header", "pts_mst_report_header.id=pts_trn_user_activity_log.versionid", 'left');
        $this->db->join("pts_mst_sys_id", "pts_mst_sys_id.room_code=pts_trn_user_activity_log.room_code", 'left');
        $this->db->where("pts_trn_user_activity_log.workflownextstep>", '-1');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
        if($system_id!=''){
        $this->db->where("pts_mst_sys_id.device_id", $system_id);
        }
        if ($docid != "" && $docid != NULL && $actid == "") {
            $this->db->where("pts_trn_user_activity_log.doc_id", $docid);
        }
        if ($actid > 0) {
            $this->db->where("pts_trn_user_activity_log.id", $actid);
        }
        $this->db->order_by("pts_trn_user_activity_log.activity_start", 'DESC');
        //$this->db->group_by("pts_trn_user_activity_log.activity_id");
        $this->db->group_by("pts_trn_user_activity_log.doc_id");
        $result = $this->db->get()->result_array();
//       echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Get QA Approval Activity List
     * Created By :Rahul Chauhan
     * Date:27-01-2020
     */
    public function GetQaApprovalActivityList($system_id) {
        $result = array();
//        $this->db->select("pts_trn_user_activity_log.*,pts_trn_user_activity_log.id as act_id,pts_trn_user_activity_log.activity_id,pts_trn_user_activity_log.doc_id,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.activity_remarks,pts_trn_room_log_activity.activity_name,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_start, '%Y-%m-%d') as start_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%Y-%m-%d') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.stop_by,pts_trn_user_activity_log.stop_by_id");
//        $this->db->from("pts_trn_user_activity_log");
//        $this->db->join("pts_trn_room_log_activity", "pts_trn_room_log_activity.mst_act_id=pts_trn_user_activity_log.roomlogactivity", 'left');
//        $this->db->where("pts_trn_user_activity_log.status", 'active');
//        $this->db->where("pts_trn_user_activity_log.is_qa_approve", 'no');
//        $this->db->group_by("pts_trn_user_activity_log.doc_id");
//        $result = $this->db->get()->result_array();
        
        $this->db->select("pts_trn_workflowsteps.next_step,pts_trn_workflowsteps.role_id,pts_trn_user_activity_log.activity_stop,pts_trn_user_activity_log.workflowstatus,pts_trn_user_activity_log.workflownextstep,pts_trn_user_activity_log.id as act_id,pts_trn_user_activity_log.activity_id as activity_id,pts_trn_user_activity_log.doc_id,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.activity_remarks,pts_mst_activity.activity_name,pts_trn_user_activity_log.roomlogactivity as processid,pts_mst_activity.type,pts_mst_activity.activity_url,pts_trn_user_activity_log.equip_code,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_start, '%Y-%m-%d') as start_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,from_unixtime(pts_trn_user_activity_log.activity_start, '%Y-%m-%d') as start_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%Y-%m-%d') as stop_date,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.workflownextstep,pts_trn_user_activity_log.user_id,pts_trn_user_activity_log.stop_by,pts_trn_user_activity_log.stop_by_id,pts_trn_user_activity_log.dies_no,pts_trn_user_activity_log.sampling_rod_id,pts_trn_user_activity_log.act_name,pts_trn_user_activity_log.id as log_id,pts_trn_user_activity_log.user_id,pts_mst_activity.activity_name as home_activity,pts_mst_report_header.form_no,pts_mst_report_header.user_version_no as version_no,pts_mst_report_header.effective_date,pts_mst_report_header.retired_date,pts_mst_report_header.id as headerid,pts_mst_sys_id.room_type,pts_trn_user_activity_log.selection_status");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->join("pts_trn_room_log_activity", "pts_trn_room_log_activity.mst_act_id=pts_trn_user_activity_log.roomlogactivity", 'left');
        $this->db->join("pts_trn_workflowsteps", "pts_trn_user_activity_log.activity_id=pts_trn_workflowsteps.activity_id", 'left');
        $this->db->join("pts_mst_activity", "pts_mst_activity.id=pts_trn_user_activity_log.activity_id", 'left');
        $this->db->join("pts_mst_report_header", "pts_mst_report_header.id=pts_trn_user_activity_log.versionid", 'left');
        $this->db->join("pts_mst_sys_id", "pts_mst_sys_id.room_code=pts_trn_user_activity_log.room_code", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
        $this->db->where("pts_trn_user_activity_log.is_qa_approve", 'no');
        $this->db->where("pts_trn_user_activity_log.`workflownextstep`", '-1');
        $this->db->order_by("pts_trn_user_activity_log.activity_start", 'DESC');
        if($system_id!=''){
            $this->db->where("pts_mst_sys_id.device_id", $system_id);
        }
        //$this->db->group_by("pts_trn_user_activity_log.activity_id");
        $this->db->group_by("pts_trn_user_activity_log.doc_id");
        $result = $this->db->get()->result_array();
        //echo $this->db->last_query();exit;
        
        return $result;
    }

    /**
     * Make Approval Record into approval table and updated inprogress activity too
     * Created By :Bhupendra kumar
     * Date:28-01-2020
     */
    public function Senttonextapproval($actlogtblid, $actid, $roleid, $remark, $status_id, $next_step, $empid, $empname, $email, $actstatus) {
        $arr = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'approve', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => $actstatus);
        $this->db->insert("pts_trn_activity_approval_log", $arr);

        $arr2 = array("workflowstatus" => $status_id, "workflownextstep" => $next_step);
        $this->db->where("id", $actlogtblid);
        if ($this->db->update("pts_trn_user_activity_log", $arr2)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function Sentfornextapproval($actlogtblid, $actid, $roleid, $remark, $status_id, $next_step, $empid, $empname, $email, $actstatus, $docno, $tblname, $result_rgt, $result_lft) {
        $arr = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'approve', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => $actstatus);
        $this->db->insert("pts_trn_activity_approval_log", $arr);

        if ($next_step == -1) {
            $ar = array("is_in_workflow" => 'no');
            if ($result_rgt != '' && $result_lft != '') {
                $ar['result_left '] = $result_lft;
                $ar['result_right'] = $result_rgt;
            }
            $this->db->where("doc_no", $docno);
            $this->db->update($tblname, $ar);
//            echo $this->db->last_query();
            $ar2 = array("workflowstatus" => $status_id, "workflownextstep" => $next_step, "activity_stop" => time());
            $this->db->where("id", $actlogtblid);
            if ($this->db->update("pts_trn_user_activity_log", $ar2)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            $arr2 = array("workflowstatus" => $status_id, "workflownextstep" => $next_step);
            $this->db->where("id", $actlogtblid);
            if ($this->db->update("pts_trn_user_activity_log", $arr2)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function SenttoQAapproval($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email) {
        $arr = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'QAapprove', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time());
        $this->db->insert("pts_trn_activity_approval_log", $arr);

        $arr2 = array("is_qa_approve" => 'yes', "qa_approved_by_userid" => $empid);
        $this->db->where("id", $actlogtblid);
        if ($this->db->update("pts_trn_user_activity_log", $arr2)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Stop running activity
     * Created By :Bhupendra kumar
     * Date:28-01-2020
     */
    public function Senttostop($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email, $status_id, $next_step, $stopreason) {
        $arr = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'stop', "reason" => $stopreason);

        $this->db->insert("pts_trn_activity_approval_log", $arr);

        $arr2 = array("workflowstatus" => $status_id, "workflownextstep" => $next_step, "activity_stop" => time(), "stop_by" => $empname, "stop_by_id" => $empid);
        $this->db->where("id", $actlogtblid);
        if ($this->db->update("pts_trn_user_activity_log", $arr2)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Stop running activity
     * Created By :Bhupendra kumar
     * Date:28-01-2020
     */
    public function vaccumcleanerstop($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email, $stopreason, $status_id, $next_step) {
        $arr = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'stop', "reason" => $stopreason);

        $this->db->insert("pts_trn_activity_approval_log", $arr);

        $arr2 = array("workflowstatus" => $status_id, "workflownextstep" => $next_step, "activity_stop" => time(), "stop_by" => $empname, "stop_by_id" => $empid);
        $this->db->where("id", $actlogtblid);
        if ($this->db->update("pts_trn_user_activity_log", $arr2)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Stop running activity
     * Created By :Bhupendra kumar
     * Date:28-01-2020
     */
    public function prefilterstop($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email, $stopreason) {
        $arr = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'stop', "reason" => $stopreason);

        $this->db->insert("pts_trn_activity_approval_log", $arr);

        $arr2 = array("activity_stop" => time());
        $this->db->where("id", $actlogtblid);
        if ($this->db->update("pts_trn_user_activity_log", $arr2)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Stop running activity
     * Created By :Rahul Chauhan
     * Date:18-09-2020
     */
    public function airfilterstopped($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email,$data,$stop_id) {
        $arr = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'stop');

        $this->db->insert("pts_trn_activity_approval_log", $arr);

        $arr2 = array("activity_stop" => time());
        $this->db->where("id", $stop_id);
        $this->db->update("pts_trn_return_air_filter", $data);
        $this->db->where("id", $actlogtblid);
        if ($this->db->update("pts_trn_user_activity_log", $arr2)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * This function is used to start the Selected activity at home page
     * Created By :Bhupendra kumar
     * Date:28-01-2020
     */
    public function Senttostart($roomcode, $processid, $equipmentcode, $actid, $actname, $product_no, $batch_no, $roleid, $empid, $empname, $email, $status_id, $next_step, $remark, $remark2, $doc_no, $sampling_rod_id, $dies_no, $act_name2, $headerRecordid) {
        $arr = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => $equipmentcode, "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "versionid" => $headerRecordid, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);
        if ($sampling_rod_id != '' && $dies_no != '' && $act_name2 != "") {
            $arr['sampling_rod_id '] = $sampling_rod_id;
            $arr['dies_no'] = $dies_no;
            $arr['act_name'] = $act_name2;
            $arr['activity_name']=$actname;
        }

        $this->db->insert("pts_trn_user_activity_log", $arr);
        $actlogtblid = $this->db->insert_id();
        $arr3 = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');
        $this->db->insert("pts_trn_activity_approval_log", $arr3);

        $arr2 = array("workflowstatus" => $status_id, "workflownextstep" => $next_step);
        $this->db->where("id", $actlogtblid);
        if ($this->db->update("pts_trn_user_activity_log", $arr2)) {
            return $actlogtblid;
        } else {
            return "";
        }
    }

    /**
     * This function is used to start the Selected activity at home page
     * Created By :Bhupendra kumar
     * Date:28-01-2020
     */
    public function updatestart($roomcode, $processid, $equipmentcode, $actid, $actname, $product_no, $batch_no, $roleid, $empid, $empname, $email, $status_id, $next_step, $remark, $remark2, $doc_no, $sampling_rod_id, $dies_no, $act_name2, $update_id) {
        $arr = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => $equipmentcode, "updated_by" => $empid, "updated_on" => date("Y-m-d H:i:s"), "update_remark" => $remark);
        if ($sampling_rod_id != '' && $dies_no != '' && $act_name2 != "") {
            $arr['sampling_rod_id '] = $sampling_rod_id;
            $arr['dies_no'] = $dies_no;
            $arr['act_name'] = $act_name2;
            $arr['activity_name']=$actname;
        }
        $this->db->where("id", $update_id);
        $res = $this->db->update("pts_trn_user_activity_log", $arr);
        return $res;
    }

    public function submit_return_air_filter($isworkflow, $doc_no, $frequency, $product_no, $batch_no, $selectedfilters, $roomcode, $processid, $actid, $actname, $roleid, $empid, $empname, $email, $status_id, $next_step, $remark, $remark2) {
        if ($isworkflow == 0) {
            $arr0 = array("doc_no" => $doc_no, "product_code" => $product_no, "batch_no" => $batch_no, "frequency_id" => $frequency, "filter" => $selectedfilters, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "room_code" => $roomcode);

            if ($this->db->insert("pts_trn_return_air_filter", $arr0)) {
                $re_air_filter_tblid = $this->db->insert_id();
                return $re_air_filter_tblid;
            } else {
                return "";
            }
        } else {
            $arr0 = array("doc_no" => $doc_no, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "frequency_id" => $frequency, "filter" => $selectedfilters, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'yes');
            $this->db->insert("pts_trn_return_air_filter", $arr0);
            $re_air_filter_tblid = $this->db->insert_id();
            if ($re_air_filter_tblid > 0) {
                $arr = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);
                $this->db->insert("pts_trn_user_activity_log", $arr);
                $actlogtblid = $this->db->insert_id();

                $arr3 = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');
                if ($this->db->insert("pts_trn_activity_approval_log", $arr3)) {
                    return $actlogtblid;
                } else {
                    return "";
                }
            } else {
                return "";
            }
        }
    }

    /**
     * Takeover running activity
     * Created By :Bhupendra kumar
     * Date:28-01-2020
     */
    public function Senttotakeover($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email) {
        $arr = array("usr_activity_log_id" => $actlogtblid, "activity_status" => 'Takeover', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "remarks" => $remark, "activity_time" => time());
        if ($this->db->insert("pts_trn_activity_log", $arr)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Get Last Activity Product and Batch Number
     * Created By :Rahul Chauhan
     * Date:05-02-2020
     */
    public function GetLastProductAndBatchNumber($roomlogactivity) {
        $result = array();
        $this->db->select("*");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->where_in("roomlogactivity", $roomlogactivity);
        $this->db->order_by("id", 'DESC');
        $result = $this->db->get()->row_array();

        return $result;
    }

    /**
     * Get Last Activity Product and Batch Number equipment Wise
     * Created By :Rahul Chauhan
     * Date:05-02-2020
     */
    public function GetLastBactProductActivity($equipment_codess) {
        $result = array();
        $this->db->select("*");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->where_in("equip_code", $equipment_codess);
        $this->db->order_by("id", 'DESC');
        $result = $this->db->get()->result_array();

        return $result;
    }

    /**
     * Get Air filter last Activity based on room
     * Created By :Rahul Chauhan
     * Date:11-02-2020
     */
    public function GetLastFilterActivity($room_code, $table_name,$doc_no = null) {
        $result = array();
        $edit_id = $table_name . '.id as edit_id';
        $this->db->select($table_name . ".*,$edit_id,pts_trn_user_activity_log.*,pts_trn_user_activity_log.id as act_id");
        $this->db->from($table_name);
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=$table_name.doc_no");
        $this->db->where("$table_name.room_code", $room_code);
        $this->db->where("$table_name.status", "active");
        $this->db->where("$table_name.is_in_workflow", "yes");
        if(!empty($doc_no)){
            $this->db->where("$table_name.doc_no", $doc_no);
        }
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    public function GetLastFilterActivityForTT($docno, $table_name) {
        $result = array();
        $edit_id = $table_name . '.id as edit_id';
        $this->db->select($table_name . ".*,$edit_id,pts_trn_user_activity_log.*,pts_trn_user_activity_log.id as act_id");
        $this->db->from($table_name);
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=$table_name.doc_no");
        $this->db->where("$table_name.doc_no", $docno);
        $this->db->where("$table_name.status", "active");
        $this->db->where("$table_name.is_in_workflow", "yes");
        $result = $this->db->get()->result_array();
        //echo $this->db->last_query();exit;
        return $result;
    }
    public function getLastTTDocNo($room_code, $product_code, $punch_set) {
        $this->db->select("`pts_trn_tablet_tooling`.doc_no");
        $this->db->from("pts_trn_tablet_tooling");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_tablet_tooling.doc_no");
        $this->db->where("pts_trn_tablet_tooling.room_code", $room_code);
        $this->db->where("pts_trn_tablet_tooling.status", "active");
        $this->db->where("pts_trn_tablet_tooling.is_in_workflow", "yes");
        $this->db->where("pts_trn_tablet_tooling.product_code", $product_code);
        $this->db->where("pts_trn_tablet_tooling.punch_set_number", $punch_set);
        $this->db->order_by("pts_trn_tablet_tooling.id", 'DESC');
        $this->db->limit(1);
        $result = $this->db->get()->row_array();
        if(!empty($result)){
            return $result['doc_no'];
        }else{
            return '';
        }
    }

    /**
     * Get Header Data
     * Created By : Bhupendra kumar
     * Date: 20/04/2020
     */
    public function GetHeaderData($room_code, $table_name) {
        $result = array();
        $this->db->select("data_log_id,magnelic_id,temp_from,temp_to,rh_from,rh_to,from_pressure,to_pressure,globe_pressure_diff,positive_check");
        $this->db->from($table_name);
        $this->db->where("$table_name.room_code", $room_code);
        $this->db->where("$table_name.status", "active");
//        $this->db->where("$table_name.id", 1);
        $this->db->order_by("id", 'DESC');
        $this->db->limit(1);
        $result = $this->db->get()->result_array();
        //echo $this->db->last_query();exit;
        return $result;
    }

//Comman function to submit form details and set for approval if it is in workflow
    public function Com_fun_to_submit_log_details($isworkflow, $tblname, $data, $data2, $data3) {
        if ($isworkflow == 0) {
            if (!empty($data[0])) {
                if ($this->db->insert_batch($tblname, $data)) {
                    $re_air_filter_tblid = $this->db->insert_id();
                    return $re_air_filter_tblid;
                } else {
                    return "";
                }
            } else {
                if ($this->db->insert($tblname, $data)) {
                    $re_air_filter_tblid = $this->db->insert_id();
                    return $re_air_filter_tblid;
                } else {
                    return "";
                }
            }
        } else {
            if (!empty($data[0])) {
                foreach ($data as $key => $value) {
                    $data[$key]['is_in_workflow'] = 'yes';
                }
                $this->db->insert_batch($tblname, $data);
            } else {
                if($tblname!='pts_trn_swab_sample_record'){
                $data['is_in_workflow'] = 'yes';
                }else{
                   $data['is_in_workflow'] = 'no';
                }
                $this->db->insert($tblname, $data);
                
            }
            $re_air_filter_tblid = $this->db->insert_id();
            if ($re_air_filter_tblid > 0) {
                $this->db->insert("pts_trn_user_activity_log", $data2);
                $actlogtblid = $this->db->insert_id();
                $data3['usr_activity_log_id'] = $actlogtblid;
                if ($this->db->insert("pts_trn_activity_approval_log", $data3)) {
                    return $actlogtblid;
                } else {
                    return "";
                }
            } else {
                return "";
            }
        }
    }

    /**
     * Get matrial Record  from pts_trn_material_retreival_relocation table based on Material code and batch number
     * Created By : Bhupendra Kumar
     * Date:5May2020
     */
    public function GetMatRecord($matcode, $matbatchcode) {
        $result = array();
        $this->db->select("*");
        $this->db->from("pts_trn_material_retreival_relocation");
        $this->db->where("material_code", $matcode);
        $this->db->where("material_batch_no", $matbatchcode);
        $this->db->where("in_status", '0');
        $this->db->where("is_in_workflow", 'no');
        $this->db->where("material_condition", 'in');
        $result = $this->db->get()->result_array();
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
        //echo $this->db->last_query();exit;
    }

    /**
     * Get Last Record matrial from pts_trn_material_retreival_relocation table based on Material code
     * Created By : Bhupendra Kumar
     * Date:18feb2020
     */
    public function GetLastMatRecord($matcode) {
        $result = array();
        $this->db->select("*");
        $this->db->from("pts_trn_material_retreival_relocation");
        $this->db->where("material_code", $matcode);
        $this->db->order_by("id", 'DESC');
        $this->db->limit(1);
        $result = $this->db->get()->result_array();
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
        //echo $this->db->last_query();exit;
    }

    public function GetProductDetailbycode($matcode) {
        $result = array();
        $this->db->select("*");
        $this->db->from("mst_product");
        $this->db->where("product_code", $matcode);
        $this->db->order_by("id", 'DESC');
        $this->db->limit(1);
        $result = $this->db->get()->result_array();
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }

    /**
     * Get Process log Record for Report
     * Created By : Bhupendra kumar
     * Date:19-02-2020
     */
    public function GetProcessLogReport($room_code, $equip_code, $type, $post, $mst_act_id) {
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];

        $select = "Select pts_trn_user_activity_log.id,pts_trn_user_activity_log.doc_id,pts_trn_user_activity_log.roomlogactivity,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%Y-%m-%d') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%Y-%m-%d') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as activity_time,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,group_concat(pts_trn_log_file_details.file_name) as logAttachments,mst_product.product_name";
        $select .= ' FROM pts_trn_user_activity_log left join pts_trn_activity_approval_log as pts_trn_activity_approval_log on pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id';
        $select .= ' left join pts_mst_activity as pts_mst_activity on pts_trn_user_activity_log.activity_id=pts_mst_activity.id';
        $select .= " left join pts_trn_log_file_details as pts_trn_log_file_details on pts_trn_log_file_details.act_id=pts_trn_activity_approval_log.usr_activity_log_id AND pts_trn_log_file_details.status='active'";
        $select .= " left join mst_product as mst_product on mst_product.product_code=pts_trn_user_activity_log.product_code";
        $select .= " where pts_trn_user_activity_log.status ='active'";
        $select .= " AND pts_trn_user_activity_log.room_code= '" . $room_code . "'";
        $select .= " AND pts_trn_user_activity_log.roomlogactivity= '" . $mst_act_id . "'";
        $select .= " AND FIND_IN_SET('$equip_code',`pts_trn_user_activity_log`.`equip_code`)";
        if (!empty($start_date) && !empty($end_date)) {
            $select .= " AND from_unixtime(pts_trn_user_activity_log.activity_start, '%Y-%m-%d') >= '" . $start_date . "'";
            $select .= " AND from_unixtime(pts_trn_user_activity_log.activity_start, '%Y-%m-%d') <= '" . $end_date . "'";
        }
        $select .= " Group By pts_trn_activity_approval_log.id";
        $select .= " ORDER BY pts_trn_user_activity_log.id ASC";
        $result = $this->db->query($select)->result_array();
        //echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Get Swab Sample Record for Report
     * Created By : Bhupendra kumar
     * Date:21-02-2020
     */
    public function GetSwabSampleReport($post) {
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_swab_sample_record.*,pts_trn_user_activity_log.id,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%b-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as activity_time,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,mst_product.product_name");
        $this->db->from("pts_trn_swab_sample_record");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_swab_sample_record.doc_no", 'left');
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
        $this->db->join("mst_product", "mst_product.product_code=pts_trn_swab_sample_record.pre_product_code", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
//        $this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
        if (!empty($start_date && $end_date)) {
            $this->db->where('pts_trn_swab_sample_record.created_on>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            $this->db->where('pts_trn_swab_sample_record.created_on<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }

        $result = $this->db->get()->result_array();
        //echo $this->db->last_query();exit;
        return $result;
    }

    /**
      <<<<<<< HEAD
     * Function to enter detail of user who is going in the room  
     * Created By :Bhupendra kumar
     * Date:26-02-2020
     */
    public function roomin($roomcode, $roleid, $empid, $empname, $email, $session_id,$remark=null) {
        $arr = array("room_code" => $roomcode, "role_id" => $roleid, "emp_code" => $empid, "emp_name" => $empname, "emp_email" => $email, "created_by" => $empid, "room_in_time" => time(), "session_id" => $session_id,'roomin_remarks'=>json_encode(['remark'=>$remark]));
        if ($this->db->insert("pts_trn_emp_roomin", $arr)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Function to enter detail of user who is going in the room  
     * Created By :Bhupendra kumar
     * Date:26-02-2020
     */
    public function roominandupdate($roomcode, $roleid, $empid, $empname, $email, $session_id) {
        $arr = array("isactive" => 0, "room_out_time" => time());
        $this->db->where("role_id", $roleid);
        $this->db->where("emp_code", $empid);
        $this->db->where("emp_name", $empname);
        $this->db->where("emp_email", $email);
        $this->db->where("room_code", $roomcode);
        if ($this->db->update("pts_trn_emp_roomin", $arr)) {
            $arr2 = array("room_code" => $roomcode, "role_id" => $roleid, "emp_code" => $empid, "emp_name" => $empname, "emp_email" => $email, "created_by" => $empid, "room_in_time" => time(), "session_id" => $session_id);
            if ($this->db->insert("pts_trn_emp_roomin", $arr2)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    /**
     * Function to enter detail of user who is going in the room  
     * Created By :Bhupendra kumar
     * Date:26-02-2020
     */
    public function roomout($roleid, $empid, $empname, $email,$remark=null) {
        $arr = array("isactive" => 0, "room_out_time" => time(),"roomout_remarks"=> json_encode(['remark'=>$remark]));
        $this->db->where("role_id", $roleid);
        $this->db->where("emp_code", $empid);
        $this->db->where("emp_name", $empname);
        $this->db->where("emp_email", $email);
        $this->db->where("isactive", 1);
        if ($this->db->update("pts_trn_emp_roomin", $arr)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Get Room In User List 
     * Created By :Rahul Chauhan
     * Date:28-02-2020
     */
    public function GetRoomInUserDetail($system_id) {
        $result = array();
        $this->db->select("pts_trn_emp_roomin.emp_name,pts_trn_emp_roomin.room_code,date(pts_trn_emp_roomin.created_on) as RoominDate,time(pts_trn_emp_roomin.created_on) as
RoominTime,mst_role.role_description");
        $this->db->from("pts_trn_emp_roomin");
        $this->db->join("mst_role", "mst_role.id=pts_trn_emp_roomin.role_id");
        $this->db->join("pts_mst_sys_id", "pts_mst_sys_id.room_code=pts_trn_emp_roomin.room_code");
        $this->db->where("pts_trn_emp_roomin.isactive", '1');
        $this->db->where("pts_trn_emp_roomin.room_out_time", null);
         if($system_id!=''){
        $this->db->where("pts_mst_sys_id.device_id", $system_id);
        }
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Get Running Batch Details 
     * Created By :Bhupendra Kumar
     * Date:02-03-2020
     */
    public function GetrunningBatchData($system_id) {
        $result = array();
        $this->db->select("pts_trn_batchin.*,date(pts_trn_batchin.created_on) as
BatchDate,time(pts_trn_batchin.created_on) as
BatchTime");
        $this->db->from("pts_trn_batchin");
        $this->db->join("pts_mst_sys_id", "pts_mst_sys_id.room_code=pts_trn_batchin.room_code");
        $this->db->where("pts_trn_batchin.isactive", '1');
        $this->db->where("pts_trn_batchin.batch_out_time", null);
         if($system_id!=''){
        $this->db->where("pts_mst_sys_id.device_id", $system_id);
        }
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Function to start batch in a room
     * Created By :Bhupendra kumar
     * Date:29-02-2020
     */
    public function batchin($roomcode, $batchcod, $prodcode, $product_desc, $roleid, $empid, $empname, $email) {
        $arr = array("room_code" => $roomcode, "batch_no" => $batchcod, "product_code" => $prodcode, "product_desc" => $product_desc, "role_id" => $roleid, "emp_code" => $empid, "emp_name" => $empname, "emp_email" => $email, "created_by" => $empid, "batch_in_time" => time());
        if ($this->db->insert("pts_trn_batchin", $arr)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Function to enter detail of user who is going in the room  
     * Created By :Bhupendra kumar
     * Date:26-02-2020
     */
    public function batchout($roomcode) {
        $arr = array("isactive" => 0, "batch_out_time" => time());
        // $this->db->where("role_id", $roleid);
        // $this->db->where("emp_code", $empid);
        // $this->db->where("emp_name", $empname);
        // $this->db->where("emp_email", $email);
        $this->db->where("isactive", 1);
        $this->db->where("room_code", $roomcode);
        //$this->db->where("session_id",$session_id);
        if ($this->db->update("pts_trn_batchin", $arr)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /*     * Get Pre Filter Report Data
     * Created By : Devendra Singh
     * Edited By: ****
     * Created Date:29-02-2020
     * Edit Date:***
     */

    public function GetPreFilterLogReportLAF($post) {
        $room_code = $post['room_code'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_pre_filter_cleaning.*,pts_trn_user_activity_log.id,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%b-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as activity_time,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,mst_product.product_name,FROM_UNIXTIME(pts_trn_activity_approval_log.activity_time, '%H:%i:%s') AS activity_time1");
        $this->db->from("pts_trn_pre_filter_cleaning");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_pre_filter_cleaning.doc_no", 'left');
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
        $this->db->join("mst_product", "pts_trn_pre_filter_cleaning.product_code=mst_product.product_code", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
       // $this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
        if ((isset($start_date) && $start_date != "" && isset($end_date) && $end_date != "")) {
            $this->db->where('pts_trn_pre_filter_cleaning.created_on>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            $this->db->where('pts_trn_pre_filter_cleaning.created_on<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }
        if (!empty($room_code) && $room_code != 'undefined') {
            $this->db->where('pts_trn_pre_filter_cleaning.room_code', $room_code);
        }
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /*     * Get Vertical Sampler Report Data
     * Created By : Bhupendra kumar
     * Edited By: ****
     * Created Date:11-03-2020
     * Edit Date:***
     */

    public function GetVerticalSamplerReport($post) {
        $equipment_code = $post['equipment_code'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_user_activity_log.id,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%m-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%m-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,pts_trn_user_activity_log.dies_no");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
//        $this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
        $this->db->where("pts_trn_user_activity_log.is_qa_approve", 'yes');
        $this->db->where("pts_trn_user_activity_log.sampling_rod_id is NOT NULL", NULL, FALSE);
        $this->db->where("pts_trn_user_activity_log.dies_no is NOT NULL", NULL, FALSE);
        //$this->db->where("pts_trn_user_activity_log.room_code",$room_code);
        //$this->db->where("pts_mst_activity.activity_url",$type);
        $this->db->where_in("pts_trn_user_activity_log.equip_code", $equipment_code);
        if (!empty($start_date && $end_date)) {
            $this->db->where('from_unixtime(pts_trn_user_activity_log.activity_start, "%Y-%m-%d") >=', $start_date);
            $this->db->where('from_unixtime(pts_trn_user_activity_log.activity_start, "%Y-%m-%d") <=', $end_date);
        }
        $result = $this->db->get()->result_array();
        // echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Audit Trail Report Data
     * Created By : Bhupendra kumar
     * Created Date:03-03-2020
     */
    public function auditTrailReportList($post) {
        //echo '<pre>'; print_r($post); die;
        $user_name = (isset($post['user_name']) && $post['user_name'] != '') ? $post['user_name'] : '';
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $start_date = date("Y-m-d", strtotime($start_date));
        $end_date = date("Y-m-d", strtotime($end_date));
        $a_procedure = "CALL getAuditdata('" . $user_name . "','" . $start_date . "','" . $end_date . "','" . $post['page'] . "','" . $post['limit'] . "')";

        if (empty($a_procedure)) {
            return false;
        }
        $index = 0;
        $ResultSet = array();
        if (mysqli_multi_query($this->db->conn_id, $a_procedure)) {
            while (true) {
                if (false != $result = mysqli_store_result($this->db->conn_id)) {
                    $rowID = 0;
                    while ($row = $result->fetch_assoc()) {
                        $ResultSet[$index][$rowID] = $row;
                        $rowID++;
                    }
                }
                $index++;
                if (mysqli_more_results($this->db->conn_id)) {
                    mysqli_next_result($this->db->conn_id);
                } else {
                    break;
                }
            }
        }
        return $res = array('result1' => array_key_exists(0, $ResultSet) ? $ResultSet[0] : array(), 'total_count' => array_key_exists(1, $ResultSet) ? $ResultSet[1][0]['RecordCount'] : 0);
    }

    /**
     * Audit Trail Report Data Print
     * Created By : Amitesh Yadav
     * Created Date:04-08-2020
     */
    public function auditTrailReportListPrint($post) {
        $user_name = (isset($post['user_name']) && $post['user_name'] != '') ? $post['user_name'] : '';
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];

        $paraArray = array(
            'username' => $user_name,
            'startdate' => $start_date,
            'enddate' => $end_date,
            //'pageNo' => $post['page'],
            //'noOfRecords' => $post['limit']
        );

        $a_procedureCount = "CALL getAuditdataPrint(?,?,?)";
        $a_resultCount = $this->db->query($a_procedureCount, $paraArray);
        $result = $a_resultCount->result_array();
        return $res = array('result' => $result);
    }

    /**
     * Get Equipment Apparatus Log Report
     * Created By : Rahul Chauhan
     * Edited By: ****
     * Created Date:29-02-2020
     * Edit Date:***
     */
    public function GetEquipmentApparatusLogReport($post) {
        $apparatus_no = $post['apparatus_no'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_equip_appratus_log.*,pts_trn_user_activity_log.id,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%b-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as activity_time,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type");
        $this->db->from("pts_trn_equip_appratus_log");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_equip_appratus_log.doc_no", 'left');
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
//        $this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
        if (!empty($start_date && $end_date)) {
            $this->db->where('pts_trn_equip_appratus_log.created_on>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            $this->db->where('pts_trn_equip_appratus_log.created_on<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }
        if (!empty($apparatus_no)) {
            $this->db->where('pts_trn_equip_appratus_log.equip_appratus_no', $apparatus_no);
        }
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Get Equipment Apparatus Leak Test Report
     * Created By : Bhupendra kumar
     * Edited By: ****
     * Created Date:05-04-2020
     * Edit Date:***
     */
    public function GetEquipmentApparatusLeakTestReport($post) {
        $apparatus_no = $post['apparatus_no'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_equip_appratus_leaktest.*,pts_trn_user_activity_log.id,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%b-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as activity_time,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type");
        $this->db->from("pts_trn_equip_appratus_leaktest");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_equip_appratus_leaktest.doc_no", 'left');
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
//        $this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
        if (!empty($start_date && $end_date)) {
            $this->db->where('pts_trn_equip_appratus_leaktest.created_on>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            $this->db->where('pts_trn_equip_appratus_leaktest.created_on<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }
        if (!empty($apparatus_no)) {
            $this->db->where('pts_trn_equip_appratus_leaktest.equip_appratus_no', $apparatus_no);
        }
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Get Instrument Log Report
     * Created By : Rahul Chauhan
     * Created Date:11-03-2020
     */
    public function GetInstrumentReport($post) {
        $apparatus_no = $post['instrument_no'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_instrument_log_register.*,pts_trn_user_activity_log.id,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%b-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as activity_time,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,mst_product.product_name");
        $this->db->from("pts_trn_instrument_log_register");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_instrument_log_register.doc_no", 'left');
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
        $this->db->join("mst_product", "pts_trn_instrument_log_register.product_code=mst_product.product_code", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
//        $this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
        if (!empty($start_date && $end_date)) {
//        $this->db->where('pts_trn_equip_appratus_log.created_on BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d 23:59:59', strtotime($end_date)).'"');
            $this->db->where('pts_trn_instrument_log_register.created_on>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            $this->db->where('pts_trn_instrument_log_register.created_on<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }
        if (!empty($apparatus_no)) {
            $this->db->where('pts_trn_instrument_log_register.instrument_code', $apparatus_no);
        }
        $result = $this->db->get()->result_array();
//       echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Vaccume Cleaner Report
     * Created By : Rahul Chauhan
     * Created Date:12-03-2020
     */
    public function getVaccumeReport($post) {
        $vaccume_no = $post['vaccume_no'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
//        echo $start_date;exit;
        $this->db->select("pts_trn_vaccum_cleaner.*,pts_trn_user_activity_log.id,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%b-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as activity_time,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,FROM_UNIXTIME(pts_trn_activity_approval_log.activity_time, '%H:%i:%s') AS activity_time1");
        $this->db->from("pts_trn_vaccum_cleaner");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_vaccum_cleaner.doc_no", 'left');
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
        $this->db->where("pts_trn_user_activity_log.activity_id", '21');
        //$this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
        if (!empty($start_date && $end_date)) {
            $this->db->where('pts_trn_vaccum_cleaner.created_no>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            $this->db->where('pts_trn_vaccum_cleaner.created_no<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }
        if (!empty($vaccume_no)) {
            $this->db->where('pts_trn_vaccum_cleaner.vaccum_cleaner_code', $vaccume_no);
        }
        if (!empty($post['room_code'])) {
            $this->db->where('pts_trn_vaccum_cleaner.room_code', $post['room_code']);
        }
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Get LAF Pressure Diff Data
     * Created By : Rahul Chauhan
     * Edited By: ****
     * Created Date:12-03-2020
     * Edit Date:***
     */
    public function lafPressureDiffReport($post) {
        $booth = $post['booth_no'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_laf_pressure_diff.*,pts_trn_user_activity_log.id,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%b-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as activity_time,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,group_concat(pts_trn_log_file_details.file_name) as logAttachments");
        $this->db->from("pts_trn_laf_pressure_diff");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_laf_pressure_diff.doc_no", 'left');
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
        $this->db->join("pts_trn_log_file_details", "pts_trn_log_file_details.act_id=pts_trn_activity_approval_log.usr_activity_log_id AND pts_trn_log_file_details.status='active'", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
//        $this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
        if (!empty($start_date && $end_date)) {
            $this->db->where('pts_trn_laf_pressure_diff.created_on>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            $this->db->where('pts_trn_laf_pressure_diff.created_on<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }
        $this->db->where('pts_trn_laf_pressure_diff.booth_no', $booth);
        $this->db->group_by('pts_trn_activity_approval_log.id');
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Air Filter Cleaning Data
     * Created By : Rahul Chauhan
     * Edited By: ****
     * Created Date:18-03-2020
     * Edit Date:***
     */
    public function airFilterCleaningReportList($post) {
        $room_code = $post['room_code'];
        $frequency = $post['frequency'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_return_air_filter.*,pts_trn_user_activity_log.id,pts_trn_user_activity_log.doc_id,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%b-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as activity_time,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,FROM_UNIXTIME(pts_trn_activity_approval_log.activity_time, '%H:%i:%s') AS activity_time1");
        $this->db->from("pts_trn_return_air_filter");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_return_air_filter.doc_no", 'left');
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
        $this->db->where("pts_trn_return_air_filter.status", 'active');
        //$this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
        if ((isset($start_date) && $start_date != "" && isset($end_date) && $end_date != "")) {
            //        $this->db->where('pts_trn_equip_appratus_log.created_on BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d 23:59:59', strtotime($end_date)).'"');
            $this->db->where('pts_trn_return_air_filter.created_on>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            $this->db->where('pts_trn_return_air_filter.created_on<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }
        $this->db->where('pts_trn_return_air_filter.room_code', $room_code);
        $this->db->where('pts_trn_return_air_filter.frequency_id', $frequency);
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /*     * Get Balance Calibration Report Data
     * Created By : Rahul Chauhan
     * Created Date:23-03-2020
     */

    public function getCalibrationReportData($post) {
        $room_code = $post['room_code'];
        $balance_no = $post['balance_no'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_balance_calibration.*,pts_trn_user_activity_log.id,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%b-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as activity_time,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type");
        $this->db->from("pts_trn_balance_calibration");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_balance_calibration.doc_no", 'left');
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
//        $this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
        if ((isset($start_date) && $start_date != "" && isset($end_date) && $end_date != "")) {
            $this->db->where('pts_trn_balance_calibration.created_on>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            $this->db->where('pts_trn_balance_calibration.created_on<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }
        $this->db->where('pts_trn_balance_calibration.room_code', $room_code);
        $this->db->where('pts_trn_balance_calibration.balance_id', $balance_no);
		//$this->db->order_by("pts_trn_activity_approval_log.activity_time", 'DESC');
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Get Previous Product and Batch Number 
     * Created By : Bhupendra kumar
     * Date: 15-02-2020 
     */
    public function GetPreBactProduct() {
        $result = array();
        $this->db->select("*");
        $this->db->from("pts_trn_vaccum_cleaner");
        $this->db->join("mst_product", "pts_trn_vaccum_cleaner.product_code=mst_product.product_code", 'left');
        $this->db->order_by("pts_trn_vaccum_cleaner.id", 'DESC');
        $this->db->limit(1);
        $result = $this->db->get()->result_array();
        return $result;
    }

    /**
     * Get Previous Product and Batch Number 
     * Created By : Bhupendra kumar
     * Date: 15-02-2020 
     */
    public function GetPreBactProductofPrefilter($room_code) {
        $result = array();
        $this->db->select("*");
        $this->db->from("pts_trn_pre_filter_cleaning");
        $this->db->join("mst_product", "pts_trn_pre_filter_cleaning.product_code=mst_product.product_code", 'left');
        $this->db->order_by("pts_trn_pre_filter_cleaning.id", 'DESC');
        $this->db->where('selection_status', 'from');
        $this->db->where('is_to', '1');
        if ($room_code != '') {
            $this->db->where('room_code', $room_code);
        }
        $this->db->limit(1);
        $result = $this->db->get()->result_array();
        return $result;
    }

    /**
     * Get Previous Product and Batch Number 
     * Created By : Bhupendra kumar
     * Date: 15-02-2020 
     */
    public function GetPreBactProductofprocesslog() {
        $result = array();
        $this->db->select("*");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->join("mst_product", "pts_trn_user_activity_log.product_code=mst_product.product_code", 'left');
        $this->db->order_by("pts_trn_user_activity_log.id", 'DESC');
        $this->db->limit(1);
        $result = $this->db->get()->result_array();
        return $result;
    }

    /**
     * Get Vertical Sampler log for Report
     * Created By : Bhupendra kumar
     * Date:19-02-2020
     */
    public function verticalSamplingReport($sampling_no, $post) {
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_user_activity_log.id,pts_mst_activity.activity_url,pts_trn_user_activity_log.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%b-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,pts_trn_user_activity_log.dies_no,pts_trn_user_activity_log.sampling_rod_id,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as activity_time");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
//        $this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
//        $this->db->where("pts_trn_user_activity_log.is_qa_approve", 'yes');
        //$this->db->where("pts_mst_activity.activity_url",$type);
        $this->db->where_in("pts_trn_user_activity_log.sampling_rod_id", $sampling_no);
        if (!empty($start_date && $end_date)) {
            $this->db->where('from_unixtime(pts_trn_user_activity_log.activity_start, "%Y-%m-%d") >=', $start_date);
            $this->db->where('from_unixtime(pts_trn_user_activity_log.activity_start, "%Y-%m-%d") <=', $end_date);
        }
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /* Common Delete function */

    public function deleterecords($col, $val, $deleteby, $deletetime, $tblname) {
        $arr2 = array("status" => 'inactive', "modified_by" => $deleteby, "modified_on" => $deletetime);
        $this->db->where($col, $val);
        if ($this->db->update($tblname, $arr2)) {
            $response = $this->db->affected_rows();
        } else {
            $response = $this->db->affected_rows();
        }

        return $response;
    }

    /* End Common Delete Function */

    /* Common Delete function */

    public function deleterecords2($col, $val, $deleteby, $deletetime, $tblname) {
        $arr2 = array("isactive" => 0, "modified_by" => $deleteby, "modified_on" => $deletetime);
        $this->db->where($col, $val);
        if ($this->db->update($tblname, $arr2)) {
            $response = $this->db->affected_rows();
        } else {
            $response = $this->db->affected_rows();
        }

        return $response;
    }

    /* End Common Delete Function */

    /* Get Environmental Report Data
     * Created By : Bhupendra 
     * Edited By: 
     * Created Date:23-03-2020
     * Edit Date:***
     */

    public function envPressureReport($post) {
        $room = $post['room_no'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_env_cond_diff.*,pts_trn_user_activity_log.id,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%b-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as activity_time,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,group_concat(pts_trn_log_file_details.file_name) as logAttachments,mst_area.block_code");
        $this->db->from("pts_trn_env_cond_diff");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_env_cond_diff.doc_no", 'left');
        $this->db->join("mst_room","mst_room.room_code=pts_trn_env_cond_diff.room_code","left");
        $this->db->join("mst_area","mst_area.area_code=mst_room.area_code","left");
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
        $this->db->join("pts_trn_log_file_details", "pts_trn_log_file_details.act_id=pts_trn_activity_approval_log.usr_activity_log_id AND pts_trn_log_file_details.status='active'", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
       // $this->db->where("pts_trn_user_activity_log.workflownextstep", -1);

        if (!empty($start_date && $end_date)) {
            $this->db->where('pts_trn_env_cond_diff.created_on>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            $this->db->where('pts_trn_env_cond_diff.created_on<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }
        if (!empty($room) && $room != 'undefined') {
            $this->db->where('pts_trn_env_cond_diff.room_code', $room);
        }
        $this->db->group_by('pts_trn_activity_approval_log.id');
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Get Material Relocation Report Data
     * Created By : Bhupendra Kumar
     * Edited By: ****
     * Created Date:23-03-2020
     * Edit Date:***
     */
    public function materialRelocationReport($post) {
        $room = $post['room_no'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("`pts_trn_material_retreival_relocation`.`id` AS `mat_id`, pts_trn_material_retreival_relocation.doc_no, DATE_FORMAT(`pts_trn_material_retreival_relocation`.`created_on`,'%d-%b-%Y %H:%i:%s') AS created_on, `pts_trn_material_retreival_relocation`.`material_code`, `pts_trn_material_retreival_relocation`.`material_condition`, `pts_trn_material_retreival_relocation`.`material_batch_no`, `pts_trn_material_retreival_relocation`.`material_type`,FROM_UNIXTIME(pts_trn_user_activity_log.activity_start, '%d-%b-%Y') AS start_date_short, FROM_UNIXTIME(pts_trn_user_activity_log.activity_start, '%H:%i:%s') AS start_time, FROM_UNIXTIME(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y %H:%i:%s') AS stop_date_time, `pts_trn_activity_approval_log`.`approval_status`,`pts_trn_activity_approval_log`.`user_name`,`mst_product`.`product_name`");
        $this->db->from("pts_trn_material_retreival_relocation");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_material_retreival_relocation.doc_no", 'left');
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
        $this->db->join("mst_product", "mst_product.product_code=pts_trn_material_retreival_relocation.material_code", 'left');
        $this->db->where("pts_trn_material_retreival_relocation.status", 'active');
        if (!empty($start_date) && !empty($end_date)) {
            $this->db->where("DATE_FORMAT(`pts_trn_material_retreival_relocation`.`created_on`,'%Y-%m-%d')>=", date('Y-m-d', strtotime($start_date)));
            $this->db->where("DATE_FORMAT(`pts_trn_material_retreival_relocation`.`created_on`,'%Y-%m-%d')<=", date('Y-m-d', strtotime($end_date)));
        }

        if (!empty($room) && $room != 'undefined') {
            $this->db->where('pts_trn_material_retreival_relocation.room_code', $room);
        }
        $this->db->order_by("pts_trn_material_retreival_relocation.id", "ASC");
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Get Tablet Tooling log for Report
     * Created By : Rahul Chauhan
     * Date:25-03-2020
     */
    public function getToolingReportData($post) {
        //print_r($post);exit;
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("from_unixtime(pts_trn_tablet_tooling.created_on, '%d-%m-%Y') as created_date,pts_trn_tablet_tooling.id,date(pts_trn_tablet_tooling.created_on) as start_date,pts_trn_tablet_tooling.created_on,from_unixtime(pts_trn_tablet_tooling.created_on, '%H:%i:%s') as start_time, pts_trn_tablet_tooling.product_code,pts_trn_tablet_tooling.doc_no,pts_trn_tablet_tooling.room_code,pts_trn_tablet_tooling.act_type,pts_trn_tablet_tooling.U,pts_trn_tablet_tooling.L,pts_trn_tablet_tooling.D,pts_trn_tablet_tooling.tablet_tooling_from,pts_trn_tablet_tooling.tablet_tooling_to,pts_trn_tablet_tooling.damaged,pts_trn_tablet_tooling.b_no,pts_trn_tablet_tooling.tab_qty,pts_trn_tablet_tooling.cum_qty,pts_trn_tablet_tooling.cum_qty_punch,
    pts_trn_tablet_tooling.done_by_user_name,pts_trn_tablet_tooling.done_by_remark,pts_trn_activity_approval_log.approval_status as apprl_status,pts_trn_activity_approval_log.user_name as checked_by,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as checked_date,pts_trn_activity_approval_log.workflow_type,
pts_trn_user_activity_log.id as usr_act_log_id,pts_trn_user_activity_log.user_name as usr_act_log_user_name,pts_trn_activity_approval_log.activity_remarks as usr_act_log_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%b-%Y') as usr_act_log_start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as
usr_act_log_start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y') as usr_act_log_stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as
usr_act_log_stop_time,pts_trn_user_activity_log.is_qa_approve as usr_act_log_is_qa_appl");
        $this->db->from("pts_trn_tablet_tooling");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_tablet_tooling.doc_no", 'left');
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->where('pts_trn_tablet_tooling.room_code', $post['room_code']);
        $this->db->where('pts_trn_tablet_tooling.product_code', $post['product_no']);
        $this->db->where('pts_trn_tablet_tooling.punch_set_number', $post['punch_set']);

        if ($start_date != "" && $end_date != "") {
            $this->db->where('pts_trn_tablet_tooling.created_on>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            $this->db->where('pts_trn_tablet_tooling.created_on<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }
//        $this->db->order_by('pts_trn_tablet_tooling.id','DESC');
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    //Comman function to submit Environmental form details and set for approval if it is in workflow
    public function Com_fun_to_submit_env_log_details($isworkflow, $tblname, $data, $data2, $data3, $id) {
        if ($isworkflow == 0) {
            if (!empty($data[0])) {
                if ($this->db->update($tblname, $data, array("id" => $id))) {
                    $re_air_filter_tblid = $this->db->insert_id();
                    return $id;
                } else {
                    return "";
                }
            } else {
                if ($this->db->update($tblname, $data, array("id" => $id))) {
                    $re_air_filter_tblid = $this->db->insert_id();
                    return $id;
                } else {
                    return "";
                }
            }
        } else {
            if (!empty($data[0])) {
                foreach ($data as $key => $value) {
                    $data[$key]['is_in_workflow'] = 'yes';
                }
                $this->db->update($tblname, $data, array("id" => $id));
            } else {
                $data['is_in_workflow'] = 'yes';
                $this->db->update($tblname, $data, array("id" => $id));
            }
            $re_air_filter_tblid = $id;
            if ($re_air_filter_tblid > 0) {
                $this->db->insert("pts_trn_user_activity_log", $data2);
                $actlogtblid = $this->db->insert_id();
                $data3['usr_activity_log_id'] = $actlogtblid;
                if ($this->db->insert("pts_trn_activity_approval_log", $data3)) {
                    return $actlogtblid;
                } else {
                    return "";
                }
            } else {
                return "";
            }
        }
    }

    //Comman function to Update form details before set for approval if it is in workflow
    public function Com_fun_to_update_env_log_details($tblname, $data, $where) {
        $this->db->update($tblname, $data, $where);
        return true;
    }

    // get the data fom mysql table
    public function getMultipleRows2($db, $table, $param, $order = NULL, $page = null) {
        $CI = &get_instance();
        $this->db = $CI->load->database($db, TRUE);
        $this->db->from($table);
        if (!empty($param)) {
            $this->db->where($param);
        }
        $this->db->order_by('log', 'ASC');
        if (!empty($page)) {
            $offset = ($page - 1) * (self::records_per_page);
            $this->db->limit(self::records_per_page, $offset);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        } else {
            return array();
        }
    }

    public function getUserModuleData($param) {
        if ($param['module_type'] == 'log') {
            $this->db->select("pts_mst_user_mgmt.id as log_id,pts_mst_user_mgmt.room_code,pts_mst_user_mgmt.type_id,pts_mst_user_mgmt.module_id,pts_mst_user_mgmt.is_create,pts_mst_user_mgmt.is_edit,pts_mst_user_mgmt.is_view,pts_mst_activity.*,pts_mst_user_mgmt.status as mstatus");
            $this->db->join("pts_mst_activity", 'pts_mst_activity.id = pts_mst_user_mgmt.module_id');
        }
        if ($param['module_type'] == 'report') {
            $this->db->select("pts_mst_user_mgmt.is_create,pts_mst_user_mgmt.is_edit,pts_mst_user_mgmt.is_view,pts_mst_report.*");
            $this->db->join("pts_mst_report", 'pts_mst_report.id = pts_mst_user_mgmt.module_id');
        }
        if ($param['module_type'] == 'master') {
            $this->db->select("pts_mst_user_mgmt.is_create,pts_mst_user_mgmt.is_edit,pts_mst_user_mgmt.is_view,pts_mst_master.*");
            $this->db->join("pts_mst_master", 'pts_mst_master.id = pts_mst_user_mgmt.module_id');
        }
        $this->db->from("pts_mst_user_mgmt");
        $this->db->where("pts_mst_user_mgmt.module_type", $param['module_type']);
        $this->db->where("pts_mst_user_mgmt.user_id", $param['user_id']);
        $this->db->order_by("pts_mst_user_mgmt.module_id", "ASC");
//        $this->db->where("pts_mst_user_mgmt.status", "active");

        $res = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $res;
    }

    /**
     * check duplicate logger and guage
     * @param type $tblname
     * @param type $logger
     * @param type $type
     * @return string
     */
    public function checkLoggerGuage($tblname, $logger, $type, $recordId = '') {
        if ($type == 'logger') {
            $this->db->where('find_in_set("' . $logger . '", logger_id) <> 0');
        } else {
            $this->db->where('find_in_set("' . $logger . '", gauge_id) <> 0');
        }

        if ($recordId > 0) {
            $this->db->where('id !=', $recordId);
        }
        $query = $this->db->get($tblname);
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return "";
        }
    }

    public function checkUserPrivelege($param) {
        $this->db->select("*");
        $this->db->from("pts_mst_user_mgmt");
        $this->db->where("pts_mst_user_mgmt.module_type", $param['module_type']);
        $this->db->where("pts_mst_user_mgmt.user_id", $param['user_id']);
        $this->db->where("pts_mst_user_mgmt.module_id", $param['module_id']);
        $this->db->where("pts_mst_user_mgmt.status", "active");
        if ($param['room_code'] != "") {
            $this->db->where("pts_mst_user_mgmt.room_code", $param['room_code']);
        }
        $res = $this->db->get()->row_array();
        //echo $this->db->last_query();exit;
        return $res;
    }

    public function checkUserPrivelegeformaster($param) {
        $this->db->select("*");
        $this->db->from("pts_mst_user_mgmt");
        $this->db->where("pts_mst_user_mgmt.module_type", $param['module_type']);
        $this->db->where("pts_mst_user_mgmt.user_id", $param['user_id']);
        $where = '(pts_mst_user_mgmt.is_create = "1" or pts_mst_user_mgmt.is_edit = "1" or pts_mst_user_mgmt.is_view = "1")';
        $this->db->where($where);
        $this->db->where("pts_mst_user_mgmt.status", "active");
        $res = $this->db->get()->row_array();
        //echo $this->db->last_query();exit;
        return $res;
    }

    public function getEmployeRoleList() {
        $this->db->select("mst_employee.id,mst_employee.emp_name,mst_employee.emp_code,mst_employee.role_id,mst_role.role_description");
        $this->db->from("mst_employee");
        $this->db->join("mst_role", 'mst_role.id = mst_employee.role_id');
        $this->db->where("mst_employee.is_active", 1);
        $this->db->where("mst_employee.is_blocked", 0);
        $res = $this->db->get()->result_array();
        return $res;
    }

    /**
     * 
     * @param type $param
     * @return type
     */
    public function previousRoomBatchAndProduct($param) {
        $this->db->select("*");
        $this->db->from("pts_trn_batchin");
        $this->db->where("room_code", $param['room_code']);
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1);
        $res = $this->db->get()->result_array();
        return $res;
    }

    /**
     * checkSameRoomWithSameEquip for line log master
     * @param type $param
     * @return type
     */
    public function checkSameRoomWithSameEquip($block_code, $area_code, $room_code, $equip_code, $record_id = '') {
        $this->db->select("*");
        $this->db->from("pts_mst_line_log");
        $this->db->where("block_code", $block_code);
        $this->db->where("area_code", $area_code);
        $this->db->where("room_code", $room_code);
        $this->db->where("status", "active");
        if (!empty($record_id)) {
            $this->db->where("id!=", $record_id);
        }
        $this->db->where('find_in_set("' . $equip_code . '", equipment_code) <> 0');
        $res = $this->db->get()->result_array();
        return $res;
    }

    //Comman function to Update form details before set for approval if it is in workflow
    public function Com_fun_to_update_log_details($tblname, $data, $where) {
        $this->db->update($tblname, $data, $where);
        return true;
    }

    //Comman function to Update form details before set for approval if it is in workflow
    public function Com_fun_to_update_log_details_forBatch($tblname, $data, $where) {
        $this->db->update_batch($tblname, $data, $where);
        return true;
    }

    /**
     * @desc Common function to audit trail history 
     * @param array $postParam
     * @return int
     * @author khushboo
     * @date 01-05-2020
     */
    public function addAuditTrailHistry($postParam) {
        $insert = $this->db->insert('pts_elog_history', $postParam) or die(" error" . mysqli_error($this->db));
        if (!$insert) {
            return FALSE;
        } else {
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }

    /**
     * @desc Common function to audit trail history 
     * @param array $postParam
     * @return int
     * @author khushboo
     * @date 01-05-2020
     */
    public function getLastAuditUpdatedCount($id, $table) {
        $this->db->select_max('update_count');
        $this->db->from('pts_elog_history');
        if (!empty($id)) {
            $this->db->where("primary_id", $id);
            $this->db->where("table_name", $table);
            $this->db->where("command_type", 'update');
        }
        $this->db->order_by('id', 'DESC');

        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        } else {
            return array();
        }
    }

    public function getRecordtoCheckDiff($id, $table) {
        if (!empty($id)) {
            $this->db->where("id", $id);
        }
        $query = $this->db->get($table);
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        } else {
            return array();
        }
    }

    /**
     * common function to add audit history data
     * Created By : Khushboo
     * Date: 04-05-2020
     */
    public function commonFunctionToAddAuditTrail($params) {
        date_default_timezone_set('Asia/Kolkata');
        if (!empty($params)) {
            if ($params['command_type'] == 'insert') {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = !empty($params['uniquefiled1']) ? $params['uniquefiled1'] : 'N/A';
                $uniquefiled2 = !empty($params['uniquefiled2']) ? $params['uniquefiled2'] : 'N/A';
                $uniquefiled3 = !empty($params['uniquefiled3']) ? $params['uniquefiled3'] : 'N/A';
                $activity_name = !empty($params['activity_name']) ? $params['activity_name'] : 'N/A';
                $type = !empty($params['type']) ? $params['type'] : 'N/A';
                $table_name = !empty($params['table_name']) ? $params['table_name'] : 'N/A';
                $primary_id = !empty($params['primary_id']) ? $params['primary_id'] : 'N/A';
                $post_params = !empty($params['post_params']) ? $params['post_params'] : 'N/A';
                $record_type = !empty($params['record_type']) ? $params['record_type'] : 'master';
                $action_taken = "Record created by " . $this->session->userdata('empname');
                $auditArray = ['created_on' => date('Y-m-d H:i:s'), 'created_by' => $this->session->userdata('user_id'), 'created_by_user' => $this->session->userdata('empname'), 'action_text' => $action_taken, 'activity_name' => $activity_name, 'type' => $type, 'table_unique_field1' => $uniquefiled1, 'table_unique_field2' => $uniquefiled2, 'table_field3' => $uniquefiled3, 'command_type' => 'insert', 'record_type' => $record_type, 'update_count' => 1, 'table_name' => $table_name, 'primary_id' => $primary_id, 'table_extra_field' => json_encode($post_params)];
                $this->addAuditTrailHistry($auditArray);
                /* End -  Insert elog audit histry */
            } else if ($params['command_type'] == 'update') {
                $uniquefiled1 = !empty($params['uniquefiled1']) ? $params['uniquefiled1'] : '';
                $uniquefiled2 = !empty($params['uniquefiled2']) ? $params['uniquefiled2'] : '';
                $uniquefiled3 = !empty($params['uniquefiled3']) ? $params['uniquefiled3'] : '';
                $activity_name = !empty($params['activity_name']) ? $params['activity_name'] : '';
                $type = !empty($params['type']) ? $params['type'] : '';
                $table_name = !empty($params['table_name']) ? $params['table_name'] : '';
                $primary_id = !empty($params['primary_id']) ? $params['primary_id'] : '';
                $post_params = !empty($params['post_params']) ? $params['post_params'] : '';
                $master_previous_data = !empty($params['master_previous_data']) ? $params['master_previous_data'] : null;
                $record_type = !empty($params['record_type']) ? $params['record_type'] : 'master';
                $count = 1;
                $updated_count = $this->getLastAuditUpdatedCount($primary_id, $table_name);
                if (!empty($updated_count) && !empty($updated_count[0]['update_count'])) {
                    $count = $updated_count[0]['update_count'] + 1;
                }
                $auditArray = array();
                $action_taken = "Record updated by " . $this->session->userdata('empname');
                $auditArray = ['created_on' => date('Y-m-d H:i:s'), 'created_by' => $this->session->userdata('user_id'), 'created_by_user' => $this->session->userdata('empname'), 'action_text' => $action_taken, 'activity_name' => $activity_name, 'type' => $type, 'table_unique_field1' => $uniquefiled1, 'table_unique_field2' => $uniquefiled2, 'table_field3' => $uniquefiled3, 'command_type' => 'update', 'record_type' => $record_type, 'update_count' => $count, 'primary_id' => $primary_id, 'table_name' => $table_name, 'table_extra_field' => json_encode($post_params),'master_previous_data'=>json_encode($master_previous_data)];
                $this->addAuditTrailHistry($auditArray);
            } else if ($params['command_type'] == 'delete') {
                $table_name = !empty($params['table_name']) ? $params['table_name'] : '';
                $primary_id = !empty($params['primary_id']) ? $params['primary_id'] : '';
                $activity_name = !empty($params['activity_name']) ? $params['activity_name'] : '';
                $type = !empty($params['type']) ? $params['type'] : '';
                $post_params = !empty($params['post_params']) ? $params['post_params'] : '';
                $record_type = !empty($params['record_type']) ? $params['record_type'] : 'master';
                $auditArray = array();
                $action_taken = "Record deleted by " . $this->session->userdata('empname');
                $auditArray = array('created_on' => date('Y-m-d H:i:s'), 'created_by' => $this->session->userdata('user_id'), 'created_by_user' => $this->session->userdata('empname'), 'action_text' => $action_taken, 'activity_name' => $activity_name, 'type' => $type, 'command_type' => 'delete', 'record_type' => $record_type, 'update_count' => 1, 'table_unique_field1' => "", 'table_unique_field2' => "", 'table_field3' => "", 'primary_id' => $primary_id, 'table_name' => $table_name, 'table_extra_field' => json_encode($post_params));
                $this->addAuditTrailHistry($auditArray);
            }
        }
    }

    public function getActivityNameType($table_name) {
        $res = array('activity_name' => '', 'type' => '');
        if ($table_name == 'pts_mst_sys_id') {
            $res['activity_name'] = 'Room configuration';
            $res['type'] = 'manage_room';
        } else if ($table_name == 'pts_trn_room_log_activity') {
            $res['activity_name'] = 'Room Log Mapping';
            $res['type'] = 'manage_room_activity';
        } else if ($table_name == 'pts_mst_balance_calibration') {
            $res['activity_name'] = 'Balance master';
            $res['type'] = 'balance_master';
        } else if ($table_name == 'pts_mst_balance_frequency_data') {
            $res['activity_name'] = 'Balance Calibration Frequency';
            $res['type'] = 'balance_frequency_master';
        } else if ($table_name == 'pts_mst_tablet_tooling_log') {
            $res['activity_name'] = 'Tablet Tooling Master';
            $res['type'] = 'tablet_tooling_master';
        } else if ($table_name == 'pts_mst_line_log') {
            $res['activity_name'] = 'Line Master';
            $res['type'] = 'line_log_master';
        } else if ($table_name == 'pts_mst_id_standard_weight') {
            $res['activity_name'] = 'Weight Standard ID Mapping';
            $res['type'] = 'manage_id_standard_weight';
        } else if ($table_name == 'pts_mst_role_workassign_to_mult_roles') {
            $res['activity_name'] = 'Work Assign Master ';
            $res['type'] = 'work_assign_asper_rolebase';
        } else if ($table_name == 'pts_mst_instrument') {
            $res['activity_name'] = 'Instrument Master ';
            $res['type'] = 'work_assign_asper_rolebase';
        } else if ($table_name == 'pts_mst_stages') {
            $res['activity_name'] = 'Log Configuration Master ';
            $res['type'] = 'stage_master';
        } else if ($table_name == 'pts_mst_report_header') {
            $res['activity_name'] = 'Report Header/footer Master';
            $res['type'] = 'report_header';
        } else if ($table_name == 'mst_block') {
            $res['activity_name'] = 'Block Master';
            $res['type'] = 'manage_block';
        } else if ($table_name == 'mst_department') {
            $res['activity_name'] = 'Sub Block Master';
            $res['type'] = 'sub_block';
        } else if ($table_name == 'mst_area') {
            $res['activity_name'] = 'Area Master';
            $res['type'] = 'manage_area';
        } else if ($table_name == 'mst_drainpoint') {
            $res['activity_name'] = 'Create Drain Point';
            $res['type'] = 'manage_drain_point';
        } else if ($table_name == 'mst_product') {
            $res['activity_name'] = 'Product Master';
            $res['type'] = 'manage_product';
        } else if ($table_name == 'mst_employee') {
            $res['activity_name'] = 'User Configuration';
            $res['type'] = 'manage_employee';
        } else if ($table_name == 'mst_room') {
            $res['activity_name'] = 'Room master';
            $res['type'] = 'create_room';
        } else if ($table_name == 'mst_plant') {
            $res['activity_name'] = 'Plant Detail';
            $res['type'] = 'create_plant';
        } else if ($table_name == 'mst_equipment') {
            $res['activity_name'] = 'Equipment Master';
            $res['type'] = 'create_equipment';
        } else if ($table_name == 'pts_trn_workflowsteps') {
            $res['activity_name'] = 'Workflow';
            $res['type'] = 'workflow';
        } else if ($table_name == 'pts_mst_server_detail') {
            $res['activity_name'] = 'Server Configuration Master ';
            $res['type'] = 'manage_server';
        }
        return $res;
    }

    /**
     * New Audit trail report page
     */
    public function newAuditTrailData($post, $count = false) {
        $this->db->select("*");
        $this->db->from("pts_elog_history");
        if ((isset($post['limit']) && isset($post['page']) && $post['limit'] > 0 && $post['page'] > 0) && !$count) {
            $offset = ( $post['page'] - 1 ) * $post['limit'];
            $this->db->limit($post['limit'], $offset);
        }

        if ($post['start_date'] != '' && $post['end_date'] != '') {
            $this->db->where('date(created_on) >=', $post['start_date']);
            $this->db->where('date(created_on) <=', $post['end_date']);
        }
        if ($post['user_id'] != '') {
            $this->db->where('created_by', $post['user_id']);
        }
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        if ($count) {
            $totalcount = $query->num_rows();
            return $totalcount;
        } else {
            return $query->result_array();
        }
    }

    /**
     * common function to add audit history For transactional log
     * Created By : Khushboo
     * Date: 06-05-2020
     */
    public function commonFunctionToAddAuditTransaction($params) {
        date_default_timezone_set('Asia/Kolkata');
        if (!empty($params)) {
            if ($params['command_type'] == 'insert') {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = !empty($params['uniquefiled1']) ? $params['uniquefiled1'] : '';
                $uniquefiled2 = !empty($params['uniquefiled2']) ? $params['uniquefiled2'] : '';
                $uniquefiled3 = !empty($params['uniquefiled3']) ? $params['uniquefiled3'] : '';
                $activity_name = !empty($params['activity_name']) ? $params['activity_name'] : '';
                $type = !empty($params['type']) ? $params['type'] : '';
                $table_name = !empty($params['table_name']) ? $params['table_name'] : '';
                $primary_id = !empty($params['primary_id']) ? $params['primary_id'] : '';
                $post_params = !empty($params['post_params']) ? $params['post_params'] : '';
                $record_type = !empty($params['record_type']) ? $params['record_type'] : 'log';
                $created_by = !empty($params['created_by']) ? $params['created_by'] : '';
                $created_by_name = !empty($params['created_by_name']) ? $params['created_by_name'] : '';
                $log_user_activity_id = !empty($params['log_user_activity_id']) ? $params['log_user_activity_id'] : 'log';
                $auditArray = array();
                $action_taken = "Record created by " . $created_by_name;
                $auditArray = array('created_on' => date('Y-m-d H:i:s'), 'created_by' => $created_by, 'created_by_user' => $created_by_name, 'action_text' => $action_taken, 'activity_name' => $activity_name, 'type' => $type, 'table_unique_field1' => $uniquefiled1, 'table_unique_field2' => $uniquefiled2, 'table_field3' => $uniquefiled3, 'command_type' => 'insert', 'record_type' => $record_type, 'update_count' => 1, 'table_name' => $table_name, 'primary_id' => $primary_id, 'log_user_activity_id' => $log_user_activity_id, 'table_extra_field' => json_encode($post_params));
                return $this->addAuditTrailHistry($auditArray);
                /* End -  Insert elog audit histry */
            } else if ($params['command_type'] == 'update') {
                $uniquefiled1 = !empty($params['uniquefiled1']) ? $params['uniquefiled1'] : '';
                $uniquefiled2 = !empty($params['uniquefiled2']) ? $params['uniquefiled2'] : '';
                $uniquefiled3 = !empty($params['uniquefiled3']) ? $params['uniquefiled3'] : '';
                $activity_name = !empty($params['activity_name']) ? $params['activity_name'] : '';
                $type = !empty($params['type']) ? $params['type'] : '';
                $table_name = !empty($params['table_name']) ? $params['table_name'] : '';
                $primary_id = !empty($params['primary_id']) ? $params['primary_id'] : '';
                $post_params = !empty($params['post_params']) ? $params['post_params'] : '';
                $record_type = !empty($params['record_type']) ? $params['record_type'] : 'log';
                $created_by = !empty($params['created_by']) ? $params['created_by'] : '';
                $created_by_name = !empty($params['created_by_name']) ? $params['created_by_name'] : '';
                $log_user_activity_id = !empty($params['log_user_activity_id']) ? $params['log_user_activity_id'] : 'log';
                $count = 1;
                $updated_count = $this->getLastAuditUpdatedCount($primary_id, $table_name);
                if (!empty($updated_count) && !empty($updated_count[0]['update_count'])) {
                    $count = $updated_count[0]['update_count'] + 1;
                }
                $auditArray = array();
                $action_taken = "Record updated by " . $created_by_name;
                $auditArray = array('created_on' => date('Y-m-d H:i:s'), 'created_by' => $created_by, 'created_by_user' => $created_by_name, 'action_text' => $action_taken, 'activity_name' => $activity_name, 'type' => $type, 'table_unique_field1' => $uniquefiled1, 'table_unique_field2' => $uniquefiled2, 'table_field3' => $uniquefiled3, 'command_type' => 'update', 'record_type' => $record_type, 'update_count' => $count, 'primary_id' => $primary_id, 'log_user_activity_id' => $log_user_activity_id, 'table_name' => $table_name, 'table_extra_field' => json_encode($post_params));
                $this->addAuditTrailHistry($auditArray);
            }
        }
    }

    public function GetLastApproval($param) {
        $this->db->select("pts_trn_activity_approval_log.*");
        $this->db->from("pts_trn_activity_approval_log");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.id=pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->where("pts_trn_user_activity_log.id", $param['id']);
        $this->db->where("pts_trn_user_activity_log.roomlogactivity", $param['roomlogactivity']);
        $this->db->where("pts_trn_user_activity_log.activity_id", $param['activity_id']);
        $this->db->where("pts_trn_user_activity_log.room_code", $param['room_code']);
        $this->db->order_by("pts_trn_activity_approval_log.id", "DESC");
        $res = $this->db->get()->row_array();
        return $res;
    }

    public function GetAttachmentList($param) {
        $this->db->select("pts_trn_log_file_details.*,mst_role.role_description");
        $this->db->from("pts_trn_log_file_details");
        $this->db->join("mst_role", "mst_role.id=pts_trn_log_file_details.role_id", 'left');
        $this->db->where("pts_trn_log_file_details.act_id", $param['id']);
        $this->db->where("pts_trn_log_file_details.process_id", $param['roomlogactivity']);
        $this->db->where("pts_trn_log_file_details.room_code", $param['room_code']);
        $this->db->where("pts_trn_log_file_details.activity_id", $param['activity_id']);
        $this->db->where("pts_trn_log_file_details.status", "active");
        $res = $this->db->get()->result_array();
        return $res;
//        echo $this->db->last_query();exit;
    }

    /**
     * Get Active/Inactive User List
     * Created By :Rahul Chauhan
     * Date:08-06-2020
     */
    public function getUserList($param) {
        $this->db->select("mst_employee.*,mst_role.role_description");
        $this->db->from("mst_employee");
        $this->db->join("mst_role", "mst_role.id=mst_employee.role_id", 'left');
        if ($param['block_code'] != "") {
            $this->db->where("FIND_IN_SET( '".$param['block_code']."',`mst_employee`.`block_code`)");
        }
        $this->db->where("mst_employee.is_active", $param['status']);
//        if(!empty($param['from_date'])){
//            $this->db->where('mst_employee.created_on >=', date('Y-m-d 00:00:00', strtotime($param['from_date'])));
//        }
        if($param['status']==0){
            if(!empty($param['to_date'])){
            $this->db->where('mst_employee.modified_on <=', date('Y-m-d 23:59:59', strtotime($param['to_date'])));
        }
        }else{
          if(!empty($param['to_date'])){
            $this->db->where('mst_employee.created_on <=', date('Y-m-d 23:59:59', strtotime($param['to_date'])));
        }  
        }
        
        $res = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $res;
    }

    /**
     * Get common log report
     * Created By : Khushboo tanwar
     * Date:17-06-2020
     */
    public function GetCommonLogReport($type, $post) {
        $activity_names = ['Line Log', 'Portable Equipment Log', 'Process Log'];
        $activity_names = isset($type) && !empty($type) ? [$type] : $activity_names;
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_user_activity_log.id,pts_trn_user_activity_log.doc_id,p.activity_name as logname,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%b-%Y %H:%i:%s') as start_date_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y %H:%i:%s') as stop_date_time,pts_trn_user_activity_log.is_qa_approve,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as activity_time,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,mst_area.block_code,mst_product.product_name,CASE WHEN mst_equipment.equipment_name is null THEN pts_mst_line_log.line_log_name ELSE mst_equipment.equipment_name END AS equipment_name", FALSE);
        $this->db->from("pts_trn_user_activity_log");
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.roomlogactivity=pts_mst_activity.id", 'left');
        $this->db->join("pts_mst_activity AS p", "p.id=pts_trn_user_activity_log.activity_id", 'left');
        $this->db->join("pts_trn_room_log_activity", "pts_trn_room_log_activity.id=pts_trn_user_activity_log.roomlogactivity", 'left');
        $this->db->join("mst_product", "mst_product.product_code=pts_trn_user_activity_log.product_code", 'left');
        $this->db->join("mst_room", "mst_room.room_code=pts_trn_user_activity_log.room_code", 'left');
        $this->db->join("mst_area", "mst_area.area_code=mst_room.area_code", 'left');
        $this->db->join("mst_equipment", "mst_equipment.equipment_code=pts_trn_user_activity_log.equip_code", 'left');
        $this->db->join("pts_mst_line_log", "pts_mst_line_log.line_log_code=pts_trn_user_activity_log.equip_code", 'left');

        $this->db->where("pts_trn_user_activity_log.status", 'active');
//        $this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
        if ($start_date != "" && $end_date != "") {
            $this->db->where('from_unixtime(pts_trn_user_activity_log.activity_start, "%Y-%m-%d") >=', $start_date);
            $this->db->where('from_unixtime(pts_trn_user_activity_log.activity_start, "%Y-%m-%d") <=', $end_date);
        }
        $this->db->where_in('pts_mst_activity.activity_name', $activity_names);
//        if (!empty($type)) {
//            $this->db->where('pts_mst_activity.activity_name', $type);
//        }

        $this->db->order_by('pts_trn_activity_approval_log.id DESC');
        if (isset($post['limit']) && isset($post['page']) && $post['limit'] > 0 && $post['page'] > 0) {
            $offset = ( $post['page'] - 1 ) * $post['limit'];
            $this->db->limit($post['limit'], $offset);
        }
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Get common log report
     * Created By : Khushboo tanwar
     * Date:17-06-2020
     */
    public function GetCommonLogReportCount($type, $post) {
        $activity_names = ['Line Log', 'Portable Equipment Log', 'Process Log'];
        $activity_names = isset($type) && !empty($type) ? [$type] : $activity_names;
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_user_activity_log.id,pts_trn_user_activity_log.doc_id,p.activity_name as logname,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%b-%Y %H:%i:%s') as start_date_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y %H:%i:%s') as stop_date_time,pts_trn_user_activity_log.is_qa_approve,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as activity_time,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,mst_area.block_code,mst_product.product_name,CASE WHEN mst_equipment.equipment_name is null THEN pts_mst_line_log.line_log_name ELSE mst_equipment.equipment_name END AS equipment_name", FALSE);
        $this->db->from("pts_trn_user_activity_log");
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.roomlogactivity=pts_mst_activity.id", 'left');
        $this->db->join("pts_mst_activity AS p", "p.id=pts_trn_user_activity_log.activity_id", 'left');
        $this->db->join("pts_trn_room_log_activity", "pts_trn_room_log_activity.id=pts_trn_user_activity_log.roomlogactivity", 'left');
        $this->db->join("mst_product", "mst_product.product_code=pts_trn_user_activity_log.product_code", 'left');
        $this->db->join("mst_room", "mst_room.room_code=pts_trn_user_activity_log.room_code", 'left');
        $this->db->join("mst_area", "mst_area.area_code=mst_room.area_code", 'left');
        $this->db->join("mst_equipment", "mst_equipment.equipment_code=pts_trn_user_activity_log.equip_code", 'left');
        $this->db->join("pts_mst_line_log", "pts_mst_line_log.line_log_code=pts_trn_user_activity_log.equip_code", 'left');

        $this->db->where("pts_trn_user_activity_log.status", 'active');
//        $this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
        if ($start_date != "" && $end_date != "") {
            $this->db->where('from_unixtime(pts_trn_user_activity_log.activity_start, "%Y-%m-%d") >=', $start_date);
            $this->db->where('from_unixtime(pts_trn_user_activity_log.activity_start, "%Y-%m-%d") <=', $end_date);
        }
        $this->db->where_in('pts_mst_activity.activity_name', $activity_names);
        $result = $this->db->get();
        $totalcount = $result->num_rows();
        //echo $this->db->last_query();exit;
        return $totalcount;
    }

    /**
     * Get Pending Approval for Email format
     * Created By :Rahul Chauhan
     * Date:19-06-2020
     */
    public function GetInprogressActivityListForEmail($act_id) {
        $result = array();
        $this->db->select("pts_trn_workflowsteps.next_step,pts_trn_workflowsteps.role_id,pts_trn_user_activity_log.activity_stop,pts_trn_user_activity_log.workflowstatus,pts_trn_user_activity_log.workflownextstep,pts_trn_user_activity_log.id as act_id,pts_trn_user_activity_log.activity_id as activity_id,pts_trn_user_activity_log.doc_id,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.activity_remarks,pts_trn_room_log_activity.activity_name,pts_trn_user_activity_log.roomlogactivity as processid,pts_mst_activity.type,pts_mst_activity.activity_url,pts_trn_user_activity_log.equip_code,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%M-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%M-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%m-%Y') as stop_date,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.workflownextstep,pts_trn_user_activity_log.user_id,pts_trn_user_activity_log.stop_by,pts_trn_user_activity_log.stop_by_id,pts_trn_user_activity_log.dies_no,pts_trn_user_activity_log.sampling_rod_id,pts_trn_user_activity_log.act_name,pts_trn_user_activity_log.id as log_id,pts_trn_user_activity_log.user_id,pts_mst_activity.activity_name as home_activity");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->join("pts_trn_room_log_activity", "pts_trn_room_log_activity.id=pts_trn_user_activity_log.roomlogactivity", 'left');
        $this->db->join("pts_trn_workflowsteps", "pts_trn_user_activity_log.activity_id=pts_trn_workflowsteps.activity_id", 'left');
        $this->db->join("pts_mst_activity", "pts_mst_activity.id=pts_trn_user_activity_log.activity_id", 'left');
        $this->db->where("pts_trn_user_activity_log.workflownextstep>", '-1');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
        if ($act_id > 0) {
            $this->db->where("pts_trn_user_activity_log.id", $act_id);
        }
        //$this->db->group_by("pts_trn_user_activity_log.activity_id");
        $this->db->group_by("pts_trn_user_activity_log.doc_id");
        $result = $this->db->get()->result_array();
//       echo $this->db->last_query();exit;
        return $result;
    }

    public function GetmstHeaderData($logid) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d");
        $result = array();
        $this->db->select("*");
        $this->db->from("pts_mst_report_header");
        $this->db->where("header_id", $logid);
        $this->db->where("effective_date <=", $date);
        $where = " (retired_date >= '" . $date . "' OR retired_date is null OR retired_date='') ";
        $this->db->where($where);
        $this->db->where("status", 'active');
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();
//        exit;
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }

    public function getMultipleRowsforDepartment($db, $table, $param, $order = NULL) {
        $CI = &get_instance();
        $this->db = $CI->load->database($db, TRUE);
        $this->db->from($table);
        $this->db->where("is_active", 1);
        $this->db->where_in("block_code", $param['block_code']);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        } else {
            return array();
        }
    }

    public function getMultipleRowsforArea($db, $table, $param, $order = NULL) {
        $CI = &get_instance();
        $this->db = $CI->load->database($db, TRUE);
        $this->db->from($table);
        $this->db->where("is_active", 1);
        $this->db->where_in("block_code", $param['block_code']);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        } else {
            return array();
        }
    }

    public function getMultipleRowsRoom($db, $table, $param, $order = NULL) {
        $CI = &get_instance();
        $this->db = $CI->load->database($db, TRUE);
        $this->db->from($table);
        $this->db->where("is_active", 1);
        $this->db->where_in("area_code", $param['area_code']);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        } else {
            return array();
        }
    }

   
    public function GetPunchSetAllocationRecord() {
        $this->db->select("pts_trn_punch_set_allocation.*,mst_product.product_name");
        $this->db->from("pts_trn_punch_set_allocation");
        $this->db->join("mst_product", "mst_product.product_code=pts_trn_punch_set_allocation.product_code", 'left');
        $this->db->where("pts_trn_punch_set_allocation.status", 'active');
        $this->db->where("pts_trn_punch_set_allocation.is_in_workflow", 'no');
        $this->db->group_by("pts_trn_punch_set_allocation.product_code");
//        if(isset($data['room_code']) && $data['room_code']!=""){
//        $this->db->where("pts_trn_punch_set_allocation.room_code",$data['room_code']);
//        }
//        if(isset($data['punch_set']) && $data['punch_set']!=""){
//        $this->db->where("pts_trn_punch_set_allocation.punch_set",$data['punch_set']);
//        }
//        if(isset($data['product_code']) && $data['product_code']!=""){
//        $this->db->where("pts_trn_punch_set_allocation.product_code",$data['product_code']);
//        }
        $result = $this->db->get()->result_array();
//       echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * @author Nitin Mittal
     *
     * @param userid @type int for finding user's passwords history userid>0
     * @param password  @type String for checking if the password has been used in last n passwords currently n = 5
     * @return true/false @type Boolean
     * 18-8-2020
     */
    public function checkUserPasswordUsed($userId, $password) {
        $this->db->select("password");
        $this->db->from("mst_employee_password_history");
        $this->db->where("user_id", $userId);
        $this->db->order_by("id", 'DESC');
        $this->db->limit(5, 0);
        $password_histroy = $this->db->get()->result_array();
        $password_array = [];
        if (!empty($password_histroy)) {
            foreach ($password_histroy as $key => $value) {
                $password_array[] = $value['password'];
            }
        }
        return in_array($password, $password_array);
    }

    public function getUsersPasswordAboutToExpire() {
        $date = date('Y-m-d', strtotime(date('Y-m-d') . ' + 2 day'));
        $this->db->select("id");
        $this->db->from("mst_employee");
        $this->db->where("DATE(expire_on)<", $date);
        $this->db->where("is_directory", 0);
        $user_list = $this->db->get()->result_array();
        $user_array = [];
        if (!empty($user_list)) {
            foreach ($user_list as $key => $value) {
                $user_array[] = $value['id'];
            }
        }
        return $user_array;
    }

    public function setFirstTimeLoginTrue($ids) {
        $this->db->set('is_first', '1');
        $this->db->where_in('id', $ids);
        $this->db->update('mst_employee');
        //echo $this->db->last_query();exit;
    }

    public function getBlockedUsers() {
        date_default_timezone_set('Asia/Kolkata');
        $date = date('Y-m-d H:i:s');
        $this->db->select("id");
        $this->db->from("mst_employee");
        $this->db->where("DATE_ADD(block_datetime,INTERVAL 15 MINUTE)<", $date);
        $this->db->where("is_blocked", '1');
        $user_list = $this->db->get()->result_array();
        $user_array = [];
        if (!empty($user_list)) {
            foreach ($user_list as $key => $value) {
                $user_array[] = $value['id'];
           }
        }
        return $user_array;
    }

    public function unblockUsers($ids) {
        $this->db->set('is_blocked', 0);
        $this->db->set('block_datetime', '');
        $this->db->where_in('id', $ids);
        $this->db->update('mst_employee');
//        echo $this->db->last_query();exit;
    }

    public function getOldRolesAndResponsibilties($role_id, $module_type) {
        $old_role = [];
        if ($module_type == 'report') {
                $this->db->select("pts_mst_role_mgmt.module_id,IF(`pts_mst_role_mgmt`.`is_create`=1,'1','') AS `is_create`,IF(`pts_mst_role_mgmt`.`is_edit`=1,'1','') AS is_edit,IF(`pts_mst_role_mgmt`.`is_view`=1,'1','') AS `is_view`,`pts_mst_report`.report_name as log_name");
                $this->db->join("pts_mst_report", 'pts_mst_report.id = pts_mst_role_mgmt.module_id','left');
        }
        if ($module_type == 'master') {
                $this->db->select("pts_mst_role_mgmt.module_id,IF(`pts_mst_role_mgmt`.`is_create`=1,'1','') AS `is_create`,IF(`pts_mst_role_mgmt`.`is_edit`=1,'1','') AS is_edit,IF(`pts_mst_role_mgmt`.`is_view`=1,'1','') AS `is_view`,`pts_mst_master`.master_name as log_name");
                $this->db->join("pts_mst_master", 'pts_mst_master.id = pts_mst_role_mgmt.module_id', 'left');
        }
        //$this->db->select("pts_mst_role_mgmt.module_id,IF(pts_mst_role_mgmt.is_create='1','Yes','No') AS can_create, IF(pts_mst_role_mgmt.is_edit='1','Yes','No') AS can_edit, IF(pts_mst_role_mgmt.is_view='1','Yes','No') AS can_view,pts_mst_master.master_name");
        $this->db->from("pts_mst_role_mgmt");
        //$this->db->join("pts_mst_master", "pts_mst_master.id =  pts_mst_role_mgmt.module_id", "left");
        $this->db->where("pts_mst_role_mgmt.role_id", $role_id);
        $this->db->where("pts_mst_role_mgmt.module_type", $module_type);
        $old_data = $this->db->get()->result_array();
//        if (!empty($old_data)) {
//            foreach ($old_data as $key => $value) {
//                $old_role[$value['module_id']] = ['log_name' => $value['master_name'], 'can_create' => $value['can_create'], 'can_edit' => $value['can_edit'], 'can_view' => $value['can_view']];
//            }
//        }
        return $old_data;
    }

    public function getNewRolesAndResponsibiltiesChanges($newArray, $oldArray) {
        //echo '<pre>'; print_r($newArray);print_r($oldArray); exit;
        $final_array = [];
        if (!empty($newArray)) {
            foreach ($newArray as $key => $value) {
                $x = '';
                if (array_key_exists($key, $oldArray)) {
                    foreach ($value as $k => $v) {
                        if ($v != $oldArray[$key][$k]) {
                            $x .= $k . ' : ' . $oldArray[$key][$k] . ' To ' . $v . ' ';
                        }
                    }
                } else {
                    $x .= $v . ' , ';
                }
                if (!empty($x)) {
                    $final_array[] = $value['log_name'] . ' : ' . $x;
                }
            }
        }
        return implode(',', $final_array);
    }

    public function GetAttachmentListforReport($act_id) {
        $this->db->select("pts_trn_log_file_details.*");
        $this->db->from("pts_trn_log_file_details");
        $this->db->where("pts_trn_log_file_details.act_id", $act_id);
        $this->db->where("pts_trn_log_file_details.status", "active");
        $res = $this->db->get()->result_array();
        $tempArray = array();
        if (!empty($res)) {
            return $res;
        } else {
            return array();
        }
    }

    /**
     * Punch Set Allocation Report
     * Created By : Mrinal Sen Gupta
     * Created Date:07-09-2020
     */
    public function getPunchSetAllocationReport($post) {
        //$vaccume_no = $post['vaccume_no'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
//        echo $start_date;exit;
        $this->db->select("pts_trn_punch_set_allocation.*,DATE_FORMAT(`pts_trn_punch_set_allocation`.`dom`,'%b-%Y') AS dom,mst_product.product_name,pts_trn_user_activity_log.id,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%b-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%b-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%b-%Y %H:%i:%s') as activity_time,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type");
        $this->db->from("pts_trn_punch_set_allocation");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_punch_set_allocation.doc_no", 'left');
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
		$this->db->join("mst_product", "pts_trn_punch_set_allocation.product_code=mst_product.product_code", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
        //$this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
     if (!empty($start_date && $end_date)) {
            $this->db->where('pts_trn_punch_set_allocation.created_on>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            $this->db->where('pts_trn_punch_set_allocation.created_on<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }
       /* if (!empty($apparatus_no)) {
            $this->db->where('pts_trn_vaccum_cleaner.vaccum_cleaner_code', $vaccume_no);
        }*/
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    public function GetQaApprovalActivityDataById($id) {
        $result = array();
        $this->db->select("pts_trn_user_activity_log.*,`pts_mst_activity`.activity_name,p.activity_name AS pro ");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->join("pts_mst_activity", "`pts_mst_activity`.`id` = `pts_trn_user_activity_log`.`activity_id`", 'left');
        $this->db->join("pts_mst_activity AS p", "p.id = `pts_trn_user_activity_log`.`roomlogactivity`", 'left');
        $this->db->where("pts_trn_user_activity_log.id", $id);
        $result = $this->db->get()->row_array();
        return $result;
    }

    // get the data fom mysql table
    public function getMultipleRowsNew($db, $table, $param = [], $orderBy = Null,$order = 'DESC', $page = null) {
        $CI = &get_instance();
        $this->db = $CI->load->database($db, TRUE);
        $this->db->from($table);
        if (!empty($param)){
            foreach($param as $key=>$value){
                $this->db->where($key,$value);
            }
        }
        if(!empty($orderBy)) {
            $this->db->order_by($orderBy,$order);
        }
        if (!empty($page)) {
            $offset = ($page - 1) * (self::records_per_page);
            $this->db->limit(self::records_per_page, $offset);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        } else {
            return array();
        }
    }

    public function writeOnExceptionFile($exception){
        $today = date('Y-m-d');
        $filename = 'exception_'.$today.'.txt';
        $fn = fopen($filename,"a");
        $date = date('Y-m-d H:i:s');
        fwrite($fn, $date.' --> '.$exception);  
        fclose($fn);
    }

    public function getNAUpdated($array){
        $a = [];
        if(!empty($array) && is_array($array)){
            foreach($array as $key=>$value){
                if(empty($value)){
                    $a[$key] = 'N/A';
                }else{
                    $a[$key] = $value;
                }
            }
        }
        return $a;
    }

    public function getUserOldRights($user_id,$module_type){
	if ($module_type == 'log') {
                $this->db->select("pts_mst_user_mgmt.id as log_id,pts_mst_user_mgmt.room_code,pts_mst_user_mgmt.type_id,pts_mst_user_mgmt.module_id,pts_mst_user_mgmt.is_create,pts_mst_user_mgmt.is_edit,pts_mst_user_mgmt.is_view,pts_mst_activity.*,pts_mst_user_mgmt.status as mstatus,pts_mst_user_mgmt.module_id");
                $this->db->join("pts_mst_activity", 'pts_mst_activity.id = pts_mst_user_mgmt.module_id');
        }
        if ($module_type == 'report') {
                $this->db->select("IF(`pts_mst_user_mgmt`.`is_create`=1,'1','') AS `is_create`,IF(`pts_mst_user_mgmt`.`is_edit`=1,'1','') AS is_edit,IF(`pts_mst_user_mgmt`.`is_view`=1,'1','') AS `is_view`,`pts_mst_report`.report_name,pts_mst_user_mgmt.module_id");
                $this->db->join("pts_mst_report", 'pts_mst_report.id = pts_mst_user_mgmt.module_id');
        }
        if ($module_type == 'master') {
                $this->db->select("IF(`pts_mst_user_mgmt`.`is_create`=1,'1','') AS `is_create`,IF(`pts_mst_user_mgmt`.`is_edit`=1,'1','') AS is_edit,IF(`pts_mst_user_mgmt`.`is_view`=1,'1','') AS `is_view`,`pts_mst_master`.master_name,pts_mst_user_mgmt.module_id");
                $this->db->join("pts_mst_master", 'pts_mst_master.id = pts_mst_user_mgmt.module_id', 'left');
        }
        $this->db->from("pts_mst_user_mgmt");
        $this->db->where("pts_mst_user_mgmt.module_type", $module_type);
        $this->db->where("pts_mst_user_mgmt.user_id", $user_id);
//        $this->db->where("pts_mst_user_mgmt.status", "active");
        $res = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $res;
    }

    public function getUserOldLogsRights($user_id,$room_code){
        $this->db->select("`pts_mst_user_mgmt`.`room_code`,`pts_mst_user_mgmt`.`type_id`,IF(`pts_mst_user_mgmt`.`is_create`=1,'Add','') AS `add`,IF(`pts_mst_user_mgmt`.`is_edit`=1,'Update','') AS edit,IF(`pts_mst_user_mgmt`.`is_view`=1,'View','') AS `view`,`pts_mst_activity`.`activity_name`, `pts_mst_user_mgmt`.`status` AS `mstatus`");
        $this->db->from("pts_mst_user_mgmt");
        $this->db->join("pts_mst_activity", 'pts_mst_activity.id = pts_mst_user_mgmt.module_id', 'left');
        $this->db->where("pts_mst_user_mgmt.room_code", $room_code);
        $this->db->where("pts_mst_user_mgmt.user_id", $user_id);
        $res = $this->db->get()->row_array();
        return $res;
    }

    public function getUserOldLogsRightsById($id){
        $this->db->select("pts_mst_user_mgmt.user_id,mst_employee.emp_name,`pts_mst_user_mgmt`.`room_code`,`pts_mst_user_mgmt`.`type_id`,IF(`pts_mst_user_mgmt`.`is_create`=1,'Add','') AS `add`,IF(`pts_mst_user_mgmt`.`is_edit`=1,'Update','') AS edit,IF(`pts_mst_user_mgmt`.`is_view`=1,'View','') AS `view`,`pts_mst_activity`.`activity_name`, `pts_mst_user_mgmt`.`status` AS `mstatus`");
        $this->db->from("pts_mst_user_mgmt");
        $this->db->join("pts_mst_activity", 'pts_mst_activity.id = pts_mst_user_mgmt.module_id', 'left');
        $this->db->join("mst_employee", 'mst_employee.id = pts_mst_user_mgmt.user_id', 'left');
        $this->db->where("pts_mst_user_mgmt.id", $id);
        $res = $this->db->get()->row_array();
        return $res;
    }

    public function getLogOldAccessRights($role_id){
        $this->db->select("pts_mst_role_mgmt.type_id,pts_mst_activity.activity_name");
        $this->db->from("pts_mst_role_mgmt");
        $this->db->join("pts_mst_activity", 'pts_mst_role_mgmt.module_id = pts_mst_activity.id', 'left');
        $this->db->where("pts_mst_role_mgmt.module_type", "log");//pts_mst_role_mgmt.module_type="log"
        $this->db->where("pts_mst_role_mgmt.role_id", $role_id);//pts_mst_role_mgmt.module_type="log"
        $this->db->where("pts_mst_role_mgmt.is_create", 1);//pts_mst_role_mgmt.module_type="log"
        $this->db->where("pts_mst_role_mgmt.is_edit", 1);//pts_mst_role_mgmt.module_type="log"
        $this->db->where("pts_mst_role_mgmt.is_view", 1);//pts_mst_role_mgmt.module_type="log"
        $res = $this->db->get()->result_array();
        return $res;
    }

    public function getUserNewRolesAndResponsibiltiesChanges($newArray, $oldArray) {
        //echo '<pre>'; print_r($newArray[1]);print_r($oldArray[1]); exit;
        $final_array = [];
        if (!empty($newArray)) {
            foreach ($newArray as $key => $value) {
                $y = '';
                if (array_key_exists($key, $oldArray)){
                    if($value[1]!=$oldArray[$key][1]){
                        $y.= $value[1];
                    }
                    if($value[2]!=$oldArray[$key][2]){
                        $y.= $value[2];
                    }
                    if($value[3]!=$oldArray[$key][3]){
                        $y.= $value[3];
                    }
                }else{
                    $y.= $value[1].$value[2].$value[3];
                }
                if(!empty($y)){
                    $final_array[] = $value[0].' : '.rtrim($y, ', ');
                }
            }
        }
        return implode(',', $final_array);
    }

    public function updateDBDocNo(){
        $this->db->select("ta.id AS log_id,ta.doc_id, c.id,c.log_user_activity_id,c.command_type,c.record_type,c.table_extra_field,c.table_unique_field1");
        $this->db->from("`pts_trn_user_activity_log` AS ta ");
        $this->db->join("pts_elog_history AS c", "ta.id= c.log_user_activity_id", 'left');
        $this->db->where("c.log_user_activity_id>", 0);
        $this->db->where("c.is_used", "0");
        $this->db->where("DATE_FORMAT(c.created_on,'%Y-%m-%d')>", "2020-12-15");
        $this->db->where("ta.id>", "282");
        $this->db->limit(10);
        $res = $this->db->get()->result_array();
        //echo $this->db->last_query();exit;
        $tempArray = array();
        if (!empty($res)) {
            foreach($res as $r){
                $extra_fields = json_decode($r['table_extra_field'],true);
                $table_unique_field1 = 'doc_no:'.$r['doc_id'];
                if(array_key_exists("doc_no", $extra_fields)){
                      $extra_fields['doc_no'] =  $r['doc_id'];                 
                }
                
                $post = ['table_extra_field'=>json_encode($extra_fields),'table_unique_field1'=>$table_unique_field1,'is_used'=>'1'];
                $this->db->where("id",$r['id']);
                $insert = $this->db->update('pts_elog_history', $post);
            }
            echo '10 records updated';exit;
        } else {
            return 'All Done';
        }
    }

    public function testcommit(){
        try{
            $this->db->trans_start();
            $this->db->insert('abcd', ['name'=>'mola']);
            $this->db->insert('acde', ['name'=>'dd']);
            $this->db->trans_complete();
            if ($this->db->trans_status() == FALSE){
                return 'transaction failed';
                //$this->db->trans_rollback();
            }else{
                return 'transaction success';
                //$this->db->trans_commit();
            }
        }catch(Exception $e){
            return $e.'nitihinini';
        }
    }

    public function newFunctionToSubmitEnvLog($isworkflow, $tblname, $data, $data2, $data3) {
        $actlogtblid = 0;
        $doc_no = '';
        try {
            $this->db->trans_start();
            $docResult = $this->db->get_where("pts_mst_document", ['id' => '13'])->row_array();
            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $arr2 = ["docno" => $docno];
                $this->db->where("id", 13);
                $this->db->update("pts_mst_document", $arr2);
            }
            $data['doc_no'] = $doc_no;
            $data2['doc_id'] = $doc_no;
            if ($isworkflow == 0) {
                if ($this->db->insert($tblname, $data)) {
                    $re_air_filter_tblid = $this->db->insert_id();
                }
            } else {
                $data['is_in_workflow'] = 'yes';
                $this->db->insert($tblname, $data);
                $re_air_filter_tblid = $this->db->insert_id();
                if ($re_air_filter_tblid > 0) {
                    $this->db->insert("pts_trn_user_activity_log", $data2);
                    $actlogtblid = $this->db->insert_id();
                    $data3['usr_activity_log_id'] = $actlogtblid;
                    $this->db->insert("pts_trn_activity_approval_log", $data3);
                }
            }
            if ($actlogtblid > 0) {
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "logger_id:" . $data['data_log_id'];
                $uniquefiled3 = "gauge_id:" . $data['magnelic_id'];
                $auditParams = array('command_type' => 'insert', 'created_by' => $data['done_by_user_id'], 'created_by_name' => $data['done_by_user_name'], 'activity_name' => 'Environmental Condition/Pressure Differential Record', 'type' => 'environmental_condition_record', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_env_cond_diff', 'primary_id' => $re_air_filter_tblid, 'log_user_activity_id' => $actlogtblid, 'post_params' => $data);
                $this->commonFunctionToAddAuditTransaction($auditParams);
            }
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }
        return $actlogtblid;
    }

    /*     * *
     * @desc Fetches table "pts_mst_document" with last Docno
     * @param $docno
     * @return array
     * @author Nitin
     * @date 18-02-2021
     */

    public function getLastDocNo() {
        return $docResult = $this->db->get_where("pts_mst_document", ['id' => '13'])->row_array();
    }

    /*     * *
     * @desc Update table "pts_mst_document" with new value 
     * @param $docno
     * @author Nitin
     * @date 18-02-2021
     */

    public function updateLastDocNo($docno) {
        $arr2 = ["docno" => $docno];
        $this->db->where("id", 13);
        $this->db->update("pts_mst_document", $arr2);
    }

    /*     * *
     * @desc Common function to start new logs portable log, process log, line log
     * @param n number of variabless from front end
     * @return int
     * @author Nitin
     * @date 19-02-2021
     */

    public function commonLogInsertNewFunction($arr, $arr3, $Logname, $data, $auditParams, $isworkflow = 0, $equipmentcode = '', $sampling_rod_id = '') {
        $actlogtblid = 0;
        $audit_id = 0;
        $doc_no = '';
        try {
            $this->db->trans_start();
            $docResult = $this->db->get_where("pts_mst_document", ['id' => '13'])->row_array();

            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $arr2 = ["docno" => $docno];
                $this->db->where("id", 13);
                $this->db->update("pts_mst_document", $arr2);
            }
            $arr['doc_id'] = $doc_no;
            $this->db->insert("pts_trn_user_activity_log", $arr);
            $actlogtblid = $this->db->insert_id();
            $arr3["usr_activity_log_id"] = $actlogtblid;
            $this->db->insert("pts_trn_activity_approval_log", $arr3);
            if ($actlogtblid > 0) {
                if ($Logname == "Linelog") {
                    $equipArray = array();
                    if ($equipmentcode != '' && $equipmentcode != 'NA') {
                        $equipArray = explode(',', $equipmentcode);
                        if (!empty($equipArray)) {
                            foreach ($equipArray as $equip) {
                                $post = array('is_inused' => 1);
                                $param = array("line_log_code" => $equip);
                                $this->updateToTable($db = 'default', 'pts_mst_line_log', $post, $param);
                            }
                        }
                    }
                } else {
                    if ($sampling_rod_id != '' && $sampling_rod_id != NULL) {
                        $post = ['is_inused' => 1];
                        $param = ["equipment_code" => $sampling_rod_id];
                        $this->updateToTable($db = 'default', 'pts_mst_instrument', $post, $param);
                    } else {
                        $equipArray = [];
                        if ($equipmentcode != '' && $equipmentcode != 'NA') {
                            $equipArray = explode(',', $equipmentcode);
                            if (!empty($equipArray)) {
                                foreach ($equipArray as $equip) {
                                    $post = ['is_inused' => 1];
                                    $param = ["equipment_code" => $equip];
                                    $this->updateToTable($db = 'default', 'mst_equipment', $post, $param);
                                }
                            }
                        }
                    }
                }
                /* Start - Insert elog audit histry */
                $data["doc_no"] = $doc_no;
                $auditParams['uniquefiled1'] = "doc_no:" . $doc_no;
                $auditParams['uniquefiled2'] = "equipment_id:" . $equipmentcode;
                $auditParams['uniquefiled3'] = "pre_barch_no:" . $data["batch_no"];
                $auditParams['primary_id'] = $actlogtblid;
                $auditParams['log_user_activity_id'] = $actlogtblid;
                $auditParams['post_params'] = $data;
                $audit_id = $this->commonFunctionToAddAuditTransaction($auditParams);
            }
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }
        return $actlogtblid;
    }

    /*     * *
     * @desc Inserts into log table when a new activity/log is started
     * @param $tblname string, $data array $data2 array $data3 array
     * @author Nitin
     * @date 18-02-2021
     */

    public function commonLogDetailsInsert($isworkflow, $tblname, $data, $data2, $data3, $id) {
        $actlogtblid = 0;
        if ($isworkflow == 0) {
            if ($this->db->update($tblname, $data, array("id" => $id))) {
                $re_air_filter_tblid = $this->db->insert_id();
            }
        } else {
            $data['is_in_workflow'] = 'yes';
            $this->db->update($tblname, $data, array("id" => $id));
            $re_air_filter_tblid = $id;
            if ($re_air_filter_tblid > 0) {
                $this->db->insert("pts_trn_user_activity_log", $data2);
                $actlogtblid = $this->db->insert_id();
                $data3['usr_activity_log_id'] = $actlogtblid;
                $this->db->insert("pts_trn_activity_approval_log", $data3);
            }
        }
        return $actlogtblid;
    }

    /*     * *
     * @desc Inserts into log table when a new activity for return air filter is started
     * @param $tblname string, $data array $data2 array $data3 array
     * @author Nitin
     * @date 22-02-2021 
     */

    public function newFunctionToSubmitReturnAirFilter($isworkflow, $data, $data2, $data3, $auditParams) {
        //pts_trn_return_air_filter
        $activityId = '';
        $insertedId = '';
        $message = '';
        $doc_no = '';
        $actlogtblid = 0;
        $re_air_filter_tblid = 0;
        $inserted_id = 0;
        $activity_id = 0;
        try {
            $this->db->trans_start();
            $docResult = $this->db->get_where("pts_mst_document", ['id' => '13'])->row_array();
            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $arr2 = ["docno" => $docno];
                $this->db->where("id", 13);
                $this->db->update("pts_mst_document", $arr2);
            }
            $data['is_in_workflow'] = 'yes';
            $data['doc_no'] = $doc_no;
            $this->db->insert('pts_trn_return_air_filter', $data);
            $re_air_filter_tblid = $this->db->insert_id();
            $data2['doc_id'] = $doc_no;
            $this->db->insert("pts_trn_user_activity_log", $data2);
            $actlogtblid = $this->db->insert_id();
            $data3["usr_activity_log_id"] = $actlogtblid;
            $this->db->insert("pts_trn_activity_approval_log", $data3);
            $auditParams['primary_id'] = $re_air_filter_tblid;
            $auditParams['log_user_activity_id'] = $actlogtblid;
            $auditParams['post_params'] = $data;
            $auditParams['uniquefiled1'] = "doc_no:" . $doc_no;
            $auditParams['uniquefiled2'] = "frequency_id:" . $data['frequency_id'];
            $auditParams['uniquefiled3'] = "batch_no:" . $data['batch_no'];
            $audit_id = $this->commonFunctionToAddAuditTransaction($auditParams);
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }
        return $actlogtblid;
    }

    /*     * *
     * @desc Inserts into log table when a new activity for return air filter is started
     * @param $tblname string, $data array $data2 array $data3 array
     * @author Nitin
     * @date 22-02-2021 
     */

    public function newFunctionToSubmitVacuumCleanerLogRecord($isworkflow, $data, $data2, $data3, $auditParams) {
        //pts_trn_return_air_filter
        $activityId = '';
        $insertedId = '';
        $message = '';
        $doc_no = '';
        $actlogtblid = 0;
        $re_air_filter_tblid = 0;
        $inserted_id = 0;
        $activity_id = 0;
        try {
            $this->db->trans_start();
            $docResult = $this->db->get_where("pts_mst_document", ['id' => '13'])->row_array();
            $doc_no = '';
            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $arr2 = ["docno" => $docno];
                $this->db->where("id", 13);
                $this->db->update("pts_mst_document", $arr2);
            }
            $data['is_in_workflow'] = 'yes';
            $data['doc_no'] = $doc_no;
            $this->db->insert('pts_trn_vaccum_cleaner', $data);
            $re_air_filter_tblid = $this->db->insert_id();
            $data2['doc_id'] = $doc_no;
            $this->db->insert("pts_trn_user_activity_log", $data2);
            $actlogtblid = $this->db->insert_id();
            $data3["usr_activity_log_id"] = $actlogtblid;
            $this->db->insert("pts_trn_activity_approval_log", $data3);
            $auditParams['primary_id'] = $re_air_filter_tblid;
            $auditParams['log_user_activity_id'] = $actlogtblid;
            $data = $this->getNAUpdated($data);
            $auditParams['post_params'] = $data;

            $auditParams['uniquefiled1'] = "doc_no:" . $doc_no;
            $audit_id = $this->commonFunctionToAddAuditTransaction($auditParams);
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }
        return $actlogtblid;
    }

    /**
     * @author Deepak Khare
     * function : Swab Log insert function with Transaction
     * Date: 18 Feb 2021
     */
    public function swabLogInsert($isworkflow, $tblname, $data, $data2, $data3) {
        try {
            $message = '';
            $doc_no = '';
            $actlogtblid = 0;
            $re_air_filter_tblid = 0;
            $inserted_id = 0;
            $activity_id = 0;
            $this->db->trans_start();
            $docResult = $this->getLastDocNo();
            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $this->updateLastDocNo($docno);
            }

            $data['doc_no'] = $doc_no;
            $data2['doc_id'] = $doc_no;

            $this->db->insert($tblname, $data);
            $re_air_filter_tblid = $this->db->insert_id();

            if ($re_air_filter_tblid > 0) {
                $this->db->insert("pts_trn_user_activity_log", $data2);
                $actlogtblid = $this->db->insert_id();
                $data3['usr_activity_log_id'] = $actlogtblid;
                $this->db->insert("pts_trn_activity_approval_log", $data3);
            }

            if ($actlogtblid > 0) {
                if ($isworkflow == 0) {
                    $inserted_id = $actlogtblid;
                    $activity_id = 0;
                } else {
                    $inserted_id = $re_air_filter_tblid;
                    $activity_id = $actlogtblid;
                }

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "equipment_id:" . $data['equipment_id'];
                $uniquefiled3 = "pre_barch_no:" . $data['pre_barch_no'];
                $auditParams = array('command_type' => 'insert', 'created_by' => $data['done_by_user_id'], 'created_by_name' => $data['done_by_user_name'], 'activity_name' => 'Swab Sample Record', 'type' => 'swab_sample_record', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_swab_sample_record', 'primary_id' => $inserted_id, 'log_user_activity_id' => $activity_id, 'post_params' => $data);
                $this->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */
            }

            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }
        return ['actlogtblid' => $actlogtblid, 'doc_no' => $doc_no, 'message' => $message];
    }

    /**
     * @author Deepak Khare
     * function : Laf Preasure Record Log insert function with Transaction
     * Date: 22 Feb 2021
     */
    public function lafPresureRecord($isworkflow, $tblname, $data, $data2, $data3) {
        $message = '';
        $doc_no = '';
        $actlogtblid = 0;
        $re_air_filter_tblid = 0;
        $inserted_id = 0;
        $activity_id = 0;
        try {

            $this->db->trans_start();
            $docResult = $this->getLastDocNo();
            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $this->updateLastDocNo($docno);
            }

            $data['doc_no'] = $doc_no;
            $data2['doc_id'] = $doc_no;
            if ($isworkflow == 0) {
                $this->db->insert($tblname, $data);
                $re_air_filter_tblid = $this->db->insert_id();
            } else {
                $data['is_in_workflow'] = 'yes';
                $this->db->insert($tblname, $data);
                $re_air_filter_tblid = $this->db->insert_id();
            }

            if ($re_air_filter_tblid > 0) {
                $this->db->insert("pts_trn_user_activity_log", $data2);
                $actlogtblid = $this->db->insert_id();
                $data3['usr_activity_log_id'] = $actlogtblid;
                $this->db->insert("pts_trn_activity_approval_log", $data3);
            }

            if ($actlogtblid > 0) {
                if ($isworkflow == 0) {
                    $inserted_id = $actlogtblid;
                    $activity_id = 0;
                } else {
                    $inserted_id = $re_air_filter_tblid;
                    $activity_id = $actlogtblid;
                }

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "room_code:" . $data['room_code'];
                $uniquefiled3 = "booth_no:" . $data['booth_no'];
                $auditParams = array('command_type' => 'insert', 'created_by' => $data['done_by_user_id'], 'created_by_name' => $data['done_by_user_name'], 'activity_name' => 'LAF Pressure Differential Record', 'type' => 'laf_pressure_record', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_laf_pressure_diff', 'primary_id' => $inserted_id, 'log_user_activity_id' => $activity_id, 'post_params' => $data);
                $this->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */
            }

            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }
        return ['actlogtblid' => $actlogtblid, 'doc_no' => $doc_no, 'message' => $message];
    }

    /**
     * @author Deepak Khare
     * function : matRetrivalRecord Log insert function with Transaction
     * Date: 22 Feb 2021
     */
    public function matRetrivalRecord($isworkflow, $tblname, $data, $data2, $data3, $insertAudit) {
        try {
            $message = '';
            $doc_no = '';
            $actlogtblid = 0;
            $re_air_filter_tblid = 0;
            $inserted_id = 0;
            $activity_id = 0;
            $this->db->trans_start();
            $docResult = $this->getLastDocNo();
            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $this->updateLastDocNo($docno);
            }

            //$data['doc_no'] = $doc_no;
            $data2['doc_id'] = $doc_no;

            if ($isworkflow == 0) {
                if (isset($data[0]) && !empty($data[0]) && count($data) > 1) {
                    foreach ($data as $key => $value) {
                        $data[$key]['doc_no'] = $doc_no;
                    }
                    $this->db->insert_batch($tblname, $data);
                    $re_air_filter_tblid = $this->db->insert_id();
                } else {
                    $data['doc_no'] = $doc_no;
                    $this->db->insert($tblname, $data);
                    $re_air_filter_tblid = $this->db->insert_id();
                }
            } else {
                if (isset($data[0]) && !empty($data[0]) && count($data) > 1) {
                    foreach ($data as $key => $value) {
                        $data[$key]['is_in_workflow'] = 'yes';
                        $data[$key]['doc_no'] = $doc_no;
                    }
                    $this->db->insert_batch($tblname, $data);
                    $re_air_filter_tblid = $this->db->insert_id();
                } else {
                    $data['is_in_workflow'] = 'yes';
                    $data['doc_no'] = $doc_no;
                    $this->db->insert($tblname, $data);
                    $re_air_filter_tblid = $this->db->insert_id();
                }
            }

            if ($re_air_filter_tblid > 0) {
                $this->db->insert("pts_trn_user_activity_log", $data2);
                $actlogtblid = $this->db->insert_id();
                $data3['usr_activity_log_id'] = $actlogtblid;
                $this->db->insert("pts_trn_activity_approval_log", $data3);
            }

            if ($actlogtblid > 0) {
                if ($isworkflow == 0) {
                    $inserted_id = $actlogtblid;
                    $activity_id = $data2['activity_id'];
                } else {
                    $inserted_id = $re_air_filter_tblid;
                    $activity_id = $actlogtblid;
                }

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "process_id:" . $insertAudit['process_id'];
                $uniquefiled3 = "pre_barch_no:" . $insertAudit['pre_barch_no'];

                $insertAudit['doc_no'] = $doc_no;
                $auditParams = array(
                    'command_type' => 'insert',
                    'created_by' => $data3['user_id'],
                    'created_by_name' => $data3['user_name'],
                    'activity_name' => 'Material Retreival and Relocation Record for Cold Room',
                    'type' => 'material_retreival_relocation',
                    'uniquefiled1' => $uniquefiled1,
                    'uniquefiled2' => $uniquefiled2,
                    'uniquefiled3' => $uniquefiled3,
                    'table_name' => 'pts_trn_material_retreival_relocation',
                    'primary_id' => $inserted_id,
                    'log_user_activity_id' => $activity_id,
                    'post_params' => $insertAudit
                );
                $this->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */
            }
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }
        return ['actlogtblid' => $actlogtblid, 'doc_no' => $doc_no, 'message' => $message];
    }

    /**
     * This function is used to start the verticalSampler activity at home page
     * Created By :Deepak Khare
     * Date:22-02-2021
     */
    public function verticalSampler($headers, $roomcode, $processid, $equipmentcode, $actid, $actname, $product_no, $batch_no, $roleid, $empid, $empname, $email, $status_id, $next_step, $remark, $remark2, $sampling_rod_id, $dies_no, $act_name2, $headerRecordid, $process_name) {
        $arr = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => $equipmentcode, "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "versionid" => $headerRecordid, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);
        if ($sampling_rod_id != '' && $dies_no != '' && $act_name2 != "") {
            $arr['sampling_rod_id '] = $sampling_rod_id;
            $arr['dies_no'] = $dies_no;
            $arr['act_name'] = $act_name2;
            $arr['activity_name'] = $actname;
        }
        $message = '';
        $doc_no = '';
        $actlogtblid = 0;
        try {
            $this->db->trans_start();
            $docResult = $this->getLastDocNo();
            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $this->updateLastDocNo($docno);
            }

            $arr['doc_id'] = $doc_no;
            $this->db->insert("pts_trn_user_activity_log", $arr);
            $actlogtblid = $this->db->insert_id();
            $arr3 = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');
            $this->db->insert("pts_trn_activity_approval_log", $arr3);

            if ($actlogtblid > 0) {
                $arr2 = array("workflowstatus" => $status_id, "workflownextstep" => $next_step);
                $this->db->where("id", $actlogtblid);
                $this->db->update("pts_trn_user_activity_log", $arr2);
                $data = array(
                    "process_name" => $process_name,
                    "doc_no" => $doc_no,
                    "room_code" => $roomcode,
                    "act_id" => $actid,
                    "equipment_id" => $equipmentcode,
                    "act_name" => $actname,
                    "product_code" => $product_no,
                    "batch_no" => $batch_no,
                    "done_by_user_id" => $empid,
                    "done_by_user_name" => $empname,
                    "done_by_role_id" => $roleid,
                    "done_by_email" => $email,
                    "done_by_remark" => $remark,
                    "is_in_workflow" => 'no'
                );


                $inserted_id = $actlogtblid;
                $activity_id = $actid;

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "equipment_id:" . $equipmentcode;
                $uniquefiled3 = "pre_barch_no:" . $batch_no;
                //get activity name
                $activity_name_for_trail = "";
                $activityData = $this->getMultipleRows('default', 'pts_mst_activity', array('id' => $processid));
                if (!empty($activityData)) {
                    $activity_name_for_trail = $activityData[0]['activity_name'];
                }
                if ($actid == 25) {
                    $actname = $current_name;
                }
                $auditParams = array(
                    'command_type' => 'insert',
                    'created_by' => $empid,
                    'creeated_by_name' => $empname,
                    'activity_name' => $activity_name_for_trail,
                    'type' => $actname,
                    'uniquefiled1' => $uniquefiled1,
                    'uniquefiled2' => $uniquefiled2,
                    'uniquefiled3' => $uniquefiled3,
                    'table_name' => 'pts_trn_user_activity_log',
                    'primary_id' => $inserted_id,
                    'log_user_activity_id' => $inserted_id,
                    'post_params' => $data
                );
                $this->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */

                $proname = 'process log  = ' . $process_name . ' Activity = ' . $actid . ' name is ' . $actname;
                $log = 'where as product is :' . $product_no . ' and batch no is: ' . $batch_no;
                $action = 'Activity Successfully Started.';
                $createby = $empname;
                $oldval = "N/A";
                $newval = "N/A";

                $insertArray = array("terminalID" => $roomcode, "processname_master" => $proname, "activity_sub_task_actions" => $log, "action_performed" => $action, "created_by" => $createby, "oldval" => $oldval, "newval" => $newval);

                $this->db->insert("pts_audit_log", $insertArray);
            }
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }

        return ['actlogtblid' => $actlogtblid, 'doc_no' => $doc_no, 'message' => $message];
    }

    /**
     * @author Deepak Khare
     * function : euipAppartusLeaktest Log insert function with Transaction
     * Date: 22 Feb 2021
     */
    public function euipAppartusLeaktest($isworkflow, $tblname, $data, $data2, $data3) {
        $message = '';
        $doc_no = '';
        $actlogtblid = 0;
        $re_air_filter_tblid = 0;
        $inserted_id = 0;
        $activity_id = 0;
        try {

            $this->db->trans_start();
            $docResult = $this->getLastDocNo();
            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $this->updateLastDocNo($docno);
            }

            $data['doc_no'] = $doc_no;
            $data2['doc_id'] = $doc_no;
            if ($isworkflow == 0) {
                $this->db->insert($tblname, $data);
                $re_air_filter_tblid = $this->db->insert_id();
            } else {
                $data['is_in_workflow'] = 'yes';
                $this->db->insert($tblname, $data);
                $re_air_filter_tblid = $this->db->insert_id();
            }

            if ($re_air_filter_tblid > 0) {
                $this->db->insert("pts_trn_user_activity_log", $data2);
                $actlogtblid = $this->db->insert_id();
                $data3['usr_activity_log_id'] = $actlogtblid;
                $this->db->insert("pts_trn_activity_approval_log", $data3);
            }

            if ($actlogtblid > 0) {
                if ($isworkflow == 0) {
                    $inserted_id = $actlogtblid;
                    $activity_id = 0;
                } else {
                    $inserted_id = $re_air_filter_tblid;
                    $activity_id = $actlogtblid;
                }

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "equip_appratus_no:" . $data['equip_appratus_no'];
                $uniquefiled3 = "batch_no:" . $data['batch_no'];
                $auditParams = array('command_type' => 'insert', 'created_by' => $data['done_by_user_id'], 'created_by_name' => $data['done_by_user_name'], 'activity_name' => 'Equipment apparatus log register (leak test)', 'type' => 'sf_equipment_apparatus_leaktest', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_equip_appratus_leaktest', 'primary_id' => $inserted_id, 'log_user_activity_id' => $activity_id, 'post_params' => $data);
                $this->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */
            }

            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }

        return ['actlogtblid' => $actlogtblid, 'doc_no' => $doc_no, 'message' => $message];
    }

    /**
     * @author Deepak Khare
     * function : euip Appartus Log Record insert function with Transaction
     * Date: 22 Feb 2021
     */
    public function euipAppartusLogRecord($isworkflow, $tblname, $data, $data2, $data3) {
        $message = '';
        $doc_no = '';
        $actlogtblid = 0;
        $re_air_filter_tblid = 0;
        $inserted_id = 0;
        $activity_id = 0;
        try {
            $this->db->trans_start();
            $docResult = $this->getLastDocNo();
            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $this->updateLastDocNo($docno);
            }

            $data['doc_no'] = $doc_no;
            $data2['doc_id'] = $doc_no;
            if ($isworkflow == 0) {
                $this->db->insert($tblname, $data);
                $re_air_filter_tblid = $this->db->insert_id();
            } else {
                $data['is_in_workflow'] = 'yes';
                $this->db->insert($tblname, $data);
                $re_air_filter_tblid = $this->db->insert_id();
            }

            if ($re_air_filter_tblid > 0) {
                $this->db->insert("pts_trn_user_activity_log", $data2);
                $actlogtblid = $this->db->insert_id();
                $data3['usr_activity_log_id'] = $actlogtblid;
                $this->db->insert("pts_trn_activity_approval_log", $data3);
            }

            if ($actlogtblid > 0) {
                if ($isworkflow == 0) {
                    $inserted_id = $actlogtblid;
                    $activity_id = 0;
                } else {
                    $inserted_id = $re_air_filter_tblid;
                    $activity_id = $actlogtblid;
                }

                //Making equipment in used
                if ($data['equip_appratus_no'] != '' && $data['equip_appratus_no'] != NULL) {
                    $post = array('is_inused' => 1);
                    $param = array("equipment_code" => $data['equip_appratus_no']);
                    $this->updateToTable($db = 'default', 'pts_mst_instrument', $post, $param);
                }

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "equip_appratus_no:" . $data['equip_appratus_no'];
                $uniquefiled3 = "batch_no:" . $data['batch_no'];
                $auditParams = array('command_type' => 'insert', 'created_by' => $data['done_by_user_id'], 'created_by_name' => $data['done_by_user_name'], 'activity_name' => 'Equipment/Apparatus Log Register (Friabilator)', 'type' => 'equipment_apparatus_log_register', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_equip_appratus_log', 'primary_id' => $inserted_id, 'log_user_activity_id' => $activity_id, 'post_params' => $data);
                $this->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */
            }

            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }

        return ['actlogtblid' => $actlogtblid, 'doc_no' => $doc_no, 'message' => $message];
    }

    /*     * *
     * @desc Inserts into log table when a new activity for Balance Calibration Log is started
     * @param $tblname string, $data array $data2 array $data3 array
     * @author Nitin
     * @date 22-02-2021 
     */

    public function newFunctionToSubmitBalanceCalibrationLog($isworkflow, $data, $data2, $data3, $auditParams) {
        $activityId = '';
        $insertedId = '';
        $actlogtblid = 0;
        $doc_no = '';
        try {
            $this->db->trans_start();
            $docResult = $this->db->get_where("pts_mst_document", ['id' => '13'])->row_array();
            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $arr2 = ["docno" => $docno];
                $this->db->where("id", 13);
                $this->db->update("pts_mst_document", $arr2);
            }

            $workFlow = 'no';
            $dataArray = [];
            if ($isworkflow == 1) {
                $workFlow = 'yes';
            }
            foreach ($data as $d) {
                $d['doc_no'] = $doc_no;
                $d['is_in_workflow'] = $workFlow;
                $this->db->insert('pts_trn_balance_calibration', $d);
                $re_air_filter_tblid = $this->db->insert_id();
                $dataArray[] = $d;
            }

            $data2['doc_id'] = $doc_no;
            $this->db->insert("pts_trn_user_activity_log", $data2);
            $actlogtblid = $this->db->insert_id();

            $data3["usr_activity_log_id"] = $actlogtblid;
            $this->db->insert("pts_trn_activity_approval_log", $data3);
            $auditParams['primary_id'] = $re_air_filter_tblid;
            $auditParams['log_user_activity_id'] = $actlogtblid;
            $auditParams['post_params'] = $dataArray;
            $auditParams['uniquefiled1'] = "doc_no:" . $doc_no;
            $audit_id = $this->commonFunctionToAddAuditTransaction($auditParams);
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }
        return $actlogtblid;
    }

    /*     * *
     * @desc Inserts into log table when a new activity for Instrument Log is started
     * @param $tblname string, $data array $data2 array $data3 array
     * @author Nitin
     * @date 22-02-2021 
     */

    public function newFunctionToSubmitInstrumentLog($data, $data2, $data3, $auditParams) {
        $activityId = '';
        $insertedId = '';
        $actlogtblid = 0;
        try {
            $this->db->trans_start();
            $docResult = $this->db->get_where("pts_mst_document", ['id' => '13'])->row_array();
            $doc_no = '';
            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $arr2 = ["docno" => $docno];
                $this->db->where("id", 13);
                $this->db->update("pts_mst_document", $arr2);
            }

            $data['doc_no'] = $doc_no;
            $this->db->insert('pts_trn_instrument_log_register', $data);
            $re_air_filter_tblid = $this->db->insert_id();

            $data2['doc_id'] = $doc_no;
            $this->db->insert("pts_trn_user_activity_log", $data2);
            $actlogtblid = $this->db->insert_id();

            $data3["usr_activity_log_id"] = $actlogtblid;
            $this->db->insert("pts_trn_activity_approval_log", $data3);
            //Making equipment in used
            if (!empty($data['instrument_code'])) {
                $post = ['is_inused' => 1];
                $param = ["equipment_code" => $data['instrument_code']];
                $this->updateToTable($db = 'default', 'pts_mst_instrument', $post, $param);
            }
            $auditParams['primary_id'] = $re_air_filter_tblid;
            $auditParams['log_user_activity_id'] = $actlogtblid;
            $auditParams['post_params'] = $data;
            $auditParams['uniquefiled1'] = "doc_no:" . $doc_no;
            $audit_id = $this->commonFunctionToAddAuditTransaction($auditParams);
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }
        return $actlogtblid;
    }

    /*     * *
     * @desc Inserts into log table when a new activity for Pre Filter Cleaning Log is started
     * @param $tblname string, $data array $data2 array $data3 array
     * @author Nitin
     * @date 22-02-2021 
     */

    public function newFunctionToSubmitPreFilterCleaningRecord($data, $data2, $data3, $auditParams) {
        $activityId = '';
        $insertedId = '';
        $actlogtblid = 0;
        $doc_no = '';
        try {
            $this->db->trans_start();
            $docResult = $this->db->get_where("pts_mst_document", ['id' => '13'])->row_array();
            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $arr2 = ["docno" => $docno];
                $this->db->where("id", 13);
                $this->db->update("pts_mst_document", $arr2);
            }

            $data['doc_no'] = $doc_no;
            $this->db->insert('pts_trn_pre_filter_cleaning', $data);
            $re_air_filter_tblid = $this->db->insert_id();

            $data2['doc_id'] = $doc_no;
            $this->db->insert("pts_trn_user_activity_log", $data2);
            $actlogtblid = $this->db->insert_id();

            $data3["usr_activity_log_id"] = $actlogtblid;
            $this->db->insert("pts_trn_activity_approval_log", $data3);

            $auditParams['primary_id'] = $re_air_filter_tblid;
            $auditParams['log_user_activity_id'] = $actlogtblid;
            $auditParams['post_params'] = $data;
            $auditParams['uniquefiled1'] = "doc_no:" . $doc_no;
            $audit_id = $this->commonFunctionToAddAuditTransaction($auditParams);
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }
        return $actlogtblid;
    }

    /*     * *
     * @desc Inserts into log table when a new activity for Pre Filter Cleaning Log is started
     * @param $tblname string, $data array $data2 array $data3 array
     * @author Nitin
     * @date 22-02-2021 
     */

    public function newFunctionToSubmitPunchSetAllocationRecord($data, $data2, $data3, $auditParams) {
        $activityId = '';
        $insertedId = '';
        $actlogtblid = 0;
        $doc_no = '';
        try {
            $this->db->trans_start();
            $docResult = $this->db->get_where("pts_mst_document", ['id' => '13'])->row_array();

            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $arr2 = ["docno" => $docno];
                $this->db->where("id", 13);
                $this->db->update("pts_mst_document", $arr2);
            }

            $data['doc_no'] = $doc_no;
            $this->db->insert('pts_trn_punch_set_allocation', $data);
            $re_air_filter_tblid = $this->db->insert_id();

            $data2['doc_id'] = $doc_no;
            $this->db->insert("pts_trn_user_activity_log", $data2);
            $actlogtblid = $this->db->insert_id();

            $data3["usr_activity_log_id"] = $actlogtblid;
            $this->db->insert("pts_trn_activity_approval_log", $data3);

            $temp = [];
            for ($i = 1; $i <= $data['no_of_subset']; $i++) {
                $temp[] = ["product_code" => $data['product_code'], "punch_set" => $data['punch_set'], "punch_set_code" => $data['punch_set'] . '/' . $i, "created_by" => $data['done_by_user_id']];
            }
            if (!empty($temp)) {
                $this->db->insert_batch("pts_trn_product_base_punch_set", $temp);
            }
            $auditParams['primary_id'] = $re_air_filter_tblid;
            $auditParams['log_user_activity_id'] = $actlogtblid;
            $auditParams['post_params'] = $data;
            $auditParams['uniquefiled1'] = "doc_no:" . $doc_no;
            $audit_id = $this->commonFunctionToAddAuditTransaction($auditParams);
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }
        return $actlogtblid;
    }

    /*     * *
     * @desc Inserts into log table when a new activity for Tablet Tooling Record Log Without WorkFlow is started
     * @param $tblname string, $data array $data2 array $data3 array
     * @author Nitin
     * @date 22-02-2021 
     */

    public function newFunctionToSubmitTabletToolingRecordWithOutWorkFlow($data, $data2, $data3, $auditParam, $tt_actid, $Damagelist) {
        $activityId = '';
        $insertedId = '';
        $actlogtblid = 0;
        $doc_no = '';
        try {
            $this->db->trans_start();
            $docResult = $this->db->get_where("pts_mst_document", ['id' => '13'])->row_array();

            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $arr2 = ["docno" => $docno];
                $this->db->where("id", 13);
                $this->db->update("pts_mst_document", $arr2);
            }

            $data['doc_no'] = $doc_no;
            $this->db->insert('pts_trn_tablet_tooling', $data);
            $re_air_filter_tblid = $this->db->insert_id();

            $data2['doc_id'] = $doc_no;
            $this->db->insert("pts_trn_user_activity_log", $data2);
            $actlogtblid = $this->db->insert_id();

            $data3["usr_activity_log_id"] = $actlogtblid;
            $this->db->insert("pts_trn_activity_approval_log", $data3);

            $this->db->where(["product_code" => $data['product_code'], "punch_set" => $data['punch_set_number']]);
            $this->db->update("pts_trn_punch_set_allocation", ["act_id" => $tt_actid, "act_name" => $data['act_type'], "uld" => $data['U']]);
            if ($Damagelist != "") {
                if (!empty($Damagelist)) {
                    foreach ($Damagelist as $Damaged) {
                        $this->db->where(["product_code" => $data['product_code'], "punch_set" => $data['punch_set_number'], "id" => $Damaged]);
                        $this->db->update("pts_trn_product_base_punch_set", ["status" => "damaged"]);
                    }
                }
            }

            $auditParam['primary_id'] = $re_air_filter_tblid;
            $auditParam['log_user_activity_id'] = $actlogtblid;
            $auditParam['post_params'] = $data;
            $auditParam['uniquefiled1'] = "doc_no:" . $doc_no;
            $audit_id = $this->commonFunctionToAddAuditTransaction($auditParam);
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }
        return ['acttblid' => $actlogtblid, 'doc_no' => $doc_no];
    }

    /*     * *
     * @desc Inserts into log table when a new activity for Tablet Tooling Record Log With WorkFlow is started
     * @param $tblname string, $data array $data2 array $data3 array
     * @author Nitin
     * @date 22-02-2021 
     */

    public function newFunctionToSubmitTabletToolingRecordWithWorkFlow($data, $data2, $data3, $auditParam, $tt_actid, $Damagelist) {
        $activityId = '';
        $insertedId = '';
        $actlogtblid = 0;
        $doc_no = '';
        try {
            $this->db->trans_start();
            $docResult = $this->db->get_where("pts_mst_document", ['id' => '13'])->row_array();

            if (!empty($docResult)) {
                $docno = $docResult['docno'] + 1;
                $doc_pre = $docResult['prefix'];
                $doc_no = $doc_pre . $docno;
                $arr2 = ["docno" => $docno];
                $this->db->where("id", 13);
                $this->db->update("pts_mst_document", $arr2);
            }

            $data['doc_no'] = $doc_no;
            $this->db->insert('pts_trn_tablet_tooling', $data);
            $re_air_filter_tblid = $this->db->insert_id();

            $data2['doc_id'] = $doc_no;
            $this->db->insert("pts_trn_user_activity_log", $data2);
            $actlogtblid = $this->db->insert_id();

            $data3["usr_activity_log_id"] = $actlogtblid;
            $this->db->insert("pts_trn_activity_approval_log", $data3);

            $this->db->where(["product_code" => $data['product_code'], "punch_set" => $data['punch_set_number']]);
            $this->db->update("pts_trn_punch_set_allocation", ["act_id" => $tt_actid, "act_name" => $data['act_type']]);
            if ($Damagelist != "") {
                if (!empty($Damagelist)) {
                    foreach ($Damagelist as $Damaged) {
                        $this->db->where(["product_code" => $data['product_code'], "punch_set" => $data['punch_set_number'], "id" => $Damaged]);
                        $this->db->update("pts_trn_product_base_punch_set", ["status" => "damaged"]);
                    }
                }
            }

            if ($data['act_type'] == 'No. of Punch/Die Returned to Storage Cabinet') {
                $this->db->where("punch_set", $data['punch_set_number']);
                $this->db->where("status !=", "damaged");
                $this->db->update("pts_trn_product_base_punch_set", ["status" => 'available']);

                $this->db->where(["product_code" => $data['product_code'], "punch_set" => $data['punch_set_number']]);
                $this->db->update("pts_trn_punch_set_allocation", ["uld" => null]);
            }
            $auditParam['primary_id'] = $re_air_filter_tblid;
            $auditParam['log_user_activity_id'] = $actlogtblid;
            $auditParam['post_params'] = $data;
            $auditParam['uniquefiled1'] = "doc_no:" . $doc_no;
            $audit_id = $this->commonFunctionToAddAuditTransaction($auditParam);
            if ($this->db->trans_status() === FALSE) {
                # Something went wrong.
                $this->db->trans_rollback();
            } else {
                # Everything is Perfect. 
                # Committing data to the database.
                $this->db->trans_commit();
            }
            $this->db->trans_complete();
        } catch (Exception $e) {
            $this->writeOnExceptionFile($e);
        }
        return ['acttblid' => $actlogtblid, 'doc_no' => $doc_no];
    }

}
