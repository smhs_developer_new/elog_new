<?php

class Usermodel extends CI_Model {
	
	public function getDocid(){

		$this->db->limit(1);
		$this->db->order_by("doc_id","DESC");
		$q = $this->db->get('trn_solutiondetails');
		if($q->num_rows() > 0)
		{	$res = $q->row_array();
			$response = explode("WSP", $res["doc_id"]); 
			//$response = str_pad($response[0], 8, '0', STR_PAD_LEFT);
			$number = sprintf('%08d',intval($response[1])+1);
			$response = "WSP".$number;
			return $response;
		}	
		else
		{
			//$this->db->select("doc_id");
			//$this->db->where("id","7");
			//$query = $this->db->get("mst_document");
			$response = "WSP00000001";
			
			return $response;
		}	
	}

	// getting Activities for home page
	public function getActivitytype()
	{
		$this->db->distinct();
		$this->db->select("acttype.activitytype_name,acttype.id as id,acttype.activitytype_icon as activitytype_icon,acttype.activityfunction_tocall as activityfunction_tocall,acttype.activity_blockcolor as activity_blockcolor");
		$this->db->from("mst_activitytype as acttype");
		$this->db->join("trn_activityrole as actrole","acttype.id=actrole.activity_typeid","INNER");
		$this->db->where("actrole.role_id",$this->session->userdata("roleid"));
		$this->db->where('actrole.is_active',1);
		$this->db->where('actrole.block_code',$this->session->userdata('empblock_code'));
		$q= $this->db->get();
		return $q;
	}

	public function getASpendingoperationsforapproval2($statusid)
	{
		$this->db->where('status',2);
		$this->db->where('next_step',$statusid);
		$result = $this->db->get('trn_operationlogheader');
		if($result->num_rows() > 0)
		{	
			return $result;
		}
		else{
			return $result="";
		}		
	}

	public function getpendingoperationsforapproval($statusid)
	{
		
		$emparea_code = $this->session->userdata('emparea_code');
		$array = explode(",",$emparea_code);
		$query="(";
    	foreach ($array as $ar) {
    			$query = $query. "area_code = '".$ar."' OR ";
    	}
    	$query = substr($query, 0, -3);
    	$query = $query.")";

		$this->db->where($query);
		$this->db->where('next_step',$statusid);
		$result = $this->db->get('trn_operationlogheader');
		if($result->num_rows() > 0)
		{	
			//$response = $q->result_array();
			//return $response;
			return $result;
		}		
	}

	public function getpendingsolutionpreparationforapproval($statusid){
		$emparea_code = $this->session->userdata('emparea_code');
		$array = explode(",",$emparea_code);
		$query="(";
    	foreach ($array as $ar) {
    			$query = $query. "area_code = '".$ar."' OR ";
    	}
    	$query = substr($query, 0, -3);
    	$query = $query.")";

		$this->db->where($query);
		$this->db->where('next_step',$statusid);
		$this->db->where('is_active',1);
		$result = $this->db->get('trn_solutiondetails');
		if($result->num_rows() > 0)
		{	
			//$response = $q->result_array();
			//return $response;
			return $result;
		}
	}

	public function getpendingdrainpointforapproval($statusid){
		$emparea_code = $this->session->userdata('emparea_code');
		$array = explode(",",$emparea_code);
		$query="(";
    	foreach ($array as $ar) {
    			$query = $query. "area_code = '".$ar."' OR ";
    	}
    	$query = substr($query, 0, -3);
    	$query = $query.")";

		$this->db->where($query);
		$this->db->where('next_step',$statusid);
		$this->db->where('is_active',1);
		$result = $this->db->get('trn_drainpointdetails');
		if($result->num_rows() > 0)
		{	
			//$response = $q->result_array();
			//return $response;
			return $result;
		}
	}

	public function getpendingdailycleaningforapproval($statusid){
		$emparea_code = $this->session->userdata('emparea_code');
		$array = explode(",",$emparea_code);
		$query="(";
    	foreach ($array as $ar) {
    			$query = $query. "area_code = '".$ar."' OR ";
    	}
    	$query = substr($query, 0, -3);
    	$query = $query.")";

		$this->db->where($query);
		$this->db->where('next_step',$statusid);
		$this->db->where('is_active',1);
		$result = $this->db->get('trn_dailycleaning');
		if($result->num_rows() > 0)
		{	
			//$response = $q->result_array();
			//return $response;
			return $result;
		}
	}

	public function getpendinginprocesscleaningforapproval($statusid){
		$emparea_code = $this->session->userdata('emparea_code');
		$array = explode(",",$emparea_code);
		$query="(";
    	foreach ($array as $ar) {
    			$query = $query. "area_code = '".$ar."' OR ";
    	}
    	$query = substr($query, 0, -3);
    	$query = $query.")";

		$this->db->where($query);
		$this->db->where('next_step',$statusid);
		$this->db->where('is_active',1);
		$result = $this->db->get('trn_inprocesscontainercleaning');
		if($result->num_rows() > 0)
		{	
			//$response = $q->result_array();
			//return $response;
			return $result;
		}
	}

	public function getpendaccessorycleaningforapproval($statusid){
		$emparea_code = $this->session->userdata('emparea_code');
		$array = explode(",",$emparea_code);
		$query="(";
    	foreach ($array as $ar) {
    			$query = $query. "area_code = '".$ar."' OR ";
    	}
    	$query = substr($query, 0, -3);
    	$query = $query.")";

		$this->db->where($query);
		$this->db->where('next_step',$statusid);
		$this->db->where('is_active',1);
		$result = $this->db->get('trn_accessorycleaning');
		if($result->num_rows() > 0)
		{	
			//$response = $q->result_array();
			//return $response;
			return $result;
		}
	}

	public function getpendportableforapproval($statusid){
		$emparea_code = $this->session->userdata('emparea_code');
		$array = explode(",",$emparea_code);
		$query="(";
    	foreach ($array as $ar) {
    			$query = $query. "area_code = '".$ar."' OR ";
    	}
    	$query = substr($query, 0, -3);
    	$query = $query.")";

		$this->db->where($query);
		$this->db->where('next_step',$statusid);
		$this->db->where('is_active',1);
		$result = $this->db->get('trn_portabledetail');
		if($result->num_rows() > 0)
		{	
			//$response = $q->result_array();
			//return $response;
			return $result;
		}
	}

	public function getQApendingoperationsforapproval()
	{
		$this->db->where('status',3);
		$this->db->where('next_step',4);
		$result = $this->db->get('trn_operationlogheader');
		if($result->num_rows() > 0)
		{	
			//$response = $q->result_array();
			//return $response;
			return $result;
		}		
	}

	public function getPMpendingoperationsforapproval()
	{
		$this->db->where('status',4);
		$this->db->where('next_step',0);
		$result = $this->db->get('trn_operationlogheader');
		if($result->num_rows() > 0)
		{	
			//$response = $q->result_array();
			//return $response;
			return $result;
		}		
	}

	public function getActivitytype2()
	{
		$q = $this->db->get('mst_activitytype');
		if($q->num_rows() > 0)
		{	
			//$response = $q->result_array();
			//return $response;
			return $q;
		}		
	}

	public function getActivities()
	{
		$q = $this->db->query('Select distinct activity_code, activity_name from mst_activity');
		//$q = $this->db->get('mst_activity');
		if($q->num_rows() > 0)
		{	
			return $q;
		}		
	}

	public function getRole()
	{

		$q = $this->db->get('mst_role');
		if($q->num_rows() > 0)
		{	
			return $q;
		}		
	}

	public function getActivitiesforworkflow()
	{
		//$this->db->query('Select distinct act.activity_code, act.activity_name');
		$this->db->distinct();
		$this->db->select("act.activity_code, act.activity_name, act.activity_icon");
		$this->db->from("mst_activity as act");
		$this->db->join("mst_area as a","act.area_code=a.area_code","INNER");
		$this->db->where('act.is_active',1);
		$this->db->where('a.is_active',1);
		//$this->db->where('a.block_code',$this->session->userdata('empblock_code'));
		$q= $this->db->get();
		if($q->num_rows() > 0)
		{	
			return $q;
		}		
	}

	public function getRoleforworkflow()
	{

		$q = $this->db->get('mst_role');
		if($q->num_rows() > 0)
		{	
			return $q;
		}		
	}

	public function GetDocumentId($trntblname,$trncolname,$msttblname,$getcolname,$searchcolname,$val){

		$this->db->limit(1);
		$this->db->order_by($trncolname,"DESC");		
		$q = $this->db->get($trntblname);
		if($q->num_rows() > 0)
		{	

			$res = $q->row_array();
			$sbr = substr($res[$trncolname],0, 3);
			$response = explode($sbr, $res[$trncolname]); 
			$number = sprintf('%08d',intval($response[1])+1);
			$response = $sbr.$number;
			return $response;
		}	
		else
		{
			$this->db->select($getcolname);
			$this->db->where($searchcolname,$val);
			$query = $this->db->get($msttblname);
			$res = $query->row_array();

			$sbr = substr($res[$getcolname],0, 3);
			$response = explode($sbr, $res[$getcolname]); 
			$number = sprintf('%08d',intval($response[1])+1);
			$response = $sbr.$number;
			return $response;
		}	
	}

	public function sec_fun_GetDocumentId($trntblname,$trncolname,$msttblname,$getcolname,$searchcolname,$val){

		$this->db->limit(1);
		$this->db->order_by($trncolname,"DESC");
		if($this->session->userdata('activityType')=="operation" || $this->session->userdata('activityType')=="type A" || $this->session->userdata('activityType')=="type B")
		{
			$this->db->where('operation_type',$this->session->userdata('activityType'));
		}
		$q = $this->db->get($trntblname);
		if($q->num_rows() > 0)
		{	

			$res = $q->row_array();
			$sbr = substr($res[$trncolname],0, 3);
			$response = explode($sbr, $res[$trncolname]); 
			$number = sprintf('%08d',intval($response[1])+1);
			$response = $sbr.$number;
			return $response;
		}	
		else
		{
			$this->db->select($getcolname);
			$this->db->where($searchcolname,$val);
			$query = $this->db->get($msttblname);
			$res = $query->row_array();

			if($res>0)
			{
			$sbr = substr($res[$getcolname],0, 3);
			$response = explode($sbr, $res[$getcolname]); 
			$number = sprintf('%08d',intval($response[1])+1);
			$response = $sbr.$number;
			return $response;
			}
		}	
	}

	public function getAllactivities($role_id){
			$this->db->where("role_id",$role_id);
			$query = $this->db->get("trn_workflowsteps");
			$res = $query->result_array();
			return $res;
	}

	/*workflow Delete*/
	public function deleterecords($act)
	{
	$this->db->query("delete  from trn_workflowsteps where activity_id='".$act."'");
	return $this->db->affected_rows() > 0;
	}
	public function getLogsview($data){
		if($data['aid']==27){
		$tablename = "trn_solutiondetails";
		$this->db->select("*,doc_id as document_no,solution_destroy_checked_by as checked_by");	
		$this->db->where("doc_id",$data['rid']);	
		}
		else if($data['aid']==29){
		$this->db->select("*,start_time as created_on,end_time as checked_on");	 	
		$tablename = "trn_drainpointdetails";	
		$this->db->where("document_no",$data['rid']);	
		}
		else if($data['aid']==30){
		$this->db->select("*,start_time as created_on,end_time as checked_on");
		$tablename = "trn_dailycleaning";	
		$this->db->where("document_no",$data['rid']);	
		}
		else if($data['aid']==4){
		$this->db->select("*,start_time as created_on,end_time as checked_on");		
		$tablename = "trn_inprocesscontainercleaning";	
		$this->db->where("document_no",$data['rid']);	
		}
		else if($data['aid']==26){
		$this->db->select("*,start_time as created_on,stop_time as checked_on");	
		$tablename = "trn_accessorycleaning";	
		$this->db->where("document_no",$data['rid']);	
		}
		else if($data['aid']==5){
		//$this->db->select("*,start_time as created_on,stop_time as checked_on");	
		$tablename = "trn_portabledetail";	
		$this->db->where("document_no",$data['rid']);	
		}
		//$this->db->where("role_id",1);
		$query = $this->db->get($tablename);
			$res = $query->result_array();
			return $res;
	}


	public function getequipmentdtlnow($data){
		$this->db->select("*,peq.id as ids");
		$this->db->from("trn_portableequipmentdetail as peq");
		$this->db->join("mst_equipment as eq","eq.equipment_code=peq.equipment_code","INNER");
		$this->db->join("mst_sop as sop","sop.sop_code=peq.sop_code","INNER");
		$this->db->where("peq.document_portable_id",$data['rid']);
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	}

	public function getworkflow(){
		$this->db->distinct();
		$this->db->select("wf.activity_id,DATE_FORMAT(wf.created_on,'%d-%m-%Y') as created_on,wf.created_by,emp.emp_name,act.activity_name");
		$this->db->from("trn_workflowsteps as wf");
		$this->db->join("mst_employee as emp","wf.created_by=emp.emp_code","INNER");
		$this->db->join("mst_activity as act","act.activity_code=wf.activity_id","INNER");
		$this->db->where("wf.is_active",1);
		$query = $this->db->get();
		return $query;
	}

	public function getworkflowbyactid($actid){
		$this->db->select("wf.id,wf.activity_id,wf.role_id,DATE_FORMAT(wf.created_on,'%d-%m-%Y') as created_on,wf.created_by,role.role_description");
		$this->db->from("trn_workflowsteps as wf");
		$this->db->join("mst_role as role","wf.role_id=role.id","INNER");
		$this->db->where("wf.activity_id",$actid);
		$this->db->where("wf.is_active",1);
		$this->db->where("wf.status_id>",1);
		$query = $this->db->get();
		return $query;
	}

}