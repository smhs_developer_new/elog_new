<?php

class Loginmodel extends CI_Model {
	
	public function userauth($userid, $pwd,$userCheck = 0)
	{
            if($userCheck==0){
		$this->db->where('emp_email',$userid);
		$this->db->where('emp_password',$pwd);
		$this->db->where('is_active',1);
		$q = $this->db->get('mst_employee');
		return $q;
            }else{
                $this->db->where('emp_email',$userid);
		//$this->db->where('emp_password',$pwd);
		$this->db->where('is_active',1);
		$q = $this->db->get('mst_employee');
                return $q;
            }
	}

	public function getdepartmentname($area_code){
		$this->db->where("area_code",$area_code);
		$this->db->where('is_active',1);
		$query = $this->db->get("mst_department");
		$res = $query->row_array();
		return $res["department_name"];
	}

	public function getblock($area_code){
		$this->db->where("area_code",$area_code);
		$this->db->where('is_active',1);
		$query = $this->db->get("mst_area");
		$res = $query->row_array();
		return $res["block_code"];
	}

	public function Blockeduser($tblname,$wherecol,$whereval)
	{
		$arr = array('is_blocked'=>1,'block_datetime'=>date('Y-m-d H:i:s'));
		$this->db->where($wherecol,$whereval);
		$this->db->update($tblname,$arr);
		$AR = $this->db->affected_rows();
		return $AR;	
	}
        
        public function getRoleDetailById($role_id)
	{
                $this->db->where("id",$role_id);
		$q = $this->db->get('mst_role');
		if($q->num_rows() > 0)
		{	
			return $q->row_array();
		}		
	}
        
        public function adduserloginhistory($user_data)
        {
            $this->db->insert('pts_user_login_history',$user_data);
        }

        public function updateUserloginhistory()//sets logout time for logging out user
        {
            $arr = array('logout_time'=>date('Y-m-d H:i:s'));
            $this->db->where('session_id',$this->session->session_id);
            $this->db->update('pts_user_login_history',$arr);
        }
        public function checkUserExist($emp_email)//sets logout time for logging out user
        {
            $user_name = [];
            $this->db->select('id,emp_email,emp_name,is_directory');
            $this->db->where('emp_email',$emp_email);
            $q = $this->db->get('mst_employee')->row_array();
            if(!empty($q)){
                $user_name = $q;
            }
            return $user_name;
        }
}