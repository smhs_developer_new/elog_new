<?php

class Injectablemodel extends CI_Model {

public function getProductnames($keyword){
	$this->db->like('product_name', $keyword);
    $this->db->where("is_active",1);
    $dev = $this->db->get("mst_product");
    foreach ($dev->result_array() as $eq) {
      $response[] = $eq["product_name"];
    }
	return $response;
}

public function getProductlotno($keyword){
	$this->db->select("ba.batch_code as batch_code");
	$this->db->from("mst_product as pr");
	$this->db->join("trn_batch as ba","ba.product_code=pr.product_code","INNER");
	$this->db->where('pr.product_name', $keyword);
	//$this->db->where("is_active",1);
	$dev = $this->db->get();
	$response = $dev->row_array();
	return $response;
}

public function getEquipments($keyword){
	$this->db->like('equipment_name', $keyword); 	
    $this->db->where("equipment_remarks","Injectibles");
    $this->db->where("is_active",1);
    $dev = $this->db->get("mst_equipment");
    foreach ($dev->result_array() as $eq) {
      $response[] = $eq["equipment_name"];
    }
	return $response;
}

public function getEquipmentsval($keyword){
	$this->db->where('equipment_name', $keyword);
	$this->db->where("equipment_remarks","Injectibles");
	$this->db->where("is_active",1);
	$dev = $this->db->get("mst_equipment");
	$response = $dev->row_array();
	return $response;
}

public function checkAuth($data){
	if($data["userid"]!=$this->session->userdata('empemail')){
	$this->db->where("emp_email",$data["userid"]);
	$this->db->where("emp_password",$data["password"]);
	$this->db->where("is_active",1);
	$query = $this->db->get("mst_employee");
	if($query->num_rows() == 1){
		$response = array("status"=>1,"data"=>$query->row_array());
	}
	else{
		$response = array("status"=>0,"msg"=>"Invalid userid/password !");
	}
	}
	else{
		$response = array("status"=>0,"msg"=>"Invalid Checkby. The same operator cannot check");
	}
	return $response;
}

public function equipmentcleaningSubmit($data){
$array = array("lot_no"=>$data["lotno"],"sop_no"=>$data["sopno"],"equipmentname"=>$data["equipmentname"],"code_no"=>$data["codeno"],"cleaning_status"=>$data["cleaningstatus"],"checked_by"=>$data["ucode"]);

if($this->db->insert("trn_injequipmentdetails",$array)){
$this->db->where("lot_no",$data["lotno"]);
$this->db->where("code_no",$data["codeno"]);
$this->db->where("checked_by",$data["ucode"]);
$this->db->where("is_active",1);
$this->db->order_by("id","DESC");
$this->db->limit(1);
$query = $this->db->get("trn_injequipmentdetails");
$response = array("status"=>1,"data"=>$query->row_array());
}
else{
$response = array("status"=>0);	
}
return $response;
}


public function solutionNames($keyword){
$this->db->group_start();
$this->db->like('solution_name', $keyword);
$this->db->or_like('Short_Name', $keyword);
$this->db->group_end();
//$this->db->where("Block_Code",$this->session->userdata('empblock_code'));
$this->db->where("is_active",1);
$dev = $this->db->get("mst_inj_solution");
foreach ($dev->result_array() as $sn) {
  $response[] = $sn["solution_name"];
}
return $response;
}

public function getSolutionval($keyword){
$this->db->where('solution_name', $keyword);
//$this->db->where("Block_Code",$this->session->userdata('empblock_code'));
$this->db->where("is_active",1);
$dev = $this->db->get("mst_inj_solution");
$response = $dev->row_array();
return $response;	
}


public function submitPreparation($data){
	$this->db->order_by("id","DESC");
	$this->db->limit(1);
	$query = $this->db->get("trn_injequipmentdetails");
	$res = $query->row_array();

	$array = array("form_id"=>$res["id"],"doc_id"=>$res["id"],"solutionname"=>$data["solutionname"],"required_quantity"=>$data["requiredquantity"],"standard_water_qty"=>$data["sqwfi"],"Standard_hydrogen_qty"=>$data["sqhp"],"arn_water"=>$data["warno"],"wfi_makeup_volume"=>$data["sqqs"],"actual_water_qty"=>$data["wati"],"uom_water"=>$data["wauom"],"actual_hydrogen_qty"=>$data["hpq"],"arn_hydrogen"=>$data["hparno"],"uom_hydrogen"=>$data["hpauom"],"actual_wfi_makeup_volume"=>$data["wfiq"],"arn_wfi"=>$data["wfarno"],"expiry_datatime"=>$data["ubdatetime"],"uom_wfi"=>$data["wfauom"],"created_by"=>$this->session->userdata('empcode'));

	if($this->db->insert("trn_injsolutiondetails",$array)){
	$response = array("status"=>1);
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}


public function stopPreparation(){
	$this->db->order_by("id","DESC");
	$this->db->limit(1);
	$query = $this->db->get("trn_injsolutiondetails");
	$res = $query->row_array();


	$array = array("stop_by"=>$this->session->userdata('empcode'),"stop_on"=>date("Y-m-d H:i:s"));

	$this->db->where("id",$res["id"]);
	if($this->db->update("trn_injsolutiondetails",$array)){
	$response = array("status"=>1);
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

public function setrilizationSubmit($data){
	$this->db->order_by("id","DESC");
	$this->db->limit(1);
	$query = $this->db->get("trn_injequipmentdetails");
	$res = $query->row_array();

	$array = array("form_id"=>$res["id"],"equipmentname"=>$data["equipmentname_steri"],"code_no"=>$data["codeno_steri"],"sterilizer_no"=>$data["sterilizerno"],"run_no"=>$data["runno"],"ref_graph_no"=>$data["graphno"],"created_on"=>$data["createdon"],"from_time"=>$data["fromtime"],"to_time"=>$data["totime"],"checked_by"=>$data["ucode_steri"]);

	if($this->db->insert("trn_injsterilizationdetails",$array)){
	$response = array("status"=>1);
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

public function filtrationSubmit($data){
	$this->db->order_by("id","DESC");
	$this->db->limit(1);
	$query = $this->db->get("trn_injequipmentdetails");
	$res = $query->row_array();

	$array = array("form_id"=>$res["id"],"lot_no"=>$data["lotno"],"pre_limit"=>$data["prelimit"],"collection_fromtime"=>$data["fil_fromtime"],"collection_totime"=>$data["fil_totime"],"post_limit"=>$data["postlimit"]);

	if($this->db->insert("trn_injfiltrationdetails",$array)){
	$response = array("status"=>1);
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}


public function filtrationCheckby($data){
	$this->db->order_by("id","DESC");
	$this->db->limit(1);
	$query = $this->db->get("trn_injfiltrationdetails");
	$res = $query->row_array();

	$array = array("checked_by"=>$data["ucode_fil"]);
	$this->db->where("id",$res["id"]);
	if($this->db->update("trn_injfiltrationdetails",$array)){
	$response = array("status"=>1);
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}


public function submitCleaning($data){
	/*$this->db->order_by("id","DESC");
	$this->db->limit(1);
	$query = $this->db->get("trn_injequipmentdetails");
	$res = $query->row_array();*/

	$array = array("equipmentcode"=>$data["cleaningequipmentcode"],"sop_no"=>"PAR-064","cleaning_start"=>date("Y-m-d H:i:s"),"clean_by"=>$this->session->userdata('empcode').' - '.$this->session->userdata('empname'));

	if($this->db->insert("trn_injpar64details",$array)){

	/*$this->db->order_by("id","DESC");
	$this->db->limit(1);
	$query = $this->db->get("trn_injpar64details");
	$res = $query->row_array();*/
	$this->db->where("id",$this->db->insert_id());
	$query = $this->db->get("trn_injpar64details");
	$response = array("status"=>1,"logs"=>$query->row_array());
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

public function stopCleaning(){
	$this->db->order_by("id","DESC");
	$this->db->limit(1);
	$query = $this->db->get("trn_injpar64details");
	$res = $query->row_array();


	$array = array("cleanstop_by"=>$this->session->userdata('empcode').' - '.$this->session->userdata('empname'),"cleaning_stop"=>date("Y-m-d H:i:s"));

	$this->db->where("id",$res["id"]);
	if($this->db->update("trn_injpar64details",$array)){
	$this->db->where("id",$res["id"]);
	$query = $this->db->get("trn_injpar64details");
	$response = array("status"=>1,"logs"=>$query->row_array());
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

public function cleaningCheckby($data){
	$this->db->order_by("id","DESC");
	$this->db->limit(1);
	$query = $this->db->get("trn_injpar64details");
	$res = $query->row_array();


	$array = array("clean_remarks"=>$data['cleanremarks'],"clean_checkby"=>$data['ucode']);

	$this->db->where("id",$res["id"]);
	if($this->db->update("trn_injpar64details",$array)){
	//$this->db->join('mst_employee','trn_injpar64details.cleanstop_by = mst_employee.employee_code', 'left');
	$this->db->where("id",$res["id"]);
	$query = $this->db->get("trn_injpar64details");
	$response = array("status"=>1,"logs"=>$query->row_array());
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

public function submitOperation($data){
	/*$this->db->order_by("id","DESC");
	$this->db->limit(1);
	$query = $this->db->get("trn_injequipmentdetails");
	$res = $query->row_array();*/

	$array = array("equipmentcode"=>$data["proequipmentcode"],"sop_no"=>"PAR-064", "op_productname"=>$data['productname'],"op_lotno"=>$data['productlotno'],"op_start"=>date("Y-m-d H:i:s"),"op_by"=>$this->session->userdata('empcode').' - '.$this->session->userdata('empname'));

	if($this->db->insert("trn_injpar64details",$array)){
	$this->db->where("id",$this->db->insert_id());
	$query = $this->db->get("trn_injpar64details");
	$response = array("status"=>1,"logs"=>$query->row_array());
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

public function stopOperation(){
	$this->db->order_by("id","DESC");
	$this->db->limit(1);
	$query = $this->db->get("trn_injpar64details");
	$res = $query->row_array();


	$array = array("opstop_by"=>$this->session->userdata('empcode').' - '.$this->session->userdata('empname'),"op_stop"=>date("Y-m-d H:i:s"));

	$this->db->where("id",$res["id"]);
	if($this->db->update("trn_injpar64details",$array)){
	$this->db->where("id",$res["id"]);
	$query = $this->db->get("trn_injpar64details");
	$response = array("status"=>1,"logs"=>$query->row_array());
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

public function confirmOutput($data){
	$this->db->order_by("id","DESC");
	$this->db->limit(1);
	$query = $this->db->get("trn_injpar64details");
	$res = $query->row_array();


	$array = array("op_output"=>$data['opoutput']);

	$this->db->where("id",$res["id"]);
	if($this->db->update("trn_injpar64details",$array)){
	$this->db->where("id",$res["id"]);
	$query = $this->db->get("trn_injpar64details");
	$response = array("status"=>1,"logs"=>$query->row_array());
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}


public function operationCheckby($data){
	$this->db->order_by("id","DESC");
	$this->db->limit(1);
	$query = $this->db->get("trn_injpar64details");
	$res = $query->row_array();


	$array = array("op_remarks"=>$data['opremarks'],"op_checkby"=>$data['ucode_steri']);

	$this->db->where("id",$res["id"]);
	if($this->db->update("trn_injpar64details",$array)){
	$this->db->where("id",$res["id"]);
	$query = $this->db->get("trn_injpar64details");
	$response = array("status"=>1,"logs"=>$query->row_array());
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

public function breakStart(){
	$this->db->order_by("id","DESC");
	$this->db->limit(1);
	$query = $this->db->get("trn_injpar64details");
	$res = $query->row_array();


	$array = array("break_fromtime"=>date("Y-m-d H:i:s"));

	$this->db->where("id",$res["id"]);
	if($this->db->update("trn_injpar64details",$array)){
	$this->db->where("id",$res["id"]);
	$query = $this->db->get("trn_injpar64details");
	$response = array("status"=>1,"logs"=>$query->row_array());
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

public function breakdownCheckby($data){
	$this->db->order_by("id","DESC");
	$this->db->limit(1);
	$query = $this->db->get("trn_injpar64details");
	$res = $query->row_array();


	$array = array("break_totime"=>date("Y-m-d H:i:s"),"break_remarks"=>$data['breakremarks'],"break_checkby"=>$data['ucode_break']);

	$this->db->where("id",$res["id"]);
	if($this->db->update("trn_injpar64details",$array)){
	$response = array("status"=>1);
	}
	else{
	$response = array("status"=>0);	
	}
	return $response;
}

}	


