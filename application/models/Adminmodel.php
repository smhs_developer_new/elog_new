<?php

date_default_timezone_set('Asia/Kolkata');
$date = date("Y-m-d H:i:s");

class Adminmodel extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->model('Ptscommon');
    }

    public function insertActivityarea($data) {
        $this->db->select("act.id as actid,act.activity_code,ar.area_code,b.block_code");
        $this->db->from("mst_activity as act");
        $this->db->join("mst_area as ar", "act.area_code=ar.area_code", "INNER");
        $this->db->join("mst_block as b", "ar.block_code=b.block_code", "INNER");
        $this->db->where("ar.`block_code`", $data['block']);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach ($q->result_array() as $sn) {
                //Deleting previous Records
                $this->db->where('id', $sn["actid"]);
                $this->db->where('area_code !=', "A-000");
                $this->db->delete('mst_activity');
                //End Deleting     				 	
            }
        }

        $i = 1;
        foreach ($data['activity'] as $a) {
            if (isset($data['area' . $i])) {
                $actdata = explode("##", $a);
                $activity_code = $actdata[0];
                $activity_name = $actdata[1];
                $activitytype_id = $actdata[2];
                $activity_type = $actdata[3];
                $activityfunc_tocall = $actdata[4];
                $activity_icon = $actdata[5];
                $activity_remark = $actdata[6];

                $mstarea = $data['area' . $i];
                $cby = $this->session->userdata('empname');
                $error = 0;

                foreach ($data['area' . $i] as $area) {
                    $areadata = explode("##", $area);
                    $areacode = $areadata[0];
                    $areaname = $areadata[1];

                    // Inserting New Records area vise
                    $arr = array('activity_code' => $activity_code, 'activity_name' => $activity_name, 'activitytype_id' => $activitytype_id, 'activity_type' => $activity_type, 'activityfunc_tocall' => $activityfunc_tocall, 'activity_icon' => $activity_icon, 'activity_remark' => $activity_remark, 'area_code' => $areacode, 'created_by' => $cby, 'modified_by' => $cby);

                    if ($this->db->insert('mst_activity', $arr)) {
                        $error = 1;
                    }
                    //End Inserting
                }
            }
            /* else
              {
              $actdata = explode("##",$a);
              $activity_code = $actdata[0];
              $activity_name = $actdata[1];
              $activitytype_id = $actdata[2];
              $activity_type = $actdata[3];
              $activityfunc_tocall = $actdata[4];
              $activity_icon = $actdata[5];
              $activity_remark = $actdata[6];
              $cby = $this->session->userdata('empname');

              // Inserting New Records area vise
              $arr2 = array('activity_code'=>$activity_code,'activity_name'=>$activity_name,'activitytype_id'=>$activitytype_id,'activity_type'=>$activity_type,'activityfunc_tocall'=>$activityfunc_tocall,'activity_icon'=>$activity_icon,'activity_remark'=>$activity_remark,'area_code'=>" ",'created_by'=>$cby,'modified_by'=>$cby);

              if ($this->db->insert('mst_activity',$arr2)) {
              $error = 1;
              }
              //End Inserting
              } */
            $i++;
        }


        if ($error == 1) {
            $response = array("status" => 1, "msg" => "Successfully Inserted", "msgheader" => "Successfully Inserted");
        } else {
            $response = array("status" => 0);
        }

        return $response;
    }

    public function getActivities() {
        $where = '(activitytype_id=14 or activitytype_id = 17 or activitytype_id=18 or activitytype_id=20)';

        $this->db->group_by('activity_code');
        $this->db->where($where);
        $q = $this->db->get('mst_activity');
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
    }

    public function getAreaformap() {
        $this->db->where("area_code !=", "A-000");
        $q = $this->db->get('mst_area');
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
    }

    public function getareabyblock($block) {
        $this->db->where("area_code !=", "A-000");
        $this->db->where("block_code", $block);
        $q = $this->db->get('mst_area');
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
    }

    public function getArea() {
        $q = $this->db->get('mst_area');
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
    }

    public function sA($actcode) {
        $this->db->where("activity_code", $actcode);
        $dd2 = $this->db->get("mst_activity");
        $response = array("ndata" => $dd2->result_array());
        return $response;
    }

    public function get_activityBytypeid($col1, $val1, $tblname) {
        $this->db->where($col1, $val1);
        $this->db->where('is_active', 1);
        $this->db->order_by("activity_code", "ASC");
        $q = $this->db->get($tblname);
        return $q;
    }

    /* Common Delete function */

    public function deleterecords($col, $val, $deleteby, $deletetime, $tblname) {
        $arr2 = array("is_active" => 0, "modified_by" => $deleteby, "modified_on" => $deletetime);
        $this->db->where($col, $val);
        if ($this->db->update($tblname, $arr2)) {
            $response = $this->db->affected_rows();
        } else {
            $response = $this->db->affected_rows();
        }

        return $response;
    }

    /* End Common Delete Function */

    //Function to insert records by Bhupendra

    public function insert17para_with_tblnama($tblname, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7, $col8, $val8) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7, $col8 => $val8);
        $this->db->insert($tblname, $arr);
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function insert19para_with_tblnama($tblname, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7, $col8, $val8, $col9, $val9) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7, $col8 => $val8, $col9 => $val9);
        $this->db->insert($tblname, $arr);
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function insert5para_with_tblnama($tblname, $col1, $val1, $col2, $val2) {
        $arr = array($col1 => $val1, $col2 => $val2);
        $this->db->insert($tblname, $arr);
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function insert7para_with_tblnama($tblname, $col1, $val1, $col2, $val2, $col3, $val3) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3);
        $this->db->insert($tblname, $arr);
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function insert9para_with_tblnama($tblname, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4);
        $this->db->insert($tblname, $arr);
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function insert11para_with_tblnama($tblname, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5);
        $this->db->insert($tblname, $arr);
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function insert13para_with_tblnama($tblname, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6);
        $this->db->insert($tblname, $arr);
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function insert15para_with_tblnama($tblname, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7);
        $this->db->insert($tblname, $arr);
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function insert21para_with_tblnama($tblname, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7, $col8, $val8, $col9, $val9, $col10, $val10) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7, $col8 => $val8, $col9 => $val9, $col10 => $val10);
        $this->db->insert($tblname, $arr);
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function insert23para_with_tblnama($tblname, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7, $col8, $val8, $col9, $val9, $col10, $val10, $col11, $val11) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7, $col8 => $val8, $col9 => $val9, $col10 => $val10, $col11 => $val11);
        $this->db->insert($tblname, $arr);
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function insert25para_with_tblnama($tblname, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7, $col8, $val8, $col9, $val9, $col10, $val10, $col11, $val11, $col12, $val12) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7, $col8 => $val8, $col9 => $val9, $col10 => $val10, $col11 => $val11, $col12 => $val12);
        $this->db->insert($tblname, $arr);
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function insert27para_with_tblnama($tblname, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7, $col8, $val8, $col9, $val9, $col10, $val10, $col11, $val11, $col12, $val12, $col13, $val13) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7, $col8 => $val8, $col9 => $val9, $col10 => $val10, $col11 => $val11, $col12 => $val12, $col13 => $val13);
        $this->db->insert($tblname, $arr);
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function insert29para_with_tblnama($tblname, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col8, $val8, $col9, $val9, $col10, $val10, $col11, $val11, $col12, $val12, $col13, $val13, $col14, $val14, $col15, $val15, $col16, $val16, $col17, $val17, $col18, $val18) {
        if($col16==1){
            $expire_on = NULL;
        }else{
            $expire_on = date('Y-m-d H:i:s', strtotime("+60 days"));
        }
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col8 => $val8, $col9 => $val9, $col10 => $val10, $col11 => $val11, $col12 => $val12, $col13 => $val13, $col14 => $val14, "expire_on" => $expire_on, $col15 => $val15, $col16 => $val16, $col17 => $val17, $col18 => $val18);
        $this->db->insert($tblname, $arr);
        $last_id = $this->db->insert_id();
//        $this->db->insert("mst_employee_password_history", array("user_id" => $last_id, "password" => $val7, "created_by" => $this->session->userdata('user_id')));
        return $last_id;
    }

    //End function to insert records
    //Function to get records by Bhupendra
    public function get_dtl6($colname, $colval, $colname2, $colval2, $colname3, $colval3, $tblname) {
        $this->db->where($colname, $colval);
        $this->db->where($colname2, $colval2);
        $this->db->where($colname3, $colval3);
        $this->db->where('is_active', 1);
        $this->db->order_by("created_on", "DESC");
        $q = $this->db->get($tblname);
        return $q;
    }

    public function get_dtl3($id, $colname, $colval, $tblname) {
        $this->db->where('id', $id);
        $this->db->where($colname, $colval);
        $this->db->where('is_active', 1);
        $this->db->order_by("created_on", "DESC");
        $q = $this->db->get($tblname);
        return $q;
    }

    public function get_dtl2($colname, $colval, $tblname) {
        $this->db->where($colname, $colval);
        $this->db->where('is_active', 1);
        $this->db->order_by("created_on", "DESC");
        $q = $this->db->get($tblname);
        return $q;
    }

    public function get_detail2($colname, $colval, $tblname) {
        $this->db->where($colname, $colval);
        $this->db->where('is_active', 1);
        $q = $this->db->get($tblname);
        return $q;
    }

    public function get_deptByBlockcode($colname, $colval, $tblname) {
        $this->db->where($colname, $colval);
        $this->db->where('is_active', 1);
        $this->db->order_by("created_on", "DESC");
        $this->db->group_by("department_code");
        $q = $this->db->get($tblname);
        return $q;
    }

    public function getdtl2($colval, $tblname) {
        $this->db->where($colval);
        $this->db->where('is_active', 1);
        $this->db->order_by("created_on", "DESC");
        $q = $this->db->get($tblname);
        return $q;
    }

    public function getdeptbyblock($colval) {

        $this->db->select();
        $this->db->from("mst_department as dept");
        $this->db->join("mst_area as ar", "dept.department_code=ar.department_code", "INNER");
        $this->db->where('dept.block_code', $colval);
        $this->db->where('dept.is_active', 1);
        $this->db->group_by('dept.department_code');
        $this->db->order_by("dept.created_on", "DESC");
        $q = $this->db->get();
        return $q;
    }

    public function getareabysubblock($val, $tblname) {
        $this->db->where($val);
        $this->db->where('is_active', 1);
        $this->db->order_by("created_on", "DESC");
        $q = $this->db->get($tblname);
        return $q;
    }

    public function get_dtl5($colname, $colval, $colname2, $colval2, $tblname) {
        $this->db->where($colname, $colval);
        $this->db->where($colname2, $colval2);
        $this->db->where('is_active', 1);
        $this->db->order_by("created_on", "DESC");
        $q = $this->db->get($tblname);
        return $q;
    }

    public function getlike_dtl2($colname, $colval, $tblname) {
        $this->db->like($colname, $colval);
        $this->db->where('is_active', 1);
        $this->db->order_by("created_on", "DESC");
        $q = $this->db->get($tblname);
        return $q;
    }

    public function get_dtl1($tblname) {
        $this->db->where('is_active', 1);
        $this->db->order_by("created_on", "DESC");
        $q = $this->db->get($tblname);
        return $q;
    }

    public function getblock($tblname) {
        $this->db->where('is_active', 1);
        $this->db->where('block_code !=', 'Block-000');
        $this->db->order_by("created_on", "DESC");
        $q = $this->db->get($tblname);
        return $q;
    }

    public function getRoleStatusMapping() {
        $this->db->select("rwm.id as id,st.status_name as status,r.role_description as role,rwm.created_by,rwm.created_on");
        $this->db->from("mst_status as st");
        $this->db->join("mst_roleid_workflow_mapping as rwm", "st.id=rwm.status_id", "INNER");
        $this->db->join("mst_role as r", "r.id=rwm.role_id", "INNER");
        $this->db->where('st.is_active', 1);
        $this->db->where('rwm.is_active', 1);
        $this->db->order_by("created_on", "DESC");
        $q = $this->db->get();
        return $q;
    }

    //end function to get records
    //function for update records by Bhupendra
    public function update33_15para_with_1where_and_tbl($tblname, $wherecol, $whereval, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7, $col8, $val8, $col9, $val9, $col10, $val10, $col11, $val11, $col12, $val12, $col13, $val13, $col14, $val14, $col15, $val15) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7, $col8 => $val8, $col9 => $val9, $col10 => $val10, $col11 => $val11, $col12 => $val12, $col13 => $val13, $col14 => $val14, $col15 => $val15);
        $this->db->where($wherecol, $whereval);
        $this->db->update($tblname, $arr);
        $AR = $this->db->affected_rows();
        return $AR;
    }

    public function update31_14para_with_1where_and_tbl($tblname, $wherecol, $whereval, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7, $col8, $val8, $col9, $val9, $col10, $val10, $col11, $val11, $col12, $val12, $col13, $val13, $col14, $val14) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7, $col8 => $val8, $col9 => $val9, $col10 => $val10, $col11 => $val11, $col12 => $val12, $col13 => $val13, $col14 => $val14);
        $this->db->where($wherecol, $whereval);
        $this->db->update($tblname, $arr);
        $AR = $this->db->affected_rows();
        return $AR;
    }

    public function update29_13para_with_1where_and_tbl($tblname, $wherecol, $whereval, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7, $col8, $val8, $col9, $val9, $col10, $val10, $col11, $val11, $col12, $val12, $col13, $val13) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7, $col8 => $val8, $col9 => $val9, $col10 => $val10, $col11 => $val11, $col12 => $val12, $col13 => $val13);
        $this->db->where($wherecol, $whereval);
        $this->db->update($tblname, $arr);
        $AR = $this->db->affected_rows();
        return $AR;
    }

    public function update27_12para_with_1where_and_tbl($tblname, $wherecol, $whereval, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7, $col8, $val8, $col9, $val9, $col10, $val10, $col11, $val11, $col12, $val12, $col13, $val13, $col14, $val14, $col15, $val15, $col16, $val16) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7, $col8 => $val8, $col9 => $val9, $col10 => $val10, $col11 => $val11, $col12 => $val12, $col13 => $val13, $col14 => $val14, $col15 => $val15, $col16 => $val16);
        $this->db->where($wherecol, $whereval);
        $this->db->update($tblname, $arr);
        $AR = $this->db->affected_rows();
        return $AR;
    }

    public function update25_11para_with_1where_and_tbl($tblname, $wherecol, $whereval, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7, $col8, $val8, $col9, $val9, $col10, $val10, $col11, $val11) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7, $col8 => $val8, $col9 => $val9, $col10 => $val10, $col11 => $val11);
        $this->db->where($wherecol, $whereval);
        $this->db->update($tblname, $arr);
        $AR = $this->db->affected_rows();
        return $AR;
    }

    public function update23_10para_with_1where_and_tbl($tblname, $wherecol, $whereval, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7, $col8, $val8, $col9, $val9, $col10, $val10) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7, $col8 => $val8, $col9 => $val9, $col10 => $val10);
        $this->db->where($wherecol, $whereval);
        $this->db->update($tblname, $arr);
        $AR = $this->db->affected_rows();
        return $AR;
    }

    public function update21_9para_with_1where_and_tbl($tblname, $wherecol, $whereval, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7, $col8, $val8, $col9, $val9) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7, $col8 => $val8, $col9 => $val9);
        $this->db->where($wherecol, $whereval);
        $this->db->update($tblname, $arr);
        $AR = $this->db->affected_rows();
        return $AR;
    }

    public function update19_8para_with_1where_and_tbl($tblname, $wherecol, $whereval, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7, $col8, $val8) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7, $col8 => $val8);
        $this->db->where($wherecol, $whereval);
        $this->db->update($tblname, $arr);
        $AR = $this->db->affected_rows();
        return $AR;
    }

    public function update17_7para_with_1where_and_tbl($tblname, $wherecol, $whereval, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6, $col7, $val7) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6, $col7 => $val7);
        $this->db->where($wherecol, $whereval);
        $this->db->update($tblname, $arr);
        $AR = $this->db->affected_rows();
        return $AR;
    }

    public function update15_6para_with_1where_and_tbl($tblname, $wherecol, $whereval, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5, $col6, $val6) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5, $col6 => $val6);
        $this->db->where($wherecol, $whereval);
        $this->db->update($tblname, $arr);
        $AR = $this->db->affected_rows();
        return $AR;
    }

    public function update13_5para_with_1where_and_tbl($tblname, $wherecol, $whereval, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4, $col5, $val5) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4, $col5 => $val5);
        $this->db->where($wherecol, $whereval);
        $this->db->update($tblname, $arr);
        $AR = $this->db->affected_rows();
        return $AR;
    }

    public function update11_4para_with_1where_and_tbl($tblname, $wherecol, $whereval, $col1, $val1, $col2, $val2, $col3, $val3, $col4, $val4) {
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, $col4 => $val4);
        $this->db->where($wherecol, $whereval);
        $this->db->update($tblname, $arr);
        $AR = $this->db->affected_rows();
        return $AR;
    }

    public function update9_3para_with_1where_and_tbl($tblname, $wherecol, $whereval, $col1, $val1, $col2, $val2, $col3, $val3) {
        $expire_on = date('Y-m-d H:i:s', strtotime("+60 days"));
        $arr = array($col1 => $val1, $col2 => $val2, $col3 => $val3, "expire_on" => $expire_on);
        $this->db->where($wherecol, $whereval);
        $this->db->update($tblname, $arr);
        $AR = $this->db->affected_rows();
        //$this->db->insert("mst_employee_password_history", array("user_id" => $whereval, "password" => $val1, "created_by" => $this->session->userdata('user_id')));
        return $AR;
    }

    //End function for update records
    public function checkPlantcode($data) {
        $this->db->where("plant_code", $data["pcode"]);
        $this->db->where("is_active", 1);
        $query = $this->db->get("mst_plant");
        if ($query->num_rows() == 0) {
            $response = array("status" => 1);
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function mstPlantview() {
        $this->db->where("is_active", 1);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get("mst_plant");
        $response = $query->result_array();
        return $response;
    }

    public function mstPlantviewnow($id) {
        $this->db->where("id", $id);
        $query = $this->db->get("mst_plant");
        $response = $query->row_array();
        return $response;
    }

    public function mstPlantUpdate($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $array = ["plant_name" => $data["pname"], "plant_address" => $data["paddress"], "plant_remarks" => $data["premarks"], "plant_contact" => $data["pcontact"], "number_of_blocks" => $data["pblock"], "modified_by" => $this->session->userdata('empname'), "modified_on" => $date];
        if ($data['status'] == 'true') {
            $array['is_active'] = 1;
            $status = 'Active';
        } else {
            $array['is_active'] = 0;
            $status = 'Inactive';
        }
        $master_previous_data =[];
        $old_data = $this->db->get_where('mst_plant',['id='=>$data["plantid"]])->row_array();
        if(empty($old_data)){
            return $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Plant Does Not Exist.');
        }else{
            $master_previous_data = [
                'plant_name'=> $old_data['plant_name'],
                'plant_address'=>$old_data['plant_address'],
                'plant_contact'=>$old_data['plant_contact'],
                'status'=>$old_data['is_active']==1?'Active':'Inactive',
                'number_of_blocks'=>$old_data['number_of_blocks']
            ];
            $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
        }
        $this->db->where("id", $data["plantid"]);
        if ($this->db->update("mst_plant", $array)) {
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Plant Data Updated Successfuly.');
            /* Start - Insert elog audit histry */
            unset($array['plant_remarks']);
            unset($array['is_active']);
            $array["status"] = $status;
            $array["remarks"] = $data['premarks'];
            $uniquefiled1 = "plant_code:" . $data["pcode"];
            $uniquefiled2 = "plant_name:" . $data["pname"];
            $uniquefiled3 = "";
            $inserted_id = $this->db->insert_id();
            $array = ['plant_code'=>$data['pcode']] + $array;
            $auditParams = ['command_type' => 'update', 'activity_name' => 'Update Plant', 'type' => 'create_plant', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_plant', 'primary_id' => $data["plantid"], 'post_params' => $array,'master_previous_data'=>$master_previous_data];
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        } else {
            $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Plant data does not updated.');
        }
        return $response;
    }

    public function deletePlant($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $array = array("is_active" => 0, "modified_by" => $this->session->userdata('empname'), "modified_on" => $date);
        $this->db->where("id", $data["plantid"]);
        if ($this->db->update("mst_plant", $array)) {
            $response = array("status" => 1);

            $param = array("id" => $data["plantid"]);
            $rocordres = $this->Ptscommon->getMultipleRows('default', "mst_plant", $param);
            /* Start - Insert elog audit histry */
            $activity = $this->Ptscommon->getActivityNameType('mst_plant');
            $auditParams = array('command_type' => 'delete', 'activity_name' => $activity['activity_name'], 'type' => $activity['type'], 'table_name' => 'mst_plant', 'primary_id' => $data["plantid"], 'post_params' => array("Record Detail as :- " => $rocordres, "Deactivated_by" => $this->session->userdata('empname'), "Deactivated_on" => $date));
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function mstPlantSubmit($data) {
        $array = array("plant_code" => $data["pcode"], "plant_name" => $data["pname"], "plant_address" => $data["paddress"], "plant_remarks" => $data["premarks"], "plant_contact" => $data["pcontact"], "number_of_blocks" => $data["pblock"], "created_by" => $this->session->userdata('empname'));
        $this->db->where("plant_code", $data["pcode"]);
        $getdata = $this->db->get("mst_plant")->row_array();
        if (!empty($getdata)) {
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Plant Code already exist');
        } else {
            if ($this->db->insert("mst_plant", $array)) {
                $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Plant Data saved successfully.');
                unset($array['plant_remarks']);
                $array["status"] = 'Active';
                unset($array['is_active']);
                $array["remarks"] = $data['premarks'];
                $array = $this->Ptscommon->getNAUpdated($array);
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "plant_code:" . $data["pcode"];
                $uniquefiled2 = "plant_name:" . $data["pname"];
                $uniquefiled3 = "";
                $inserted_id = $this->db->insert_id();
                $array['is_active'] = "1";
                $auditParams = ['command_type' => 'insert', 'activity_name' => 'Create Plant','type' => 'create_plant','uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_plant', 'primary_id' => $inserted_id,'post_params' =>$array];
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */
            } else {
                $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Plant does not Added');
            }
        }
        return $response;
    }

    public function checkBlockcode($data) {
        $this->db->where("block_code", $data["bcode"]);
        $this->db->where("is_active", 1);
        $query = $this->db->get("mst_block");
        if ($query->num_rows() == 0) {
            $response = array("status" => 1);
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function mstBlockview() {
        $this->db->where("is_active", 1);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get("mst_block");
        $response = $query->result_array();
        return $response;
    }

    public function mstEmpview() {
        //$this->db->where("is_active", 1);
        //$this->db->order_by("id", "DESC");
        $query = $this->db->get("mst_employee");
        $response = $query->result_array();
        return $response;
    }

    public function mstBlockSubmit($data) {
        $array = array("block_code" => $data["bcode"], "block_name" => $data["bname"], "block_contact" => $data["bcontact"], "number_of_areas" => $data["barea"], "block_remarks" => $data["bremarks"], "plant_code" => $data["pcode"], "created_by" => $this->session->userdata('empname'));
        $this->db->where("block_code", $data["bcode"]);
        //  $this->db->or_where("block_name",$data["bname"]);
        $block_data = $this->db->get("mst_block")->row_array();
        if (!empty($block_data)) {
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Block Code already exist.');
        } else {
                $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Block data saved successfully.');
				if ($this->db->insert("mst_block", $array)) {
                unset($array['number_of_areas']);
                unset($array['block_remarks']);
                $array['status'] = 'Active';
                $array['number_of_sub_blocks'] = $data["barea"];
                $array['remarks'] = $data['bremarks'];
                $array = $this->Ptscommon->getNAUpdated($array);
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "block_code:" . $data["bcode"];
                $uniquefiled2 = "plant_code:" . $data["pcode"];
                $uniquefiled3 = "block_name:" . $data["bname"];
                $inserted_id = $this->db->insert_id();
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Block Master', 'type' => 'manage_block', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_block', 'primary_id' => $inserted_id, 'post_params' => $array);
                $abtest = $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */
            } else {
                $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Block does not Added.');
            }
        }
		
        return $response;
    }

    public function mstBlockviewnow($id) {
        $this->db->where("id", $id);
        $query = $this->db->get("mst_block");
        $response = $query->row_array();
        return $response;
    }

    public function mstBlockUpdate($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $array = array("block_name" => $data["bname"], "block_contact" => $data["bcontact"], "number_of_areas" => $data["barea"], "block_remarks" => $data["bremarks"], "plant_code" => $data["pcode"], "modified_by" => $this->session->userdata('empname'), "modified_on" => $date);
        if ($data['status'] == 'true') {
            $array['is_active'] = 1;
            $status = 'active';
        } else {
            $array['is_active'] = 0;
            $status = 'inactive';
        }
        $master_previous_data = [];
        $old_data = $this->db->get_where('mst_block',['id='=>$data["blockid"]])->row_array();
        if(empty($old_data)){
            return $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Block Does Not Exist.');
        }else{
            $master_previous_data = [
                'block_name'=> $old_data['block_name'],
                'plant_code'=>$old_data['plant_code'],
                'block_contact'=>$old_data['block_contact'],
                'status'=>$old_data['is_active']==1?'Active':'Inactive',
                'number_of_sub_blocks'=>$old_data['number_of_areas']
            ];
            $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
        }
        $this->db->where("id", $data["blockid"]);
        if ($this->db->update("mst_block", $array)) {
            $this->db->update("mst_department", array("modified_by" => $this->session->userdata('empname'), "modified_on" => $date, "is_active" => $array['is_active']), array("block_code" => $data['bcode']));
            $this->db->update("pts_mst_instrument", array("modified_by" => $this->session->userdata('empname'), "modified_on" => $date, "status" => $status), array("block_code" => $data['bcode']));
            $this->db->update("mst_equipment", array("modified_by" => $this->session->userdata('empname'), "modified_on" => $date, "is_active" => $array['is_active']), array("block_code" => $data['bcode']));
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Block Data Updated Successfuly.');
            /* Start - Insert elog audit histry */
            $uniquefiled1 = "block_code:" . $data["bcode"];
            $uniquefiled2 = "plant_code:" . $data["pcode"];
            $uniquefiled3 = "block_name:" . $data["bname"];
            unset($array['is_active']);
            unset($array['number_of_areas']);
            unset($array['block_remarks']);
            $array['status'] = $status;
            $array['number_of_sub_blocks'] = $data["barea"];
            $array['remarks'] = $data['bremarks'];
            $array = ['block_code'=>$old_data['block_code']] + $array;
            $array = $this->Ptscommon->getNAUpdated($array);
            $auditParams = array('command_type' => 'update', 'activity_name' => 'Block Master', 'type' => 'manage_block', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_block', 'primary_id' => $data["blockid"], 'post_params' => $array,"master_previous_data"=>$master_previous_data);
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        } else {
            $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Block data does not updated.');
        }
        return $response;
    }

    public function deleteBlock($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $array = array("is_active" => 0, "modified_by" => $this->session->userdata('empname'), "modified_on" => $date);
        $this->db->where("id", $data["blockid"]);
        if ($this->db->update("mst_block", $array)) {
            $response = array("status" => 1);

            $param = array("id" => $data["blockid"]);
            $rocordres = $this->Ptscommon->getMultipleRows('default', "mst_block", $param);
            /* Start - Insert elog audit histry */
            $activity = $this->Ptscommon->getActivityNameType('mst_block');
            $auditParams = array('command_type' => 'delete', 'activity_name' => $activity['activity_name'], 'type' => $activity['type'], 'table_name' => 'mst_block', 'primary_id' => $data["blockid"], 'post_params' => array("Record Detail as :- " => $rocordres, "Deactivated_by" => $this->session->userdata('empname'), "Deactivated_on" => $date));
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function checkAreacode($data) {
        $this->db->where("area_code", $data["acode"]);
        $this->db->where("is_active", 1);
        $query = $this->db->get("mst_area");
        if ($query->num_rows() == 0) {
            $response = array("status" => 1);
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function mstAreaSubmit($data) {
        $response = array("status" => 0);
        $subblockcode = explode("##", $data["bcode"]);
        $subblock = $subblockcode[0];
        $block = $subblockcode[1];
        $array = ["area_code" => $data["acode"], "area_name" => $data["aname"], "area_contact" => $data["acontact"], "area_remarks" => $data["aremarks"], "area_sop_no" => $data["asop"], "number_of_rooms" => $data["aroom"], "block_code" => $block, "department_code" => $subblock, "created_by" => $this->session->userdata('empname')];
        $arrayForAudit = ["area_code" => $data["acode"], "area_name" => $data["aname"], "contact" => $data["acontact"], "area_sop_no" => $data["asop"], "number_of_rooms" => $data["aroom"], "block_code" => $block, "sub_block_code" => $subblock,"status"=>'Active',"remarks" => $data["aremarks"]];
        $arrayForAudit = $this->Ptscommon->getNAUpdated($arrayForAudit);
        $this->db->where("area_code", $data["acode"]);
        //   $this->db->or_where("area_name",$data["aname"]);
        $getdata = $this->db->get("mst_area")->row_array();
        if (!empty($getdata)) {
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Area Code already exist.');
        } else {
            if ($this->db->insert("mst_area", $array)) {
                $inserted_id = $this->db->insert_id();
                $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Area Master data saved successfully.');
                /* checking for Sub Block where Area is Null */
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "area_code:" . $data["acode"];
                $uniquefiled2 = "block_code:" . $block;
                $uniquefiled3 = "department_code:" . $subblock;
                $array['is_active'] = "1";
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Area Master', 'type' => 'manage_area', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_area', 'primary_id' => $inserted_id, 'post_params' => $arrayForAudit);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */
            } else {
                $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Area Master does not Added.');
            }
        }
        return $response;
    }

    public function mstAreaview() {
        $this->db->where("is_active", 1);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get("mst_area");
        $response = $query->result_array();
        return $response;
    }

    public function mstAreaviewnow($id) {
        $this->db->where("id", $id);
        $query = $this->db->get("mst_area");
        $response = $query->row_array();
        return $response;
    }

    public function mstAreaupdate($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $response = array("status" => 0);
        $subblockcode = explode("##", $data["bcode"]);
        $subblock = $subblockcode[0];
        $block = $subblockcode[1];
        $array = array("area_name" => $data["aname"], "area_contact" => $data["acontact"], "area_remarks" => $data["aremarks"], "area_sop_no" => $data["asop"], "number_of_rooms" => $data["aroom"], "block_code" => $block, "department_code" => $subblock, "modified_by" => $this->session->userdata('empname'), "modified_on" => $date);
        if ($data['status'] == 'true') {
            $array['is_active'] = 1;
            $status = 'Active';
        } else {
            $array['is_active'] = 0;
            $status = 'Inactive';
        }
        $arrayForAudit = ["area_name" => $data["aname"], "contact" => $data["acontact"], "area_sop_no" => $data["asop"], "number_of_rooms" => $data["aroom"], "block_code" => $block, "sub_block_code" => $subblock,"status"=>$status,"remarks" => $data["aremarks"]];
        $master_previous_data = [];
        $old_data = $this->db->get_where('mst_area',['id='=>$data["areaid"]])->row_array();
        if(empty($old_data)){
            return $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Area Does Not Exist.');
        }else{
            $master_previous_data = [
                'area_name'=> $old_data['area_name'],
                'area_address'=>$old_data['area_address'],
                'contact'=>$old_data['area_contact'],
                'status'=>$old_data['is_active']==1?'Active':'Inactive',
                'area_sop_no'=>$old_data['area_sop_no'],
                'block_code'=>$old_data['block_code'],
                'sub_block_code'=>$old_data['department_code'],
                'number_of_rooms'=>$old_data['number_of_rooms'],
            ];
            $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
        }
        $this->db->where("id", $data["areaid"]);
        if ($this->db->update("mst_area", $array)) {
            $arrayForAudit= ["area_code"=>$old_data['area_code']]+ $arrayForAudit;
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Area Master Updated Successfuly.');
            /* Start - Insert elog audit histry */
            $arrayForAudit = $this->Ptscommon->getNAUpdated($arrayForAudit);
            $uniquefiled1 = "area_code:" . $data["acode"];
            $uniquefiled2 = "block_code:" . $block;
            $uniquefiled3 = "department_code:" . $subblock;
            $auditParams = array('command_type' => 'update', 'activity_name' => 'Area Master', 'type' => 'manage_area', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_area', 'primary_id' => $data["areaid"], 'post_params' => $arrayForAudit,"master_previous_data"=>$master_previous_data);
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        } else {
            $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Area Master does not updated.');
        }

        return $response;
    }

    public function deleteArea($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $array = array("is_active" => 0, "modified_by" => $this->session->userdata('empname'), "modified_on" => $date);
        $this->db->where("id", $data["areaid"]);
        if ($this->db->update("mst_area", $array)) {
            $response = array("status" => 1);

            $param = array("id" => $data["areaid"]);
            $rocordres = $this->Ptscommon->getMultipleRows('default', "mst_area", $param);
            /* Start - Insert elog audit histry */
            $activity = $this->Ptscommon->getActivityNameType('mst_area');
            $auditParams = array('command_type' => 'delete', 'activity_name' => $activity['activity_name'], 'type' => $activity['type'], 'table_name' => 'mst_area', 'primary_id' => $data["areaid"], 'post_params' => array("Record Detail as :- " => $rocordres, "Deactivated_by" => $this->session->userdata('empname'), "Deactivated_on" => $date));
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function checkRoomcode($data) {
        $this->db->where("room_code", $data["rcode"]);
        //  $this->db->where("is_active", 1);
        $query = $this->db->get("mst_room");
        if ($query->num_rows() == 0) {
            $response = array("status" => 1);
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function mstRoomSubmit($data) {
        $room_array = $array = array("room_code" => $data["rcode"], "room_name" => $data["rname"], "room_remarks" => $data["rremarks"], "area_code" => $data["acode"], "created_by" => $this->session->userdata('empname'));
        $room_array_audit = array("room_code" => $data["rcode"], "room_name" => $data["rname"], "remarks" => $data["rremarks"], "area_code" => $data["acode"],"status"=>'Active');
        $this->db->where("room_code", $data["rcode"]);
        $getdata = $this->db->get("mst_room")->row_array();
        if (!empty($getdata)) {
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Room Code already exist.');
        } else {
            if ($this->db->insert("mst_room", $array)) {
                $inserted_id = $this->db->insert_id();
                $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Room data saved successfully.');

                $room_array_audit = $this->Ptscommon->getNAUpdated($room_array_audit);
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "room_code:" . $data["rcode"];
                $uniquefiled2 = "room_name:" . $data["rname"];
                $uniquefiled3 = "area_code:" . $data["acode"];
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Room Master', 'type' => 'create_room', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_room', 'primary_id' => $inserted_id, 'post_params' => $room_array_audit);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */
            } else {
                $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Room does not Added.');
            }
        }
        return $response;
    }

    public function mstRoomview() {
        $this->db->where("is_active", 1);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get("mst_room");
        $response = $query->result_array();
        return $response;
    }

    public function get_roomeditdrainpoint($roomid) {
        $this->db->where('id', $roomid);
        $this->db->where('is_active', 1);
        $r = $this->db->get('mst_room');
        $data = $r->row_array();

        $this->db->where('is_active', 1);
        $this->db->where('room_code', $data["room_code"]);
        $this->db->or_where('room_code', Null);
        $q = $this->db->get("mst_drainpoint");
        return $q;
    }

    public function mstRoomviewnow($id) {
        $this->db->where("id", $id);
        $query = $this->db->get("mst_room");
        $response = $query->row_array();
        return $response;
    }

    public function mstRoomupdate($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $room_array = $array = array("room_code" => $data["rcode"], "room_name" => $data["rname"], "room_remarks" => $data["rremarks"], "area_code" => $data["acode"], "modified_by" => $this->session->userdata('empname'), "modified_on" => $date);
        if ($data['status'] == 'true') {
            $array['is_active'] = 1;
            $status = 'Active';
        } else {
            $array['is_active'] = 0;
            $status = 'Inactive';
        }
        $room_array_audit = ["room_code" => $data["rcode"], "room_name" => $data["rname"], "area_code" => $data["acode"],"status"=>$status,"remarks" => $data["rremarks"],];
        $room_array_audit = $this->Ptscommon->getNAUpdated($room_array_audit);
//$predata = $this->Ptscommon->getRecordtoCheckDiff($data["roomid"], "mst_room");                
        $this->db->where("workflownextstep>", '-1');
        $logData = $this->db->get_where("pts_trn_user_activity_log", array("room_code" => $data["rcode"]))->result_array();
        if (!empty($logData) && $data['status'] == 'false') {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'You can not deactivate this.Some Logs are in progress.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        $master_previous_data = [];
        $old_data = $this->db->get_where('mst_room',['id='=>$data["roomid"]])->row_array();
        if(empty($old_data)){
            return $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Room Does Not Exist.');
        }else{
            $master_previous_data = [
                'room_name'=>$old_data['room_name'],
                'contact'=>$old_data['room_contact'],
                'area_code'=>$old_data['area_code'],
                'status'=>$old_data['is_active']==1?'Active':'Inactive'
            ];
            $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
        }
        $this->db->where("id", $data["roomid"]);
        if ($this->db->update("mst_room", $array)) {
            $this->db->update("mst_equipment", array("is_active" => $array['is_active'], "modified_by" => $this->session->userdata('empname'), "modified_on" => $date), array("room_code" => $data["rcode"]));
            $this->db->update("pts_mst_line_log", array("status" => $status, "modified_by" => $this->session->userdata('empname'), "modified_on" => $date), array("room_code" => $data["rcode"]));
            $this->db->update("pts_mst_balance_calibration", array("status" => $status, "modified_by" => $this->session->userdata('empname'), "modified_on" => $date), array("room_code" => $data["rcode"]));
            $this->db->update("pts_mst_sys_id", array("status" => $status, "modified_by" => $this->session->userdata('empname'), "modified_on" => $date), array("room_code" => $data["rcode"]));
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Room Master Updated Successfuly.');
            /* Start - Insert elog audit histry */

            //$resultDiff=array_diff($room_array,$predata[0]);
            $uniquefiled1 = "room_code:" . $data["rcode"];
            $uniquefiled2 = "room_name:" . $data["rname"];
            $uniquefiled3 = "area_code:" . $data["acode"];
            $inserted_id = $this->db->insert_id();
            $auditParams = array('command_type' => 'update', 'activity_name' => 'Room Master', 'type' => 'create_room', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_room', 'primary_id' => $data["roomid"], 'post_params' => $room_array_audit,"master_previous_data"=>$master_previous_data);
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        } else {
            $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Room Master does not updated.');
        }
        return $response;
    }

    public function deleteRoom($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $array = array("is_active" => 0, "modified_by" => $this->session->userdata('empname'), "modified_on" => $date);
        $this->db->where("id", $data["roomid"]);
        if ($this->db->update("mst_room", $array)) {
            $response = array("status" => 1);
            $param = array("id" => $data["roomid"]);
            $rocordres = $this->Ptscommon->getMultipleRows('default', "mst_room", $param);
            /* Start - Insert elog audit histry */
            $activity = $this->Ptscommon->getActivityNameType('mst_room');
            $auditParams = array('command_type' => 'delete', 'activity_name' => $activity['activity_name'], 'type' => $activity['type'], 'table_name' => 'mst_room', 'primary_id' => $data["roomid"], 'post_params' => array("Record Detail as :- " => $rocordres, "Deactivated_by" => $this->session->userdata('empname'), "Deactivated_on" => $date));
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function checkDocumentcode($data) {
        $this->db->where("doc_id", $data["dcode"]);
        $query = $this->db->get("mst_document");
        if ($query->num_rows() == 0) {
            $response = array("status" => 1);
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function mstDocumentSubmit($data) {
        $array = array("doc_id" => $data["dcode"], "remarks" => $data["dremarks"], "created_by" => $this->session->userdata('empname'));

        if ($this->db->insert("mst_document", $array)) {
            $response = array("status" => 1);
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function mstDocumentview() {
        $this->db->where("is_active", 1);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get("mst_document");
        $response = $query->result_array();
        return $response;
    }

    public function mstDocumentviewnow($id) {
        $this->db->where("id", $id);
        $query = $this->db->get("mst_document");
        $response = $query->row_array();
        return $response;
    }

    public function mstDocumentupdate($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $array = array("remarks" => $data["dremarks"], "modified_by" => $this->session->userdata('empname'), "modified_on" => $date);
        $this->db->where("id", $data["docid"]);
        if ($this->db->update("mst_document", $array)) {
            $response = array("status" => 1);
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function deleteDocument($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $array = array("is_active" => 0, "modified_by" => $this->session->userdata('empname'), "modified_on" => $date);
        $this->db->where("id", $data["docid"]);
        if ($this->db->update("mst_document", $array)) {
            $response = array("status" => 1);
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function checkDraincode($data) {
        $this->db->where("drainpoint_code", $data["dpcode"]);
        $this->db->where("is_active", 1);
        $query = $this->db->get("mst_drainpoint");
        if ($query->num_rows() == 0) {
            $response = array("status" => 1);
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function mstDrainpointSubmit($data) {
        $this->db->where("drainpoint_code", $data["dpcode"]);
        $this->db->where("is_active", 1);
        $query = $this->db->get("mst_drainpoint");
        if ($query->num_rows() > 0) {
            $response = array("status" => 2);
        } else {
            $array = array("drainpoint_code" => $data["dpcode"], "drainpoint_name" => $data["dpname"], "drainpoint_remark" => $data["dpremarks"], "solution_code" => $data["scode"], "created_by" => $this->session->userdata('empname'));
            if ($this->db->insert("mst_drainpoint", $array)) {
                $response = array("status" => 1);
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "drainpoint_code:" . $data["dpcode"];
                $uniquefiled2 = "solution_code:" . $data["scode"];
                $uniquefiled3 = "drainpoint_name:" . $data["dpname"];
                $inserted_id = $this->db->insert_id();
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Create Drain Point', 'type' => 'manage_drain_point', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_drainpoint', 'primary_id' => $inserted_id, 'post_params' => $array);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */
            } else {
                $response = array("status" => 0);
            }
        }
        return $response;
    }

    public function mstDrainpointview() {
        $this->db->where("is_active", 1);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get("mst_drainpoint");
        $response = $query->result_array();
        return $response;
    }

    public function mstDrainpointviewnow($id) {
        $this->db->where("id", $id);
        $query = $this->db->get("mst_drainpoint");
        $response = $query->row_array();
        return $response;
    }

    public function mstDrainpointupdate($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $array = array("drainpoint_name" => $data["dpname"], "drainpoint_remark" => $data["dpremarks"], "solution_code" => $data["scode"], "modified_by" => $this->session->userdata('empname'), "modified_on" => $date);
        $this->db->where("id", $data["dpid"]);
        if ($this->db->update("mst_drainpoint", $array)) {
            $response = array("status" => 1);

            /* Start - Insert elog audit histry */
            $uniquefiled1 = "drainpoint_code:" . $data["dpcode"];
            $uniquefiled2 = "solution_code:" . $data["scode"];
            $uniquefiled3 = "drainpoint_name:" . $data["dpname"];
            $auditParams = array('command_type' => 'update', 'activity_name' => 'Update Drain Point', 'type' => 'manage_drain_point', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_drainpoint', 'primary_id' => $data["dpid"], 'post_params' => $array);
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function deleteDrainpoint($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $array = array("is_active" => 0, "modified_by" => $this->session->userdata('empname'), "modified_on" => $date);
        $this->db->where("id", $data["drainid"]);
        if ($this->db->update("mst_drainpoint", $array)) {
            $response = array("status" => 1);

            $param = array("id" => $data["drainid"]);
            $rocordres = $this->Ptscommon->getMultipleRows('default', "mst_drainpoint", $param);
            /* Start - Insert elog audit histry */
            $activity = $this->Ptscommon->getActivityNameType('mst_drainpoint');
            $auditParams = array('command_type' => 'delete', 'activity_name' => $activity['activity_name'], 'type' => $activity['type'], 'table_name' => 'mst_drainpoint', 'primary_id' => $data["drainid"], 'post_params' => array("Record Detail as :- " => $rocordres, "Deactivated_by" => $this->session->userdata('empname'), "Deactivated_on" => $date));
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function checkDepartmentcode($data) {
        $this->db->where("department_code", $data["dcode"]);
        $this->db->where("is_active", 1);
        $query = $this->db->get("mst_department");
        if ($query->num_rows() == 0) {
            $response = array("status" => 1);
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function checkAreasbyblock($data) {
        $this->db->where("block_code", $data["bcode"]);
        $query = $this->db->get("mst_area");
        if ($query->num_rows() > 0) {
            $response = array("status" => 1, "data" => $query->result_array());
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function mstDepartmentSubmit($data) {
        $array = ["department_code" => $data["dcode"], "department_name" => $data["dname"], "department_contact" => $data["dcontact"], "department_remark" => $data["dremarks"], "block_code" => $data["bcode"], "created_by" => $this->session->userdata('empname')];
        $arrayForAudit = ["sub_block_code" => $data["dcode"], "sub_block_name" => $data["dname"], "contact" => $data["dcontact"], "remarks" => $data["dremarks"], "block_code" => $data["bcode"],"status"=>"Active"];
        $this->db->where("department_code", $data["dcode"]);
        $depart_data = $this->db->get("mst_department")->row_array();
        if (!empty($depart_data)) {
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Sub Block Code already exist.');
        } else {
            if ($this->db->insert("mst_department", $array)) {
                $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Sub Block data saved successfully.');

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "department_code:" . $data["dcode"];
                $uniquefiled2 = "block_code:" . $data["bcode"];
                $uniquefiled3 = "department_name:" . $data["dname"];
                $inserted_id = $this->db->insert_id();
                $arrayForAudit = $this->Ptscommon->getNAUpdated($arrayForAudit);
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Sub Block Master', 'type' => 'sub_block', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_department', 'primary_id' => $inserted_id, 'post_params' => $arrayForAudit);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */
            } else {
                $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Block does not Added.');
            }
        }
        return $response;
    }

    public function mstDepartmentview() {
        $this->db->where("is_active", 1);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get("mst_department");
        $response = $query->result_array();
        return $response;
    }

    public function mstDepartmentviewnow($id) {
        $this->db->where("id", $id);
        $query = $this->db->get("mst_department");
        $response = $query->row_array();
        return $response;
    }

    public function mstGetareanow($departmentdata) {
        $this->db->where("is_active", 1);
        $this->db->where("block_code", $departmentdata["block_code"]);
        $this->db->order_by("id", "DESC");
        $response = $this->db->get("mst_area");
        return $response;
    }

    public function mstDepartmentupdate($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $array = array("department_code" => $data["dcode"], "department_name" => $data["dname"], "department_contact" => $data["dcontact"], "department_remark" => $data["dremarks"], "block_code" => $data["bcode"], "modified_by" => $this->session->userdata('empname'), "modified_on" => $date);
        if ($data['status'] == 'true') {
            $array['is_active'] = 1;
            $status = 'Active';
        } else {
            $array['is_active'] = 0;
            $status = 'Inactive';
        }
        $arrayForAudit = ["sub_block_code" => $data["dcode"], "sub_block_name" => $data["dname"], "contact" => $data["dcontact"],"block_code" => $data["bcode"],"status"=>$status,"remarks" => $data["dremarks"],];
        $master_previous_data = [];
        $old_data = $this->db->get_where('mst_department',['department_code='=>$data["dcode"]])->row_array();
        if(empty($old_data)){
            return $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Sub Block Does Not Exist.');
        }else{
            $master_previous_data = [
                'sub_block_name'=> $old_data['department_name'],
                'contact'=>$old_data['department_contact'],
                'block_code'=>$old_data['block_code'],
                'status'=>$old_data['is_active']==1?'Active':'Inactive',
            ];
            $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
        }
//        print_r($data["dcode"]);exit;
        $this->db->where("department_code", $data["dcode"]);
//        $this->db->where("is_active", 1);
        if ($this->db->update("mst_department", $array)) {
            $this->db->update("mst_area", array("modified_by" => $this->session->userdata('empname'), "modified_on" => $date, "is_active" => $array['is_active']), array("department_code" => $data['dcode']));
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Block Data Updated Successfuly.');

            $arrayForAudit = $this->Ptscommon->getNAUpdated($arrayForAudit);
            /* Start - Insert elog audit histry */
            $uniquefiled1 = "department_code:" . $data["dcode"];
            $uniquefiled2 = "block_code:" . $data["bcode"];
            $uniquefiled3 = "department_name:" . $data["dname"];
            $where = array('department_code' => $data["dcode"], 'is_active' => 1);
//            $inserted_id = $this->db->where($where)->get("mst_department")->row()->id;
            $auditParams = array('command_type' => 'update', 'activity_name' => 'Sub Block Master', 'type' => 'sub_block', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_department', 'primary_id' => $data['blockid'], 'post_params' => $arrayForAudit,"master_previous_data"=>$master_previous_data);
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        } else {
            $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Block data does not updated.');
        }
        return $response;
    }

    public function deleteDepartment($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $array = array("is_active" => 0, "modified_by" => $this->session->userdata('empname'), "modified_on" => $date);
        $this->db->where("id", $data["departmentid"]);
        if ($this->db->update("mst_department", $array)) {
            $response = array("status" => 1);

            $param = array("id" => $data["departmentid"]);
            $rocordres = $this->Ptscommon->getMultipleRows('default', "mst_department", $param);
            /* Start - Insert elog audit histry */
            $activity = $this->Ptscommon->getActivityNameType('mst_department');
            $auditParams = array('command_type' => 'delete', 'activity_name' => $activity['activity_name'], 'type' => $activity['type'], 'table_name' => 'mst_department', 'primary_id' => $data["departmentid"], 'post_params' => array("Record Detail as :- " => $rocordres, "Deactivated_by" => $this->session->userdata('empname'), "Deactivated_on" => $date));
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function checkPlantlimit($data) {
        $this->db->where("is_active", 1);
        $this->db->where("plant_code", $data["pcode"]);
        $query = $this->db->get("mst_plant");
        $res1 = $query->row_array();
        $maxcount = $res1["number_of_blocks"];

        $this->db->where("is_active", 1);
        $this->db->where("plant_code", $data["pcode"]);
        $query2 = $this->db->get("mst_block");
        $actcount = $query2->num_rows();

        if ($actcount == $maxcount) {
            $response = array(STATUS => FALSE, "result" => FALSE, MESSAGE => '');
        } else {
            $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => '');
        }

        return $response;
    }

    public function checkBlocklimit($data) {
        $this->db->where("is_active", 1);
        $this->db->where("block_code", $data["bcode"]);
        $query = $this->db->get("mst_block");
        $res1 = $query->row_array();
        $maxcount = $res1["number_of_areas"];

        $this->db->where("is_active", 1);
        $this->db->where("block_code", $data["bcode"]);
        $query2 = $this->db->get("mst_department");
        $actcount = $query2->num_rows();

        if ($actcount == $maxcount) {
            $response = array(STATUS => FALSE, "result" => FALSE, MESSAGE => '');
        } else {
            $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => '');
        }

        return $response;
    }

    public function checkArealimit($data) {
        $this->db->where("is_active", 1);
        $this->db->where("area_code", $data["acode"]);
        $query = $this->db->get("mst_area");
        $res1 = $query->row_array();
        $maxcount = $res1["number_of_rooms"];

        $this->db->where("is_active", 1);
        $this->db->where("area_code", $data["acode"]);
        $query2 = $this->db->get("mst_room");
        $actcount = $query2->num_rows();
        if ($actcount == $maxcount) {
            $response = array(STATUS => FALSE, "result" => FALSE, MESSAGE => '');
        } else {
            $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => '');
        }


        return $response;
    }

    public function checkEquipmentcode($data) {
        $this->db->where("equipment_code", $data["ecode"]);
        $query = $this->db->get("mst_equipment");
        if ($query->num_rows() == 0) {
            $response = array("status" => 1);
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function getDrainpointlist($keyword) {
        $this->db->group_start();
        $this->db->like('drainpoint_code', $keyword);
        $this->db->group_end();
        $this->db->where("is_active", 1);
        //$this->db->limit(5);
        $this->db->group_by('drainpoint_code');
        $dev = $this->db->get("mst_drainpoint");
        foreach ($dev->result_array() as $sn) {
            $response[] = $sn["drainpoint_code"];
        }
        return $response;
    }

    public function getSoplist($keyword) {
        $this->db->group_start();
        $this->db->like('sop_code', $keyword);
        $this->db->group_end();
        $this->db->where("is_active", 1);
        //$this->db->limit(5);
        $this->db->group_by('sop_code');
        $dev = $this->db->get("mst_sop");
        foreach ($dev->result_array() as $sn) {
            $response[] = $sn["sop_code"];
        }
        return $response;
    }

    public function mstEquipmentview() {
        $this->db->where("is_active", 1);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get("mst_equipment");
        $response = $query->result_array();
        return $response;
    }

    public function mstEquipmentviewnow($id) {
        $this->db->select("eq.id,eq.equipment_code,eq.equipment_name,eq.equipment_icon,eq.equipment_type,eq.equipment_remarks,eq.room_code,rm.room_name,eq.block_code,eq.drain_point_code,eq.sop_code,ar.area_code,ar.area_name");
        $this->db->from("mst_equipment as eq");
        $this->db->join("mst_room as rm", "eq.room_code=rm.room_code", "LEFT");
        $this->db->join("mst_area as ar", "ar.area_code=rm.area_code", "LEFT");
        $this->db->where('eq.is_active', 1);
        $this->db->where("eq.id", $id);
        $query = $this->db->get();
        $response = $query->row_array();
        return $response;
    }

    public function deleteEquipment($data) {
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $array = array("is_active" => 0, "modified_by" => $this->session->userdata('empname'), "modified_on" => $date);
        $this->db->where("id", $data["equipmentid"]);
        if ($this->db->update("mst_equipment", $array)) {
            $response = array("status" => 1);

            $param = array("id" => $data["equipmentid"]);
            $rocordres = $this->Ptscommon->getMultipleRows('default', "mst_equipment", $param);
            /* Start - Insert elog audit histry */
            $activity = $this->Ptscommon->getActivityNameType("mst_equipment");
            $auditParams = array('command_type' => 'delete', 'activity_name' => $activity['activity_name'], 'type' => $activity['type'], 'table_name' => "mst_equipment", 'primary_id' => $data["equipmentid"], 'post_params' => array("Record Detail as :- " => $rocordres, "Deactivated_by" => $this->session->userdata('empname'), "Deactivated_on" => $date));
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function checkcode($tblname, $col, $val) {
        $this->db->where($col, $val);
        $this->db->where("is_active", 1);
        $query = $this->db->get($tblname);
        if ($query->num_rows() > 0) {
            $response = array("status" => 1);
        } else {
            $response = array("status" => 0);
        }
        return $response;
    }

    public function importDataExcel($data, $tablename) {
        $res = $this->db->insert_batch($tablename, $data);
        if ($res) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getroomwiseactivity($user_id) {
        $userdata = $this->db->get_where("mst_employee", array("id" => $user_id))->row_array();
        if ($userdata['room_code'] != "") {
            $room_array = explode(",", $userdata['room_code']);
            $this->db->select("pts_trn_room_log_activity.*,pts_mst_sys_id.room_code");
            $this->db->from("pts_trn_room_log_activity");
            $this->db->join("pts_mst_sys_id", "pts_mst_sys_id.room_id=pts_trn_room_log_activity.room_id", "LEFT");
//        $this->db->join("pts_mst_server_detail", "pts_mst_server_detail.server_ip=pts_mst_sys_id.device_id", "LEFT");
//        $this->db->where('pts_mst_server_detail.status', 'active');
            $this->db->where('pts_trn_room_log_activity.status', 'active');
            $this->db->where('pts_mst_sys_id.status', 'active');
            $this->db->where_in('pts_mst_sys_id.room_code', $room_array);
            $query = $this->db->get();
            $response = $query->result_array();
            return $response;
        } else {
            return array();
        }
    }

    /**
     * Get Role Assigned modules
     * Created By : Rahul Chauhan
     * Date: 20-07-2020
     */
    public function getRoleModuleData($param) {
        if ($param['module_type'] == 'log') {
            $this->db->select("pts_mst_role_mgmt.id as log_id,pts_mst_role_mgmt.type_id,pts_mst_role_mgmt.module_id,pts_mst_role_mgmt.is_create,pts_mst_role_mgmt.is_edit,pts_mst_role_mgmt.is_view,pts_mst_activity.*");
            $this->db->join("pts_mst_activity", 'pts_mst_activity.id = pts_mst_role_mgmt.module_id');
        }
        if ($param['module_type'] == 'report') {
            $this->db->select("pts_mst_role_mgmt.is_create,pts_mst_role_mgmt.is_edit,pts_mst_role_mgmt.is_view,pts_mst_report.*");
            $this->db->join("pts_mst_report", 'pts_mst_report.id = pts_mst_role_mgmt.module_id');
        }
        if ($param['module_type'] == 'master') {
            $this->db->select("pts_mst_role_mgmt.is_create,pts_mst_role_mgmt.is_edit,pts_mst_role_mgmt.is_view,pts_mst_master.*");
            $this->db->join("pts_mst_master", 'pts_mst_master.id = pts_mst_role_mgmt.module_id');
        }
        $this->db->from("pts_mst_role_mgmt");
        $this->db->where("pts_mst_role_mgmt.module_type", $param['module_type']);
        $this->db->where("pts_mst_role_mgmt.role_id", $param['role_id']);
        $this->db->where("pts_mst_role_mgmt.status", "active");
        $this->db->order_by("pts_mst_role_mgmt.module_id", 'ASC');
        $res = $this->db->get()->result_array();
        return $res;
    }

    public function getuserstatusdata($post) {
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $empname = $post['emp_name'];
        $blockcode = $post['block_code'];
        $this->db->select("pts_trn_emp_roomin.room_code,pts_trn_emp_roomin.emp_name,pts_trn_emp_roomin.room_in_time,pts_trn_emp_roomin.room_out_time,pts_trn_user_activity_log.user_id,mst_block.block_code,mst_room.room_name");
        $this->db->from("pts_trn_emp_roomin");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.room_code=pts_trn_emp_roomin.room_code", 'inner');
        $this->db->join("mst_employee", "mst_employee.id=pts_trn_emp_roomin.emp_code", 'inner');
        $this->db->join("mst_block", "mst_block.block_code=mst_employee.block_code", 'inner');
        $this->db->join("mst_room", "mst_room.room_code=pts_trn_emp_roomin.room_code", 'inner');
        $this->db->where("mst_block.block_code", $blockcode);
        if (!empty($empname)) {
            $this->db->where("pts_trn_emp_roomin.emp_name", $empname);
        }
        $this->db->where("pts_trn_user_activity_log.status", 'active');
        if (!empty($start_date && $end_date)) {
            $this->db->where('pts_trn_emp_roomin.created_on BETWEEN "' . date('Y-m-d 00:00:00', strtotime($start_date)) . '" and "' . date('Y-m-d 00:00:00', strtotime($end_date)) . '"');
            //$this->db->where('pts_trn_emp_roomin.created_on>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            //$this->db->where('pts_trn_emp_roomin.created_on<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }

        $result = $this->db->get()->result_array();
        //echo $this->db->last_query();exit;
        return $result;
        //print_r($result);exit;
    }
    
    public function getuserstatusdataNew($post) {
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $empname = $post['emp_name'];
        $blockcode = $post['block_code'];
        $page = isset($post['page'])?$post['page']:'';
        $limit = '';
        if(!empty($page)){
            $offset = ($page - 1) * ($this->Ptscommon::records_per_page);
            $limit = 'LIMIT '.$offset.','.$this->Ptscommon::records_per_page;
        }
        $this->db->select("`pts_trn_emp_roomin`.`room_code`,`pts_trn_emp_roomin`.`emp_name`,`pts_trn_emp_roomin`.`room_in_time`,`pts_trn_emp_roomin`.`room_out_time`,mst_employee.emp_code,`mst_room`.`room_name`,`mst_block`.`block_code`,`mst_employee`.`emp_email`");
        $this->db->from("pts_trn_emp_roomin");
        $this->db->join("`mst_employee`", "`mst_employee`.`id` = `pts_trn_emp_roomin`.`emp_code`");
        $this->db->join("mst_room", "pts_trn_emp_roomin.room_code=mst_room.room_code", 'left');
        $this->db->join("mst_area", "mst_room.area_code=mst_area.area_code", 'left');
        $this->db->join("mst_block", "mst_area.block_code=mst_block.block_code", 'left');
        
        if (!empty($blockcode)) {
            $this->db->where("mst_block.block_code", $blockcode);
        }
        if (!empty($empname) && $empname>0) {
            $this->db->where("pts_trn_emp_roomin.emp_code", $empname);
        }
        if (!empty($start_date)) {
            $this->db->where('pts_trn_emp_roomin.created_on >= "' . date('Y-m-d H:i:s', strtotime($start_date)) . '"');
        }
        if (!empty($end_date)) {
            $this->db->where('pts_trn_emp_roomin.created_on <= "' . date('Y-m-d 23:59:59', strtotime($end_date)) . '"');
        }
        $query1 = $this->db->get_compiled_select();

 

        $this->db->select("'' AS room_code,`pts_user_login_history`.emp_name, UNIX_TIMESTAMP(`pts_user_login_history`.login_time) AS room_in_time,UNIX_TIMESTAMP(`pts_user_login_history`.logout_time) AS room_out_time,`pts_user_login_history`.emp_code,'' AS room_name,pts_user_login_history.block_code,`mst_employee`.`emp_email`");
        $this->db->from("pts_user_login_history");
        $this->db->join("`mst_employee`", "`mst_employee`.`id` = `pts_user_login_history`.`emp_id`");
        if (!empty($blockcode)) {
            $this->db->where("FIND_IN_SET('".$blockcode."',`pts_user_login_history`.`block_code`)>",0);
        }
        if (!empty($empname)) {
            $this->db->where("pts_user_login_history.emp_id", $empname);
        }
        if (!empty($start_date)) {
            $this->db->where('pts_user_login_history.login_time >= "' . date('Y-m-d H:i:s', strtotime($start_date)) . '"');
        }
        if (!empty($end_date)) {
            $this->db->where('pts_user_login_history.login_time <= "' . date('Y-m-d 23:59:59', strtotime($end_date)) . '"');
        }

 

        $query2 = $this->db->get_compiled_select();
        $count =  $this->db->query("SELECT * from  (" . $query1 . " UNION All " . $query2 . ") as temp ORDER BY temp.room_in_time ASC")->result_array();
        $records =  $this->db->query("SELECT * from  (" . $query1 . " UNION All " . $query2 . ") as temp ORDER BY temp.room_in_time ASC ".$limit)->result_array();
        return ['count'=>count($count),'records'=>$records];
         
    }

}