<?php

class Ptsadminmodel extends CI_Model {

	/*workflow Delete*/
	public function deleterecords($act,$flowtype)
	{
	$this->db->query("delete  from pts_trn_workflowsteps where activity_id='".$act."' and workflowtype='".$flowtype."'");
	return $this->db->affected_rows() > 0;
	}

	public function getActivitiesforworkflow()
	{
		//$this->db->query('Select distinct act.activity_code, act.activity_name');
		$this->db->distinct();
		$this->db->select("act.id, act.activity_name, act.type");
		$this->db->from("pts_mst_activity as act");
		$this->db->where('act.status','active');
		$q= $this->db->get();
		if($q->num_rows() > 0)
		{	
			return $q;
		}		
	}

	public function userauth($userid, $pwd)
	{
		$this->db->where('emp_email',$userid);
		$this->db->where('emp_password',$pwd);
		$this->db->where('is_active',1);
		$res = $this->db->get('mst_employee');
		if($res->num_rows() > 0)
		{	
			return $res->row_array();			
		}
		else
		{
			return "";
		}			
	}

	public function get_dtl3($colname,$colval,$tblname)
	{
		$this->db->where($colname, $colval);		
		$this->db->where('isactive',1);
		$res = $this->db->get($tblname);
		return $res;				
	}

	public function get_dtl5($colname,$colval,$colname2,$colval2,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);		
		$this->db->where('isactive',1);
		$res = $this->db->get($tblname);
		if($res->num_rows() > 0)
		{	
			return $res;			
		}
		else
		{
			return "";
		}	
	}

	public function get_dtl7($colname,$colval,$colname2,$colval2,$colname3,$colval3,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where($colname3, $colval3);		
		$this->db->where('isactive',1);
		$res = $this->db->get($tblname);	
			return $res;			
	}

	public function get_dtl9($colname,$colval,$colname2,$colval2,$colname3,$colval3,$colname4,$colval4,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where($colname3, $colval3);
		$this->db->where($colname4, $colval4);		
		$this->db->where('isactive',1);
		$res = $this->db->get($tblname);	
			return $res;			
	}

	public function get_dtl11($colname,$colval,$colname2,$colval2,$colname3,$colval3,$colname4,$colval4,$colname5,$colval5,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where($colname3, $colval3);
		$this->db->where($colname4, $colval4);
		$this->db->where($colname5, $colval5);		
		$this->db->where('isactive',1);
		$res = $this->db->get($tblname);	
			return $res;			
	}

	public function get_lastrecord($colname,$colval,$colname2,$colval2,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);		
		$this->db->where('status','active');
		//$this->db->limit(1);
		//$this->db->order_by("id","DESC");
		$res = $this->db->get($tblname);
		if($res->num_rows() > 0)
		{	
			return $res;			
		}
		else
		{
			return "";
		}	
	}

	public function getnext_step($status_id,$role_id,$activity_id,$Actstatus)
	{		
		$this->db->where("status_id",$status_id);
		$this->db->where("role_id",$role_id);
		$this->db->where("activity_id",$activity_id);
		$this->db->where("workflowtype",$Actstatus);
		$this->db->where('is_active',1);		
		$res = $this->db->get('pts_trn_workflowsteps');
		return $res;		
	}

	public function getnext_step2($status_id,$activity_id,$Actstatus)
	{		
		$this->db->where("status_id",$status_id);
		$this->db->where("activity_id",$activity_id);
		$this->db->where("workflowtype",$Actstatus);
		$this->db->where('is_active',1);		
		$res = $this->db->get('pts_trn_workflowsteps');
		return $res;		
	}

	public function getnext_step_on_start_stop($activity_id,$Actstatus)
	{		
		$this->db->where("activity_id",$activity_id);
		$this->db->where("workflowtype",$Actstatus);
		$this->db->where('is_active',1);
		$this->db->limit(1);
		$this->db->order_by("id","ASC");		
		$res = $this->db->get('pts_trn_workflowsteps');
		return $res;		
	}

	public function getRoleforworkflow()
	{
		$q = $this->db->get('mst_role');
		if($q->num_rows() > 0)
		{	
			return $q;
		}		
	}

	public function getRole()
	{
		$this->db->where("is_active",1);
		$q = $this->db->get('mst_role');
		if($q->num_rows() > 0)
		{	
			return $q;
		}		
	}

	public function getstartworkflow()
	{
		$this->db->distinct();
		$this->db->select("wf.workflowtype,wf.activity_id,DATE_FORMAT(wf.created_on,'%d-%m-%Y') as created_on,wf.created_by,emp.emp_name,act.activity_name");
		$this->db->from("pts_trn_workflowsteps as wf");
		$this->db->join("mst_employee as emp","wf.created_by=emp.emp_code","INNER");
		$this->db->join("pts_mst_activity as act","act.id=wf.activity_id","INNER");
		//$this->db->where("wf.workflowtype",'start');
		$this->db->where("wf.is_active",1);
		$query = $this->db->get();
		return $query;
	}

	public function getstopworkflow()
	{
		$this->db->distinct();
		$this->db->select("wf.workflowtype,wf.activity_id,DATE_FORMAT(wf.created_on,'%d-%m-%Y') as created_on,wf.created_by,emp.emp_name,act.activity_name");
		$this->db->from("pts_trn_workflowsteps as wf");
		$this->db->join("mst_employee as emp","wf.created_by=emp.emp_code","INNER");
		$this->db->join("pts_mst_activity as act","act.id=wf.activity_id","INNER");
		//$this->db->where("wf.workflowtype",'stop');
		$this->db->where("wf.is_active",1);
		$query = $this->db->get();
		return $query;
	}

	public function getworkflowbyactid($actid,$type){
		$this->db->select("wf.id,wf.activity_id,wf.role_id,DATE_FORMAT(wf.created_on,'%d-%m-%Y') as created_on,wf.created_by,role.role_description");
		$this->db->from("pts_trn_workflowsteps as wf");
		$this->db->join("mst_role as role","wf.role_id=role.id","INNER");
		$this->db->where("wf.activity_id",$actid);
		$this->db->where("wf.workflowtype",$type);
		$this->db->where("wf.is_active",1);
		$this->db->where("wf.status_id>",1);
		$query = $this->db->get();
		return $query;
	}
}