<?php

//error_reporting(1);
//if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ptscommon extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database('');
    }

    // get the data fom mysql table
    public function getMultipleRows($db, $table, $param, $order = NULL) {
        $CI = &get_instance();
        $this->db = $CI->load->database($db, TRUE);
        $this->db->from($table);
        if (!empty($param)) {
            $this->db->where($param);
        }
        //$this->db->order_by('id', 'DESC');

        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $row = $query->result_array();
            return $row;
        } else {
            return array();
        }
    }

    /**
     * add single entry in table
     * $tablename=table name
     * $post=data in array formats like array('id'=>$val['id'],'name'=>$name)
     */
    public function addToTable($db, $tablename, $post = '', $param = '') {
        $CI = &get_instance();
        $this->db2 = $CI->load->database($db, TRUE);
        if (!empty($param)) {
            $insert = $this->db2->query($param);
            if (!$insert) {
                return FALSE;
            } else {
                return TRUE;
            }
        } else {
            $insert = $this->db2->insert($tablename, $post) or die(" error" . mysqli_error($this->db2));
            if (!$insert) {
                return FALSE;
            } else {
                return $this->db2->insert_id();
            }
        }
    }

    /**
     * update single entry in table
     * $tablename=table name
     * $post=data in array formats like array('id'=>$val['id'],'name'=>$name)
     */
    public function updateToTable($db, $tablename, $post = '', $param = '') {
        $CI = &get_instance();
        $this->db2 = $CI->load->database($db, TRUE);
        $this->db2->where($param);
        $insert = $this->db2->update($tablename, $post) or die(" error" . mysqli_error($this->db2));
        if (!$insert) {
            return FALSE;
        } else {
            return $this->db2->insert_id();
        }
    }

    /**
     * Get Inprogress Activity List
     * Created By :Rahul Chauhan
     * Date:27-01-2020
     */
    public function GetInprogressActivityList() {
        $actid = isset($_GET['actid']) ? $_GET['actid'] : '';
        $result = array();
        $this->db->select("pts_trn_workflowsteps.next_step,pts_trn_user_activity_log.activity_stop,pts_trn_user_activity_log.workflowstatus,pts_trn_user_activity_log.workflownextstep,pts_trn_user_activity_log.id as act_id,pts_trn_user_activity_log.activity_id as activity_id,pts_trn_user_activity_log.doc_id,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.activity_remarks,pts_trn_room_log_activity.activity_name,pts_trn_user_activity_log.roomlogactivity as processid,pts_mst_activity.type,pts_mst_activity.activity_url,pts_trn_user_activity_log.equip_code,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%m-%Y') as start_date,pts_trn_user_activity_log.user_name");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->join("pts_trn_room_log_activity", "pts_trn_room_log_activity.id=pts_trn_user_activity_log.roomlogactivity", 'left');
        $this->db->join("pts_trn_workflowsteps", "pts_trn_user_activity_log.activity_id=pts_trn_workflowsteps.activity_id", 'left');
        $this->db->join("pts_mst_activity", "pts_mst_activity.id=pts_trn_user_activity_log.activity_id", 'left');
        $this->db->where("pts_trn_user_activity_log.workflownextstep>", '-1');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
        if ($actid != "" && $actid != NULL) {
            $this->db->where("pts_trn_user_activity_log.id", $actid);
        }
        //$this->db->group_by("pts_trn_user_activity_log.activity_id");
        $this->db->group_by("pts_trn_user_activity_log.equip_code");
        $result = $this->db->get()->result_array();
        //echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Get QA Approval Activity List
     * Created By :Rahul Chauhan
     * Date:27-01-2020
     */
    public function GetQaApprovalActivityList() {
        $result = array();
        $this->db->select("pts_trn_user_activity_log.id as act_id,pts_trn_user_activity_log.doc_id,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.activity_remarks,pts_trn_room_log_activity.activity_name,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%m-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%m-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.user_name");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->join("pts_trn_room_log_activity", "pts_trn_room_log_activity.id=pts_trn_user_activity_log.roomlogactivity", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
        $this->db->where("pts_trn_user_activity_log.is_qa_approve", 'no');
        $result = $this->db->get()->result_array();
        return $result;
    }

    /**
     * Make Approval Record into approval table and updated inprogress activity too
     * Created By :Bhupendra kumar
     * Date:28-01-2020
     */
    public function Senttonextapproval($actlogtblid, $actid, $roleid, $remark, $status_id, $next_step, $empid, $empname, $email, $actstatus) {
        $arr = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'approve', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => $actstatus);
        $this->db->insert("pts_trn_activity_approval_log", $arr);

        $arr2 = array("workflowstatus" => $status_id, "workflownextstep" => $next_step);
        $this->db->where("id", $actlogtblid);
        if ($this->db->update("pts_trn_user_activity_log", $arr2)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function Sentfornextapproval($actlogtblid, $actid, $roleid, $remark, $status_id, $next_step, $empid, $empname, $email, $actstatus, $docno, $tblname, $result_rgt, $result_lft) {
        $arr = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'approve', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => $actstatus);
        $this->db->insert("pts_trn_activity_approval_log", $arr);

        if ($next_step == -1) {
            $ar = array("is_in_workflow" => 'no');
            if ($result_rgt != '' && $result_lft != '') {
                $ar['result_left '] = $result_lft;
                $ar['result_right'] = $result_rgt;
            }
            $this->db->where("doc_no", $docno);
            $this->db->update($tblname, $ar);
            $ar2 = array("workflowstatus" => $status_id, "workflownextstep" => $next_step, "activity_stop" => time());
            $this->db->where("id", $actlogtblid);
            if ($this->db->update("pts_trn_user_activity_log", $ar2)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            $arr2 = array("workflowstatus" => $status_id, "workflownextstep" => $next_step);
            $this->db->where("id", $actlogtblid);
            if ($this->db->update("pts_trn_user_activity_log", $arr2)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    public function SenttoQAapproval($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email) {
        $arr = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'approve', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time());
        $this->db->insert("pts_trn_activity_approval_log", $arr);

        $arr2 = array("is_qa_approve" => 'yes');
        $this->db->where("id", $actlogtblid);
        if ($this->db->update("pts_trn_user_activity_log", $arr2)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Stop running activity
     * Created By :Bhupendra kumar
     * Date:28-01-2020
     */
    public function Senttostop($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email, $status_id, $next_step, $stopreason) {
        $arr = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'stop', "reason" => $stopreason);

        $this->db->insert("pts_trn_activity_approval_log", $arr);

        $arr2 = array("workflowstatus" => $status_id, "workflownextstep" => $next_step, "activity_stop" => time());
        $this->db->where("id", $actlogtblid);
        if ($this->db->update("pts_trn_user_activity_log", $arr2)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * This function is used to start the Selected activity at home page
     * Created By :Bhupendra kumar
     * Date:28-01-2020
     */
    public function Senttostart($roomcode, $processid, $equipmentcode, $actid, $actname, $product_no, $batch_no, $roleid, $empid, $empname, $email, $status_id, $next_step, $remark, $remark2, $doc_no, $sampling_rod_id, $dies_no) {
        $arr = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => $equipmentcode, "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);
        if ($sampling_rod_id != '' && $dies_no != '') {
            $arr['sampling_rod_id '] = $sampling_rod_id;
            $arr['dies_no'] = $dies_no;
        }
        $this->db->insert("pts_trn_user_activity_log", $arr);
        $actlogtblid = $this->db->insert_id();
        $arr3 = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');
        $this->db->insert("pts_trn_activity_approval_log", $arr3
        );

        $arr2 = array("workflowstatus" => $status_id, "workflownextstep" => $next_step);
        $this->db->where("id", $actlogtblid);
        if ($this->db->update("pts_trn_user_activity_log", $arr2)) {
            return $actlogtblid;
        } else {
            return "";
        }
    }

    public function submit_return_air_filter($isworkflow, $doc_no, $frequency, $product_no, $batch_no, $selectedfilters, $roomcode, $processid, $actid, $actname, $roleid, $empid, $empname, $email, $status_id, $next_step, $remark, $remark2) {
        if ($isworkflow == 0) {
            $arr0 = array("doc_no" => $doc_no, "product_code" => $product_no, "batch_no" => $batch_no, "frequency_id" => $frequency, "filter" => $selectedfilters, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "room_code" => $roomcode);

            if ($this->db->insert("pts_trn_return_air_filter", $arr0)) {
                $re_air_filter_tblid = $this->db->insert_id();
                return $re_air_filter_tblid;
            } else {
                return "";
            }
        } else {
            $arr0 = array("doc_no" => $doc_no, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "frequency_id" => $frequency, "filter" => $selectedfilters, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'yes');
            $this->db->insert("pts_trn_return_air_filter", $arr0);
            $re_air_filter_tblid = $this->db->insert_id();
            if ($re_air_filter_tblid > 0) {
                $arr = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);
                $this->db->insert("pts_trn_user_activity_log", $arr);
                $actlogtblid = $this->db->insert_id();

                $arr3 = array("usr_activity_log_id" => $actlogtblid, "approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');
                if ($this->db->insert("pts_trn_activity_approval_log", $arr3)) {
                    return $actlogtblid;
                } else {
                    return "";
                }
            } else {
                return "";
            }
        }
    }

    /**
     * Takeover running activity
     * Created By :Bhupendra kumar
     * Date:28-01-2020
     */
    public function Senttotakeover($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email) {
        $arr = array("usr_activity_log_id" => $actlogtblid, "activity_status" => 'Takeover', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "remarks" => $remark, "activity_time" => time());
        if ($this->db->insert("pts_trn_activity_log", $arr)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Get Last Activity Product and Batch Number
     * Created By :Rahul Chauhan
     * Date:05-02-2020
     */
    public function GetLastProductAndBatchNumber($roomlogactivity) {
        $result = array();
        $this->db->select("*");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->where_in("roomlogactivity", $roomlogactivity);
        $this->db->order_by("id", 'DESC');
        $result = $this->db->get()->row_array();

        return $result;
    }

    /**
     * Get Last Activity Product and Batch Number equipment Wise
     * Created By :Rahul Chauhan
     * Date:05-02-2020
     */
    public function GetLastBactProductActivity($equipment_codess) {
        $result = array();
        $this->db->select("*");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->where_in("equip_code", $equipment_codess);
        $this->db->order_by("id", 'DESC');
        $result = $this->db->get()->result_array();

        return $result;
    }

    /**
     * Get Air filter last Activity based on room
     * Created By :Rahul Chauhan
     * Date:11-02-2020
     */
    public function GetLastFilterActivity($room_code, $table_name) {
        $result = array();
        $this->db->select($table_name . ".*,pts_trn_user_activity_log.*");
        $this->db->from($table_name);
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=$table_name.doc_no");
        $this->db->where("$table_name.room_code", $room_code);
        $this->db->where("$table_name.status", "active");
        $this->db->where("$table_name.is_in_workflow", "yes");
        $result = $this->db->get()->result_array();
        return $result;
    }

//Comman function to submit form details and set for approval if it is in workflow
    public function Com_fun_to_submit_log_details($isworkflow, $tblname, $data, $data2, $data3) {
        if ($isworkflow == 0) {
            if (!empty($data[0])) {
                if ($this->db->insert_batch($tblname, $data)) {
                    $re_air_filter_tblid = $this->db->insert_id();
                    return $re_air_filter_tblid;
                } else {
                    return "";
                }
            } else {
                if ($this->db->insert($tblname, $data)) {
                    $re_air_filter_tblid = $this->db->insert_id();
                    return $re_air_filter_tblid;
                } else {
                    return "";
                }
            }
        } else {
            if (!empty($data[0])) {
                foreach ($data as $key => $value) {
                    $data[$key]['is_in_workflow'] = 'yes';
                }
                $this->db->insert_batch($tblname, $data);
            } else {
                $data['is_in_workflow'] = 'yes';
                $this->db->insert($tblname, $data);
            }
            $re_air_filter_tblid = $this->db->insert_id();
            if ($re_air_filter_tblid > 0) {
                $this->db->insert("pts_trn_user_activity_log", $data2);
                $actlogtblid = $this->db->insert_id();
                $data3['usr_activity_log_id'] = $actlogtblid;
                if ($this->db->insert("pts_trn_activity_approval_log", $data3)) {
                    return $actlogtblid;
                } else {
                    return "";
                }
            } else {
                return "";
            }
        }
    }

    /**
     * Get Last Record matrial from pts_trn_material_retreival_relocation table based on Material code
     * Created By : Bhupendra Kumar
     * Date:18feb2020
     */
    public function GetLastMatRecord($matcode) {
        $result = array();
        $this->db->select("*");
        $this->db->from("pts_trn_material_retreival_relocation");
        $this->db->where("material_code", $matcode);
        $this->db->order_by("id", 'DESC');
        $this->db->limit(1);
        $result = $this->db->get()->result_array();
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
//        echo $this->db->last_query();exit;
    }

    /**
     * Get Process log Record for Report
     * Created By : Bhupendra kumar
     * Date:19-02-2020
     */
    public function GetProcessLogReport($room_code, $equip_code, $type, $post) {
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_user_activity_log.id,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%m-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%m-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
        $this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
        $this->db->where("pts_trn_user_activity_log.is_qa_approve", 'yes');
        $this->db->where("pts_trn_user_activity_log.room_code", $room_code);
        //$this->db->where("pts_mst_activity.activity_url",$type);
        $this->db->where_in("pts_trn_user_activity_log.equip_code", $equip_code);
        if (!empty($start_date && $end_date)) {
            $this->db->where('from_unixtime(pts_trn_user_activity_log.activity_start, "%Y-%m-%d") >=', $start_date);
            $this->db->where('from_unixtime(pts_trn_user_activity_log.activity_start, "%Y-%m-%d") <=', $end_date);
        }
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Get Swab Sample Record for Report
     * Created By : Bhupendra kumar
     * Date:21-02-2020
     */
    public function GetSwabSampleReport($post) {
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        // print_r($post);
        // die();
        $result = array();
        $this->db->select("*");
        //
        $this->db->from("pts_trn_swab_sample_record");
        $this->db->where('date(created_on) >=', $start_date);
        $this->db->where('date(created_on) <=', $end_date);
        // $this->db->where("date(created_on) BETWEEN $start_date AND $end_date");
        $result = $this->db->get()->result_array();
        // echo $this->db->last_query();exit;
        if (!empty($result)) {
            return $result;
        } else {
            return array();
        }
    }

    /**
      <<<<<<< HEAD
     * Function to enter detail of user who is going in the room  
     * Created By :Bhupendra kumar
     * Date:26-02-2020
     */
    public function roomin($roomcode, $roleid, $empid, $empname, $email, $session_id) {
        $arr = array("room_code" => $roomcode, "role_id" => $roleid, "emp_code" => $empid, "emp_name" => $empname, "emp_email" => $email, "created_by" => $empid, "room_in_time" => time(), "session_id" => $session_id);
        if ($this->db->insert("pts_trn_emp_roomin", $arr)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Function to enter detail of user who is going in the room  
     * Created By :Bhupendra kumar
     * Date:26-02-2020
     */
    public function roominandupdate($roomcode, $roleid, $empid, $empname, $email, $session_id) {
        $arr = array("isactive" => 0, "room_out_time" => time());
        $this->db->where("role_id", $roleid);
        $this->db->where("emp_code", $empid);
        $this->db->where("emp_name", $empname);
        $this->db->where("emp_email", $email);
        $this->db->where("room_code", $roomcode);
        if ($this->db->update("pts_trn_emp_roomin", $arr)) {
            $arr2 = array("room_code" => $roomcode, "role_id" => $roleid, "emp_code" => $empid, "emp_name" => $empname, "emp_email" => $email, "created_by" => $empid, "room_in_time" => time(), "session_id" => $session_id);
            if ($this->db->insert("pts_trn_emp_roomin", $arr2)) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }

    /**
     * Function to enter detail of user who is going in the room  
     * Created By :Bhupendra kumar
     * Date:26-02-2020
     */
    public function roomout($roleid, $empid, $empname, $email) {
        $arr = array("isactive" => 0, "room_out_time" => time());
        $this->db->where("role_id", $roleid);
        $this->db->where("emp_code", $empid);
        $this->db->where("emp_name", $empname);
        $this->db->where("emp_email", $email);
        //$this->db->where("room_code",$roomcode);
        //$this->db->where("session_id",$session_id);
        if ($this->db->update("pts_trn_emp_roomin", $arr)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Get Room In User List 
     * Created By :Rahul Chauhan
     * Date:28-02-2020
     */
    public function GetRoomInUserDetail() {
        $result = array();
        $this->db->select("pts_trn_emp_roomin.*,mst_role.role_description");
        $this->db->from("pts_trn_emp_roomin");
        $this->db->join("mst_role", "mst_role.id=pts_trn_emp_roomin.role_id");
        $this->db->where("pts_trn_emp_roomin.isactive", '1');
        $this->db->where("pts_trn_emp_roomin.room_out_time", null);
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Get Running Batch Details 
     * Created By :Bhupendra Kumar
     * Date:02-03-2020
     */
    public function GetrunningBatchData() {
        $result = array();
        $this->db->select("pts_trn_batchin.*");
        $this->db->from("pts_trn_batchin");
        $this->db->where("pts_trn_batchin.isactive", '1');
        $this->db->where("pts_trn_batchin.batch_out_time", null);
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Function to start batch in a room
     * Created By :Bhupendra kumar
     * Date:29-02-2020
     */
    public function batchin($roomcode, $batchcod, $prodcode, $product_desc, $roleid, $empid, $empname, $email) {
        $arr = array("room_code" => $roomcode, "batch_no" => $batchcod, "product_code" => $prodcode, "product_desc" => $product_desc, "role_id" => $roleid, "emp_code" => $empid, "emp_name" => $empname, "emp_email" => $email, "created_by" => $empid, "batch_in_time" => time());
        if ($this->db->insert("pts_trn_batchin", $arr)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Function to enter detail of user who is going in the room  
     * Created By :Bhupendra kumar
     * Date:26-02-2020
     */
    public function batchout($roleid, $empid, $empname, $email) {
        $arr = array("isactive" => 0, "batch_out_time" => time());
        $this->db->where("role_id", $roleid);
        $this->db->where("emp_code", $empid);
        $this->db->where("emp_name", $empname);
        $this->db->where("emp_email", $email);
        //$this->db->where("room_code",$roomcode);
        //$this->db->where("session_id",$session_id);
        if ($this->db->update("pts_trn_batchin", $arr)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /*     * Get Pre Filter Report Data
     * Created By : Devendra Singh
     * Edited By: ****
     * Created Date:29-02-2020
     * Edit Date:***
     */

/*   * Get Pre Filter Report Data
   * Created By : Devendra Singh
   * Edited By: Bhupendra on 11/03/2020
   * Created Date:29-02-2020
   * Edit Date:***
   */
  public function GetPreFilterLogReportLAF($post) {
    $area = $post['area'];
    $year = $post['year'];
    $month = $post['month'];
    $this->db->select("date(pts_trn_pre_filter_cleaning.created_on) as start_date, pts_trn_pre_filter_cleaning.product_code, pts_trn_pre_filter_cleaning.batch_no,pts_trn_pre_filter_cleaning.doc_no,pts_trn_pre_filter_cleaning.room_code,pts_trn_pre_filter_cleaning.selection_status,pts_trn_pre_filter_cleaning.pre_filter_cleaning,pts_trn_pre_filter_cleaning.outer_surface_cleaning,
pts_trn_pre_filter_cleaning.done_by_user_name,pts_trn_activity_approval_log.approval_status as apprl_status,pts_trn_activity_approval_log.user_name as checked_by,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%m-%Y') as checked_date,pts_trn_activity_approval_log.workflow_type,
pts_trn_user_activity_log.id as usr_act_log_id,pts_trn_user_activity_log.user_name as usr_act_log_user_name,pts_trn_activity_approval_log.activity_remarks as usr_act_log_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%m-%Y') as usr_act_log_start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as
usr_act_log_start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%m-%Y') as usr_act_log_stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as
usr_act_log_stop_time,pts_trn_user_activity_log.is_qa_approve as usr_act_log_is_qa_appl");
    $this->db->from("pts_trn_pre_filter_cleaning");
    $this->db->join("pts_trn_user_activity_log","pts_trn_user_activity_log.doc_id=pts_trn_pre_filter_cleaning.doc_no",'left');
    $this->db->join("pts_trn_activity_approval_log","pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id",'left');
    if(!empty($year) && $year != 'undefined'){
      $this->db->where('year(pts_trn_pre_filter_cleaning.created_on)', $year);
    }
    if(!empty($month) && $month != 'undefined'){
      $this->db->where('month(pts_trn_pre_filter_cleaning.created_on)', $month);
    }
    $result = $this->db->get()->result_array();
    //echo $this->db->last_query();exit;
    return $result;
  }

  /*   * Get Vertical Sampler Report Data
   * Created By : Bhupendra kumar
   * Edited By: ****
   * Created Date:11-03-2020
   * Edit Date:***
   */
  public function GetVerticalSamplerReport($post) { 
    $equipment_code = $post['equipment_code'];
    $start_date = $post['start_date'];
    $end_date = $post['end_date'];
    $this->db->select("pts_trn_user_activity_log.id,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%m-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%m-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,pts_trn_user_activity_log.dies_no");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->join("pts_trn_activity_approval_log","pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id",'left');
        $this->db->join("pts_mst_activity","pts_trn_user_activity_log.activity_id=pts_mst_activity.id",'left');
        $this->db->where("pts_trn_user_activity_log.status",'active');
        $this->db->where("pts_trn_user_activity_log.workflownextstep",-1);
        $this->db->where("pts_trn_user_activity_log.is_qa_approve",'yes');
        $this->db->where("pts_trn_user_activity_log.sampling_rod_id is NOT NULL", NULL, FALSE);
        $this->db->where("pts_trn_user_activity_log.dies_no is NOT NULL", NULL, FALSE);
        //$this->db->where("pts_trn_user_activity_log.room_code",$room_code);
        //$this->db->where("pts_mst_activity.activity_url",$type);
        $this->db->where_in("pts_trn_user_activity_log.equip_code",$equipment_code);
        if(!empty($start_date && $end_date)){
          $this->db->where('from_unixtime(pts_trn_user_activity_log.activity_start, "%Y-%m-%d") >=', $start_date);
          $this->db->where('from_unixtime(pts_trn_user_activity_log.activity_start, "%Y-%m-%d") <=', $end_date);
        }
        $result = $this->db->get()->result_array();
       // echo $this->db->last_query();exit;
        return $result;
  }
    
    /**
     * Audit Trail Report Data
     * Created By : Bhupendra kumar
     * Created Date:03-03-2020
     */
    public function auditTrailReportList($post) {
        $user_name = $post['user_name'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
//    echo '<pre>';print_r($post);exit;
        $this->db->select("doc_id,pts_trn_room_log_activity.activity_name as logname,pts_mst_activity.activity_name as actname,approval_status,workflow_type,pts_trn_activity_approval_log.user_name, from_unixtime(activity_time, '%Y-%m-%d %H:%i:%s') as datetime,from_unixtime(activity_time, '%Y-%m-%d') as activity_start,room_code,pts_trn_activity_approval_log.activity_remarks");
        $this->db->from("pts_trn_activity_approval_log");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_activity_approval_log.usr_activity_log_id=pts_trn_user_activity_log.id", 'left');
        $this->db->join("pts_mst_activity", "pts_mst_activity.id=pts_trn_user_activity_log.activity_id", 'left');
        $this->db->join("pts_trn_room_log_activity", "pts_trn_room_log_activity.id=pts_trn_user_activity_log.roomlogactivity", 'left');
        $this->db->order_by('datetime', 'DESC');

        if (isset($post['limit']) && isset($post['page']) && $post['limit'] > 0 && $post['page'] > 0) {
            $offset = ( $post['page'] - 1 ) * $post['limit'];
            $this->db->limit($post['limit'], $offset);
        }

        if ($start_date != '' && $end_date != '') {
            $this->db->where('from_unixtime(activity_start, "%Y-%m-%d") >=', $start_date);
            $this->db->where('from_unixtime(activity_start, "%Y-%m-%d") <=', $end_date);
        }
        if ($user_name != '') {
            $this->db->where('pts_trn_activity_approval_log.user_name', $user_name);
        }


        $result = $this->db->get()->result_array();
//   echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * get lotal number of result of audit trail
     * Created By : Bhupendra kumar
     * Created Date:03-03-2020
     */
    public function getTotalnumberofaudittrail($post) {
        $user_name = $post['user_name'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("doc_id,pts_trn_room_log_activity.activity_name as logname,pts_mst_activity.activity_name as actname,approval_status,workflow_type,pts_trn_activity_approval_log.user_name, from_unixtime(activity_time, '%Y-%m-%d %H:%i:%s') as datetime,from_unixtime(activity_time, '%Y-%m-%d') as activity_start,room_code,pts_trn_activity_approval_log.activity_remarks");
        $this->db->from("pts_trn_activity_approval_log");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_activity_approval_log.usr_activity_log_id=pts_trn_user_activity_log.id", 'left');
        $this->db->join("pts_mst_activity", "pts_mst_activity.id=pts_trn_user_activity_log.activity_id", 'left');
        $this->db->join("pts_trn_room_log_activity", "pts_trn_room_log_activity.id=pts_trn_user_activity_log.roomlogactivity", 'left');
        $this->db->order_by('datetime', 'DESC');
        if ($start_date != '' && $end_date != '') {
            $this->db->where('from_unixtime(activity_start, "%Y-%m-%d") >=', $start_date);
            $this->db->where('from_unixtime(activity_start, "%Y-%m-%d") <=', $end_date);
        }
        if ($user_name != '') {
            $this->db->where('pts_trn_activity_approval_log.user_name', $user_name);
        }
        $result = $this->db->get();
        $totalcount = $result->num_rows();
        //echo $this->db->last_query();exit;
        return $totalcount;
    }

    /**
     * Get Equipment Apparatus Log Report
     * Created By : Devendra Singh
     * Edited By: ****
     * Created Date:29-02-2020
     * Edit Date:***
     */
    public function GetEquipmentApparatusLogReport($post) {
        $apparatus_no = $post['apparatus_no'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_equip_appratus_log.id,pts_trn_equip_appratus_log.created_on as created_date,pts_trn_equip_appratus_log.product_code, pts_trn_equip_appratus_log.batch_no,pts_trn_equip_appratus_log.stage,
pts_trn_equip_appratus_log.done_by_user_name,pts_trn_equip_appratus_log.done_by_remark,
pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,
pts_trn_user_activity_log.id as usr_act_log_id,pts_trn_user_activity_log.user_name as usr_act_log_user_name,pts_trn_user_activity_log.activity_remarks as usr_act_log_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%m-%Y') as usr_act_log_start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as
usr_act_log_start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%m-%Y') as usr_act_log_stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as
usr_act_log_stop_time,pts_trn_user_activity_log.is_qa_approve as usr_act_log_is_qa_appl");
        $this->db->from("pts_trn_equip_appratus_log");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_equip_appratus_log.doc_no", 'left');
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_activity_approval_log.usr_activity_log_id = pts_trn_user_activity_log.id", 'left');
        // $this->db->where('pts_trn_equip_appratus_log.equip_appratus_no', 'APPARATUS001');
        if (!empty($start_date && $end_date)) {
//        $this->db->where('pts_trn_equip_appratus_log.created_on BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d 23:59:59', strtotime($end_date)).'"');
            $this->db->where('pts_trn_equip_appratus_log.created_on>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            $this->db->where('pts_trn_equip_appratus_log.created_on<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }
        if (!empty($apparatus_no)) {
            $this->db->where('pts_trn_equip_appratus_log.equip_appratus_no', $apparatus_no);
        }
        $result = $this->db->get()->result_array();
//     echo $this->db->last_query();exit;
        return $result;
    }

    /**
     * Get Instrument Log Report
     * Created By : Rahul Chauhan
     * Created Date:11-03-2020
     */
    public function GetInstrumentReport($post) {
        $apparatus_no = $post['instrument_no'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_instrument_log_register.id,pts_trn_instrument_log_register.created_on as created_date,pts_trn_instrument_log_register.product_code, pts_trn_instrument_log_register.batch_no,pts_trn_instrument_log_register.stage,
pts_trn_instrument_log_register.done_by_user_name,pts_trn_instrument_log_register.done_by_remark,pts_trn_instrument_log_register.result_left,pts_trn_instrument_log_register.result_right,pts_trn_instrument_log_register.test,
pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,
pts_trn_user_activity_log.id as usr_act_log_id,pts_trn_user_activity_log.user_name as usr_act_log_user_name,pts_trn_user_activity_log.activity_remarks as usr_act_log_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%m-%Y') as usr_act_log_start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as
usr_act_log_start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%m-%Y') as usr_act_log_stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as
usr_act_log_stop_time,pts_trn_user_activity_log.is_qa_approve as usr_act_log_is_qa_appl");
        $this->db->from("pts_trn_instrument_log_register");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_instrument_log_register.doc_no", 'left');
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_activity_approval_log.usr_activity_log_id = pts_trn_user_activity_log.id", 'left');
        // $this->db->where('pts_trn_equip_appratus_log.equip_appratus_no', 'APPARATUS001');
        if (!empty($start_date && $end_date)) {
//        $this->db->where('pts_trn_equip_appratus_log.created_on BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d 23:59:59', strtotime($end_date)).'"');
            $this->db->where('pts_trn_instrument_log_register.created_on>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            $this->db->where('pts_trn_instrument_log_register.created_on<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }
        if (!empty($apparatus_no)) {
            $this->db->where('pts_trn_instrument_log_register.instrument_code', $apparatus_no);
        }
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }
    
     /**
     * Vaccume Cleaner Report
     * Created By : Rahul Chauhan
     * Created Date:12-03-2020
     */
    public function getVaccumeReport($post) {
        $vaccume_no = $post['vaccume_no'];
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_vaccum_cleaner.id,pts_trn_vaccum_cleaner.created_on as created_date,pts_trn_vaccum_cleaner.product_code, pts_trn_vaccum_cleaner.batch_no,pts_trn_vaccum_cleaner.vaccum_cleaner_code,
pts_trn_vaccum_cleaner.done_by_user_name,pts_trn_vaccum_cleaner.done_by_remark,
pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,
pts_trn_user_activity_log.id as usr_act_log_id,pts_trn_user_activity_log.user_name as usr_act_log_user_name,pts_trn_user_activity_log.activity_remarks as usr_act_log_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%m-%Y') as usr_act_log_start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as
usr_act_log_start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%m-%Y') as usr_act_log_stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as
usr_act_log_stop_time,pts_trn_user_activity_log.is_qa_approve as usr_act_log_is_qa_appl");
        $this->db->from("pts_trn_vaccum_cleaner");
        $this->db->join("pts_trn_user_activity_log", "pts_trn_user_activity_log.doc_id=pts_trn_vaccum_cleaner.doc_no", 'left');
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_activity_approval_log.usr_activity_log_id = pts_trn_user_activity_log.id", 'left');
        // $this->db->where('pts_trn_equip_appratus_log.equip_appratus_no', 'APPARATUS001');
        if (!empty($start_date && $end_date)) {
//        $this->db->where('pts_trn_equip_appratus_log.created_on BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d 23:59:59', strtotime($end_date)).'"');
            $this->db->where('pts_trn_vaccum_cleaner.created_on>=', date('Y-m-d 00:00:00', strtotime($start_date)));
            $this->db->where('pts_trn_vaccum_cleaner.created_on<=', date('Y-m-d 23:59:59', strtotime($end_date)));
        }
        if (!empty($apparatus_no)) {
            $this->db->where('pts_trn_vaccum_cleaner.vaccum_cleaner_code', $vaccume_no);
        }
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }


   /**
   * Get LAF Pressure Diff Data
   * Created By : Bhupendra Kumar
   * Edited By: ****
   * Created Date:12-03-2020
   * Edit Date:***
   */
  public function lafPressureDiffReport($post) {
    $area = $post['area'];
    $year = $post['year'];
    $month = $post['month'];
    $this->db->select("date(pts_trn_pre_filter_cleaning.created_on) as start_date, pts_trn_pre_filter_cleaning.product_code, pts_trn_pre_filter_cleaning.batch_no,pts_trn_pre_filter_cleaning.doc_no,pts_trn_pre_filter_cleaning.room_code,pts_trn_pre_filter_cleaning.selection_status,pts_trn_pre_filter_cleaning.pre_filter_cleaning,pts_trn_pre_filter_cleaning.outer_surface_cleaning,
pts_trn_pre_filter_cleaning.done_by_user_name,pts_trn_activity_approval_log.approval_status as apprl_status,pts_trn_activity_approval_log.user_name as checked_by,from_unixtime(pts_trn_activity_approval_log.activity_time, '%d-%m-%Y') as checked_date,pts_trn_activity_approval_log.workflow_type,
pts_trn_user_activity_log.id as usr_act_log_id,pts_trn_user_activity_log.user_name as usr_act_log_user_name,pts_trn_activity_approval_log.activity_remarks as usr_act_log_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%m-%Y') as usr_act_log_start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as
usr_act_log_start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%m-%Y') as usr_act_log_stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as
usr_act_log_stop_time,pts_trn_user_activity_log.is_qa_approve as usr_act_log_is_qa_appl");
    $this->db->from("pts_trn_pre_filter_cleaning");
    $this->db->join("pts_trn_user_activity_log","pts_trn_user_activity_log.doc_id=pts_trn_pre_filter_cleaning.doc_no",'left');
    $this->db->join("pts_trn_activity_approval_log","pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id",'left');
    if(!empty($year) && $year != 'undefined'){
      $this->db->where('year(pts_trn_pre_filter_cleaning.created_on)', $year);
    }
    if(!empty($month) && $month != 'undefined'){
      $this->db->where('month(pts_trn_pre_filter_cleaning.created_on)', $month);
    }
    $result = $this->db->get()->result_array();
    //echo $this->db->last_query();exit;
    return $result;
  }

/**
     * Get Previous Product and Batch Number 
     * Created By : Bhupendra kumar
     * Date: 15-02-2020 
     */
    public function GetPreBactProduct() {
        $result = array();
        $this->db->select("*");
        $this->db->from("pts_trn_vaccum_cleaner");
        $this->db->join("mst_product","pts_trn_vaccum_cleaner.product_code=mst_product.product_code",'left');
        $this->db->order_by("pts_trn_vaccum_cleaner.id", 'DESC');
        $this->db->limit(1);
        $result = $this->db->get()->result_array();
        return $result;
    }

    /**
     * Get Previous Product and Batch Number 
     * Created By : Bhupendra kumar
     * Date: 15-02-2020 
     */
    public function GetPreBactProductofPrefilter() {
        $result = array();
        $this->db->select("*");
        $this->db->from("pts_trn_pre_filter_cleaning");
        $this->db->join("mst_product","pts_trn_pre_filter_cleaning.product_code=mst_product.product_code",'left');
        $this->db->order_by("pts_trn_pre_filter_cleaning.id", 'DESC');
        $this->db->where('selection_status','from');
        $this->db->limit(1);
        $result = $this->db->get()->result_array();
        return $result;
    }

    /**
     * Get Previous Product and Batch Number 
     * Created By : Bhupendra kumar
     * Date: 15-02-2020 
     */
    public function GetPreBactProductofprocesslog() {
        $result = array();
        $this->db->select("*");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->join("mst_product","pts_trn_user_activity_log.product_code=mst_product.product_code",'left');
        $this->db->order_by("pts_trn_user_activity_log.id", 'DESC');
        $this->db->limit(1);
        $result = $this->db->get()->result_array();
        return $result;
    }
    
    /**
     * Get Vertical Sampler log for Report
     * Created By : Bhupendra kumar
     * Date:19-02-2020
     */
    public function verticalSamplingReport($sampling_no, $post) {
        $start_date = $post['start_date'];
        $end_date = $post['end_date'];
        $this->db->select("pts_trn_user_activity_log.id,pts_mst_activity.activity_url,pts_mst_activity.activity_name,pts_trn_user_activity_log.room_code,pts_trn_user_activity_log.equip_code,pts_trn_user_activity_log.product_code,pts_trn_user_activity_log.batch_no,pts_trn_user_activity_log.user_name,pts_trn_user_activity_log.activity_remarks,from_unixtime(pts_trn_user_activity_log.activity_start, '%d-%m-%Y') as start_date,from_unixtime(pts_trn_user_activity_log.activity_start, '%H:%i:%s') as start_time,from_unixtime(pts_trn_user_activity_log.activity_stop, '%d-%m-%Y') as stop_date,from_unixtime(pts_trn_user_activity_log.activity_stop, '%H:%i:%s') as stop_time,pts_trn_user_activity_log.is_qa_approve,pts_trn_activity_approval_log.approval_status,pts_trn_activity_approval_log.user_name,pts_trn_activity_approval_log.activity_remarks,pts_trn_activity_approval_log.workflow_type,pts_trn_user_activity_log.dies_no,pts_trn_user_activity_log.sampling_rod_id");
        $this->db->from("pts_trn_user_activity_log");
        $this->db->join("pts_trn_activity_approval_log", "pts_trn_user_activity_log.id = pts_trn_activity_approval_log.usr_activity_log_id", 'left');
        $this->db->join("pts_mst_activity", "pts_trn_user_activity_log.activity_id=pts_mst_activity.id", 'left');
        $this->db->where("pts_trn_user_activity_log.status", 'active');
        $this->db->where("pts_trn_user_activity_log.workflownextstep", -1);
        $this->db->where("pts_trn_user_activity_log.is_qa_approve", 'yes');
        //$this->db->where("pts_mst_activity.activity_url",$type);
        $this->db->where_in("pts_trn_user_activity_log.sampling_rod_id", $sampling_no);
        if (!empty($start_date && $end_date)) {
            $this->db->where('from_unixtime(pts_trn_user_activity_log.activity_start, "%Y-%m-%d") >=', $start_date);
            $this->db->where('from_unixtime(pts_trn_user_activity_log.activity_start, "%Y-%m-%d") <=', $end_date);
        }
        $result = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        return $result;
    }

}
