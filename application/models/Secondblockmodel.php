<?php

class Secondblockmodel extends CI_Model {

public function insertmattransferdtl($equipcode,$sop_code,$docid,$date,$time,$dpvalue,$ensuredby,$matdtl,$ftime,$ttime,$Sagent,$prfmdby){

    $arr = array("mat_eqip_dtl"=>$matdtl,"mat_eqip_rec_transfer_FT"=>$ftime,"mat_eqip_rec_transfer_TT"=>$ttime,"sanitization_agent"=>$Sagent,"performedby"=>$prfmdby,"created_by"=>$prfmdby);

    if ($this->db->insert("trn_material_transfer_dtl",$arr)) {
    	$last_id = $this->db->insert_id();
    	$arr2 = array("mat_trans_tbl_id"=>$last_id,"equip_code"=>$equipcode,"sop_code"=>$sop_code,"documentno"=>$docid,"date"=>$date,"time"=>$time,"dpvalue"=>$dpvalue,"ensuredby"=>$ensuredby,"created_by"=>$ensuredby);

    	if ($this->db->insert("trn_dpmonitoring",$arr2)) 
    	{
        	/*$this->db->where("id",22);
			$mres = $this->db->get("mst_messages");
			$msg = $mres->row_array();
        	$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);*/
        	$response = array("status"=>1);
     	} 
    	else 
    	{
        	$response = array("status"=>0);
  		}
  	}

    return $response;
	}

	public function insertDP_monitoringdtl($equipcode,$sop_code,$docid,$date,$time,$dpvalue,$ensuredby)
	{
    	$arr2 = array("equip_code"=>$equipcode,"sop_code"=>$sop_code,"documentno"=>$docid,"date"=>$date,"time"=>$time,"dpvalue"=>$dpvalue,"ensuredby"=>$ensuredby,"created_by"=>$ensuredby);
    	if ($this->db->insert("trn_dpmonitoring",$arr2)) 
    	{
        	$response = array("status"=>1);
     	} 
    	else 
    	{
        	$response = array("status"=>0);
  		}

    return $response;
	}

	public function insertCSdtl($equipcode,$sop_code,$docid,$date,$lotno,$starttime,$empcode,$empname)
	{
    	$arr2 = array("equipcode"=>$equipcode,"sop_code"=>$sop_code,"documentno"=>$docid,"date"=>$date,"lotno"=>$lotno,"strarttime"=>$starttime,"startby"=>$empname,"created_by"=>$empcode);
    	if ($this->db->insert("trn_cleaningandsanitization",$arr2)) 
    	{
        	$response = array("status"=>1);
     	} 
    	else 
    	{
        	$response = array("status"=>0);
  		}

    return $response;
	}

	public function updateCSdtl($id,$now,$empcode,$empname)
	{
    	$arr2 = array("endtime"=>$now,"endby"=>$empname);
    	$this->db->where("id",$id);
    	if ($this->db->update("trn_cleaningandsanitization",$arr2)) 
    	{
        	$response = array("status"=>1);
     	} 
    	else 
    	{
        	$response = array("status"=>0);
  		}

    return $response;
	}

	public function get_dtl1($tblname)
	{
		$this->db->where('is_active',1);
		$this->db->order_by("created_on","DESC");
		$q = $this->db->get($tblname);

		return $q;
	}

	public function get_dtl5_withorderby($tblname,$orderbycol,$type,$col2,$val2)
	{
		$this->db->where($col2.">=",$val2);
		$q = $this->db->get($tblname);
		$this->db->order_by($orderbycol,$type);
		return $q;
	}

	public function get_cleaningtype()
	{
		$this->db->distinct();
		$this->db->select("type, priority");
		$this->db->from("mst_inj_cleaning");

		//$this->db->select("distinct(type) as type , priority as priority from mst_inj_cleaning");
		$q = $this->db->get();
		return $q;
	}

	public function getmatdtl(){
		$this->db->select("dp.equip_code,dp.date,dp.time,dp.dpvalue,dp.ensuredby,mat.id,mat.mat_eqip_dtl,mat.mat_eqip_rec_transfer_FT,mat.mat_eqip_rec_transfer_TT,mat.sanitization_agent,mat.performedby,mat.checkedby_name,mat.remark");
		$this->db->from("trn_material_transfer_dtl as mat");
		$this->db->join("trn_dpmonitoring as dp","mat.id=dp.mat_trans_tbl_id","INNER");
		$this->db->order_by("mat.created_on","DESC");
		$response = $this->db->get();		 
		return $response;
	}

	public function getDPdtl($tblname)
	{
		$this->db->where('is_active',1);
		$this->db->where('mat_trans_tbl_id',null);
		$this->db->order_by("created_on","DESC");
		$q = $this->db->get($tblname);
		return $q;
	}
	
	public function insertCSAAA($date,$solddl,$roomddl,$lotno,$cleantype,$starttime,$empname,$emp_code)
	{
    	$arr2 = array("date"=>$date,"solution_code"=>$solddl,"room_code"=>$roomddl,"cleaning_activity_type"=>$cleantype,"lot_no"=>$lotno,"starttime"=>$starttime,"createdby_name"=>$empname,"created_by"=>$emp_code);
    	if ($this->db->insert("trn_inj_roomcleaningheader",$arr2)) 
    	{
    		$last_id = $this->db->insert_id();
        	$response = array("status"=>1, "last_id"=>$last_id);
     	} 
    	else 
    	{
        	$response = array("status"=>0, "last_id"=>0);
  		}

    return $response;
	}

	public function insertCSAAAdetail($act,$headerid,$empname,$emp_code)
	{
		$timezone = "Asia/Kolkata";
 	    date_default_timezone_set($timezone);
		for ($i = 0; $i < count($act); $i++) {
    		$myArray = explode('###', $act[$i]);
    		$arr = array("header_id"=>$headerid,"cleaning_activity_code"=>$myArray[0],"status"=>$myArray[1],"created_by"=>$empname);
    		$this->db->insert("trn_roomcleaning_detail",$arr);
		}	
   		$arr2 = array("endtime"=>date("Y-m-d H:i:s"));
   		$this->db->where("id",$headerid);
   		if ($this->db->update("trn_inj_roomcleaningheader",$arr2)) 
   		{
       		$response = array("status"=>1);
       	}
       	else 
   		{
       		$response = array("status"=>0);
		}

    return $response;
	}

	public function getCSAAAheaderid($date,$emp_code)
	{
		$this->db->where('is_active',1);
		$this->db->where('date',$date);
		$this->db->where('created_by',$emp_code);
		$this->db->order_by("created_on","DESC");
		$this->db->limit(1);
		$q = $this->db->get('trn_inj_roomcleaningheader');
		return $q;
	}

	public function getCSAAAdtl($id)
	{
		$this->db->select("h.id,h.date,h.solution_code,h.room_code,h.cleaning_activity_type,h.lot_no,h.starttime,h.endtime,h.checkedby_name,h.created_by,h.remark,d.cleaning_activity_code,d.status");
		$this->db->from("trn_inj_roomcleaningheader as h");
		$this->db->join("trn_roomcleaning_detail as d","h.id=d.header_id","INNER");
		$this->db->where('h.is_active',1);
		$this->db->where('d.header_id',$id);
		$this->db->order_by("d.created_on","ASC");
		$q= $this->db->get();
		return $q;
	}
}