<?php

class Reportmodel extends CI_Model {

	public function getdata($time,$area_code,$room_code)
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
		if($time=="Today"){
			$q = $this->db->query("SELECT * FROM `operationsqnlog` where area_code='$area_code' and room_code='$room_code' and start_time > DATE_SUB(CURDATE(), INTERVAL 1 DAY) ORDER BY start_time DESC");
		}
		elseif ($time=="Lastweek") {
			$q = $this->db->query("SELECT * FROM `operationsqnlog` where area_code='$area_code' and room_code='$room_code' and start_time > DATE_SUB(CURDATE(), INTERVAL 1 WEEK) ORDER BY start_time DESC");
		}
		elseif ($time=="Lastmonth") {
			$q = $this->db->query("SELECT * FROM `operationsqnlog` where area_code='$area_code' and room_code='$room_code' and start_time > DATE_SUB(CURDATE(), INTERVAL 1 MONTH) ORDER BY start_time DESC");
		}
		elseif ($time=="Custom") {
			$q = "";
		}
		if($q->num_rows() > 0)
		{	
			return $q->result_array();
		}
		else
		{
			return "";			# code...
		}		
	}

	public function getcustomdata($fromtime,$time,$area_code,$room_code)
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);

		$q = $this->db->query("SELECT * FROM `operationsqnlog` where (start_time BETWEEN '$fromtime' and '$time') and area_code='$area_code' and room_code='$room_code' ORDER BY start_time DESC");

		//$q = $this->db->query("SELECT * FROM `operationsqnlog` where start_time>='$fromtime' and start_time<'$time' and area_code='$area_code' and room_code='$room_code' ORDER BY start_time DESC");
		
		if($q->num_rows() > 0)
		{	
			return $q->result_array();
		}
		else
		{
			return "";			# code...
		}		
	}

	public function getstatusreport($time,$area_code,$room_code,$status)
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
    	$q="";
		if($status==1)
		{
			if($time=="Today"){
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,created_on as actiontime,created_by as actionby from getstart_stoped_data where area_code='$area_code' and room_code='$room_code' and status=1 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 DAY) ORDER BY created_on DESC");
			}
			elseif ($time=="Lastweek") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,created_on as actiontime,created_by as actionby,operation_type from getstart_stoped_data where area_code='$area_code' and room_code='$room_code' and status=1 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 WEEK) ORDER BY created_on DESC");
			}
			elseif ($time=="Lastmonth") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,created_on as actiontime,created_by as actionby,operation_type from getstart_stoped_data where area_code='$area_code' and room_code='$room_code' and status=1 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 MONTH) ORDER BY created_on DESC");
			}
			elseif ($time=="All") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,created_on as actiontime,created_by as actionby,operation_type from getstart_stoped_data where area_code='$area_code' and room_code='$room_code' and status=1 ORDER BY created_on DESC");
			}
		}
		elseif ($status==2) {
			if($time=="Today"){
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,created_on as actiontime,created_by as actionby,operation_type from getstart_stoped_data where area_code='$area_code' and room_code='$room_code' and status=2 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 DAY) ORDER BY created_on DESC");
			}
			elseif ($time=="Lastweek") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,created_on as actiontime,created_by as actionby,operation_type from getstart_stoped_data where area_code='$area_code' and room_code='$room_code' and status=2 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 WEEK) ORDER BY created_on DESC");
			}
			elseif ($time=="Lastmonth") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,created_on as actiontime,created_by as actionby,operation_type from getstart_stoped_data where area_code='$area_code' and room_code='$room_code' and status=2 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 MONTH) ORDER BY created_on DESC");
			}
			elseif ($time=="All") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,created_on as actiontime,created_by as actionby,operation_type from getstart_stoped_data where area_code='$area_code' and room_code='$room_code' and status=2 ORDER BY created_on DESC");
			}
		}
		elseif ($status==3) {
			if($time=="Today"){
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=3 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 DAY) ORDER BY created_on DESC");
			}
			elseif ($time=="Lastweek") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=3 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 WEEK) ORDER BY created_on DESC");
			}
			elseif ($time=="Lastmonth") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=3 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 MONTH) ORDER BY created_on DESC");
			}
			elseif ($time=="All") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=3 ORDER BY created_on DESC");
			}
		}
		elseif ($status==4) {
			if($time=="Today"){
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=4 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 DAY) ORDER BY created_on DESC");
			}
			elseif ($time=="Lastweek") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=4 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 WEEK) ORDER BY created_on DESC");
			}
			elseif ($time=="Lastmonth") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=4 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 MONTH) ORDER BY created_on DESC");
			}
			elseif ($time=="All") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=4 ORDER BY created_on DESC");
			}
		}
		elseif ($status==5) {
			if($time=="Today"){
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=5 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 DAY) ORDER BY created_on DESC");
			}
			elseif ($time=="Lastweek") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=5 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 WEEK) ORDER BY created_on DESC");
			}
			elseif ($time=="Lastmonth") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=5 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 MONTH) ORDER BY created_on DESC");
			}
			elseif ($time=="All") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=5 ORDER BY created_on DESC");
			}
		}
		elseif ($status==6) {
			if($time=="Today"){
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=6 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 DAY) ORDER BY created_on DESC");
			}
			elseif ($time=="Lastweek") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=6 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 WEEK) ORDER BY created_on DESC");
			}
			elseif ($time=="Lastmonth") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=6 and created_on > DATE_SUB(CURDATE(), INTERVAL 1 MONTH) ORDER BY created_on DESC");
			}
			elseif ($time=="All") {
			$q = $this->db->query("SELECT product_description,document_no,status_name,batch_no,actiontime,actionby,operation_type from getstatusrecord where area_code='$area_code' and room_code='$room_code' and status=6 ORDER BY created_on DESC");
			}
		}		
		if($q->num_rows() > 0)
		{	
			return $q->result_array();
		}
		else
		{
			return "";			# code...
		}		
	}
}