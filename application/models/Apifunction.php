<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//

class Apifunction extends CI_Model{
	
	public function get_referer(){
		return $_SERVER['HTTP_REFERER'];
	}
	
	public function response($data,$status){
		$this->_code = ($status)?$status:200;
		$this->set_headers();
		echo $data;
		exit;
	}
	
	// Rest validation 
	public function check_variables($name,$value,$numricCheck="no",$customMessage=FALSE){
	    if($customMessage==TRUE){
	        $responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Please enter valid data or consult api document');
	        $this->Apifunction->response($this->Apifunction->json($responseresult), 406);
	    }
	    if($numricCheck=='yes'){
	        $responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => $value.' Should be numeric only');
	        $this->Apifunction->response($this->Apifunction->json($responseresult), 406);
	    }
	    if(empty($name)){
	        $responseresult = array(STATUS => FAIL, "result" => false, MESSAGE =>$value.' is empty Please provide the same');
	        $this->Apifunction->response($this->Apifunction->json($responseresult), 406);
	    }
	}
	
	
	public function get_status_message(){
		$status = array(
				100 => 'Continue',
				101 => 'Switching Protocols',
				200 => 'OK',
				201 => 'Created',
				202 => 'Accepted',
				203 => 'Non-Authoritative Information',
				204 => 'No Content',
				205 => 'Reset Content',
				206 => 'Partial Content',
				300 => 'Multiple Choices',
				301 => 'Moved Permanently',
				302 => 'Found',
				303 => 'See Other',
				304 => 'Not Modified',
				305 => 'Use Proxy',
				306 => '(Unused)',
				307 => 'Temporary Redirect',
				400 => 'Bad Request',
				401 => 'Unauthorized',
				402 => 'Payment Required',
				403 => 'Forbidden',
				404 => 'Not Found',
				405 => 'Method Not Allowed',
				406 => 'Not Acceptable',
				407 => 'Proxy Authentication Required',
				408 => 'Request Timeout',
				409 => 'Conflict',
				410 => 'Gone',
				411 => 'Length Required',
				412 => 'Precondition Failed',
				413 => 'Request Entity Too Large',
				414 => 'Request-URI Too Long',
				415 => 'Unsupported Media Type',
				416 => 'Requested Range Not Satisfiable',
				417 => 'Expectation Failed',
				500 => 'Internal Server Error',
				501 => 'Not Implemented',
				502 => 'Bad Gateway',
				503 => 'Service Unavailable',
				504 => 'Gateway Timeout',
				505 => 'HTTP Version Not Supported');
		return ($status[$this->_code])?$status[$this->_code]:$status[500];
	}
	
	public function set_headers(){
		header("HTTP/1.1 ".$this->_code." ".$this->get_status_message());
		header("Content-Type:".$this->_content_type);
	}
	
	public function json($data){
		if(is_array($data)){
			return json_encode($data);
		}
	}
	
	
	function cleanString($string) {
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
	
	// Login submit function will check user atuthentication if success then return user data from ldap
	public function loginsubmit($username,$password){
		$username=str_replace("@timesinternet.in","",$username);
		$username=trim($username);
		$ldap = ldap_connect(LDAP_IP,LDAP_PORT);//connect with ldap
		if(empty($ldap)){
			$responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Ldap Connection failed');
			$this->Apifunction->response($this->Apifunction->json($responseresult), 404);
		}
		
		$ldaprdn = LDAP_CONSTANT_NAME . "\\" . $username;
		ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
		ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);
		$bind = @ldap_bind($ldap, $ldaprdn, $password);// bind with ldap data
		//
		// if user found in ldap
		if ($bind) {
			$filter="(sAMAccountName=$username)";
			$result = @ldap_search($ldap,"dc=timesgroup,dc=COM",$filter);
			@ldap_sort($ldap,$result,"sn");
			$info = ldap_get_entries($ldap, $result);
			$displayName=$info[0]["displayname"][0];
			$userprincipalname=$info[0]["userprincipalname"][0];
			$title=$info[0]["title"][0];
			$division=(!empty($info[0]["division"][0])?$info[0]["division"][0]:'');
			$company=$info[0]["company"][0];
			$fullname=$info[0]["cn"][0];
			$physicalLocation=$info[0]['physicaldeliveryofficename'][0];
			
			/* 
			if($username=='itdesk.noida'){
				$department='TIL - SYSTEM & NETWORK ADMIN';
				$_SESSION['itdesk']='itdesk.noida';
			}else{
				$department=$info[0]["department"][0];
				unset($_SESSION['itdesk']);
			} 
			*/
			
			$username=$info[0]["displayname"][0];
			$user=$info[0]["samaccountname"][0];
			$mobile=(!empty($info[0]["mobile"][0])?$info[0]["mobile"][0]:'');
			$department=$info[0]["department"][0];
			$result=array();
			$result['userprincipalname']=$userprincipalname;
			$result['userlocation']=trim(str_replace("TIL -","",$physicalLocation));;
			$result['title']=$title;
			$result['fullname']=$fullname;
			$result['division']=$division;
			$result['company']=$company;
			$result['department']=$department;
			$result['user']=$user;
			$result['displayName']=$displayName;
			$result['mobile']=$mobile;
			$department_head=$this->Common->getDepartmentHOD($result['department']);
			$result['HOD']=$department_head['0']['deptheademail'];
			$tablename='userlog';
			$postdata=array();
			$postdata['username']=$displayName;
			$postdata['deparment']=$department;
			$postdata['email']=$userprincipalname;
			$postdata['ip']=$_SERVER['REMOTE_ADDR'];
			$getUserAccessData=$this->Common->addToTable($tablename,$postdata);//insert into log table
			$tokendetails=$this->GenricFunction->getUpdateTokendetails($userprincipalname);// insert into token
			$extraparameter=array();
			$extraparameter['emailid']=$userprincipalname;
			$tokenresult=$this->GenricFunction->getRecordwithmulticondition('token',$extraparameter);
			$result['tokenid']=$tokenresult['tokenid'];
			$result['expire_time']=$tokenresult['expire_time'];
			$responseresult = array(STATUS => SUCCESS, "result" => $result);
			$this->Apifunction->response($this->Apifunction->json($responseresult), 200);
		}
		else {
			$responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Invalid Username or Password');
			$this->Apifunction->response($this->Apifunction->json($responseresult), 404);
		}
	}
	
	
	// login submit function
	public function Validateusertoken($emailid,$tokenid){
		$extraparameter=array();
		$extraparameter['emailid']=$emailid;
		$extraparameter['tokenid']=$tokenid;
		$extraparameter['expire_time >']=time();
		$tokenresult=$this->GenricFunction->getRecordwithmulticondition('token',$extraparameter);
		//$this->Apifunction->response($tokenresult, 200);
		if(!empty($tokenresult['id'])){
			$responseresult = array(STATUS => SUCCESS, "result" => TRUE, MESSAGE => 'Valid Token');
			$this->Apifunction->response($this->Apifunction->json($responseresult), 200);
	
		} else {
			$responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Invalid Token');
			$this->Apifunction->response($this->Apifunction->json($responseresult), 404);
		}
	}
	
	
	// login submit function
	public function getusertoken($emailid){
		$extraparameter=array();
		$extraparameter['emailid']=$emailid;
		
		$tokenresult=$this->GenricFunction->getRecordwithmulticondition('token',$extraparameter,TRUE);
		//$this->Apifunction->response($tokenresult, 200);
		if(!empty($tokenresult['id'])){
			$responseresult = array(STATUS => SUCCESS, "result" => TRUE, 'token' => $tokenresult['tokenid']);
			$this->Apifunction->response($this->Apifunction->json($responseresult), 200);
	
		} else {
			$responseresult = array(STATUS => FAIL, "result" => false, 'token' => '0');
			$this->Apifunction->response($this->Apifunction->json($responseresult), 404);
		}
	}
}