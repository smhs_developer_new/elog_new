<?php

class Ptsadminmodel extends CI_Model {

	/*workflow Delete*/
	public function deleterecords($act,$flowtype)
	{
	$this->db->query("delete  from pts_trn_workflowsteps where activity_id='".$act."' and workflowtype='".$flowtype."'");
	return $this->db->affected_rows() > 0;
	}

	public function getworkflow($act,$flowtype)
	{
		$this->db->where('activity_id',$act);
		$this->db->where('workflowtype',$flowtype);
		$this->db->where('is_active',1);
		$res = $this->db->get('pts_trn_workflowsteps');
		if($res->num_rows() > 0)
		{	
			return $res->result();
		}
	}

	public function getActivitiesforworkflow()
	{
		//$this->db->query('Select distinct act.activity_code, act.activity_name');
		$this->db->distinct();
		$this->db->select("act.id, act.activity_name, act.type,act.is_need_approval");
		$this->db->from("pts_mst_activity as act");
		$this->db->where('act.status','active');
		$q= $this->db->get();
		if($q->num_rows() > 0)
		{	
			return $q;
		}		
	}

	public function userauth($userid, $pwd)
	{
		$this->db->where('emp_email',$userid);
		$this->db->where('emp_password',$pwd);
		$this->db->where('is_active',1);
		$res = $this->db->get('mst_employee');
		if($res->num_rows() > 0)
		{	
			return $res->row_array();
//                        echo $this->db->last_query();exit;
		}
		else
		{
			return "";
		}			
	}
        
        public function userauthforAD($userid)
	{
		$this->db->where('emp_email',$userid);
//		$this->db->where('emp_password',$pwd);
		$this->db->where('is_active',1);
		$res = $this->db->get('mst_employee');
		if($res->num_rows() > 0)
		{	
			return $res->row_array();
//                        echo $this->db->last_query();exit;
		}
		else
		{
			return "";
		}			
	}

	public function userauthBypwd($pwd)
	{
		$this->db->where('emp_password',$pwd);
		$this->db->where('is_active',1);
		$res = $this->db->get('mst_employee');
		if($res->num_rows() > 0)
		{	
			return $res->row_array();			
		}
		else
		{
			return "";
		}			
	}

	public function get_dtl3($colname,$colval,$tblname)
	{
		$this->db->where($colname, $colval);		
		$this->db->where('isactive',1);
		$res = $this->db->get($tblname);
		return $res;				
	}

	public function get_doc3($colname,$colval,$tblname)
	{
		$this->db->where($colname, $colval);		
		$this->db->where('isactive',1);
		$res = $this->db->get($tblname);
		return $res;				
	}

	public function get_dtl5($colname,$colval,$colname2,$colval2,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);		
		$this->db->where('isactive',1);
		$res = $this->db->get($tblname);
		if($res->num_rows() > 0)
		{	
			return $res;			
		}
		else
		{
			return "";
		}	
	}

	public function get_dtl7($colname,$colval,$colname2,$colval2,$colname3,$colval3,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where($colname3, $colval3);		
		$this->db->where('isactive',1);
		$res = $this->db->get($tblname);	
			return $res;			
	}

	public function get_dtl9($colname,$colval,$colname2,$colval2,$colname3,$colval3,$colname4,$colval4,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where($colname3, $colval3);
		$this->db->where($colname4, $colval4);		
		$this->db->where('isactive',1);
		$res = $this->db->get($tblname);	
			return $res;			
	}

	public function get_dtl11($colname,$colval,$colname2,$colval2,$colname3,$colval3,$colname4,$colval4,$colname5,$colval5,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);
		$this->db->where($colname3, $colval3);
		$this->db->where($colname4, $colval4);
		$this->db->where($colname5, $colval5);		
		$this->db->where('isactive',1);
		$res = $this->db->get($tblname);	
			return $res;			
	}

	public function get_lastrecord($colname,$colval,$colname2,$colval2,$tblname)
	{
		$this->db->where($colname, $colval);
		$this->db->where($colname2, $colval2);		
		$this->db->where('status','active');
		//$this->db->limit(1);
		//$this->db->order_by("id","DESC");
		$res = $this->db->get($tblname);
		if($res->num_rows() > 0)
		{	
			return $res;			
		}
		else
		{
			return "";
		}	
	}

	public function getnext_step($status_id,$role_id,$activity_id,$Actstatus)
	{		
		$this->db->where("status_id",$status_id);
		$this->db->where("role_id",$role_id);
		$this->db->where("activity_id",$activity_id);
		$this->db->where("workflowtype",$Actstatus);
		$this->db->where('is_active',1);		
		$res = $this->db->get('pts_trn_workflowsteps');
		return $res;		
	}

	public function getnext_step2($status_id,$activity_id,$Actstatus)
	{		
		$this->db->where("status_id",$status_id);
		$this->db->where("activity_id",$activity_id);
		$this->db->where("workflowtype",$Actstatus);
		$this->db->where('is_active',1);		
		$res = $this->db->get('pts_trn_workflowsteps');
		return $res;		
	}

	public function getnext_step_on_start_stop($activity_id,$Actstatus)
	{		
		$this->db->where("activity_id",$activity_id);
		$this->db->where("workflowtype",$Actstatus);
		$this->db->where('is_active',1);
		$this->db->limit(1);
		$this->db->order_by("id","ASC");		
		$res = $this->db->get('pts_trn_workflowsteps');
		return $res;		
	}

	public function getworkflow_step($role_id,$activity_id,$Actstatus)
	{		
		$this->db->where("role_id",$role_id);
		$this->db->where("activity_id",$activity_id);
		$this->db->where("workflowtype",$Actstatus);
		$this->db->where('is_active',1);		
		$res = $this->db->get('pts_trn_workflowsteps');
		return $res;		
	}

	public function getRoleforworkflow()
	{
		$q = $this->db->get('mst_role');
		if($q->num_rows() > 0)
		{	
			return $q;
		}		
	}

	public function getRole()
	{
		$this->db->where("is_active",1);
		$q = $this->db->get('mst_role');
		if($q->num_rows() > 0)
		{	
			return $q;
		}		
	}

	public function getstartworkflow($module="")
	{
            
		$this->db->distinct();
		$this->db->select("wf.workflowtype,wf.activity_id,DATE_FORMAT(wf.created_on,'%d-%m-%Y %H:%i:%s') as created_on,wf.created_by,act.activity_name,wf.is_active");
		$this->db->from("pts_trn_workflowsteps as wf");
//		$this->db->join("mst_employee as emp","wf.created_by=emp.emp_name","INNER");
		$this->db->join("pts_mst_activity as act","act.id=wf.activity_id","INNER");
                $this->db->order_by('created_on','DESC');
                $this->db->group_by(array("wf.activity_id", "wf.workflowtype")); 
//                $this->db->group_by("wf.activity_id");
		//$this->db->where("wf.workflowtype",'start');
                if($module!='master'){
                    $this->db->where("wf.is_active",1);
                }
		$query = $this->db->get();
//                echo $this->db->last_query();exit;
		return $query;
	}

	public function getstopworkflow()
	{
		$this->db->distinct();
		$this->db->select("wf.workflowtype,wf.activity_id,DATE_FORMAT(wf.created_on,'%d-%m-%Y %H:%i:%s') as created_on,wf.created_by,act.activity_name,wf.is_active");
		$this->db->from("pts_trn_workflowsteps as wf");
//		$this->db->join("mst_employee as emp","wf.created_by=emp.emp_code","INNER");
		$this->db->join("pts_mst_activity as act","act.id=wf.activity_id","INNER");
		//$this->db->where("wf.workflowtype",'stop');
		$this->db->where("wf.is_active",1);
		$query = $this->db->get();
		return $query;
	}

	public function getworkflowbyactid($actid,$type,$module=""){
		$this->db->select("wf.id,wf.activity_id,wf.role_id,DATE_FORMAT(wf.created_on,'%d-%m-%Y') as created_on,wf.created_by,role.role_level,role.role_description");
		$this->db->from("pts_trn_workflowsteps as wf");
		$this->db->join("mst_role as role","wf.role_id=role.id","INNER");
		$this->db->where("wf.activity_id",$actid);
		$this->db->where("wf.workflowtype",$type);
                if($module!='master'){
		$this->db->where("wf.is_active",1);
                }
		//$this->db->where("wf.status_id>",1);
		$query = $this->db->get();
		return $query;
	}
        public function checkcode($tblname,$col,$val){
		$this->db->where($col,$val);
		//$this->db->where("status","active");
		$query = $this->db->get($tblname);
		if($query->num_rows()>0){
		$response = array("status"=>1);
		}
		else{
		$response = array("status"=>0);
		}
		return $response;
		}
        /*
         * check user exist 
         */
        public function userexist($userid)
	{
		$this->db->where('emp_email',$userid);
		$this->db->where('is_active',1);
		$res = $this->db->get('mst_employee');
		if($res->num_rows() > 0)
		{	
			return $res->row_array();			
		}
		else
		{
			return "";
		}			
	}
        
        
        /**
         * @desc update user data
         * @param type $tblname
         * @param type $update
         * @param type $wherecol
         * @param type $whereval
         * @return type
         */
        public function updateuser($tblname,$update,$wherecol,$whereval)
	{
		
		$this->db->where($wherecol,$whereval);
		$this->db->update($tblname,$update);
		$AR = $this->db->affected_rows();
		return $AR;	
	}

        public function getworkflowOld($act,$flowtype){
            $this->db->select("GROUP_CONCAT(`role_name`)AS `old_role_name`,`is_active`");
            $this->db->from("pts_trn_workflowsteps");
            $this->db->where('activity_id',$act);
            $this->db->where('workflowtype',$flowtype);
            $this->db->group_by('pts_trn_workflowsteps.activity_id');
            $this->db->group_by('pts_trn_workflowsteps.workflowtype');
            $res = $this->db->get()->row_array();//;
            if(!empty($res)){
                return ['workflow'=>$res['old_role_name'],'status'=>$res['is_active']==1?'Active':'Inactive'];
            }
            return ['workflow'=>'','status'=>''];
	}
}