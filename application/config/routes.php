<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |    example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |    https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |    $route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |    $route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |    $route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:    my-controller/index    -> my_controller/index
  |        my-controller/my-method    -> my_controller/my_method
 */
$route['default_controller'] = 'Login';
$route['admin'] = 'Login';
$route['login'] = 'Login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// Ponta Sahib Rest API Routes
$route['PTS_GET_ROOM'] = 'Rest/Pontasahibelog/Pontasahib/Roomlist';
$route['PTS_ASSIGN_IP'] = 'Rest/Pontasahibelog/Pontasahib/Assignip'; // to assign ip for rooms
$route['PTS_GET_ASSIGN_ROOM_LIST'] = 'Rest/Pontasahibelog/Pontasahib/Getassignedroomlist'; // Get a list of rooms
$route['PTS_ADD_ROOM_PROCS_LOG_ACTIVITY'] = 'Rest/Pontasahibelog/Pontasahib/Addroomactivity'; // Add room activity
$route['PTS_GET_PROCS_LOG_ACTIVITY_LOG'] = 'Rest/Pontasahibelog/Pontasahib/Getactivitylistforroom'; // Get a list of Activity list for provided room
$route['PTS_GET_FIXED_EQUIPMENT'] = 'Rest/Pontasahibelog/Pontasahib/Getfixedequiplist'; // Get a list of Activity list for provided room
$route['PTS_GET_PORTABLE_EQUIPMENT'] = 'Rest/Pontasahibelog/Pontasahib/Getportableequiplist'; // Get a list of Activity list for provided room
$route['PTS_GET_MASTER_ACTIVITY_LIST'] = 'Rest/Pontasahibelog/Pontasahib/Getmasteractivitylist'; // Get a list of Master Activity list

$route['PTS_GET_INPROGRESS_ACTIVITY_LIST'] = 'Rest/Pontasahibelog/Pontasahib/GetInprogressActivityList'; // Get In Progress Activity list
$route['PTS_USER_LOGIN'] = 'Rest/Pontasahibelog/Pontasahib/Login'; //User Authentication

$route['PTS_GET_QAAPPROVAL_ACTIVITY_LIST'] = 'Rest/Pontasahibelog/Pontasahib/GetQaApprovalActivityList'; // Get QA Approval Activity list
$route['PTS_GET_PRODUCT_LIST'] = 'Rest/Pontasahibelog/Pontasahib/GetProductList'; // Get Product list
$route['PTS_GET_FREQUENCY_LIST'] = 'Rest/Pontasahibelog/Pontasahib/GetFrequencyList'; // Get Frequency list
$route['PTS_GET_FILTER_LIST'] = 'Rest/Pontasahibelog/Pontasahib/GetRoomWiseFilterList'; // Get Room wise filter list
$route['PTS_GET_VACCUM_CLEANER_LIST'] = 'Rest/Pontasahibelog/Pontasahib/GetVaccumCleanerlist'; // Get a list of Activity list for provided room
$route['PTS_GET_EQUIPMENT_APPARATUS_LIST'] = 'Rest/Pontasahibelog/Pontasahib/GetEquipmentApparatuslist'; // Get a list of Activity list for provided room
$route['PTS_GET_EQUIPMENT_LIST'] = 'Rest/Pontasahibelog/Pontasahib/GetEquipmentlist'; // Get a list of Activity list for provided room
// Ponta Sahib User View Routes
$route['home'] = 'ponta_sahib/Pontasahibcontroller/home';
$route['inprogress_activity'] = 'ponta_sahib/Pontasahibcontroller/InProgressActivity';
$route['pending_approval'] = 'ponta_sahib/Pontasahibcontroller/PendingApproval';
$route['workflow'] = 'ponta_sahib/Ptsadmin/workflow';
$route['makeworkflow'] = 'ponta_sahib/Ptsadmin/makeworkflow';
$route['editworkflow'] = 'ponta_sahib/Ptsadmin/editstep/';
$route['deleteworkflow'] = 'ponta_sahib/Ptsadmin/deleteworkflow';
$route['processlog_equipment'] = 'ponta_sahib/Pontasahibcontroller/ProcessLogEquipment';
$route['process_log_portable_equipment'] = 'ponta_sahib/Pontasahibcontroller/ProcessLogPortableEquipment';
$route['air_filter_cleaning_record'] = 'ponta_sahib/Pontasahibcontroller/AirFilterCleaningRecord';
$route['material_retreival_relocation_record'] = 'ponta_sahib/Pontasahibcontroller/MaterialRetreivalRelocationRecord';
$route['environmental_condition_record'] = 'ponta_sahib/Pontasahibcontroller/EnvironmentalConditionRecord';
$route['vaccum_cleaner_logbook'] = 'ponta_sahib/Pontasahibcontroller/VaccumCleanerLogbook';
$route['laf_pressure_record'] = 'ponta_sahib/Pontasahibcontroller/LAFPressureRecord';
$route['balance_calibration_record'] = 'ponta_sahib/Pontasahibcontroller/BalanceCalibrationRecord';
$route['vertical_sampler_dies_cleaning'] = 'ponta_sahib/Pontasahibcontroller/VerticalSamplerDiesCleaner';
$route['instrument_log_register'] = 'ponta_sahib/Pontasahibcontroller/InstrumentLogRegister';
$route['equipment_apparatus_log_register'] = 'ponta_sahib/Pontasahibcontroller/EquipmentApparatusLogRegister';
$route['swab_sample_record'] = 'ponta_sahib/Pontasahibcontroller/SwabSampleRecord';
$route['pre_filter_cleaning_record'] = 'ponta_sahib/Pontasahibcontroller/PreFilterCleaningRecord';
$route['line_log'] = 'ponta_sahib/Pontasahibcontroller/LineLog';
$route['tablet_tooling_log_card'] = 'ponta_sahib/Pontasahibcontroller/TabletToolingLogCard';

//new design changes
$route['sf_home'] = 'ponta_sahib/Pontasahibcontroller/sf_home';
$route['sf_processlog_equipment'] = 'ponta_sahib/Pontasahibcontroller/sfProcessLogEquipment';
$route['sf_line_log'] = 'ponta_sahib/Pontasahibcontroller/sfLineLog';
$route['sf_process_log_portable_equipment'] = 'ponta_sahib/Pontasahibcontroller/sfProcessLogPortableEquipment';
$route['sf_vaccum_cleaner_logbook'] = 'ponta_sahib/Pontasahibcontroller/sfVaccumCleanerLogbook';
$route['sf_environmental_condition_record'] = 'ponta_sahib/Pontasahibcontroller/sfEnvironmentalConditionRecord';
$route['sf_air_filter_cleaning_record'] = 'ponta_sahib/Pontasahibcontroller/sfAirFilterCleaningRecord';
$route['sf_laf_pressure_record'] = 'ponta_sahib/Pontasahibcontroller/sfLAFPressureRecord';
$route['sf_balance_calibration_record'] = 'ponta_sahib/Pontasahibcontroller/sfBalanceCalibrationRecord';
$route['sf_vertical_sampler_dies_cleaning'] = 'ponta_sahib/Pontasahibcontroller/sfVerticalSamplerDiesCleaner';
$route['sf_instrument_log_register'] = 'ponta_sahib/Pontasahibcontroller/sfInstrumentLogRegister';
$route['sf_equipment_apparatus_log_register'] = 'ponta_sahib/Pontasahibcontroller/sfEquipmentApparatusLogRegister';
$route['sf_equipment_apparatus_leaktest'] = 'ponta_sahib/Pontasahibcontroller/sfEquipmentApparatusLeakTest';
$route['sf_swab_sample_record'] = 'ponta_sahib/Pontasahibcontroller/sfSwabSampleRecord';
$route['sf_pre_filter_cleaning_record'] = 'ponta_sahib/Pontasahibcontroller/sfPreFilterCleaningRecord';
$route['sf_material_retreival_relocation_record'] = 'ponta_sahib/Pontasahibcontroller/sfMaterialRetreivalRelocationRecord';
$route['sf_pending_approval'] = 'ponta_sahib/Pontasahibcontroller/sfPendingApproval';
$route['sf_inprogress_activity'] = 'ponta_sahib/Pontasahibcontroller/sfInProgressActivity';
$route['sf_tablet_tooling_log_card'] = 'ponta_sahib/Pontasahibcontroller/sfTabletToolingLogCard';
$route['punch_set_allocation_record'] = 'ponta_sahib/Pontasahibcontroller/punchSetAlocationRecord';


// Master data View Routes
$route['manage_material'] = 'ponta_sahib/Mastercontroller/ManageMaterial';
$route['manage_room'] = 'ponta_sahib/Mastercontroller/ManageRoom';
$route['filter_approval'] = 'ponta_sahib/Mastercontroller/FilterApproval';
$route['manage_room_activity'] = 'ponta_sahib/Mastercontroller/ManageRoomActivity';
$route['balance_master'] = 'ponta_sahib/Mastercontroller/ManageBalanceMaster';
$route['tablet_tooling_master'] = 'ponta_sahib/Mastercontroller/ManageTabletToolingMaster';
$route['manage_id_standard_weight'] = 'ponta_sahib/Mastercontroller/ManageIdStandardWeight';
$route['work_assign_asper_rolebase'] = 'ponta_sahib/Mastercontroller/WorkAssignAsPerRoleBase';
$route['manage_filter'] = 'ponta_sahib/Mastercontroller/ManageFilterMaster';
$route['line_log_master'] = 'ponta_sahib/Mastercontroller/LineLogMaster';
$route['instrument_master'] = 'ponta_sahib/Mastercontroller/InstrumentMaster';
$route['balance_frequency_master'] = 'ponta_sahib/Mastercontroller/ManageBalanceFrequencyMaster';
$route['stage_master'] = 'ponta_sahib/Mastercontroller/StageMaster';
$route['report_header'] = 'ponta_sahib/Mastercontroller/reportHeader';
$route['manage_block'] = 'ponta_sahib/Mastercontroller/mstBlock';
$route['block_view'] = 'ponta_sahib/Mastercontroller/mstBlockview';
$route['block_view_edit/(:any)'] = 'ponta_sahib/Mastercontroller/mstBlockviewnow/$1';
$route['manage_area'] = 'ponta_sahib/Mastercontroller/mstArea';
$route['area_view'] = 'ponta_sahib/Mastercontroller/mstAreaview';
$route['area_view_edit/(:any)'] = 'ponta_sahib/Mastercontroller/mstAreaviewnow/$1';
$route['manage_drain_point'] = 'ponta_sahib/Mastercontroller/mstDrainpoint';
$route['drain_point_view'] = 'ponta_sahib/Mastercontroller/mstDrainpointview';
$route['drain_point_view_edit/(:any)'] = 'ponta_sahib/Mastercontroller/mstDrainpointviewnow/$1';
$route['batch_drain_point'] = 'ponta_sahib/Mastercontroller/batchDrainpoint';
$route['manage_product'] = 'ponta_sahib/Mastercontroller/AddProduct';
$route['product_view'] = 'ponta_sahib/Mastercontroller/Productlist';
$route['product_edit/(:any)'] = 'ponta_sahib/Mastercontroller/EditProduct/$1';
$route['batch_product'] = 'ponta_sahib/Mastercontroller/batchProduct';
$route['manage_employee'] = 'ponta_sahib/Mastercontroller/AddEmployee';
$route['employee_view'] = 'ponta_sahib/Mastercontroller/Employeelist';
$route['employee_view_edit/(:any)'] = 'ponta_sahib/Mastercontroller/editemployee/$1';
$route['create_room'] = 'ponta_sahib/Mastercontroller/mstRoom';
$route['room_view'] = 'ponta_sahib/Mastercontroller/mstRoomview';
$route['room_view_edit/(:any)'] = 'ponta_sahib/Mastercontroller/mstRoomviewnow/$1';
$route['create_plant'] = 'ponta_sahib/Mastercontroller/mstPlant';
$route['plant_view'] = 'ponta_sahib/Mastercontroller/mstPlantview';
$route['plant_view_edit/(:any)'] = 'ponta_sahib/Mastercontroller/mstPlantviewnow/$1';
$route['create_sub_block'] = 'ponta_sahib/Mastercontroller/mstDepartment';
$route['sub_block_view'] = 'ponta_sahib/Mastercontroller/mstDepartmentview';
$route['sub_block_view_edit/(:any)'] = 'ponta_sahib/Mastercontroller/mstDepartmentviewnow/$1';
$route['create_equipment'] = 'ponta_sahib/Mastercontroller/mstEquipment';
$route['equipment_view'] = 'ponta_sahib/Mastercontroller/mstEquipmentview';
$route['equipment_edit/(:any)'] = 'ponta_sahib/Mastercontroller/mstEquipmentviewnow/$1';
$route['batch_equipment'] = 'ponta_sahib/Mastercontroller/batchEquipment';
$route['user_management'] = 'ponta_sahib/Mastercontroller/userManagement';
$route['manage_server'] = 'ponta_sahib/Mastercontroller/ManageServerIp';
$route['role_management'] = 'ponta_sahib/Mastercontroller/roleManagement';
$route['role_master'] = 'ponta_sahib/Mastercontroller/roleMaster';
$route['tablet_tooling'] = 'ponta_sahib/Mastercontroller/TabletToolingMaster';



$route['PTS_ADD_MATERIAL_RELOCATION'] = 'Rest/Pontasahibelog/Pontasahib/Addmaterialrelocation'; // Get a list of Master Activity list
$route['PTS_GET_MATERIAL_RELOCATION'] = 'Rest/Pontasahibelog/Pontasahib/Getmaterialrelocation';
$route['PTS_ADD_BALANCE_CALIBRATION'] = 'Rest/Pontasahibelog/Pontasahib/AddRoomBalanceCalibration'; // Add Balance Calibration
$route['PTS_GET_BALANCE_CALIBRATION'] = 'Rest/Pontasahibelog/Pontasahib/GetBalanceCalibrationList'; //Get Balance Calibration Master List
$route['PTS_GET_VERTICAL_SAMPLER'] = 'Rest/Pontasahibelog/Pontasahib/GetsampleRodList'; //Get Vertical Sampler List
$route['PTS_ADD_TABLET_TOOLING'] = 'Rest/Pontasahibelog/Pontasahib/AddTabletToolingLog'; // Add Tablet Tooling Log Card
$route['PTS_GET_TABLET_TOOLING'] = 'Rest/Pontasahibelog/Pontasahib/GetTabletToolingLogList'; //Get Tablet Tooling Log Card List
//Report View Routes
$route['process_room_log_report'] = 'ponta_sahib/Mastercontroller/ProcessRoomLogReport';
$route['line_log_Report'] = 'ponta_sahib/Mastercontroller/LineLogReport';
$route['protable_equipment_report'] = 'ponta_sahib/Mastercontroller/ProtableEquipmentReport';
$route['equipment_apparatus_report'] = 'ponta_sahib/Mastercontroller/EquipmentApparatusReport';
$route['equipment_apparatus_leaktest_report'] = 'ponta_sahib/Mastercontroller/EquipmentApparatusLeakTestReport';
$route['swab_sample_report'] = 'ponta_sahib/Mastercontroller/SwabSampleReport';
$route['audit_trail_report'] = 'ponta_sahib/Mastercontroller/auditTrailReport';
$route['pre_filter_report'] = 'ponta_sahib/Mastercontroller/preFilterReport';
// $route['vertical_sampler_report'] = 'ponta_sahib/Mastercontroller/verticalSamplerReport';
$route['leak_test_report'] = 'ponta_sahib/Mastercontroller/leakTestReport';
$route['friabilator_report'] = 'ponta_sahib/Mastercontroller/friabilatorReport';
$route['instrument_log_report'] = 'ponta_sahib/Mastercontroller/instrumentReport';
$route['vertical_sampler_report'] = 'ponta_sahib/Mastercontroller/verticalSamplerReport';
$route['laf_pressure_differential_report'] = 'ponta_sahib/Mastercontroller/lafPressureDiffReport';
$route['vaccume_cleaner_log_report'] = 'ponta_sahib/Mastercontroller/vaccumeCleanerReport';
$route['environmental_pressure_report'] = 'ponta_sahib/Mastercontroller/envPressureDiffReport';
$route['material_retrieval_relocation'] = 'ponta_sahib/Mastercontroller/materialRelocationReport';
$route['air_filter_cleaning_report'] = 'ponta_sahib/Mastercontroller/airFilterCleaningReport';
$route['balance_calibration_report'] = 'ponta_sahib/Mastercontroller/balaneCalibrationReport';
$route['tablet_tooling_report'] = 'ponta_sahib/Mastercontroller/tabletToolingReport';
$route['audit_trail_report_new'] = 'ponta_sahib/Mastercontroller/auditTrailReportNew';
$route['active_inactive_user_list'] = 'ponta_sahib/Mastercontroller/userListReport';
$route['common_log_report'] = 'ponta_sahib/Mastercontroller/commonLogReport';
$route['createxls'] = 'ponta_sahib/Mastercontroller/createXLS';
$route['master_report'] = 'ponta_sahib/Mastercontroller/ManageMaster';
$route['punch_set_allocation_report'] = 'ponta_sahib/Mastercontroller/PunchSetAllocationReport';

$route['PTS_GET_MATERIAL_RELOCATION'] = 'Rest/Pontasahibelog/Pontasahib/Getmaterialrelocation'; // Get a list of Master Activity list
$route['PTS_GET_BAL_STD_CALABRATION'] = 'Rest/Pontasahibelog/Pontasahib/Getstandardcalibration'; // Get a list of Master Activity list


$route['testAPI'] = 'ponta_sahib/Pontasahibcontroller/testAPI';
$route['user_status_report'] = 'ponta_sahib/Mastercontroller/UserStatusReportView';
$route['getUsersPasswordAboutToExpire'] = 'Croncontroller/getUsersPasswordAboutToExpire';
$route['getBlockedUsers'] = 'Croncontroller/getBlockedUsers';

$route['sendPasswordReminderEmail'] = 'Rest/Pontasahibelog/Pontasahib/sendPasswordReminderEmail';
$route['sendHeaderReminderEmail'] = 'Rest/Pontasahibelog/Pontasahib/sendHeaderReminderEmail';

$route['ADLDAPIPTEST'] = 'Networkcontroller/ADLDAPIPTEST';
$route['SMTPTEST'] = 'Networkcontroller/SMTPTEST';
$route['MysqlTEST'] = 'Networkcontroller/MysqlTEST';

$route['getdatabasebackup'] = 'Croncontroller/getDatabaseBackup';
$route['restoredatabase'] = 'Croncontroller/getDatabaseRestore';
$route['updatedbdoc'] = 'Croncontroller/updateDBDocNo';
$route['a'] = 'Croncontroller/a';