<?php

class Injectable extends CI_Controller{


  public function __construct()
    {
      parent::__construct();
      if(!$this->session->userdata('empemail'))
      {
        return redirect("login");
      }
    }

   public function injectable(){
   	// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('injectable');
   }

   public function par64(){
   	// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('par64');
   }

   public function getProductnames(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Injectablemodel');
    $response = $this->Injectablemodel->getProductnames($_POST["str"]);
    echo json_encode($response);
   }

   public function getProductlotno(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Injectablemodel');
    $response = $this->Injectablemodel->getProductlotno($_POST["str"]);
    echo json_encode($response);
   }

   public function getEquipments(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->getEquipments($_POST["str"]);
   	echo json_encode($response);
   }

   public function getEquipmentsval(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->getEquipmentsval($_POST["str"]);
   	echo json_encode($response);
   }

   public function checkAuth(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->checkAuth($_POST);
   	echo json_encode($response);
   }

   public function equipmentcleaningSubmit(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->equipmentcleaningSubmit($_POST);
   	echo json_encode($response);
   }

   public function solutionNames(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->solutionNames($_POST["str"]);
   	echo json_encode($response);
   }

   public function getSolutionval(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->getSolutionval($_POST["str"]);
   	echo json_encode($response);
   }

   public function submitPreparation(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->submitPreparation($_POST);
   	echo json_encode($response);
   }

   public function stopPreparation(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->stopPreparation();
   	echo json_encode($response);
   } 
   
   public function setrilizationSubmit(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->setrilizationSubmit($_POST);
   	echo json_encode($response);
   }

   public function filtrationSubmit(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->filtrationSubmit($_POST);
   	echo json_encode($response);
   }

   
   public function filtrationCheckby(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->filtrationCheckby($_POST);
   	echo json_encode($response);
   }




  /*public function activities()
  {
    $Acttypeid =  $_GET['ATid'];
    $this->load->model('Productionmodel');
    $activity = $this->Productionmodel->get_dtl6('role_id',$this->session->userdata('roleid'),'activity_typeid',$Acttypeid,'block_code',$this->session->userdata('empblock_code'));

    $data = array('room_code' => $room_code,'activity' => $activity);
    if($activity != false)
    {   
      $this->load->view('header');
      $this->load->view('process/activity', ['data'=>$data]);
      $this->load->view('footer');
    }
    else
    {
      //$this->load->view('production/room', ['room'=>"aasa"]);
    }
  }*/

  public function activities()
  {
    $area_code = "A_195";
    $room_code =  "0";
    $this->session->set_userdata('room_code', $room_code);
    $this->load->model('Activitymodel');
    $activityByRole = $this->Activitymodel->getActivityByRoles($this->session->userdata('roleid'));
    $this->load->model('Productionmodel');
    $activity = $this->Productionmodel->get_dtl4('area_code',$area_code,'activity_type','Aseptic','mst_activity',$activityByRole);

    $data = array('room_code' => $room_code,'activity' => $activity);
    if($activity != false)
    {   
      // header menu code
      $result = $this->Usermodel->getActivitytype();
      $this->load->view('header', ['result'=>$result]);
      // End header menu code  
      $this->load->view('injectable/activity', ['data'=>$data]);
      $this->load->view('footer');
    }
    else
    {
      //$this->load->view('production/room', ['room'=>"aasa"]);
    }
  }
   public function submitCleaning(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->submitCleaning($_POST);
   	echo json_encode($response);
   }

   public function stopCleaning(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->stopCleaning();
   	echo json_encode($response);
   } 

   public function cleaningCheckby(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->cleaningCheckby($_POST);
   	echo json_encode($response);
   } 

   public function submitOperation(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->submitOperation($_POST);
   	echo json_encode($response);
   }

   public function stopOperation(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->stopOperation();
   	echo json_encode($response);
   } 

   public function confirmOutput(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->confirmOutput($_POST);
   	echo json_encode($response);
   } 

   public function operationCheckby(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->operationCheckby($_POST);
   	echo json_encode($response);
   } 

   public function breakStart(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->breakStart();
   	echo json_encode($response);
   } 

   public function breakdownCheckby(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Injectablemodel');
   	$response = $this->Injectablemodel->breakdownCheckby($_POST);
   	echo json_encode($response);
   } 


}    