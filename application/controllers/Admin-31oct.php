<?php

class Admin extends CI_Controller{

	public function test(){

		$result = $this->Usermodel->getActivitytype();
		$this->load->view('header', ['result'=>$result]);
		// End header menu code
		$this->load->view('Admin/test');
			
	}


	public function activityareaMap(){
		header('Access-Control-Allow-Origin: *');  
		header("Content-Type: application/json", true);
		$this->load->model('Adminmodel');
		$response = $this->Adminmodel->insertActivityarea($_POST);
		echo json_encode($response);
	}

	public function activityMapping(){

		$this->load->model('Adminmodel');
		$activity = $this->Adminmodel->getActivities();
		$data["activity"] = $activity;
		$data["area"] = $this->Adminmodel->getAreaformap();
		//$data["selected"] = $this->Activitymodel->selectedActivity();
		// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
		$this->load->view('Admin/Activity_Area_Mapping',$data);
			
	}

	public function Activity($acttypeid)
	{
		$this->load->model('Adminmodel');
		$activity = $this->Adminmodel->get_activityBytypeid('activitytype_id',$acttypeid,'mst_activity');

		$data = array('activity' => $activity,'acttypeid'=>$acttypeid);
		if($activity != false)
		{		
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Activity', ['data'=>$data]);
			$this->load->view('footer');
		}
		else
		{
			//$this->load->view('production/room', ['room'=>"aasa"]);
		}
	}

	public function Updaterole()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
    	$timezone = "Asia/Kolkata";
 		date_default_timezone_set($timezone);
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$roletype = $_POST['roletype'];
			$remark = $_POST['remark'];
			$id = $_POST['id'];

			$res = $this->Adminmodel->update11_4para_with_1where_and_tbl('mst_role','id',$id,'role_description',$roletype,'role_remark',$remark,'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
			if($res>0)
			{
				$response = array("status"=>1);
				//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     		} 
     		else 
     		{
        		$response = array("status"=>0);
  			}
  			echo json_encode($response);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function editrole($id)
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$roledtl = $this->Adminmodel->get_dtl2("id",$id,"mst_role");
			$data = array(
			'roledtl' => $roledtl,
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Edit_Role', ['data'=>$data]);
			$this->load->view('footer');
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
	}

	public function Rolelist()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code

			$role = $this->Adminmodel->get_dtl1("mst_role");

			$data = array(
			'role' => $role,	
			);

			$this->load->view('Admin/Role_List', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function Addrole()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Add_Role');
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function Addnewrole()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
    	$timezone = "Asia/Kolkata";
 		date_default_timezone_set($timezone);
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$roletype = $_POST['roletype'];
			$remark = $_POST['remark'];

			$res = $this->Adminmodel->insert7para_with_tblnama('mst_role','role_description',$roletype,'role_remark',$remark,'created_by',$this->session->userdata('empcode'));
			if($res>0)
			{
				$response = array("status"=>1);
				//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     		} 
     		else 
     		{
        		$response = array("status"=>0);
  			}
  			echo json_encode($response);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function ForgotPassword()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
    	$timezone = "Asia/Kolkata";
 		date_default_timezone_set($timezone);
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$res = $this->Adminmodel->update9_3para_with_1where_and_tbl('mst_employee','id',$_POST['FPhdd'],'emp_password',$_POST['newpwd'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
			if($res>0)
			{
				$response = array("status"=>1);
				//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     		} 
     		else 
     		{
        		$response = array("status"=>0);
  			}
  			echo json_encode($response);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function UpdateEmployee()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
    	$subblock="";
    	$area="";
    	foreach ($_POST['subblock'] as $sub) {
    			$subblock = $subblock . $sub . ",";
    	}
    	$subblock = substr($subblock, 0, -1);
    	foreach ($_POST['area'] as $ar) {
    			$area = $area . $ar . ",";
    	}
    	$area = substr($area, 0, -1);

    	$timezone = "Asia/Kolkata";
 		date_default_timezone_set($timezone);
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$res = $this->Adminmodel->update31_14para_with_1where_and_tbl('mst_employee','id',$_POST['hdd'],'emp_code',$_POST['code'],'emp_name',$_POST['empname'],'email2',$_POST['email2'],'emp_address',$_POST['add'],'emp_contact',$_POST['cont'],'emp_email',$_POST['email'],'designation_code',$_POST['desg'],'Sub_block_code',$subblock,'area_code',$area,'block_code',$_POST['block'],'role_id',$_POST['role'],'emp_remark',$_POST['remark'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
			if($res>0)
			{
				$response = array("status"=>1);
				//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     		} 
     		else 
     		{
        		$response = array("status"=>0);
  			}
  			echo json_encode($response);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function editemployee($id)
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$role = $this->Adminmodel->get_dtl1("mst_role");
			$block = $this->Adminmodel->getblock("mst_block");
			$subblock = $this->Adminmodel->get_dtl1("mst_department");
			$area = $this->Adminmodel->get_dtl1("mst_area");

			$empdtl = $this->Adminmodel->get_dtl2("id",$id,"mst_employee");
			/*$empdtl1 = $empdtl->result();
			$subblock = $empdtl1['Sub_block_code'];
			$area = $empdtl1['area_code'];

			$query1 = "";
			$myArray = explode(',', $subblock); 
            foreach($myArray as $sub) {
            		$query1 = $query1 . "department_code = ".$sub." OR ";
            }
            $query1 = substr($query1, 0, -3);
            $subblockdtl = $this->Adminmodel->getdtl2($query1,"mst_department");
            $query2 = "";
            $myArray1 = explode(',', $area); 
            foreach($myArray1 as $ar) {
            		$query2 = $query2 . "area_code = ".$area." OR ";
            }
            $query2 = substr($query2, 0, -3);
            $areadtl = $this->Adminmodel->getdtl2($query1,"mst_department");*/


			$data = array(
			'empdtl' => $empdtl,
			'role' => $role,
			'block' => $block,
			'subblock' => $subblock,
			'area' => $area	
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Edit_Employee', ['data'=>$data]);
			//$this->load->view('footer');
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
	}

	public function Employeelist()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$empdtl = $this->Adminmodel->get_dtl1("mst_employee");

			$data = array(
			'empdtl' => $empdtl,
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Employee_List', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function AddEmployee()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{

			$role = $this->Adminmodel->get_dtl1("mst_role");
			$block = $this->Adminmodel->getblock("mst_block");

			$data = array(
			'role' => $role,
			'block' => $block		
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Add_Employee', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function AddNewEmployee(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);

    $subblock="";
    $area="";
    foreach ($_POST['subblock'] as $sub) {
    	$subblock = $subblock . $sub . ",";
    }
    $subblock = substr($subblock, 0, -1);
    foreach ($_POST['area'] as $ar) {
    	$area = $area . $ar . ",";
    }
    $area = substr($area, 0, -1);

   	$res = $this->Adminmodel->insert29para_with_tblnama('mst_employee','emp_code',$_POST['code'],'emp_name',$_POST['empname'],'email2',$_POST['email2'],'emp_address',$_POST['add'],'emp_contact',$_POST['cont'],'emp_email',$_POST['email'],'emp_password',$_POST['pwd'],'designation_code',$_POST['desg'],'block_code',$_POST['block'],'Sub_block_code',$subblock,'area_code',$area,'role_id',$_POST['role'],'emp_remark',$_POST['remark'],'created_by',$this->session->userdata('empcode'));
   	if($res>0)
	{
		$response = array("status"=>1);
		//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
    } 
    else 
    {
    	$response = array("status"=>0);
  	}
  	echo json_encode($response);
   }

   	public function Addstatus()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$role = $this->Adminmodel->get_dtl1("mst_role");
			$status = $this->Adminmodel->get_dtl1("mst_status");
			$data = array(
			'role' => $role,
			'status' => $status	
			);
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Add_Status', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

   public function AddNewStatus(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);

	//$statusdtl = $this->Adminmodel->get_dtl2("id",$_POST['statusid'],"mst_status");
	//$statusdtl = $statusdtl->result();

	if($_POST['statusid']!="" && $_POST['statusid']!=null && $_POST['statusid']!="0" && $_POST['statusid']!=0)
	{
		$AR= $this->Adminmodel->deleterecords('role_id',$_POST['role'],$this->session->userdata('empcode'),date("Y-m-d H:i:s"),'mst_roleid_workflow_mapping');
    	if($AR>0)
    	{
   			$res = $this->Adminmodel->insert7para_with_tblnama('mst_roleid_workflow_mapping','role_id',$_POST['role'],'status_id',$_POST['statusid'],'created_by',$this->session->userdata('empcode'));
		}
  		else
  		{
  			$res = $this->Adminmodel->insert7para_with_tblnama('mst_roleid_workflow_mapping','role_id',$_POST['role'],'status_id',$_POST['statusid'],'created_by',$this->session->userdata('empcode'));			
  		}
	}    
	else
	{
		$res2 = $this->Adminmodel->insert5para_with_tblnama('mst_status','status_name',$_POST['status'],'created_by',$this->session->userdata('empcode'));
		$AR= $this->Adminmodel->deleterecords('role_id',$_POST['role'],$this->session->userdata('empcode'),date("Y-m-d H:i:s"),'mst_roleid_workflow_mapping');
    	if($AR>0)
    	{
   			$res = $this->Adminmodel->insert7para_with_tblnama('mst_roleid_workflow_mapping','role_id',$_POST['role'],'status_id',$res2,'created_by',$this->session->userdata('empcode'));
		}
  		else
  		{
  			$res = $this->Adminmodel->insert7para_with_tblnama('mst_roleid_workflow_mapping','role_id',$_POST['role'],'status_id',$res2,'created_by',$this->session->userdata('empcode'));
  		}

	}
    if($res>0)
	{
		$response = array("status"=>1);
		//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
    } 
    else 
    {
		$response = array("status"=>0);
	}
  	echo json_encode($response);
   }

   public function UpdateRolestatusMapping(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $timezone = "Asia/Kolkata";
 	date_default_timezone_set($timezone);
	if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
	{
		$res = $this->Adminmodel->update11_4para_with_1where_and_tbl('mst_roleid_workflow_mapping','id',$_POST['hdd'],'role_id',$_POST['role'],'status_id',$_POST['status'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
		if($res>0)
		{
			$response = array("status"=>1);
			//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
   		} 
   		else 
   		{
       		$response = array("status"=>0);
		}
		echo json_encode($response);
	}
	else
	{
		return redirect(base_url(). 'User/logout');
	}

   }

   public function EditRoleStatusMapping($id)
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$role = $this->Adminmodel->get_dtl1("mst_role");
			$status = $this->Adminmodel->get_dtl1("mst_status");
			$rsmdtl = $this->Adminmodel->get_dtl2("id",$id,"mst_roleid_workflow_mapping");
			$data = array(
			'role' => $role,
			'status' => $status,
			'rsmdtl' => $rsmdtl	
			);
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Edit_RoleStatus', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

   public function RoleStatusMappinglist()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$rsmdtl = $this->Adminmodel->getRoleStatusMapping();

			$data = array(
			'rsmdtl' => $rsmdtl,
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/RSM_List', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

   public function AddRolestatusMapping()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$role = $this->Adminmodel->get_dtl1("mst_role");
			$status = $this->Adminmodel->get_dtl1("mst_status");
			$data = array(
			'role' => $role,
			'status' => $status	
			);
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/AddRoleStatus', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

   public function AddNewRolestatusMapping(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $AR= $this->Adminmodel->deleterecords('role_id',$_POST['role'],$this->session->userdata('empcode'),date("Y-m-d H:i:s"),'mst_roleid_workflow_mapping');
    if($AR>0)
    {
   		$res = $this->Adminmodel->insert7para_with_tblnama('mst_roleid_workflow_mapping','role_id',$_POST['role'],'status_id',$_POST['status'],'created_by',$this->session->userdata('empcode'));
  	}
  	else
  	{
  		$res = $this->Adminmodel->insert7para_with_tblnama('mst_roleid_workflow_mapping','role_id',$_POST['role'],'status_id',$_POST['status'],'created_by',$this->session->userdata('empcode'));
  	}
  	if($res>0)
	{
		$response = array("status"=>1);
		//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
    } 
    else 
    {
	   	$response = array("status"=>0);
	}
  	echo json_encode($response);
   }



   public function UpdateProduct()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
    	$timezone = "Asia/Kolkata";
 		date_default_timezone_set($timezone);
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{

			$res = $this->Adminmodel->update15_6para_with_1where_and_tbl('mst_product','id',$_POST['hdd'],'product_code',$_POST['pcode'],'product_name',$_POST['pname'],'product_market',$_POST['market'],'product_remark',$_POST['remark'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
			if($res>0)
			{
				$response = array("status"=>1);
				//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     		} 
     		else 
     		{
        		$response = array("status"=>0);
  			}
  			echo json_encode($response);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function EditProduct($id)
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$prodtl = $this->Adminmodel->get_dtl2("id",$id,"mst_product");
			$data = array(
			'prodtl' => $prodtl,	
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Edit_Product', ['data'=>$data]);
			$this->load->view('footer');
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
	}

	public function Productlist()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$prodtl = $this->Adminmodel->get_dtl1("mst_product");

			$data = array(
			'prodtl' => $prodtl,
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Product_List', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

   public function AddProduct()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Add_Product');
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function AddNewProduct()
	{
	   	header('Access-Control-Allow-Origin: *');  
	    header("Content-Type: application/json", true);
	   	$res = $this->Adminmodel->insert11para_with_tblnama('mst_product','product_code',$_POST['pcode'],'product_name',$_POST['pname'],'product_market',$_POST['market'],'product_remark',$_POST['remark'],'created_by',$this->session->userdata('empcode'));
   		if($res>0)
		{
			$response = array("status"=>1);
			//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
    	} 
    	else 
    	{
	    	$response = array("status"=>0);
	  	}
	  	echo json_encode($response);		
	}

	public function UpdateMsg()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
    	$timezone = "Asia/Kolkata";
 		date_default_timezone_set($timezone);
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$res = $this->Adminmodel->update11_4para_with_1where_and_tbl('mst_messages','id',$_POST['hdd'],'msg-header',$_POST['msghead'],'message',$_POST['msg'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
			if($res>0)
			{
				$response = array("status"=>1);
				//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     		} 
     		else 
     		{
        		$response = array("status"=>0);
  			}
  			echo json_encode($response);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function EditMsg($id)
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$Msgdtl = $this->Adminmodel->get_dtl2("id",$id,"mst_messages");
			$data = array(
			'Msgdtl' => $Msgdtl,	
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Edit_Msg', ['data'=>$data]);
			$this->load->view('footer');
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
	}

	public function Msglist()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$Msgdtl = $this->Adminmodel->get_dtl1("mst_messages");

			$data = array(
			'Msgdtl' => $Msgdtl,
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Msg_List', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function AddMsg()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Add_Msg');
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function AddNewMsg()
	{
	   	header('Access-Control-Allow-Origin: *');  
	    header("Content-Type: application/json", true);
	   	$res = $this->Adminmodel->insert7para_with_tblnama('mst_messages','msg-header',$_POST['msghead'],'message',$_POST['msg'],'created_by',$this->session->userdata('empcode'));
   		if($res>0)
		{
			$response = array("status"=>1);
			//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
    	} 
    	else 
    	{
	    	$response = array("status"=>0);
	  	}
	  	echo json_encode($response);		
	}

	public function UpdateSolution()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
    	$timezone = "Asia/Kolkata";
 		date_default_timezone_set($timezone);
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{

			$res = $this->Adminmodel->update25_11para_with_1where_and_tbl('mst_solution','id',$_POST['hdd'],'solution_code',$_POST['solcode'],'solution_name',$_POST['solname'],'Block_Code',$_POST['block'],'Precautions',$_POST['Precaution'],'Short_Name',$_POST['shortname'],'sol_qty',$_POST['solquantity'],'water_qty',$_POST['waterquantity'],'tot_qty',$_POST['totquantity'],'UoM',$_POST['uom'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
			if($res>0)
			{
				$response = array("status"=>1);
				//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     		} 
     		else 
     		{
        		$response = array("status"=>0);
  			}
  			echo json_encode($response);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function EditSolution($id)
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$block = $this->Adminmodel->getblock("mst_block");
			$Soldtl = $this->Adminmodel->get_dtl2("id",$id,"mst_solution");
			$data = array(
			'Soldtl' => $Soldtl,
			'block' => $block,	
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Edit_Solution', ['data'=>$data]);
			$this->load->view('footer');
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
	}

	public function Solutionlist()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$Soldtl = $this->Adminmodel->get_dtl1("mst_solution");

			$data = array(
			'Soldtl' => $Soldtl,
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Solution_List', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function AddSolution()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$block = $this->Adminmodel->getblock("mst_block");

			$data = array(
			'block' => $block,		
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Add_Solution', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function AddNewSolution(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$res = $this->Adminmodel->insert21para_with_tblnama('mst_solution','solution_code',$_POST['solcode'],'solution_name',$_POST['solname'],'Block_Code',$_POST['block'],'Precautions',$_POST['Precaution'],'Short_Name',$_POST['shortname'],'sol_qty',$_POST['solquantity'],'water_qty',$_POST['waterquantity'],'tot_qty',$_POST['totquantity'],'UoM',$_POST['uom'],'created_by',$this->session->userdata('empcode'));
   	if($res>0)
	{
		$response = array("status"=>1);
		//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
    } 
    else 
    {
    	$response = array("status"=>0);
  	}
  	echo json_encode($response);
   }

   public function UpdateSop()
	{
	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $config['upload_path']="./uploads";
    $config['allowed_types']='pdf|PDF';
    $config['encrypt_name'] = TRUE;

    $this->load->library('upload',$config);
	if($this->upload->do_upload("sopfile")){
	    $data = $this->upload->data();
        $title= $this->input->post('title');
        $image= $data['file_name']; 
	        
        $res = $this->Adminmodel->update19_8para_with_1where_and_tbl('mst_sop','id',$_POST['hdd'],'sop_code',$_POST['sopcode'],'sop_name',$_POST['sopname'],'sop_type',$_POST['soptype'],'sop_frequency',$_POST['sopfrequency'],'sop_remark',$_POST['soprmk'],'filepath',$data['file_name'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
    }
	else
	{
		$res = $this->Adminmodel->update17_7para_with_1where_and_tbl('mst_sop','id',$_POST['hdd'],'sop_code',$_POST['sopcode'],'sop_name',$_POST['sopname'],'sop_type',$_POST['soptype'],'sop_frequency',$_POST['sopfrequency'],'sop_remark',$_POST['soprmk'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
	}
			if($res>0)
			{
				$response = array("status"=>1);
				//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
     		} 
     		else 
     		{
        		$response = array("status"=>0);
  			}
  			echo json_encode($response);
	}

	public function EditSop($id)
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$area = $this->Adminmodel->get_dtl1('mst_area');
			$sopdtl = $this->Adminmodel->get_dtl2("id",$id,"mst_sop");
			$data = array(
			'sopdtl' => $sopdtl,
			'area' => $area	
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Edit_Sop', ['data'=>$data]);
			//$this->load->view('footer');
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
	}

	public function Soplist()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$sopdtl = $this->Adminmodel->get_dtl1("mst_sop");

			$data = array(
			'sopdtl' => $sopdtl,
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Sop_List', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

   public function AddSop()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$area = $this->Adminmodel->get_dtl1('mst_area');

			$data = array(
			'area' => $area,		
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Add_Sop', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function AddNewSop(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $config['upload_path']="./uploads";
    $config['allowed_types']='pdf|PDF';
    $config['encrypt_name'] = TRUE;

    $this->load->library('upload',$config);
	if($this->upload->do_upload("sopfile")){
	    $data = $this->upload->data();
        $title= $this->input->post('title');
        $image= $data['file_name']; 
	        
        $res = $this->Adminmodel->insert15para_with_tblnama('mst_sop','sop_code',$_POST['sopcode'],'sop_name',$_POST['sopname'],'sop_type',$_POST['soptype'],'sop_frequency',$_POST['sopfrequency'],'sop_remark',$_POST['soprmk'],'filepath',$data['file_name'],'created_by',$this->session->userdata('empcode'));
    }
	else
	{
		$res = $this->Adminmodel->insert13para_with_tblnama('mst_sop','sop_code',$_POST['sopcode'],'sop_name',$_POST['sopname'],'sop_type',$_POST['soptype'],'sop_frequency',$_POST['sopfrequency'],'sop_remark',$_POST['soprmk'],'created_by',$this->session->userdata('empcode'));
	}
	    /*if($res>0){
		$q2 = $this->Productionmodel->get_dtl2('id',46,'mst_messages');
		$msgdtl = $q2->row_array();
	 	$response = array("status"=>1,"msg"=>$msgdtl["message"],"msgheader"=>$msgdtl["msg-header"]);
   		} else {
   		$q2 = $this->Productionmodel->get_dtl2('id',47,'mst_messages');
		$msgdtl = $q2->row_array();
	 	$response = array("status"=>0,"msg"=>$msgdtl["message"],"msgheader"=>$msgdtl["msg-header"]);
	   	} 
		echo json_encode($response);*/
   	if($res>0)
	{
		$response = array("status"=>1);
    } 
    else 
    {
    	$response = array("status"=>0);
  	}
  	echo json_encode($response);
   }

   public function EditCategory($id)
   {
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$catdtl = $this->Adminmodel->get_dtl2("id",$id,"mst_activitytype");
			$data = array(
			'catdtl' => $catdtl,
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Edit_Category', ['data'=>$data]);
			$this->load->view('footer');
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
	}

   public function Categorylist()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$catdtl = $this->Adminmodel->get_dtl1("mst_activitytype");
			$data = array(
			'catdtl' => $catdtl,	
			);
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Category_List', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}		
	}

   public function AddCategory()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Add_Category');
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function add_new_cat(){
        $config['upload_path']="./img";
        $config['allowed_types']='gif|jpg|png';
        $config['encrypt_name'] = TRUE;
        
        $this->load->library('upload',$config);
	    if($this->upload->do_upload("icon")){
	        $data = $this->upload->data();

	        //Resize and Compress Image
            $config['image_library']='gd2';
            $config['source_image']='./img/'.$data['file_name'];
            $config['create_thumb']= FALSE;
            $config['maintain_ratio']= FALSE;
            $config['quality']= '60%';
            $config['width']= 600;
            $config['height']= 400;
            $config['new_image']= './img/'.$data['file_name'];
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

	        $title= $this->input->post('title');
	        $image= $data['file_name']; 
	        
	        $res = $this->Adminmodel->insert13para_with_tblnama('mst_activitytype','activitytype_name',$_POST['name'],'activitytype_icon',$data['file_name'],'activityfunction_tocall',$_POST['functocall'],'activity_remarks',$_POST['rmk'],'activity_blockcolor',$_POST['color'],'created_by',$this->session->userdata('empcode'));
   		   	if($res>0)
			{
				$response = "Successfully Saved.";
				//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
    		} 
    		else 
    		{
		    	$response = "Not Saved";
  			}
        }
		else
		{
				$response = $this->upload->display_errors();				
		}
		echo $response;
     }

    public function update_cat()
    {
    	if(!empty($_FILES['icon']['name']))
    	{
        	$config['upload_path']="./img";
        	$config['allowed_types']='gif|jpg|png';
        	$config['encrypt_name'] = TRUE;
        
        	$this->load->library('upload',$config);
	    	if($this->upload->do_upload("icon")){
		        $data = $this->upload->data();

		        //Resize and Compress Image
            	$config['image_library']='gd2';
            	$config['source_image']='./img/'.$data['file_name'];
            	$config['create_thumb']= FALSE;
            	$config['maintain_ratio']= FALSE;
            	$config['quality']= '60%';
            	$config['width']= 600;
            	$config['height']= 400;
            	$config['new_image']= './img/'.$data['file_name'];
            	$this->load->library('image_lib', $config);
            	$this->image_lib->resize();

		        $title= $this->input->post('title');
	        	$image= $data['file_name']; 
	        
	        	$res = $this->Adminmodel->update17_7para_with_1where_and_tbl('mst_activitytype','id',$_POST['hdd'],'activitytype_name',$_POST['name'],'activitytype_icon',$data['file_name'],'activityfunction_tocall',$_POST['functocall'],'activity_remarks',$_POST['rmk'],'activity_blockcolor',$_POST['color'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
	        }
			else
			{
					$response = $this->upload->display_errors();				
			}
		}
		else
		{
				$res = $this->Adminmodel->update15_6para_with_1where_and_tbl('mst_activitytype','id',$_POST['hdd'],'activitytype_name',$_POST['name'],'activityfunction_tocall',$_POST['functocall'],'activity_remarks',$_POST['rmk'],'activity_blockcolor',$_POST['color'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
		}
	if($res>0)
	{
		$response = "Successfully Updated.";
		//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
    } 
    else 
    {
	   	$response = "Not Updated";
  	}
	echo $response;
    }

   public function update_acat()
    {
    	if(!empty($_FILES['icon']['name']))
    	{
        	$config['upload_path']="./img";
        	$config['allowed_types']='gif|jpg|png';
        	$config['encrypt_name'] = TRUE;
        
        	$this->load->library('upload',$config);
	    	if($this->upload->do_upload("icon")){
		        $data = $this->upload->data();

		        //Resize and Compress Image
            	/*$config['image_library']='gd2';
            	$config['source_image']='./img/'.$data['file_name'];
            	$config['create_thumb']= FALSE;
            	$config['maintain_ratio']= FALSE;
            	$config['quality']= '60%';
            	$config['width']= 600;
            	$config['height']= 400;
            	$config['new_image']= './img/'.$data['file_name'];
            	$this->load->library('image_lib', $config);
            	$this->image_lib->resize();*/

		        $title= $this->input->post('title');
	        	$image= $data['file_name']; 
	        
	        	$res = $this->Adminmodel->update23_10para_with_1where_and_tbl('mst_activity','id',$_POST['hdd'],'activity_code',$_POST['actcode'],'activity_name',$_POST['actname'],'activitytype_id',$_POST['type'],'activity_type',$_POST['typename'],'activityfunc_tocall',$_POST['functocall'],'activity_icon',$data['file_name'],'area_code',$_POST['area'],'activity_remark',$_POST['rmk'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
	        }
			else
			{
					$response = $this->upload->display_errors();				
			}
		}
		else
		{
				$res = $this->Adminmodel->update21_9para_with_1where_and_tbl('mst_activity','id',$_POST['hdd'],'activity_code',$_POST['actcode'],'activity_name',$_POST['actname'],'activitytype_id',$_POST['type'],'activity_type',$_POST['typename'],'activityfunc_tocall',$_POST['functocall'],'area_code',$_POST['area'],'activity_remark',$_POST['rmk'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
		}
	if($res>0)
	{
		$response = "Successfully Updated.";
		//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
    } 
    else 
    {
	   	$response = "Not Updated";
  	}
	echo $response;
    }

	public function EditActivity($id)
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			//$area = $this->Adminmodel->get_dtl1('mst_area');
			$Acttype = $this->Adminmodel->get_dtl1('mst_activitytype');
			$actdtl = $this->Adminmodel->get_dtl2("id",$id,"mst_activity");
			$data = array(
			'Acttype' => $Acttype,
			'actdtl' => $actdtl,
			//'area' => $area	
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Edit_Activity', ['data'=>$data]);
			$this->load->view('footer');
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
	}

   public function ActivityList()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$actdtl = $this->Adminmodel->get_dtl1("mst_activity");
			$data = array(
			'actdtl' => $actdtl,	
			);
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Activity_List', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}		
	}

   public function AddActivity()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			//$area = $this->Adminmodel->get_dtl1('mst_area');
			$Acttype = $this->Adminmodel->get_dtl1('mst_activitytype');
			$data = array(
			//'area' => $area,
			'Acttype' => $Acttype,	
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Add_Activity', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function add_new_acat(){
		header('Access-Control-Allow-Origin: *');  
		header("Content-Type: application/json", true);
        $config['upload_path']="./img";
        $config['allowed_types']='gif|jpg|png';
        $config['encrypt_name'] = TRUE;
        
        $this->load->library('upload',$config);
	    if($this->upload->do_upload("icon")){
	        $data = $this->upload->data();

	        $title= $this->input->post('title');
	        $image= $data['file_name']; 
	        
	        $res = $this->Adminmodel->insert19para_with_tblnama('mst_activity','activity_code',$_POST['actcode'],'activity_name',$_POST['actname'],'activitytype_id',$_POST['type'],'activity_type',$_POST['typename'],'activityfunc_tocall',$_POST['functocall'],'activity_icon',$data['file_name'],'area_code',$_POST['area'],'activity_remark',$_POST['rmk'],'created_by',$this->session->userdata('empcode'));

	        if($res>0){
			$q2 = $this->Productionmodel->get_dtl2('id',46,'mst_messages');
			$msgdtl = $q2->row_array();
		 	$response = array("status"=>1,"msg"=>$msgdtl["message"],"msgheader"=>$msgdtl["msg-header"]);
     		} 
     		else {
     		$q2 = $this->Productionmodel->get_dtl2('id',47,'mst_messages');
			$msgdtl = $q2->row_array();
		 	$response = array("status"=>0,"msg"=>$msgdtl["message"],"msgheader"=>$msgdtl["msg-header"]);
     		} 
        }
		else
		{
				$response = array("status"=>2,"msg"=>$this->upload->display_errors(),"msgheader"=>"Error");
				//$response = $this->upload->display_errors();				
		}
		//echo $response;
		echo json_encode($response);
     }

	public function getdept(){
		header('Access-Control-Allow-Origin: *');  
		header("Content-Type: application/json", true);
		$this->load->model('Productionmodel');
		$block = $_POST["block"];
		$blockdtl = $this->Adminmodel->getdeptbyblock($block);
		$response = $blockdtl->result_array();
		echo json_encode($response);
	}

	public function getarea(){
		header('Access-Control-Allow-Origin: *');  
		header("Content-Type: application/json", true);
		$this->load->model('Productionmodel');
		$subblock = $_POST["subblock"];
		$query="";
		foreach ($subblock as $sb) {
			$query = $query.'department_code = "'.$sb.'" OR ';
		}
		$query = substr($query, 0, -3);
		$areadtl = $this->Adminmodel->getareabysubblock($query,'mst_area');
		$response = $areadtl->result_array();
		echo json_encode($response);
		//echo json_encode($query);
	}

	public function CommonDelete()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
    	$timezone = "Asia/Kolkata";
 		date_default_timezone_set($timezone);
		$emp_code = $this->session->userdata('empcode');
		$roleid = $this->session->userdata('roleid');
		$block_code = $this->session->userdata('empblock_code');
		$id = $_POST['id'];
		$tblname = $_POST['tblname'];
		$colname = $_POST['colname'];
		$AR= $this->Adminmodel->deleterecords($colname,$id,$this->session->userdata('empcode'),date("Y-m-d H:i:s"),$tblname);
		echo json_decode($AR);
	}

	public function Update_Status(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $timezone = "Asia/Kolkata";
 	date_default_timezone_set($timezone);
	if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
	{
		$res = $this->Adminmodel->update9_3para_with_1where_and_tbl('mst_status','id',$_POST['hdd'],'status_name',$_POST['status'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
		if($res>0)
		{
			$response = array("status"=>1);
			//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
   		} 
   		else 
   		{
       		$response = array("status"=>0);
		}
		echo json_encode($response);
	}
	else
	{
		return redirect(base_url(). 'User/logout');
	}

   }

   public function Edit_Status($id)
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$statusdtl = $this->Adminmodel->get_dtl2("id",$id,"mst_status");
			$data = array(
			'statusdtl' => $statusdtl,	
			);
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/Editstatus', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

   public function Status_List()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$statusdtl = $this->Adminmodel->get_dtl1("mst_status");

			$data = array(
			'statusdtl' => $statusdtl,
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/StatusList', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

   public function Add_Status()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$statusdtl = $this->Adminmodel->get_dtl1("mst_status");

			$data = array(
			'statusdtl' => $statusdtl,
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('Admin/AddStatus', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

    public function Add_NewStatus(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
  	$res = $this->Adminmodel->insert5para_with_tblnama('mst_status','status_name',$_POST['status'],'created_by',$this->session->userdata('empcode'));
  	
  	if($res>0)
	{
		$response = array("status"=>1);
		//$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
    } 
    else 
    {
	   	$response = array("status"=>0);
	}
  	echo json_encode($response);
   }

 

	public function __construct()
    {
      parent::__construct();
      if(!$this->session->userdata('empemail'))
      {
        return redirect("login");
      }
    }

	 public function mstPlant(){
   	// header menu code
	$result = $this->Usermodel->getActivitytype();
	$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/mstPlant');
   }

   public function checkPlantcode(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->checkPlantcode($_POST);
    echo json_encode($response);
   }

    public function mstPlantSubmit(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Adminmodel');
   	$response = $this->Adminmodel->mstPlantSubmit($_POST);
   	echo json_encode($response);
   }

   public function mstPlantview(){
    $this->load->model('Adminmodel');
    $data = $this->Adminmodel->mstPlantview();
   // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
    $this->load->view('Admin/mstPlantview', ['data'=>$data]);
   }

   public function mstPlantviewnow($id){
    $this->load->model('Adminmodel');
    $data = $this->Adminmodel->mstPlantviewnow($id);
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
    $this->load->view('Admin/mstPlantviewnow', ['data'=>$data]);
   }

   public function mstPlantUpdate(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->mstPlantUpdate($_POST);
    echo json_encode($response);
   }

   public function deletePlant(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->deletePlant($_POST);
    echo json_encode($response);
   }

   public function mstBlock(){
	   $this->load->model('Productionmodel');
	   $plantcode = $this->Productionmodel->get_dtl2('is_active','1','mst_plant');
	   $data = array('pcode' => $plantcode);
	   // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code 
     $this->load->view('Admin/mstBlock', ['data'=>$data]);
   }

   public function checkBlockcode(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->checkBlockcode($_POST);
    echo json_encode($response);
   }

   public function mstBlockSubmit(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Adminmodel');
   	$response = $this->Adminmodel->mstBlockSubmit($_POST);
   	echo json_encode($response);
   }

   public function mstBlockview(){
    $this->load->model('Adminmodel');
    $data = $this->Adminmodel->mstBlockview();
   // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
    $this->load->view('Admin/mstBlockview', ['data'=>$data]);
   }

   public function mstBlockviewnow($id){
    $this->load->model('Adminmodel');
    $this->load->model('Productionmodel');
    $plantcode = $this->Productionmodel->get_dtl2('is_active','1','mst_plant');
    $blockdata = $this->Adminmodel->mstBlockviewnow($id);
    $data = array('pcode' => $plantcode,'blockdata'=>$blockdata);
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/mstBlockviewnow', ['data'=>$data]);
   }

   public function mstBlockUpdate(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->mstBlockUpdate($_POST);
    echo json_encode($response);
   }

   public function deleteBlock(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->deleteBlock($_POST);
    echo json_encode($response);
   } 


    public function mstArea(){
   	$this->load->model('Productionmodel');
	$areacode = $this->Productionmodel->get_dtl2('is_active','1','mst_department','department_code');
	$data = array('acode' => $areacode);
	// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/mstArea', ['data'=>$data]);
   }

   public function checkAreacode(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->checkAreacode($_POST);
    echo json_encode($response);
   }


   public function mstAreaSubmit(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Adminmodel');
   	$response = $this->Adminmodel->mstAreaSubmit($_POST);
   	echo json_encode($response);
   }

   public function mstAreaview(){
    $this->load->model('Adminmodel');
    $data = $this->Adminmodel->mstAreaview();
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/mstAreaview', ['data'=>$data]);
   }

   public function mstAreaviewnow($id){
    $this->load->model('Adminmodel');
    $this->load->model('Productionmodel');
    $blockcode = $this->Productionmodel->get_dtl2('is_active','1','mst_department');
    $areadata = $this->Adminmodel->mstAreaviewnow($id);
    $data = array('bcode' => $blockcode,'areadata'=>$areadata);
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/mstAreaviewnow', ['data'=>$data]);
   }

   public function mstAreaupdate(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->mstAreaupdate($_POST);
    echo json_encode($response);
   }

   public function deleteArea(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->deleteArea($_POST);
    echo json_encode($response);
   }


   public function mstRoom(){
   	$this->load->model('Productionmodel');
	$areacode = $this->Productionmodel->get_dtl2('is_active','1','mst_area');
	//$data[] = array('acode' => $areacode);
	$draincode = $this->Productionmodel->get_dtl2('is_active','1','mst_drainpoint');
	$data = array('acode' => $areacode,'dcode' => $draincode);
    //$dataavaialable = 1;
	$countarea = $areacode->num_rows();
    $countdp = $draincode->num_rows();
	/*if($countarea >0 && $countdp >0)
	  {
         $dataavaialable = 1;
	   }*/

	// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
        //if($dataavaialable == 1){
        //$data["items"] = $this->Sanitizationmodel->getdrainPoints();
        $this->load->view('Admin/mstRoom', ['data'=>$data]);
		//$this->load->view('Admin/mstRoom', $data);
      //}
     /* else{
		$this->load->model('Sanitizationmodel');
        $msg = $this->Sanitizationmodel->errMsg();
        $data["header"] = $msg["msg_header"];
        $data["message"] = $msg["message"]."<br> <b>mst_area</b> <br> <b>mst_drainpoint</b>";
        //"Issue occured in following Tables . Please contact administrator.<br> mst_drainpoint";
        $this->load->view("error500",$data);
        
      }*/
    
   }

   public function checkRoomcode(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->checkRoomcode($_POST);
    echo json_encode($response);
   }


   public function mstRoomSubmit(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Adminmodel');
   	$response = $this->Adminmodel->mstRoomSubmit($_POST);
   	echo json_encode($response);
   }

   public function mstRoomview(){
    $this->load->model('Adminmodel');
    $data = $this->Adminmodel->mstRoomview();
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/mstRoomview', ['data'=>$data]);
   }

   public function mstRoomviewnow($id){
    $this->load->model('Adminmodel');
    $this->load->model('Productionmodel');
    $areacode = $this->Productionmodel->get_dtl2('is_active','1','mst_area');
	$draincode = $this->Productionmodel->get_dtl2('is_active','1','mst_drainpoint');
    $roomdata = $this->Adminmodel->mstRoomviewnow($id);
    $data = array('acode' => $areacode,'roomdata'=>$roomdata,'dcode'=>$draincode);
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
    $this->load->view('Admin/mstRoomviewnow', ['data'=>$data]);
   }

   public function mstRoomupdate(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->mstRoomupdate($_POST);
    echo json_encode($response);
   }

   public function deleteRoom(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->deleteRoom($_POST);
    echo json_encode($response);
   }

   public function mstDocument(){
   // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/mstDocument');
   }

   public function checkDocumentcode(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->checkDocumentcode($_POST);
    echo json_encode($response);
   }

   public function mstDocumentSubmit(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Adminmodel');
   	$response = $this->Adminmodel->mstDocumentSubmit($_POST);
   	echo json_encode($response);
   }

   public function mstDocumentview(){
    $this->load->model('Adminmodel');
    $data = $this->Adminmodel->mstDocumentview();
   // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/mstDocumentview', ['data'=>$data]);
   }

    public function mstDocumentviewnow($id){
    $this->load->model('Adminmodel');
    $data = $this->Adminmodel->mstDocumentviewnow($id);
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/mstDocumentviewnow', ['data'=>$data]);
   }

   public function mstDocumentupdate(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->mstDocumentupdate($_POST);
    echo json_encode($response);
   }

   public function deleteDocument(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->deleteDocument($_POST);
    echo json_encode($response);
   }

   public function mstDrainpoint(){
	   $this->load->model('Productionmodel');
	   $roomcode = $this->Productionmodel->get_dtl2('is_active','1','mst_room');
	   $solcode = $this->Productionmodel->get_dtl2('is_active','1','mst_solution');
	   $data = array('rcode' => $roomcode,'scode' => $solcode );
	   // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
       $this->load->view('Admin/mstDrainpoint', ['data'=>$data]);
   }

    public function checkDraincode(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->checkDraincode($_POST);
    echo json_encode($response);
   }


   public function mstDrainpointSubmit(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
   	$this->load->model('Adminmodel');
   	$response = $this->Adminmodel->mstDrainpointSubmit($_POST);
   	echo json_encode($response);
   }

   public function mstDrainpointview(){
    $this->load->model('Adminmodel');
    $data = $this->Adminmodel->mstDrainpointview();
   // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
    $this->load->view('Admin/mstDrainpointview', ['data'=>$data]);
   }

   public function mstDrainpointviewnow($id){
     $this->load->model('Productionmodel');
     $roomcode = $this->Productionmodel->get_dtl2('is_active','1','mst_room');
     $solcode = $this->Productionmodel->get_dtl2('is_active','1','mst_solution');
     $this->load->model('Adminmodel');
     $dpdtls = $this->Adminmodel->mstDrainpointviewnow($id);
     $data = array('rcode' => $roomcode,'scode' => $solcode ,'dpdetails' => $dpdtls);
     // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
       $this->load->view('Admin/mstDrainpointviewnow', ['data'=>$data]);
   }

    public function mstDrainpointupdate(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->mstDrainpointupdate($_POST);
    echo json_encode($response);
   }
   
   
   public function deleteDrainpoint(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->deleteDrainpoint($_POST);
    echo json_encode($response);
   }

   public function mstDepartment(){
    $this->load->model('Productionmodel');
    $blockcode = $this->Productionmodel->get_dtl2('is_active','1','mst_block');
    $data = array('bcode' => $blockcode);
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
    $this->load->view('Admin/mstDepartment', ['data'=>$data]);
   }

   
   public function checkDepartmentcode(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->checkDepartmentcode($_POST);
    echo json_encode($response);
   }

   public function checkAreasbyblock(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->checkAreasbyblock($_POST);
    echo json_encode($response);
   }

   public function mstDepartmentSubmit(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->mstDepartmentSubmit($_POST);
    echo json_encode($response);
   }

   public function mstDepartmentview(){
    $this->load->model('Adminmodel');
    $data = $this->Adminmodel->mstDepartmentview();
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/mstDepartmentview', ['data'=>$data]);
   }

   public function mstDepartmentviewnow($id){
    $this->load->model('Adminmodel');
    $this->load->model('Productionmodel');
    $blockcode = $this->Productionmodel->get_dtl2('is_active','1','mst_block');
    $departmentdata = $this->Adminmodel->mstDepartmentviewnow($id);
    $areacode = $this->Adminmodel->mstGetareanow($departmentdata);
    $data = array('acode' => $areacode,'bcode' => $blockcode,'departmentdata'=>$departmentdata);
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/mstDepartmentviewnow', ['data'=>$data]);
   }

   public function mstDepartmentupdate(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->mstDepartmentupdate($_POST);
    echo json_encode($response);
   }

   public function deleteDepartment(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->deleteDepartment($_POST);
    echo json_encode($response);
   }

   public function mstEquipment(){
	   $this->load->model('Productionmodel');
	   $roomcode = $this->Productionmodel->get_dtl2('is_active','1','mst_room');
	   $sopdata = $this->Productionmodel->get_dtl2('is_active','1','mst_sop');
	   $data = array('rcode' => $roomcode,'sop' => $sopdata );
	  // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code 
       $this->load->view('Admin/mstEquipment', ['data'=>$data]);
   }

   public function checkPlantlimit(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->checkPlantlimit($_POST);
    echo json_encode($response);
   }

   public function checkBlocklimit(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->checkBlocklimit($_POST);
    echo json_encode($response);
   }

   public function checkArealimit(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->checkArealimit($_POST);
    echo json_encode($response);
   }

   public function checkEquipmentcode(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->checkEquipmentcode($_POST);
    echo json_encode($response);
   }

   public function getDrainpointlist(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->getDrainpointlist($_POST["str"]);
    echo json_encode($response);
   }

   public function getSoplist(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->getSoplist($_POST["str"]);
    echo json_encode($response);
   }

   public function mstEquipmentSubmit(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $picture = 'default.png';
    if(!empty($_FILES['eicon']['name'])){
	$config['upload_path'] = realpath(APPPATH.'../img/icons/');
    $config['allowed_types'] = 'jpg|jpeg|png|gif';
    $config['file_name'] = $_FILES['eicon']['name'];
    $this->load->library('upload',$config);
    $this->upload->initialize($config);

    if($this->upload->do_upload('eicon')){
        $uploadData = $this->upload->data();
        $picture = $uploadData['file_name'];
    }else{
        $picture = 'default.png';
    }
	}
	$arr = array("equipment_code"=>$_POST["ecode"],"equipment_name"=>$_POST["ename"],"equipment_icon"=>$picture,"equipment_type"=>$_POST["etype"],"equipment_remarks"=>$_POST["eremarks"],"room_code"=>$_POST["rcode"],"drain_point_code"=>$_POST["dpcode"],"sop_code"=>$_POST["spcode"],"created_by"=>$this->session->userdata('empcode'));
	if($this->db->insert("mst_equipment",$arr)){
	$response = array("status"=>1);	
	}
	else{
	$response = array("status"=>0);	
	}
    echo json_encode($response);
   }

   public function mstEquipmentview(){
    $this->load->model('Adminmodel');
    $data = $this->Adminmodel->mstEquipmentview();
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code 
    $this->load->view('Admin/mstEquipmentview', ['data'=>$data]);
   }

   public function mstEquipmentviewnow($id){
    $this->load->model('Adminmodel');
    $this->load->model('Productionmodel');
    $roomcode = $this->Productionmodel->get_dtl2('is_active','1','mst_room');
    $equipmentdata = $this->Adminmodel->mstEquipmentviewnow($id);
    $data = array('rcode'=>$roomcode,'equipmentdata'=>$equipmentdata);
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/mstEquipmentviewnow', ['data'=>$data]);
   }

   public function deleteEquipment(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->deleteEquipment($_POST);
    echo json_encode($response);
   }

   public function mstEquipmentUpdate(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    date_default_timezone_set('Asia/Kolkata');	
	$date = date("Y-m-d H:i:s");
    //$this->load->model('Adminmodel');
    //$response = $this->Adminmodel->mstEquipmentUpdate($_POST);
    if(!empty($_FILES['eicon']['name'])){
	$config['upload_path'] = realpath(APPPATH.'../img/icons/');
    $config['allowed_types'] = 'jpg|jpeg|png|gif';
    $config['file_name'] = $_FILES['eicon']['name'];
    $this->load->library('upload',$config);
    $this->upload->initialize($config);

    if($this->upload->do_upload('eicon')){
		/*if($pic!=""){
		$ddr=explode("/",$pic);
		//echo $ddr[6];
		$somefile="assets/images/".$ddr[6];
		unlink($somefile);
		}*/
        $uploadData = $this->upload->data();
        $picture = $uploadData['file_name'];
        //$picture = base_url()."assets/images/".$picture;
    }else{
        $picture = 'default.png';
    }

    $arr = array("equipment_code"=>$_POST["ecode"],"equipment_name"=>$_POST["ename"],"equipment_icon"=>$picture,"equipment_type"=>$_POST["etype"],"equipment_remarks"=>$_POST["eremarks"],"room_code"=>$_POST["rcode"],"drain_point_code"=>$_POST["dpcode"],"sop_code"=>$_POST["spcode"],"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);

    $this->db->where("id",$_POST["equipmentid"]);
    $this->db->update("mst_equipment",$arr);

    $response = array("status"=>1);
	}
	else{
	$arr = array("equipment_code"=>$_POST["ecode"],"equipment_name"=>$_POST["ename"],"equipment_type"=>$_POST["etype"],"equipment_remarks"=>$_POST["eremarks"],"room_code"=>$_POST["rcode"],"drain_point_code"=>$_POST["dpcode"],"sop_code"=>$_POST["spcode"],"modified_by"=>$this->session->userdata('empcode'),"modified_on"=>$date);
	$this->db->where("id",$_POST["equipmentid"]);
    $this->db->update("mst_equipment",$arr);	

	$response = array("status"=>1);	
	}

    echo json_encode($response);
   }


   	public function chkcode(){
   	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $tblname = $_POST['tblname'];
    $col = $_POST['col'];
    $val = $_POST['code'];
    $this->load->model('Adminmodel');
    $response = $this->Adminmodel->checkcode($tblname,$col,$val);
    echo json_encode($response);
   }


public function batchSop(){
    
	// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/uploadSop');
	}


public function uploadSopExcel(){
	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);

	$this->load->model('Adminmodel');

 
  //if ($this->input->post('submit')) {

            //echo "file name is ". $_FILES['uploadFile']['name'];

     		$result = 0;
			$path = 'uploads/';
            require_once APPPATH . "/third_party/PHPExcel/Classes/PHPExcel.php";
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'xlsx|xls|csv';
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);            
            if (!$this->upload->do_upload('uploadFile')) {
			//if (!$this->upload->do_upload($_FILES['uploadFile']['name'])) {

                $error = array('error' => $this->upload->display_errors());
				$response = array("status"=>0);

            } else {
                $data = array('upload_data' => $this->upload->data());
            }
            if(empty($error)){
              if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
				//$import_xls_file = $_FILES['uploadFile']['name'];
            } else {
                $import_xls_file = 0;
            }
            $inputFileName = $path . $import_xls_file;

			
            
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
                $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $flag = true;
                $i=0;
                foreach ($allDataInSheet as $value) {
                  if($flag){
                    $flag =false;
                    continue;
                  }
                  $this->db->where("is_active",1);
	              $this->db->where("sop_code",$value['A']);
	              //$this->db->where("department_code",$subblock);
	              $query = $this->db->get("mst_sop");
				  if($query->num_rows()>0){
					  $result = $this->Adminmodel->update15_6para_with_1where_and_tbl('mst_sop','sop_code',$value['A'],'sop_name',$value['B'],'sop_type',$value['C'],'sop_frequency',$value['D'],'sop_remark',$value['E'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
					  /*if($res>0)
			           {
				         //echo "Imported successfully";
						 $response = array("status"=>1);				
     		           } */
               	  }
				  else{
                  
				  $inserdata[$i]['sop_code'] = $value['A'];
                  $inserdata[$i]['sop_name'] = $value['B'];
                  $inserdata[$i]['sop_type'] = $value['C'];
                  $inserdata[$i]['sop_frequency'] = $value['D'];
                  $inserdata[$i]['sop_remark'] = $value['E'];
				  //$inserdata[$i]['area_code'] = $value['F'];
				  //$inserdata[$i]['activity_code'] = $value['G'];
				  $inserdata[$i]['created_by'] = $this->session->userdata('empcode');
				  $i++;
				  }
                  

                }

                //print_r($inserdata);
                unlink($inputFileName);               
                
				if(isset($inserdata)){
                $result = $this->Adminmodel->importDataExcel($inserdata,"mst_sop"); 
				}

                if($result){
                  //echo "Imported successfully";
				  $response = array("status"=>1);
                }else{
                  //echo "ERROR !";
				  $response = array("status"=>0);
                } 
                           
 
          } catch (Exception $e) {
               die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' .$e->getMessage());
            }
          }else{
             
			  echo $error['error'];
			 $response = array("status"=>0);
            }
            
            
   //}
   echo json_encode($response);
    
  }


  public function batchProduct(){
    
	// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/uploadProduct');
	}


public function uploadProductExcel(){
	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);

	$this->load->model('Adminmodel');

 
  //if ($this->input->post('submit')) {

            //echo "file name is ". $_FILES['uploadFile']['name'];

     		$result = 0;
			$path = 'uploads/';
            require_once APPPATH . "/third_party/PHPExcel/Classes/PHPExcel.php";
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'xlsx|xls|csv';
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);            
            if (!$this->upload->do_upload('uploadFile')) {
			//if (!$this->upload->do_upload($_FILES['uploadFile']['name'])) {

                $error = array('error' => $this->upload->display_errors());
				$response = array("status"=>0);

            } else {
                $data = array('upload_data' => $this->upload->data());
            }
            if(empty($error)){
              if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
				//$import_xls_file = $_FILES['uploadFile']['name'];
            } else {
                $import_xls_file = 0;
            }
            $inputFileName = $path . $import_xls_file;

			
            
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
                $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $flag = true;
                $i=0;
                foreach ($allDataInSheet as $value) {
                  if($flag){
                    $flag =false;
                    continue;
                  }
                  $this->db->where("is_active",1);
	              $this->db->where("sop_code",$value['A']);
	              //$this->db->where("department_code",$subblock);
	              $query = $this->db->get("mst_sop");
				  if($query->num_rows()>0){
					  $result = $this->Adminmodel->update13_5para_with_1where_and_tbl('mst_product','product_code',$value['A'],'product_name',$value['B'],'product_market',$value['C'],'product_remark',$value['D'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
					  /*if($res>0)
			           {
				         //echo "Imported successfully";
						 $response = array("status"=>1);				
     		           } */
               	  }
				  else{
                  
				  $inserdata[$i]['product_code'] = $value['A'];
                  $inserdata[$i]['product_name'] = $value['B'];
                  $inserdata[$i]['product_market'] = $value['C'];
                  $inserdata[$i]['product_remark'] = $value['D'];
                  $inserdata[$i]['created_by'] = $this->session->userdata('empcode');
				  $i++;
				  }
                  

                }

                //print_r($inserdata);
                unlink($inputFileName);               
                
				if(isset($inserdata)){
                $result = $this->Adminmodel->importDataExcel($inserdata,"mst_product"); 
				}

				//echo "result is ".$result;

                if($result){
                  //echo "Imported successfully";
				  $response = array("status"=>1);
                }else{
                  //echo "ERROR !";
				  $response = array("status"=>0);
                } 
                           
 
          } catch (Exception $e) {
               die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' .$e->getMessage());
            }
          }else{
             
			  echo $error['error'];
			 $response = array("status"=>0);
            }
            
            
   //}
   echo json_encode($response);
    
  }



public function batchDrainpoint(){
    
	// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/uploadDrainpoint');
	}


public function uploadDrainpointExcel(){
	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);

	$this->load->model('Adminmodel');

 
  //if ($this->input->post('submit')) {

            //echo "file name is ". $_FILES['uploadFile']['name'];

     		$result = 0;
			$path = 'uploads/';
            require_once APPPATH . "/third_party/PHPExcel/Classes/PHPExcel.php";
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'xlsx|xls|csv';
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);            
            if (!$this->upload->do_upload('uploadFile')) {
			//if (!$this->upload->do_upload($_FILES['uploadFile']['name'])) {

                $error = array('error' => $this->upload->display_errors());
				$response = array("status"=>0);

            } else {
                $data = array('upload_data' => $this->upload->data());
            }
            if(empty($error)){
              if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
				//$import_xls_file = $_FILES['uploadFile']['name'];
            } else {
                $import_xls_file = 0;
            }
            $inputFileName = $path . $import_xls_file;

			
            
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
                $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $flag = true;
                $i=0;
                foreach ($allDataInSheet as $value) {
                  if($flag){
                    $flag =false;
                    continue;
                  }
                  $this->db->where("is_active",1);
	              $this->db->where("sop_code",$value['A']);
	              //$this->db->where("department_code",$subblock);
	              $query = $this->db->get("mst_sop");
				  if($query->num_rows()>0){
					  $result = $this->Adminmodel->update13_5para_with_1where_and_tbl('mst_drainpoint','drainpoint_code',$value['A'],'drainpoint_name',$value['B'],'drainpoint_remark',$value['C'],'solution_code',$value['E'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
					  /*if($res>0)
			           {
				         //echo "Imported successfully";
						 $response = array("status"=>1);				
     		           } */
               	  }
				  else{
                  
				  $inserdata[$i]['drainpoint_code'] = $value['A'];
                  $inserdata[$i]['drainpoint_name'] = $value['B'];
                  $inserdata[$i]['drainpoint_remark'] = $value['C'];
                  $inserdata[$i]['solution_code'] = $value['D'];
				  //$inserdata[$i]['solution_code'] = $value['E'];
                  $inserdata[$i]['created_by'] = $this->session->userdata('empcode');
				  $i++;
				  }
                  

                }

                //print_r($inserdata);
                unlink($inputFileName);               
                
				if(isset($inserdata)){
                $result = $this->Adminmodel->importDataExcel($inserdata,"mst_drainpoint"); 
				}

				//echo "result is ".$result;

                if($result){
                  //echo "Imported successfully";
				  $response = array("status"=>1);
                }else{
                  //echo "ERROR !";
				  $response = array("status"=>0);
                } 
                           
 
          } catch (Exception $e) {
               die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' .$e->getMessage());
            }
          }else{
             
			  echo $error['error'];
			 $response = array("status"=>0);
            }
            
            
   //}
   echo json_encode($response);
    
  }



public function batchEquipment(){
    
	// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
    $this->load->view('Admin/uploadEquipment');
	}


public function uploadEquipmentExcel(){
	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);

	$this->load->model('Adminmodel');

 
  //if ($this->input->post('submit')) {

            //echo "file name is ". $_FILES['uploadFile']['name'];

     		$result = 0;
			$path = 'uploads/';
            require_once APPPATH . "/third_party/PHPExcel/Classes/PHPExcel.php";
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'xlsx|xls';
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);            
            if (!$this->upload->do_upload('uploadFile')) {
			//if (!$this->upload->do_upload($_FILES['uploadFile']['name'])) {

                $error = array('error' => $this->upload->display_errors());
				$response = array("status"=>0);

            } else {
                $data = array('upload_data' => $this->upload->data());
            }
            if(empty($error)){
              if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
				//$import_xls_file = $_FILES['uploadFile']['name'];
            } else {
                $import_xls_file = 0;
            }
            $inputFileName = $path . $import_xls_file;

			
            
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
                $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $flag = true;
                $i=0;
                foreach ($allDataInSheet as $value) {
                  if($flag){
                    $flag =false;
                    continue;
                  }
                  $this->db->where("is_active",1);
	              $this->db->where("sop_code",$value['A']);
	              //$this->db->where("department_code",$subblock);
	              $query = $this->db->get("mst_sop");
				  if($query->num_rows()>0){
					  $result = $this->Adminmodel->update19_8para_with_1where_and_tbl('mst_equipment','equipment_code',$value['A'],'equipment_name',$value['B'],'equipment_type',$value['C'],'equipment_remarks',$value['D'],'room_code',$value['E'],'drain_point_code',$value['F'],'sop_code',$value['G'],'modified_by',$this->session->userdata('empcode'),'modified_on',date("Y-m-d H:i:s"));
					  /*if($res>0)
			           {
				         //echo "Imported successfully";
						 $response = array("status"=>1);				
     		           } */
               	  }
				  else{
                  
				  $inserdata[$i]['equipment_code'] = $value['A'];
                  $inserdata[$i]['equipment_name'] = $value['B'];
                  $inserdata[$i]['equipment_icon'] = 'default.png';
                  $inserdata[$i]['equipment_type'] = $value['C'];
				  $inserdata[$i]['equipment_remarks'] = $value['D'];
				  $inserdata[$i]['room_code'] = $value['E'];
                  $inserdata[$i]['drain_point_code'] = $value['F'];
				  $inserdata[$i]['sop_code'] = $value['G'];
                  $inserdata[$i]['created_by'] = $this->session->userdata('empcode');
				  $i++;
				  }
                   

                }

                //print_r($inserdata);
                unlink($inputFileName);               
                
				if(isset($inserdata)){
                $result = $this->Adminmodel->importDataExcel($inserdata,"mst_equipment"); 
				}

				//echo "result is ".$result;

                if($result){
                  //echo "Imported successfully";
				  $response = array("status"=>1);
                }else{
                  //echo "ERROR !";
				  $response = array("status"=>0);
                } 
                           
 
          } catch (Exception $e) {
               die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' .$e->getMessage());
            }
          }else{
             
			  echo $error['error'];
			 $response = array("status"=>0);
            }
            
            
   //}
   echo json_encode($response);
    
  }






}