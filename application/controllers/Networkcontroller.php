<?php

/* * *
 * @author Nitin Mittal
 * @Desc Manages Cron Jobs For System
 */
date_default_timezone_set('Asia/Kolkata');

class Networkcontroller extends CI_Controller {

    const AD_LDAP_IP = '10.4.0.11';
    const AD_LDAP_PORT = '389';
    const SMTP_IP = '172.30.58.249';
    const SMTP_PORT = '25';
    const DB_IP = '10.4.0.88';
    const DB_USER = 'root';
    const DB_PWD = 'Sp!l@1234';
    const DB_NAME = 'elogbook';

    public function __construct() {
        parent::__construct();
    }

    public function ADLDAPIPTEST() {
        if ($fp = fsockopen(AD_LDAP_IP, AD_LDAP_PORT)) {
            $this->insertInDb('ADLDAPIP.txt');
        } else {
            $this->writeOnFile('ADLDAPIP.txt');
        }
        fclose($fp);
    }

    public function SMTPTEST() {
        if ($fp = fsockopen(SMTP_IP, SMTP_PORT)) {
            insertInDb('SMTP.txt');
        } else {
            writeOnFile('SMTP.txt');
        }
        fclose($fp);
    }

    public function MysqlTEST() {
        $mysqli = new mysqli(DB_IP, DB_USER, DB_PWD, DB_NAME);
        if ($mysqli->connect_errno) {
            writeOnFile('Mysql.txt');
            echo "Failed to connect to MySQL: " . $mysqli->connect_error;
            exit();
        } else {
            insertInDb('Mysql.txt');
        }
        $mysqli->close();
    }

    public function writeOnFile($fileName = '') {
        $fn = fopen($fileName, "r");
        $result = fgets($fn);
        $no_of_lines = count(file($fileName));
        $handle = fopen($fileName, "w+");
        $date = date('Y-m-d H:i:s');
        if ($no_of_lines > 0) {
            //do nothing
        } else {
            fwrite($handle, $date);
        }
        fclose($handle);
    }

    public function insertInDb($fileName = '') {
        $this->load->model('Ptscommon');
        $fn = fopen($fileName, "r");
        $result = fgets($fn);
        $txt = file_get_contents($fileName);
        if ($result) {
            $handle = fopen($fileName, "w+");
            $date = date('Y-m-d H:i:s');
            switch ($fileName) {
                case 'ADLDAPIP.txt':
                    $service = "LDAP_AD";
                    break;
                case 'SMTP.txt':
                    $service = "SMTP";
                    break;
                case 'Mysql.txt':
                    $service = 'Mysql';
                    break;
                default:
                    $service = "";
            }
            $post = ['down_time' => $result, 'up_time' => $date, 'service_type' => $service];
            $action_taken = "Record created by " . $service;
            $auditArray = ['created_on' => date('Y-m-d H:i:s'), 'created_by' => 0, 'created_by_user' => "N/A", 'action_text' => $action_taken, 'activity_name' => 'Network Outage', 'type' => 'network_outage', 'table_unique_field1' => '', 'table_unique_field2' => '', 'table_field3' => '', 'command_type' => 'insert', 'record_type' => 'master', 'update_count' => 1, 'table_name' => '', 'primary_id' => 0, 'table_extra_field' => json_encode($post)];
            $this->Ptscommon->addAuditTrailHistry($auditArray);
            fwrite($handle, '');
            fclose($handle);
        }
    }

    public function deleteExceptionFiles() {
        $dayBeforeYesterday = date('Y-m-d', strtotime("-1 days"));
        $fileToBeDeleted = 'exception_' . $dayBeforeYesterday . '.txt';
        unlink($fileToBeDeleted);
    }

    public function captureNetworkStatus() {
        $downtime=isset($_POST['down_time'])?$_POST['down_time']:'';
        $up_time=isset($_POST['up_time'])?$_POST['up_time']:'';
        $msg=isset($_POST['msg'])?$_POST['msg']:'';
        $url=isset($_POST['url'])?$_POST['url']:'';
        $record_type= isset($_POST['record_type'])?$_POST['record_type']:'';
        $post = ['down_time'=> date('d-M-Y H:i:s',strtotime($downtime)), 'up_time' => date('d-M-Y H:i:s',strtotime($up_time)),"url"=>$url];
        if(!empty($downtime) && !empty($url)){
            $action_taken = "Record created by Network Error";
            $auditArray = ['created_on' => date('Y-m-d H:i:s'), 'created_by' => 0, 'created_by_user' => "N/A", 'action_text' => $action_taken, 'activity_name' => 'Network Outage', 'type' => 'network_outage', 'table_unique_field1' => '', 'table_unique_field2' => '', 'table_field3' => '', 'command_type' => 'insert', 'record_type' => $record_type, 'update_count' => 1, 'table_name' => '', 'primary_id' => 0, 'table_extra_field' => json_encode($post)];
            $this->Ptscommon->addAuditTrailHistry($auditArray);
            return true;
        }else{
            return false;
        }
    }

}
