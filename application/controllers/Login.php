<?php

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
            
         if (isset($_SESSION) && !empty($_SESSION['empemail'])) {            
            redirect(base_url() . 'User/home');         
        }
    }

    public function index() {

        $this->session->set_userdata('attempt', 0);
        $this->load->view('login', ['data' => array()]);
    }

    public function userlogin() {
        $server_ip = $_SERVER['SERVER_ADDR'];
        $this->load->model('Ptscommon');
        $userid = $this->input->post('emailid');
        $pwd = $this->input->post('pwd');
//        echo $pwd;exit;
        $password = $this->input->post('password');
        $auth = base64_decode(base64_decode($password));
        $this->load->model('Loginmodel');
        $emp_data = $this->Loginmodel->checkUserExist($userid);
        $empname = !empty($emp_data) ? $emp_data['emp_name'] : '';
        $empemail = !empty($emp_data) ? $emp_data['emp_email'] : '';
        $isDirectoryUser = !empty($emp_data) ? $emp_data['is_directory'] : '0';
        $userCheckStatus = 0;
        if ($isDirectoryUser == 1) {
            $auth = base64_decode(base64_decode($password));
            $userExist = file_get_contents(URL_BASE_TEST_AD ."username=$userid&password=$auth");
            $userCheck = json_decode($userExist, true);
            $userCheckStatus = ($userCheck['status'] == true || $userCheck['status'] == 1) ? '1' : 0;
        }
        $userids = $empid = !empty($emp_data) ? $emp_data['id'] : '';
        $q = $this->Loginmodel->userauth($userid, $pwd, $userCheckStatus);
//        echo '<pre>';print_r($q->row_array()['id']);exit;
        $attempt_user = $this->session->userdata('attempt_user');
        if (isset($attempt_user) && $attempt_user != $userid) {
            $this->session->set_userdata('attempt', 0);
        }
//        echo '<pre>';print_r($_SESSION);exit;
        $this->session->set_userdata('attempt_user', $userid);
        $this->db->order_by("id", "DESC");
        $this->db->where("record_type", "master");
        $this->db->where("type!=", "network_outage");
        $this->db->or_where("type", "user_login");
        $user_status = $this->db->get_where("pts_elog_history", array("created_by" => $q->row_array()['id']))->row_array();
        $to_time = strtotime(date('Y-m-d H:i:s'));
        $from_time = strtotime($user_status['created_on']);
        $diff = round(abs(($to_time - $from_time) / 60), 2);
//        echo $diff;exit;
          if(isset($_SESSION) && isset($_SESSION['empemail'])){        
        if (isset($_SESSION['empemail']) && $_SESSION['empemail'] != $this->input->post('emailid')) {
            $data = array('error' => "You can't logged in, already an user is logged in from the same Browser.");
            $this->session->set_flashdata('error', "You can't logged in, already an user is logged in from the same Browser.");
            redirect(base_url());
            //$this->load->view('login', ['data' => $data]);
        }
        if (isset($_SESSION) && $_SESSION['empemail'] != '' && ($_SESSION['empemail'] == $this->input->post('emailid')) && $diff < 3) {
            $data = array('error' => "You are already logged in from other machine/terminal");
            $this->session->set_flashdata('error', 'You are already logged in from other machine/terminal');
            redirect(base_url());
            //$this->load->view('login', ['data' => $data]);
        }
       }
//        exit;
        if ($q->num_rows() > 0) {
            $row = $q->row_array();
            $emp_id = $row['id'];
            $empcode = $row['emp_code'];
            $empname = $row['emp_name'];
            $empemail = $row['emp_email'];
            $eq = ['login_attampt' => 0];
            $this->db->where("id", $emp_id);
            $this->db->update("mst_employee", $eq);
            if ($row['is_blocked'] == 0) {
                $this->db->order_by("id", "DESC");
                $this->db->where("record_type", "master");
                $this->db->where("type!=", "network_outage");
                $this->db->or_where("type", "user_login");
                $user_status = $this->db->get_where("pts_elog_history", array("created_by" => $emp_id))->row_array();
                $to_time = strtotime(date('Y-m-d H:i:s'));
                $from_time = strtotime($user_status['created_on']);

                $diff = round(abs($to_time - $from_time) / 60, 2);
//                 echo $diff;echo '<pre>';print_r($user_status);exit;
                if ($diff > 3) {
                    $this->db->where("id", $emp_id);
                    $this->db->update("mst_employee", array("login_status" => "0"));
                    $row['login_status'] = "0";
                }
                if ($row['login_status'] == "1") {
                    $data = array('error' => "You are already logged in from other machine/terminal.");
                    $this->load->view('login', ['data' => $data]);
                } else {
                    $this->db->where("id", $emp_id);
                    $this->db->update("mst_employee", array("login_status" => "1"));
                    $this->db->where_in("module_type", array("master", "report", "log"));
                    $prilivege_data = $this->db->get_where("pts_mst_user_mgmt", array("user_id" => $row['id'], "status" => 'active'))->result_array();
//                echo $this->db->last_query();exit;
//                $prilivege_data = $this->checkUserPrivelegeformaster($row['id'], "master");
//                echo '<pre>';print_r($prilivege_data);exit;
                    if (!empty($prilivege_data) || $row['role_id'] == -1) {
                        if ($row['is_first'] == 1) {
                            $auditParams = array('command_type' => 'insert', 'created_by' => $row['id'], 'created_by_name' => $row['emp_name'], 'activity_name' => 'User Login', 'type' => 'user_login', 'uniquefiled1' => 'id:' . $row['id'], 'uniquefiled2' => '', 'uniquefiled3' => '', 'table_name' => 'mst_employee', 'primary_id' => 0, 'log_user_activity_id' => 0, 'post_params' => ['reason' => 'Login successfully for user id : ' . $row['emp_email'] . ' and redirected to reset password']);
                            $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                            $data = $row;
                            $this->load->view('login', ['data' => $data]);
                        } else {
                            $empcode = $row['emp_code'];
                            $empname = $row['emp_name'];
                            $empemail = $row['emp_email'];
                            $empblock_code = $row['block_code'];
                            $emparea_code = $row['area_code'];
                            $roleid = $row['role_id'];
                            $user_id = $row['id'];
                            $empdepartment_name = $this->Loginmodel->getdepartmentname($emparea_code);
                            $role_detail = $this->Loginmodel->getRoleDetailById($roleid);
                            $user_data = [
                                'emp_id' => $user_id,
                                'emp_code' => $empcode,
                                'emp_name' => $empname,
                                'emp_email' => $empemail,
                                'block_code' => $empblock_code,
                                'area_code' => $emparea_code,
                                'role_id' => $roleid,
                                'department_name' => $empdepartment_name,
                                'role_code' => isset($role_detail['role_code']) ? $role_detail['role_code'] : '',
                                'role_description' => isset($role_detail['role_description']) ? $role_detail['role_description'] : '',
                                'login_time' => date('Y-m-d H:i:s')
                            ];


                            $this->session->set_userdata('user_id', $user_id);
                            $this->session->set_userdata('empcode', $empcode);
                            $this->session->set_userdata('empname', $empname);
                            $this->session->set_userdata('empemail', $empemail);
                            $this->session->set_userdata('emparea_code', $emparea_code);
                            $this->session->set_userdata('empblock_code', $empblock_code);
                            $this->session->set_userdata('empdepartment_name', $empdepartment_name);
                            $this->session->set_userdata('roleid', $roleid);
                            $this->session->set_userdata('attempt', 0);
                            $this->session->set_userdata('role_description', $role_detail['role_description']);
                            $user_data['session_id'] = $this->session->session_id;
                            $auditParams = array('command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'User Login', 'type' => 'user_login', 'uniquefiled1' => 'id:' . $empid, 'uniquefiled2' => '', 'uniquefiled3' => '', 'table_name' => 'mst_employee', 'primary_id' => 0, 'log_user_activity_id' => 0, 'post_params' => ['reason' => 'Login successfully for user id : ' . $row['emp_email']]);
                            $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                            $this->Loginmodel->adduserloginhistory($user_data);
                            return redirect(base_url() . 'User/home');
                        }
                    } else {
                        $data = array('error' => "You have no rights to access, please contact to the administrator...!");
                        $this->load->view('login', ['data' => $data]);
                        $this->session->set_userdata('attempt', 0);
                    }
                }
            } else {
                $auditParams = array('command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'User Login', 'type' => 'user_login', 'uniquefiled1' => 'id:' . $empid, 'uniquefiled2' => '', 'uniquefiled3' => '', 'table_name' => 'mst_employee', 'primary_id' => 0, 'log_user_activity_id' => 0, 'post_params' => ['reason' => 'Login failed Due to Blocked Account for user id : ' . $empemail]);
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                $data = array('error' => "Your account has been blocked, please contact the administrator.");
                $this->load->view('login', ['data' => $data]);
                $this->session->set_userdata('attempt', 0);
            }
        } else {
            $attempt = $this->session->userdata('attempt');
            $attempt++;
            $this->session->set_userdata('attempt', $attempt);
            if ($attempt < 5) {
                $data = array('error' => "User id or password is incorrect");
                $auditParams = array('command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'User Login', 'type' => 'user_login', 'uniquefiled1' => 'id:' . $empid, 'uniquefiled2' => '', 'uniquefiled3' => '', 'table_name' => 'mst_employee', 'primary_id' => 0, 'log_user_activity_id' => 0, 'post_params' => ['reason' => 'Login wrong attempt No. ' . $attempt . ' for user id : ' . $userid]);
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                $this->load->view('login', ['data' => $data]);
            } else {
                $auditParams = array('command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'User Login', 'type' => 'user_login', 'uniquefiled1' => 'id:' . $empid, 'uniquefiled2' => '', 'uniquefiled3' => '', 'table_name' => 'mst_employee', 'primary_id' => 0, 'log_user_activity_id' => 0, 'post_params' => ['reason' => 'Account blocked due to 5 wrong attempts for user id : ' . $userid]);
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                //$res = $this->Adminmodel->Blockeduser('mst_employee','emp_email',$userid);
                $eq = array("is_blocked" => 1, 'login_attampt' => 0, 'block_datetime' => date('Y-m-d H:i:s'));
                $this->db->where("emp_email", $userid);
                $this->db->update("mst_employee", $eq);

                $data = array('error' => "Your account has been blocked, please contact the administrator.");
                $this->load->view('login', ['data' => $data]);
            }
        }
    }

    public function checkUserPrivelegeformaster($user_id, $module_type) {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("module_type" => $module_type, "user_id" => $user_id);
        $res = $this->Ptscommon->checkUserPrivelegeformaster($param);
        if (!empty($res)) {
            return $res;
        } else {
            $res = array();
            return array();
        }
    }

    /* public function userlogin()
      {
      $userid = $this->input->post('emailid');
      $pwd = $this->input->post('pwd');
      $this->load->model('Loginmodel');
      $q = $this->Loginmodel->userauth($userid,$pwd);
      if($q != false)
      {
      $row = $q->row_array();
      $empcode = $row['emp_code'];
      $empname = $row['emp_name'];
      $empemail = $row['emp_email'];
      $emparea_code = $row['area_code'];
      $roleid = $row['role_id'];
      $empdepartment_name = $this->Loginmodel->getdepartmentname($emparea_code);
      $empblock_code = $this->Loginmodel->getblock($emparea_code);
      $this->session->set_userdata('empcode', $empcode);
      $this->session->set_userdata('empname', $empname);
      $this->session->set_userdata('empemail', $empemail);
      $this->session->set_userdata('emparea_code', $emparea_code);
      $this->session->set_userdata('empblock_code', $empblock_code);
      $this->session->set_userdata('empdepartment_name', $empdepartment_name);
      $this->session->set_userdata('roleid', $roleid);

      if($roleid==1)
      {
      return redirect(base_url(). 'User/home');
      }
      else if($roleid==2)
      {
      return redirect(base_url(). 'User/home');
      }
      else if($roleid==3)
      {
      return redirect(base_url(). 'User/home');
      }
      else if($roleid==4)
      {
      return redirect(base_url(). 'User/home');
      }
      else if($roleid==5)
      {
      return redirect(base_url(). 'User/home');
      }
      else if($roleid==6)
      {
      return redirect(base_url(). 'User/home');
      }
      }
      else
      {
      $this->load->view('login');
      }
      } */

    //Rest Password
    public function resetPassword() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $objId = isset($_POST['user_id']) ? $_POST['user_id'] : '';
        $password = isset($_POST['password']) ? $_POST['password'] : '';
        $password_used = $this->Ptscommon->checkUserPasswordUsed($objId, $password);
        if ($password_used) {
            $responseresult = array(STATUS => TRUE, "result" => False, MESSAGE => 'Password Can Not Be The Same As Last 5 Passwords');
            echo json_encode($responseresult);
            exit;
        }
        $expire_on = date('Y-m-d H:i:s', strtotime("+60 days"));
        $arr2 = array("emp_password" => $password, "modified_by" => $this->session->userdata('empname'), "modified_on" => date("Y-m-d H:i:s"), "emp_remark" => "Password Reset", "is_first" => '0', "expire_on" => $expire_on);
        $this->db->where("id", $objId);
        $res = $this->db->update("mst_employee", $arr2);
        $this->db->insert("mst_employee_password_history", array("user_id" => $objId, "password" => $password, "created_by" => $objId));
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Password Reset Successfully.');
            echo json_encode($responseresult);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please try again.');
            echo json_encode($responseresult);
        }
    }

}
