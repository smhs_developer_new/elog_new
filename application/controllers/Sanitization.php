<?php

class Sanitization extends CI_Controller{


  public function __construct()
    {
      parent::__construct();
      if(!$this->session->userdata('empemail'))
      {
        return redirect("login");
      }
	  if(!isset($_SERVER['HTTP_REFERER'])) {         
		  return redirect("login?err=1");
		  //$this->load->view('login');
        } 
    }

  /*public function area()
  {
    $empblock_code = $this->session->userdata('empblock_code');
    $this->load->model('Sanitizationmodel');
    $area = $this->Sanitizationmodel->get_dtl2('block_code',$empblock_code,'mst_area');
    $this->session->userdata('empblock_code');
      // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
    $this->load->view('sanitization/area', ['area'=>$area]);
  }

  public function room()
  {
    $area_code =  $_GET['area'];
    $this->session->set_userdata('area_code', $area_code);
    $this->load->model('Sanitizationmodel');
    $area_name =$this->Sanitizationmodel->get_area_name($area_code);
    $this->session->set_userdata('area_name', $area_name);
    
    $room = $this->Sanitizationmodel->get_dtl2('area_code',$area_code,'mst_room');
    $data = array('area_code' => $area_code,'room' => $room);
    if($room != false)
    { 
      // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code  
      $this->load->view('sanitization/room', ['data'=>$data]);
    }
    else
    {
      //$this->load->view('production/room', ['room'=>"aasa"]);
    }
  }*/

  public function Exception_error($header)
  {
      $msg = $this->Sanitizationmodel->errMsg($header); 
      $data["header"] = $msg["msg-header"]; 
      $data["message"] = $msg["message"];
      // header menu code
      $result = $this->Usermodel->getActivitytype();
      $this->load->view('header', ['result'=>$result]);
      // End header menu code
      $this->load->view("error500",$data);
      $this->load->view('footer');

  }

  public function area()
  {
    $empblock_code = $this->session->userdata('empblock_code');
    $this->load->model('Productionmodel');
    $emparea_code = $this->session->userdata('emparea_code');
    $array = explode(",",$emparea_code);
    $query="";
      foreach ($array as $ar) {
          $query = $query. "area_code = '".$ar."' OR ";
      }
      $query = substr($query, 0, -3);
    $area = $this->Productionmodel->getdtl2($query,'mst_area');
    // header menu code
      $result = $this->Usermodel->getActivitytype();
      $this->load->view('header', ['result'=>$result]);
      // End header menu code
    $this->load->view('sanitization/area', ['area'=>$area,'array'=>$array]);
    $this->load->view('footer');
  }

  public function room()
  {
    $area_code = $_GET['area'];
    $this->session->set_userdata('area_code', $area_code);

    //Bhupendra Get and Set Department Name in session
    $q = $this->Productionmodel->get_dtl2('area_code',$area_code,'mst_department');
    $res = $q->row_array();
    $this->session->set_userdata('empdepartment_name', $res['department_name']);
    //Bhupendra End Get and et Department Name is session

    $this->load->model('Productionmodel');
      $area_name =$this->Productionmodel->get_area_name($area_code);
      $this->session->set_userdata('area_name', $area_name);
    $room = $this->Productionmodel->get_dtl2('area_code',$area_code,'mst_room');
    $data = array('area_code' => $area_code,'room' => $room);
    if($room != false)
    {   
      // header menu code
      $result = $this->Usermodel->getActivitytype();
      $this->load->view('header', ['result'=>$result]);
      // End header menu code
      $this->load->view('sanitization/room', ['data'=>$data]);
      $this->load->view('footer');
    }
    else
    {
      //$this->load->view('production/room', ['room'=>"aasa"]);
    }
  }

  public function activity()
  {
    $area_code = $this->session->userdata('area_code');
    $room_code =  $_GET['room'];
	$this->load->model('Activitymodel');
	$activityByRole = $this->Activitymodel->getActivityByRoles($this->session->userdata('roleid'));
    $this->load->model('Sanitizationmodel');
    $activity = $this->Sanitizationmodel->get_dtl4('area_code',$area_code,'activity_type','Sanitization','mst_activity',$activityByRole);
    $data = array('room_code' => $room_code,'activity' => $activity);

    if($activity != "" && $activity != null && count($activity->result())>0)
    { 
      // header menu code
      $result = $this->Usermodel->getActivitytype();
      $this->load->view('header', ['result'=>$result]);
      // End header menu code  
      $this->load->view('sanitization/activity', ['data'=>$data]);    
    }
    else
    {   
      $errheader = "ERR_ActivityNotFound"; 
      redirect('sanitization/Exception_error/'.$errheader); 
    }
  }

  public function batch()
  {

    $activity_code =  $_GET['activitycode'];
    $this->session->set_userdata('activity_code', $activity_code);
    $this->load->model('Sanitizationmodel');
    $batchcode = $this->Sanitizationmodel->get_dtl1('trn_batch');
    $data = array('activity_code' => $activity_code,'batchcode' => $batchcode);
    if($batchcode != false)
    { 
       $view = $this->Sanitizationmodel->get_view($activity_code);
       $this->$view();
       /*
      if($activity_code==27){
      //echo $view;
      $this->$view();
      //$this->solutionPreparationview();
      }
      if($activity_code==29){
      $this->drainPointview();
      }
      if($activity_code==30){
      $this->dailyCleaningview(); 
      } 
      //$this->load->view('process/batch', ['data'=>$data]);
      */
    }
    else
    {
      //$this->load->view('production/room', ['room'=>"aasa"]);
    }     
  }  


public function solname(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Sanitizationmodel');
    $response = $this->Sanitizationmodel->solutionNames($_POST["str"]);
    echo json_encode($response);
  } 

public function solnamegetval(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Sanitizationmodel');
    $response = $this->Sanitizationmodel->solNamegetval($_POST["str"]);
    echo json_encode($response);
  }

public function solUsed(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Sanitizationmodel');
    $response = $this->Sanitizationmodel->solUsed($_POST["str"]);
    echo json_encode($response);
}

public function solUseddata(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Sanitizationmodel');
    $response = $this->Sanitizationmodel->solUseddata($_POST["docid"]);
    echo json_encode($response);
}   

  public function solpre(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Sanitizationmodel');
    $response = $this->Sanitizationmodel->submitSolutionPreparation($_POST);
    echo json_encode($response);
  } 


  public function solpre2(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $arr = array("check_remark"=>$this->input->post('checkremark'),"solution_destroy_checked_by"=>$this->input->post('soldesby'),"is_active"=>0);
    $this->db->where("doc_id",$this->input->post('doc_id'));
    
    if ($this->db->update("trn_solutiondetails",$arr)) {
        $response = array("status"=>1);
     } 
     else {
        $response = array("status"=>0);
  	}

  echo json_encode($response);
  }


  public function solutionPreparation(){
    $this->load->model('Usermodel');
    $this->load->model('Sanitizationmodel');
    $doc_id = $this->Usermodel->GetDocumentId("trn_solutiondetails","doc_id","mst_document","doc_id","doc_id","WSP00000000");
    
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code 
    

      $cc = $this->Sanitizationmodel->getsolNames();
    if($cc > 0){
        $data["docid"] = $doc_id;
        $this->load->view('sanitization/solutionPreparation',$data);
      }
      else{
        $errheader = "ERR_solutionPreparation";
		$msg = $this->Sanitizationmodel->errMsg($errheader);
        $data["header"] = $msg["msg-header"];
        $data["message"] = $msg["message"];
        //"Issue occured in following Tables . Please contact administrator.<br> mst_drainpoint";
        $this->load->view("error500",$data);
        
      }
  }

  public function solutionPreparationview(){
      $this->load->model('Sanitizationmodel');
      $count = $this->Sanitizationmodel->checkedBycount("trn_solutiondetails","solution_destroy_checked_by");

      if($count>0){
        $data["list"] = $this->Sanitizationmodel->solutionPreparationview();
        // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
        $this->load->view("sanitization/solutionPreparationview",$data);
      }
      else{
        redirect("Sanitization/solutionPreparation");
      }
      
  }
  
  public function solutionPreparationviewnow($id){
    $this->load->model('Sanitizationmodel');
    $row = $this->Sanitizationmodel->getsolutionPreparationviewnow($id);
    $data["row"] = $this->Sanitizationmodel->getsolutionPreparationviewnow($id);
    $data["logs"] = $this->Sanitizationmodel->getsolutionPreparationlogs($row["doc_id"]);
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
    $this->load->view("sanitization/solutionPreparationviewnow",$data);
  }    

  public function solutionPreparationtakeover(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Sanitizationmodel');
    $response = $this->Sanitizationmodel->solutionPreparationtakeover($_POST);
    echo json_encode($response);
  }

  public function solutionPreparationdesrory(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Sanitizationmodel');
    $response = $this->Sanitizationmodel->solutionPreparationdesrory($_POST);
    echo json_encode($response);
  }

  public function drainPoint(){
    $this->load->model('Usermodel');
      $doc_id = $this->Usermodel->GetDocumentId("trn_drainpointdetails","document_no","mst_document","doc_id","doc_id","WDP00000000");
      $this->load->model('Sanitizationmodel');
      
      // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
      if($this->Sanitizationmodel->getSolused() > 0){
      $cc = count($this->Sanitizationmodel->getdrainPoints());
      if($cc > 0){
        $data["docid"] = $doc_id;
        $data["items"] = $this->Sanitizationmodel->getdrainPoints();
        $this->load->view('sanitization/drainPoint',$data);
      }
      else{
        $errheader = "ERR_drainPoint";
		$msg = $this->Sanitizationmodel->errMsg($errheader);
        $data["header"] = $msg["msg-header"];
        $data["message"] = $msg["message"];
        $this->load->view("error500",$data);
        
      }
      }
      else{
        $errheader = "ERR_drainPointsolstock";
        $msg = $this->Sanitizationmodel->errMsg($errheader);
        $data["header"] = $msg["msg-header"];
        $data["message"] = $msg["message"];
        $this->load->view("error500",$data);
      }
  }

  public function drainPointview(){
    $this->load->model('Sanitizationmodel');
      $count = $this->Sanitizationmodel->checkedBycount("trn_drainpointdetails","checked_by");
      if($count>0){
        $data["list"] = $this->Sanitizationmodel->drainPointview();
        // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
        $this->load->view("sanitization/drainPointview",$data);
      }
      else{
        redirect("Sanitization/drainPoint");
      }
  }

  public function dailyCleaningdetails(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Sanitizationmodel');
    $response = $this->Sanitizationmodel->insertDailycleaning($_POST);
    echo json_encode($response);
  }

  public function drainPointdetails(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Sanitizationmodel');
    $response = $this->Sanitizationmodel->insertDrainpoint($_POST);
    echo json_encode($response);
  }

  public function drainPointviewnow($id){
    $this->load->model('Sanitizationmodel');
    $row = $this->Sanitizationmodel->getdrainPointviewnow($id);
    $data["row"] = $this->Sanitizationmodel->getdrainPointviewnow($id);
    $data["logs"] = $this->Sanitizationmodel->getdrainPointviewlogs($row["document_no"]);
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
    $this->load->view("sanitization/drainPointviewnow",$data);
  }

  public function drainPointfinish(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Sanitizationmodel');
    $response = $this->Sanitizationmodel->drainPointfinish($_POST);
    echo json_encode($response);
  }

  public function drainPointtakeover(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Sanitizationmodel');
    $response = $this->Sanitizationmodel->drainPointtakeover($_POST);
    echo json_encode($response);
  }

  public function dailyCleaning(){
    $this->load->model('Usermodel');
        $doc_id = $this->Usermodel->GetDocumentId("trn_dailycleaning","document_no","mst_document","doc_id","doc_id","WDC00000000");
        $this->load->model('Sanitizationmodel');
        // header menu code
        $result = $this->Usermodel->getActivitytype();
        $this->load->view('header', ['result'=>$result]);
        // End header menu code

      if($this->Sanitizationmodel->getSolused() > 0){
      $cc = count($this->Sanitizationmodel->getdrainPoints());
      if($cc > 0){
        $data["docid"] = $doc_id;
        $data["items"] = $this->Sanitizationmodel->getdrainPoints();
        $this->load->view("sanitization/dailyCleaning",$data);
      }
      else{
        /*$errheader = "ERR_dailyCleaning";
		$msg = $this->Sanitizationmodel->errMsg($errheader);
        $data["header"] = $msg["msg-header"];
        $data["message"] = $msg["message"];
        $this->load->view("error500",$data);*/
		$data["docid"] = $doc_id;
		$data["items"] = "No Drain Point Available for Selected Room";
        $this->load->view("sanitization/dailyCleaning",$data);
        
      }
      }
      else{
        $errheader = "ERR_dailyCleaning";
		$msg = $this->Sanitizationmodel->errMsg($errheader);
        $data["header"] = $msg["msg-header"];
        $data["message"] = $msg["message"];
        $this->load->view("error500",$data);
      }
  }

  public function dailyCleaningview(){
      $this->load->model('Sanitizationmodel');
      $count = $this->Sanitizationmodel->checkedBycount("trn_dailycleaning","checked_by");
      if($count>0){
        $data["list"] = $this->Sanitizationmodel->dailyCleaningview();
        // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
        $this->load->view("sanitization/dailyCleaningview",$data);
      }
      else{
        redirect("Sanitization/dailyCleaning");
      }
  }

  public function dailyCleaningviewnow($id){
      $this->load->model('Sanitizationmodel');
      $row = $this->Sanitizationmodel->dailyCleaningviewnow($id);
      $data["row"] = $this->Sanitizationmodel->dailyCleaningviewnow($id);
      $data["logs"] = $this->Sanitizationmodel->dailyCleaninglogs($row["document_no"]);
      // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
      $this->load->view("sanitization/dailyCleaningviewnow",$data);
  }

  public function dailyCleaningtakeover(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Sanitizationmodel');
    $response = $this->Sanitizationmodel->dailyCleaningtakeover($_POST);
    echo json_encode($response);
  }

  public function dailyCleaningfinish(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Sanitizationmodel');
    $response = $this->Sanitizationmodel->dailyCleaningfinish($_POST);
    echo json_encode($response);
  }

 } 