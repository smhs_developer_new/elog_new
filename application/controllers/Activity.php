<?php

class Activity extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('empemail'))
		{
			return redirect("login");
		}
		if(!isset($_SERVER['HTTP_REFERER'])) {         
		  return redirect("login?err=1");
		  //$this->load->view('login');
        } 
	}

	public function activityMapping(){

		if(empty($_POST))
		{
		$this->load->model('Activitymodel');
		$data["areaselected"] = "A-000";
		$activity = $this->Activitymodel->getActivities($data);
		$data["activity"] = $activity;
		$data["role"] = $this->Activitymodel->getRole();
		$data["blocks"] = $this->Activitymodel->getBlocks();
        
		}
		else
		{
		$this->load->model('Activitymodel');
		
		//$data["areaselected"] = $_POST['area'];
		$data["blockselected"] = $_POST['block'];
		$activity = $this->Activitymodel->getActivitiesByBlock($data);
		//$data["blockselected"] = $_POST['block'];
		//$data["subblockselected"] = $_POST['subblock'];
		$data["activity"] = $activity;
		$data["role"] = $this->Activitymodel->getRole();
		$data["blocks"] = $this->Activitymodel->getBlocks();
		//$data["subblocks"] = $this->Activitymodel->getSubblocks($_POST);
		//$data["areas"] = $this->Activitymodel->getAreas($_POST);
		}
		/*echo"<pre>";
		print_r($_POST);
		echo"</pre>";*/
		// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
		   $this->load->view('activity/activityMapping',$data);
			
	}

	

	public function getSubblocks(){
		header('Access-Control-Allow-Origin: *');  
		header("Content-Type: application/json", true);
		$this->load->model('Activitymodel');
		$response = $this->Activitymodel->getSubblocks($_POST);
		echo json_encode($response);
	}

	public function getAreas(){
		header('Access-Control-Allow-Origin: *');  
		header("Content-Type: application/json", true);
		$this->load->model('Activitymodel');
		$response = $this->Activitymodel->getAreas($_POST);
		echo json_encode($response);
	}

	public function dev(){
		header('Access-Control-Allow-Origin: *');  
		header("Content-Type: application/json", true);
		$this->db->group_by("role_id");
		$dd = $this->db->get("trn_activityrole");
		foreach ($dd->result_array() as $k) {
			$this->db->where("role_id",$k["role_id"]);
			$dd2 = $this->db->get("trn_activityrole");
			$nres[] = array("rid"=>$k["role_id"],"ndata"=>$dd2->result_array());
		 	
		}

		echo json_encode($data); 	
	}
	
	public function activityMap(){
		header('Access-Control-Allow-Origin: *');  
		header("Content-Type: application/json", true);
		$this->load->model('Activitymodel');
		$response = $this->Activitymodel->insertActivityRole($_POST);
		echo json_encode($response);
	}
	
	public function activityview()
	{
		$this->load->model('Activitymodel');
		$activity = $this->Activitymodel->getTrnActivity();
		$data["trnactivity"] = $activity;	
		// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
		$this->load->view('activity/activityView',$data);   
	} 
	  
	public function activityDelete()
	{
		$this->load->model('Activitymodel');
		$activity = $this->Activitymodel->deleteTrnActivityById($_GET['id']);		
		redirect('activity/activityView', 'refresh');  
	}
	
}
