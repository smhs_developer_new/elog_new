<?php

class Production extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('empemail'))
		{
			return redirect("login");
		}
		
		if(!isset($_SERVER['HTTP_REFERER'])) {

         
		  //return redirect("login?err=1");
		  //$this->load->view('login');
        } 
		
	}

	public function Exception_error($header)
	{
			$msg = $this->Sanitizationmodel->errMsg($header); 
			$data["header"] = $msg["msg-header"]; 
			$data["message"] = $msg["message"];
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view("error500",$data);
			$this->load->view('footer');

	}

	public function area()
	{
		$empblock_code = $this->session->userdata('empblock_code');
		$this->load->model('Productionmodel');
		$emparea_code = $this->session->userdata('emparea_code');
		$array = explode(",",$emparea_code);
		$query="";
    	foreach ($array as $ar) {
    			$query = $query. "area_code = '".$ar."' OR ";
    	}
    	$query = substr($query, 0, -3);
		$area = $this->Productionmodel->getdtl2($query,'mst_area');
		// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
		$this->load->view('process/area', ['area'=>$area,'array'=>$array]);
		$this->load->view('footer');
	}

	public function room()
	{
		$area_code = $_GET['area'];
		$this->session->set_userdata('area_code', $area_code);

		//Bhupendra Get and Set Department Name in session
		$q = $this->Productionmodel->get_dtl2('area_code',$area_code,'mst_department');
		$res = $q->row_array();
		$this->session->set_userdata('empdepartment_name', $res['department_name']);
		//Bhupendra End Get and et Department Name is session

		$this->load->model('Productionmodel');
    	$area_name =$this->Productionmodel->get_area_name($area_code);
	    $this->session->set_userdata('area_name', $area_name);
		$room = $this->Productionmodel->get_dtl2('area_code',$area_code,'mst_room');
		$data = array('area_code' => $area_code,'room' => $room);
		if($room != false)
		{		
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('process/room', ['data'=>$data]);
			$this->load->view('footer');
		}
		else
		{
			//$this->load->view('production/room', ['room'=>"aasa"]);
		}
	}

	public function activity()
	{
		$area_code = $this->session->userdata('area_code');
		$room_code =  $_GET['room'];
		$this->session->set_userdata('room_code', $room_code);
		$this->load->model('Activitymodel');
		$activityByRole = $this->Activitymodel->getActivityByRoles($this->session->userdata('roleid'));
		$this->load->model('Productionmodel');
		$activity = $this->Productionmodel->get_dtl4('area_code',$area_code,'activity_type','Production','mst_activity',$activityByRole);

		$data = array('room_code' => $room_code,'activity' => $activity);
		if($activity != "" && $activity != null && count($activity->result())>0)
		{ 
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('process/activity', ['data'=>$data]);
			$this->load->view('footer');			
		}
		else
		{		
			$errheader = "ERR_ActivityNotFound"; 
			redirect('Production/Exception_error/'.$errheader); 
		}
	}

	public function batch()
	{
		$room_code = $this->session->userdata('room_code');
		$area_code = $this->session->userdata('area_code');
		$activity_code =  $_GET['activitycode'];
		$this->session->set_userdata('activity_code', $activity_code);
		$this->load->model('Productionmodel');
		$batchcode = $this->Productionmodel->get_dtl1('trn_batch');
		$product = $this->Productionmodel->get_dtl1('mst_product');
		$data = array('activity_code' => $activity_code,'batchcode' => $batchcode, 'product' => $product);
		
		if($batchcode != false)
		{	
			if($activity_code==26){		
			$this->accessoryCleaningview();
			}

			if($activity_code==5){		
				$pcount =$this->Productionmodel->countPortable();
				//echo $pcount;
				//exit;
		   		if($pcount != 0)
				{
					$this->portableEquipmentcleaningview();
					
				}
				else
				{	
					//$this->load->model('Productionmodel');
					//$batchcode = $this->Productionmodel->get_dtl1('trn_batch');
					//$data = array('activity_code' => $activity_code,'batchcode' => $batchcode);
					// header menu code
					$result = $this->Usermodel->getActivitytype();
					$this->load->view('header', ['result'=>$result]);
					// End header menu code	
					$this->load->view('process/batchPortable', ['data'=>$data]);
				}

			}

			if($activity_code==4){
			$this->inprocessContainercleaningview();	
			}

			if($activity_code==1)
			{

				//Added by Bhupendra 23Aug2019
				//checking is batch runing in the same area and same room then skip batch input
				$q = $this->Productionmodel->get_Header4('area_code',$area_code,'status',1,'trn_operationlogheader');
				if($q->num_rows() > 0)
				{
					$res = $q->row_array();
					$batchno = $res["batch_no"];
					$lotno = $res["lot_no"];
					$dev = $this->Productionmodel->get_dtl2('batch_code',$batchno,'trn_batch');
	    			$response = $dev->row_array();

	    			$pcode = $response['product_code'];
	    			$this->session->set_userdata('pcode', $pcode);
   					$pdesc = $response['product_description'];
   					$this->session->set_userdata('pdesc', $pdesc);
   					$market = $response['market'];
   					$this->session->set_userdata('market', $market);
   					$bcode = $batchno;
   					$this->session->set_userdata('bcode', $bcode);
   					$bsizeUOM = $response['batch_size'];
   					$this->session->set_userdata('bsizeUOM', $bsizeUOM);
   					$UOM = $response['UOM'];
   					$this->session->set_userdata('UOM', $UOM);
   					$this->session->set_userdata('lotno', $lotno);
   					redirect("Production/operation");
   					//End Bhupendra code of 23aug2019
				}	
				else
				{				
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('process/batch', ['data'=>$data]);
			}
			}

			if($activity_code==2 || $activity_code==3)
			{	
				$q = $this->Productionmodel->get_Header4('area_code',$area_code,'status',1,'trn_operationlogheader');
				if($q->num_rows() > 0)
				{
					//Added by Bhupendra 23Aug2019
					//checking is batch runing in the same area and same room then skip batch input
					$res = $q->row_array();
					$batchno = $res["batch_no"];
					$lotno = $res["lot_no"];
					$dev = $this->Productionmodel->get_dtl2('batch_code',$batchno,'trn_batch');
	    			$response = $dev->row_array();

	    			$pcode = $response['product_code'];
	    			$this->session->set_userdata('pcode', $pcode);
   					$pdesc = $response['product_description'];
   					$this->session->set_userdata('pdesc', $pdesc);
   					$market = $response['market'];
   					$this->session->set_userdata('market', $market);
   					$bcode = $batchno;
   					$this->session->set_userdata('bcode', $bcode);
   					$bsizeUOM = $response['batch_size'];
   					$this->session->set_userdata('bsizeUOM', $bsizeUOM);
   					$UOM = $response['UOM'];
   					$this->session->set_userdata('UOM', $UOM);
   					$this->session->set_userdata('lotno', $lotno);
   					if($activity_code==2)
					{
   					redirect("Production/typeA_operation");
   					}
   					if($activity_code==3)
					{
   					redirect("Production/typeB_operation");
   					}
   					
   					//End Bhupendra code of 23aug2019

				}	
				else
				{				
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('process/typeA_batch', ['data'=>$data]);
			}
			
			}
		}
		else
		{
			//$this->load->view('production/room', ['room'=>"aasa"]);
		}			
	}

	public function accessoryCleaning(){

		$this->load->model('Usermodel');
		$doc_id = $this->Usermodel->GetDocumentId("trn_accessorycleaning","document_no","mst_document","doc_id","doc_id","WAC00000000");
		$data["docid"] = $doc_id;
		$this->load->model('Productionmodel');
		$data["accessory"] = $this->Productionmodel->getAccessory();	
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
     if(count($data["accessory"])> 0){
			$this->load->view('process/accessoryCleaning',$data);
			
	}
	else
		{
		$this->load->model('Sanitizationmodel');
		$errheader = "ERR_accessoryCleaning";
		$msg = $this->Sanitizationmodel->errMsg($errheader);
        $data["header"] = $msg["msg-header"];
        $data["message"] = $msg["message"];
        //"Issue occured in following Tables . Please contact administrator.<br> mst_drainpoint";
        $this->load->view("error500",$data);
		}
	}

	public function productName(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Productionmodel');
    $response = $this->Productionmodel->getProductname($this->input->post("str"));
    echo json_encode($response);
  }

  public function accessoryCleaningview(){
  	$this->load->model('Productionmodel');
      $count = $this->Productionmodel->checkedBycount("trn_accessorycleaning","checked_by");
      if($count>0){
        $data["list"] = $this->Productionmodel->getaccessoryCleaningview();
        // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
        $this->load->view("process/accessoryCleaningview",$data);
      }
      else{
        redirect("Production/accessoryCleaning");
      }
  }

  public function accessoryCleaningviewnow($id){
    $this->load->model('Productionmodel');
    $row = $this->Productionmodel->getaccessoryCleaningviewnow($id);
    $data["row"] = $this->Productionmodel->getaccessoryCleaningviewnow($id);
    $data["logs"] = $this->Productionmodel->getaccessoryCleaninglogs($row["document_no"]);
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
    $this->load->view("process/accessoryCleaningviewnow",$data);
  }

  public function accessoryCleaningtakeover(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Productionmodel');
    $response = $this->Productionmodel->accessoryCleaningtakeover($_POST);
    echo json_encode($response);
  }

  public function accessoryCleaningfinish(){
  	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Productionmodel');
    $response = $this->Productionmodel->accessoryCleaningfinish($_POST);
    echo json_encode($response);
  }

  public function cleaningDetails(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Productionmodel');
    $response = $this->Productionmodel->insertAccessorycleaning($_POST);
    echo json_encode($response);
  }

	public function batchcode()
	{
	    header('Access-Control-Allow-Origin: *');  
	    header("Content-Type: application/json", true);
	    $bcode = $_POST["bcode"];    
	    $this->load->model('Productionmodel');
		$dev = $this->Productionmodel->getlike_dtl2('batch_code',$bcode,'trn_batch');
	    foreach ($dev->result_array() as $sn) {
      		$response[] = $sn["batch_code"];
    	}
    	echo json_encode($response);
  	}

  	public function areacode()
	{
	    header('Access-Control-Allow-Origin: *');  
	    header("Content-Type: application/json", true);
	    $arcode = $_POST["arcode"];    
	    $this->load->model('Productionmodel');
		$dev = $this->Productionmodel->get2like_dtl('area_code','area_name',$arcode,'mst_area');
	    foreach ($dev->result_array() as $sn) {
      		$response[] = $sn["area_code"]."/".$sn["area_name"];
    	}
    	echo json_encode($response);
  	}

  	public function roomcode()
	{
	    header('Access-Control-Allow-Origin: *');  
	    header("Content-Type: application/json", true);
	    $rcode = $_POST["rcode"];    
	    $this->load->model('Productionmodel');
		$dev = $this->Productionmodel->get4like_dtl4('room_code','room_name',$rcode,'area_code',$this->session->userdata('area_code'),'mst_room');
	    foreach ($dev->result_array() as $sn) {
      		$response[] = $sn["room_code"]."/".$sn["room_name"];
    	}
    	echo json_encode($response);
  	}

	public function batchdata()
	{
	    header('Access-Control-Allow-Origin: *');  
	    header("Content-Type: application/json", true);
	    $bcode = $_POST["bcode"];
	    $this->session->set_userdata('batch_code', $bcode);
	    $this->load->model('Productionmodel');
		$dev = $this->Productionmodel->get_dtl2('batch_code',$bcode,'trn_batch');
	    $response = $dev->row_array();
	    echo json_encode($response);
  	}

  	public function areadata()
	{
	    header('Access-Control-Allow-Origin: *');  
	    header("Content-Type: application/json", true);
	    $arcode = $_POST["arcode"];
	    $this->load->model('Productionmodel');
		$dev = $this->Productionmodel->get_dtl2('area_code',$arcode,'mst_area');
	    $response = $dev->row_array();
	    echo json_encode($response);
  	}

  	public function roomdata()
	{
	    header('Access-Control-Allow-Origin: *');  
	    header("Content-Type: application/json", true);
	    $rcode = $_POST["rcode"];
	    $this->load->model('Productionmodel');
		$dev = $this->Productionmodel->get_dtl2('room_code',$rcode,'mst_room');
	    $response = $dev->row_array();
	    echo json_encode($response);
  	}

  	public function Getproduct_Bycode()
	{
	    header('Access-Control-Allow-Origin: *');  
	    header("Content-Type: application/json", true);
	    $productcode = $_POST["productcode"];
	    $this->session->set_userdata('productcode', $productcode);
	    $this->load->model('Productionmodel');
		$dev = $this->Productionmodel->get_dtl2('product_code',$productcode,'mst_product');
	    $response = $dev->row_array();
	    echo json_encode($response);
  	}

  	public function AddBatch()
	{
	    header('Access-Control-Allow-Origin: *');  
	    header("Content-Type: application/json", true);

	    $res = $this->Adminmodel->insert17para_with_tblnama('trn_batch','batch_code',$_POST['hddbatchcode2'],'batch_description',$_POST['BatchDesc'],'product_code',$_POST['hddpro_code2'],'product_description',$_POST['hddpro_desc2'],'UOM',$_POST['UOMinput'],'batch_size',$_POST['Batchsize'],'market',$_POST['hddmarket2'],'created_by',$this->session->userdata('empcode'));
   		if($res>0)
		{
			$response = array("status"=>1);
    	} 
    	else 
    	{
	    	$response = array("status"=>0);
	  	}
	    echo json_encode($response);
  	}
  	public function chkcleaning()
  	{
  		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);

  		date_default_timezone_set('Asia/Calcutta');
		$endtime="";
		$diffHours="0";
		$val="1";
		$this->db->select("hdr.id,hdr.room_code,hdr.operation_type,opdl.header_id,opdl.start_time,opdl.end_time,war.header_id,war.name,war.status,war.type");
		$orcon = "(hdr.operation_type='type A' or hdr.operation_type='type B')";
		$this->db->from("trn_operationlogheader as hdr");
		$this->db->join("trn_operationlogdetail as opdl","hdr.id=opdl.header_id","INNER");
		$this->db->join("trn_operation_workflow_approval_records as war","hdr.id=war.header_id","INNER");
		$this->db->where($orcon);
		$this->db->where("hdr.room_code",$this->session->userdata('room_code'));
		$this->db->where("hdr.next_step",'0');		
		$this->db->where("war.type",'Approved');
		$this->db->group_by("opdl.end_time");
		$this->db->order_by("opdl.end_time","DESC");

		$query = $this->db->get();
		$response2 = $query->result();

		//print_r($this->db->last_query()); 
		//exit;
		if($query->num_rows() > 0)
		{
		foreach($response2 as $row) 
		{
			if($val=="1")
			{
			$endtime=$row->end_time;
			}
			$val="2";
		}		
     	$day1 = strtotime($endtime);
     	$day2 = date('Y-m-d H:i:s');
     	$day2 = strtotime($day2);
   		$diffHours = round(($day2 - $day1) / 3600);
   		}
   		else
   		{
   			$diffHours=-1;
   		}
   		echo json_encode($diffHours);
  	}
  	public function chkcleaning2()
  	{
  		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
    	$bcode = $_POST['bcode'];
  		date_default_timezone_set('Asia/Calcutta');
		$endtime="";
		$diffHours="0";
		$val="1";
		$this->db->select("hdr.id,hdr.room_code,hdr.batch_no,hdr.operation_type,opdl.header_id,opdl.start_time,opdl.end_time,war.header_id,war.name,war.status,war.type");
		$orcon = "(hdr.operation_type='type A' or hdr.operation_type='type B')";
		$this->db->from("trn_operationlogheader as hdr");
		$this->db->join("trn_operationlogdetail as opdl","hdr.id=opdl.header_id","INNER");
		$this->db->join("trn_operation_workflow_approval_records as war","hdr.id=war.header_id","INNER");
		$this->db->where($orcon);
		$this->db->where("hdr.room_code",$this->session->userdata('room_code'));
		$this->db->where("hdr.next_step",'0');		
		$this->db->where("war.type",'Approved');
		$this->db->group_by("opdl.end_time");
		$this->db->order_by("opdl.end_time","DESC");
		$query = $this->db->get();
		$response2 = $query->result();
		if($query->num_rows() > 0)
		{
		foreach($response2 as $row) 
		{
			if($val=="1")
			{
			$endtime=$row->end_time;
			$batchno=$row->batch_no;
			}
			$val="2";
		}		
				if($batchno==$bcode)
				{
     			$day1 = strtotime($endtime);
     			$day2 = date('Y-m-d H:i:s');
     			$day2 = strtotime($day2);
   				$diffHours = round(($day2 - $day1) / 3600);
   				}
   				else
   				{
   					$diffHours="-1";
   				}
   		}
   		else
   		{
   			$diffHours="-1";
   		}
   		echo json_encode($diffHours);
  	}
	public function operation()
	{
		$this->session->set_userdata('activityType','operation');
		$headerlogdtl ="";
		$emp_code = $this->session->userdata('empcode');
		$room_code = $this->session->userdata('room_code');
		$area_code = $this->session->userdata('area_code');
		if(isset($_POST['hddpro_code'])){$pcode = $_POST["hddpro_code"];$this->session->set_userdata('pcode', $pcode);}else{$pcode = $this->session->userdata('pcode');}
   		if(isset($_POST['hddpro_desc'])){$pdesc = $_POST["hddpro_desc"];$this->session->set_userdata('pdesc', $pdesc);}else{$pdesc = $this->session->userdata('pdesc');}
   		if(isset($_POST['hddmarket'])){$market = $_POST["hddmarket"];$this->session->set_userdata('market', $market);}else{$market = $this->session->userdata('market');}
   		if(isset($_POST['hddbatchcode'])){$bcode = $_POST["hddbatchcode"];$this->session->set_userdata('bcode', $bcode);}else{$bcode = $this->session->userdata('bcode');}
   		if(isset($_POST['hddbatchsizeUOM'])){$bsizeUOM = $_POST["hddbatchsizeUOM"];$this->session->set_userdata('bsizeUOM', $bsizeUOM);}else{$bsizeUOM = $this->session->userdata('bsizeUOM');}
   		if(isset($_POST['hddUOM'])){$UOM = $_POST["hddUOM"];$this->session->set_userdata('UOM', $UOM);}else{$UOM = $this->session->userdata('UOM');}
		if(isset($_POST['lotno'])){$lotno = $_POST["lotno"];$this->session->set_userdata('lotno', $lotno);}else{$lotno = $this->session->userdata('lotno');}
   		//$lotno = $this->session->userdata('lotno');
		//$bcode = $this->session->userdata('batch_code');
		$activity_code = $this->session->userdata('activity_code');	

		$this->load->model('Productionmodel');
		$response = $this->Productionmodel->get_HeaderID12('area_code',$area_code,'room_code',$room_code,'batch_no',$bcode,'activity_code',$activity_code,'lot_no',$lotno,'operation_type','operation','trn_operationlogheader');
		$header_id=$response['id'];
		$docid=$response['docid'];
		
   		if($header_id != "" && $header_id !=null)
		{

			$this->load->model('Usermodel');
			$docid = $docid;
			//$this->Usermodel->sec_fun_GetDocumentId('trn_operationlogheader','document_no','mst_document','doc_id','remarks','Operation');
			$this->load->model('Productionmodel');
			$fixequipment = $this->Productionmodel->getpendingtaskEquipmentsdtl($header_id);

			$brkdwnlog = array();
		foreach($fixequipment->result() as $row) {

		$brkdwnlog[] = $this->Productionmodel->getresultset_of_eachEuipment_of_Pendingtask($header_id,$row->equipment_code);
			
			}




			$q = $this->Productionmodel->get_dtl2('area_code',$area_code,'mst_department');
			$res = $q->row_array();



			$departmentname =  $res['department_name'];
			$this->load->model('Productionmodel');
			$q2 = $this->Productionmodel->get_dtl2('header_id',$header_id,'trn_operationlogdetail');
			$headerlogdtl = $q2;
			$data = array(
			'header_id' => $header_id,
			'departmentname' => $departmentname,
			'docid' => $docid,
			'pcode' => $pcode,
			'pdesc' => $pdesc,
			'market' => $market,
			'bcode' => $bcode,
			'bsizeUOM' => $bsizeUOM,
			'UOM' => $UOM,
			'lotno' => $lotno,
			'fixequipment' => $fixequipment,
			'headerlogdtl' => $headerlogdtl,
			'brkdwnlog' => $brkdwnlog		
			);	
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('process/pendingoperation', ['data'=>$data]);
		}
		else
		{
			$docid = $this->Usermodel->sec_fun_GetDocumentId('trn_operationlogheader','document_no','mst_document','doc_id','remarks','Operation');
			if($docid == "" || $docid == null )
			{ $errheader = "ERR_docid"; redirect('Production/Exception_error/'.$errheader); }



			$this->session->set_userdata('docid',$docid);
			$empblock_code = $this->session->userdata('empblock_code');

			$area = $this->Productionmodel->get_dtl2('block_code',$empblock_code,'mst_area');
			if($area == "" || $area == null )
			{ $errheader = "ERR_AreaNotfoundByBlockcode"; redirect('Production/Exception_error/'.$errheader); }

			$fixequipment = $this->Productionmodel->getEquipmentsdtl();
			$portequipment = $this->Productionmodel->getPortEquipmentsdtl();			

			if(($fixequipment->num_rows() ==0 || $fixequipment == "" || $fixequipment == null) && ($portequipment->num_rows() ==0 || $portequipment == "" || $portequipment == null))
			{ 
				$errheader="ERR_InEquipmentNotFoundByRoomcode";
				redirect('Production/Exception_error/'.$errheader);
			}


			$q = $this->Productionmodel->get_dtl2('area_code',$area_code,'mst_department');
			$res = $q->row_array();
			if($res == "" || $res == null )
			{ 
				$departmentname =  "";
			}
			else
			{
				$departmentname =  $res['department_name'];
			}

			$data = array(
			'header_id' => "",
			'departmentname' => $departmentname,
			'docid' => $docid,
			'pcode' => $pcode,
			'pdesc' => $pdesc,
			'market' => $market,
			'bcode' => $bcode,
			'bsizeUOM' => $bsizeUOM,
			'UOM' => $UOM,
			'lotno' => $lotno,
			'fixequipment' => $fixequipment,
			'portequipment' => $portequipment,
			'area' => $area->result_array()		
			);	
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('process/operation', ['data'=>$data]);
		}			
	}

	public function typeA_operation()
	{
		$this->session->set_userdata('activityType','type A');
		$headerlogdtl ="";
		$emp_code = $this->session->userdata('empcode');
		$room_code = $this->session->userdata('room_code');
		$area_code = $this->session->userdata('area_code');
		$activity_code = $this->session->userdata('activity_code');
		$lotno="";
		if(isset($_POST['hddpro_code'])){$pcode = $_POST["hddpro_code"];$this->session->set_userdata('pcode', $pcode);}else{$pcode = $this->session->userdata('pcode');}
   		if(isset($_POST['hddpro_desc'])){$pdesc = $_POST["hddpro_desc"];$this->session->set_userdata('pdesc', $pdesc);}else{$pdesc = $this->session->userdata('pdesc');}
   		if(isset($_POST['hddmarket'])){$market = $_POST["hddmarket"];$this->session->set_userdata('market', $market);}else{$market = $this->session->userdata('market');}
   		if(isset($_POST['hddbatchcode'])){$bcode = $_POST["hddbatchcode"];$this->session->set_userdata('bcode', $bcode);}else{$bcode = $this->session->userdata('bcode');}
   		if(isset($_POST['hddbatchsizeUOM'])){$bsizeUOM = $_POST["hddbatchsizeUOM"];$this->session->set_userdata('bsizeUOM', $bsizeUOM);}else{$bsizeUOM = $this->session->userdata('bsizeUOM');}
   		if(isset($_POST['hddUOM'])){$UOM = $_POST["hddUOM"];$this->session->set_userdata('UOM', $UOM);}else{$UOM = $this->session->userdata('UOM');}
		$this->load->model('Productionmodel');
		
		$this->load->model('Usermodel');

		// getting department name
		$q = $this->Productionmodel->get_dtl2('area_code',$area_code,'mst_department');
		$res = $q->row_array();
		if($res == "" || $res == null )
			{ 
				$departmentname =  "";
			}
			else
			{
				$departmentname =  $res['department_name'];
			}
		

		// getting header ID from header table

		$response = $this->Productionmodel->get_HeaderID12('area_code',$area_code,'room_code',$room_code,'batch_no',$bcode,'activity_code',$activity_code,'lot_no',$lotno,'operation_type',$this->session->userdata('activityType'),'trn_operationlogheader');
		$header_id=$response['id'];
		$docid=$response['docid'];
		
   		if($header_id != "" && $header_id !=null)
		{						
			// getting equipments
			$fixequipment = $this->Productionmodel->getpendingtaskEquipmentsdtl($header_id);			

			// getting header details
			$q2 = $this->Productionmodel->get_dtl2('header_id',$header_id,'trn_operationlogdetail');
			$headerlogdtl = $q2;

			$data = array(
			'header_id' => $header_id,
			'departmentname' => $departmentname,
			'docid' => $docid,
			'pcode' => $pcode,
			'pdesc' => $pdesc,
			'market' => $market,
			'bcode' => $bcode,
			'bsizeUOM' => $bsizeUOM,
			'UOM' => $UOM,
			'fixequipment' => $fixequipment,
			'headerlogdtl' => $headerlogdtl,	
			);
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			
			$this->load->view('process/typeA_pendingoperation', ['data'=>$data]);
		}
		else
		{	
			// getting doc ID from mst_document table
		$docid = $this->Usermodel->sec_fun_GetDocumentId('trn_operationlogheader','document_no','mst_document','doc_id','remarks','Type A');
		if($docid == "" || $docid == null )
			{ $errheader = "ERR_docid"; redirect('Production/Exception_error/'.$errheader); }
		$this->session->set_userdata('docid',$docid);
			
			// getting equipments
			$fixequipment = $this->Productionmodel->getEquipmentsdtl();
			$portequipment = $this->Productionmodel->getPortEquipmentsdtl();			

			if(($fixequipment->num_rows() ==0 || $fixequipment == "" || $fixequipment == null) && ($portequipment->num_rows() ==0 || $portequipment == "" || $portequipment == null))
			{ 
				$errheader="ERR_InEquipmentNotFoundByRoomcode";
				redirect('Production/Exception_error/'.$errheader);
			}

			$empblock_code = $this->session->userdata('empblock_code');
			$area = $this->Productionmodel->get_dtl2('block_code',$empblock_code,'mst_area');
			if($area == "" || $area == null )
			{ $errheader = "ERR_AreaNotfoundByBlockcode"; redirect('Production/Exception_error/'.$errheader); }

			$data = array(
			'departmentname' => $departmentname,
			'docid' => $docid,
			'pcode' => $pcode,
			'pdesc' => $pdesc,
			'market' => $market,
			'bcode' => $bcode,
			'bsizeUOM' => $bsizeUOM,
			'UOM' => $UOM,
			'fixequipment' => $fixequipment,
			'portequipment' => $portequipment,
			'area' => $area->result_array()	
			);
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('process/typeA_operation', ['data'=>$data]);
		}		
			
	}

	public function typeB_operation()
	{
		$this->session->set_userdata('activityType','type B');
		$headerlogdtl ="";
		$emp_code = $this->session->userdata('empcode');
		$room_code = $this->session->userdata('room_code');
		$area_code = $this->session->userdata('area_code');
		$activity_code = $this->session->userdata('activity_code');
		$lotno="";
		if(isset($_POST['hddpro_code'])){$pcode = $_POST["hddpro_code"];$this->session->set_userdata('pcode', $pcode);}else{$pcode = $this->session->userdata('pcode');}
   		if(isset($_POST['hddpro_desc'])){$pdesc = $_POST["hddpro_desc"];$this->session->set_userdata('pdesc', $pdesc);}else{$pdesc = $this->session->userdata('pdesc');}
   		if(isset($_POST['hddmarket'])){$market = $_POST["hddmarket"];$this->session->set_userdata('market', $market);}else{$market = $this->session->userdata('market');}
   		if(isset($_POST['hddbatchcode'])){$bcode = $_POST["hddbatchcode"];$this->session->set_userdata('bcode', $bcode);}else{$bcode = $this->session->userdata('bcode');}
   		if(isset($_POST['hddbatchsizeUOM'])){$bsizeUOM = $_POST["hddbatchsizeUOM"];$this->session->set_userdata('bsizeUOM', $bsizeUOM);}else{$bsizeUOM = $this->session->userdata('bsizeUOM');}
   		if(isset($_POST['hddUOM'])){$UOM = $_POST["hddUOM"];$this->session->set_userdata('UOM', $UOM);}else{$UOM = $this->session->userdata('UOM');}
		$this->load->model('Productionmodel');
		// getting doc ID from mst_document table
		$this->load->model('Usermodel');		

		// getting department name
		$q = $this->Productionmodel->get_dtl2('area_code',$area_code,'mst_department');
		$res = $q->row_array();
		if($res == "" || $res == null )
			{ 
				$departmentname =  "";
			}
			else
			{
				$departmentname =  $res['department_name'];
			}

		// getting header ID from header table

		$response = $this->Productionmodel->get_HeaderID12('area_code',$area_code,'room_code',$room_code,'batch_no',$bcode,'activity_code',$activity_code,'lot_no',$lotno,'operation_type',$this->session->userdata('activityType'),'trn_operationlogheader');
		$header_id=$response['id'];
		$docid=$response['docid'];
		
   		if($header_id != "" && $header_id !=null)
		{						
			// getting equipments
			$fixequipment = $this->Productionmodel->getpendingtaskEquipmentsdtl($header_id);			

			// getting header details
			$q2 = $this->Productionmodel->get_dtl2('header_id',$header_id,'trn_operationlogdetail');
			$headerlogdtl = $q2;

			$data = array(
			'header_id' => $header_id,
			'departmentname' => $departmentname,
			'docid' => $docid,
			'pcode' => $pcode,
			'pdesc' => $pdesc,
			'market' => $market,
			'bcode' => $bcode,
			'bsizeUOM' => $bsizeUOM,
			'UOM' => $UOM,
			'fixequipment' => $fixequipment,
			'headerlogdtl' => $headerlogdtl,	
			);
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('process/typeA_pendingoperation', ['data'=>$data]);
		}
		else
		{		
			// getting doc ID from mst_document table
		$docid = $this->Usermodel->sec_fun_GetDocumentId('trn_operationlogheader','document_no','mst_document','doc_id','remarks','Type B');
		if($docid == "" || $docid == null )
			{ $errheader = "ERR_docid"; redirect('Production/Exception_error/'.$errheader); }
		$this->session->set_userdata('docid',$docid);
		

			// getting equipments
			$fixequipment = $this->Productionmodel->getEquipmentsdtl();
			$portequipment = $this->Productionmodel->getPortEquipmentsdtl();			

			if(($fixequipment->num_rows() ==0 || $fixequipment == "" || $fixequipment == null) && ($portequipment->num_rows() ==0 || $portequipment == "" || $portequipment == null))
			{ 
				$errheader="ERR_InEquipmentNotFoundByRoomcode";
				redirect('Production/Exception_error/'.$errheader);
			}
			$empblock_code = $this->session->userdata('empblock_code');
			$area = $this->Productionmodel->get_dtl2('block_code',$empblock_code,'mst_area');
			if($area == "" || $area == null )
			{ $errheader = "ERR_AreaNotfoundByBlockcode"; redirect('Production/Exception_error/'.$errheader); }

			$data = array(
			'departmentname' => $departmentname,
			'docid' => $docid,
			'pcode' => $pcode,
			'pdesc' => $pdesc,
			'market' => $market,
			'bcode' => $bcode,
			'bsizeUOM' => $bsizeUOM,
			'UOM' => $UOM,
			'fixequipment' => $fixequipment,
			'portequipment' => $portequipment,
			'area' => $area->result_array()	
			);
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('process/typeA_operation', ['data'=>$data]);
		}		
			
	}

	public function makeinitiallog()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true); 
		$emp_code = $this->session->userdata('empcode');
		$roleid = $this->session->userdata('roleid');
		$block_code = $this->session->userdata('empblock_code');
		$area_code = $this->session->userdata('area_code');
		$room_code = $this->session->userdata('room_code');
		$activity_code = $this->session->userdata('activity_code');
		//$response = null ;

		if(isset($_POST['operation_type'])){$operation_type = $_POST["operation_type"];}else{$operation_type = "";}
		if(isset($_POST['docid'])){$docid = $_POST["docid"];}else{$docid = "";}
		if(isset($_POST['lotno'])){$lotno = $_POST["lotno"];}else{$lotno = "";}
		if(isset($_POST['bcode'])){$bcode = $_POST["bcode"];}else{$bcode = "";}
		if(isset($_POST['myArray'])){$myArray = $_POST["myArray"];}else{$myArray = "";}

		date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
		$now = date('Y-m-d H:i:s');
		$now2 = date('Y-m-d H:i:s');

		$this->load->model('Productionmodel');
		$q = $this->Productionmodel->getnext_step(1,$this->session->userdata("roleid"),$activity_code);
		$workflowdata = $q->row_array();
		$next_step = $workflowdata['next_step'];

		$this->load->model('Logmaintainmodel');
		$id = $this->Logmaintainmodel->insert21para_with_tblnama('trn_operationlogheader','document_no',$docid,'lot_no',$lotno,'area_code',$area_code,'room_code',$room_code,'batch_no',$bcode,'activity_code',$activity_code,'status','1','next_step',$next_step,'created_by',$emp_code,'operation_type',$operation_type);
		$this->session->set_userdata('header_id',$id);

		$res = $this->Logmaintainmodel->insert15para_with_tblnama('trn_operationlogdetail','header_id',$id,'start_time',$now,'end_time','','checked_by_time','','performed_by',$this->session->userdata('empname'),'checked_by','','created_by',$emp_code);

		foreach ($myArray as $array){
			$res2 = $this->Logmaintainmodel->insert9para_with_tblnama('trn_operationequipmentheaderlog','header_id',$id,'equipment_code',$array['eqcode'],'sop_code',$array['sopcode'],'created_by',$emp_code);

			$eq = array("is_inused"=>1);		
    		$this->db->where("equipment_code",$array['eqcode']);
    		$this->db->update("mst_equipment",$eq);

		}
		if($id>0){

			if($operation_type=="operation") 
			{
				$q2 = $this->Productionmodel->get_dtl2('id',1,'mst_messages');
				$room_array= array("in_use"=>'1',"inuse_activity"=>"Operation is in progress");
							
			} 
			else if($operation_type=="type A") 
			{
				$q2 = $this->Productionmodel->get_dtl2('id',5,'mst_messages');
				$room_array= array("in_use"=>'1',"inuse_activity"=>"Type A is in progress");
			}
			else if($operation_type=="type B") 
			{
				$q2 = $this->Productionmodel->get_dtl2('id',44,'mst_messages');
				$room_array= array("in_use"=>'1',"inuse_activity"=>"Type B is in progress");
			}
			$this->db->where("room_code", $this->session->userdata("room_code"));
			$this->db->update("mst_room", $room_array);
			$msgdtl = $q2->row_array();
		 	$response = array("status"=>1,"msg"=>$msgdtl["message"],"msgheader"=>$msgdtl["msg-header"]);
		 	echo json_encode($response);
     	} 
     	else { $response = array("status"=>0);
     		echo json_encode($response);
     	} 
     	//$response = "array";
     					
	}

	public function makestartbrkdwnlog()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);

    	$this->load->model('Productionmodel');
		$emp_code = $this->session->userdata('empcode');
		if(isset($_POST['operation_type'])){$operation_type = $_POST["operation_type"];}else{$operation_type = "";}
		if(isset($_POST['lotno'])){$lotno = $_POST["lotno"];}else{$lotno = "";}
		if(isset($_POST['bcode'])){$bcode = $_POST["bcode"];}else{$bcode = "";}
		$area_code = $this->session->userdata('area_code');
		$room_code = $this->session->userdata('room_code');
		$activity_code = $this->session->userdata('activity_code');
		$eqcode = $_POST['eqcode'];
		$eqtype = $_POST['eqtype'];
		//$sopcode = $_POST['sopcode'];
		$equiptblid = $_POST['equiptblid'];

		if($this->session->userdata('header_id') == "" || $this->session->userdata('header_id') == null)
		{
			$query = $this->Productionmodel->get_Header12('area_code',$area_code,'room_code',$room_code,'batch_no',$bcode,'activity_code',$activity_code,'lot_no',$lotno,'operation_type',$operation_type,'trn_operationlogheader');
			$response = $query->row_array();
			$header_id = $response['id'];
		}
		else
		{
			$header_id = $this->session->userdata('header_id');
		}

		$query = $this->Productionmodel->getLastHeader_dtl3('header_id',$header_id,'trn_operationlogdetail');
		$response = $query->row_array();
		$Pre_emp_code = $response['created_by'];
		if($Pre_emp_code == $emp_code)
		{
			
			$this->load->model('Logmaintainmodel');
			date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
			$now2 = date('Y-m-d H:i:s');

			$insertedid = $this->Logmaintainmodel->insert25para_with_tblnama('trn_operationequipmentdetaillog','equipmentheader_table_id',$equiptblid,'header_id',$header_id,'equipment_code',$eqcode,'equipment_type',$eqtype,'strattime',$now2,'endtime','','memo_number','','remark','','performed_by',$this->session->userdata('empname'),'checked_by','','created_by',$emp_code,'action_type','breakdown');
			echo json_encode($insertedid);
		}
		else
		{
			echo json_encode("-1");
		}
	}

	public function makeendbrkdwnlog()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true); 
		$emp_code = $this->session->userdata('empcode');
		$sopcode = $_POST['sopcode'];
		$eqcode = $_POST['eqcode'];
		$eqtype = $_POST['eqtype'];
		$equiptblid = $_POST['equiptblid'];
		$memonumber = $_POST['memonumber'];
		$memoremark = $_POST['memoremark'];
		if(isset($_POST['operation_type'])){$operation_type = $_POST["operation_type"];}else{$operation_type = "";}
		if(isset($_POST['lotno'])){$lotno = $_POST["lotno"];}else{$lotno = "";}
		if(isset($_POST['bcode'])){$bcode = $_POST["bcode"];}else{$bcode = "";}

		$this->load->model('Productionmodel');
		$area_code = $this->session->userdata('area_code');
		$room_code = $this->session->userdata('room_code');
		$activity_code = $this->session->userdata('activity_code');

		if($this->session->userdata('header_id') == "" || $this->session->userdata('header_id') == null)
		{
			

			$query = $this->Productionmodel->get_Header12('area_code',$area_code,'room_code',$room_code,'batch_no',$bcode,'activity_code',$activity_code,'lot_no',$lotno,'operation_type',$operation_type,'trn_operationlogheader');
			$response = $query->row_array();
			$header_id = $response['id'];
		}
		else
		{
			$header_id = $this->session->userdata('header_id');
		}

		$query = $this->Productionmodel->getLastHeader_dtl3('header_id',$header_id,'trn_operationlogdetail');
		$response = $query->row_array();
		$Pre_emp_code = $response['created_by'];

		if($Pre_emp_code == $emp_code) // checking previouse operator with current operator
		{
			
			$this->load->model('Productionmodel');
			date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
			$now2 = date('Y-m-d H:i:s');

			$res2 = $this->Productionmodel->brkdownfinish($header_id,$eqcode,$now2,$emp_code,$memonumber,$memoremark,$equiptblid);
			echo json_encode($res2);
		}
		else
		{
			echo json_encode("-1");
		}	
	}

	public function makechkbylog()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);    	
		$emp_code = $this->session->userdata('empcode');
		if(isset($_POST['operation_type'])){$operation_type = $_POST["operation_type"];}else{$operation_type = "";}
		if(isset($_POST['lotno'])){$lotno = $_POST["lotno"];}else{$lotno = "";}
		if(isset($_POST['bcode'])){$bcode = $_POST["bcode"];}else{$bcode = "";}
		$area_code = $this->session->userdata('area_code');
		$room_code = $this->session->userdata('room_code');
		$activity_code = $this->session->userdata('activity_code');		

		$this->load->model('Productionmodel');
		$query = $this->Productionmodel->get_Header12('area_code',$area_code,'room_code',$room_code,'batch_no',$bcode,'activity_code',$activity_code,'lot_no',$lotno,'operation_type',$operation_type,'trn_operationlogheader');
		$response = $query->row_array();
		$header_id = $response['id'];
			
		if($header_id != "" && $header_id !=null)
		{
			$this->load->model('Productionmodel');
			$query = $this->Productionmodel->getLastHeader_dtl3('header_id',$header_id,'trn_operationlogdetail');
			$response = $query->row_array();
			$Pre_emp_code = $response['created_by'];
			$headerdtl_Last_id = $response['id'];
			if($Pre_emp_code != $emp_code)
			{
				date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
				$now2 = date('Y-m-d H:i:s');
				$this->load->model('Productionmodel');
				$response = $this->Productionmodel->operationcheckby($header_id,$headerdtl_Last_id,$now2,$emp_code);

				if($operation_type=="operation") {
					$q2 = $this->Productionmodel->get_dtl2('id',3,'mst_messages');
					} else if($operation_type=="type A") {
						$q2 = $this->Productionmodel->get_dtl2('id',8,'mst_messages');
					}else if($operation_type=="type B") {
						$q2 = $this->Productionmodel->get_dtl2('id',6,'mst_messages');
					}
				$msgdtl = $q2->row_array();
		 		$responsed = array("status"=>1,"msg"=>$msgdtl["message"],"msgheader"=>$msgdtl["msg-header"],"query"=>$response);

				echo json_encode($responsed);
			}
			else
			{
				echo json_encode(0);
			}
		}
	}

	public function makefinishlog()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
		$empname = $this->session->userdata('empname');
		$emp_code = $this->session->userdata('empcode');
		$roleid = $this->session->userdata('roleid');
		if(isset($_POST['operation_type'])){$operation_type = $_POST["operation_type"];}else{$operation_type = "";}
		if(isset($_POST['lotno'])){$lotno = $_POST["lotno"];}else{$lotno = "";}
		if(isset($_POST['bcode'])){$bcode = $_POST["bcode"];}else{$bcode = "";}
		$area_code = $this->session->userdata('area_code');
		$room_code = $this->session->userdata('room_code');
		$activity_code = $this->session->userdata('activity_code');		

		$this->load->model('Productionmodel');
		$query = $this->Productionmodel->get_Header12('area_code',$area_code,'room_code',$room_code,'batch_no',$bcode,'activity_code',$activity_code,'lot_no',$lotno,'operation_type',$operation_type,'trn_operationlogheader');
		$response = $query->row_array();
		$header_id = $response['id'];

		$q = $this->Productionmodel->getnext_step(2,$this->session->userdata("roleid"),$this->session->userdata('activity_code'));
		$workflowdata = $q->row_array();
		$next_step = $workflowdata['next_step'];
			
		if($header_id != "" && $header_id !=null)
		{
			$this->load->model('Productionmodel');
			$query = $this->Productionmodel->getLastHeader_dtl3('header_id',$header_id,'trn_operationlogdetail');
			$response = $query->row_array();
			$Pre_emp_code = $response['created_by'];
			$headerdtl_Last_id = $response['id'];
			if($Pre_emp_code == $emp_code)
			{

			date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
			$endtime = date('Y-m-d H:i:s');
			$this->load->model('Productionmodel');
			$query = $this->Productionmodel->operationfinish($header_id,$headerdtl_Last_id,$endtime,$empname,$next_step);

			$this->db->where("header_id", $header_id);
			$this->db->where('is_active',1);
			$eqrow = $this->db->get("trn_operationequipmentheaderlog");
			if($eqrow->num_rows() > 0)
			{
				foreach($eqrow->result() as $row)
				{
				$eq = array("is_inused"=>0);		
    			$this->db->where("equipment_code",$row->equipment_code);
    			$this->db->update("mst_equipment",$eq);
    			}
			}			


				if($operation_type=="operation") {
					$q2 = $this->Productionmodel->get_dtl2('id',4,'mst_messages');					
				} elseif ($operation_type=="type A") {
					$q2 = $this->Productionmodel->get_dtl2('id',9,'mst_messages');
				} elseif ($operation_type=="type B") {
					$q2 = $this->Productionmodel->get_dtl2('id',7,'mst_messages');
				}
				$room_array= array("in_use"=>'0',"inuse_activity"=>null);
				$this->db->where("room_code", $this->session->userdata("room_code"));
				$this->db->update("mst_room", $room_array);
				$msgdtl = $q2->row_array();
		 		$response = array("status"=>1,"msg"=>$msgdtl["message"],"msgheader"=>$msgdtl["msg-header"],"query"=>$query);
			echo json_encode($response);
			}
			else
			{
				echo json_encode(0);
			}
		}
	}

	public function dd(){
		$this->load->model('Productionmodel');
		$area_code = $this->session->userdata("area_code");
		$room_code = $this->session->userdata("room_code");
		
		$this->load->model('Productionmodel');
		$fixequipment = $this->Productionmodel->get_dtl2('room_code',$room_code,'mst_equipment');
		$this->load->model('Productionmodel');
		$q = $this->Productionmodel->get_dtl2('area_code',$area_code,'mst_department');
		$res = $q->row_array();
		$departmentname =  $res['department_name'];

			$data = array(
			'departmentname' => $departmentname,
			'docid' => $docid,
			'pcode' => $pcode,
			'pdesc' => $pdesc,
			'market' => $market,
			'bcode' => $bcode,
			'bsizeUOM' => $bsizeUOM,
			'UOM' => $UOM,
			'lotno' => $lotno,
			'fixequipment' => $fixequipment,
			//'portequipment' => $portequipment								
			);
			
			print_r($data);
	}


  	public function batchcodeDetails(){
  		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
    	$this->load->model('Productionmodel');
    	$response = $this->Productionmodel->getbatchcodeDetails($_POST);
  		echo json_encode($response);
  	}


  	public function inprocessContainercleaning(){
  	$this->load->model('Usermodel');
	$doc_id = $this->Usermodel->GetDocumentId("trn_inprocesscontainercleaning","document_no","mst_document","doc_id","doc_id","WPC00000000");
	$data["docid"] = $doc_id;
	$this->load->model('Productionmodel');
    $data["items"] = $this->Productionmodel->getinprocessEquipment();
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code	
	if(count($data["items"]) > 0)
		{
    
    $this->load->view("process/inprocessContainercleaning",$data);			
  	}
	else
		{
		$this->load->model('Sanitizationmodel');
		$errheader = "ERR_inprocessContainercleaning";
		$msg = $this->Sanitizationmodel->errMsg($errheader);
        $data["header"] = $msg["msg-header"];
        $data["message"] = $msg["message"];
        //"Issue occured in following Tables . Please contact administrator.<br> mst_drainpoint";
        $this->load->view("error500",$data);
		}
	}

  	public function inprocessContainercleaningview(){
  	$this->load->model('Productionmodel');
      $count = $this->Productionmodel->checkedBycount("trn_inprocesscontainercleaning","checked_by");
      if($count>0){
	    $data["list"] = $this->Productionmodel->getinprocessContainercleaningview();
	    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
	    $this->load->view("process/inprocessContainercleaningview",$data);
      }
      else{
        redirect("Production/inprocessContainercleaning");
      }		
  	}		

  	public function inprocesscleaningDetails(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Productionmodel');
    $response = $this->Productionmodel->inprocesscleaningDetails($_POST);
    echo json_encode($response);		
  	}

  	public function sopList(){
  	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Productionmodel');
    $response = $this->Productionmodel->getsopList($this->input->post("str"));
    echo json_encode($response);	
  	}

  	public function inprocessContainercleaningviewnow($id){
  	$this->load->model('Productionmodel');
    $row = $this->Productionmodel->getinprocessContainercleaningviewnow($id);
    $data["row"] = $this->Productionmodel->getinprocessContainercleaningviewnow($id);
    $data["logs"] = $this->Productionmodel->getinprocessContainercleaninglogs($row["document_no"]);
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
    $this->load->view("process/inprocessContainercleaningviewnow",$data);	
  	}

  	public function inprocesscontainerCleaningfinish(){
  	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Productionmodel');
    $response = $this->Productionmodel->inprocesscontainerCleaningfinish($_POST);
    echo json_encode($response);
  	}

  	public function inprocesscontainerCleaningtakeover(){
	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Productionmodel');
    $response = $this->Productionmodel->inprocesscontainerCleaningtakeover($_POST);
    echo json_encode($response);	
	}



	public function portable()
	{
		$this->session->set_userdata('activityType','portable');
		$headerlogdtl ="";
		$emp_code = $this->session->userdata('empcode');
		$room_code = $this->session->userdata('room_code');
		$area_code = $this->session->userdata('area_code');
		if(isset($_POST['hddpro_code'])){$pcode = $_POST["hddpro_code"];$this->session->set_userdata('pcode', $pcode);}else{$pcode = $this->session->userdata('pcode');}
   		if(isset($_POST['hddpro_desc'])){$pdesc = $_POST["hddpro_desc"];$this->session->set_userdata('pdesc', $pdesc);}else{$pdesc = $this->session->userdata('pdesc');}
   		if(isset($_POST['hddmarket'])){$market = $_POST["hddmarket"];$this->session->set_userdata('market', $market);}else{$market = $this->session->userdata('market');}
   		if(isset($_POST['hddbatchcode'])){$bcode = $_POST["hddbatchcode"];$this->session->set_userdata('bcode', $bcode);}else{$bcode = $this->session->userdata('bcode');}
   		if(isset($_POST['hddbatchsizeUOM'])){$bsizeUOM = $_POST["hddbatchsizeUOM"];$this->session->set_userdata('bsizeUOM', $bsizeUOM);}else{$bsizeUOM = $this->session->userdata('bsizeUOM');}
   		if(isset($_POST['hddUOM'])){$UOM = $_POST["hddUOM"];$this->session->set_userdata('UOM', $UOM);}else{$UOM = $this->session->userdata('UOM');}
		if(isset($_POST['lotno'])){$lotno = $_POST["lotno"];$this->session->set_userdata('lotno', $lotno);}else{$lotno = $this->session->userdata('lotno');}
   		//$lotno = $this->session->userdata('lotno');
		//$bcode = $this->session->userdata('batch_code');
		$activity_code = $this->session->userdata('activity_code');	


			$empblock_code = $this->session->userdata('empblock_code');		
			$this->load->model('Usermodel');
			$docid = $this->Usermodel->GetDocumentId('trn_portabledetail','document_no','mst_document','doc_id','doc_id','WPE00000000');

			$this->session->set_userdata('docid',$docid);
			$this->load->model('Productionmodel');
			$fixequipment = $this->Productionmodel->getEquipmentsdtnoroom();
			$portequipment = $this->Productionmodel->getPortEquipmentsdtl();
			$area = $this->Productionmodel->get_dtl2('block_code',$empblock_code,'mst_area');
			$q = $this->Productionmodel->get_dtl2('area_code',$area_code,'mst_department');
			$res = $q->row_array();
			$departmentname =  $res['department_name'];
			$data = array(
			'header_id' => "",
			'departmentname' => $this->session->userdata('empdepartment_name'),
			'docid' => $docid,
			'pcode' => $pcode,
			'pdesc' => $pdesc,
			'market' => $market,
			'bcode' => $bcode,
			'bsizeUOM' => $bsizeUOM,
			'UOM' => $UOM,
			'portequipment' => $portequipment,
			//'fixequipment' => $fixequipment,
			//'headerlogdtl' => $headerlogdtl
			'area' => $area->result_array()		
			);

			
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code	
			$this->load->view('process/portableEquipmentcleaning', ['data'=>$data]);
				
			
	}


	public function portableEquipmentcleaningview(){
		$this->load->model('Productionmodel');
	    $data["list"] = $this->Productionmodel->getportableEquipmentcleaningview();
	    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
	    $this->load->view("process/portableEquipmentcleaningview",$data);  
	}


	public function portableEquipmentcleaningsubmit(){
		header('Access-Control-Allow-Origin: *');  
		header("Content-Type: application/json", true);
	    $this->load->model('Productionmodel');
	    $response = $this->Productionmodel->portableEquipmentcleaningsubmit($_POST);
	    echo json_encode($response);
	}

	public function getRoomnow(){
		header('Access-Control-Allow-Origin: *');  
		header("Content-Type: application/json", true);
		$this->load->model('Productionmodel');
		$area_code = $_POST["area"];
		$room = $this->Productionmodel->get_dtl2('area_code',$area_code,'mst_room');
		$response = $room->result_array();
		echo json_encode($response);
	}

	public function getEqm(){
		header('Access-Control-Allow-Origin: *');  
		header("Content-Type: application/json", true);
		$this->load->model('Productionmodel');
		$response = $this->Productionmodel->getEqm($_POST["room"]);
echo json_encode($response);
 }

 	public function portableEquipmentcleaning(){
 		$this->load->model('Productionmodel');
 		$activity_code = $this->session->userdata("activity_code");
		$batchcode = $this->Productionmodel->get_dtl1('trn_batch');
		$product = $this->Productionmodel->get_dtl1('mst_product');

		$data = array('activity_code' => $activity_code,'batchcode' => $batchcode, 'product' => $product);
		// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code		
		$this->load->view('process/batchPortable', ['data'=>$data]);
 	}

 	public function portableEquipmentcleaningviewnow($id){
 		$this->load->model('Productionmodel');
    $row = $this->Productionmodel->getportableEquipmentcleaningviewnow($id);
    $data["row"] = $this->Productionmodel->getportableEquipmentcleaningviewnow($id);

    $data["edata"] = $this->Productionmodel->getEdata($row["response1"]["batch_code"]);
//$dd = $this->Productionmodel->getEdata($row["response1"]["batch_code"]);

//print_r($dd);
//exit;

    $data["logs"] = $this->Productionmodel->getportableEquipmentcleaninglogs($row["response1"]["document_no"]);
    
    // header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code	
	$this->load->view("process/portableEquipmentcleaningviewnow",$data);
 	}	

 	public function portableEquipmentcleaningfinish(){
 		header('Access-Control-Allow-Origin: *');  
	    header("Content-Type: application/json", true);
	    $this->load->model('Productionmodel');
	    $response = $this->Productionmodel->portableEquipmentcleaningfinish($_POST);
	    echo json_encode($response);
 	}

 	public function portableEquipmentcleaningtakeover(){
 		header('Access-Control-Allow-Origin: *');  
	    header("Content-Type: application/json", true);
	    $this->load->model('Productionmodel');
	    $response = $this->Productionmodel->portableEquipmentcleaningtakeover($_POST);
echo json_encode($response);
 }
}
