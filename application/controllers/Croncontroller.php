<?php

/***
 * @author Nitin Mittal
 * @Desc Manages Cron Jobs For System
 */
date_default_timezone_set('Asia/Kolkata');
class Croncontroller extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function getUsersPasswordAboutToExpire(){
        $this->load->model('Ptscommon');
        $users_ids = $this->Ptscommon->getUsersPasswordAboutToExpire();
        if(!empty($users_ids)){
            $this->Ptscommon->setFirstTimeLoginTrue($users_ids);
            echo 'Cron Successfully Run';exit;
        }
    }
    
    public function getBlockedUsers(){
        $this->load->model('Ptscommon');
        $users_ids = $this->Ptscommon->getBlockedUsers();
        if(!empty($users_ids)){
            $this->Ptscommon->unblockUsers($users_ids);
            echo 'Cron Successfully Run';exit;
        }
    }
    
    public function getDatabaseBackup(){
        $this->load->view('database_backup');
    }
    public function getDatabaseRestore(){
        $this->load->view('restore_database');
    }

    public function updateDBDocNo(){
        $this->load->model('Ptscommon');
        $response ='';
        while($response!='All Done'){
            $response = $this->Ptscommon->updateDBDocNo();
        }
        //echo $users_ids; exit;
    }

    public function a(){
        $this->load->model('Ptscommon');
        $this->Ptscommon->testcommit();
    }
}
