<?php

class Secondblock extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('empemail'))
		{
			return redirect("login");
		}
		if(!isset($_SERVER['HTTP_REFERER'])) {         
		  return redirect("login?err=1");
		  //$this->load->view('login');
        } 
	}

	public function DP_monitoring()
	{
		$empblock_code = $this->session->userdata('empblock_code');
		$this->load->model('Productionmodel');
		$equip = $this->Productionmodel->get_dtl2('room_code',$this->session->userdata('emparea_code'),'mst_equipment');
		$this->load->model('Secondblockmodel');
		$dpdtl = $this->Secondblockmodel->getDPdtl('trn_dpmonitoring');
		$data = array(
			'equip' => $equip,
			'dpdtl' => $dpdtl		
			);
		// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
		$this->load->view('injectable/DP_monitoring', ['data'=>$data]);
	}

	public function CleaningAndSanitizationActivityAsepticArea()
	{
		$result = $this->Usermodel->getActivitytype();
		$timezone = "Asia/Kolkata";
 		date_default_timezone_set($timezone);
		$empblock_code = $this->session->userdata('empblock_code');
		$emp_code = $this->session->userdata('empcode');
		$this->load->model('Productionmodel');
		$this->load->model('Secondblockmodel');
		$sol = $this->Productionmodel->get_dtl2('stop_on !=',NULL,'trn_injsolutiondetails');
		$room = $this->Productionmodel->get_dtl2('area_code',$this->session->userdata('emparea_code'),'mst_room');
		$cleaningtype = $this->Secondblockmodel->get_cleaningtype('mst_inj_cleaning');
		

		$CSAAAheaderid = $this->Secondblockmodel->getCSAAAheaderid(date("Y-m-d"),$emp_code);
		$CSAAAheaderid = $CSAAAheaderid->row_array();
		$id = $CSAAAheaderid['id'];
		$CSAAAdtl = $this->Secondblockmodel->getCSAAAdtl($id);
		
		$cleaning = $this->Secondblockmodel->get_dtl5_withorderby('mst_inj_cleaning','seq','ASC','seq','1');


		$data = array(

			'sol' => $sol,
			'room' => $room,
			'cleaningtype' => $cleaningtype,
			'cleaning' => $cleaning,
			'CSAAAdtl' => $CSAAAdtl
			);


		   // header menu code
			
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
		//$this->load->view('header');
		$this->load->view('injectable/CleaningAndSanitizationActivityAsepticArea', ['data'=>$data]);
	}


	public function Material_transfer_detail()
	{
		$empblock_code = $this->session->userdata('empblock_code');
		$this->load->model('Productionmodel');
		$this->load->model('Secondblockmodel');
		$equip = $this->Productionmodel->get_dtl2('room_code',$this->session->userdata('emparea_code'),'mst_equipment');
		$matdtl = $this->Secondblockmodel->getmatdtl();
		$data = array(
			'equip' => $equip,
			'matdtl' => $matdtl		
			);
		// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
		$this->load->view('injectable/Material_transfer_details', ['data'=>$data]);
	}
	public function CleaningAndSanitization()
	{
		$emp_code = $this->session->userdata('empcode');
    	$empname = $this->session->userdata('empname');
		$empblock_code = $this->session->userdata('empblock_code');
		$this->load->model('Productionmodel');
		$equip = $this->Productionmodel->get_dtl2('room_code',$this->session->userdata('emparea_code'),'mst_equipment');
		$this->load->model('Secondblockmodel');
		$CSdtl = $this->Secondblockmodel->get_dtl1('trn_cleaningandsanitization');
		$CSsingledtl = $this->Productionmodel->get_dtl5('created_by',$emp_code,'endtime',null,'trn_cleaningandsanitization');
		$response=$CSsingledtl->row_array();
		$id = $response['id'];
		$eqcode = $response['equipcode'];

		$q = $this->Productionmodel->get_dtl2('equipment_code',$eqcode,'mst_equipment');
		$res = $q->row_array();
		$equipment_name =  $res['equipment_name'];

		$date = $response['date'];
		$lotno = $response['lotno'];

		$data = array(
			'id' => $id,
			'eqcode' => $eqcode,
			'equipment_name' => $equipment_name,
			'date' => $date,
			'lotno' => $lotno,
			'equip' => $equip,
			'CSdtl' => $CSdtl		
			);
		// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
		$this->load->view('injectable/CleaningAndSanitization', ['data'=>$data]);
	}
	public function Materialtransferdtl(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);

    $equipcode = $_POST['equipcode'];
    $sop_code = $_POST['sop_code'];
    $docid = $_POST['docid'];
    $date = $_POST['date'];
    $time = $_POST['time'];
    $dpvalue = $_POST['dpvalue'];
    $ensuredby = $_POST['ensuredby'];
    $matdtl = $_POST['matdtl'];
    $ftime = $_POST['ftime'];
    $ttime = $_POST['ttime'];
    $Sagent = $_POST['Sagent'];
    $prfmdby = $_POST['prfmdby'];

    $this->load->model('Secondblockmodel');
    $response = $this->Secondblockmodel->insertmattransferdtl($equipcode,$sop_code,$docid,$date,$time,$dpvalue,$ensuredby,$matdtl,$ftime,$ttime,$Sagent,$prfmdby);
    echo json_encode($response);
  }

  public function DP_monitoringdtl(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);

    $equipcode = $_POST['equipcode'];
    $sop_code = $_POST['sop_code'];
    $docid = $_POST['docid'];
    $date = $_POST['date'];
    $time = $_POST['time'];
    $dpvalue = $_POST['dpvalue'];
    $ensuredby = $_POST['ensuredby'];

    $this->load->model('Secondblockmodel');
    $response = $this->Secondblockmodel->insertDP_monitoringdtl($equipcode,$sop_code,$docid,$date,$time,$dpvalue,$ensuredby);
    echo json_encode($response);
  }

  public function CSdtl(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);

    $emp_code = $this->session->userdata('empcode');
    $empname = $this->session->userdata('empname');
    $equipcode = $_POST['equipcode'];
    $sop_code = $_POST['sop_code'];
    $docid = $_POST['docid'];
    $CSdate = $_POST['CSdate'];
    $lotno = $_POST['lotno'];

    $this->load->model('Secondblockmodel');
    date_default_timezone_set('Asia/Calcutta'); # add your city to set local time zone
	$now = date('Y-m-d H:i:s');
    $response = $this->Secondblockmodel->insertCSdtl($equipcode,$sop_code,$docid,$CSdate,$lotno,$now,$emp_code,$empname);
    echo json_encode($response);
  }

  public function stopCSdtl(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);

    $emp_code = $this->session->userdata('empcode');
    $empname = $this->session->userdata('empname');
    $id = $_POST['id'];
    $this->load->model('Secondblockmodel');
    date_default_timezone_set('Asia/Calcutta'); # add your city to set local time zone
	$now = date('Y-m-d H:i:s');
    $response = $this->Secondblockmodel->updateCSdtl($id,$now,$emp_code,$empname);
    echo json_encode($response);
  }

  public function getsop(){
		header('Access-Control-Allow-Origin: *');  
		header("Content-Type: application/json", true);
		$this->load->model('Productionmodel');
		$area_code = $_POST["area"];
		$q = $this->Productionmodel->get_dtl2('area_code',$area_code,'mst_sop');
		$res = $q->row_array();
		$sop_code =  $res['sop_code'];
		echo json_encode($sop_code);
	}

	public function userauthentication()
	{
		header('Access-Control-Allow-Origin: *');  
		header("Content-Type: application/json", true);
		$userid = $_POST['emailid'];
		$pwd = $_POST['pwd'];
		$rmk = $_POST['rmk'];
		$id = $_POST['id'];
		$tblname = $_POST['tblname'];
		$this->load->model('Loginmodel');
		$q = $this->Loginmodel->userauth($userid,$pwd);
		if($q != false)
		{
			$row = $q->row_array();
			$empcode = $row['emp_code']; 
        	$empname = $row['emp_name'];
        	date_default_timezone_set('Asia/Calcutta'); # add your city to set local time zone
			$now = date('Y-m-d H:i:s');
			if($empcode!=$this->session->userdata('empcode'))
			{
        	$arr = array("checkedby_code"=>$empcode,"checkedby_name"=>$empname,"checkedon"=>$now,"remark"=>$rmk,"modified_by"=>$empname,"modified_on"=>$now);		
    		$this->db->where("id",$id);  
    		$this->db->update($tblname,$arr);
    		$response = array("status"=>1,"msg"=>"successfully Checked.");
    		}
    		else
    		{
    			$response = array("status"=>0,"msg"=>"This operation can not valid by you.");
    		}
		}
		else
		{
			$response = array("status"=>0,"msg"=>"Invalid credantial !");
		}
		echo json_encode($response);
	}

	public function CSAAA(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $timezone = "Asia/Kolkata";
 	date_default_timezone_set($timezone);
    $emp_code = $this->session->userdata('empcode');
    $empname = $this->session->userdata('empname');
    $solddl = $_POST['solddl'];
    $roomddl = $_POST['roomddl'];
    $lotno = $_POST['lotno'];
    $cleantype = $_POST['cleantype'];

    $this->load->model('Secondblockmodel');
    $response = $this->Secondblockmodel->insertCSAAA(date("Y-m-d"),$solddl,$roomddl,$lotno,$cleantype,date("Y-m-d H:i:s"),$empname,$emp_code);
    echo json_encode($response);
  }

    public function stopCSAAA(){
    header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);

    $emp_code = $this->session->userdata('empcode');
    $empname = $this->session->userdata('empname');
    $act = $_POST['act'];
    $headerid = $_POST['headerid'];



    $this->load->model('Secondblockmodel');
    $response = $this->Secondblockmodel->insertCSAAAdetail($act,$headerid,$empname,$emp_code);
    echo json_encode($response);
  }
}