<?php

class User extends CI_Controller{

	public function __construct()
    {
      parent::__construct();
      if(!$this->session->userdata('empemail'))
      {
        return redirect("login");
      }
	  if(!isset($_SERVER['HTTP_REFERER'])) {         
		  //return redirect("login?err=1");
		  //$this->load->view('login');
        } 
    }

	public function home()
	{
		$empcode = $this->session->userdata('empcode');
		$empname = $this->session->userdata('empname');
		$empemail = $this->session->userdata('empemail');

		if(!$this->session->userdata('empemail'))
		{
			return redirect("login");
			//$this->load->view('home');
		}
		else
		{
			$this->load->model('Usermodel');
			// header menu code
			$result = $this->Usermodel->getActivitytype();
                        
                        $this->data['the_view_content'] = $this->load->view("home", ['result'=>$result], true);
                        $this->load->view('template/master/layout_master', $this->data);
		}
	}

	public function test()
	{
		if(isset($_POST['act'])){$act = $_POST["act"];}else{$act = "";}
		if(isset($_POST['myArray'])){$myArray = $_POST["myArray"];}else{$myArray = "";}

$keys = array_keys($myArray);
foreach(array_keys($keys) as $index ){       
    $current_key = current($keys); // or $current_key = $keys[$index];
    $current_value = $a[$current_key]; // or $current_value = $a[$keys[$index]];

    $next_key = next($keys); 
    $next_value = $myArray[$next_key] ?? null; // for php version >= 7.0

    echo  "{$index}: current = ({$current_key} => {$current_value}); next = ({$next_key} => {$next_value})\n";
}
	}

	public function getheaderdetail()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
		$header_id = $_POST['headerid'];
		$this->load->model('Productionmodel');
		$q2 = $this->Productionmodel->get_dtl2('header_id',$header_id,'trn_operationlogdetail');
		$headerlogdtl = $q2->result_array();
		echo json_encode($headerlogdtl);
	}

	public function getmessage()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
		$id = $_POST['id'];
		$this->load->model('Productionmodel');
		$q2 = $this->Productionmodel->get_dtl2('id',$id,'mst_messages');
		$msgdtl = $q2->result_array();
		echo json_encode($msgdtl);
	}

	public function getequipmentdetail()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
		$header_id = $_POST['headerid'];
		$this->load->model('Productionmodel');
		$q2 = $this->Productionmodel->getpendingtaskEquipmentsdtl($header_id);
		$equipmentdtl = $q2->result_array();
		echo json_encode($equipmentdtl);
	}

	public function getEachequipmentlogdetail()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
		$header_id = $_POST['headerid'];
		$equipmentcode = $_POST['equipmentcode'];
		$this->load->model('Productionmodel');
		$equipmentlogdtl = $this->Productionmodel->getresultset_of_eachEuipment_of_Pendingtask($header_id,$equipmentcode);
		echo json_encode($equipmentlogdtl);
	}

	public function getLogsview(){
		header('Access-Control-Allow-Origin: *');  
	    header("Content-Type: application/json", true);
	    $this->load->model('Usermodel');
	    //$rid = $_POST['rid'];
	    //$aid = $_POST['aid'];
	    $response = $this->Usermodel->getLogsview($_POST);
	    echo json_encode($response);
	}

	public function getequipmentdtlnow(){
		header('Access-Control-Allow-Origin: *');  
	    header("Content-Type: application/json", true);
	    $this->load->model('Usermodel');
	    $response = $this->Usermodel->getequipmentdtlnow($_POST);
	    echo json_encode($response);
	}

	public function AS()
	{

		$empcode = $this->session->userdata('empcode');
		$empname = $this->session->userdata('empname');
		$empemail = $this->session->userdata('empemail');

		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==2)	
		{
			$this->load->model('Usermodel');
			$this->load->model('Productionmodel');
			$this->load->model('Sanitizationmodel');
			$workflowdata = $this->Productionmodel->getnext_step2($this->session->userdata('roleid'));
			$workflowdata = $workflowdata->row_array();
			$status_id = $workflowdata['status_id'];
			//$next_step = $workflowdata['next_step'];
			$result = $this->Usermodel->getpendingoperationsforapproval($status_id);

			//rajesh
			$userworkflow = $this->Usermodel->getAllactivities($this->session->userdata('roleid'));
			$result2 = 0;
			$result3 = 0;
			$result4 = 0;
			$result5 = 0;
			$result6 = 0;
			$result7 = 0;

			foreach ($userworkflow as $wf) {
			if($wf["activity_id"]==27){
			$workflowdata2 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"27");
			$workflowdata2 = $workflowdata2->row_array();
			$status_id2 = $workflowdata2['status_id'];
			//$next_step = $workflowdata['next_step'];
			$result2 = $this->Usermodel->getpendingsolutionpreparationforapproval($status_id2);
			}

			if($wf["activity_id"]==29){
			$workflowdata3 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"29");
			$workflowdata3 = $workflowdata3->row_array();
			$status_id3 = $workflowdata3['status_id'];
			$result3 = $this->Usermodel->getpendingdrainpointforapproval($status_id3);
			}

			if($wf["activity_id"]==30){
			$workflowdata4 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"30");
			$workflowdata4 = $workflowdata4->row_array();
			$status_id4 = $workflowdata4['status_id'];
			$result4 = $this->Usermodel->getpendingdailycleaningforapproval($status_id4);
			}

			if($wf["activity_id"]==4){
			$workflowdata5 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"4");
			$workflowdata5 = $workflowdata5->row_array();
			$status_id5 = $workflowdata5['status_id'];
			$result5 = $this->Usermodel->getpendinginprocesscleaningforapproval($status_id5);
			}

			if($wf["activity_id"]==26){
			$workflowdata6 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"26");
			$workflowdata6 = $workflowdata6->row_array();
			$status_id6 = $workflowdata6['status_id'];
			$result6 = $this->Usermodel->getpendaccessorycleaningforapproval($status_id6);
			}
			

			if($wf["activity_id"]==5){
			$workflowdata7 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"5");
			$workflowdata7 = $workflowdata7->row_array();
			$status_id7 = $workflowdata7['status_id'];
			$result7 = $this->Usermodel->getpendportableforapproval($status_id7);
			}
			}
			$data = array(
			'result' => $result,
			'result2' => $result2,
			'result3' => $result3,
			'result4' => $result4,
			'result5' => $result5,
			'result6' => $result6,
			'result7' => $result7	
			);
			//rajesh
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('pending-approvals', ['data'=>$data]);
		}
		elseif($this->session->userdata('empemail') && $this->session->userdata('roleid')==3)	
		{
			return redirect(base_url(). 'User/QC');
		}
		elseif($this->session->userdata('empemail') && $this->session->userdata('roleid')==4)	
		{
			return redirect(base_url(). 'User/PM');
		}
		elseif($this->session->userdata('empemail') && $this->session->userdata('roleid')==5)	
		{
			return redirect(base_url(). 'User/QA');
		}
		elseif($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			return redirect(base_url(). 'User/logout');
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}


	public function sentforQAapprovalsol(){
	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    $this->load->model('Sanitizationmodel');
    $rid = $_POST['rid'];
    $aid = $_POST['aid'];
	$empname = $this->session->userdata('empname');
	$emp_code = $this->session->userdata('empcode');
	$q = $this->Sanitizationmodel->getnext_step2($this->session->userdata("roleid"),$aid);
	$workflowdata = $q->row_array();
	$status_id = $workflowdata['status_id'];
	$next_step = $workflowdata['next_step'];

	date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
	$endtime = date('Y-m-d H:i:s');
	//$arr2 = array("status"=>$status_id,"next_step"=>$next_step,"modified_by"=>$empname,"modified_on"=>$endtime);		
    if($aid==27){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_solutiondetails");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_name"=>$empname,"modified_on"=>$endtime);
    $this->db->where("doc_id",$data["doc_id"]);  
    if ($this->db->update("trn_solutiondetails",$arr2)) 
    {
		if($next_step == 0)
		{
		$arrstock = array("is_active"=>1);
		$this->db->where("document_no",$data["doc_id"]);
		$this->db->update("trn_solutionstock",$arrstock);
		}

		$room_array= array("in_use"=>'0',"inuse_activity"=>"");
		$this->db->where("room_code", $data["room_code"]);
		$this->db->update("mst_room", $room_array);

		
    	$arr = array("doc_id"=>$data["doc_id"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"solutionname"=>$data["solutionname"],"batch_no"=>$data["batch_no"],"qcar_no"=>$data["qcar_no"],"department_name"=>$data["department_name"],"standard_solution_qty"=>$data["standard_solution_qty"],"purified_water"=>$data["purified_water"],"makeup_volume"=>$data["makeup_volume"],"actual_solution_qty"=>$data["actual_solution_qty"],"actual_purified_water"=>$data["actual_purified_water"],"solution_valid_upto"=>$data["solution_valid_upto"],"check_remark"=>$data["check_remark"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"created_name"=>$empname,"status"=>$status_id,"next_step"=>$next_step,"required_quantity"=>$data["required_quantity"],"expiry_datatime"=>$data["expiry_datatime"]);
    	$this->db->insert("trn_solutiondetails",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==29){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_drainpointdetails");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_drainpointdetails",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"sanitizationused"=>$data["sanitizationused"],"batch_no"=>$data["batch_no"],"drain_points"=>$data["drain_points"],"identification_drainpoints"=>$data["identification_drainpoints"],"check_remark"=>$data["check_remark"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"status"=>$status_id,"next_step"=>$next_step);


    	$this->db->insert("trn_drainpointdetails",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==30){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_dailycleaning");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_dailycleaning",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"roomno"=>$data["roomno"],"wastebin"=>$data["wastebin"],"floorcovering"=>$data["floorcovering"],"drain_points"=>$data["drain_points"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"status"=>$status_id,"next_step"=>$next_step);


    	$this->db->insert("trn_dailycleaning",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==4){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_inprocesscontainercleaning");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_inprocesscontainercleaning",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"batch_no"=>$data["batch_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"product_name"=>$data["product_name"],"sop_no"=>$data["sop_no"],"remarks"=>$data["remarks"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"equipment_code"=>$data["equipment_code"],"status"=>$status_id,"next_step"=>$next_step);


    	$this->db->insert("trn_inprocesscontainercleaning",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==26){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_accessorycleaning");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_accessorycleaning",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"batch_no"=>$data["batch_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"product_name"=>$data["product_name"],"accessorylist"=>$data["accessorylist"],"remarks"=>$data["remarks"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"performed_by"=>$emp_code,"status"=>$status_id,"next_step"=>$next_step);


    	$this->db->insert("trn_accessorycleaning",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==5){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_portabledetail");
    $data2 = $this->Sanitizationmodel->geteqdetails($rid,"trn_portableequipmentdetail");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_portabledetail",$arr2)) 
    {


    	$arr = array("document_no"=>$data["document_no"],"department_name"=>$data["department_name"],"batch_code"=>$data["batch_code"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"product_code"=>$data["product_code"],"created_by"=>$emp_code,"status"=>$status_id,"next_step"=>$next_step,"role_id"=>$this->session->userdata('roleid'));


    	$this->db->insert("trn_portabledetail",$arr);

    	$this->db->where("document_no",$data["document_no"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_portabledetail");
		$response = $query->row_array();



    	foreach ($data2 as $av) {

       $arrn = array("document_portable_id"=>$response["id"],"document_no"=>$data["document_no"],"equipment_code"=>$av["equipment_code"],"equipment_type"=>$av["equipment_type"],"sop_code"=>$av["sop_code"],"changepart"=>$av["changepart"]);
		$this->db->insert("trn_portableequipmentdetail",$arrn);
    	}

		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}
	
  	echo json_encode($response);
	}

public function sentforQCapprovalsol(){
	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
    
	/*echo"<pre>";
	print_r($_POST);
	echo"</pre>";
	exit();*/

	$this->load->model('Sanitizationmodel');
    $rid = $_POST['apprid'];
    $aid = $_POST['appactid'];
	$remark = $_POST['rmk'];
	$empname = $this->session->userdata('empname');
	$emp_code = $this->session->userdata('empcode');
	$q = $this->Sanitizationmodel->getnext_step2($this->session->userdata("roleid"),$aid);
	$workflowdata = $q->row_array();
	$status_id = $workflowdata['status_id'];
	$next_step = $workflowdata['next_step'];

	date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
	$endtime = date('Y-m-d H:i:s');
	    $config['upload_path']="./uploads";
        $config['allowed_types']='PDF|pdf';
        $config['encrypt_name'] = TRUE;
        
        $this->load->library('upload',$config);
	    if($this->upload->do_upload("fl")) {
	        $data = $this->upload->data();
	        $title= $this->input->post('title');
	        $filename= $data['file_name']; 

	//$arr2 = array("status"=>$status_id,"next_step"=>$next_step,"modified_by"=>$empname,"modified_on"=>$endtime);		
    if($aid==27){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_solutiondetails");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_name"=>$empname,"modified_on"=>$endtime);
    $this->db->where("doc_id",$data["doc_id"]);  
    if ($this->db->update("trn_solutiondetails",$arr2)) 
    {
		if($next_step == 0)
		{
		$arrstock = array("is_active"=>1);
		$this->db->where("document_no",$data["doc_id"]);
		$this->db->update("trn_solutionstock",$arrstock);
		}


		$arr = array("doc_id"=>$data["doc_id"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"solutionname"=>$data["solutionname"],"batch_no"=>$data["batch_no"],"qcar_no"=>$data["qcar_no"],"department_name"=>$data["department_name"],"standard_solution_qty"=>$data["standard_solution_qty"],"purified_water"=>$data["purified_water"],"makeup_volume"=>$data["makeup_volume"],"actual_solution_qty"=>$data["actual_solution_qty"],"actual_purified_water"=>$data["actual_purified_water"],"solution_valid_upto"=>$data["solution_valid_upto"],"check_remark"=>$data["check_remark"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"created_name"=>$empname,"status"=>$status_id,"next_step"=>$next_step,"required_quantity"=>$data["required_quantity"],"expiry_datatime"=>$data["expiry_datatime"],"qc_filename"=>$filename,"qc_approveremarks"=>$remark);
    	$this->db->insert("trn_solutiondetails",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==29){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_drainpointdetails");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_drainpointdetails",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"sanitizationused"=>$data["sanitizationused"],"batch_no"=>$data["batch_no"],"drain_points"=>$data["drain_points"],"identification_drainpoints"=>$data["identification_drainpoints"],"check_remark"=>$data["check_remark"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"status"=>$status_id,"next_step"=>$next_step,"qc_filename"=>$filename,"qc_approveremarks"=>$remark);


    	$this->db->insert("trn_drainpointdetails",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==30){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_dailycleaning");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_dailycleaning",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"roomno"=>$data["roomno"],"wastebin"=>$data["wastebin"],"floorcovering"=>$data["floorcovering"],"drain_points"=>$data["drain_points"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"status"=>$status_id,"next_step"=>$next_step,"qc_filename"=>$filename,"qc_approveremarks"=>$remark);


    	$this->db->insert("trn_dailycleaning",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==4){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_inprocesscontainercleaning");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_inprocesscontainercleaning",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"batch_no"=>$data["batch_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"product_name"=>$data["product_name"],"sop_no"=>$data["sop_no"],"remarks"=>$data["remarks"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"equipment_code"=>$data["equipment_code"],"status"=>$status_id,"next_step"=>$next_step,"qc_filename"=>$filename,"qc_approveremarks"=>$remark);


    	$this->db->insert("trn_inprocesscontainercleaning",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==26){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_accessorycleaning");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_accessorycleaning",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"batch_no"=>$data["batch_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"product_name"=>$data["product_name"],"accessorylist"=>$data["accessorylist"],"remarks"=>$data["remarks"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"performed_by"=>$emp_code,"status"=>$status_id,"next_step"=>$next_step,"qc_filename"=>$filename,"qc_approveremarks"=>$remark);


    	$this->db->insert("trn_accessorycleaning",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==5){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_portabledetail");
    $data2 = $this->Sanitizationmodel->geteqdetails($rid,"trn_portableequipmentdetail");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_portabledetail",$arr2)) 
    {


    	$arr = array("document_no"=>$data["document_no"],"department_name"=>$data["department_name"],"batch_code"=>$data["batch_code"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"product_code"=>$data["product_code"],"created_by"=>$emp_code,"status"=>$status_id,"next_step"=>$next_step,"role_id"=>$this->session->userdata('roleid'),"qc_filename"=>$filename,"qc_approveremarks"=>$remark);


    	$this->db->insert("trn_portabledetail",$arr);

    	$this->db->where("document_no",$data["document_no"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_portabledetail");
		$response = $query->row_array();



    	foreach ($data2 as $av) {

       $arrn = array("document_portable_id"=>$response["id"],"document_no"=>$data["document_no"],"equipment_code"=>$av["equipment_code"],"equipment_type"=>$av["equipment_type"],"sop_code"=>$av["sop_code"],"changepart"=>$av["changepart"]);
		$this->db->insert("trn_portableequipmentdetail",$arrn);
    	}

		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}
			}
			else
	{$response = array("status"=>$this->upload->display_errors());
			//$response = $this->upload->display_errors(); 
			}

  	echo json_encode($response);
	}

	public function rejectedQAapprovalsol(){
	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
	$this->load->model('Sanitizationmodel');
    $rid = isset($_POST['rid']) ? $_POST['rid'] : $_POST['rid2'] ;
    $aid = isset($_POST['aid']) ? $_POST['aid'] : $_POST['aid2'] ;
	$remark = $_POST['remark'];
	$empname = $this->session->userdata('empname');
	$emp_code = $this->session->userdata('empcode');
	$q = $this->Sanitizationmodel->getnext_step2($this->session->userdata("roleid"),$aid);
	$workflowdata = $q->row_array();
	$status_id = $workflowdata['status_id'];
	$next_step = $workflowdata['next_step'];

	date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
	$endtime = date('Y-m-d H:i:s');
	//$arr2 = array("status"=>$status_id,"next_step"=>$next_step,"modified_by"=>$empname,"modified_on"=>$endtime);		
    if($aid==27){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_solutiondetails");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_name"=>$empname, "modified_on"=>$endtime);
    $this->db->where("doc_id",$data["doc_id"]);  
    if ($this->db->update("trn_solutiondetails",$arr2)) 
    {
		
    	$room_array= array("in_use"=>'0',"inuse_activity"=>"");
		$this->db->where("room_code", $data["room_code"]);
		$this->db->update("mst_room", $room_array);
		
		
		$arr = array("doc_id"=>$data["doc_id"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"solutionname"=>$data["solutionname"],"batch_no"=>$data["batch_no"],"qcar_no"=>$data["qcar_no"],"department_name"=>$data["department_name"],"standard_solution_qty"=>$data["standard_solution_qty"],"purified_water"=>$data["purified_water"],"makeup_volume"=>$data["makeup_volume"],"actual_solution_qty"=>$data["actual_solution_qty"],"actual_purified_water"=>$data["actual_purified_water"],"solution_valid_upto"=>$data["solution_valid_upto"],"check_remark"=>$data["check_remark"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code, "created_name"=>$empname,"status"=>0,"next_step"=>0,"required_quantity"=>$data["required_quantity"],"expiry_datatime"=>$data["expiry_datatime"],"rejection_remarks"=>$remark);
    	$this->db->insert("trn_solutiondetails",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==29){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_drainpointdetails");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_name"=>$empname,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_drainpointdetails",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"sanitizationused"=>$data["sanitizationused"],"batch_no"=>$data["batch_no"],"drain_points"=>$data["drain_points"],"identification_drainpoints"=>$data["identification_drainpoints"],"check_remark"=>$data["check_remark"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"status"=>0,"next_step"=>0, "rejection_remarks"=>$remark);


    	$this->db->insert("trn_drainpointdetails",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==30){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_dailycleaning");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_name"=>$empname,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_dailycleaning",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"roomno"=>$data["roomno"],"wastebin"=>$data["wastebin"],"floorcovering"=>$data["floorcovering"],"drain_points"=>$data["drain_points"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"status"=>0,"next_step"=>0, "rejection_remarks"=>$remark);


    	$this->db->insert("trn_dailycleaning",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==4){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_inprocesscontainercleaning");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_name"=>$empname,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_inprocesscontainercleaning",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"batch_no"=>$data["batch_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"product_name"=>$data["product_name"],"sop_no"=>$data["sop_no"],"remarks"=>$data["remarks"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"equipment_code"=>$data["equipment_code"],"status"=>0,"next_step"=>0, "rejection_remarks"=>$remark);


    	$this->db->insert("trn_inprocesscontainercleaning",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==26){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_accessorycleaning");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_name"=>$empname,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_accessorycleaning",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"batch_no"=>$data["batch_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"product_name"=>$data["product_name"],"accessorylist"=>$data["accessorylist"],"remarks"=>$data["remarks"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"performed_by"=>$emp_code,"status"=>0,"next_step"=>0, "rejection_remarks"=>$remark);


    	$this->db->insert("trn_accessorycleaning",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==5){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_portabledetail");
    $data2 = $this->Sanitizationmodel->geteqdetails($rid,"trn_portableequipmentdetail");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code, "modified_name"=>$empname,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_portabledetail",$arr2)) 
    {


    	$arr = array("document_no"=>$data["document_no"],"department_name"=>$data["department_name"],"batch_code"=>$data["batch_code"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"product_code"=>$data["product_code"],"created_by"=>$emp_code,"status"=>0,"next_step"=>0,"role_id"=>$this->session->userdata('roleid'),"rejection_remarks"=>$remark);


    	$this->db->insert("trn_portabledetail",$arr);

    	$this->db->where("document_no",$data["document_no"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_portabledetail");
		$response = $query->row_array();



    	foreach ($data2 as $av) {

       $arrn = array("document_portable_id"=>$response["id"],"document_no"=>$data["document_no"],"equipment_code"=>$av["equipment_code"],"equipment_type"=>$av["equipment_type"],"sop_code"=>$av["sop_code"],"changepart"=>$av["changepart"]);
		$this->db->insert("trn_portableequipmentdetail",$arrn);
    	}

		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}


  	echo json_encode($response);
	}



public function rejectedQCapprovalsol(){
	header('Access-Control-Allow-Origin: *');  
    header("Content-Type: application/json", true);
	
    $this->load->model('Sanitizationmodel');
    $rid = isset($_POST['rid']) ? $_POST['rid'] : $_POST['rid2'] ;
    $aid = isset($_POST['aid']) ? $_POST['aid'] : $_POST['aid2'] ;
	$remark = $_POST['remark'];
	$empname = $this->session->userdata('empname');
	$emp_code = $this->session->userdata('empcode');
	$q = $this->Sanitizationmodel->getnext_step2($this->session->userdata("roleid"),$aid);
	$workflowdata = $q->row_array();
	$status_id = $workflowdata['status_id'];
	$next_step = $workflowdata['next_step'];

	date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
	$endtime = date('Y-m-d H:i:s');
     $config['upload_path']="./uploads";
        $config['allowed_types']='PDF|pdf';
        $config['encrypt_name'] = TRUE;        
        $this->load->library('upload',$config);
	    if($this->upload->do_upload("fl"))
			{
	        $data = $this->upload->data();
	        $title= $this->input->post('title');
	        $filename= $data['file_name'];

	//$arr2 = array("status"=>$status_id,"next_step"=>$next_step,"modified_by"=>$empname,"modified_on"=>$endtime);		
    if($aid==27){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_solutiondetails");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_name"=>$empname, "modified_on"=>$endtime);
    $this->db->where("doc_id",$data["doc_id"]);  
    if ($this->db->update("trn_solutiondetails",$arr2)) 
    {
		
    	$arr = array("doc_id"=>$data["doc_id"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"solutionname"=>$data["solutionname"],"batch_no"=>$data["batch_no"],"qcar_no"=>$data["qcar_no"],"department_name"=>$data["department_name"],"standard_solution_qty"=>$data["standard_solution_qty"],"purified_water"=>$data["purified_water"],"makeup_volume"=>$data["makeup_volume"],"actual_solution_qty"=>$data["actual_solution_qty"],"actual_purified_water"=>$data["actual_purified_water"],"solution_valid_upto"=>$data["solution_valid_upto"],"check_remark"=>$data["check_remark"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code, "created_name"=>$empname,"status"=>0,"next_step"=>0,"required_quantity"=>$data["required_quantity"],"expiry_datatime"=>$data["expiry_datatime"],"rejection_remarks"=>$remark,"qc_filename"=>$filename);
    	$this->db->insert("trn_solutiondetails",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==29){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_drainpointdetails");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_name"=>$empname,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_drainpointdetails",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"sanitizationused"=>$data["sanitizationused"],"batch_no"=>$data["batch_no"],"drain_points"=>$data["drain_points"],"identification_drainpoints"=>$data["identification_drainpoints"],"check_remark"=>$data["check_remark"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"status"=>0,"next_step"=>0, "rejection_remarks"=>$remark,"qc_filename"=>$filename);


    	$this->db->insert("trn_drainpointdetails",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==30){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_dailycleaning");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_name"=>$empname,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_dailycleaning",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"roomno"=>$data["roomno"],"wastebin"=>$data["wastebin"],"floorcovering"=>$data["floorcovering"],"drain_points"=>$data["drain_points"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"status"=>0,"next_step"=>0, "rejection_remarks"=>$remark,"qc_filename"=>$filename);


    	$this->db->insert("trn_dailycleaning",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==4){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_inprocesscontainercleaning");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_name"=>$empname,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_inprocesscontainercleaning",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"batch_no"=>$data["batch_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"product_name"=>$data["product_name"],"sop_no"=>$data["sop_no"],"remarks"=>$data["remarks"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"equipment_code"=>$data["equipment_code"],"status"=>0,"next_step"=>0, "rejection_remarks"=>$remark,"qc_filename"=>$filename);


    	$this->db->insert("trn_inprocesscontainercleaning",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==26){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_accessorycleaning");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code,"modified_name"=>$empname,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_accessorycleaning",$arr2)) 
    {

    	$arr = array("document_no"=>$data["document_no"],"batch_no"=>$data["batch_no"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"department_name"=>$data["department_name"],"product_name"=>$data["product_name"],"accessorylist"=>$data["accessorylist"],"remarks"=>$data["remarks"],"role_id"=>$this->session->userdata('roleid'),"created_by"=>$emp_code,"performed_by"=>$emp_code,"status"=>0,"next_step"=>0, "rejection_remarks"=>$remark,"qc_filename"=>$filename);


    	$this->db->insert("trn_accessorycleaning",$arr);
		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}

  	if($aid==5){
    $data = $this->Sanitizationmodel->getdocsoldetails($rid,"trn_portabledetail");
    $data2 = $this->Sanitizationmodel->geteqdetails($rid,"trn_portableequipmentdetail");
	$arr2 = array("is_active"=>0,"modified_by"=>$emp_code, "modified_name"=>$empname,"modified_on"=>$endtime);
    $this->db->where("document_no",$data["document_no"]);  
    if ($this->db->update("trn_portabledetail",$arr2)) 
    {


    	$arr = array("document_no"=>$data["document_no"],"department_name"=>$data["department_name"],"batch_code"=>$data["batch_code"],"area_code"=>$data["area_code"],"room_code"=>$data["room_code"],"product_code"=>$data["product_code"],"created_by"=>$emp_code,"status"=>0,"next_step"=>0,"role_id"=>$this->session->userdata('roleid'),"rejection_remarks"=>$remark,"qc_filename"=>$filename);


    	$this->db->insert("trn_portabledetail",$arr);

    	$this->db->where("document_no",$data["document_no"]);
		$this->db->where("checked_by",null);
		$this->db->where("is_active",1);
		$query = $this->db->get("trn_portabledetail");
		$response = $query->row_array();



    	foreach ($data2 as $av) {

       $arrn = array("document_portable_id"=>$response["id"],"document_no"=>$data["document_no"],"equipment_code"=>$av["equipment_code"],"equipment_type"=>$av["equipment_type"],"sop_code"=>$av["sop_code"],"changepart"=>$av["changepart"]);
		$this->db->insert("trn_portableequipmentdetail",$arrn);
    	}

		$response = array("status"=>1);
    } 
    else 
    {
       	$response = array("status"=>0);
  	}
  	}
		}
		else { 
			//$response = array("status"=>$this->upload->display_errors()); 
	          $response = $this->upload->display_errors(); 
			  }

  	echo json_encode($response);
	}




	public function QC()
	{
		$empcode = $this->session->userdata('empcode');
		$empname = $this->session->userdata('empname');
		$empemail = $this->session->userdata('empemail');

		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==3)	
		{
			$this->load->model('Usermodel');
			$this->load->model('Productionmodel');
			$workflowdata = $this->Productionmodel->getnext_step2($this->session->userdata('roleid'));
			$workflowdata = $workflowdata->row_array();
			$status_id = $workflowdata['status_id'];
			$next_step = $workflowdata['next_step'];
			$result = $this->Usermodel->getpendingoperationsforapproval($status_id,$next_step);

			//rajesh
			$this->load->model('Sanitizationmodel');
			$userworkflow = $this->Usermodel->getAllactivities($this->session->userdata('roleid'));
			$result2 = 0;
			$result3 = 0;
			$result4 = 0;
			$result5 = 0;
			$result6 = 0;
			$result7 = 0;

			foreach ($userworkflow as $wf) {
			if($wf["activity_id"]==27){
			$workflowdata2 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"27");
			$workflowdata2 = $workflowdata2->row_array();
			$status_id2 = $workflowdata2['status_id'];
			//$next_step = $workflowdata['next_step'];
			$result2 = $this->Usermodel->getpendingsolutionpreparationforapproval($status_id2);
			}

			if($wf["activity_id"]==29){
			$workflowdata3 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"29");
			$workflowdata3 = $workflowdata3->row_array();
			$status_id3 = $workflowdata3['status_id'];
			$result3 = $this->Usermodel->getpendingdrainpointforapproval($status_id3);
			}

			if($wf["activity_id"]==30){
			$workflowdata4 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"30");
			$workflowdata4 = $workflowdata4->row_array();
			$status_id4 = $workflowdata4['status_id'];
			$result4 = $this->Usermodel->getpendingdailycleaningforapproval($status_id4);
			}

			if($wf["activity_id"]==4){
			$workflowdata5 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"4");
			$workflowdata5 = $workflowdata5->row_array();
			$status_id5 = $workflowdata5['status_id'];
			$result5 = $this->Usermodel->getpendinginprocesscleaningforapproval($status_id5);
			}

			if($wf["activity_id"]==26){
			$workflowdata6 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"26");
			$workflowdata6 = $workflowdata6->row_array();
			$status_id6 = $workflowdata6['status_id'];
			$result6 = $this->Usermodel->getpendaccessorycleaningforapproval($status_id6);
			}

			if($wf["activity_id"]==5){
			$workflowdata7 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"5");
			$workflowdata7 = $workflowdata7->row_array();
			$status_id7 = $workflowdata7['status_id'];
			$result7 = $this->Usermodel->getpendportableforapproval($status_id7);
			}
			}
			$data = array(
			'result' => $result,
			'result2' => $result2,
			'result3' => $result3,
			'result4' => $result4,
			'result5' => $result5,
			'result6' => $result6,
			'result7' => $result7	
			);
			//rajesh

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('pending-approvals', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function QCapproved()
	{
		$empname = $this->session->userdata('empname');
		$emp_code = $this->session->userdata('empcode');
		$roleid = $this->session->userdata('roleid');
		date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
		$endtime = date('Y-m-d H:i:s');
        $config['upload_path']="./uploads";
        $config['allowed_types']='PDF|pdf';
        $config['encrypt_name'] = TRUE;
        
        $this->load->library('upload',$config);
	    if($this->upload->do_upload("fl")){
	        $data = $this->upload->data();
	        $title= $this->input->post('title');
	        $filename= $data['file_name']; 
	        $this->load->model('Productionmodel');
	        $headerdata = $this->Productionmodel->getLastHeader_dtl3('id',$_POST['hddheaderid'],'trn_operationlogheader');
			$headerdata = $headerdata->row_array();
			$headernext_step = $headerdata['next_step'];
			$q = $this->Productionmodel->getnext_step($headernext_step,$this->session->userdata("roleid"),$_POST['hddactid']);
			$workflowdata = $q->row_array();
			$status_id = $workflowdata['status_id'];
			$next_step = $workflowdata['next_step'];

	        $arr = array("header_id"=>$_POST['hddheaderid'],"doneby"=>$emp_code,"name"=>$empname,"status"=>$_POST['hddstatus'],"roleid"=>$roleid,"time"=>$endtime,"type"=>"Approved","remark"=>$_POST['rmk'],"filepath"=>$filename,"created_by"=>$emp_code);
			$this->db->insert("trn_operation_workflow_approval_records",$arr);

			$arr2 = array("status"=>$status_id,"next_step"=>$next_step,"modified_by"=>$empname,"modified_on"=>$endtime);	
    		$this->db->where("id",$_POST['hddheaderid']);  
    		if ($this->db->update("trn_operationlogheader",$arr2)) { $response = "Successfully Approved."; } else { $response = "Not Approved."; }
        }
		else
		{
				$response = $this->upload->display_errors();				
		}
		echo $response;
     }


	public function senttoapproval()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);
    		$this->load->model('Productionmodel');
    		$header_id = $_POST['headerid'];
    		$actid = $_POST['actid'];
    		$status = $_POST['status'];
			$empname = $this->session->userdata('empname');
			$emp_code = $this->session->userdata('empcode');
			$roleid = $this->session->userdata('roleid');
			$headerdata = $this->Productionmodel->getLastHeader_dtl3('id',$header_id,'trn_operationlogheader');
			$headerdata = $headerdata->row_array();
			$headernext_step = $headerdata['next_step'];
			$q = $this->Productionmodel->getnext_step($headernext_step,$this->session->userdata("roleid"),$actid);
			$workflowdata = $q->row_array();
			$status_id = $workflowdata['status_id'];
			$next_step = $workflowdata['next_step'];

			date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
			$endtime = date('Y-m-d H:i:s');

			$arr = array("header_id"=>$header_id,"doneby"=>$emp_code,"name"=>$empname,"status"=>$status_id,"roleid"=>$roleid,"time"=>$endtime,"type"=>"Approved","created_by"=>$emp_code);
			$this->db->insert("trn_operation_workflow_approval_records",$arr);

			$arr2 = array("status"=>$status_id,"next_step"=>$next_step,"modified_by"=>$empname,"modified_on"=>$endtime);	
    		$this->db->where("id",$header_id);  
    		if ($this->db->update("trn_operationlogheader",$arr2)) { $response = array("status"=>1); } else { $response = array("status"=>0); }
  			echo json_encode($response);
	}

	public function senttoreject() {
			header('Access-Control-Allow-Origin: *');  
		    header("Content-Type: application/json", true);
    		$header_id = $_POST['headerid'];
    		$actid = $_POST['actid'];
    		$status = $_POST['status'];
    		$rmk = $_POST['rmk'];
			$empname = $this->session->userdata('empname');
			$emp_code = $this->session->userdata('empcode');
			$roleid = $this->session->userdata('roleid');
			date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
			$endtime = date('Y-m-d H:i:s');
			$arr = array("header_id"=>$header_id,"doneby"=>$emp_code,"name"=>$empname,"status"=>0,"roleid"=>$roleid,"time"=>$endtime,"type"=>"Rejected","remark"=>$rmk,"created_by"=>$emp_code);
			$this->db->insert("trn_operation_workflow_approval_records",$arr);
			$arr2 = array("status"=>0,"next_step"=>0,"modified_by"=>$empname,"modified_on"=>$endtime);	
    		$this->db->where("id",$header_id);  
    		if ($this->db->update("trn_operationlogheader",$arr2))
    		{ 
    			$response = array("status"=>1); 
    		} else { 
    			$response = array("status"=>0); 
    		}
  			echo json_encode($response);
	}

	public function QCreject() {
		$empname = $this->session->userdata('empname');
		$emp_code = $this->session->userdata('empcode');
		$roleid = $this->session->userdata('roleid');
		date_default_timezone_set('Asia/Kolkata'); # add your city to set local time zone
		$endtime = date('Y-m-d H:i:s');
        $config['upload_path']="./uploads";
        $config['allowed_types']='PDF|pdf';
        $config['encrypt_name'] = TRUE;        
        $this->load->library('upload',$config);
	    if($this->upload->do_upload("fl"))		 {
	        $data = $this->upload->data();
	        $title= $this->input->post('title');
	        $filename= $data['file_name'];
	        $arr = array("header_id"=>$_POST['hdheaderid'],"doneby"=>$emp_code,"name"=>$empname,"status"=>0,"roleid"=>$roleid,"time"=>$endtime,"type"=>"Rejected","remark"=>$_POST['rmk'],"filepath"=>$filename,"created_by"=>$emp_code);
			$this->db->insert("trn_operation_workflow_approval_records",$arr);
			$arr2 = array("status"=>0,"next_step"=>0,"modified_by"=>$empname,"modified_on"=>$endtime);	
    		$this->db->where("id",$_POST['hdheaderid']);  
    		if ($this->db->update("trn_operationlogheader",$arr2)) { $response = "Successfully Rejected."; } else { $response = "Not Rejected."; }
        } else { $response = $this->upload->display_errors(); }
		echo $response;
     }

	public function QA()
	{
		$empcode = $this->session->userdata('empcode');
		$empname = $this->session->userdata('empname');
		$empemail = $this->session->userdata('empemail');

		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==5)	
		{
			$this->load->model('Usermodel');
			$this->load->model('Productionmodel');
			$workflowdata = $this->Productionmodel->getnext_step2($this->session->userdata('roleid'));
			$workflowdata = $workflowdata->row_array();
			$status_id = $workflowdata['status_id'];
			$next_step = $workflowdata['next_step'];
			$result = $this->Usermodel->getpendingoperationsforapproval($status_id,$next_step);

			//rajesh
			$this->load->model('Sanitizationmodel');
			$userworkflow = $this->Usermodel->getAllactivities($this->session->userdata('roleid'));
			$result2 = 0;
			$result3 = 0;
			$result4 = 0;
			$result5 = 0;
			$result6 = 0;
			$result7 = 0;

			foreach ($userworkflow as $wf) {
			if($wf["activity_id"]==27){
			$workflowdata2 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"27");
			$workflowdata2 = $workflowdata2->row_array();
			$status_id2 = $workflowdata2['status_id'];
			//$next_step = $workflowdata['next_step'];
			$result2 = $this->Usermodel->getpendingsolutionpreparationforapproval($status_id2);
			}

			if($wf["activity_id"]==29){
			$workflowdata3 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"29");
			$workflowdata3 = $workflowdata3->row_array();
			$status_id3 = $workflowdata3['status_id'];
			$result3 = $this->Usermodel->getpendingdrainpointforapproval($status_id3);
			}

			if($wf["activity_id"]==30){
			$workflowdata4 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"30");
			$workflowdata4 = $workflowdata4->row_array();
			$status_id4 = $workflowdata4['status_id'];
			$result4 = $this->Usermodel->getpendingdailycleaningforapproval($status_id4);
			}

			if($wf["activity_id"]==4){
			$workflowdata5 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"4");
			$workflowdata5 = $workflowdata5->row_array();
			$status_id5 = $workflowdata5['status_id'];
			$result5 = $this->Usermodel->getpendinginprocesscleaningforapproval($status_id5);
			}

			if($wf["activity_id"]==26){
			$workflowdata6 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"26");
			$workflowdata6 = $workflowdata6->row_array();
			$status_id6 = $workflowdata6['status_id'];
			$result6 = $this->Usermodel->getpendaccessorycleaningforapproval($status_id6);
			}

			if($wf["activity_id"]==5){
			$workflowdata7 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"5");
			$workflowdata7 = $workflowdata7->row_array();
			$status_id7 = $workflowdata7['status_id'];
			$result7 = $this->Usermodel->getpendportableforapproval($status_id7);
			}
			}
			$data = array(
			'result' => $result,
			'result2' => $result2,
			'result3' => $result3,
			'result4' => $result4,
			'result5' => $result5,
			'result6' => $result6,
			'result7' => $result7	
			);
			//rajesh

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('pending-approvals', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function PM()
	{
		$empcode = $this->session->userdata('empcode');
		$empname = $this->session->userdata('empname');
		$empemail = $this->session->userdata('empemail');

		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==4)	
		{
			$this->load->model('Usermodel');
			$this->load->model('Productionmodel');
			$workflowdata = $this->Productionmodel->getnext_step2($this->session->userdata('roleid'));
			$workflowdata = $workflowdata->row_array();
			$status_id = $workflowdata['status_id'];
			$next_step = $workflowdata['next_step'];
			$result = $this->Usermodel->getpendingoperationsforapproval($status_id,$next_step);

			//rajesh
			$this->load->model('Sanitizationmodel');
			$userworkflow = $this->Usermodel->getAllactivities($this->session->userdata('roleid'));
			$result2 = 0;
			$result3 = 0;
			$result4 = 0;
			$result5 = 0;
			$result6 = 0;
			$result7 = 0;

			foreach ($userworkflow as $wf) {
			if($wf["activity_id"]==27){
			$workflowdata2 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"27");
			$workflowdata2 = $workflowdata2->row_array();
			$status_id2 = $workflowdata2['status_id'];
			//$next_step = $workflowdata['next_step'];
			$result2 = $this->Usermodel->getpendingsolutionpreparationforapproval($status_id2);
			}

			if($wf["activity_id"]==29){
			$workflowdata3 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"29");
			$workflowdata3 = $workflowdata3->row_array();
			$status_id3 = $workflowdata3['status_id'];
			$result3 = $this->Usermodel->getpendingdrainpointforapproval($status_id3);
			}

			if($wf["activity_id"]==30){
			$workflowdata4 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"30");
			$workflowdata4 = $workflowdata4->row_array();
			$status_id4 = $workflowdata4['status_id'];
			$result4 = $this->Usermodel->getpendingdailycleaningforapproval($status_id4);
			}

			if($wf["activity_id"]==4){
			$workflowdata5 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"4");
			$workflowdata5 = $workflowdata5->row_array();
			$status_id5 = $workflowdata5['status_id'];
			$result5 = $this->Usermodel->getpendinginprocesscleaningforapproval($status_id5);
			}

			if($wf["activity_id"]==26){
			$workflowdata6 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"26");
			$workflowdata6 = $workflowdata6->row_array();
			$status_id6 = $workflowdata6['status_id'];
			$result6 = $this->Usermodel->getpendaccessorycleaningforapproval($status_id6);
			}

			if($wf["activity_id"]==5){
			$workflowdata7 = $this->Sanitizationmodel->getnext_step2($this->session->userdata('roleid'),"5");
			$workflowdata7 = $workflowdata7->row_array();
			$status_id7 = $workflowdata7['status_id'];
			$result7 = $this->Usermodel->getpendportableforapproval($status_id7);
			}
			}
			$data = array(
			'result' => $result,
			'result2' => $result2,
			'result3' => $result3,
			'result4' => $result4,
			'result5' => $result5,
			'result6' => $result6,
			'result7' => $result7	
			);
			//rajesh

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('pending-approvals', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function addworkflow()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==2)	
		{
			$this->load->model('Usermodel');
			$activities = $this->Usermodel->getActivitiesforworkflow();
			$role = $this->Usermodel->getRole();

			$data = array(
			'activities' => $activities,
			'role' => $role,
			);
			$this->load->view('addworkflow', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function workflow()
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$this->load->model('Usermodel');
			$activities = $this->Usermodel->getActivitiesforworkflow();
			$role = $this->Usermodel->getRole();
			$wf = $this->Usermodel->getworkflow();

			$data = array(
			'activities' => $activities,
			'role' => $role,
			'wf' => $wf,
			);
			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('workflow', ['data'=>$data]);
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function addstep($actid)
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$this->load->model('Usermodel');
			$role = $this->Usermodel->getRole();

			$data = array(
			'actid' => $actid,
			'role' => $role,
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('addstep', ['data'=>$data]);
			$this->load->view('footer');
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
		
	}

	public function editstep($actid)
	{
		if($this->session->userdata('empemail') && $this->session->userdata('roleid')==6)	
		{
			$this->load->model('Usermodel');
			$role = $this->Usermodel->getRole();
			$wfdtl = $this->Usermodel->getworkflowbyactid($actid);



			$data = array(
			'actid' => $actid,
			'role' => $role,
			'wfdtl' => $wfdtl,
			);

			// header menu code
			$result = $this->Usermodel->getActivitytype();
			$this->load->view('header', ['result'=>$result]);
			// End header menu code
			$this->load->view('addstep', ['data'=>$data]);
			$this->load->view('footer');
		}
		else
		{
			return redirect(base_url(). 'User/logout');
		}
	}

	public function makeworkflow()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);

		$emp_code = $this->session->userdata('empcode');
		$roleid = $this->session->userdata('roleid');
		$block_code = $this->session->userdata('empblock_code');
		if(isset($_POST['act'])){$act = $_POST["act"];}else{$act = "";}
		if(isset($_POST['myArray'])){$myArray = $_POST["myArray"];}else{$myArray = "";}
		$this->load->model('Usermodel');
		$this->load->model('Productionmodel');
		$this->load->model('Logmaintainmodel');

		$AR= $this->Usermodel->deleterecords($act);

		$k=0;
		$count = count($myArray);
		$last = $count-1;
		if($count>1)
		{
		for($i=0;$i<$count;$i++)
		{
			if($i==$last)
			{
				$RWM = $this->Productionmodel->get_dtl2('role_id',$myArray[$i]['role'],'mst_roleid_workflow_mapping');
		  		$workflowdata = $RWM->row_array();
		  		$status_id = $workflowdata['status_id'];

	  			$AWF=$this->Logmaintainmodel->insert11para_with_tblnama('trn_workflowsteps','status_id',$status_id,'activity_id',$act,'role_id',$myArray[$i]['role'],'next_step','0','created_by',$this->session->userdata('empcode'));
			}
			else
			{
				if($myArray[$i]['role']==1 && $k==0)
				{
					$AWF=$this->Logmaintainmodel->insert11para_with_tblnama('trn_workflowsteps','status_id',1,'activity_id',$act,'role_id',$myArray[$i]['role'],'next_step',2,'created_by',$this->session->userdata('empcode'));
					$k=1;
				}

		  		$RWM = $this->Productionmodel->get_dtl2('role_id',$myArray[$i]['role'],'mst_roleid_workflow_mapping');
		  		$workflowdata = $RWM->row_array();
		  		$status_id = $workflowdata['status_id'];

	  			$RWM = $this->Productionmodel->get_dtl2('role_id',$myArray[$i+1]['role'],'mst_roleid_workflow_mapping');
	  			$workflowdata = $RWM->row_array();
	  			$nxt = $workflowdata['status_id'];

	  			$AWF2=$this->Logmaintainmodel->insert11para_with_tblnama('trn_workflowsteps','status_id',$status_id,'activity_id',$act,'role_id',$myArray[$i]['role'],'next_step',$nxt,'created_by',$this->session->userdata('empcode'));
			}
		}
		echo json_decode($AWF);
		}
		else
		{
			echo json_decode("-1");
		}					
	}

	public function deleteworkflow()
	{
		header('Access-Control-Allow-Origin: *');  
    	header("Content-Type: application/json", true);

		$emp_code = $this->session->userdata('empcode');
		$roleid = $this->session->userdata('roleid');
		$block_code = $this->session->userdata('empblock_code');
		if(isset($_POST['act'])){$act = $_POST["act"];}else{$act = "";}
		$this->load->model('Usermodel');
		$this->load->model('Productionmodel');
		$this->load->model('Logmaintainmodel');

		$AR= $this->Usermodel->deleterecords($act);

		echo json_decode($AR);						
	}

	public function pending_approvals_rejection()
	{
		header('Access-Control-Allow-Origin: *');  
		header("Content-Type: application/json", true);
		$rmk = $_POST['rmk'];
		$id = $_POST['id'];

        	$arr = array("checkedby_code"=>$empcode,"checkedby_name"=>$empname,"checkedon"=>$now,"remark"=>$rmk,"modified_by"=>$empname,"modified_on"=>$now);		
    		$this->db->where("id",$id); 

    		if($this->db->update($tblname,$arr))
    		{
    		$response = array("status"=>1,"msg"=>"successfully Checked.");
    		}
    		else
    		{
    			$response = array("status"=>0,"msg"=>"This operation can not valid by you.");
    		}
		echo json_encode($response);
	}

	public function logout()
	{
                $this->load->model("Loginmodel");
                $this->load->model('Ptscommon');
                $this->db->where("id", $this->session->userdata('user_id'));
                $this->db->update("mst_employee", array("login_status" => "0"));
                $auditParams = array('command_type' => 'insert', 'created_by' => $this->session->userdata('user_id'), 'created_by_name' => $this->session->userdata('empname'), 'activity_name' => 'User Logout', 'type' => 'user_logout', 'uniquefiled1' => 'id:'.$this->session->userdata('user_id'), 'uniquefiled2' => '', 'uniquefiled3' => '', 'table_name' => 'mst_employee', 'primary_id' => 0, 'log_user_activity_id' => 0, 'post_params' => ['reason'=>'Logout successful for user id : '.$this->session->userdata('empemail')]);
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                $this->Loginmodel->updateUserloginhistory();//Sets User's Logout time
		$this->session->unset_userdata('empcode');
		$this->session->unset_userdata('empname');
		$this->session->unset_userdata('empemail');
		$this->session->unset_userdata('empblock_code');
		@session_destroy();

		return redirect('login');
		//$this->load->view('login');
	}
        public function autoLogout()
	{
                $this->load->model("Loginmodel");
                $this->load->model('Ptscommon');
                $this->db->where("id", $this->session->userdata('user_id'));
                $this->db->update("mst_employee", array("login_status" => "0"));
                $auditParams = array('command_type' => 'insert', 'created_by' => $this->session->userdata('user_id'), 'created_by_name' => $this->session->userdata('empname'), 'activity_name' => 'User Logout', 'type' => 'user_logout', 'uniquefiled1' => 'id:'.$this->session->userdata('user_id'), 'uniquefiled2' => '', 'uniquefiled3' => '', 'table_name' => 'mst_employee', 'primary_id' => 0, 'log_user_activity_id' => 0, 'post_params' => ['reason'=>'Auto logout for user id : '.$this->session->userdata('empemail')]);
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                $this->Loginmodel->updateUserloginhistory();//Sets User's Logout time
		$this->session->unset_userdata('empcode');
		$this->session->unset_userdata('empname');
		$this->session->unset_userdata('empemail');
		$this->session->unset_userdata('empblock_code');
		@session_destroy();

		return redirect('login');
		//$this->load->view('login');
	}
}
