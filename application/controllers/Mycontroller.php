<?php
class Mycontroller extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->library("email");
    }
	
     Public function testEmail($email="", $password="") {
        $email="nittin.mittal@smhs.motherson.com";
        $password="TEST123";
        $msg = '<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
   <head>
      <meta charset="utf-8">
      <title>Password</title>
      <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,300;1,400&display=swap" rel="stylesheet">
   </head>
   <body width="100%" style="margin: 0; padding: 30px 0 !important; background-color: #f1f1f1; font-family: Lato, sans-serif;">
      <center>
         <table width="600px" border="0" cellspacing="0" cellpadding="0" style="background-color: #fff; padding: 10px 20px; border-collapse: separate;
            border-spacing: 0px; border: 1px solid #ccc; border-radius: 10px;">
            <tr>
               <td style="font-size:18px; font-weight:bold; text-align:center;">SMHS eLogBook</td>
            </tr>
            <tr>
               <td style="height: 10px; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 1px; background-color: #efefef; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Dear Sir/Maam</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Your First Time password is <b>' . $password . '</b>.</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;">Please login and Reset your Password</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"><a href="'. base_url().'login">Reset Password</a></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Regards</td>
            </tr>
            <tr>
               <td style="height: 10px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Admin</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
         </table>
      </center>
   </body>
</html>';
        $subject = "Password 123";
        $this->load->library('email');
        $this->email->initialize(MAIL_CONFIG);
        $this->email->set_newline("\r\n");
        $this->email->from(MAIL_FROM); // change it to yours
        $this->email->to($email); // change it to yours
        $this->email->subject($subject);
        $this->email->message($msg);
        $this->email->set_mailtype(SMTP_MAIL_TYPE);
        if ($this->email->send()) {
            echo 'mail sent successfully';exit;
            return true;
        } else {
            echo 'mail not sent';exit;
            return false;
        }
    }
    
    
    public function sendPasswordEmailNew($email='nittin.mittal@smhs.motherson.com', $password = '123455') {
        //$this->send_email();
        $msg = '<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
   <head>
      <meta charset="utf-8">
      <title>Password</title>
      <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,300;1,400&display=swap" rel="stylesheet">
   </head>
   <body width="100%" style="margin: 0; padding: 30px 0 !important; background-color: #f1f1f1; font-family: Lato, sans-serif;">
      <center>
         <table width="600px" border="0" cellspacing="0" cellpadding="0" style="background-color: #fff; padding: 10px 20px; border-collapse: separate;
            border-spacing: 0px; border: 1px solid #ccc; border-radius: 10px;">
            <tr>
               <td style="font-size:18px; font-weight:bold; text-align:center;">SMHS eLogBook</td>
            </tr>
            <tr>
               <td style="height: 10px; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 1px; background-color: #efefef; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Dear Sir/Maam</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Your First Time password is <b>' . $password . '</b>.</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;">Please login and Reset your Password</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"><a href="' . base_url() . 'login">Reset Password</a></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Regards</td>
            </tr>
            <tr>
               <td style="height: 10px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Admin</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
         </table>
      </center>
   </body>
</html>';
        $subject = "Password";
        $this->load->library('email', MAIL_CONFIG);
        $this->email->set_newline("\r\n");
        $this->email->from(MAIL_FROM); // change it to yours
        $this->email->to($email); // change it to yours
        $this->email->subject($subject);
        $this->email->message($msg);
        $this->email->set_mailtype(SMTP_MAIL_TYPE);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }
}