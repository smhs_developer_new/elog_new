<?php

require_once("Myotworestinc.php");

class Pontasahib extends REST2 {
    /*
      public function __construct() {
      parent::__construct();
      $this->inputs();
      } */

    /**
     * Validate Toekn Authentication
     * @param string $method1
     * @param string $tokencheck
     */
    function getValidateCustomHeader($method1 = 'GET', $headers, $tokencheck = TRUE) {
        $method = $this->get_request_method();
        if ($method != $method1) { // check post method exist or not
            $error = array(STATUS => FAIL, MESSAGE => "Method not allow");
            $this->Apifunction->response($this->Apifunction->json($error), 405);
        }
        if ($headers['Content-Type'] != "application/json") { // check post method exist or not
            $error = array(STATUS => FAIL, MESSAGE => "Only application/json content type allow");
            $this->Apifunction->response($this->Apifunction->json($error), 406);
        }
    }

    /**
     * GEt Room List
     * @param string $method1
     * @param string $tokencheck
     */
    public function Roomlist() {
        $this->load->model('Ptscommon');
        $area_code = isset($_GET['area_code']) ? $_GET['area_code'] : '';
        $param = array();
        if ($area_code != '') {
            $param = array("is_active" => "1", "area_code" => $area_code);
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_room', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => False, "room_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => False, MESSAGE => 'Room does not found', "room_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End

    /**
     * Assign IO to Room
     * @param string $method1
     * @param string $tokencheck
     */
    public function Assignip() {
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $system_id = isset($_POST['system_id']) ? $_POST['system_id'] : '';
        $room_name = isset($_POST['room_name']) ? $_POST['room_name'] : '';
        $room_code = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $room_type = isset($_POST['room_type']) ? $_POST['room_type'] : '';
        $room_id = isset($_POST['room_id']) ? $_POST['room_id'] : '';
        $area_id = isset($_POST['area_id']) ? $_POST['area_id'] : '';
        $gauge_id = isset($_POST['gauge_id']) ? $_POST['gauge_id'] : '';
        $logger_id = isset($_POST['logger_id']) ? $_POST['logger_id'] : '';
        $filterData = json_decode($_POST['filter_data'], true);
        $filterArray = array();
        foreach ($filterData as $key => $value) {
            if ($value['filter_name'] != "" && $room_type!="room") {
                $filterArray[] = array("filter_name" => $value['filter_name'], "room_code" => $room_code, "filter_desc" => $value['filter_desc'],"is_approved"=>1);
            }
            else
            {
                $filterArray[] = array("filter_name" => $value['filter_name'], "room_code" => $room_code, "filter_desc" => $value['filter_desc'],"is_approved"=>0);
            }
        }
        $this->Apifunction->check_variables($system_id, "System id", $numricCheck = 'no');
        $this->Apifunction->check_variables($room_name, "Room Name", $numricCheck = 'no');
        $this->Apifunction->check_variables($room_code, "Room Code", $numricCheck = 'no');
        $this->Apifunction->check_variables($room_id, "Room Id", $numricCheck = 'no');
        $this->Apifunction->check_variables($area_id, "Area Id", $numricCheck = 'no');
        $this->load->model('Ptscommon');
        /*
          $param=array("room_name"=>$room_name,"room_code"=>$room_code,"room_id"=>$room_id,"area_id"=>$area_id);
          $res=$this->Ptscommon->getMultipleRows('default','pts_mst_sys_id',$param);
          if(!empty($res)){
          $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE =>'Device IP_ID Already Assigned to Provided Room');
          $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
          }
         */
        $time = time();
        $post = array("device_id" => $system_id, "room_name" => $room_name, "room_code" => $room_code, "room_id" => $room_id, "area_id" => $area_id, "created_date" => $time, "logger_id" => $logger_id, "gauge_id" => $gauge_id, "room_type" => $room_type);
        //$param="insert into pts_mst_sys_id(device_id,room_name,room_code,room_id,area_id,created_date) values($system_id,'$room_name',$room_code,$room_id,'$area_id',$time)";
        $data = $this->db->get_where("pts_mst_sys_id", array("room_code" => $room_code, "area_id" => $area_id, "room_id" => $room_id, "status" => 'active'))->row_array();
        if (!empty($data)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'This Room is Already assign to this terminal ' . $data['device_id']);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            
            $res = $this->Ptscommon->addToTable($db = 'default', 'pts_mst_sys_id', $post, $param = '');
            if ($res) {
                $this->db->delete("pts_mst_filter", array("room_code" => $room_code));
                $this->db->insert_batch("pts_mst_filter", $filterArray);
                $responseresult = array(STATUS => TRUE, "result" => TRUE, "message" => "Device IP_ID Assigned to Room Successfully");
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Device IP/ID does not Created');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
          

        }
    }

    // End

    /**
     * Get Room List assigned to an ip
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getassignedroomlist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $system_id = isset($_GET['system_id']) ? $_GET['system_id'] : '';
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        //$this->Apifunction->check_variables($system_id, "System id", $numricCheck = 'no');
        $param = array();
        if ($system_id != '') {
            $param = array("device_id" => $system_id);
        }
        if ($room_code != '') {
            $param = array("room_code" => $room_code);
        }

        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_sys_id', $param);
        foreach ($res as $key => $value) {
            $filter = $this->Ptscommon->getMultipleRows('default', 'pts_mst_filter', array("room_code" => $value['room_code'], "status" => "on"));
            if (!empty($filter)) {
                $filterData = array();
                foreach ($filter as $key1 => $value1) {
                    $filterData[] = $value1['filter_name'];
                }
                $res[$key]['filter'] = implode(",", $filterData);
            } else {
                $res[$key]['filter'] = "";
            }
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "room_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Room does not found', "room_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End

    /**
     * Add Activity for rooms
     * @param string $method1
     * @param string $tokencheck
     */
    public function Addroomactivity() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $activity_name = isset($_POST['activity_name']) ? $_POST['activity_name'] : '';
        $activity_url = isset($_POST['activity_url']) ? $_POST['activity_url'] : '';
        $room_id = isset($_POST['room_id']) ? $_POST['room_id'] : '';
        $mst_act_id = isset($_POST['mst_act_id']) ? $_POST['mst_act_id'] : '';
        $this->Apifunction->check_variables($activity_name, "Activity Name", $numricCheck = 'no');
        $this->Apifunction->check_variables($activity_url, "Activity Url", $numricCheck = 'no');
        $this->Apifunction->check_variables($room_id, "Room Id", $numricCheck = 'no');
        $this->Apifunction->check_variables($mst_act_id, "Room Log Activity Id", $numricCheck = 'no');

        $param = array("activity_name" => $activity_name, "room_id" => $room_id, "activity_url" => $activity_url, "mst_act_id" => $mst_act_id);
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_trn_room_log_activity', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Room Activity Already Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
        $time = time();
        $post = array("activity_name" => $activity_name, "activity_url" => $activity_url, "room_id" => $room_id, "mst_act_id" => $mst_act_id, "created_date" => $time);
        $res = $this->Ptscommon->addToTable($db = 'default', 'pts_trn_room_log_activity', $post, $param = '');
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Room Activity Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Room Activity does not Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End

    /**
     * Get a list of Activity for provided room
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getactivitylistforroom() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $room_id = isset($_GET['room_id']) ? $_GET['room_id'] : '';
//        $this->Apifunction->check_variables($room_id, "Room Id", $numricCheck = 'no');
        $param = array();
        if ($room_id != '') {
            $param = array("room_id" => $room_id);
        }

        $res = $this->Ptscommon->getMultipleRows('default', 'pts_trn_room_log_activity', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "activity_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found For Provided Room', "activity_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End

    /**
     * Get a list of Fixed Equipment
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getfixedequiplist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $room_id = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        $this->Apifunction->check_variables($room_id, "Room Code", $numricCheck = 'no');
        if ($type != '') {
            $param = array("room_code" => $room_id, "is_inused" => 0, "is_active" => 1, "equipment_type" => "Line");
        } else {
            $param = array("room_code" => $room_id, "is_inused" => 0, "is_active" => 1, "equipment_type" => "Fixed");
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "fixed_euip_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Fixed Equipment Found', "fixed_euip_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End

    /**
     * Get a list of sampling rodID 
     * @param string $method1
     * @param string $tokencheck
     */
    public function GetSampleRodIDList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => '1', "equipment_type" => "Samplingrod");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_instrument', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "SamplingRod_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No record found', "SamplingRod_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End

    /**
     * Get a list of Portable Equipment
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getportableequiplist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $room_id = isset($_GET['room_code']) ? $_GET['room_code'] : '';
//        $this->Apifunction->check_variables($room_id, "Room Code", $numricCheck = 'no');
        $param = array("is_inused" => 0, "is_active" => 1, "equipment_type" => "Portable");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "potable_euip_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Portable Equipment Found', "potable_euip_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End

    /**
     * Get a list of Portable Equipment
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getmasteractivitylist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $activity_type = isset($_GET['activity_type']) ? $_GET['activity_type'] : '';
        if (!empty($activity_type)) {
            $param = array("type" => $activity_type, "status" => "active");
        } else {
            $param = '';
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_activity', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "master_activity_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Master Activity Found', "master_activity_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

        /**
     * Get a list of Portable Equipment
     * @param string $method1
     * @param string $tokencheck
     */
    public function GetVerticalmasteractivitylist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $activity_type = isset($_GET['activity_type']) ? $_GET['activity_type'] : '';
        if (!empty($activity_type)) {
            $param = array("type" => $activity_type, "status" => "active");
        } else {
            $param = '';
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_vertical_sampler_activity', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "master_activity_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Master Activity Found', "master_activity_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Inprogress Activity List
     * Created By :Rahul Chauhan
     * Date:27-01-2020
     */
    public function GetInprogressActivityList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $res = $this->Ptscommon->GetInprogressActivityList();
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "in_progress_activity" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Master Activity Found', "in_progress_activity" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**

      /**
     * Get QA Approval Activity List
     * Created By :Rahul Chauhan
     * Date:27-01-2020
     */
    public function GetQaApprovalActivityList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $res = $this->Ptscommon->GetQaApprovalActivityList();
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "qa_approval_activity" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Master Activity Found', "qa_approval_activity" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Product List
     * Created By :Rahul Chauhan
     * Date:28-01-2020
     */
    public function GetProductList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("is_active" => 1);
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_product', $param);
        $newArray[] = array("product_code" => "NA", "product_name" => "NA");
        $res = $newArray + $res;
//        echo '<pre>';print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "product_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Product Found', "product_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Reason List
     * Created By : Bhupendra kumar
     * Date: 15-03-2020
     */
    public function GetReasonData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => 'active');
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_stop_reason', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "ReasonData" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Product Found', "ReasonData" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Batch List
     * Created By : Bhupendra kumar
     * Date:21-02-2020
     */
    public function GetBatchList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("is_active" => 1);
        $res = $this->Ptscommon->getMultipleRows('default', 'trn_batch', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "batch_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Product Found', "batch_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Product Detail based on batch code
     * Created By : Bhupendra kumar
     * Date:21-02-2020
     */
    public function GetProductDetail() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $batch = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $param = array("is_active" => 1, "batch_code" => $batch);
        $res = $this->Ptscommon->getMultipleRows('default', 'trn_batch', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "product_detail" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Product Found', "product_detail" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Frequency List
     * Created By :Rahul Chauhan
     * Date:29-01-2020
     */
    public function GetFrequencyList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "active");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_frequency', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "frequency_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Frequency Found', "frequency_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Roomwise Filter List
     * Created By :Rahul Chauhan
     * Date:29-01-2020
     */
    public function GetRoomWiseFilterList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        if ($room_code != '') {
            $param = array("status" => "on", "room_code" => $room_code);
        } else {
            $param = array("status" => "on");
        }


        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_filter', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "filter_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Filter Found', "filter_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Bhupendra's code as below:-

    /**
     * User login
     */
    public function Login() {
        $headers = apache_request_headers(); // fetch header value
        $userid = isset($_POST['email']) ? $_POST['email'] : '';
        $pwd = isset($_POST['password']) ? $_POST['password'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $res = $this->Ptsadminmodel->userauth($userid, $pwd);
        if (!empty($res)) {

            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => '', "userdata" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Credential not match', "userdata" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    /**
     * Get Incremented document number from Doc table.
     */
    public function getdocno() {
        $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
        $doc_row = $doc_res->row_array();

        $doc_pre = $doc_row['prefix'];
        $doc_no = $doc_row['docno'] + 1;
        $docno = $doc_pre . $doc_no;

        $arr2 = array("docno" => $doc_no);
        $this->db->where("id", 13);
        if ($this->db->update("pts_mst_document", $arr2)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => '', "docno" => $docno);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Doc not found', "docno" => '');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * **************************************** End **************************** */

    /**
     * Get Approved in progress activities
     */
    public function getapproved() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $actlogtblid = isset($_POST['actlogtblid']) ? $_POST['actlogtblid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $workflowstatus = isset($_POST['workflowstatus']) ? $_POST['workflowstatus'] : '';
        $workflownextstep = isset($_POST['workflownextstep']) ? $_POST['workflownextstep'] : '';
        $actstatus = isset($_POST['actstatus']) ? $_POST['actstatus'] : '';
        $curr_url = isset($_GET['curr_url']) ? $_GET['curr_url'] : '';
        //Check is the person room in before perform an operation 
        //if ($curr_url != 'inprogress_activity') {
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
//            echo '<pre>';print_r($chkroomres);exit;
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        //} else {
        //  $isroomin = 1;
        //}
        // //Check is the person room in before perform an operation 
        // $isroomin=0;
        // $chkroomres = $this->Ptsadminmodel->get_dtl11('role_id',$roleid,'emp_code',$empid,'emp_name',$empname,'emp_email',$email,'session_id',$this->session->userdata('session_id'),'pts_trn_emp_roomin');
        // if ($chkroomres->num_rows() > 0) {
        //   $isroomin = 1;
        // } else {
        //   $isroomin = 0;
        //   $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
        //   $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        //   exit;
        // }

        $res = $this->Ptsadminmodel->getnext_step2($workflownextstep, $actid, $actstatus);

        if ($res->num_rows() > 0) {
            $workflowdata = $res->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";
            $btnval = 'off';

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
//            echo '<pre>';print_r($res2->result());exit;
            $getbtntext = $res2->result();
            //$btnval=$getbtntext[0]->prty_btn_text;

            if ($btnval == 'off') {
                foreach ($res2->result() as $r2) {
                    if (($r2->workassign_to_roleid == $roleid)) {
                        //getting Last trn record to match credential with previous one...
                        $lastrecord = $this->Ptsadminmodel->get_lastrecord('workflow_type', $actstatus, 'usr_activity_log_id', $actlogtblid, 'pts_trn_activity_approval_log');

                        if ($lastrecord == "") {
                            $candone = 1;
                        } else {
                            //checking is current user had worked on this activity?
                            $iscur_user_wrk = 0;
                            foreach ($lastrecord->result() as $Lastrec) {
                                if ($Lastrec->user_name == $empname) {
                                    $iscur_user_wrk = 1;
                                    break;
                                }
                            }
                            if ($iscur_user_wrk == 1) {
                                $candone = 0;
                            } else {
                                $candone = 1;
                            }
                        }
                        break;
                    } else {
                        $candone = 0;
                    }
                }
            }

            if ($candone == 1 && $isroomin == 1) {
                $result = $this->Ptscommon->Senttonextapproval($actlogtblid, $actid, $roleid, $remark, $status_id, $next_step, $empid, $empname, $email, $actstatus);
                if ($result == TRUE) {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Approved.');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    /* if ($res->num_rows() > 0) {
                      $workflowdata = $res->row_array();
                      $status_id = $workflowdata['status_id'];
                      $next_step = $workflowdata['next_step'];
                      if ($workflownextstep == $status_id) {
                      $result = $this->Ptscommon->Senttonextapproval($actlogtblid, $actid, $roleid, $remark, $status_id, $next_step, $empcode);
                      if ($result == TRUE) {
                      $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Approved.');
                      $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                      } else {
                      $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                      $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                      }
                      } else {
                      $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to Approved this record.');
                      $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                      } */
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to Approved this record 1.');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to Approved this record 2.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    /**
     * Get Approved in progress activities
     */
    public function com_approval_fun() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $actlogtblid = isset($_POST['actlogtblid']) ? $_POST['actlogtblid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $workflowstatus = isset($_POST['workflowstatus']) ? $_POST['workflowstatus'] : '';
        $workflownextstep = isset($_POST['workflownextstep']) ? $_POST['workflownextstep'] : '';
        $actstatus = isset($_POST['actstatus']) ? $_POST['actstatus'] : '';
        $result_rgt = isset($_POST['result_rgt']) ? $_POST['result_rgt'] : '';
        $result_lft = isset($_POST['result_lft']) ? $_POST['result_lft'] : '';
        $docno = isset($_POST['docno']) ? $_POST['docno'] : '';
        $tblname = isset($_POST['tblname']) ? $_POST['tblname'] : '';


        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }


        //Previous room in code below
        //Check is the person room in before perform an operation 
        // $isroomin=0;
        // $chkroomres = $this->Ptsadminmodel->get_dtl11('role_id',$roleid,'emp_code',$empid,'emp_name',$empname,'emp_email',$email,'session_id',$this->session->userdata('session_id'),'pts_trn_emp_roomin');
        // if ($chkroomres->num_rows() > 0) {
        //   $isroomin = 1;
        // } else {
        //   $isroomin = 0;
        //   $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
        //   $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        //   exit;
        // }
        // End

        $res = $this->Ptsadminmodel->getnext_step2($workflownextstep, $actid, $actstatus);
        if ($res->num_rows() > 0) {
            $workflowdata = $res->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";
            $btnval = 'off';

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            $getbtntext = $res2->result();
            //$btnval=$getbtntext[0]->prty_btn_text;

            if ($btnval == 'off') {
                foreach ($res2->result() as $r2) {
                    if (($r2->workassign_to_roleid == $roleid)) {
                        //getting Last trn record to match credential with previous one...
                        $lastrecord = $this->Ptsadminmodel->get_lastrecord('workflow_type', $actstatus, 'usr_activity_log_id', $actlogtblid, 'pts_trn_activity_approval_log');

                        if ($lastrecord == "") {
                            $candone = 1;
                        } else {
                            //checking is current user had worked on this activity?
                            $iscur_user_wrk = 0;
                            foreach ($lastrecord->result() as $Lastrec) {
                                if ($Lastrec->user_name == $empname) {
                                    $iscur_user_wrk = 1;
                                    break;
                                }
                            }
                            if ($iscur_user_wrk == 1) {
                                $candone = 0;
                            } else {
                                $candone = 1;
                            }
                        }
                        break;
                    } else {
                        $candone = 0;
                    }
                }
            }

            if ($candone == 1 && $isroomin == 1) {
                $result = $this->Ptscommon->Sentfornextapproval($actlogtblid, $actid, $roleid, $remark, $status_id, $next_step, $empid, $empname, $email, $actstatus, $docno, $tblname, $result_rgt, $result_lft);
                if ($result == TRUE) {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Approved.');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to Approved this record.');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to Approved this record.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    /**
     * Get QA Approve activities
     */
    public function getQAapproved() {
        $headers = apache_request_headers(); // fetch header value
        $actlogtblid = isset($_POST['actlogtblid']) ? $_POST['actlogtblid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';

        $result = $this->Ptscommon->SenttoQAapproval($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email);
        if ($result == TRUE) {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Approved.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    /**
     * Get Stop in progress activities
     */
    public function getstoped() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $actlogtblid = isset($_POST['actlogtblid']) ? $_POST['actlogtblid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $stopreason = isset($_POST['stopreason']) ? $_POST['stopreason'] : '';
        $curr_url = isset($_GET['curr_url']) ? $_GET['curr_url'] : '';
        //Check is the person room in before perform an operation 
        //if ($curr_url != 'inprogress_activity') {
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        // } else {
        //     $isroomin = 1;
        // }
        //Getting Next step of workflow of current Activity
        $res = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($res->num_rows() > 0) {
            $workflowdata = $res->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = -1;
        }

        if ($candone == 1 && $isroomin == 1) {
            $result = $this->Ptscommon->Senttostop($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email, $status_id, $next_step, $stopreason);
            if ($result == TRUE) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Stopped.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to stop this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    /**
     * Get start of selected activity
     */
    public function getstarted() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $equipmentcode = isset($_POST['equipmentcode']) ? $_POST['equipmentcode'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $get_doc = isset($_POST['docno']) ? $_POST['docno'] : '';
        $sampling_rod_id = isset($_POST['sampling_rod_id']) ? $_POST['sampling_rod_id'] : '';
        $dies_no = isset($_POST['dies_no']) ? $_POST['dies_no'] : '';

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        if ($get_doc == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $doc_no = $doc_row['docno'] + 1;
            $docno = $doc_pre . $doc_no;

            $arr2 = array("docno" => $doc_no);
            $this->db->where("id", 13);
            $this->db->update("pts_mst_document", $arr2);
        } else {
            $docno = $get_doc;
        }
        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'start');
        if ($wfres->num_rows() > 0) {
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
        }



        if ($candone == 1 && $isroomin == 1) {
            $result = $this->Ptscommon->Senttostart($roomcode, $processid, $equipmentcode, $actid, $actname, $product_no, $batch_no, $roleid, $empid, $empname, $email, $status_id, $next_step, $remark, $remark2, $docno, $sampling_rod_id, $dies_no);
            if ($result > 0) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Started.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get start of selected activity
     */
    public function verticalstarted() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $equipmentcode = isset($_POST['equipmentcode']) ? $_POST['equipmentcode'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $get_doc = isset($_POST['docno']) ? $_POST['docno'] : '';
        $sampling_rod_id = isset($_POST['sampling_rod_id']) ? $_POST['sampling_rod_id'] : '';
        $dies_no = isset($_POST['dies_no']) ? $_POST['dies_no'] : '';

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        if ($get_doc == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $doc_no = $doc_row['docno'] + 1;
            $docno = $doc_pre . $doc_no;

            $arr2 = array("docno" => $doc_no);
            $this->db->where("id", 13);
            $this->db->update("pts_mst_document", $arr2);
        } else {
            $docno = $get_doc;
        }
        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'start');
        if ($wfres->num_rows() > 0) {
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
        }



        if ($candone == 1 && $isroomin == 1) {
            $result = $this->Ptscommon->Senttostart($roomcode, $processid, $equipmentcode, $actid, $actname, $product_no, $batch_no, $roleid, $empid, $empname, $email, $status_id, $next_step, $remark, $remark2, $docno, $sampling_rod_id, $dies_no);
            if ($result > 0) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Started.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    public function submit_return_air_filter() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $frequency = isset($_POST['frequency']) ? $_POST['frequency'] : '';
        $selectedfilters = isset($_POST['selectedfilters']) ? $_POST['selectedfilters'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {

            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "frequency_id" => $frequency, "filter" => $selectedfilters, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no');

            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);

            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');

            $result = $this->Ptscommon->Com_fun_to_submit_log_details($isworkflow, 'pts_trn_return_air_filter', $data, $data2, $data3);
            if ($result > 0) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_vaccum_cleaner_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $vaccum_code = isset($_POST['vaccum_code']) ? $_POST['vaccum_code'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {

            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "vaccum_cleaner_code" => $vaccum_code, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no');

            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);

            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');

            $result = $this->Ptscommon->Com_fun_to_submit_log_details($isworkflow, 'pts_trn_vaccum_cleaner', $data, $data2, $data3);
            if ($result > 0) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_env_con_diff_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';

        $logger_id = isset($_POST['logger_id']) ? $_POST['logger_id'] : '';
        $gauge_id = isset($_POST['gauge_id']) ? $_POST['gauge_id'] : '';
        $pressure_differential = isset($_POST['pressure_differential']) ? $_POST['pressure_differential'] : '';
        $reading = isset($_POST['reading']) ? $_POST['reading'] : '';
        $temp = isset($_POST['temp']) ? $_POST['temp'] : '';
        $min_temp = isset($_POST['min_temp']) ? $_POST['min_temp'] : '';
        $max_temp = isset($_POST['max_temp']) ? $_POST['max_temp'] : '';
        $rh_value = isset($_POST['rh_value']) ? $_POST['rh_value'] : '';
        $min_rh = isset($_POST['min_rh']) ? $_POST['min_rh'] : '';
        $max_rh = isset($_POST['max_rh']) ? $_POST['max_rh'] : '';
        $from_temp = isset($_POST['from_temp']) ? $_POST['from_temp'] : '';
        $to_temp = isset($_POST['to_temp']) ? $_POST['to_temp'] : '';
        $from_humidity = isset($_POST['from_humidity']) ? $_POST['from_humidity'] : '';
        $to_humidity = isset($_POST['to_humidity']) ? $_POST['to_humidity'] : '';
        $pressure = isset($_POST['pressure']) ? $_POST['pressure'] : '';
        $from_pressure = isset($_POST['from_pressure']) ? $_POST['from_pressure'] : '';
        $to_pressure = isset($_POST['to_pressure']) ? $_POST['to_pressure'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {

            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "data_log_id" => $logger_id, "magnelic_id" => $gauge_id, "pressure_diff" => $pressure_differential, "reading" => $reading, "temp" => $temp, "temp_min" => $min_temp, "temp_max" => $max_temp, "rh" => $rh_value, "rh_min" => $min_rh, "rh_max" => $max_rh, "temp_from" => $from_temp, "temp_to" => $to_temp, "rh_from" => $from_humidity, "rh_to" => $to_humidity, "globe_pressure_diff" => $pressure, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no', "from_pressure" => $from_pressure, "to_pressure" => $to_pressure);

            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);

            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');

            $result = $this->Ptscommon->Com_fun_to_submit_log_details($isworkflow, 'pts_trn_env_cond_diff', $data, $data2, $data3);
            if ($result > 0) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_equip_apparatus_log_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $apparatus_code = isset($_POST['apparatus_code']) ? $_POST['apparatus_code'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $stage = isset($_POST['stage']) ? $_POST['stage'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $cleaning_verification = isset($_POST['cleaning_verification']) ? $_POST['cleaning_verification'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {

            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_appratus_no" => $apparatus_code, "stage" => $stage, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no', "cleaning_verification" => $cleaning_verification);

            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);

            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');

            $result = $this->Ptscommon->Com_fun_to_submit_log_details($isworkflow, 'pts_trn_equip_appratus_log', $data, $data2, $data3);
            if ($result > 0) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_laf_pressure_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $booth_code = isset($_POST['booth_code']) ? $_POST['booth_code'] : '';
        $mi5_mi15 = isset($_POST['mi5_mi15']) ? $_POST['mi5_mi15'] : '';
        $mi0_mi5 = isset($_POST['mi0_mi5']) ? $_POST['mi0_mi5'] : '';
        $gi0_gi3 = isset($_POST['gi0_gi3']) ? $_POST['gi0_gi3'] : '';
        $gi15_gi5 = isset($_POST['gi15_gi5']) ? $_POST['gi15_gi5'] : '';
        $gi8_gi14 = isset($_POST['gi8_gi14']) ? $_POST['gi8_gi14'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {

            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "m1" => $mi5_mi15, "m2" => $mi0_mi5, "g1" => $gi0_gi3, "g2" => $gi15_gi5, "g3" => $gi8_gi14, "booth_no" => $booth_code, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no');

            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);

            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');

            $result = $this->Ptscommon->Com_fun_to_submit_log_details($isworkflow, 'pts_trn_laf_pressure_diff', $data, $data2, $data3);
            if ($result > 0) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_bal_calibration_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';

        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $StandardDataArray = [];
        $arraylength = false;
        $standard_array = json_decode($_POST['standardData'], true);

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        foreach ($standard_array as $key => $value) {
            $StandardDataArray[] = array(
                "doc_no" => $value['doc_no'],
                "room_code" => $value['room_code'],
                "balance_no" => $value['balance_no'],
                "frequency_id" => $value['frequency_id'],
                "standerd_wt" => $value['standard_value'],
                "id_no_of_st_wt" => implode(",", $value['id_standard']),
                "oberved_wt" => $value['observed_weight'],
                "diff" => $value['difference'],
                "sprit_level_status" => $value['spirit_level'],
                "time_status" => $value['time'],
                "done_by_user_id" => $empid,
                "done_by_user_name" => $empname,
                "done_by_role_id" => $roleid,
                "done_by_email" => $email,
                "done_by_remark" => $remark,
                "is_in_workflow" => 'no'
            );
        }
        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {

            if (count($StandardDataArray) > 1) {
                $data = $StandardDataArray;
                $arraylength = true;
            } else {
                $data = $StandardDataArray[0];
                $arraylength = false;
            }

            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);

            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');

            $result = $this->Ptscommon->Com_fun_to_submit_log_details($isworkflow, 'pts_trn_balance_calibration', $data, $data2, $data3);
            if ($result > 0) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_mat_retreival_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';

        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $material_condition = isset($_POST['material_condition']) ? $_POST['material_condition'] : '';
        $formDataArray = [];
        $arraylength = false;
        $formData = json_decode($_POST['formData'], true);

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        foreach ($formData as $key => $value) {
            $formDataArray[] = array(
                "doc_no" => $doc_no,
                "room_code" => $roomcode,
                "material_condition" => $material_condition,
                "material_batch_no" => $value['material_batch_no'],
                "material_type" => $value['material_type'],
                "material_code" => $value['material_code'],
                "done_by_user_id" => $empid,
                "done_by_user_name" => $empname,
                "done_by_role_id" => $roleid,
                "done_by_email" => $email,
                "done_by_remark" => $remark,
                "is_in_workflow" => 'no'
            );
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {

            if (count($formDataArray) > 1) {
                $data = $formDataArray;
                $arraylength = true;
            } else {
                $data = $formDataArray[0];
                $arraylength = false;
            }
            $this->db->order_by("id", "DESC");
            $getdata = $this->db->get_where("pts_trn_material_retreival_relocation", array("room_code" => $data['room_code'], "material_code" => $data['material_code'], "material_condition" => "in", "status" => "active"))->row_array();
            if (empty($getdata) && $data['material_condition'] == 'out') {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'First make Material In then perform Material out Activity', 'acttblid' => $getdata);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);

                $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');

                $result = $this->Ptscommon->Com_fun_to_submit_log_details($isworkflow, 'pts_trn_material_retreival_relocation', $data, $data2, $data3);
                if ($result > 0) {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_swab_sample_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $ar_number = isset($_POST['ar_number']) ? $_POST['ar_number'] : '';
        $equipment_code = isset($_POST['equipment_code']) ? $_POST['equipment_code'] : '';
        $equipment_desc = isset($_POST['equipment_desc']) ? $_POST['equipment_desc'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {

            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "ar_numer" => $ar_number, "equipment_id" => $equipment_code, "equipment_desc" => $equipment_desc, "pre_product_code" => $product_no, "pre_barch_no" => $batch_no, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no');

            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);

            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');

            $result = $this->Ptscommon->Com_fun_to_submit_log_details($isworkflow, 'pts_trn_swab_sample_record', $data, $data2, $data3);
            if ($result > 0) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_pre_filter_cleaning_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $product_code = isset($_POST['product_code']) ? $_POST['product_code'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $seletion_status = isset($_POST['seletion_status']) ? $_POST['seletion_status'] : '';
        $pre_filter_cleaning = isset($_POST['pre_filter_cleaning']) ? $_POST['pre_filter_cleaning'] : '';
        $outer_surface_cleaning = isset($_POST['outer_surface_cleaning']) ? $_POST['outer_surface_cleaning'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {

            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "selection_status" => $seletion_status, "pre_filter_cleaning" => $pre_filter_cleaning, "outer_surface_cleaning" => $outer_surface_cleaning, "product_code" => $product_code, "batch_no" => $batch_no, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no');

            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_code, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);

            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');

            $result = $this->Ptscommon->Com_fun_to_submit_log_details($isworkflow, 'pts_trn_pre_filter_cleaning', $data, $data2, $data3);
            if ($result > 0) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_tablet_tooling_log_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $product_code = isset($_POST['product_code']) ? $_POST['product_code'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $mst_mat_id = isset($_POST['mst_mat_id']) ? $_POST['mst_mat_id'] : '';
        $currentActivity = isset($_POST['currentActivity']) ? $_POST['currentActivity'] : '';
        $U = isset($_POST['U']) ? $_POST['U'] : '';
        $L = isset($_POST['L']) ? $_POST['L'] : '';
        $D = isset($_POST['D']) ? $_POST['D'] : '';
        $Damage = isset($_POST['Damage']) ? $_POST['Damage'] : '';
        $from = isset($_POST['from']) ? $_POST['from'] : '';
        $to = isset($_POST['to']) ? $_POST['to'] : '';
        $bno = isset($_POST['bno']) ? $_POST['bno'] : '';
        $tabqty = isset($_POST['tabqty']) ? $_POST['tabqty'] : '';
        $cumqty = isset($_POST['cumqty']) ? $_POST['cumqty'] : '';
        $cumqtypunch = isset($_POST['cumqtypunch']) ? $_POST['cumqtypunch'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $isworkflow = 0;

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {
            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "mst_matrial_id" => $mst_mat_id, "act_type" => $currentActivity, "U" => $U, "L" => $L, "D" => $D, "tablet_tooling_from" => $from, "tablet_tooling_to" => $to, "damaged" => $Damage, "b_no" => $bno, "tab_qty" => $tabqty, "cum_qty" => $cumqty, "cum_qty_punch" => $cumqtypunch, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no');

            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_code, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);

            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');

            $result = $this->Ptscommon->Com_fun_to_submit_log_details($isworkflow, 'pts_trn_tablet_tooling', $data, $data2, $data3);
            if ($result > 0) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_instrument_log_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';
        $instrument_code = isset($_POST['instrument_code']) ? $_POST['instrument_code'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $stage = isset($_POST['stage']) ? $_POST['stage'] : '';
        $test = isset($_POST['test']) ? $_POST['test'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {

            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "instrument_code" => $instrument_code, "stage" => $stage, "test" => $test, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no');

            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);

            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');

            $result = $this->Ptscommon->Com_fun_to_submit_log_details($isworkflow, 'pts_trn_instrument_log_register', $data, $data2, $data3);
            if ($result > 0) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    /**
     * Get takeover of progress activities
     */
    public function gettakeover() {
        $headers = apache_request_headers(); // fetch header value
        $actlogtblid = isset($_POST['actlogtblid']) ? $_POST['actlogtblid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $curr_url = isset($_GET['curr_url']) ? $_GET['curr_url'] : '';
        //Check is the person room in before perform an operation 
        //if ($curr_url != 'inprogress_activity') {
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        // } else {
        //     $isroomin = 1;
        // }

        if ($isroomin == 1) {
            $result = $this->Ptscommon->Senttotakeover($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email);

            if ($result == TRUE) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Takken by you.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
    }

    /*     * ********************** End ************************** */

    // End

    /**
     * Add a Material Relocation
     * @param string $method1
     * @param string $tokencheck
     */
    public function Addmaterialrelocation() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $material_name = isset($_POST['material_name']) ? $_POST['material_name'] : '';
        $material_desc = isset($_POST['material_desc']) ? $_POST['material_desc'] : '';
        $room_code = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $material_type = isset($_POST['material_type']) ? $_POST['material_type'] : '';
        $material_batch_no = isset($_POST['material_batch_no']) ? $_POST['material_batch_no'] : '';
        $material_relocation_type = isset($_POST['material_relocation_type']) ? $_POST['material_relocation_type'] : '';
        $material_code = isset($_POST['material_code']) ? $_POST['material_code'] : '';

        $this->Apifunction->check_variables($material_name, "Material name", $numricCheck = 'no');
        $this->Apifunction->check_variables($room_code, "Room Code", $numricCheck = 'no');
        $this->Apifunction->check_variables($material_type, "Serial Type", $numricCheck = 'no');
        $this->Apifunction->check_variables($material_batch_no, "Material batch no", $numricCheck = 'no');
        $this->Apifunction->check_variables($material_relocation_type, "Material Type", $material_relocation_type = 'no');
        $this->Apifunction->check_variables($material_relocation_type, "Material Relocation Type", $numricCheck = 'no');
        $this->Apifunction->check_variables($material_code, "Material Code", $numricCheck = 'no');

        $post = array("material_desc" => $material_desc, "material_relocation_type" => $material_relocation_type, "material_batch_no" => $material_batch_no, "material_name" => $material_name, "room_code" => $room_code, "material_type" => $material_type, "material_code" => $material_code);
        $res = $this->Ptscommon->addToTable($db = 'default', 'pts_mst_material_relocation', $post, $param = '');
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Material Relocation Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'aterial Relocation Does Not Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End

    /**
     * Get a list of Material Relocation
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getmaterialrelocation() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $param = array();
        if ($room_code != '') {
            $param = array("room_code" => $room_code);
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_material_relocation', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "material_relocation" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Material Relocation Found', "material_relocation" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Roomwise Vaccum Cleaner List
     * Created By :Rahul Chauhan
     * Date:03-02-2020
     */
    public function GetVaccumCleanerlist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $param = array("is_active" => "1", "is_inused" => 0, "equipment_type" => "Vacuum");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "vaccum_cleaner_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Vacuum Cleaner Found', "vaccum_cleaner_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Roomwise Dispensing Booth List
     * Created By :Rahul Chauhan
     * Date:03-02-2020
     */
    public function GetDispensingBoothList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $param = array("is_active" => "1", "room_code" => $room_code, "is_inused" => 0, "equipment_type" => "Booth");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "booth_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Dispensing Booth Found', "booth_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Add Balance calibration Master
     * Created By :Rahul Chauhan
     * Date:03-02-2020
     */
    public function AddRoomBalanceCalibration() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $balance_id = isset($_POST['balance_id']) ? $_POST['balance_id'] : 0;
        $balance_no = isset($_POST['balance_no']) ? $_POST['balance_no'] : '';
        $capacity = isset($_POST['capacity']) ? $_POST['capacity'] : '';
        $least_count = isset($_POST['least_count']) ? $_POST['least_count'] : '';
        $standard_data = isset($_POST['standard_data']) ? $_POST['standard_data'] : '';
        $this->Apifunction->check_variables($balance_no, "Balance No", $numricCheck = 'no');
        $this->Apifunction->check_variables($capacity, "Capacity", $numricCheck = 'no');
        $this->Apifunction->check_variables($least_count, "Least Count", $numricCheck = 'no');
        $post = array("balance_no" => $balance_no, "capacity" => $capacity, "least_count" => $least_count);
        if ($balance_id > 0) {
            $param = array("id" => $balance_id);
            $this->Ptscommon->updateToTable($db = 'default', 'pts_mst_balance_calibration', $post, $param);
            $res = $balance_id;
        } else {
            $res = $this->Ptscommon->addToTable($db = 'default', 'pts_mst_balance_calibration', $post, $param = '');
        }
        $standard_array = json_decode($standard_data, true);
        foreach ($standard_array as $key => $value) {
            $standard_array[$key]['balance_calibration_id'] = $res;
        }
        if (!empty($res)) {
            $standard_array = json_decode($standard_data, true);
            foreach ($standard_array as $key => $value) {
                if ($balance_id > 0) {
                    unset($standard_array[$key]["balance_calibration_id"]);
                    unset($standard_array[$key]["id"]);
                    unset($standard_array[$key]["status"]);
                    unset($standard_array[$key]["created_on"]);
                }
                $standard_array[$key]['balance_calibration_id'] = $res;
            }
//            echo '<pre>';print_r($standard_array);exit;
            $this->db->delete("pts_mst_balance_calibration_standard", array("balance_calibration_id" => $res));
            $this->db->insert_batch('pts_mst_balance_calibration_standard', $standard_array);
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Balance Calibration Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Balance Calibration Does Not Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //End of Bhupendra's code

    /**
      <<<<<<< HEAD
     * Add Balance Frequency Master  Data
     * Created By :Rahul Chauhan
     * Date:27-02-2020 
     */
    public function SaveFrequencyBalance() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $balance_no = isset($_POST['balance_no']) ? $_POST['balance_no'] : '';
        $frequency_id = isset($_POST['frequency_id']) ? $_POST['frequency_id'] : '';
        $standard_data = isset($_POST['standard_data']) ? $_POST['standard_data'] : '';
        $this->Apifunction->check_variables($balance_no, "Balance No", $numricCheck = 'no');
        $this->Apifunction->check_variables($frequency_id, "Capacity", $numricCheck = 'no');
        $standard_array = json_decode($standard_data, true);
        foreach ($standard_array as $key => $value) {
            unset($standard_array[$key]["id"]);
            unset($standard_array[$key]["ischecked"]);
            $standard_array[$key]['balance_no'] = $balance_no;
            $standard_array[$key]['frequency_id'] = $frequency_id;
        }
        $this->db->delete("pts_mst_balance_frequency_data", array("balance_no" => $balance_no, "frequency_id" => $frequency_id));
        $res = $this->db->insert_batch('pts_mst_balance_frequency_data', $standard_array);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Balance Frequency Added Successfully');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Dispensing Booth Found', "instrument_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Balance Calibration List 
      =======
     * Get Balance Calibration List
      >>>>>>> elog_25feb
     * Created By :Rahul Chauhan
     * Date:03-02-2020
     */
    public function GetBalanceCalibrationList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "1");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_balance_calibration', $param);
        foreach ($res as $key => $value) {
            $param = array("balance_calibration_id" => $value['id']);
            $standardData = array();
            $standardData = $this->Ptscommon->getMultipleRows('default', 'pts_mst_balance_calibration_standard', $param);
            $res[$key]['standard_data'] = $standardData;
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "balance_calibration_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Balance Calibration Found', "balance_calibration_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //End of Bhupendra's code

    /**
     * Get Id No of Standard Weight List
     * Created By :Rahul Chauhan
     * Date:18-02-2020
     */
    public function GetIdNoStandardWeightList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "active");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_id_standard_weight', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "id_standard_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Id No of standard weight Found', "id_standard_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Vertical Sample Rod List
     * Created By :Rahul Chauhan
     * Date:03-02-2020
     */
    public function GetsampleRodList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "1");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_vertical_sampler', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "sampler_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Vertical Sampler Found', "sampler_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Instrument Log List
     * Created By :Rahul Chauhan
     * Date:03-02-2020
     */
    public function GetInstrumentLogList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $param = array("is_active" => "1", "room_code" => $room_code, "is_inused" => 0, "equipment_type" => "Instrument");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "instrument_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Dispensing Booth Found', "instrument_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Balance calibration Frequency wise list
     * Created By :Rahul Chauhan
     * Date:04-02-2020
     */
    public function GetStandardData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $balance_no = isset($_GET['balance_no']) ? $_GET['balance_no'] : '';
        $frequency_id = isset($_GET['frequency_id']) ? $_GET['frequency_id'] : '';
        $param = array("status" => "active", "balance_no" => $balance_no, "frequency_id" => $frequency_id);
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_balance_frequency_data', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "standard_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Standard Data Found', "standard_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Equipment Apparatus Log List
     * Created By :Rahul Chauhan
     * Date:04-02-2020
     */
    public function GetEquipmentApparatuslist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $param = array("is_active" => "1", "room_code" => $room_code, "is_inused" => 0, "equipment_type" => "Apparatus");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "apparatus_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Apparatus Found', "apparatus_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Stage List
     * Created By :Rahul Chauhan
     * Date:04-02-2020
     */
    public function GetStageList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "active");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_stages', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "stage_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Stage Found', "stage_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Equipment List
     * Created By :Rahul Chauhan
     * Date:04-02-2020
     */
    public function GetEquipmentlist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        if ($room_code != '') {
            $param = array("is_active" => "1", "room_code" => $room_code, "is_inused" => 0);
        } else {
            $param = array("is_active" => "1", "is_inused" => 0);
        }

        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "equipment_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Equipment Found', "equipment_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Last Activity Product and Batch Number
     * Created By :Rahul Chauhan
     * Date:05-02-2020
     */
    public function GetLastProductAndBatchNumber() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $roomlogactivity = isset($_GET['roomlogactivity']) ? $_GET['roomlogactivity'] : '';
        $param = explode(",", $roomlogactivity);
        $res = $this->Ptscommon->GetLastProductAndBatchNumber($param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "last_product_code" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found', "last_product_code" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Last Activity Product and Batch Number 
     * Created By :Rahul Chauhan
     * Date:05-02-2020 
     */
    public function GetLastBactProductActivity() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $equipment_code = isset($_GET['equipment_code']) ? $_GET['equipment_code'] : '';
        $res = $this->Ptscommon->GetLastBactProductActivity($equipment_code);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "last_activity_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found', "last_activity_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Add Tablet Tooling Log Master
     * Created By :Rahul Chauhan
     * Date:05-02-2020
     */
    public function AddTabletToolingLog() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $product_code = isset($_POST['product_code']) ? $_POST['product_code'] : '';
        $tablet_data = isset($_POST['tablet_data']) ? $_POST['tablet_data'] : '';
        $this->Apifunction->check_variables($product_code, "Product Code", $numricCheck = 'no');
        $tablet_array = json_decode($tablet_data, true);
        foreach ($tablet_array as $key => $value) {
            $tablet_array[$key]['product_code'] = $product_code;
        }
        $res = $this->db->insert_batch('pts_mst_tablet_tooling_log', $tablet_array);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Tablet Tooling Log Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Tablet Tooling Log does not Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Tablet Tooling Log Card List
     * Created By :Rahul Chauhan
     * Date:05-02-2020
     */
    public function GetTabletToolingLogList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $product_code = isset($_GET['product_code']) ? $_GET['product_code'] : '';
        $param = array("status" => "active");
        if ($product_code != '') {
            $param = array("status" => "active", "product_code" => $product_code);
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_tablet_tooling_log', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "tablet_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Tablet Tooling Log Found', "tablet_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Master Activity Log List
     * Created By :Rahul Chauhan
     * Date:10-02-2020
     */
    public function GetMstActivityList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "active", "type" => "log");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_activity', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "mst_act_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Master Log Activity Found', "mst_act_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Id Standard Weight List
     * Created By :Rahul Chauhan
     * Date:11-02-2020
     */
    public function GetLastFilterActivity() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $table_name = isset($_GET['table_name']) ? $_GET['table_name'] : '';
        $res = $this->Ptscommon->GetLastFilterActivity($room_code, $table_name);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Approval Found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to Check Material Status, Is it in or out
     * Created By : Bhupendra
     * Created Date : 18Feb2020
     */

    public function chkmatIn_Out() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $matcode = isset($_POST['matcode']) ? $_POST['matcode'] : '';
        $res = $this->Ptscommon->GetLastMatRecord($matcode);

        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {

            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Id No of Standard Weight List
     * Created By :Rahul Chauhan
     * Date:18-02-2020
     */
    public function GetStandardWeightList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "active");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_id_standard_weight', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "standard_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Standard Weight Found', "standard_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Add Id No Standard Weight Master
     * Created By :Rahul Chauhan
     * Date:18-02-2020
     */
    public function SaveIdStandard() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $weight = isset($_POST['weight']) ? $_POST['weight'] : '';
        $measurement_unit = isset($_POST['measurement_unit']) ? $_POST['measurement_unit'] : '';
        $standard_data = isset($_POST['standard_data']) ? $_POST['standard_data'] : '';
        $standard_array = json_decode($standard_data, true);
        foreach ($standard_array as $key => $value) {
            $standard_array[$key]['weight'] = $weight;
            $standard_array[$key]['measurement_unit'] = $measurement_unit;
        }
        $this->db->delete("pts_mst_id_standard_weight", array("weight" => $weight, "measurement_unit" => $measurement_unit));
        $res = $this->db->insert_batch('pts_mst_id_standard_weight', $standard_array);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Id No Standard Weight Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Id No Standard Weight does not Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Role List
     * Created By :Rahul Chauhan
     * Date:19-02-2020
     */
    public function GetRoleList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("is_active" => "1");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_role', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "role_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Role Found', "role_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Assigned Role List
     * Created By :Rahul Chauhan
     * Date:19-02-2020
     */
    public function GetAssignRoleList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("isactive" => "1");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_role_workassign_to_mult_roles', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "role_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Role Found', "role_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Work Assign Role Master
     * Created By :Rahul Chauhan
     * Date:18-02-2020
     */
    public function SaveWorkAssign() {

        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $role_id = isset($_POST['role_id']) ? $_POST['role_id'] : '';
        $work_id = isset($_POST['work_id']) ? $_POST['work_id'] : '';
        $work_array = json_decode($work_id, true);
        $tempArry = array();
        if ($this->session->userdata('empname')) {
            $created_by = $this->session->userdata('empname');
        } else {
            $created_by = "SuperAdmin";
        }
        foreach ($work_array as $key => $value) {
            $tempArry[] = array("roleid" => $role_id, "workassign_to_roleid" => $value, "created_by" => $created_by);
        }
        $this->db->delete("pts_mst_role_workassign_to_mult_roles", array("roleid" => $role_id));
        $res = $this->db->insert_batch('pts_mst_role_workassign_to_mult_roles', $tempArry);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Work Assign Role Added Successfully');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Work Assign Role does not Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to get the process log records for report
     * Created By : Bhupendra kumar
     * Created Date : 18Feb2020
     */

    public function GetProcessLogReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $equip_code = isset($_POST['equip_code']) ? $_POST['equip_code'] : '';
        $type = isset($_POST['type']) ? $_POST['type'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $post['end_date'] = isset($_POST['end_date']) ? $_POST['end_date'] : '';
        $res = $this->Ptscommon->GetProcessLogReport($room_code, $equip_code, $type, $post);

        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Pre Filter Report Data
     * Created By : Bhupendra Kumar
     * Edited By: ****
     * Created Date:05-03-2020
     * Edit Date:***
     */
    public function GetPreFilterReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['area'] = isset($_POST['area']) ? $_POST['area'] : '';
        $post['year'] = isset($_POST['year']) ? $_POST['year'] : '';
        $post['month'] = isset($_POST['month']) ? $_POST['month'] : '';
        $res = $this->Ptscommon->GetPreFilterLogReportLAF($post);
        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Vertical Sampler Report Data
     * Created By : Bhupendra Kumar
     * Edited By: ****
     * Created Date:11-03-2020
     * Edit Date:***
     */
    public function GetVerticalSamplerReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['equipment_code'] = isset($_POST['equipment_code']) ? $_POST['equipment_code'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $post['end_date'] = isset($_POST['end_date']) ? $_POST['end_date'] : '';
        $res = $this->Ptscommon->GetVerticalSamplerReport($post);
        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Audit trail Report Data
     * Created By : Devendra Singh
     * Edited By: ****
     * Created Date:29-02-2020
     * Edit Date:***
     */
    public function airFilterCleaningReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['user_name'] = isset($_POST['user_name']) ? $_POST['user_name'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $post['end_date'] = isset($_POST['end_date']) ? $_POST['end_date'] : '';
        // print_r($post);
        // die();
        $res = $this->Ptscommon->airFilterCleaningReportList($post);
        // echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Audit trail Report Data
     * Created By : Bhupendra Kumar
     * Created Date:03-03-2020
     */
    public function auditTrailReportList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['user_name'] = isset($_POST['user_name']) ? $_POST['user_name'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $post['end_date'] = isset($_POST['end_date']) ? $_POST['end_date'] : '';
        $post['limit'] = isset($_GET['page_limit']) ? $_GET['page_limit'] : 0;
        $post['page'] = isset($_GET['page_no']) ? $_GET['page_no'] : 0;

        $totalcount = $this->Ptscommon->getTotalnumberofaudittrail($post);
        $res = $this->Ptscommon->auditTrailReportList($post);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res, "total_count" => $totalcount);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array(), "total_count" => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Equipment Apparatus Log Report
     * Created By : Devendra Singh
     * Edited By: ****
     * Created Date:29-02-2020
     * Edit Date:***
     */
    public function GetEquipmentApparatus() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['apparatus_no'] = isset($_POST['apparatus_no']) ? $_POST['apparatus_no'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $post['end_date'] = isset($_POST['end_date']) ? $_POST['end_date'] : '';
        // print_r($post);
        // die();
        $res = $this->Ptscommon->GetEquipmentApparatusLogReport($post);
        // echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to get the swab sample records for report
     * Created By : Bhupendra kumar
     * Created Date : 18Feb2020
     */

    public function GetSwabSampleReport() {
        $this->load->model('Ptscommon');
        $post['start_date'] = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $post['end_date'] = isset($_POST['end_date']) ? $_POST['end_date'] : '';
        $res = $this->Ptscommon->GetSwabSampleReport($post);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Save Filter Master
     * Created By :Rahul Chauhan
     * Date:21-02-2020
     */
    public function saveFilterData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $filter_data = isset($_POST['filter_data']) ? $_POST['filter_data'] : '';
        $filter_array = json_decode($filter_data, true);
        $res = $this->db->insert_batch('pts_mst_filter', $filter_array);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Filter Data Added Successfully');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Filter Data does not Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Save Filter Master
     * Created By :Rahul Chauhan
     * Date:21-02-2020
     */
    public function saveLineLogData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $line_log_name = isset($_POST['line_log_name']) ? $_POST['line_log_name'] : '';
        $line_log_code = isset($_POST['line_log_code']) ? $_POST['line_log_code'] : '';
        $block_code = isset($_POST['block_code']) ? $_POST['block_code'] : '';
        $area_code = isset($_POST['area_code']) ? $_POST['area_code'] : '';
        $room_code = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $equipmentData = isset($_POST['equipmentData']) ? $_POST['equipmentData'] : '';
        $equipmentArray = json_decode($equipmentData, true);
        $insertArray = array();
        $exclude = array();
        foreach ($equipmentArray as $key => $value) {
//            print_r($value);exit;
            $data = $this->db->get_where("pts_mst_line_log", array("block_code" => $block_code, "area_code" => $area_code, "room_code" => $room_code, "equipment_code" => $value, "status" => "active"))->row_array();
            if (empty($data)) {
                $insertArray[] = array("block_code" => $block_code, "area_code" => $area_code, "room_code" => $room_code, "line_log_code" => $line_log_code, "line_log_name" => $line_log_name, "equipment_code" => $value);
            } else {
                $exclude[] = $value;
            }
        }
//        print_r($exclude);exit;
        if (!empty($insertArray)) {
            $res = $this->db->insert_batch('pts_mst_line_log', $insertArray);
        }
//        print_r($exclude);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Line Log Data Added Successfully', "exclude_equipment" => $exclude);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Line Log Data does not Added', "exclude_equipment" => $exclude);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to get Line Log Master List
     * Created By : Rahul Chauhan
     * Created Date : 24-02-2020
     */

    public function GetLineLogList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "active");
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        if ($room_code != '') {
            $param = array("status" => "active", "room_code" => $room_code);
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_line_log', $param);
        //echo "<pre/>";print_r($res);exit;
        $final = array();
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $value['ischecked'] = false;
                $final[$value['line_log_name']][] = $value;
            }
            $newArray = array();

            foreach ($final as $key => $value) {
                $tempArray = array();
                $equipArray = array();
                foreach ($value as $key1 => $value1) {
                    $equipArray[] = $value1['equipment_code'];
                    if ($key == $value1['line_log_name']) {
                        $tempArray = $value1;
                    }
                }
                $tempArray['equipment_code'] = implode(", ", $equipArray);
                $newArray[] = $tempArray;
            }
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "line_log_data" => $newArray);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "line_log_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to get Block List
     * Created By : Rahul Chauhan
     * Created Date : 24-02-2020
     */

    public function GetBlockList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("is_active" => "1");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_block', $param);
        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "block_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "block_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to Block Wise Area List
     * Created By : Rahul Chauhan
     * Created Date : 24-02-2020
     */

    public function GetAreaList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $block_code = isset($_GET['block_code']) ? $_GET['block_code'] : '';
        $param = array("is_active" => "1", "block_code" => $block_code);
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_area', $param);
        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "area_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "area_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to save Istrument master Data
     * Created By : Rahul Chauhan
     * Created Date : 24-02-2020
     */

    public function saveInstrument() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $block_code = isset($_POST['block_code']) ? $_POST['block_code'] : '';
        $equipment_name = isset($_POST['equipment_name']) ? $_POST['equipment_name'] : '';
        $equipment_code = isset($_POST['equipment_code']) ? $_POST['equipment_code'] : '';
        $equipment_desc = isset($_POST['equipment_desc']) ? $_POST['equipment_desc'] : '';
        $equipment_type = isset($_POST['equipment_type']) ? $_POST['equipment_type'] : '';
        $insertArray = array("block_code" => $block_code, "equipment_name" => $equipment_name, "equipment_code" => $equipment_code, "equipment_desc" => $equipment_desc, "equipment_type" => $equipment_type);
        $data = $this->db->get_where("pts_mst_instrument", array("block_code" => $block_code, "equipment_name" => $equipment_name, "equipment_code" => $equipment_code, "status" => "1"))->row_array();
        if (!empty($data)) {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'This ' . $equipment_type . 'already exists', "area_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
        $res = $this->Ptscommon->addToTable($db = 'default', 'pts_mst_instrument', $insertArray, $param = '');
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Instrument data saved Successfully');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong.Please try again.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to get Instrument List
     * Created By : Rahul Chauhan
     * Created Date : 24-02-2020
     */

    public function GetInstrumentList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $equipment_type = isset($_GET['equipment_type']) ? $_GET['equipment_type'] : "";
        $param = array("status" => "1");
        if ($equipment_type != "") {
            $param = array("status" => "1", "equipment_type" => $equipment_type);
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_instrument', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "instrument_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "instrument_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get a person room in
     */
    public function getroomin() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        if ($this->session->userdata('session_id') != FALSE) {
            $session_id = $this->session->userdata('session_id');
        } else {
            $sess = md5(microtime(true) . "." . $empid);
            $this->session->set_userdata('session_id', $sess);
            $session_id = $this->session->userdata('session_id');
        }
        // $res = $this->Ptsadminmodel->get_dtl11('room_code', $roomcode, 'role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'session_id', $session_id, 'pts_trn_emp_roomin'); 
        // if ($res->num_rows() < 1) {
        // $res2 = $this->Ptsadminmodel->get_dtl9('room_code', $roomcode,'role_id', $roleid, 'emp_code', $empid, 'emp_email', $email,'pts_trn_emp_roomin');
        // if ($res2->num_rows() < 1) {
        $res3 = $this->Ptsadminmodel->get_dtl7('role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($res3->num_rows() < 1) {
            $result = $this->Ptscommon->roomin($roomcode, $roleid, $empid, $empname, $email, $session_id);
            if ($result == TRUE) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Room In please go ahead.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You are already in a room please do room out first...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }

        //} else {
        // $result = $this->Ptscommon->roominandupdate($roomcode, $roleid, $empid, $empname, $email, $session_id);
        // if ($result == TRUE) {
        //     $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Room In please go ahead.');
        //     $this->Apifunction->response($this->Apifunction->json($responseresult), 200);                    
        // } else {
        //     $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again.');
        //     $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        // }
        //   $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You are already in room : '.$roomcode.' please go ahead...');
        //   $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        // }
        // } else {
        //     $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You are already in room : '.$roomcode.' please go ahead...');
        //     $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        // }
    }

    /**
     * Get a person room out
     */
    public function getroomout() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $session_id = $this->session->userdata('session_id');

        $getres = $this->Ptsadminmodel->get_dtl7('role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($getres->num_rows() > 0) {
            $roominData = $getres->row_array();
            $roomcode2 = $roominData['room_code'];

            $getroommres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'room_code', $roomcode, 'pts_trn_emp_roomin');
            if ($getroommres->num_rows() > 0) {
                $result = $this->Ptscommon->roomout($roleid, $empid, $empname, $email);
                if ($result == TRUE) {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Room Out');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again.');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action because you have selected wrong room, please select the correct room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please do room in first, Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Balance Frequency List
     * Created By :Rahul Chauhan
     * Date:27-02-2020
     */
    public function GetBalanceFrequencyList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "active");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_balance_frequency_data', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "balance_frequency_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Balance Frequency Data Found', "balance_frequency_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    /**
     * Get Room In User List List 
     * Created By :Rahul Chauhan
     * Date:28-02-2020 
     */
    public function GetRoomInUserDetail() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $res = $this->Ptscommon->GetRoomInUserDetail();
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "roomin_user" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Vertical Sampler Found', "roomin_user" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Running Batch Details 
     * Created By :Bhupendra kumar
     * Date:02-03-2020 
     */
    public function GetrunningBatchData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $res = $this->Ptscommon->GetrunningBatchData();
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "BatchDetail" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Record Found', "BatchDetail" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get start a batch in room
     */
    public function getbatchin() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $batchcod = isset($_POST['batchcod']) ? $_POST['batchcod'] : '';
        $prodcode = isset($_POST['prodcode']) ? $_POST['prodcode'] : '';
        $product_desc = isset($_POST['product_desc']) ? $_POST['product_desc'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        // $session_id = $this->session->userdata('session_id');
        $getres = $this->Ptsadminmodel->get_dtl7('role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($getres->num_rows() > 0) {
            $roominData = $getres->row_array();
            $roomcode2 = $roominData['room_code'];

            $getroommres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'room_code', $roomcode, 'pts_trn_emp_roomin');
            if ($getroommres->num_rows() > 0) {
                $res2 = $this->Ptsadminmodel->get_dtl3('room_code', $roomcode2, 'pts_trn_batchin');
                if ($res2->num_rows() < 1) {
                    $result = $this->Ptscommon->batchin($roomcode2, $batchcod, $prodcode, $product_desc, $roleid, $empid, $empname, $email);
                    if ($result == TRUE) {
                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Batch In please go ahead.');
                        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    } else {
                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again.');
                        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    }
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Batch is already running in this room, Thank you...!');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    exit;
                }
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action because you have selected wrong room, please select the correct room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please do room in first, Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get stop a running batch of room
     */
    /* public function getroomout() {
      $headers = apache_request_headers(); // fetch header value
      $batchcod = isset($_POST['batchcod']) ? $_POST['batchcod'] : '';
      $prodcode = isset($_POST['prodcode']) ? $_POST['prodcode'] : '';
      $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
      $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
      $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
      $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
      $email = isset($_POST['email']) ? $_POST['email'] : '';
      $session_id = $this->session->userdata('session_id');
      $result = $this->Ptscommon->roomout($roomcode, $roleid, $empid, $empname, $email, $session_id);
      if ($result == TRUE) {
      $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Room Out');
      $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
      } else {
      $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again.');
      $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
      }
      } */

    /**
     * Get Room Wise Batch and Product List 
     * Created By :Rahul Chauhan
     * Date:29-02-2020
     */
    public function getRoomBatchInProductData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $param = array("room_code" => $room_code, "isactive" => "1", "batch_out_time" => null);
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_trn_batchin', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "batch_product_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Vertical Sampler Found', "batch_product_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function getbatchout() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';

        $getres = $this->Ptsadminmodel->get_dtl7('role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($getres->num_rows() > 0) {
            $roominData = $getres->row_array();
            $roomcode2 = $roominData['room_code'];

            $getroommres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'room_code', $roomcode, 'pts_trn_emp_roomin');
            if ($getroommres->num_rows() > 0) {
                $res2 = $this->Ptsadminmodel->get_dtl3('room_code', $roomcode2, 'pts_trn_batchin');
                if ($res2->num_rows() > 0) {
                    $result = $this->Ptscommon->batchout($roleid, $empid, $empname, $email);
                    if ($result == TRUE) {
                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Batch Out');
                        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    } else {
                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again.');
                        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    }
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Batch is not running in this room, Thank you...!');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    exit;
                }
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action because you have selected wrong room, please select the correct room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please do room in first, Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to get User List
     * Created By : Bhupendra Kumar
     * Created Date : 05-03-2020
     */

    public function getUsersList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("is_active" => "1");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_employee', $param);
        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "user_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "user_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Instrument Log Report
     * Created By : Rahul Chauhan
     * Created Date:11-03-2020
     */
    public function GetInstrumentReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['instrument_no'] = isset($_POST['instrument_no']) ? $_POST['instrument_no'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $post['end_date'] = isset($_POST['end_date']) ? $_POST['end_date'] : '';
        // print_r($post);
        // die();
        $res = $this->Ptscommon->GetInstrumentReport($post);
        // echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

   /**
   * Get LAF Pressure Diff Data
   * Created By : Bhupendra Kumar
   * Edited By: ****
   * Created Date:12-03-2020
   * Edit Date:***
   */
    public function lafPressureDiffReport(){
      $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['area'] = isset($_POST['area']) ? $_POST['area'] : '';
        $post['year'] = isset($_POST['year']) ? $_POST['year'] : '';
        $post['month'] = isset($_POST['month']) ? $_POST['month'] : '';
        $res = $this->Ptscommon->lafPressureDiffReport($post);
         //echo "<pre/>";print_r($res);exit;
         if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }
    
    /**
     * Vaccume Cleaner Report
     * Created By : Rahul Chauhan
     * Created Date:12-03-2020
     */
    public function getVaccumeReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['vaccume_no'] = isset($_POST['vaccume_no']) ? $_POST['vaccume_no'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $post['end_date'] = isset($_POST['end_date']) ? $_POST['end_date'] : '';
        // print_r($post);
        // die();
        $res = $this->Ptscommon->getVaccumeReport($post);
        // echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Stage Master Save
     * Created By :Rahul Chauhan
     * Date:13-03-2020
     */
    public function saveStage() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $stage_name = isset($_POST['stage_name']) ? $_POST['stage_name'] : '';
        $stage_desc = isset($_POST['stage_desc']) ? $_POST['stage_desc'] : '';
        $stage_id = isset($_POST['stage_id']) ? $_POST['stage_id'] : '';
        if ($stage_id > 0) {
            $res = $this->db->update("pts_mst_stages", array("stage_name" => $stage_name, "stage_desc" => $stage_desc), array("id" => $stage_id));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Stage Data Updated Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Stage Data does not Updated');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_stages', array("stage_name" => $stage_name, "status" => "active"));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Stage Name Already Exist');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
            $post = array("stage_name" => $stage_name, "stage_desc" => $stage_desc);
            $res = $this->Ptscommon->addToTable($db = 'default', 'pts_mst_stages', $post, $param = '');
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Stage Name Added Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Stage Name does not Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
    }

    /**
     * Delete Stage Master
     * Created By :Rahul Chauhan
     * Date:13-03-2020
     */
    public function deleteStage() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        if ($id > 0) {
            $res = $this->db->update('pts_mst_stages', array("status" => "inactive"), array("id" => $id));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Stage Name Deleted Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Stage Name does not Deleted');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Somethig went wrong.Please try again');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     *  Save Report Header Master Data
     * Created By :Rahul Chauhan
     * Date:16-03-2020
     */
    public function saveHeader() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $document_no = isset($_POST['document_no']) ? $_POST['document_no'] : '';
        $effective_date = isset($_POST['effective_date']) ? $_POST['effective_date'] : '';
        $version_no = isset($_POST['version_no']) ? $_POST['version_no'] : '';
        $form_no = isset($_POST['form_no']) ? $_POST['form_no'] : '';
        $header_id = isset($_POST['header_id']) ? $_POST['header_id'] : '';
        if ($header_id > 0) {
            $res = $this->db->update("pts_mst_report_header", array("form_no" => $form_no, "version_no" => $version_no, "effective_date" => date('Y-m-d', strtotime($effective_date)), "document_no" => $document_no), array("id" => $header_id));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Report Header Updated Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Report Header does not Updated');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_report_header', array("form_no" => $form_no, "document_no" => $document_no));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Header Already Exist');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
            $post = array("form_no" => $form_no, "version_no" => $version_no, "effective_date" => date('Y-m-d', strtotime($effective_date)), "document_no" => $document_no, "created_by" => $this->session->userdata('empname'));
            $res = $this->Ptscommon->addToTable($db = 'default', 'pts_mst_report_header', $post, $param = '');
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Report Header Added Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Report Header does not Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
    }
     /**
     * Get Filter Room List
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getroomfilters_forApproval() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("is_approved" => 0);
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_filter', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "room_filterlist" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Room does not found', "room_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Filter Approved
     * Created By : Bhupendra
     * Created On : 14march2020
     */
    public function getfilterApproved() {
        $headers = apache_request_headers(); // fetch header value
        $id = $_POST['id']; 
        $this->load->model('Ptscommon');
        $post = array("is_approved" => 1);
        $param = array("id" => $id);
        $res = $this->Ptscommon->updateToTable($db = 'default', 'pts_mst_filter', $post, $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {            
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Successfully Approved');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Header List
     * Created By :Rahul Chauhan
     * Date:16-03-2020
     */
    public function getHeaderList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "active");
        $this->db->select("*");
        $this->db->from("pts_mst_report_header");
        $this->db->where("status", "active");
        $this->db->or_where("status", "default");
        $res = $this->db->get()->result_array();
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "header_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Header Found', "header_list" => array());
        }
    }
     /* Get Previous Product and Batch Number 
     * Created By : Bhupendra kumar
     * Date: 15-02-2020 
     */
    public function GetPreBactProduct() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $res = $this->Ptscommon->GetPreBactProduct();
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "PreBactProduct" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found', "PreBactProduct" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

     /**
     * Get Previous Product and Batch Number 
     * Created By : Bhupendra kumar
     * Date: 15-02-2020 
     */
    public function GetPreBactProductofPrefilter() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $res = $this->Ptscommon->GetPreBactProductofPrefilter();
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "PreBactProduct" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found', "PreBactProduct" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Delete Stage Master
     * Created By :Rahul Chauhan
     * Date:13-03-2020
     */
    public function deleteHeader() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $action = isset($_POST['action']) ? $_POST['action'] : '';
        if ($id > 0 && $action == 'Deactive') {
            $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_report_header', array("id" => $id, "status" => "default"));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => "it's default Report Header.Please set another default header first then remove it");
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $res = $this->db->update('pts_mst_report_header', array("status" => "inactive"), array("id" => $id));
                if (!empty($res)) {
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Report Header Deleted Successfully');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Report Header does not Deleted');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }
        } elseif ($id > 0 && $action == 'Set Default') {
            $res = $this->db->update('pts_mst_report_header', array("status" => "active"), array("status" => "default"));
            $res = $this->db->update('pts_mst_report_header', array("status" => "default"), array("id" => $id));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Set Default Report Header Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Report Header does not Deleted');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Somethig went wrong.Please try again');
        }
    }
     /* Get Previous Product and Batch Number 
     * Created By : Bhupendra kumar
     * Date: 15-02-2020 
     */
    public function GetPreBactProductofprocesslog() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $res = $this->Ptscommon->GetPreBactProductofprocesslog();
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "PreBactProduct" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found', "PreBactProduct" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }
    
    
    /* This Function is used to get the Vertical Sampler records for report
     * Created By : Rahul Chauhan
     * Created Date : 17-03-2020
     */

    public function verticalSamplingReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $sampling_no = isset($_POST['sampling_no']) ? $_POST['sampling_no'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $post['end_date'] = isset($_POST['end_date']) ? $_POST['end_date'] : '';
        $res = $this->Ptscommon->verticalSamplingReport($sampling_no, $post);

        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

}
