<?php
	// create rest class and load controller
class REST2  extends  CI_Controller{
		
		public $_allow = array();
		public $_content_type = "application/json";
		public $_request = array();
		
		public $_method = "";		
		public $_code = 200;
		
		/* public function __construct(){
			$this->inputs();
		} */
		/* public static function inputcall(){
			$this->inputs();
		} */
		
		public function get_request_method(){
			return $_SERVER['REQUEST_METHOD'];
		}
		
		public function inputs(){
			switch($this->get_request_method()){
				case "POST":
					$this->_request = $this->cleanInputs($_POST);
					break;
				case "GET":
				case "DELETE":
					$this->_request = $this->cleanInputs($_GET);
					break;
				case "PUT":
					parse_str(file_get_contents("php://input"),$this->_request);
					$this->_request = $this->cleanInputs($this->_request);
					break;
				default:
					$this->response('',406);
					break;
			}
		}		
		
		public function cleanInputs($data){
			$clean_input = array();
			if(is_array($data)){
				foreach($data as $k => $v){
					$clean_input[$k] = $this->cleanInputs($v);
				}
			}else{
				if(get_magic_quotes_gpc()){
					$data = trim(stripslashes($data));
				}
				$data = strip_tags($data);
				$clean_input = trim($data);
			}
			return $clean_input;
		}		
		
		/**
		 * Get Invalid Toekn Message
		 */
		public function getTokenStatus(){
			$responseresult = array(STATUS => FAIL, "result" => false, MESSAGE => 'Invalid Token');
			$this->Apifunction->response($this->Apifunction->json($responseresult), 406);
		}
	}	
?>