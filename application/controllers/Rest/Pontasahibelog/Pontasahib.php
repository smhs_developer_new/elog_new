<?php

date_default_timezone_set('Asia/Kolkata');
require_once("Myotworestinc.php");

class Pontasahib extends REST2 {

    public function __construct() {
        parent::__construct();
        $this->load->library("email");
    }

    /*
      public function __construct() {
      parent::__construct();
      $this->inputs();
      } */

    /**
     * Validate Toekn Authentication
     * @param string $method1
     * @param string $tokencheck
     */
    function getValidateCustomHeader($method1 = 'GET', $headers, $tokencheck = TRUE) {
        $method = $this->get_request_method();
        if ($method != $method1) { // check post method exist or not
            $error = array(STATUS => FAIL, MESSAGE => "Method not allow");
            $this->Apifunction->response($this->Apifunction->json($error), 405);
        }
        if ($headers['Content-Type'] != "application/json") { // check post method exist or not
            $error = array(STATUS => FAIL, MESSAGE => "Only application/json content type allow");
            $this->Apifunction->response($this->Apifunction->json($error), 406);
        }
    }

    /**
     * GEt Room List
     * @param string $method1
     * @param string $tokencheck
     */
    public function Roomlist() {
        $this->load->model('Ptscommon');
        $area_code = isset($_GET['area_code']) ? $_GET['area_code'] : '';
        $param = array();
        if ($area_code != '') {
            $param = array("is_active" => "1", "area_code" => $area_code);
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_room', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => False, "room_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => False, MESSAGE => 'Room does not found', "room_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End 

    /**
     * Assign IO to Room
     * @param string $method1
     * @param string $tokencheck
     */
    public function Assignip() {
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $recordid = isset($_POST['recordid']) ? $_POST['recordid'] : '';
        $system_id = isset($_POST['system_id']) ? $_POST['system_id'] : '';
        $room_name = isset($_POST['room_name']) ? $_POST['room_name'] : '';
        $room_code = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $room_type = isset($_POST['room_type']) ? $_POST['room_type'] : '';
        $room_id = isset($_POST['room_id']) ? $_POST['room_id'] : '';
        $area_id = isset($_POST['area_id']) ? $_POST['area_id'] : '';
        $gauge_id = isset($_POST['gauge_id']) ? $_POST['gauge_id'] : '';
        $logger_id = isset($_POST['logger_id']) ? $_POST['logger_id'] : '';
        $status = isset($_POST['status']) ? $_POST['status'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $filterData = json_decode($_POST['filter_data'], true);
        $filterArray = $filtersArray = [];
        if (!empty($filterData)) {
            foreach ($filterData as $key => $value) {
                if ($value != "" && $room_type != "room") {
                    $filterArray[] = array("filter_name" => $value, "room_code" => $room_code, "is_approved" => 1);
                } else {
                    $filterArray[] = array("filter_name" => $value, "room_code" => $room_code, "is_approved" => 0);
                }
                $filtersArray[] = $value;
            }
        }
        $filtersCount = count($filtersArray);
        $filtersArray = implode(',', $filtersArray);
        $this->Apifunction->check_variables($system_id, "System id", $numricCheck = 'no');
        $this->Apifunction->check_variables($room_name, "Room Name", $numricCheck = 'no');
        $this->Apifunction->check_variables($room_code, "Room Code", $numricCheck = 'no');
        $this->Apifunction->check_variables($room_id, "Room Id", $numricCheck = 'no');
        $this->Apifunction->check_variables($area_id, "Area Id", $numricCheck = 'no');
        $this->load->model('Ptscommon');
        $time = time();
        $post = ["room_code" => $room_code, "device_id" => $system_id, "room_name" => $room_name, "room_id" => $room_id, "area_id" => $area_id, "created_date" => $time, "logger_id" => $logger_id, "gauge_id" => $gauge_id, "room_type" => $room_type];
        $audit_array = ["room_code" => $room_code, "room_name" => $room_name, "device_id" => $system_id, "room_type" => $room_type, "logger_id" => $logger_id, "gauge_id" => $gauge_id, "filter_count" => $filtersCount, "filters_desc" => $filtersArray];

        // check if already exist logger_id and gauge_id (khushboo)
        $loggerIds = array();
        if (isset($logger_id) && $logger_id != "") {
            $loggerIds = explode(',', $logger_id);
        }

        if (isset($loggerIds) && !empty($loggerIds)) {

            foreach ($loggerIds as $logger) {
                if ($recordid > 0) {
                    $checklogger = $this->Ptscommon->checkLoggerGuage('pts_mst_sys_id', $logger, 'logger', $recordid);
                } else {
                    $checklogger = $this->Ptscommon->checkLoggerGuage('pts_mst_sys_id', $logger, 'logger');
                }
                if (!empty($checklogger)) {
                    $message = $logger . " already used in room " . $checklogger['room_code'] . "=>" . $checklogger['room_name'] . "=>" . $checklogger['area_id'];
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => $message);
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }
        }
        $gaugeIds = array();
        if (isset($logger_id) && $gauge_id != "") {
            $gaugeIds = explode(',', $gauge_id);
        }

        if (isset($gaugeIds) && !empty($gaugeIds)) {
            foreach ($gaugeIds as $gauge) {
                if ($recordid > 0) {
                    $checklogger = $this->Ptscommon->checkLoggerGuage('pts_mst_sys_id', $gauge, 'gauge', $recordid);
                } else {
                    $checklogger = $this->Ptscommon->checkLoggerGuage('pts_mst_sys_id', $gauge, 'gauge');
                }

                if (!empty($checklogger)) {
                    $message = $gauge . " already used in room " . $checklogger['room_code'] . "=>" . $checklogger['room_name'] . "=>" . $checklogger['area_id'];
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => $message);
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }
        }

        // End check exist logger_id and gauge_id (khushboo)

        if ($recordid > 0) {
            $param = ["id" => $recordid];

            $old_data_sys = $this->db->get_where("pts_mst_sys_id", $param)->row_array();

            $this->db->select("COUNT(filter_name)AS count,GROUP_CONCAT(filter_name) AS filter_desc");
            $this->db->from("pts_mst_filter");
            $this->db->where("room_code", $room_code);
            $this->db->group_by('room_code');
            $old_data_filter = $this->db->get()->row_array();
            $filter_count = 0;
            $filters_old_array = '';
            if (!empty($old_data_filter)) {
                $filter_count = $old_data_filter['count'];
                $filters_old_array = $old_data_filter['filter_desc'];
            }

            $master_previous_data = [];
            if (!empty($old_data_sys)) {
                $master_previous_data = [
                    'room_name' => $old_data_sys['room_name'],
                    'device_id' => $old_data_sys['device_id'],
                    'room_type' => $old_data_sys['room_type'],
                    'logger_id' => $old_data_sys['logger_id'],
                    'gauge_id' => $old_data_sys['gauge_id'],
                    'filter_count' => $filter_count,
                    'filters_desc' => $filters_old_array,
                    'status' => ucfirst($old_data_sys['status'])
                ];
                $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
            }
            if ($status == 'true') {
                $post['status'] = "active";
            } else {
                $post['status'] = "inactive";
            }
            $audit_array['status'] = ucfirst($post['status']);
            $post['remark'] = $audit_array['remarks'] = $remark;
            $post['modified_on'] = date('Y-m-d H:i:s');
            $post['modified_by'] = $this->session->userdata('empname');
            $this->Ptscommon->updateToTable($db = 'default', 'pts_mst_sys_id', $post, $param);
            $res = $recordid;
            if ($res) {
                $audit_array = $this->Ptscommon->getNAUpdated($audit_array);
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "room_name:" . $room_name;
                $uniquefiled2 = "room_code:" . $room_code;
                $uniquefiled3 = "room_type:" . $room_type;
                unset($post['room_id']);
                $auditParams = array('command_type' => 'update', 'activity_name' => 'Room Configuration', 'type' => 'manage_room', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_sys_id', 'primary_id' => $recordid, 'post_params' => $audit_array, 'master_previous_data' => $master_previous_data);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */
                $this->db->delete("pts_mst_filter", array("room_code" => $room_code));
                if (!empty($filterArray)) {
                    $this->db->insert_batch("pts_mst_filter", $filterArray);
                }
                $responseresult = array(STATUS => TRUE, "result" => TRUE, "message" => "Room configuration data updated successfully");
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Room configuration data does not Updated');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $data = $this->db->get_where("pts_mst_sys_id", array("room_code" => $room_code, "area_id" => $area_id, "room_id" => $room_id))->row_array();
            if (!empty($data)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'This Room is Already assign to this terminal ' . $data['device_id']);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $post['created_on'] = date('Y-m-d H:i:s');
                $post['created_by'] = $this->session->userdata('empname');
                $res = $this->Ptscommon->addToTable($db = 'default', 'pts_mst_sys_id', $post, $param = '');
                if ($res) {
                    unset($post['room_id']);
                    $post['status'] = 'active';
                    $audit_array['status'] = ucfirst($post['status']);
                    $post['remark'] = $audit_array['remarks'] = $remark;
                    $audit_array = $this->Ptscommon->getNAUpdated($audit_array);
                    /* Start - Insert elog audit histry */
                    $uniquefiled1 = "room_name:" . $room_name;
                    $uniquefiled2 = "room_code:" . $room_code;
                    $uniquefiled3 = "room_type:" . $room_type;
                    $auditParams = ['command_type' => 'insert', 'activity_name' => 'Room Configuration', 'type' => 'manage_room', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_sys_id', 'primary_id' => $res, 'post_params' => $audit_array];
                    $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                    /* End -  Insert elog audit histry */
                    if (!empty($filterArray)) {
                        $this->db->delete("pts_mst_filter", array("room_code" => $room_code));
                        $this->db->insert_batch("pts_mst_filter", $filterArray);
                    }
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "message" => "Room configuration data saved successfully");
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Room configuration data does not Created');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }
        }
    }

    // End

    /**
     * Get Room List assigned to an ip
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getassignedroomlist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $system_id = isset($_GET['terminal_id']) ? $_GET['terminal_id'] : '';
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $module = isset($_GET['module']) ? $_GET['module'] : '';
        $user_room = isset($_GET['room_array']) ? $_GET['room_array'] : '';
        //$this->Apifunction->check_variables($system_id, "System id", $numricCheck = 'no');
        $param = array();
        if ($system_id != '') {
            $param = array("device_id" => $system_id, "status" => 'active');
        } else if ($room_code != '') {
            $param = array("room_code" => $room_code, "status" => 'active');
        } else {
            $param = array("status" => 'active');
        }
        if ($module == 'master') {
            $param = array();
        }
        if ($user_room != "") {
            $room_array = explode(",", $user_room);
            $param['room_array'] = $room_array;
        }
        //print_r($param);exit;
        $page = isset($_GET['page']) ? $_GET['page'] : null;
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = 0;
        if (!isset($param['room_array']) && empty($param['room_array'])) {
            $count = $this->Ptscommon->getRowsCount('default', 'pts_mst_sys_id', $param);
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_sys_id', $param, $order = null, $page);
        foreach ($res as $key => $value) {
            $filter = $this->Ptscommon->getMultipleRows('default', 'pts_mst_filter', array("room_code" => $value['room_code'], "status" => "on"));
            if (!empty($filter)) {
                $filterData = array();
                foreach ($filter as $key1 => $value1) {
                    $filterData[] = $value1['filter_name'];
                }
                $res[$key]['filter'] = implode(",", $filterData);
            } else {
                $res[$key]['filter'] = "";
            }
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "room_list" => $res, 'total_records' => $count, 'records_per_page' => $records_per_page);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Room does not found', "room_list" => array(), 'total_records' => 0, 'records_per_page' => $records_per_page);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End

    /**
     * Add Activity for rooms
     * @param string $method1
     * @param string $tokencheck
     */
    public function Addroomactivity() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $recordid = isset($_POST['recordid']) ? $_POST['recordid'] : '';
        $activity_name = isset($_POST['activity_name']) ? $_POST['activity_name'] : '';
        $activity_url = isset($_POST['activity_url']) ? $_POST['activity_url'] : '';
        $room_id = isset($_POST['room_id']) ? $_POST['room_id'] : '';
        $mst_act_id = isset($_POST['mst_act_id']) ? $_POST['mst_act_id'] : '';
        $this->Apifunction->check_variables($activity_name, "Activity Name", $numricCheck = 'no');
        $this->Apifunction->check_variables($activity_url, "Activity Url", $numricCheck = 'no');
        $this->Apifunction->check_variables($room_id, "Room Id", $numricCheck = 'no');
        $this->Apifunction->check_variables($mst_act_id, "Room Log Activity Id", $numricCheck = 'no');

        $param = array("activity_name" => $activity_name, "room_id" => $room_id, "activity_url" => $activity_url, "mst_act_id" => $mst_act_id, "status" => 'active');
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_trn_room_log_activity', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Room Activity Already Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
        $time = time();
        $post = array("activity_name" => $activity_name, "activity_url" => $activity_url, "room_id" => $room_id, "mst_act_id" => $mst_act_id);
        if ($recordid > 0) {
            $param = array("id" => $recordid);
            $post['modified_on'] = time();
            $post['modified_by'] = $this->session->userdata('empname');
            $this->Ptscommon->updateToTable($db = 'default', 'pts_trn_room_log_activity', $post, $param);
            $res = $recordid;
            if (!empty($res)) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "room_id:" . $room_id;
                $uniquefiled2 = "activity_name:" . $activity_name;
                $uniquefiled3 = "";
                $auditParams = array('command_type' => 'update', 'activity_name' => 'Room Log Mapping', 'type' => 'manage_room_activity', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_room_log_activity', 'primary_id' => $recordid, 'post_params' => $post);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Room Activity Updated');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Room Activity does not Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $post['created_date'] = time();
            $post['created_by'] = $this->session->userdata('empname');
            $res = $this->Ptscommon->addToTable($db = 'default', 'pts_trn_room_log_activity', $post, $param = '');
            if (!empty($res)) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "room_id:" . $room_id;
                $uniquefiled2 = "activity_name:" . $activity_name;
                $uniquefiled3 = "";
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Room Log Mapping', 'type' => 'manage_room_activity', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_room_log_activity', 'primary_id' => $res, 'post_params' => $post);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Room Activity Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Room Activity does not Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
    }

    public function Saveroomactivities() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $Roomactivities = json_decode($_POST['Roomactivities'], true);
        $roomid = $Roomactivities[0]["room_id"];
        $recordid = isset($_POST['recordid']) ? $_POST['recordid'] : '';
        $status = isset($_POST['status']) ? $_POST['status'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $roomData = $this->db->get_where("mst_room", array("id" => $roomid))->row_array();
        $roomName = !empty($roomData) ? $roomData['room_name'] : '';
        $roomCode = !empty($roomData) ? $roomData['room_code'] : '';
        if ($recordid > 0) {
            $this->db->where("workflownextstep>", '-1');
            $logData = $this->db->get_where("pts_trn_user_activity_log", array("room_code" => $roomData['room_code']))->result_array();
            $old_activities = [];
            if (!empty($logData) && $status == 'false') {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'You can not deactivate this.Some Logs are in progress.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
            if (!empty($Roomactivities)) {
                $newData = $oldData = [];
                $newActivityList = [];
                foreach ($Roomactivities as $key => $value) {
                    if ($status == 'true') {
                        $Roomactivities[$key]['status'] = "active";
                    } else {
                        $Roomactivities[$key]['status'] = "inactive";
                    }
                    $Roomactivities[$key]['activity_name'] = trim($Roomactivities[$key]['activity_name']);
                    $newActivityList[] = trim($Roomactivities[$key]['activity_name']);
                    $Roomactivities[$key]['activity_url'] = trim($Roomactivities[$key]['activity_url']);
                    $Roomactivities[$key]['created_date'] = time();
                    $Roomactivities[$key]['created_by'] = $this->session->userdata('empname');
                    $Roomactivities[$key]['remark'] = $remark;
                    $Roomactivities[$key]['modified_on'] = date('Y-m-d H:i:s');
                    $Roomactivities[$key]['modified_by'] = $this->session->userdata('empname');
                }
                $newActivityList = implode(',', $newActivityList);
                $newData = ['room_code' => $roomCode, 'room_name' => $roomName, 'activity_name' => $newActivityList, 'status' => ucfirst($Roomactivities[0]['status']), 'remarks' => $remark];
                $newData = $this->Ptscommon->getNAUpdated($newData);
                $this->db->where("room_id", $roomid);
                $oldArray = $this->db->get("pts_trn_room_log_activity")->result_array();
                if (!empty($oldArray)) {
                    foreach ($oldArray as $key => $value) {
                        $old_activities[] = $value['activity_name'];
                        $old_status = $value['status'];
                    }
                    $old_activities = implode(',', $old_activities);
                    $oldData = ['room_name' => $roomName, 'activity_name' => $old_activities, 'status' => ucfirst($old_status)];
                    $oldData = $this->Ptscommon->getNAUpdated($oldData);
                }
                $this->db->delete("pts_trn_room_log_activity", array("room_id" => $roomid));
                $res = $this->db->insert_batch('pts_trn_room_log_activity', $Roomactivities);
                if ($res) {
                    /* Start - Insert elog audit histry */
                    $uniquefiled1 = "room_id:" . $roomid;
                    $uniquefiled2 = "activity_name:" . "Room Log Mapping";
                    $uniquefiled3 = "";
                    $auditParams = array('command_type' => 'update', 'activity_name' => 'Room Log Mapping', 'type' => 'manage_room_activity', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_room_log_activity', 'primary_id' => $recordid, 'post_params' => $newData, 'master_previous_data' => $oldData);
                    $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                    /* End -  Insert elog audit histry */
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'data updated successfully');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'something went wrong.Plese try again.');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }
        } else {
            $param = array("room_id" => $roomid, "status" => "active");
            $res = $this->Ptscommon->getMultipleRows('default', 'pts_trn_room_log_activity', $param);
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Some activities already assigned for this room, please delete the previous once to assign again, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                if (!empty($Roomactivities)) {
                    $this->db->delete("pts_trn_room_log_activity", array("room_id" => $roomid));
                    foreach ($Roomactivities as $key => $value) {
                        $Roomactivities[$key]['activity_name'] = trim($Roomactivities[$key]['activity_name']);
                        $Roomactivities[$key]['activity_url'] = trim($Roomactivities[$key]['activity_url']);
                        $Roomactivities[$key]['created_date'] = time();
                        $Roomactivities[$key]['created_by'] = $this->session->userdata('empname');
                    }
                    $res = $this->db->insert_batch('pts_trn_room_log_activity', $Roomactivities);
                    if ($res) {
                        $activity_name = '';
                        foreach ($Roomactivities as $key => $value) {
                            $activity_name .= $Roomactivities[$key]['activity_name'] . ',';
                            unset($Roomactivities[$key]['activity_url']);
                            unset($Roomactivities[$key]['created_date']);
                            unset($Roomactivities[$key]['created_by']);
                        }
                        $activity_name = rtrim($activity_name, ',');
                        $newData = ['room_code' => $roomCode, 'room_name' => $roomName, 'activity_name' => $activity_name, 'status' => 'Active', 'remarks' => $remark];
                        /* Start - Insert elog audit histry */
                        $newData = $this->Ptscommon->getNAUpdated($newData);
                        $uniquefiled1 = "room_id:" . $roomid;
                        $uniquefiled2 = "activity_name:Room Log Mapping";
                        $uniquefiled3 = "";
                        $auditParams = array('command_type' => 'insert', 'activity_name' => 'Room Log Mapping', 'type' => 'manage_room_activity', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_room_log_activity', 'primary_id' => $res, 'post_params' => $newData);
                        $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                        /* End -  Insert elog audit histry */
                        $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'data saved successfully');
                        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    } else {
                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'something went wrong.Plese try again.');
                        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    }
                }
            }
        }
    }

    // End

    /**
     * Get a list of Activity for provided room
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getactivitylistforroom() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $room_id = isset($_GET['room_id']) ? $_GET['room_id'] : '';
//        $this->Apifunction->check_variables($room_id, "Room Id", $numricCheck = 'no');
        $param = array("status" => 'active');
        if ($room_id != '') {
            $param = array("room_id" => $room_id, "status" => 'active');
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_trn_room_log_activity', $param);
        foreach ($res as $key => $value) {
            $roomData = $this->db->get_where("mst_room", array("id" => $value['room_id']))->row_array();
            $res[$key]['room_code'] = $roomData['room_code'];
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "activity_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found For Provided Room', "activity_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function Getactivitylistforroomwithgroupby() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $room_id = isset($_GET['room_id']) ? $_GET['room_id'] : '';
        $module = isset($_GET['module']) ? $_GET['module'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : '';
//        $this->Apifunction->check_variables($room_id, "Room Id", $numricCheck = 'no');
        $param = array("status" => 'active');
        if ($room_id != '') {
            $param = array("room_id" => $room_id, "status" => 'active');
        }
        if ($module == 'master') {
            $param = array();
        }
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = $this->Ptscommon->getMultipleRowswithgroupbyCount('default', 'pts_trn_room_log_activity', $param, "room_id");
        $res = $this->Ptscommon->getMultipleRowswithgroupby('default', 'pts_trn_room_log_activity', $param, "room_id", $page);
        //print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "activity_list" => $res, 'total_records' => $count, 'records_per_page' => $records_per_page);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found For Provided Room', "activity_list" => array(), 'total_records' => 0, 'records_per_page' => $records_per_page);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function Getactivitylistofselectedroom() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_id = isset($_POST['room_id']) ? $_POST['room_id'] : '';
        $module = isset($_GET['module']) ? $_GET['module'] : '';
        if ($module == 'master') {
            $param = array("room_id" => $room_id);
        } else {
            $param = array("room_id" => $room_id, "status" => 'active');
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_trn_room_log_activity', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "log_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found For Provided Room', "log_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function GetWorkAssignRoleListofselectedrole() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $param = array("roleid" => $roleid);
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_role_workassign_to_mult_roles', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "role_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found For Provided Room', "role_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End

    /**
     * Get a list of Fixed Equipment
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getfixedequiplist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $room_id = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        $in_line = isset($_GET['in_line']) ? $_GET['in_line'] : '';
        $this->Apifunction->check_variables($room_id, "Room Code", $numricCheck = 'no');
        if ($type != '') {
            $param = array("room_code" => $room_id, "is_inused" => 0, "is_active" => 1, "equipment_type" => "Line");
        } else {
            $param = array("room_code" => $room_id, "is_inused" => 0, "is_active" => 1, "equipment_type" => "Fixed");
        }
        if ($in_line != "") {
            $param['is_in_line'] = $in_line;
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "fixed_euip_list" => $res);
            //print_r($responseresult);exit;
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Fixed Equipment Found', "fixed_euip_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }
	
	/**
     * Get a list of Fixed Equipment
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getfixedequiplistnew() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $room_id = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        $in_line = isset($_GET['in_line']) ? $_GET['in_line'] : '';
        $this->Apifunction->check_variables($room_id, "Room Code", $numricCheck = 'no');
        if ($type != '') {
            $param = array("room_code" => $room_id, "is_inused" => 0, "is_active" => 1, "equipment_type" => "Line");
			$res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        } else {
            $param = array("room_code" => $room_id, "is_inused" => 0, "is_active" => 1);
			$res = $this->Ptscommon->getMultipleRowsNewCreate('default', 'mst_equipment', $param);
        }
        if ($in_line != "") {
            $param['is_in_line'] = $in_line;
			$res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        }
        
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "fixed_euip_list" => $res);
            //print_r($responseresult);exit;
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Fixed Equipment Found', "fixed_euip_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get a list of Fixed Equipment
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getfixedequiplistforreport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $room_id = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        $this->Apifunction->check_variables($room_id, "Room Code", $numricCheck = 'no');
        if ($type != '') {
            $param = array("room_code" => $room_id, "is_active" => 1, "equipment_type" => "Line");
        } else {
            $param = array("room_code" => $room_id, "is_active" => 1, "equipment_type" => "Fixed");
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "fixed_euip_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Fixed Equipment Found', "fixed_euip_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

	/**
     * Get a list of Fixed Equipment
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getfixedequiplistforreportnew() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $room_id = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        $this->Apifunction->check_variables($room_id, "Room Code", $numricCheck = 'no');
        if ($type != '') {
            $param = array("room_code" => $room_id, "is_active" => 1, "equipment_type" => "Line");
			$res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        } else {
            $param = array("room_code" => $room_id, "is_active" => 1);
			$res = $this->Ptscommon->getMultipleRowsNewCreate('default', 'mst_equipment', $param);
        }
        
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "fixed_euip_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Fixed Equipment Found', "fixed_euip_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }
    // End

    /**
     * Get a list of sampling rodID 
     * @param string $method1
     * @param string $tokencheck
     */
    public function GetSampleRodIDList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => '1', "equipment_type" => "Samplingrod");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_instrument', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "SamplingRod_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No record found', "SamplingRod_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End

    /**
     * Get a list of Portable Equipment
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getportableequiplist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $room_id = isset($_GET['room_code']) ? $_GET['room_code'] : '';
//        $this->Apifunction->check_variables($room_id, "Room Code", $numricCheck = 'no');
        $param = array("is_inused" => 0, "is_active" => 1, "is_inused" => 0, "equipment_type" => "Portable");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "potable_euip_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Portable Equipment Found', "potable_euip_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get a list of Portable Equipment
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getportableequiplistforreport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $room_id = isset($_GET['room_code']) ? $_GET['room_code'] : '';
//        $this->Apifunction->check_variables($room_id, "Room Code", $numricCheck = 'no');
        $param = array("is_active" => 1, "equipment_type" => "Portable");
        if ($room_id != "") {
            $param['room_code'] = $room_id;
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "potable_euip_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Portable Equipment Found', "potable_euip_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End

    /**
     * Get a list of Portable Equipment
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getmasteractivitylist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $activity_type = isset($_GET['activity_type']) ? $_GET['activity_type'] : '';
        if (!empty($activity_type)) {
            $param = array("type" => $activity_type, "status" => "active");
        } else {
            $param = '';
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_activity', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "master_activity_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Master Activity Found', "master_activity_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function GetmstHeaderData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $logid = isset($_GET['logid']) ? $_GET['logid'] : '';
        $res = $this->Ptscommon->GetmstHeaderData($logid);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "HeaderData" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No HeaderData Found', "HeaderData" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get a list of Portable Equipment
     * @param string $method1
     * @param string $tokencheck
     */
    public function GetVerticalmasteractivitylist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("field_name" => 'Activity', "status" => "active");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_stages', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "master_activity_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Master Activity Found', "master_activity_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Inprogress Activity List
     * Created By :Rahul Chauhan
     * Date:27-01-2020
     */
    function GetActivityApprovalLevel($activity_id) {
        $res = $this->db->get_where("pts_trn_workflowsteps", array("activity_id" => $activity_id))->result_array();
        return $res;
    }

    public function GetInprogressActivityList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $system_id = isset($_GET['terminal_id']) ? $_GET['terminal_id'] : '';
        $res = $this->Ptscommon->GetInprogressActivityList($system_id);
//        echo '<pre>';print_r($res);exit;
        foreach ($res as $key => $value) {
            //print_r($res);exit;
            $res[$key]['activity_level'] = $this->GetActivityApprovalLevel($value['activity_id']);
            $res[$key]['activity_url'] = $this->GetActivityUrl($value['processid'])['activity_url'];
            $res[$key]['mst_act_id'] = $this->GetActivityUrl($value['processid'])['mst_act_id'];
            $res[$key]['last_approval'] = $this->GetLastApproval(array("id" => $value['act_id'], "room_code" => $value['room_code'], "roomlogactivity" => $value['processid'], "activity_id" => $value['activity_id']));
            $res[$key]['attachment_list'] = $this->GetAttachmentList(array("id" => $value['act_id'], "room_code" => $value['room_code'], "roomlogactivity" => $value['processid'], "activity_id" => $this->GetActivityUrl($value['processid'])['mst_act_id']));
        }
        if (!empty($res)) {
            //print_r($res);exit;
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "in_progress_activity" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'NoMasterActivityFound', "in_progress_activity" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function GetActivityUrl($process_id) {
        $res = $this->db->get_where("pts_trn_room_log_activity", array("mst_act_id" => $process_id))->row_array();
        if (!empty($res)) {
            return $res;
        } else {
            return null;
        }
    }

    /**

      /**
     * Get QA Approval Activity List
     * Created By :Rahul Chauhan
     * Date:27-01-2020
     */
    public function GetQaApprovalActivityList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $system_id = isset($_GET['terminal_id']) ? $_GET['terminal_id'] : '';
        $res = $this->Ptscommon->GetQaApprovalActivityList($system_id);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "qa_approval_activity" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Master Activity Found', "qa_approval_activity" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Product List
     * Created By :Rahul Chauhan
     * Date:28-01-2020
     */
    public function GetProductList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("is_active" => 1);
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_product', $param);
        //$newArray[] = array("product_code" => "NA", "product_name" => "NA");
        //$res = array_merge($newArray, $res);
//        $res = $newArray + $res;
//        echo '<pre>';print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "product_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Product Found', "product_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Reason List
     * Created By : Bhupendra kumar
     * Date: 15-03-2020
     */
    public function GetReasonData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => 'active');
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_stop_reason', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "ReasonData" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Product Found', "ReasonData" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Batch List
     * Created By : Bhupendra kumar
     * Date:21-02-2020
     */
    public function GetBatchList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("is_active" => 1);
        $res = $this->Ptscommon->getMultipleRows('default', 'trn_batch', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "batch_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Product Found', "batch_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Product Detail based on batch code
     * Created By : Bhupendra kumar
     * Date:21-02-2020
     */
    public function GetProductDetail() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $batch = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $param = array("is_active" => 1, "batch_code" => $batch);
        $res = $this->Ptscommon->getMultipleRows('default', 'trn_batch', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "product_detail" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Product Found', "product_detail" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Frequency List
     * Created By :Rahul Chauhan
     * Date:29-01-2020
     */
    public function GetFrequencyList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "active");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_frequency', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "frequency_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Frequency Found', "frequency_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Roomwise Filter List
     * Created By :Rahul Chauhan
     * Date:29-01-2020
     */
    public function GetRoomWiseFilterList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        if ($room_code != '') {
            $param = array("status" => "on", "room_code" => $room_code);
        } else {
            $param = array("status" => "on");
        }


        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_filter', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "filter_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Filter Found', "filter_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Bhupendra's code as below:-

    /**
     * User login
     */
    public function Login_backup() {
        $headers = apache_request_headers(); // fetch header value
        $userid = isset($_POST['email']) ? $_POST['email'] : '';
        $pwd = isset($_POST['password']) ? $_POST['password'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $module_id = isset($_POST['module_id']) ? $_POST['module_id'] : '';
        $module_type = isset($_POST['module_type']) ? $_POST['module_type'] : '';
        $action = isset($_POST['action']) ? $_POST['action'] : '';
        $act_type = isset($_POST['act_type']) ? $_POST['act_type'] : '';
        $room_code = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $password = isset($_POST['auth']) ? $_POST['auth'] : '';
        if ($action == 'submit') {
            $action = 'start';
        }
        $userData = $this->db->get_where("mst_employee", array("emp_email" => $userid, "is_active" => 1, "is_blocked" => 0))->row_array();
        if ($action == 'roomin' && $userData['is_generate'] == '1') {
            if ($userData['is_first'] == '1') {
                $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Fail due to reset password', $userid, 'N/A', 'N/A');
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'password', "userdata" => $userData);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
        if ($userData['is_directory'] == '1') {
            $auth = base64_decode(base64_decode($password));
            $userExist = file_get_contents(URL_BASE_TEST_AD ."username=$userid&password=$auth");
            $userCheck = json_decode($userExist, true);
            if (!empty($userCheck) && $userCheck['status'] == false) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'The user does not exist in Active Directory, Authentication Failure. Please provide the correct user name and password.', "userdata" => array());
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
        $auth = base64_decode(base64_decode($password));
//        if ($module_id == 16) {
//            $userExist = file_get_contents("https://smhsdev.motherson.com/testAD.php?username=$userid&password=$auth");
//            $userCheck = json_decode($userExist, true);
//            if (!empty($userCheck) && $userCheck['status'] == false) {
//                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'The user does not exist in Active Directory, Authentication Failure. Please provide the correct user name and password.', "userdata" => array());
//                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
//            } else {
//                $this->db->where("emp_email", $userid);
//                $this->db->update("mst_employee", array("emp_password" => $pwd));
//            }
//        }
        if ($userData['is_generate'] == '1') {
            $res = $this->Ptsadminmodel->userauth($userid, $pwd);
        } else {
            $res = $this->Ptsadminmodel->userauthforAD($userid);
        }
        if (!empty($res)) {
            if ($res['is_blocked'] == 1) {
                $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Fail due to blocked user', $userid, 'N/A', 'N/A');
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Your account has been blocked, please contact the administrator.', "userdata" => array());
            } else {
                $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Success', $userid, 'N/A', 'N/A');

                if ($action == 'batchout' || $action == 'roomin' || $action == 'roomout' || $action == 'batchin') {

                    $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => '', "userdata" => $res);
                } else {
                    //$responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => '', "userdata" => $res);
                    //Code for user privilege data
                    $prilivege_data = $this->checkUserPrivelege($res['id'], $module_id, $module_type, $action, $room_code);
                    if (!empty($prilivege_data)) {
                        if ($module_id == 16 || $module_id == 17 || $module_id == 30) {

                            if (in_array($act_type, explode(",", $prilivege_data['type_id']))) {

                                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => '', "userdata" => $res);
                            } else {
                                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'You have no Role and Responsibility to ' . $action . ' this Activity', "userdata" => $res);
                            }
                        } else {
                            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => '', "userdata" => $res);
                        }
                    } else {
                        $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'You have no rights to ' . $action . ' this activity, please contact to the administrator', "userdata" => $res);
                    }
                }
            }
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $userexist = $this->Ptsadminmodel->userexist($userid);
            if (!empty($userexist)) {
                if ($userexist['is_blocked'] == 1) {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Your account has been blocked, please contact the administrator.', "userdata" => array());
                    $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Fail due to blocked user', $userid, 'N/A', 'N/A');
                } else {
                    $attampt = $userexist['login_attampt'] + 1;
                    if ($attampt == 5) {
                        $params = array('login_attampt' => 0);
                        $userexist = $this->Ptsadminmodel->updateuser('mst_employee', $params, 'emp_email', $userid);
                    } else {
                        $params = array('login_attampt' => $attampt);
                        $userexist = $this->Ptsadminmodel->updateuser('mst_employee', $params, 'emp_email', $userid);
                    }

                    if ($attampt == 5) {
                        $params = array('is_blocked' => 1, 'block_datetime' => date('Y-m-d H:i:s'), 'login_attampt' => 0);
                        $userexist = $this->Ptsadminmodel->updateuser('mst_employee', $params, 'emp_email', $userid);
                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Your account has been blocked, please contact the administrator.', "userdata" => array());
                        $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Fail due to blocked user', $userid, 'N/A', 'N/A');
                    } else {
                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Credential not match', "userdata" => array());
                        $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Fail due to wrong password which is ' . $pwd, $userid, 'N/A', 'N/A');
                    }
                }
            } else {
                $getdata = $this->Ptsadminmodel->userauthBypwd($pwd);
                if (!empty($getdata)) {
                    $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Fail due to wrong userid which is ' . $userid, $userid, 'N/A', 'N/A');
                } else {
                    $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Fail due to wrong password which is ' . $pwd, $userid, 'N/A', 'N/A');
                }
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Credential not match', "userdata" => array());
            }

            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function Login() {
        $headers = apache_request_headers(); // fetch header value
        $userid = isset($_POST['email']) ? $_POST['email'] : '';
        $pwd = isset($_POST['password']) ? $_POST['password'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $module_id = isset($_POST['module_id']) ? $_POST['module_id'] : '';
        $module_type = isset($_POST['module_type']) ? $_POST['module_type'] : '';
        $action = isset($_POST['action']) ? $_POST['action'] : '';
        $act_type = isset($_POST['act_type']) ? $_POST['act_type'] : '';
        $room_code = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $password = isset($_POST['auth']) ? $_POST['auth'] : '';
        if ($action == 'submit') {
            $action = 'start';
        }
        $act_id = isset($_POST['act_id']) ? $_POST['act_id'] : '';
        if ($act_id == 1 || $act_id == 2 || $act_id == 3 || $act_id == 4 || $act_id == 5 || $act_id == 6 || $act_id == 7) {
            $workflowtype = 'start';
        } else {
            $workflowtype = 'stop';
        }
        $userData = $this->db->get_where("mst_employee", array("emp_email" => $userid, "is_active" => 1))->row_array();
        $empid = $userData['id'];
        $empname = $userData['emp_name'];
        if (!empty($userData) && $userData['is_blocked'] == 0) {
            $this->db->where("emp_email", $userid);
            $this->db->update("mst_employee", array("login_attampt" => 0));
            $checkApproval = $this->db->get_where("pts_mst_activity", array("id" => $act_id, "status" => "active", "is_need_approval" => 'yes'))->row_array();
            if (!empty($checkApproval)) {
                $checkWorkflow = $this->db->get_where("pts_trn_workflowsteps", array("activity_id" => $act_id, "is_active" => "1", "workflowtype" => $workflowtype))->result_array();
                if (empty($checkWorkflow)) {
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Selected Activity has no workflow. Please create a workflow for this Activity.', "userdata" => $userData);
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }
            if ($action == 'roomin' && $userData['is_generate'] == '1') {
                if ($userData['is_first'] == '1') {
                    $auditParams = array('command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'User Login', 'type' => 'user_login', 'uniquefiled1' => 'id:' . $empid, 'uniquefiled2' => '', 'uniquefiled3' => '', 'table_name' => 'mst_employee', 'primary_id' => 0, 'log_user_activity_id' => 0, 'post_params' => ['reason' => 'Login failed due to reset password for user id : ' . $userData['emp_email']]);
                    $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                    $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Fail due to reset password', $userid, 'N/A', 'N/A');
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'password', "userdata" => $userData);
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
                $room_list = explode(',', $userData['room_code']);
                if (!in_array($room_code, $room_list)) {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You are not authorised to login room "' . $room_code . '"', "userdata" => $userData);
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }
            if ($userData['is_directory'] == '1') {
                $auth = base64_decode(base64_decode($password));
                $userExist = file_get_contents(URL_BASE . "/testAD.php?username=$userid&password=$auth");
                $userCheck = json_decode($userExist, true);
                if (!empty($userCheck) && $userCheck['status'] == false) {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'The user does not exist in Active Directory, Authentication Failure. Please provide the correct user name and password.', "userdata" => array());
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }
            $auth = base64_decode(base64_decode($password));
            //        if ($module_id == 16) {
            //            $userExist = file_get_contents("https://smhsdev.motherson.com/testAD.php?username=$userid&password=$auth");
            //            $userCheck = json_decode($userExist, true);
            //            if (!empty($userCheck) && $userCheck['status'] == false) {
            //                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'The user does not exist in Active Directory, Authentication Failure. Please provide the correct user name and password.', "userdata" => array());
            //                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            //            } else {
            //                $this->db->where("emp_email", $userid);
            //                $this->db->update("mst_employee", array("emp_password" => $pwd));
            //            }
            //        }
            if ($userData['is_generate'] == '1') {
                $res = $this->Ptsadminmodel->userauth($userid, $pwd);
            } else {
                $res = $this->Ptsadminmodel->userauthforAD($userid);
            }
            if (!empty($res)) {
                if ($res['is_blocked'] == 1) {
                    $auditParams = array('command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'User Login', 'type' => 'user_login', 'uniquefiled1' => 'id:' . $empid, 'uniquefiled2' => '', 'uniquefiled3' => '', 'table_name' => 'mst_employee', 'primary_id' => 0, 'log_user_activity_id' => 0, 'post_params' => ['reason' => 'Account blocked For user id : ' . $userData['emp_email']]);
                    $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                    $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Fail due to blocked user', $userid, 'N/A', 'N/A');
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Your account has been blocked, please contact the administrator.', "userdata" => array());
                } else {
                    $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Success', $userid, 'N/A', 'N/A');
                    if(!in_array($action,['roomin','roomout'])){
                        $auditParams = array('command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'User Login', 'type' => 'user_login', 'uniquefiled1' => 'id:' . $empid, 'uniquefiled2' => '', 'uniquefiled3' => '', 'table_name' => 'mst_employee', 'primary_id' => 0, 'log_user_activity_id' => 0, 'post_params' => ['reason' => 'Login successfully for user id : ' . $userData['emp_email']]);
                        $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                    }
                    if ($action == 'batchout' || $action == 'roomin' || $action == 'roomout' || $action == 'batchin') {

                        $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => '', "userdata" => $res);
                    } else {
                        //$responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => '', "userdata" => $res);
                        //Code for user privilege data
                        $prilivege_data = $this->checkUserPrivelege($res['id'], $module_id, $module_type, $action, $room_code);
//                        print_r($prilivege_data);exit;
                        if (!empty($prilivege_data)) {
                            if ($module_id == 16 || $module_id == 17 || $module_id == 30) {



                                if (in_array($act_type, explode(",", $prilivege_data['type_id']))) {



                                    $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => '', "userdata" => $res);
                                } else {
                                    $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'You have no Role and Responsibility to ' . $action . ' this Activity', "userdata" => $res);
                                }
                            } else {
                                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => '', "userdata" => $res);
                            }
                        } else {
                            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'You have no rights to ' . $action . ' this activity, please contact to the administrator', "userdata" => $res);
                        }
                    }
                }
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $userexist = $this->Ptsadminmodel->userexist($userid);
                if (!empty($userexist)) {
                    if ($userexist['is_blocked'] == 1) {
                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Your account has been blocked, please contact the administrator.', "userdata" => array());
                        $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Fail due to blocked user', $userid, 'N/A', 'N/A');
                        $auditParams = array('command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'User Login', 'type' => 'user_login', 'uniquefiled1' => 'id:' . $empid, 'uniquefiled2' => '', 'uniquefiled3' => '', 'table_name' => 'mst_employee', 'primary_id' => 0, 'log_user_activity_id' => 0, 'post_params' => ['reason' => 'Login failed due to blocked account for user id : ' . $userData['emp_email']]);
                        $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                    } else {
                        $attampt = $userexist['login_attampt'] + 1;
                        if ($attampt == 5) {
                            $params = array('login_attampt' => 0);
                            $auditParams = array('command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'User Login', 'type' => 'user_login', 'uniquefiled1' => 'id:' . $empid, 'uniquefiled2' => '', 'uniquefiled3' => '', 'table_name' => 'mst_employee', 'primary_id' => 0, 'log_user_activity_id' => 0, 'post_params' => ['reason' => 'Account blocked due to 5 attempts with wrong password for user id ' . $userData['emp_email']]);
                            $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                            $userexist = $this->Ptsadminmodel->updateuser('mst_employee', $params, 'emp_email', $userid);
                        } else {
                            $params = array('login_attampt' => $attampt);
                            $auditParams = array('command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'User Login', 'type' => 'user_login', 'uniquefiled1' => 'id:' . $empid, 'uniquefiled2' => '', 'uniquefiled3' => '', 'table_name' => 'mst_employee', 'primary_id' => 0, 'log_user_activity_id' => 0, 'post_params' => ['reason' => 'Login fauiled due to wrong password for user id : ' . $userData['emp_email'] . ' attempt : .' . $attampt]);
                            $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                            $userexist = $this->Ptsadminmodel->updateuser('mst_employee', $params, 'emp_email', $userid);
                        }



                        if ($attampt == 5) {
                            $params = array('is_blocked' => 1);
                            $userexist = $this->Ptsadminmodel->updateuser('mst_employee', $params, 'emp_email', $userid);
                            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Your account has been blocked, please contact the administrator.', "userdata" => array());
                            $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Fail due to blocked user', $userid, 'N/A', 'N/A');
                        } else {
                            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Credential not match', "userdata" => array());
                            $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Fail due to wrong password which is ' . $pwd, $userid, 'N/A', 'N/A');
                        }
                    }
                } else {
                    $getdata = $this->Ptsadminmodel->userauthBypwd($pwd);
                    if (!empty($getdata)) {
                        $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Fail due to wrong userid which is ' . $userid, $userid, 'N/A', 'N/A');
                    } else {
                        $this->AuditLog('N/A', 'User Login', 'N/A', 'Login Fail due to wrong password which is ' . $pwd, $userid, 'N/A', 'N/A');
                    }
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Credential not match', "userdata" => array());
                }



                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } elseif (!empty($userData) && $userData['is_blocked'] > 0) {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'User account is block', "userdata" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'The user does not exist. Please provide the correct user name and password.', "userdata" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    /**
     * Get Incremented document number from Doc table.
     */
    public function getdocno() {
        $doc_res = $this->Ptsadminmodel->get_doc3('id', 13, 'pts_mst_document');
        $doc_row = $doc_res->row_array();

        $doc_pre = $doc_row['prefix'];
        $doc_no = $doc_row['docno'] + 1;
        $docno = $doc_pre . $doc_no;

        $arr2 = array("docno" => $doc_no);
        $this->db->where("id", 13);
        if ($this->db->update("pts_mst_document", $arr2)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => '', "docno" => $docno);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Doc not found', "docno" => '');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * **************************************** End **************************** */

    /**
     * Get Approved in progress activities
     */
    public function getapproved() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $actlogtblid = isset($_POST['actlogtblid']) ? $_POST['actlogtblid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $workflowstatus = isset($_POST['workflowstatus']) ? $_POST['workflowstatus'] : '';
        $workflownextstep = isset($_POST['workflownextstep']) ? $_POST['workflownextstep'] : '';
        $actstatus = isset($_POST['actstatus']) ? $_POST['actstatus'] : '';
        $curr_url = isset($_GET['curr_url']) ? $_GET['curr_url'] : '';
        //Check is the person room in before perform an operation 
        //if ($curr_url != 'inprogress_activity') {
        $isroomin = 0;

        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
//            echo '<pre>';print_r($chkroomres);exit;
        //To Get Role Id using Role Level

        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;

            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $res = $this->Ptsadminmodel->getnext_step2($workflownextstep, $actid, $actstatus);

        if ($res->num_rows() > 0) {
            $workflowdata = $res->row_array();

            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";
            $btnval = 'off';

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
//            echo '<pre>';print_r($res2->result());exit;
            $getbtntext = $res2->result();
            //$btnval=$getbtntext[0]->prty_btn_text;
            if ($btnval == 'off') {

                foreach ($res2->result() as $r2) {

                    if (($r2->workassign_to_roleid == $roleid)) {
                        //getting Last trn record to match credential with previous one...
                        $lastrecord = $this->Ptsadminmodel->get_lastrecord('workflow_type', $actstatus, 'usr_activity_log_id', $actlogtblid, 'pts_trn_activity_approval_log');

                        if ($lastrecord == "") {
                            $candone = 1;
                        } else {
                            //checking is current user had worked on this activity?
                            $iscur_user_wrk = 0;
                            foreach ($lastrecord->result() as $Lastrec) {
                                if ($Lastrec->user_id == $empid) {
                                    $iscur_user_wrk = 1;
                                    break;
                                }
                            }
                            if ($iscur_user_wrk == 1) {
                                $candone = 0;
                            } else {
                                $candone = 1;
                            }
                        }
                        break;
                    } else {
                        $candone = 0;
                    }
                }
            }
            if ($candone == 1 && $isroomin == 1) {
                $result = $this->Ptscommon->Senttonextapproval($actlogtblid, $actid, $roleid, $remark, $status_id, $next_step, $empid, $empname, $email, $actstatus);
                if ($result == TRUE) {
                    if ($next_step > 0) {
                        $this->commonEmailFunction($actlogtblid);
                    }
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Approved Successfully', 'next_step' => $next_step);
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to Approved this record');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to Approved this record');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    /**
     * Get Approved in progress activities
     */
    public function com_approval_fun() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $actlogtblid = isset($_POST['actlogtblid']) ? $_POST['actlogtblid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $workflowstatus = isset($_POST['workflowstatus']) ? $_POST['workflowstatus'] : '';
        $workflownextstep = isset($_POST['workflownextstep']) ? $_POST['workflownextstep'] : '';
        $actstatus = isset($_POST['actstatus']) ? $_POST['actstatus'] : '';
        $result_rgt = isset($_POST['result_rgt']) ? $_POST['result_rgt'] : '';
        $result_lft = isset($_POST['result_lft']) ? $_POST['result_lft'] : '';
        $docno = isset($_POST['docno']) ? $_POST['docno'] : '';
        $tblname = isset($_POST['tblname']) ? $_POST['tblname'] : '';
        $pre_doc = isset($_GET['pre_doc']) ? $_GET['pre_doc'] : '';
        $material_condition = isset($_POST['material_condition']) ? $_POST['material_condition'] : '';
//        echo $tblname;exit;
        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        $res = $this->Ptsadminmodel->getnext_step2($workflownextstep, $actid, "stop");
        if ($res->num_rows() > 0) {
            $workflowdata = $res->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";
            $btnval = 'off';

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            $getbtntext = $res2->result();

            if ($btnval == 'off') {
                foreach ($res2->result() as $r2) {
                    if (($r2->workassign_to_roleid == $roleid)) {
                        $lastrecord = $this->Ptsadminmodel->get_lastrecord('workflow_type', $actstatus, 'usr_activity_log_id', $actlogtblid, 'pts_trn_activity_approval_log');
                        //echo "</pre>";print_r($lastrecord->result());exit;
                        if ($lastrecord == "") {
                            $candone = 1;
                        } else {
                            //checking is current user had worked on this activity?
                            $iscur_user_wrk = 0;
                            foreach ($lastrecord->result() as $Lastrec) {
                                if ($Lastrec->user_id == $empid) {
                                    $iscur_user_wrk = 1;
                                    break;
                                }
                            }
                            if ($iscur_user_wrk == 1) {
                                $candone = 0;
                            } else {
                                $candone = 1;
                            }
                        }
                        break;
                    } else {
                        $candone = 0;
                    }
                }
            }
            if ($candone == 1 && $isroomin == 1) {
                $result = $this->Ptscommon->Sentfornextapproval($actlogtblid, $actid, $roleid, $remark, $status_id, $next_step, $empid, $empname, $email, $actstatus, $docno, $tblname, $result_rgt, $result_lft);
                if ($result == TRUE) {
                    if ($next_step > 0) {
                        $this->commonEmailFunction($actlogtblid);
                    } else {
                        if ($tblname == 'pts_trn_pre_filter_cleaning' && $pre_doc != "") {
                            $this->db->where_in("doc_no", array($pre_doc, $docno));
                            $this->db->update("pts_trn_pre_filter_cleaning", array("is_to" => "0"));
                        }
                        if ($tblname == 'pts_trn_material_retreival_relocation' && $material_condition == "out") {
                            $this->db->where_in("doc_no", $docno);
                            $this->db->update("pts_trn_material_retreival_relocation", array("in_status" => "1"));
                        }
                    }
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Approved Successfully', 'next_step' => $next_step);
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to Approved this record.');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to Approved this record.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    /**
     * Get QA Approve activities
     */
    public function getQAapproved() {
        $headers = apache_request_headers(); // fetch header value
        $actlogtblid = isset($_POST['actlogtblid']) ? $_POST['actlogtblid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';

        $role_description = $this->db->get_where("mst_role", ["id" => $roleid])->row_array();
        $role_name = !empty($role_description) ? $role_description['role_description'] : '';
        if ($role_name != 'IPQA/QC') {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Only IPQA/QC Is Authorised To Approve');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
        $data = [];
        if (!empty($actlogtblid)) {
            $data = $this->Ptscommon->GetQaApprovalActivityDataById($actlogtblid);
        }
        $uniquefiled1 = "doc_no:" . !empty($data) ? $data['doc_id'] : '';
        $uniquefiled2 = "equipment_id:" . !empty($data) ? $data['equip_code'] : '';
        $uniquefiled3 = "pre_barch_no:" . !empty($data) ? $data['batch_no'] : '';
        $result = $this->Ptscommon->SenttoQAapproval($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email);
        if ($result == TRUE) {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Approved Successfully');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    /**
     * Get Stop in progress activities
     */
    public function getstoped() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $actlogtblid = isset($_POST['actlogtblid']) ? $_POST['actlogtblid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $mst_act_id = isset($_POST['mst_act_id']) ? $_POST['mst_act_id'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $stopreason = isset($_POST['stopreason']) ? $_POST['stopreason'] : '';
        $curr_url = isset($_GET['curr_url']) ? $_GET['curr_url'] : '';
        //Check is the person room in before perform an operation 
        //if ($curr_url != 'inprogress_activity') {
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        //Getting Next step of workflow of current Activity
//        echo $actid;exit;
        $res = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($res->num_rows() > 0) {
            $workflowdata = $res->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = -1;
        }

        if ($candone == 1 && $isroomin == 1) {
            $result = $this->Ptscommon->Senttostop($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email, $status_id, $next_step, $stopreason);
            if ($result == TRUE) {
                if ($next_step > 0) {
                    $this->commonEmailFunction($actlogtblid);
                }
                //Making equipment in unused

                $res = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array("id" => $actlogtblid));
                if (!empty($res)) {
                    $logid = $res[0]['roomlogactivity'];
                    $sampling_rod_id = $res[0]['sampling_rod_id'];
                    $equipmentcode = $res[0]['equip_code'];
                    if ($mst_act_id == 30) {
                        $equipArray = array();
                        if ($equipmentcode != '' && $equipmentcode != 'NA') {
                            $equipArray = explode(',', $equipmentcode);
                            if (!empty($equipArray)) {
                                foreach ($equipArray as $equip) {
                                    $post = array('is_inused' => 0);
                                    $param = array("line_log_code" => $equip);
                                    $this->Ptscommon->updateToTable($db = 'default', 'pts_mst_line_log', $post, $param);
                                }
                            }
                        }
                    } else {
                        if ($sampling_rod_id != '' && $sampling_rod_id != NULL) {
                            $post = array('is_inused' => 0);
                            $param = array("equipment_code" => $sampling_rod_id);
                            $this->Ptscommon->updateToTable($db = 'default', 'pts_mst_instrument', $post, $param);
                        } else {
                            $equipArray = array();
                            if ($equipmentcode != '' && $equipmentcode != 'NA') {
                                $equipArray = explode(',', $equipmentcode);
                                if (!empty($equipArray)) {
                                    foreach ($equipArray as $equip) {
                                        $post = array('is_inused' => 0);
                                        $param = array("equipment_code" => $equip);
                                        $this->Ptscommon->updateToTable($db = 'default', 'mst_equipment', $post, $param);
                                    }
                                }
                            }
                        }
                    }
                }
                //End

                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Stopped Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to stop this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    /**
     * Get Stop in progress activities
     */
    public function getvaccumcleanerstopped() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $actlogtblid = isset($_POST['actlogtblid']) ? $_POST['actlogtblid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $stopreason = isset($_POST['stopreason']) ? $_POST['stopreason'] : '';
        $curr_url = isset($_GET['curr_url']) ? $_GET['curr_url'] : '';
        //Check is the person room in before perform an operation 
        //if ($curr_url != 'inprogress_activity') {
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        $isworkflow = 0;

        //echo"<pre/>";print_r($actid);exit;
        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        // echo"<pre/>";print_r($wfres->result());exit;
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";
            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($isroomin == 1) {
            $result = $this->Ptscommon->vaccumcleanerstop($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email, $stopreason, $status_id, $next_step);
            if ($result == TRUE) {
                if ($next_step > 0) {
                    $this->commonEmailFunction($actlogtblid);
                }
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Stopped Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to stop this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function getprefilterstopped() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $actlogtblid = isset($_POST['actlogtblid']) ? $_POST['actlogtblid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $stopreason = isset($_POST['stopreason']) ? $_POST['stopreason'] : '';
        $curr_url = isset($_GET['curr_url']) ? $_GET['curr_url'] : '';
        //Check is the person room in before perform an operation 
        //if ($curr_url != 'inprogress_activity') {
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        if ($isroomin == 1) {
            $result = $this->Ptscommon->prefilterstop($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email, $stopreason);
            if ($result == TRUE) {
                $this->commonEmailFunction($actlogtblid);
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Stopped Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to stop this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    /**
     * Get start of selected activity
     */
    //function with audit trail code by bittu
    public function getstarted() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $equipmentcode = isset($_POST['equipmentcode']) ? $_POST['equipmentcode'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $current_name = isset($_POST['current_name']) ? $_POST['current_name'] : '';
        $remark2 = $actname . ' is in progress';
        if (!empty($current_name)) {
            $remark2 = $actname . '/' . $current_name . ' is in progress';
            $actname = $current_name;
        }
        $get_doc = isset($_POST['docno']) ? $_POST['docno'] : '';
        $sampling_rod_id = isset($_POST['sampling_rod_id']) ? $_POST['sampling_rod_id'] : '';
        $dies_no = isset($_POST['dies_no']) ? $_POST['dies_no'] : '';
        $module_id = isset($_POST['module_id']) ? $_POST['module_id'] : '';
        $act_name2 = isset($_POST['actname2']) ? $_POST['actname2'] : '';
        $Logname = isset($_POST['Logname']) ? $_POST['Logname'] : '';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';
        $process_name = '';
        if (!empty($processid)) {
            $process_data = $this->db->get_where("pts_mst_activity", array("id" => $processid))->row_array();
            if (!empty($process_data)) {
                $process_name = $process_data['activity_name'];
            }
        }
        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $isworkflow = 0;
        if ($actid == 16 || $actid == 17) {
            $equipment_data = $this->db->get_where("mst_equipment", array("room_code" => $roomcode, "equipment_code" => $equipmentcode, "is_inused" => 1, "is_active" => 1))->row_array();
        } else if ($actid == 30) {
            $equipment_data = $this->db->get_where("pts_mst_line_log", array("room_code" => $roomcode, "line_log_code" => $equipmentcode, "is_inused" => 1, "status" => 'active'))->row_array();
        } else if ($actid == 25) {
            $equipment_data = $this->db->get_where("pts_mst_instrument", array("equipment_code" => $equipmentcode, "is_inused" => 1, "status" => 'active'))->row_array();
        }
        if (!empty($equipment_data)) {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you have already used this equipment, Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        if ($module_id == 16 || $module_id == 17 || $module_id == 30) {
            $inprogressActivity = array();
            $query = "SELECT * FROM `pts_trn_user_activity_log` WHERE `room_code` = '$roomcode' AND (`workflownextstep` != '-1' OR `workflownextstep` = '0') AND `activity_id` IN(1,2,3,4,5,6)";
            $inprogressActivity = $this->db->query($query)->row_array();
            if (!empty($inprogressActivity) && ($inprogressActivity['activity_id'] != $actid)) {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because there is already another activity is inprogress in this room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        }
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'start');
        if ($wfres->num_rows() > 0) {
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }
        //Getting Next step of workflow of current Activity
        if ($candone == 1 && $isroomin == 1) {
            $data = ["process_name" => $process_name,"room_code" => $roomcode,"act_id" => $actid,"equipment_id" => $equipmentcode,
                    "act_name" => $actname,"product_code" => $product_no,"batch_no" => $batch_no,"done_by_user_id" => $empid,
                    "done_by_user_name" => $empname,"done_by_role_id" => $roleid,"done_by_email" => $email,"done_by_remark" => $remark,
                    "is_in_workflow" => 'no'];
            $auditParams = ['command_type' => 'insert','created_by' => $empid,'created_by_name' => $empname,
                    'activity_name' => $process_name,'type' => $actname,'table_name' => 'pts_trn_user_activity_log'];
            $arr = ["activity_id" => $actid,"roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, 
                    "batch_no" => $batch_no, "equip_code" => $equipmentcode, "user_id" => $empid, "user_name" => $empname, "user_email" => $email, 
                    "activity_remarks" => $remark2,"versionid" => $headerRecordid, "activity_start" => time(), "workflowstatus" => $status_id, 
                    "workflownextstep" => $next_step];
            if ($sampling_rod_id != '' && $dies_no != '' && $act_name2 != "") {
                $arr['sampling_rod_id '] = $sampling_rod_id;
                $arr['dies_no'] = $dies_no;
                $arr['act_name'] = $act_name2;
                $arr['activity_name']=$actname;
            }
            $arr3 = ["approval_status" => 'N/A',"user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start'];
            $result = $this->Ptscommon->commonLogInsertNewFunction($arr, $arr3, $Logname, $data, $auditParams, $isworkflow, $equipmentcode, $sampling_rod_id);
            if ($result > 0) {
                if ($next_step > 0) {
                    $this->commonEmailFunction($result);
                }
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Started Successfully', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } 
            else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Activity Not Started please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Update Started Activity
    //function with audit trail code by bittu
    public function updatestarted() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $equipmentcode = isset($_POST['equipmentcode']) ? $_POST['equipmentcode'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $get_doc = isset($_POST['docno']) ? $_POST['docno'] : '';
        $sampling_rod_id = isset($_POST['sampling_rod_id']) ? $_POST['sampling_rod_id'] : '';
        $dies_no = isset($_POST['dies_no']) ? $_POST['dies_no'] : '';
        $module_id = isset($_POST['module_id']) ? $_POST['module_id'] : '';
        $act_name2 = isset($_POST['actname2']) ? $_POST['actname2'] : '';
        $update_id = isset($_POST['update_id']) ? $_POST['update_id'] : '';
        $current_name = isset($_POST['current_name']) ? $_POST['current_name'] : '';
        if (!empty($current_name)) {
            $remark2 = $actname . '/' . $current_name . ' is in progress';
            $actname = $current_name;
        }
        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $terID = $roomcode;
                $proname = 'process log id = ' . $processid . ' Activity = ' . $actid . ' name is ' . $actname;
                $log = 'where as product is :' . $product_no . ' and batch no is: ' . $batch_no;
                $action = 'Activity Start fail because user doing this in different room';
                $createby = $empname;
                $oldval = "N/A";
                $newval = "N/A";
                $this->AuditLog($roomcode, $proname, $log, $action, $createby, $oldval, $newval);
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $terID = $roomcode;
            $proname = 'process log id = ' . $processid . ' Activity = ' . $actid . ' name is ' . $actname;
            $log = 'where as product is :' . $product_no . ' and batch no is: ' . $batch_no;
            $action = 'Activity Start fail because user was not room in';
            $createby = $empname;
            $oldval = "N/A";
            $newval = "N/A";
            $this->AuditLog($roomcode, $proname, $log, $action, $createby, $oldval, $newval);
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
//        if ($module_id == 16 || $module_id == 17 || $module_id == 30) {
//            $inprogressActivity = array();
//            $query = "SELECT * FROM `pts_trn_user_activity_log` WHERE `room_code` = '$roomcode' AND `roomlogactivity` = '73' OR `roomlogactivity` = '52' OR `roomlogactivity` = '64' AND (`workflownextstep` != '-1' OR `workflownextstep` = '0')";
//            $inprogressActivity = $this->db->query($query)->row_array();
//            if (!empty($inprogressActivity) && ($inprogressActivity['activity_id'] != $actid)) {
//                $isroomin = 0;
//                $terID = $roomcode;
//                $proname = 'process log id = ' . $processid . ' Activity = ' . $actid . ' name is ' . $actname;
//                $log = 'where as product is :' . $product_no . ' and batch no is: ' . $batch_no;
//                $action = 'Activity Start fail because already activity in progress';
//                $createby = $empname;
//                $oldval = "N/A";
//                $newval = "N/A";
//                $this->AuditLog($roomcode, $proname, $log, $action, $createby, $oldval, $newval);
//                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because there is already another activity is inprogress in this room, Thank you...!');
//                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
//                exit;
//            }
//        }
        if ($get_doc == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $doc_no = $doc_row['docno'] + 1;
            $docno = $doc_pre . $doc_no;

            $arr2 = array("docno" => $doc_no);
            $this->db->where("id", 13);
            $this->db->update("pts_mst_document", $arr2);
        } else {
            $docno = $get_doc;
        }
        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'start');
        if ($wfres->num_rows() > 0) {
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleid, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
        }


//        if ($candone == 1 && $isroomin == 1) {
        if ($isroomin == 1) {
            $result = $this->Ptscommon->updatestart($roomcode, $processid, $equipmentcode, $actid, $actname, $product_no, $batch_no, $roleid, $empid, $empname, $email, $status_id, $next_step, $remark, $remark2, $docno, $sampling_rod_id, $dies_no, $act_name2, $update_id);
            if ($result > 0) {
                $doc_no = $docno;
                $data = array(
                    "room_code" => $roomcode,
                    "act_id" => $actid,
                    "equipment_id" => $equipmentcode,
                    "act_name" => $actname,
                    "product_code" => $product_no,
                    "batch_no" => $batch_no,
                    "done_by_user_id" => $empid,
                    "done_by_user_name" => $empname,
                    "done_by_role_id" => $roleid,
                    "done_by_email" => $email,
                    "done_by_remark" => $remark,
                    "is_in_workflow" => 'no'
                );
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "";
                $uniquefiled2 = "equipment_id:" . $equipmentcode;
                $uniquefiled3 = "pre_barch_no:" . $batch_no;
                $activityData = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array('id' => $update_id));
                if (!empty($activityData)) {
                    $activity_id = $activityData[0]['id'];
                    $uniquefiled1 = "doc_no:" . $activityData[0]['doc_id'];
                    $data['doc_no'] = $activityData[0]['doc_id'];
                }

                //get activity name
                $activity_name_for_trail = "";
                $activityData = $this->Ptscommon->getMultipleRows('default', 'pts_mst_activity', array('id' => $processid));
                if (!empty($activityData)) {
                    $activity_name_for_trail = $activityData[0]['activity_name'];
                }
                $auditParams = array(
                    'command_type' => 'update',
                    'created_by' => $empid,
                    'created_by_name' => $empname,
                    'activity_name' => $activity_name_for_trail,
                    'type' => $actname,
                    'uniquefiled1' => $uniquefiled1,
                    'uniquefiled2' => $uniquefiled2,
                    'uniquefiled3' => $uniquefiled3,
                    'table_name' => 'pts_trn_user_activity_log',
                    'primary_id' => $update_id,
                    'log_user_activity_id' => $update_id,
                    'post_params' => $data
                );
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Updated.', 'acttblid' => "");
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $terID = $roomcode;
                $proname = 'process log id = ' . $processid . ' Activity = ' . $actid . ' name is ' . $actname;
                $log = 'where as product is :' . $product_no . ' and batch no is: ' . $batch_no;
                $action = 'Something went wrong please try again later.';
                $createby = $empname;
                $oldval = "N/A";
                $newval = "N/A";
                $this->AuditLog($roomcode, $proname, $log, $action, $createby, $oldval, $newval);
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $terID = $roomcode;
            $proname = 'process log id = ' . $processid . ' Activity = ' . $actid . ' name is ' . $actname;
            $log = 'where as product is :' . $product_no . ' and batch no is: ' . $batch_no;
            $action = 'Activity Start fail because either he has no permission or he had done any step in this activity';
            $createby = $empname;
            $oldval = "N/A";
            $newval = "N/A";
            $this->AuditLog($roomcode, $proname, $log, $action, $createby, $oldval, $newval);
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to update this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Audit Trail code by Khushboo
    public function submit_return_air_filter() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $frequency = isset($_POST['frequency']) ? $_POST['frequency'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';     

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;
        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {
            $data = ["room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "frequency_id" => $frequency, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no'];
            $data2 = ["activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "versionid" => $headerRecordid, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step];
            $data3 = ["approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start'];
            $auditParams = ['command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'Return Air Filter Cleaning Record', 'type' => 'air_filter_cleaning_record', 'table_name' => 'pts_trn_return_air_filter'];
            $result = $this->Ptscommon->newFunctionToSubmitReturnAirFilter($isworkflow, $data, $data2, $data3, $auditParams);
            if ($result > 0) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Air Filter Stop 
    public function getairfilterstopped() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $actlogtblid = isset($_POST['actlogtblid']) ? $_POST['actlogtblid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $frequency = isset($_POST['frequency']) ? $_POST['frequency'] : '';
        $selectedfilters = isset($_POST['selectedfilters']) ? $_POST['selectedfilters'] : '';
        $selectedReplaces = isset($_POST['selectedReplaces']) ? $_POST['selectedReplaces'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';
        $tempArray = array_merge(explode(",", $selectedReplaces), explode(",", $selectedfilters));
        sort($tempArray);
        $tempString = implode(",", $tempArray);
        $stop_id = isset($_POST['stop_id']) ? $_POST['stop_id'] : '';
//        print_r($tempString);exit;
        //Check is the person room in before perform an operation 
        //if ($curr_url != 'inprogress_activity') {
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        if ($isroomin == 1) {
            $data = array("filter" => $tempString, "clean_filter" => $selectedfilters, "replace_filter" => $selectedReplaces);
            $result = $this->Ptscommon->airfilterstopped($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email, $data, $stop_id);
            if ($result == TRUE) {
                $this->commonEmailFunction($actlogtblid);
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Stopped Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to stop this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Audit Trail Code By Khushboo
    public function update_return_air_filter() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $frequency = isset($_POST['frequency']) ? $_POST['frequency'] : '';
        $selectedfilters = isset($_POST['selectedfilters']) ? $_POST['selectedfilters'] : '';
        $selectedReplaces = isset($_POST['selectedReplaces']) ? $_POST['selectedReplaces'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';
        $update_id = isset($_POST['update_id']) ? $_POST['update_id'] : '';
        $is_in_workflow = isset($_POST['is_in_workflow']) ? $_POST['is_in_workflow'] : '';
        $tempArray = array_merge(explode(",", $selectedReplaces), explode(",", $selectedfilters));
        sort($tempArray);
        $tempString = implode(",", $tempArray);

        if ($doc_no == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $docno = $doc_row['docno'] + 1;
            $doc_no = $doc_pre . $docno;
        } else {
            $doc_no = $doc_no;
        }

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleid, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($isroomin == 1) {
            $where = array("id" => $update_id);
            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "frequency_id" => $frequency, "filter" => $tempString, "clean_filter" => $selectedfilters, "replace_filter" => $selectedReplaces, "is_in_workflow" => $is_in_workflow, "updated_by" => $empid, "updated_on" => date("Y-m-d H:i:s"), "update_remark" => $remark);
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_user_activity_log', array("product_code" => $product_no, "batch_no" => $batch_no), array("doc_id" => $doc_no));
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_return_air_filter', $data, $where);
            if ($result > 0) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "frequency_id:" . $frequency;
                $uniquefiled3 = "batch_no:" . $batch_no;
                $activityData = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array('doc_id' => $doc_no));
                if (!empty($activityData)) {
                    $activity_id = $activityData[0]['id'];
                }
                $auditParams = array('command_type' => 'update', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'Return Air Filter Cleaning Record', 'type' => 'air_filter_cleaning_record', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_return_air_filter', 'primary_id' => $update_id, 'log_user_activity_id' => $activity_id, 'post_params' => $data);
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Updated.', 'acttblid' => "");
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to Update this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Audit Trail Code By Khushboo
    public function submit_vaccum_cleaner_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $vaccum_code = isset($_POST['vaccum_code']) ? $_POST['vaccum_code'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';
        $equipment_data = $this->db->get_where("mst_equipment", array("room_code" => $roomcode, "equipment_code" => $vaccum_code, "is_inused" => 1, "is_active" => 1))->row_array();

        if (!empty($equipment_data)) {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you have already used this equipment, Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {
            //$data["doc_no"] = $doc_no;    $data2["doc_id"] = $doc_no;
            $data = array("room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "vaccum_cleaner_code" => $vaccum_code, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no');
            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => $vaccum_code, "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "versionid" => $headerRecordid, "activity_start" => time(), "workflowstatus" => "0", "workflownextstep" => "0");
            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');
            $uniquefiled2 = "vaccum_cleaner_code:" . $vaccum_code;
            $uniquefiled3 = "batch_no:" . $batch_no;
            $auditParams = array('command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, "product_code" => $product_no,"batch_no" => $batch_no,'activity_name' => 'Vacuum Cleaner Logbook', 'type' => 'vaccum_cleaner_logbook', 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_vaccum_cleaner');
            $result = $this->Ptscommon->newFunctionToSubmitVacuumCleanerLogRecord($isworkflow, $data, $data2, $data3, $auditParams);
            if ($result > 0) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Audit Trail Code By KHushboo
    public function update_vaccum_cleaner_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $vaccum_code = isset($_POST['vaccum_code']) ? $_POST['vaccum_code'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $update_id = isset($_POST['update_id']) ? $_POST['update_id'] : '';
        $is_in_workflow = isset($_POST['is_in_workflow']) ? $_POST['is_in_workflow'] : '';

        if ($doc_no == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $docno = $doc_row['docno'] + 1;
            $doc_no = $doc_pre . $docno;
        } else {
            $doc_no = $doc_no;
        }

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleid, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {
            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "vaccum_cleaner_code" => $vaccum_code, "is_in_workflow" => $is_in_workflow, "updated_by" => $empid, "updated_on" => date("Y-m-d H:i:s"), "update_remark" => $remark);
            $where = array("id" => $update_id);
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_user_activity_log', array("product_code" => $product_no, "batch_no" => $batch_no), array("doc_id" => $doc_no));
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_vaccum_cleaner', $data, $where);
            if ($result > 0) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "vaccum_cleaner_code:" . $vaccum_code;
                $uniquefiled3 = "batch_no:" . $batch_no;
                $activityData = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array('doc_id' => $doc_no));
                if (!empty($activityData)) {
                    $activity_id = $activityData[0]['id'];
                }
                $auditParams = array('command_type' => 'update', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'Vacuum Cleaner Logbook', 'type' => 'vaccum_cleaner_logbook', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_vaccum_cleaner', 'primary_id' => $update_id, 'log_user_activity_id' => $activity_id, 'post_params' => $data);
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Updated.', 'acttblid' => "");
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to update this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_env_con_diff_record_old() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';

        $logger_id = isset($_POST['logger_id']) ? $_POST['logger_id'] : '';
        $gauge_id = isset($_POST['gauge_id']) ? $_POST['gauge_id'] : '';
        $pressure_differential = isset($_POST['pressure_differential']) ? $_POST['pressure_differential'] : '';
        $reading = isset($_POST['reading']) ? $_POST['reading'] : '';
        $temp = isset($_POST['temp']) ? $_POST['temp'] : '';
        $min_temp = isset($_POST['min_temp']) ? $_POST['min_temp'] : '';
        $max_temp = isset($_POST['max_temp']) ? $_POST['max_temp'] : '';
        $rh_value = isset($_POST['rh_value']) ? $_POST['rh_value'] : '';
        $min_rh = isset($_POST['min_rh']) ? $_POST['min_rh'] : '';
        $max_rh = isset($_POST['max_rh']) ? $_POST['max_rh'] : '';
        $from_temp = isset($_POST['from_temp']) ? $_POST['from_temp'] : '';
        $to_temp = isset($_POST['to_temp']) ? $_POST['to_temp'] : '';
        $from_humidity = isset($_POST['from_humidity']) ? $_POST['from_humidity'] : '';
        $to_humidity = isset($_POST['to_humidity']) ? $_POST['to_humidity'] : '';
        $pressure = isset($_POST['pressure']) ? $_POST['pressure'] : '';
        $from_pressure = isset($_POST['from_pressure']) ? $_POST['from_pressure'] : '';
        $to_pressure = isset($_POST['to_pressure']) ? $_POST['to_pressure'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';
        $id = isset($_POST['env_id']) ? $_POST['env_id'] : '';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';
        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;
        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {

            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "data_log_id" => $logger_id, "magnelic_id" => $gauge_id, "pressure_diff" => $pressure_differential, "reading" => $reading, "temp" => $temp, "temp_min" => $min_temp, "temp_max" => $max_temp, "rh" => $rh_value, "rh_min" => $min_rh, "rh_max" => $max_rh, "temp_from" => $from_temp, "temp_to" => $to_temp, "rh_from" => $from_humidity, "rh_to" => $to_humidity, "globe_pressure_diff" => $pressure, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no', "from_pressure" => $from_pressure, "to_pressure" => $to_pressure);

            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "doc_id" => $doc_no, "versionid" => $headerRecordid, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);

            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');

            $result = $this->Ptscommon->Com_fun_to_submit_env_log_details($isworkflow, 'pts_trn_env_cond_diff', $data, $data2, $data3, $id);
            if ($result > 0) {
                if ($next_step > 0) {
                    $this->commonEmailFunction($result);
                }
                //updating doc no
                $arr2 = array("docno" => $docno);
                $this->db->where("id", 13);
                $this->db->update("pts_mst_document", $arr2);


                /* Start - Insert elog audit histry */
                if ($isworkflow == 0) {
                    $inserted_id = $result;
                    $activity_id = 0;
                } else {
                    $insertedData = $this->Ptscommon->getMultipleRows('default', 'pts_trn_env_cond_diff', array('doc_no' => $doc_no));
                    if (!empty($insertedData)) {
                        $inserted_id = $insertedData[0]['id'];
                    }
                    $activity_id = $result;
                }

                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "logger_id:" . $logger_id;
                $uniquefiled3 = "gauge_id:" . $gauge_id;
                $auditParams = array('command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'Environmental Condition/Pressure Differential Record', 'type' => 'environmental_condition_record', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_env_cond_diff', 'primary_id' => $inserted_id, 'log_user_activity_id' => $activity_id, 'post_params' => $data);
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */

//End
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function update_env_con_diff_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';

        $pressure_differential = isset($_POST['pressure_differential']) ? $_POST['pressure_differential'] : '';
        $reading = isset($_POST['reading']) ? $_POST['reading'] : '';
        $temp = isset($_POST['temp']) ? $_POST['temp'] : '';
        $min_temp = isset($_POST['min_temp']) ? $_POST['min_temp'] : '';
        $max_temp = isset($_POST['max_temp']) ? $_POST['max_temp'] : '';
        $rh_value = isset($_POST['rh_value']) ? $_POST['rh_value'] : '';
        $min_rh = isset($_POST['min_rh']) ? $_POST['min_rh'] : '';
        $max_rh = isset($_POST['max_rh']) ? $_POST['max_rh'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';
        $is_in_workflow = isset($_POST['is_in_workflow']) ? $_POST['is_in_workflow'] : '';
        $update_id = isset($_POST['update_id']) ? $_POST['update_id'] : '';

        if ($doc_no == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $docno = $doc_row['docno'] + 1;
            $doc_no = $doc_pre . $docno;
        } else {
            $doc_no = $doc_no;
        }

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleid, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($isroomin == 1) {
            $where = array("id" => $update_id);
            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "pressure_diff" => $pressure_differential, "reading" => $reading, "temp" => $temp, "temp_min" => $min_temp, "temp_max" => $max_temp, "rh" => $rh_value, "rh_min" => $min_rh, "rh_max" => $max_rh, "is_in_workflow" => $is_in_workflow, "updated_by" => $empid, "updated_on" => date("Y-m-d H:i:s"), "update_remark" => $remark);

            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_env_cond_diff', $data, $where);
            if ($result > 0) {
                //updating doc no
//                $arr2 = array("docno" => $doc_no);
//                $this->db->where("id", 13);
//                $this->db->update("pts_mst_document", $arr2);

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "logger_id:";
                $uniquefiled3 = "gauge_id:";
                $activityData = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array('doc_id' => $doc_no));
                if (!empty($activityData)) {
                    $activity_id = $activityData[0]['id'];
                }
                $auditParams = array('command_type' => 'update', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'Environmental Condition/Pressure Differential Record', 'type' => 'environmental_condition_record', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_env_cond_diff', 'primary_id' => $update_id, 'log_user_activity_id' => $activity_id, 'post_params' => $data);
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */
//End
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully updated.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Audit Trail Code By Khushboo
    public function submit_equip_apparatus_log_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $apparatus_code = isset($_POST['apparatus_code']) ? $_POST['apparatus_code'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $stage = isset($_POST['stage']) ? $_POST['stage'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $cleaning_verification = isset($_POST['cleaning_verification']) ? $_POST['cleaning_verification'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';

        $equipment_data = $this->db->get_where("pts_mst_instrument", array("equipment_code" => $apparatus_code, "is_inused" => 1, "status" => 'active'))->row_array();
        if (!empty($equipment_data)) {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you have already used this equipment, Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        
        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 1;

        //Getting Next step of workflow of current Activity
        $candone = 1;
        if ($candone == 1 && $isroomin == 1) {

            $data = array("room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_appratus_no" => $apparatus_code, "stage" => $stage, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no', "cleaning_verification" => $cleaning_verification);
            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => $apparatus_code, "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "versionid" => $headerRecordid, "activity_start" => time(), "workflowstatus" => $roleid, "workflownextstep" => $roleid);
            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');
            $result = $this->Ptscommon->euipAppartusLogRecord($isworkflow, 'pts_trn_equip_appratus_log', $data, $data2, $data3);
            if (isset($result) && !empty($result) && $result['actlogtblid'] > 0) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result['actlogtblid']);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Audit Trail Code By Khushboo
    public function update_equip_apparatus_log_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $apparatus_code = isset($_POST['apparatus_code']) ? $_POST['apparatus_code'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $stage = isset($_POST['stage']) ? $_POST['stage'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $cleaning_verification = isset($_POST['cleaning_verification']) ? $_POST['cleaning_verification'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $update_id = isset($_POST['update_id']) ? $_POST['update_id'] : '';
        $is_in_workflow = isset($_POST['is_in_workflow']) ? $_POST['is_in_workflow'] : '';

        if ($doc_no == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $docno = $doc_row['docno'] + 1;
            $doc_no = $doc_pre . $docno;
        } else {
            $doc_no = $doc_no;
        }

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 1;

        //Getting Next step of workflow of current Activity
        $candone = 1;
        if ($candone == 1 && $isroomin == 1) {
            $where = array("id" => $update_id);
            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_appratus_no" => $apparatus_code, "stage" => $stage, "cleaning_verification" => $cleaning_verification, "is_in_workflow" => $is_in_workflow, "updated_by" => $empid, "updated_on" => date("Y-m-d H:i:s"), "update_remark" => $remark);
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_user_activity_log', array("product_code" => $product_no, "batch_no" => $batch_no), array("doc_id" => $doc_no));
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_equip_appratus_log', $data, $where);
            if ($result > 0) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "equip_appratus_no:" . $apparatus_code;
                $uniquefiled3 = "batch_no:" . $batch_no;
                $activityData = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array('doc_id' => $doc_no));
                if (!empty($activityData)) {
                    $activity_id = $activityData[0]['id'];
                }
                $auditParams = array('command_type' => 'update', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'Equipment/Apparatus Log Register (Friabilator)', 'type' => 'equipment_apparatus_log_register', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_equip_appratus_log', 'primary_id' => $update_id, 'log_user_activity_id' => $activity_id, 'post_params' => $data);
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Updated.', 'acttblid' => "");
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to update this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Audit Trail Code By Khushboo
    public function submit_equip_apparatus_leaktest() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $apparatus_code = isset($_POST['apparatus_code']) ? $_POST['apparatus_code'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $stage = isset($_POST['stage']) ? $_POST['stage'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';
        $equipment_data = $this->db->get_where("pts_mst_instrument", array("equipment_code" => $apparatus_code, "is_inused" => 1, "status" => 'active'))->row_array();
        if (!empty($equipment_data)) {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you have already used this equipment, Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        
        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 1;

        //Getting Next step of workflow of current Activity
        $candone = 1;
        if ($candone == 1 && $isroomin == 1) {

            $data = array("room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_appratus_no" => $apparatus_code, "stage" => $stage, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no');

            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => $apparatus_code, "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "versionid" => $headerRecordid, "activity_start" => time(), "workflowstatus" => $roleid, "workflownextstep" => $roleid);

            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');

            $result = $this->Ptscommon->euipAppartusLeaktest($isworkflow, 'pts_trn_equip_appratus_leaktest', $data, $data2, $data3);
              if (isset($result) && !empty($result) && $result['actlogtblid'] > 0) {
              
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result['actlogtblid']);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Audit Trail Code By Khushboo
    public function update_equip_apparatus_leaktest() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $apparatus_code = isset($_POST['apparatus_code']) ? $_POST['apparatus_code'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $stage = isset($_POST['stage']) ? $_POST['stage'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $update_id = isset($_POST['update_id']) ? $_POST['update_id'] : '';
        $is_in_workflow = isset($_POST['is_in_workflow']) ? $_POST['is_in_workflow'] : '';

        if ($doc_no == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $docno = $doc_row['docno'] + 1;
            $doc_no = $doc_pre . $docno;
        } else {
            $doc_no = $doc_no;
        }

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 1;

        //Getting Next step of workflow of current Activity
        $candone = 1;
        if ($candone == 1 && $isroomin == 1) {
            $where = array("id" => $update_id);
            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_appratus_no" => $apparatus_code, "stage" => $stage, "is_in_workflow" => $is_in_workflow, "updated_by" => $empid, "updated_on" => date("Y-m-d H:i:s"), "update_remark" => $remark);
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_user_activity_log', array("product_code" => $product_no, "batch_no" => $batch_no), array("doc_id" => $doc_no));
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_equip_appratus_leaktest', $data, $where);
            if ($result > 0) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "equip_appratus_no:" . $apparatus_code;
                $uniquefiled3 = "batch_no:" . $batch_no;
                $activityData = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array('doc_id' => $doc_no));
                if (!empty($activityData)) {
                    $activity_id = $activityData[0]['id'];
                }
                $auditParams = array('command_type' => 'update', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'Equipment apparatus log register (leak test)', 'type' => 'sf_equipment_apparatus_leaktest', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_equip_appratus_leaktest', 'primary_id' => $update_id, 'log_user_activity_id' => $activity_id, 'post_params' => $data);
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Updated.', 'acttblid' => "");
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to update this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Audit Trail Code By Khushboo
    public function submit_laf_pressure_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $booth_code = isset($_POST['booth_code']) ? $_POST['booth_code'] : '';
        $mi5_mi15 = isset($_POST['mi5_mi15']) ? $_POST['mi5_mi15'] : '';
        $mi0_mi5 = isset($_POST['mi0_mi5']) ? $_POST['mi0_mi5'] : '';
        $gi0_gi3 = isset($_POST['gi0_gi3']) ? $_POST['gi0_gi3'] : '';
        $gi15_gi5 = isset($_POST['gi15_gi5']) ? $_POST['gi15_gi5'] : '';
        $gi8_gi14 = isset($_POST['gi8_gi14']) ? $_POST['gi8_gi14'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';

        $equipment_data = $this->db->get_where("mst_equipment", array("room_code" => $roomcode, "equipment_code" => $booth_code, "is_inused" => 1, "is_active" => 1))->row_array();

        if (!empty($equipment_data)) {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you have already used this equipment, Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {

            $data = array("room_code" => $roomcode, "m1" => $mi5_mi15, "m2" => $mi0_mi5, "g1" => $gi0_gi3, "g2" => $gi15_gi5, "g3" => $gi8_gi14, "booth_no" => $booth_code, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no');

            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2,"versionid" => $headerRecordid, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);

            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'stop');

            $result = $this->Ptscommon->lafPresureRecord($isworkflow, 'pts_trn_laf_pressure_diff', $data, $data2, $data3);
            if (isset($result) && !empty($result) && $result['actlogtblid'] > 0 ) {
                if ($next_step > 0) {
                    $this->commonEmailFunction($result['actlogtblid']);
                }

                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result['actlogtblid']);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Audit Trail Code By Khushboo
    public function update_laf_pressure_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $booth_code = isset($_POST['booth_code']) ? $_POST['booth_code'] : '';
        $mi5_mi15 = isset($_POST['mi5_mi15']) ? $_POST['mi5_mi15'] : '';
        $mi0_mi5 = isset($_POST['mi0_mi5']) ? $_POST['mi0_mi5'] : '';
        $gi0_gi3 = isset($_POST['gi0_gi3']) ? $_POST['gi0_gi3'] : '';
        $gi15_gi5 = isset($_POST['gi15_gi5']) ? $_POST['gi15_gi5'] : '';
        $gi8_gi14 = isset($_POST['gi8_gi14']) ? $_POST['gi8_gi14'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $update_id = isset($_POST['update_id']) ? $_POST['update_id'] : '';
        $is_in_workflow = isset($_POST['is_in_workflow']) ? $_POST['is_in_workflow'] : '';

        if ($doc_no == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $docno = $doc_row['docno'] + 1;
            $doc_no = $doc_pre . $docno;
        } else {
            $doc_no = $doc_no;
        }

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleid, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {
            $where = array("id" => $update_id);
            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "m1" => $mi5_mi15, "m2" => $mi0_mi5, "g1" => $gi0_gi3, "g2" => $gi15_gi5, "g3" => $gi8_gi14, "booth_no" => $booth_code, "is_in_workflow" => $is_in_workflow, "updated_by" => $empid, "updated_on" => date("Y-m-d H:i:s"), "update_remark" => $remark);
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_laf_pressure_diff', $data, $where);
            if ($result > 0) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "room_code:" . $roomcode;
                $uniquefiled3 = "booth_no:" . $booth_code;
                $activityData = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array('doc_id' => $doc_no));
                if (!empty($activityData)) {
                    $activity_id = $activityData[0]['id'];
                }
                $auditParams = array('command_type' => 'update', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'LAF Pressure Differential Record', 'type' => 'laf_pressure_record', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_laf_pressure_diff', 'primary_id' => $update_id, 'log_user_activity_id' => $activity_id, 'post_params' => $data);
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Updated.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to update this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_bal_calibration_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';

        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $StandardDataArray = [];
        $standard_array = json_decode($_POST['standardData'], true);

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        foreach ($standard_array as $key => $value) {//"doc_no" => $doc_no,
            $StandardDataArray[] = ["room_code" => $value['room_code'],"balance_no" => $value['balance_no'],"balance_id" => $value['balid'],
                "frequency_id" => $value['frequency_id'],"standerd_wt" => $value['standard_value'],"id_no_of_st_wt" => implode(",", $value['id_standard']),
                "id_no_st_wt_index" => implode(",", $value['id_standard_key']),"oberved_wt" => $value['observed_weight'],"diff" => $value['difference'],
                "sprit_level_status" => $value['spirit_level'],"time_status" => $value['time'],"done_by_user_id" => $empid,
                "done_by_user_name" => $empname,"done_by_role_id" => $roleid,"done_by_email" => $email,"done_by_remark" => $remark,"is_in_workflow" => 'no'
            ];
        }

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {//$data2["doc_id"] = $doc_no;$auditParams['uniquefiled1'] = "doc_no:" . $doc_no;
            //$auditParams['primary_id'] = $inserted_id; $auditParams['log_user_activity_id'] = $activity_id; $auditParams['post_params'] = $data;
            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "versionid" => $headerRecordid, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);
            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');
            $auditParams = ['command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 
                'activity_name' => 'Balance Calibration Record', 'type' => 'balance_calibration_record', 'uniquefiled2' => '', 'uniquefiled3' => '', 
                'table_name' => 'pts_trn_balance_calibration'];
            $result = $this->Ptscommon->newFunctionToSubmitBalanceCalibrationLog($isworkflow, $StandardDataArray, $data2, $data3, $auditParams);
            if ($result > 0) {
                if ($next_step > 0) {
                    $this->commonEmailFunction($result);
                }
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Update Balance data 
    public function update_bal_calibration_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';

        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $StandardDataArray = [];
        $arraylength = false;
        $standard_array = json_decode($_POST['standardData'], true);

        $update_id = isset($_POST['update_id']) ? $_POST['update_id'] : '';
        $is_in_workflow = isset($_POST['is_in_workflow']) ? $_POST['is_in_workflow'] : '';

        if ($doc_no == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $docno = $doc_row['docno'] + 1;
            $doc_no = $doc_pre . $docno;
        } else {
            $doc_no = $doc_no;
        }

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        foreach ($standard_array as $key => $value) {
            $StandardDataArray[] = array(                
                "doc_no" => $doc_no,
                "room_code" => $value['room_code'],
                "balance_no" => $value['balance_no'],
                "balance_id" => $value['balid'],
                //"frequency_id" => $value['frequency_id'],
                "standerd_wt" => $value['standard_value'],
                //"id_no_of_st_wt" => implode(",", $value['id_standard']),
                "id_no_st_wt_index" => implode(",", $value['id_standard_key']),
                "oberved_wt" => $value['observed_weight'],
                "diff" => $value['difference'],
                "sprit_level_status" => $value['spirit_level'],
                "time_status" => $value['time'],
                "done_by_user_id" => $empid,
                "done_by_user_name" => $empname,
                "done_by_role_id" => $roleid,
                "done_by_email" => $email,
                "done_by_remark" => $remark,
                "is_in_workflow" => $is_in_workflow,
                "updated_by" => $empid,
                "updated_on" => date("Y-m-d H:i:s"),
                "update_remark" => $remark,
                "id" => $value['editid']
            );
            
        }//exit;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleid, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($isroomin == 1) {

            if (count($StandardDataArray) > 1) {
                $data = $StandardDataArray;
                $arraylength = true;
            } else {
                $data = $StandardDataArray;
                $arraylength = false;
            }
            // $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_user_activity_log', array("product_code" => $product_no, "batch_no" => $batch_no), array("doc_id" => $doc_no));
            foreach ($data as $key => $value) {
                $where = array("id" => $value['id']);
                unset($data[$key]["id"]);
                $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_balance_calibration', $data[$key], $where);
            }

            if ($result > 0) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "";
                $uniquefiled3 = "";
                $activityData = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array('doc_id' => $doc_no));
                if (!empty($activityData)) {
                    $activity_id = $activityData[0]['id'];
                }
                $auditParams = array('command_type' => 'update', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'Balance Calibration Record', 'type' => 'balance_calibration_record', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_balance_calibration', 'primary_id' => $update_id, 'log_user_activity_id' => $activity_id, 'post_params' => $data);
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Updated.', 'acttblid' => "");
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to update this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_mat_retreival_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';

        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $material_condition = isset($_POST['material_condition']) ? $_POST['material_condition'] : '';
        $formDataArray = [];
        $arraylength = false;
        $formData = json_decode($_POST['formData'], true);


        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        foreach ($formData as $key => $value) {
            $formDataArray[] = array(
                "room_code" => $roomcode,
                "material_condition" => $material_condition,
                "material_batch_no" => $value['material_batch_no'],
                "material_type" => $value['material_type'],
                "material_code" => $value['material_code'],
                "done_by_user_id" => $empid,
                "done_by_user_name" => $empname,
                "done_by_role_id" => $roleid,
                "done_by_email" => $email,
                "done_by_remark" => $remark,
                "is_in_workflow" => 'no'
            );
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {

            if (count($formDataArray) > 1) {
                $data = $formDataArray;
                $arraylength = true;
            } else {
                $data = $formDataArray[0];
                $arraylength = false;
            }
            $this->db->order_by("id", "DESC");
            $getdata = $this->db->get_where("pts_trn_material_retreival_relocation", array("room_code" => $data['room_code'], "material_code" => $data['material_code'], "material_batch_no" => $data['material_batch_no'], "material_type" => $data['material_type'], "material_condition" => "in", "status" => "active", "in_status" => '0'))->row_array();

            if (!empty($getdata) && $data['material_condition'] == 'in') {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'This product is already in please make out.', 'acttblid' => $getdata);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                if (empty($getdata) && $data['material_condition'] == 'out') {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please make this material in first then perform out Activity', 'acttblid' => $getdata);
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $data2 = array("activity_name"=>$material_condition,"activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $data['material_code'], "batch_no" => $data['material_batch_no'], "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "versionid" => $headerRecordid, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step);
                    $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');
                    $insertAudit = array(
                        "room_code" => $roomcode,
                        "act_id" => $actid,
                        "process_id" => $processid,
                        "act_name" => $actname,
                        "pre_product_code" => $product_no,
                        "pre_barch_no" => $batch_no,
                        "done_by_user_id" => $empid,
                        "done_by_user_name" => $empname,
                        "done_by_role_id" => $roleid,
                        "done_by_email" => $email,
                        "done_by_remark" => $remark,
                        "is_in_workflow" => 'no'
                    );
                    $result = $this->Ptscommon->matRetrivalRecord($isworkflow, 'pts_trn_material_retreival_relocation', $data, $data2, $data3, $insertAudit);
                    if (isset($result) && !empty($result) && $result['actlogtblid'] > 0) {
                        if ($next_step > 0) {
                            $this->commonEmailFunction($result['actlogtblid']);
                        }

                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result['actlogtblid']);
                        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    } else {
                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    }
                }
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function update_mat_retreival_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';

        $material_code = isset($_POST['material_code']) ? $_POST['material_code'] : '';
        $material_batch_no = isset($_POST['material_batch_no']) ? $_POST['material_batch_no'] : '';
        $material_condition = isset($_POST['material_condition']) ? $_POST['material_condition'] : '';
        $material_type = isset($_POST['material_type']) ? $_POST['material_type'] : '';
        $update_id = isset($_POST['update_id']) ? $_POST['update_id'] : '';
        $is_in_workflow = isset($_POST['is_in_workflow']) ? $_POST['is_in_workflow'] : '';

        if ($doc_no == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $docno = $doc_row['docno'] + 1;
            $doc_num = $doc_pre . $docno;
        } else {
            $doc_num = $doc_no;
        }
        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        $data = array(
            "doc_no" => $doc_num,
            "room_code" => $roomcode,
            "material_condition" => $material_condition,
            "material_batch_no" => $material_batch_no,
            "material_type" => $material_type,
            "material_code" => $material_code,
            "is_in_workflow" => $is_in_workflow,
            "updated_by" => $empid,
            "updated_on" => date("Y-m-d H:i:s"),
            "update_remark" => $remark
        );

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
//        print_r($wfres->row_array());
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleid, 'pts_mst_role_workassign_to_mult_roles');
//            print_r($res2->result());exit;
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }
//        echo $candone;echo $isroomin;exit;
        if ($candone == 1 && $isroomin == 1) {
            $where = array("id" => $update_id);
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_user_activity_log', array("product_code" => $material_code, "batch_no" => $material_batch_no), array("doc_id" => $doc_num));
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_material_retreival_relocation', $data, $where);
            if ($result > 0) {
                $data = array(
                    "doc_no" => $doc_num,
                    "room_code" => $roomcode,
                    "act_id" => $actid,
                    "process_id" => $processid,
                    "act_name" => $actname,
                    "material_code" => $material_code,
                    "material_batch_no" => $material_batch_no,
                    "done_by_user_id" => $empid,
                    "done_by_user_name" => $empname,
                    "done_by_role_id" => $roleid,
                    "done_by_email" => $email,
                    "done_by_remark" => $remark,
                    "is_in_workflow" => 'no'
                );
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_num;
                $uniquefiled2 = "material_code:" . $material_code;
                $uniquefiled3 = "material_batch_no:" . $material_batch_no;
                $activity_id = $actid;
                $activityData = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array('doc_id' => $doc_no));
                if (!empty($activityData)) {
                    $activity_id = $activityData[0]['id'];
                }
                $auditParams = array(
                    'command_type' => 'update',
                    'created_by' => $empid,
                    'created_by_name' => $empname,
                    'activity_name' => $actname,
                    'type' => 'material_retreival_relocation',
                    'uniquefiled1' => $uniquefiled1,
                    'uniquefiled2' => $uniquefiled2,
                    'uniquefiled3' => $uniquefiled3,
                    'table_name' => 'pts_trn_material_retreival_relocation',
                    'primary_id' => $update_id,
                    'log_user_activity_id' => $activity_id,
                    'post_params' => $data
                );
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Updated.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to update this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to Check Material Status, Is it in or out
     * Created By : Bhupendra
     * Created Date : 5May2020
     */

    public function getmatindtl() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $matcode = isset($_POST['matcode']) ? $_POST['matcode'] : '';
        $matbatchcode = isset($_POST['matbatchcode']) ? $_POST['matbatchcode'] : '';
        $res = $this->Ptscommon->GetMatRecord($matcode, $matbatchcode);

        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {

            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Audit Trail code by Khushboo
    public function submit_swab_sample_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $ar_number = isset($_POST['ar_number']) ? $_POST['ar_number'] : '';
        $equipment_code = isset($_POST['equipment_code']) ? $_POST['equipment_code'] : '';
        $equipment_desc = isset($_POST['equipment_desc']) ? $_POST['equipment_desc'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 1;
        $candone = 1;
        if ($candone == 1 && $isroomin == 1) {

            $data = array("room_code" => $roomcode, "ar_numer" => $ar_number, "equipment_id" => $equipment_code, "equipment_desc" => $equipment_desc, "pre_product_code" => $product_no, "pre_barch_no" => $batch_no, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no');

            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2,"versionid" => $headerRecordid, "activity_start" => time(), "workflowstatus" => $roleid, "workflownextstep" => "-1");

            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'stop');

            $result = $this->Ptscommon->swabLogInsert($isworkflow, 'pts_trn_swab_sample_record', $data, $data2, $data3);
            if (isset($result) && !empty($result) && $result['actlogtblid'] > 0 ) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result['actlogtblid']);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Audit Trail Code By Khushboo
    public function update_swab_sample_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $ar_number = isset($_POST['ar_number']) ? $_POST['ar_number'] : '';
        $equipment_code = isset($_POST['equipment_code']) ? $_POST['equipment_code'] : '';
        $equipment_desc = isset($_POST['equipment_desc']) ? $_POST['equipment_desc'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $update_id = isset($_POST['update_id']) ? $_POST['update_id'] : '';
        $is_in_workflow = isset($_POST['is_in_workflow']) ? $_POST['is_in_workflow'] : '';

        if ($doc_no == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $docno = $doc_row['docno'] + 1;
            $doc_no = $doc_pre . $docno;
        } else {
            $doc_no = $doc_no;
        }

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleid, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {
            $where = array("id" => $update_id);
            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "ar_numer" => $ar_number, "equipment_id" => $equipment_code, "equipment_desc" => $equipment_desc, "pre_product_code" => $product_no, "pre_barch_no" => $batch_no, "is_in_workflow" => $is_in_workflow, "updated_by" => $empid, "updated_on" => date("Y-m-d H:i:s"), "update_remark" => $remark);
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_user_activity_log', array("product_code" => $product_no, "batch_no" => $batch_no), array("doc_id" => $doc_no));
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_swab_sample_record', $data, $where);

            if ($result > 0) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "equipment_id:" . $equipment_code;
                $uniquefiled3 = "pre_barch_no:" . $batch_no;
                $activityData = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array('doc_id' => $doc_no));
                if (!empty($activityData)) {
                    $activity_id = $activityData[0]['id'];
                }
                $auditParams = array('command_type' => 'update', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'Swab Sample Record', 'type' => 'swab_sample_record', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_swab_sample_record', 'primary_id' => $update_id, 'log_user_activity_id' => $activity_id, 'post_params' => $data);
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Updated.', 'acttblid' => "");
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to update this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //code with audit trail code by bittu
    public function submit_pre_filter_cleaning_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $product_code = isset($_POST['product_code']) ? $_POST['product_code'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $seletion_status = isset($_POST['seletion_status']) ? $_POST['seletion_status'] : '';
        $pre_filter_cleaning = isset($_POST['pre_filter_cleaning']) ? $_POST['pre_filter_cleaning'] : '';
        $outer_surface_cleaning = isset($_POST['outer_surface_cleaning']) ? $_POST['outer_surface_cleaning'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        $isworkflow = 0; $workFlow = 'no';
        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workFlow = 'yes';
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {//$data["doc_no"];$data2['doc_id']= $doc_no
            $data = ["room_code" => $roomcode, "selection_status" => $seletion_status, "pre_filter_cleaning" => $pre_filter_cleaning, "outer_surface_cleaning" => $outer_surface_cleaning, "product_code" => $product_code, "batch_no" => $batch_no, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => $workFlow];
            $data2 = array("activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_code, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "versionid" => $headerRecordid, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step, "selection_status" => $seletion_status);
            $data3 = array("approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start');
            $uniquefiled2 = "product_code:" . $product_code;
            $uniquefiled3 = "pre_barch_no:" . $batch_no;
            $auditParams = ['command_type' => 'insert','created_by' => $empid,'created_by_name' => $empname,'activity_name' => 'Pre-Filter Cleaning Record of LAF',
                'type' => 'pre_filter_cleaning','uniquefiled2' => $uniquefiled2,'uniquefiled3' => $uniquefiled3,
                'table_name' => 'pts_trn_pre_filter_cleaning',
            ];
            $result = $this->Ptscommon->newFunctionToSubmitPreFilterCleaningRecord($data, $data2, $data3, $auditParams);
            if ($result > 0) {
                if ($next_step > 0 && $seletion_status == 'to') {
                    $this->commonEmailFunction($result);
                }
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //code with audit trail code by bittu
    public function update_pre_filter_cleaning_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $product_code = isset($_POST['product_code']) ? $_POST['product_code'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $seletion_status = isset($_POST['seletion_status']) ? $_POST['seletion_status'] : '';
        $pre_filter_cleaning = isset($_POST['pre_filter_cleaning']) ? $_POST['pre_filter_cleaning'] : '';
        $outer_surface_cleaning = isset($_POST['outer_surface_cleaning']) ? $_POST['outer_surface_cleaning'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $update_id = isset($_POST['update_id']) ? $_POST['update_id'] : '';
        $is_in_workflow = isset($_POST['is_in_workflow']) ? $_POST['is_in_workflow'] : '';

        if ($doc_no == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $docno = $doc_row['docno'] + 1;
            $doc_no = $doc_pre . $docno;
        } else {
            $doc_no = $doc_no;
        }

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleid, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {
            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "selection_status" => $seletion_status, "pre_filter_cleaning" => $pre_filter_cleaning, "outer_surface_cleaning" => $outer_surface_cleaning, "product_code" => $product_code, "batch_no" => $batch_no, "is_in_workflow" => $is_in_workflow, "updated_by" => $empid, "updated_on" => date("Y-m-d H:i:s"), "update_remark" => $remark);
            $where = array("id" => $update_id);
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_user_activity_log', array("product_code" => $product_code, "batch_no" => $batch_no), array("doc_id" => $doc_no));
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_pre_filter_cleaning', $data, $where);
            if ($result > 0) {
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "product_code:" . $product_code;
                $uniquefiled3 = "pre_barch_no:" . $batch_no;
                $activityData = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array('doc_id' => $doc_no));
                if (!empty($activityData)) {
                    $activity_id = $activityData[0]['id'];
                }
                $auditParams = array(
                    'command_type' => 'update',
                    'created_by' => $empid,
                    'created_by_name' => $empname,
                    'activity_name' => 'Pre-Filter Cleaning Record of LAF',
                    'type' => 'pre_filter_cleaning',
                    'uniquefiled1' => $uniquefiled1,
                    'uniquefiled2' => $uniquefiled2,
                    'uniquefiled3' => $uniquefiled3,
                    'table_name' => 'pts_trn_pre_filter_cleaning',
                    'primary_id' => $update_id,
                    'log_user_activity_id' => $activity_id,
                    'post_params' => $data
                );
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Updated.', 'acttblid' => "");
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to update this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_tablet_tooling_log_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $product_code = isset($_POST['product_code']) ? $_POST['product_code'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $mst_mat_id = isset($_POST['mst_mat_id']) ? $_POST['mst_mat_id'] : '';
        $currentActivity = isset($_POST['currentActivity']) ? $_POST['currentActivity'] : '';
        $U = isset($_POST['U']) ? $_POST['U'] : '';
        $L = isset($_POST['L']) ? $_POST['L'] : '';
        $D = isset($_POST['D']) ? $_POST['D'] : '';
        $Damagelist = !empty($_POST['Damage']) ? json_decode($_POST['Damage'], true) : '';
        $Damage = !empty($Damagelist) ? implode(",", $Damagelist) : '';

        $from = isset($_POST['from']) ? $_POST['from'] : '';
        $to = isset($_POST['to']) ? $_POST['to'] : '';
        $bno = isset($_POST['bno']) ? $_POST['bno'] : '';
        $tabqty = isset($_POST['tabqty']) ? $_POST['tabqty'] : '';
        $cumqty = isset($_POST['cumqty']) ? $_POST['cumqty'] : '';
        $cumqtypunch = isset($_POST['cumqtypunch']) ? $_POST['cumqtypunch'] : '';
        $punch_set = isset($_POST['punch_set']) ? $_POST['punch_set'] : '';
        $Subset = isset($_POST['Subset']) ? $_POST['Subset'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $product_code = isset($_POST['product_code']) ? $_POST['product_code'] : "";
        $tt_actid = isset($_POST['tt_actid']) ? $_POST['tt_actid'] : "";
        $supplier = isset($_POST['supplier']) ? $_POST['supplier'] : "";
        $remark2 = $actname . ' is in progress';
        $isworkflow = 0;
        $workFlow = 'no';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';
        if ($tt_actid >= 10) {
            $checkPreviousActivity = $this->db->get_where("pts_trn_punch_set_allocation", array("room_code" => $roomcode, "product_code" => $product_code, "punch_set" => $punch_set, 'supplier' => $supplier))->row_array();
            if ($checkPreviousActivity['act_id'] < 9) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'First Complete Cleaning Before Use and than start this.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workFlow = 'yes';
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = -1;
            $isworkflow = 1;
        }

        if ($candone == 1 && $isroomin == 1) {
            $data = ["room_code" => $roomcode, "mst_matrial_id" => $mst_mat_id, "act_type" => $currentActivity, "U" => $U, "L" => $L, "D" => $D, "tablet_tooling_from" => $from, "tablet_tooling_to" => $to, "damaged" => $Damage, "b_no" => $bno, "tab_qty" => $tabqty, "cum_qty" => $cumqty, "cum_qty_punch" => $cumqtypunch, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => $workFlow, "product_code" => $product_code, "punch_set_number" => $punch_set, "subset" => $Subset];
            $data2 = ["activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_code, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step, "versionid" => $headerRecordid];
            $data3 = ["approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start'];
            $uniquefiled2 = "product_code:" . $product_code;
            $auditParams = ['command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'Tablet Tooling Log Card', 'type' => 'Tablet Tooling Log Card', 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => "", 'table_name' => 'pts_trn_tablet_tooling'];
            $result = $this->Ptscommon->newFunctionToSubmitTabletToolingRecordWithOutWorkFlow($data, $data2, $data3, $auditParams, $tt_actid, $Damagelist);
            if ($result['acttblid'] > 0) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.','acttblid' => '');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_tablet_tooling_log_record_withworkflow() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $product_code = isset($_POST['product_code']) ? $_POST['product_code'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $mst_mat_id = isset($_POST['mst_mat_id']) ? $_POST['mst_mat_id'] : '';
        $currentActivity = isset($_POST['currentActivity']) ? $_POST['currentActivity'] : '';
        $U = isset($_POST['U']) ? $_POST['U'] : '';
        $L = isset($_POST['L']) ? $_POST['L'] : '';
        $D = isset($_POST['D']) ? $_POST['D'] : '';
        // $Damage = isset($_POST['Damage']) ? $_POST['Damage'] : '';
        $Damagelist = !empty($_POST['Damage']) ? json_decode($_POST['Damage'], true) : '';
        $Damage = !empty($Damagelist) ? implode(",", $Damagelist) : '';
        $from = isset($_POST['from']) ? $_POST['from'] : '';
        $to = isset($_POST['to']) ? $_POST['to'] : '';
        $bno = isset($_POST['bno']) ? $_POST['bno'] : '';
        $tabqty = isset($_POST['tabqty']) ? $_POST['tabqty'] : '';
        $cumqty = isset($_POST['cumqty']) ? $_POST['cumqty'] : '';
        $cumqtypunch = isset($_POST['cumqtypunch']) ? $_POST['cumqtypunch'] : '';
        $punch_set = isset($_POST['punch_set']) ? $_POST['punch_set'] : '';
        $Subset = isset($_POST['Subset']) ? $_POST['Subset'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $product_code = isset($_POST['product_code']) ? $_POST['product_code'] : "";
        $tt_actid = isset($_POST['tt_actid']) ? $_POST['tt_actid'] : "";
        $supplier = isset($_POST['supplier']) ? $_POST['supplier'] : "";
        $remark2 = $actname . ' is in progress';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';
        $isworkflow = 0;
        $workFlow = 'no';
//        if ($tt_actid >= 10) {
//            $checkPreviousActivity = $this->db->get_where("pts_trn_punch_set_allocation", array("room_code" => $roomcode, "product_code" => $product_code, "punch_set" => $punch_set, 'supplier' => $supplier))->row_array();
//            if ($checkPreviousActivity['act_id'] < 9) {
//                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'First Complete Cleaning Before Use and than start this.');
//                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
//            }
//        }
        if ($tt_actid >= 10) {
            $checkPreviousActivity = $this->db->get_where("pts_trn_punch_set_allocation", array("room_code" => $roomcode, "product_code" => $product_code, "punch_set" => $punch_set, 'supplier' => $supplier))->row_array();
            if(!empty($checkPreviousActivity)){
                if ($checkPreviousActivity['act_id'] < 9) {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'First Complete Cleaning Before Use and than start this.');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }else{
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'First Do Punch Set Allocation');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }


        
        $inprogress = $this->db->get_where("pts_trn_tablet_tooling", array("room_code" => $roomcode, "product_code" => $product_code, "punch_set_number" => $punch_set, "is_in_workflow" => 'yes'))->row_array();
        if (!empty($inprogress)) {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'First Complete Previous Activity and than start  next.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }

        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workFlow = 'yes';
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {
            $data = ["room_code" => $roomcode, "mst_matrial_id" => $mst_mat_id, "act_type" => $currentActivity, "U" => $U, "L" => $L, "D" => $D, "tablet_tooling_from" => $from, "tablet_tooling_to" => $to, "damaged" => $Damage, "b_no" => $bno, "tab_qty" => $tabqty, "cum_qty" => $cumqty, "cum_qty_punch" => $cumqtypunch, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => $workFlow, "product_code" => $product_code, "punch_set_number" => $punch_set, "subset" => $Subset];
            $data2 = ["activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_code, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step, "versionid" => $headerRecordid];
            $data3 = ["approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start'];
            $uniquefiled2 = "product_code:" . $product_code;
            $auditParams = ['command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'Tablet Tooling Log Card', 'type' => 'Tablet Tooling Log Card', 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => "", 'table_name' => 'pts_trn_tablet_tooling'];
            $result = $this->Ptscommon->newFunctionToSubmitTabletToolingRecordWithWorkFlow($data, $data2, $data3, $auditParams, $tt_actid, $Damagelist);
            if ($result['acttblid'] > 0) {
                if ($next_step > 0) {
                    $this->commonEmailFunction($result['acttblid']);
                }
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.','acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function Update_tablet_tooling_log_record_withworkflow() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $product_code = isset($_POST['product_code']) ? $_POST['product_code'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $mst_mat_id = isset($_POST['mst_mat_id']) ? $_POST['mst_mat_id'] : '';
        $currentActivity = isset($_POST['currentActivity']) ? $_POST['currentActivity'] : '';
        $U = isset($_POST['U']) ? $_POST['U'] : '';
        $L = isset($_POST['L']) ? $_POST['L'] : '';
        $D = isset($_POST['D']) ? $_POST['D'] : '';
        $Damage = isset($_POST['Damage']) ? $_POST['Damage'] : '';
        $from = isset($_POST['from']) ? $_POST['from'] : '';
        $to = isset($_POST['to']) ? $_POST['to'] : '';
        $bno = isset($_POST['bno']) ? $_POST['bno'] : '';
        $tabqty = isset($_POST['tabqty']) ? $_POST['tabqty'] : '';
        $cumqty = isset($_POST['cumqty']) ? $_POST['cumqty'] : '';
        $cumqtypunch = isset($_POST['cumqtypunch']) ? $_POST['cumqtypunch'] : '';
        $punch_set = isset($_POST['punch_set']) ? $_POST['punch_set'] : '';
        $Subset = isset($_POST['Subset']) ? $_POST['Subset'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $product_code = isset($_POST['product_code']) ? $_POST['product_code'] : "";
        $remark2 = $actname . ' is in progress';
        $update_id = isset($_POST['update_id']) ? $_POST['update_id'] : '';
        $is_in_workflow = isset($_POST['is_in_workflow']) ? $_POST['is_in_workflow'] : '';
        if ($doc_no == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $docno = $doc_row['docno'] + 1;
            $doc_no = $doc_pre . $docno;
        } else {
            $doc_no = $doc_no;
        }

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        $isworkflow = 0;
        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['next_step'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleid, 'pts_mst_role_workassign_to_mult_roles');
//            echo $roleid.'&&777'.print_r($res2->result());exit;
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }
        if ($candone == 1 && $isroomin == 1) {
            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "mst_matrial_id" => $mst_mat_id, "act_type" => $currentActivity, "U" => $U, "L" => $L, "D" => $D, "tablet_tooling_from" => $from, "tablet_tooling_to" => $to, "damaged" => $Damage, "b_no" => $bno, "tab_qty" => $tabqty, "cum_qty" => $cumqty, "cum_qty_punch" => $cumqtypunch, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no', "product_code" => $product_code, "is_in_workflow" => $is_in_workflow, "updated_by" => $empid, "updated_on" => date("Y-m-d H:i:s"), "update_remark" => $remark);
            $where = array("id" => $update_id);
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_user_activity_log', array("product_code" => $product_code, "batch_no" => $batch_no), array("doc_id" => $doc_no));
            unset($data['updated_by']);unset($data['updated_on']);unset($data['update_remark']);
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_tablet_tooling', $data, $where);
            if ($result > 0) {
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "product_code:" . $product_code;
                $uniquefiled3 = "pre_barch_no:" . $batch_no;
                $activityData = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array('doc_id' => $doc_no));
                if (!empty($activityData)) {
                    $activity_id = $activityData[0]['id'];
                }
                $auditParams = array(
                    'command_type' => 'update',
                    'created_by' => $empid,
                    'created_by_name' => $empname,
                    'activity_name' => 'Pre-Filter Cleaning Record of LAF',
                    'type' => 'pre_filter_cleaning',
                    'uniquefiled1' => $uniquefiled1,
                    'uniquefiled2' => $uniquefiled2,
                    'uniquefiled3' => $uniquefiled3,
                    'table_name' => 'pts_trn_pre_filter_cleaning',
                    'primary_id' => $update_id,
                    'log_user_activity_id' => $activity_id,
                    'post_params' => $data
                );
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Updated.', 'acttblid' => "");
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            //echo $candone;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to Updated this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_instrument_log_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';
        $instrument_code = isset($_POST['instrument_code']) ? $_POST['instrument_code'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $stage = isset($_POST['stage']) ? $_POST['stage'] : '';
        $test = isset($_POST['test']) ? $_POST['test'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';
        $equipment_data = $this->db->get_where("pts_mst_instrument", array("equipment_code" => $instrument_code, "is_inused" => 1, "status" => 'active'))->row_array();
        if (!empty($equipment_data)) {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you have already used this equipment, Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 1;
        $candone = 1;
        if ($candone == 1 && $isroomin == 1) {
            $data = ["room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "instrument_code" => $instrument_code, "stage" => $stage, "test" => $test, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'yes'];
            $data2 = ["activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => $instrument_code, "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2, "activity_start" => time(), "workflowstatus" => $roleid, "workflownextstep" => $roleid, "versionid" => $headerRecordid];
            $data3 = ["approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start'];
            $uniquefiled2 = "instrument_code:" . $instrument_code;
            $uniquefiled3 = "batch_no:" . $batch_no;
            $auditParams = ['command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'Instrument Log Register', 'type' => 'instrument_log_register', 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_instrument_log_register'];
            $result = $this->Ptscommon->newFunctionToSubmitInstrumentLog($data, $data2, $data3, $auditParams);
            if ($result > 0) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Function for update Instrument log Register
    /*     * Developer Name : Rahul Chauhan
     * Date:04/05/2020
     */
    //Audit Trail Code By Khushboo
    public function update_instrument_log_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';
        $instrument_code = isset($_POST['instrument_code']) ? $_POST['instrument_code'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $stage = isset($_POST['stage']) ? $_POST['stage'] : '';
        $test = isset($_POST['test']) ? $_POST['test'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['update_id']) ? $_POST['update_id'] : '';
        $remark2 = $actname . ' is in progress';
        $update_id = isset($_POST['update_id']) ? $_POST['update_id'] : '';
        $is_in_workflow = isset($_POST['is_in_workflow']) ? $_POST['is_in_workflow'] : '';

        if ($doc_no == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $docno = $doc_row['docno'] + 1;
            $doc_no = $doc_pre . $docno;
        } else {
            $doc_no = $doc_no;
        }

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 1;
        $candone = 1;
        if ($candone == 1 && $isroomin == 1) {
            $where = array("id" => $update_id);
            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "instrument_code" => $instrument_code, "stage" => $stage, "test" => $test, "is_in_workflow" => $is_in_workflow, "updated_by" => $empid, "updated_on" => date("Y-m-d H:i:s"), "update_remark" => $remark);
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_user_activity_log', array("product_code" => $product_no, "batch_no" => $batch_no), array("doc_id" => $doc_no));
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_instrument_log_register', $data, $where);
            if ($result > 0) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "instrument_code:" . $instrument_code;
                $uniquefiled3 = "batch_no:" . $batch_no;
                $activityData = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array('doc_id' => $doc_no));
                if (!empty($activityData)) {
                    $activity_id = $activityData[0]['id'];
                }
                $auditParams = array(
                    'command_type' => 'update',
                    'created_by' => $empid,
                    'created_by_name' => $empname,
                    'activity_name' => 'Instrument Log Register',
                    'type' => 'instrument_log_register',
                    'uniquefiled1' => $uniquefiled1,
                    'uniquefiled2' => $uniquefiled2,
                    'uniquefiled3' => $uniquefiled3,
                    'table_name' => 'pts_trn_instrument_log_register',
                    'primary_id' => $update_id,
                    'log_user_activity_id' => $activity_id,
                    'post_params' => $data
                );
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Updated.', 'acttblid' => "");
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to update this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */
    /*     * **Appoval function for instrument log and equipment Apparatus log Register******* */

    public function com_no_approval_fun() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $actlogtblid = isset($_POST['actlogtblid']) ? $_POST['actlogtblid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $workflowstatus = isset($_POST['workflowstatus']) ? $_POST['workflowstatus'] : '';
        $workflownextstep = isset($_POST['workflownextstep']) ? $_POST['workflownextstep'] : '';
        $actstatus = isset($_POST['actstatus']) ? $_POST['actstatus'] : '';
        $result_rgt = isset($_POST['result_rgt']) ? $_POST['result_rgt'] : '';
        $result_lft = isset($_POST['result_lft']) ? $_POST['result_lft'] : '';
        $docno = isset($_POST['docno']) ? $_POST['docno'] : '';
        $tblname = isset($_POST['tblname']) ? $_POST['tblname'] : '';


        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        $next_step = -1;
        $candone = 1;
        if ($candone == 1 && $isroomin == 1) {
            $result = $this->Ptscommon->Sentfornextapproval($actlogtblid, $actid, $roleid, $remark, $roleid, $next_step, $empid, $empname, $email, $actstatus, $docno, $tblname, $result_rgt, $result_lft);
            if ($result == TRUE) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Stopped Successfully', 'next_step' => $next_step);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to Approved this record.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to Approved this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ************End***************** */


    /*     * **Appoval function for instrument log and equipment Apparatus log Register******* */

    public function com_no_approval_fun2() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $actlogtblid = isset($_POST['actlogtblid']) ? $_POST['actlogtblid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $workflowstatus = isset($_POST['workflowstatus']) ? $_POST['workflowstatus'] : '';
        $workflownextstep = isset($_POST['workflownextstep']) ? $_POST['workflownextstep'] : '';
        $actstatus = isset($_POST['actstatus']) ? $_POST['actstatus'] : '';
        $result_rgt = isset($_POST['result_rgt']) ? $_POST['result_rgt'] : '';
        $result_lft = isset($_POST['result_lft']) ? $_POST['result_lft'] : '';
        $docno = isset($_POST['docno']) ? $_POST['docno'] : '';
        $tblname = isset($_POST['tblname']) ? $_POST['tblname'] : '';

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        $next_step = -1;
        $candone = 1;
        if ($candone == 1 && $isroomin == 1) {
            $result = $this->Ptscommon->Sentfornextapproval($actlogtblid, $actid, $roleid, $remark, $roleid, $next_step, $empid, $empname, $email, $actstatus, $docno, $tblname, $result_rgt, $result_lft);
            if ($result == TRUE) {

                //Making equipment in unused
                $res = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array("id" => $actlogtblid));
                if (!empty($res)) {
                    $equipmentcode = $res[0]['equip_code'];
                    if ($equipmentcode != '' && $equipmentcode != NULL) {
                        $post = array('is_inused' => 0);
                        $param = array("equipment_code" => $equipmentcode);
                        $this->Ptscommon->updateToTable($db = 'default', 'pts_mst_instrument', $post, $param);
                    }
                }
                //End

                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Stopped Successfully', 'next_step' => $next_step);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to Approved this record.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to Approved this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ************End***************** */

    /**
     * Get takeover of progress activities
     */
    public function gettakeover() {
        $headers = apache_request_headers(); // fetch header value
        $actlogtblid = isset($_POST['actlogtblid']) ? $_POST['actlogtblid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $curr_url = isset($_GET['curr_url']) ? $_GET['curr_url'] : '';
        //Check is the person room in before perform an operation 
        //if ($curr_url != 'inprogress_activity') {
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        // } else {
        //     $isroomin = 1;
        // }

        if ($isroomin == 1) {
            $result = $this->Ptscommon->Senttotakeover($actlogtblid, $actid, $roleid, $remark, $empid, $empname, $email);

            if ($result == TRUE) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Takken by you.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
    }

    /*     * ********************** End ************************** */

    // End

    /**
     * Add a Material Relocation
     * @param string $method1
     * @param string $tokencheck
     */
    public function Addmaterialrelocation() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $material_name = isset($_POST['material_name']) ? $_POST['material_name'] : '';
        $material_desc = isset($_POST['material_desc']) ? $_POST['material_desc'] : '';
        $room_code = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $material_type = isset($_POST['material_type']) ? $_POST['material_type'] : '';
        $material_batch_no = isset($_POST['material_batch_no']) ? $_POST['material_batch_no'] : '';
        $material_relocation_type = isset($_POST['material_relocation_type']) ? $_POST['material_relocation_type'] : '';
        $material_code = isset($_POST['material_code']) ? $_POST['material_code'] : '';

        $this->Apifunction->check_variables($material_name, "Material name", $numricCheck = 'no');
        $this->Apifunction->check_variables($room_code, "Room Code", $numricCheck = 'no');
        $this->Apifunction->check_variables($material_type, "Serial Type", $numricCheck = 'no');
        $this->Apifunction->check_variables($material_batch_no, "Material batch no", $numricCheck = 'no');
        $this->Apifunction->check_variables($material_relocation_type, "Material Type", $material_relocation_type = 'no');
        $this->Apifunction->check_variables($material_relocation_type, "Material Relocation Type", $numricCheck = 'no');
        $this->Apifunction->check_variables($material_code, "Material Code", $numricCheck = 'no');

        $post = array("material_desc" => $material_desc, "material_relocation_type" => $material_relocation_type, "material_batch_no" => $material_batch_no, "material_name" => $material_name, "room_code" => $room_code, "material_type" => $material_type, "material_code" => $material_code);
        $res = $this->Ptscommon->addToTable($db = 'default', 'pts_mst_material_relocation', $post, $param = '');
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Material Relocation Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'aterial Relocation Does Not Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End

    /**
     * Get a list of Material Relocation
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getmaterialrelocation() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $param = array();
        if ($room_code != '') {
            $param = array("room_code" => $room_code);
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_material_relocation', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "material_relocation" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Material Relocation Found', "material_relocation" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Roomwise Vaccum Cleaner List
     * Created By :Rahul Chauhan
     * Date:03-02-2020
     */
    public function GetVaccumCleanerlist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $param = array("is_active" => "1", "is_inused" => 0, "equipment_type" => "Vacuum");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "vaccum_cleaner_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Vacuum Cleaner Found', "vaccum_cleaner_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Roomwise Dispensing Booth List
     * Created By :Rahul Chauhan
     * Date:03-02-2020
     */
    public function GetDispensingBoothList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $param = array("is_active" => "1", "room_code" => $room_code, "is_inused" => 0, "equipment_type" => "Booth");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "booth_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Dispensing Booth Found', "booth_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Add Balance calibration Master
     * Created By :Rahul Chauhan
     * Date:03-02-2020
     */
    public function AddRoomBalanceCalibration() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $balance_id = isset($_POST['balance_id']) ? $_POST['balance_id'] : 0;
        $balance_no = isset($_POST['balance_no']) ? $_POST['balance_no'] : '';
        $capacity = isset($_POST['capacity']) ? $_POST['capacity'] : '';
        $accep_limit = isset($_POST['accep_limit']) ? $_POST['accep_limit'] : '';
        $least_count = isset($_POST['least_count']) ? $_POST['least_count'] : '';
        $standard_data = isset($_POST['standard_data']) ? $_POST['standard_data'] : '';
        $status = isset($_POST['status']) ? $_POST['status'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $room_id = isset($_POST['room_id']) ? $_POST['room_id'] : '';
        $room_code = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $uom = substr($capacity, -2);
        $orgnlCapacity = $this->Apifunction->check_variables($balance_no, "Balance No", $numricCheck = 'no');
        $this->Apifunction->check_variables($capacity, "Capacity", $numricCheck = 'no');
        $this->Apifunction->check_variables($least_count, "Least Count", $numricCheck = 'no');
        $post = ["room_code" => $room_code, "room_id" => $room_id, "balance_no" => $balance_no, "capacity" => $capacity, "least_count" => $least_count];
        $audit_array = ["balance_no" => $balance_no, "room_code" => $room_code, "capacity" => substr($capacity, 0, -2), "uom" => $uom, "least_count" => $least_count];
        $update = false;
        $old_least_count = '';
        if ($balance_id > 0) {
            $param = array("id" => $balance_id);
            if ($status == 'true') {
                $post['status'] = "active";
            } else {
                $post['status'] = "inactive";
            }
            $this->db->where("is_in_workflow", 'yes');
            $logData = $this->db->get_where("pts_trn_balance_calibration", array("balance_no" => $balance_no))->result_array();
            $oldDataFromMainTable = $this->db->get_where("pts_mst_balance_calibration", array("id" => $balance_id))->row_array();
            if (!empty($oldDataFromMainTable)) {
                $old_least_count = $oldDataFromMainTable['least_count'];
            }else{
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'No such balance exists.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
            //if (!empty($logData) && $status == 'false') {
            if (!empty($logData)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'You can not update this Balance No.Some Logs are in progress.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
            $post['remark'] = $remark;
            $post['modified_on'] = date('Y-m-d H:i:s');
            $post['modified_by'] = $this->session->userdata('empname');
            $this->Ptscommon->updateToTable($db = 'default', 'pts_mst_balance_calibration', $post, $param);
            $res = $balance_id;
            $update = true;
        } else {
            $balanceInfo = $this->db->get_where("pts_mst_balance_calibration", array("balance_no" => $balance_no))->row_array();
            if (!empty($balanceInfo)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'This Balance Number already exist.Please try another one');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            } else {
                $post['created_by'] = $this->session->userdata('empname');
                $res = $this->Ptscommon->addToTable($db = 'default', 'pts_mst_balance_calibration', $post, $param = '');
                $update = false;
            }
        }
        $standard_array = json_decode($standard_data, true);
        foreach ($standard_array as $key => $value) {
            $standard_array[$key]['balance_calibration_id'] = $res;
        }
        if (!empty($res)) {
            $weights_added = '';
            $standard_array = json_decode($standard_data, true);
            $i = 1;
            foreach ($standard_array as $key => $value) {
                if ($balance_id > 0) {
                    unset($standard_array[$key]["balance_calibration_id"]);
                    unset($standard_array[$key]["id"]);
                    unset($standard_array[$key]["status"]);
                    unset($standard_array[$key]["created_on"]);
                }
                $standard_array[$key]['balance_calibration_id'] = $res;
                $standard_array[$key]['modified_on'] = date('y-m-d H:i:s');
                $standard_array[$key]['id_standard'] = 0;
                $standard_array[$key]['frequency_id'] = 0;
                $standard_array[$key]['modified_by'] = $this->session->userdata('empname');
                $standard_array[$key]['created_by'] = $this->session->userdata('empname');
                if ($status == 'true') {
                    $standard_array[$key]['status'] = "active";
                } else {
                    $standard_array[$key]['status'] = "inactive";
                }
                $weights_added .= $i . '.measurement_unit:' . $standard_array[$key]['measurement_unit'] . ',standard_weight:' . $standard_array[$key]['standard_value'] . ',max_weight:' . $standard_array[$key]['max_value'] . ',min_weight:' . $standard_array[$key]['min_value'] . ',';
                $i++;
            }
            $weights_added = rtrim($weights_added, ',');
            $old_balance_info = $this->db->get_where("pts_mst_balance_calibration_standard", array("balance_calibration_id" => $balance_id))->result_array();
            $this->db->delete("pts_mst_balance_calibration_standard", array("balance_calibration_id" => $res));
            //echo '<pre>'; print_r($standard_array);exit;
            $this->db->insert_batch('pts_mst_balance_calibration_standard', $standard_array);

            $old_audit_data = [];
            $old_standard_weight_data = '';
            $old_status = 'N/A';
            if (!empty($old_balance_info)) {
                $j = 1;
                foreach ($old_balance_info as $key => $value) {
                    $old_standard_weight_data .= $j . '.measurement_unit:' . $value['measurement_unit'] . ',standard_weight:' . $value['standard_value'] . ',max_weight:' . $standard_array[$key]['max_value'] . ',min_weight:' . $value['min_value'] . ',';
                    $j++;
                }
                $old_standard_weight_data = rtrim($old_standard_weight_data, ',');
                $old_status = ucfirst($old_balance_info[0]['status']);
            }
            if ($update) {
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "balance_no:" . $balance_no;
                $uniquefiled2 = "";
                $uniquefiled3 = "";
                $records = 'Updated Data is: ';
                $audit_array = ["balance_no" => $balance_no, "room_code" => $room_code, "capacity" => substr($capacity, 0, -2), "uom" => $uom, "least_count" => $least_count, 'standard_weigths' => $weights_added, 'status' => ucfirst($post['status']), 'remarks' => $remark];
                $audit_array = ["balance_no" => $balance_no, "room_code" => $room_code, "capacity" => substr($capacity, 0, -2), "uom" => $uom, "least_count" => $least_count, 'standard_weigths' => $weights_added, 'status' => 'Active', 'remarks' => $remark];
                $audit_array = $this->Ptscommon->getNAUpdated($audit_array);
                $master_previous_data = ["room_code" => $room_code, "capacity" => substr($capacity, 0, -2), "uom" => $uom, "least_count" => $old_least_count, 'standard_weigths' => $old_standard_weight_data, 'status' => ucfirst($old_status)];
                $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
                $auditParams = array('command_type' => 'update', 'activity_name' => 'Balance Master', 'type' => 'balance_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_balance_calibration', 'primary_id' => $res, 'post_params' => $audit_array, 'master_previous_data' => $master_previous_data);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */
            } else {
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "balance_no:" . $balance_no;
                $uniquefiled2 = "";
                $uniquefiled3 = "";
                $audit_array = ["balance_no" => $balance_no, "room_code" => $room_code, "capacity" => substr($capacity, 0, -2), "uom" => $uom, "least_count" => $least_count, 'standard_weigths' => $weights_added, 'status' => 'Active', 'remarks' => $remark];
                $audit_array = $this->Ptscommon->getNAUpdated($audit_array);
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Balance Master', 'type' => 'balance_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_balance_calibration', 'primary_id' => $res, 'post_params' => $audit_array);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */
            }

            if ($update) {
                $msg = "Balance Master Updated Successfully";
            } else {
                $msg = "Balance Master added Successfully";
            }
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => $msg);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Balance Calibration Does Not Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //End of Bhupendra's code

    /**
     * Add Balance Frequency Master  Data
     * Created By :Rahul Chauhan
     * Date:27-02-2020 
     */
    public function SaveFrequencyBalance() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $balance_no = isset($_POST['balance_no']) ? $_POST['balance_no'] : '';
        $frequency_id = isset($_POST['frequency_id']) ? $_POST['frequency_id'] : '';
        $standard_data = isset($_POST['standard_data']) ? $_POST['standard_data'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $recordid = isset($_POST['recordid']) ? $_POST['recordid'] : '';
        if ($_POST['status'] == "true") {
            $active = "active";
        } else if ($_POST['status'] == "false") {
            $active = "inactive";
        }
        $frequency_data = $this->db->get_where("pts_mst_frequency", ['id' => $frequency_id])->row_array();
        $frequency_name = !empty($frequency_data) ? $frequency_data['frequency_name'] : 'N/A';
        $balanceData = $this->db->get_where("pts_mst_balance_frequency_data", array("balance_no" => $balance_no, "frequency_id" => $frequency_id))->result_array();
        if (!empty($balanceData) && $recordid == '') {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Balance Calibration Frequency alredy exist on this Balance No.', "instrument_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
        $this->Apifunction->check_variables($balance_no, "Balance No", $numricCheck = 'no');
        $this->Apifunction->check_variables($frequency_id, "Capacity", $numricCheck = 'no');
        $standard_array = json_decode($standard_data, true);

        $i = 1;
        $new_weights = '';
        foreach ($standard_array as $key => $value) {
            unset($standard_array[$key]["ischecked"]);
            unset($standard_array[$key]["checked"]);
            $standard_array[$key]['calibration_standard'] = $standard_array[$key]["id"];
            unset($standard_array[$key]["id"]);
            $standard_array[$key]['balance_no'] = $balance_no;
            $standard_array[$key]['frequency_id'] = $frequency_id;
            $standard_array[$key]['remark'] = $remark;
            $standard_array[$key]['status'] = $active;
            $standard_array[$key]['created_by'] = $this->session->userdata('empname');
            $standard_array[$key]['modified_by'] = $this->session->userdata('empname');
            $standard_array[$key]['modified_on'] = date('Y-m-d H:i:s');
            $new_weights .= $i . '.measurement_unit:' . $standard_array[$key]['measurement_unit'] . ',standard_weight:' . $standard_array[$key]['standard_value'] . ',max_weight:' . $standard_array[$key]['max_value'] . ',min_weight:' . $standard_array[$key]['min_value'] . ',';
            $i++;
        }
        $new_weights = rtrim($new_weights, ',');
        $auditArray = ['balance_no' => $balance_no, 'frequency' => $frequency_name, "standard_weight" => $new_weights, "status" => ucfirst($active), "remarks" => $remark];
        $auditArray = $this->Ptscommon->getNAUpdated($auditArray);
        $old_status = 'N/A';
        $operation = 'insert';
        $old_weights = '';
        $master_prevoius_data = [];
        if (!empty($balanceData)) {
            $operation = 'update';
            $i = 1;
            foreach ($balanceData as $key => $value) {
                $old_weights .= $i . '.measurement_unit:' . $balanceData[$key]['measurement_unit'] . ',standard_weight:' . $balanceData[$key]['standard_value'] . ',max_weight:' . $balanceData[$key]['max_value'] . ',min_weight:' . $balanceData[$key]['min_value'] . ',';
                $i++;
            }
            $old_weights = rtrim($old_weights, ',');
            $master_prevoius_data = ['standard_weight' => $old_weights, 'status' => ucfirst($balanceData[0]['status'])];
            $master_prevoius_data = $this->Ptscommon->getNAUpdated($master_prevoius_data);
        }

        $this->db->delete("pts_mst_balance_frequency_data", array("balance_no" => $balance_no, "frequency_id" => $frequency_id));
        $res = $this->db->insert_batch('pts_mst_balance_frequency_data', $standard_array);
        if (!empty($res)) {

            /* Start - Insert elog audit histry */
            $uniquefiled1 = "balance_no:" . $balance_no;
            $uniquefiled2 = "frequency_id:" . $frequency_id;
            $uniquefiled3 = "";
            $inserted_id = $this->db->insert_id();
            if ($operation == 'insert') {
                $auditParams = ['command_type' => $operation, 'activity_name' => 'Balance Calibration Frequency', 'type' => 'pts_mst_balance_frequency_data', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_balance_frequency_data', 'primary_id' => $inserted_id, 'post_params' => $auditArray];
            } else {
                $auditParams = array('command_type' => $operation, 'activity_name' => 'Balance Calibration Frequency', 'type' => 'pts_mst_balance_frequency_data', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_balance_frequency_data', 'primary_id' => $inserted_id, 'post_params' => $auditArray, 'master_previous_data' => $master_prevoius_data);
            }

            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */

            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Balance Frequency Added Successfully');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Dispensing Booth Found', "instrument_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Balance Calibration List 
     * Created By :Rahul Chauhan
     * Date:03-02-2020
     */
    public function GetBalanceCalibrationList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $module = isset($_GET['module']) ? $_GET['module'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : null;
        $room = isset($_GET['room']) ? $_GET['room'] : null;
        $id = isset($_GET['balance_id']) ? $_GET['balance_id'] : null;
        $param = array("status" => "active");
        if(!empty($room)){
            $param['room_code']=$room;
        }
        if(!empty($id)){
            $param['id']=$id;
        }
        if ($module == 'master') {
            $param = array();
        }
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = $this->Ptscommon->getRowsCount('default', 'pts_mst_balance_calibration', $param);
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_balance_calibration', $param, $order = null, $page);
        foreach ($res as $key => $value) {
            $param = array("balance_calibration_id" => $value['id']);
            $standardData = array();
            $standardData = $this->Ptscommon->getMultipleRows('default', 'pts_mst_balance_calibration_standard', $param);
            $res[$key]['standard_data'] = $standardData;
            $res[$key]['room_code'] = $this->db->get_where("pts_mst_sys_id",array("room_id"=>$value['room_id']))->row_array()['room_code'];
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "balance_calibration_list" => $res, 'total_records' => $count, 'records_per_page' => $records_per_page);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Balance Calibration Found', "balance_calibration_list" => array(), 'total_records' => 0, 'records_per_page' => $records_per_page);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function Getbalance_info() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $balance_id = isset($_GET['id']) ? $_GET['id'] : '';
        $param = array("status" => "active","id" => $balance_id);
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_balance_calibration', $param);
        foreach ($res as $key => $value) {
            $param = array("balance_calibration_id" => $value['id']);
            $standardData = array();
            $standardData = $this->Ptscommon->getMultipleRows('default', 'pts_mst_balance_calibration_standard', $param);
            $res[$key]['standard_data'] = $standardData;
            $res[$key]['room_code'] = $this->db->get_where("pts_mst_sys_id",array("room_id"=>$value['room_id']))->row_array()['room_code'];
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "balance_info" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Balance info Found', "balance_info" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //End of Bhupendra's code

    /**
     * Get Id No of Standard Weight List
     * Created By :Rahul Chauhan
     * Date:18-02-2020
     */
    public function GetIdNoStandardWeightList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "active");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_id_standard_weight', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "id_standard_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Id No of standard weight Found', "id_standard_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Vertical Sample Rod List
     * Created By :Rahul Chauhan
     * Date:03-02-2020
     */
    public function GetsampleRodList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "1");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_vertical_sampler', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "sampler_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Vertical Sampler Found', "sampler_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Instrument Log List
     * Created By :Rahul Chauhan
     * Date:03-02-2020
     */
    public function GetInstrumentLogList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $param = array("is_active" => "1", "room_code" => $room_code, "is_inused" => 0, "equipment_type" => "Instrument");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "instrument_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Dispensing Booth Found', "instrument_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Balance calibration Frequency wise list
     * Created By :Rahul Chauhan
     * Date:04-02-2020
     */
    public function GetStandardData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $balance_no = isset($_GET['balance_no']) ? $_GET['balance_no'] : '';
        $frequency_id = isset($_GET['frequency_id']) ? $_GET['frequency_id'] : '';
        $param = array("status" => "active", "balance_no" => $balance_no);
        if ($frequency_id > 0) {
            $param["frequency_id"] = $frequency_id;
        }
        // $this->db->select("id,doc_no,room_code,balance_id,balance_no,frequency_id,standerd_wt,id_no_of_st_wt,id_no_st_wt_index,ROUND(oberved_wt, 4),ROUND(diff, 4),sprit_level_status,time_status,done_by_user_id,done_by_user_name,done_by_role_id,done_by_email,done_by_remark,created_on,status,is_in_workflow,updated_on,updated_by,update_remark");
        // $this->db->from("pts_mst_balance_frequency_data");
        // $this->db->where($param);
        // $res = $this->db->get()->result_array();
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_balance_frequency_data', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "standard_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Standard Data Found', "standard_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Equipment Apparatus Log List
     * Created By :Rahul Chauhan
     * Date:04-02-2020
     */
    public function GetEquipmentApparatuslist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $param = array("is_active" => "1", "room_code" => $room_code, "is_inused" => 0, "equipment_type" => "Apparatus");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "apparatus_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Apparatus Found', "apparatus_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Stage List
     * Created By :Rahul Chauhan
     * Date:04-02-2020
     */
    public function GetStageList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $log = isset($_GET['log']) ? $_GET['log'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $field_name = isset($_GET['field_name']) ? $_GET['field_name'] : '';
        if ($log != '' && $field_name != '') {
            $param = array("log" => $log, "field_name" => $field_name);
        } else {
            $param = array();
        }
        $res = $this->Ptscommon->getMultipleRows2('default', 'pts_mst_stages', $param, $order = null, $page);
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = $this->Ptscommon->getRowsCount('default', 'pts_mst_stages', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "stage_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Stage Found', "stage_list" => [], 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Equipment List
     * Created By :Rahul Chauhan
     * Date:04-02-2020
     */
    public function GetEquipmentlist() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        if ($room_code != '') {
            $param = array("is_active" => "1", "room_code" => $room_code, "is_inused" => 0);
        } else {
            $param = array("is_active" => "1", "is_inused" => 0);
        }

        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "equipment_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Equipment Found', "equipment_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Last Activity Product and Batch Number
     * Created By :Rahul Chauhan
     * Date:05-02-2020
     */
    public function GetLastProductAndBatchNumber() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $roomlogactivity = isset($_GET['roomlogactivity']) ? $_GET['roomlogactivity'] : '';
        $param = explode(",", $roomlogactivity);
        $res = $this->Ptscommon->GetLastProductAndBatchNumber($param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "last_product_code" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found', "last_product_code" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Last Activity Product and Batch Number 
     * Created By :Rahul Chauhan
     * Date:05-02-2020 
     */
    public function GetLastBactProductActivity() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $equipment_code = isset($_GET['equipment_code']) ? $_GET['equipment_code'] : '';
        $res = $this->Ptscommon->GetLastBactProductActivity($equipment_code);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "last_activity_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found', "last_activity_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Add Tablet Tooling Log Master
     * Created By :Rahul Chauhan
     * Date:05-02-2020
     */
    public function AddTabletToolingLog() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $recordid = isset($_POST['recordid']) ? $_POST['recordid'] : '';
        $product_code = isset($_POST['product_code']) ? $_POST['product_code'] : '';
        $tablet_data = isset($_POST['tablet_data']) ? $_POST['tablet_data'] : '';
        $this->Apifunction->check_variables($product_code, "Product Code", $numricCheck = 'no');
        $tablet_array = json_decode($tablet_data, true);
        foreach ($tablet_array as $key => $value) {
            $tablet_array[$key]['product_code'] = $product_code;
        }

        if ($recordid > 0) {
//            $param = array("id" => $recordid); // bug fix - v2.11 (338)
            $this->db->delete("pts_mst_tablet_tooling_log", array("product_code" => $product_code));
            $res = $this->db->insert_batch('pts_mst_tablet_tooling_log', $tablet_array);
//            $this->Ptscommon->updateToTable($db = 'default', 'pts_mst_tablet_tooling_log', $tablet_array[0], $param);
            $res = $recordid;
            if (!empty($res)) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "product_code:" . $product_code;
                $uniquefiled2 = "";
                $uniquefiled3 = "";
                $auditParams = array('command_type' => 'update', 'activity_name' => 'Tablet Tooling Master', 'type' => 'tablet_tooling_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_tablet_tooling_log', 'primary_id' => $res, 'post_params' => $tablet_array);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Tablet Tooling Log Updated');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Tablet Tooling Log does not Updated');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $res = $this->db->insert_batch('pts_mst_tablet_tooling_log', $tablet_array);
            if (!empty($res)) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "product_code:" . $product_code;
                $uniquefiled2 = "";
                $uniquefiled3 = "";
                $inserted_id = $this->db->insert_id();
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Tablet Tooling Master', 'type' => 'tablet_tooling_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_tablet_tooling_log', 'primary_id' => $inserted_id, 'post_params' => $tablet_array);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Tablet Tooling Log Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Tablet Tooling Log does not Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
    }

    /**
     * Get Tablet Tooling Log Card List
     * Created By :Rahul Chauhan
     * Date:05-02-2020
     */
    public function GetTabletToolingLogList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $product_code = isset($_GET['product_code']) ? $_GET['product_code'] : '';
        $punch_set = isset($_GET['punch_set']) ? $_GET['punch_set'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $param = array("status" => "active");
        if ($product_code != '') {
            $param = array("status" => "active", "product_code" => $product_code);
        }
        if ($punch_set != '') {
            $param['no_of_subset'] = $punch_set;
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_tablet_tooling_log', $param, $order = null, $page);
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = $this->Ptscommon->getRowsCount('default', 'pts_mst_tablet_tooling_log', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "tablet_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Tablet Tooling Log Found', "tablet_list" => array(), 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

//    public function GetTabletToolingpuchsetdata() {
//        $this->load->model('Ptscommon');
//        $headers = apache_request_headers(); // fetch header value
//        $product_code = isset($_GET['product_code']) ? $_GET['product_code'] : '';
//        $this->db->select("pts_trn_punch_set_allocation.id,pts_trn_punch_set_allocation.uld,pts_trn_punch_set_allocation.doc_no,pts_trn_punch_set_allocation.room_code,pts_trn_punch_set_allocation.product_code,pts_trn_punch_set_allocation.dom,pts_trn_punch_set_allocation.act_id,pts_trn_punch_set_allocation.act_name,pts_trn_punch_set_allocation.punch_set,pts_trn_punch_set_allocation.no_of_subset,pts_trn_punch_set_allocation.supplier,pts_mst_tablet_tooling.upper_punch,pts_mst_tablet_tooling.upper_embossing,pts_mst_tablet_tooling.lower_punch,pts_mst_tablet_tooling.lower_embossing,pts_mst_tablet_tooling.year,pts_trn_punch_set_allocation.dimension,pts_mst_tablet_tooling.shape,pts_mst_tablet_tooling.machine,pts_mst_tablet_tooling.die_quantity");
//        //$this->db->select("*");
//        $this->db->from("pts_trn_punch_set_allocation");
//        $this->db->join("pts_mst_tablet_tooling", "pts_trn_punch_set_allocation.punch_set=pts_mst_tablet_tooling.no_of_subset");
//        $this->db->where("pts_trn_punch_set_allocation.status", 'active');
//        $this->db->where("pts_trn_punch_set_allocation.product_code", $product_code);
//        //$this->db->group_by('punch_set');
//        $res = $this->db->get()->result_array();
//        if (!empty($res)) {
//            $responseresult = array(STATUS => TRUE, "result" => TRUE, "punch_set_data" => $res);
//            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
//        } else {
//            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Tablet Tooling Log Found', "punch_set_data" => array());
//            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
//        }
//    }
    
    /**
     * New code with resolved bugs
     */
    public function GetTabletToolingpuchsetdata() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = $_GET['room_code'] ?? '';
        $product_code = isset($_GET['product_code']) ? $_GET['product_code'] : '';
        $this->db->select("pts_trn_punch_set_allocation.id,pts_trn_punch_set_allocation.uld,pts_trn_punch_set_allocation.doc_no,pts_trn_punch_set_allocation.room_code,pts_trn_punch_set_allocation.product_code,pts_trn_punch_set_allocation.dom,pts_trn_punch_set_allocation.act_id,pts_trn_punch_set_allocation.act_name,pts_trn_punch_set_allocation.punch_set,pts_trn_punch_set_allocation.no_of_subset,pts_trn_punch_set_allocation.supplier,pts_mst_tablet_tooling.upper_punch,pts_mst_tablet_tooling.upper_embossing,pts_mst_tablet_tooling.lower_punch,pts_mst_tablet_tooling.lower_embossing,pts_mst_tablet_tooling.year,pts_trn_punch_set_allocation.dimension,pts_mst_tablet_tooling.shape,pts_mst_tablet_tooling.machine,pts_mst_tablet_tooling.die_quantity");
        //$this->db->select("*");
        $this->db->from("pts_trn_punch_set_allocation");
        $this->db->join("pts_mst_tablet_tooling", "pts_trn_punch_set_allocation.punch_set=pts_mst_tablet_tooling.no_of_subset");
        $this->db->where("pts_trn_punch_set_allocation.status", 'active');
        $this->db->where("pts_trn_punch_set_allocation.product_code", $product_code);
        if(!empty($room_code)){
            $this->db->where("pts_trn_punch_set_allocation.room_code", $room_code);
        }
        //$this->db->group_by('punch_set');
        $res = $this->db->get()->result_array();
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "punch_set_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Tablet Tooling Log Found', "punch_set_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }


    public function GetTabletToolingpuchsetdata2() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $product_code = isset($_GET['product_code']) ? $_GET['product_code'] : '';
        $this->db->select("*");
        //$this->db->select("*");
        $this->db->from("pts_trn_punch_set_allocation");
        $this->db->where("pts_trn_punch_set_allocation.status", 'active');
        $this->db->where("pts_trn_punch_set_allocation.product_code", $product_code);
        //$this->db->group_by('punch_set');
        $res = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "punch_set_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Tablet Tooling Log Found', "punch_set_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function GetTabletToolingpuchsubset() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $punch_set = isset($_GET['punch_set']) ? $_GET['punch_set'] : '';
        $product_code = isset($_GET['product_code']) ? $_GET['product_code'] : '';
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $this->db->select("*");
        $this->db->from("pts_trn_product_base_punch_set");
        if ($product_code != '') {
            $this->db->where("pts_trn_product_base_punch_set.product_code", $product_code);
        }
        $this->db->where("pts_trn_product_base_punch_set.punch_set", $punch_set);
        $res = $this->db->get()->result_array();
        $lastDocNo = $this->Ptscommon->getLastTTDocNo($room_code, $product_code, $punch_set);
        $res1 = $this->Ptscommon->GetLastFilterActivityForTT($lastDocNo, "pts_trn_tablet_tooling");
        //$res1 = $this->Ptscommon->getLastTTDocNo($room_code, $product_code, $punch_set);
        if(!empty($res1)){
            foreach ($res1 as $key => $value) {
    //            print_r($value);exit;
                $res1[$key]['activity_level'] = $this->GetActivityApprovalLevel($value['activity_id']);
                $res1[$key]['mst_act_id'] = $this->GetActivityUrl($value['roomlogactivity'])['mst_act_id'];
                $res1[$key]['last_approval'] = $this->GetLastApproval(array("id" => $value['act_id'], "room_code" => $value['room_code'], "roomlogactivity" => $value['roomlogactivity'], "activity_id" => $value['activity_id']));
                $res1[$key]['attachment_list'] = $this->GetAttachmentList(array("id" => $value['act_id'], "room_code" => $value['room_code'], "roomlogactivity" => $value['roomlogactivity'], "activity_id" => $this->GetActivityUrl($value['roomlogactivity'])['mst_act_id']));
            }
        }
        foreach ($res1 as $key => $value) {
//            print_r($value);exit;
            $res1[$key]['activity_level'] = $this->GetActivityApprovalLevel($value['activity_id']);
            $res1[$key]['mst_act_id'] = $this->GetActivityUrl($value['roomlogactivity'])['mst_act_id'];
            $res1[$key]['last_approval'] = $this->GetLastApproval(array("id" => $value['act_id'], "room_code" => $value['room_code'], "roomlogactivity" => $value['roomlogactivity'], "activity_id" => $value['activity_id']));
            $res1[$key]['attachment_list'] = $this->GetAttachmentList(array("id" => $value['act_id'], "room_code" => $value['room_code'], "roomlogactivity" => $value['roomlogactivity'], "activity_id" => $this->GetActivityUrl($value['roomlogactivity'])['mst_act_id']));
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "punch_subset_data" => [$res,$res1]);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Tablet Tooling Log Found', "punch_subset_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function GetTabletToolingpuchsubset_for_mst() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $punch_set = isset($_GET['punch_set']) ? $_GET['punch_set'] : '';
        $product_code = isset($_GET['product_code']) ? $_GET['product_code'] : '';
        $this->db->select("*");
        $this->db->from("pts_trn_product_base_punch_set");
        $this->db->join("pts_trn_punch_set_allocation", "pts_trn_product_base_punch_set.punch_set=pts_trn_punch_set_allocation.punch_set", 'left');
        if ($product_code != '') {
            $this->db->where("pts_trn_product_base_punch_set.product_code", $product_code);
        }
        $this->db->where("pts_trn_product_base_punch_set.punch_set", $punch_set);
        $res = $this->db->get()->result_array();
        //echo $this->db->last_query();exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "punch_subset_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Tablet Tooling Log Found', "punch_subset_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function subsetissue() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $subsets = json_decode($_POST['subsets'], true);
        foreach ($subsets as $key => $value) {
            $post = array("status" => 'issued', "last_issued" => 'yes');
            $param = array("punch_set_code" => $subsets[$key]['subsetcode']);
            $res = $this->Ptscommon->updateToTable($db = 'default', 'pts_trn_product_base_punch_set', $post, $param);
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Successfully Issued');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Master Activity Log List
     * Created By :Rahul Chauhan
     * Date:10-02-2020
     */
    public function GetMstActivityList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value

        $type = isset($_GET['type']) ? $_GET['type'] : '';
        if ($type != '') {
            $param = array("status" => "active");
        } else {
            $param = array("status" => "active", "type" => "log");
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_activity', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "mst_act_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Master Log Activity Found', "mst_act_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Id Standard Weight List
     * Created By :Rahul Chauhan
     * Date:11-02-2020
     */
    public function GetLastFilterActivity() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $table_name = isset($_GET['table_name']) ? $_GET['table_name'] : '';
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $doc_no = isset($_GET['doc_no']) ? $_GET['doc_no'] : '';
        $res = $this->Ptscommon->GetLastFilterActivity($room_code, $table_name,$doc_no);

        foreach ($res as $key => $value) {
//            print_r($value);exit;
            $res[$key]['activity_level'] = $this->GetActivityApprovalLevel($value['activity_id']);
            $res[$key]['mst_act_id'] = $this->GetActivityUrl($value['roomlogactivity'])['mst_act_id'];
            $res[$key]['last_approval'] = $this->GetLastApproval(array("id" => $value['act_id'], "room_code" => $value['room_code'], "roomlogactivity" => $value['roomlogactivity'], "activity_id" => $value['activity_id']));
            $res[$key]['attachment_list'] = $this->GetAttachmentList(array("id" => $value['act_id'], "room_code" => $value['room_code'], "roomlogactivity" => $value['roomlogactivity'], "activity_id" => $this->GetActivityUrl($value['roomlogactivity'])['mst_act_id']));
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Approval Found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function GetLastFilterActivityForTT() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $docno = isset($_GET['docno']) ? $_GET['docno'] : '';
        $table_name = isset($_GET['table_name']) ? $_GET['table_name'] : '';
        $res = $this->Ptscommon->GetLastFilterActivityForTT($docno, $table_name);

        foreach ($res as $key => $value) {
//            print_r($value);exit;
            $res[$key]['activity_level'] = $this->GetActivityApprovalLevel($value['activity_id']);
            $res[$key]['mst_act_id'] = $this->GetActivityUrl($value['roomlogactivity'])['mst_act_id'];
            $res[$key]['last_approval'] = $this->GetLastApproval(array("id" => $value['act_id'], "room_code" => $value['room_code'], "roomlogactivity" => $value['roomlogactivity'], "activity_id" => $value['activity_id']));
            $res[$key]['attachment_list'] = $this->GetAttachmentList(array("id" => $value['act_id'], "room_code" => $value['room_code'], "roomlogactivity" => $value['roomlogactivity'], "activity_id" => $this->GetActivityUrl($value['roomlogactivity'])['mst_act_id']));
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Approval Found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function getLastTTDocNo() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = $_GET['room_code'] ?? '';
        $product_code = $_GET['product_code'] ?? '';
        $punch_set = $_GET['punch_set_number'] ?? '';
        $res = $this->Ptscommon->getLastTTDocNo($room_code, $product_code, $punch_set);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Activity In Progress Found',"doc_no" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity In Progress Found', "doc_no" => '');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Header Data of Env. Con
     * Created By : Bhupendra kumar
     * Date: 20-04-2020
     */
    public function GetHeaderData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $table_name = isset($_GET['table_name']) ? $_GET['table_name'] : '';
        $res = $this->Ptscommon->GetHeaderData($room_code, $table_name);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Approval Found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to Check Material Status, Is it in or out
     * Created By : Bhupendra
     * Created Date : 18Feb2020
     */

    public function chkmatIn_Out() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $matcode = isset($_POST['matcode']) ? $_POST['matcode'] : '';
        $res = $this->Ptscommon->GetLastMatRecord($matcode);

        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {

            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function GetProductDetailbycode() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $matcode = isset($_POST['matcode']) ? $_POST['matcode'] : '';
        $res = $this->Ptscommon->GetProductDetailbycode($matcode);

        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {

            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Id No of Standard Weight List
     * Created By :Rahul Chauhan
     * Date:18-02-2020
     */
    public function GetStandardWeightList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $module = isset($_GET['module']) ? $_GET['module'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $param = array("status" => "active");
        if ($module == 'master') {
            $param = array();
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_id_standard_weight', $param, $order = null, $page);
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = $this->Ptscommon->getRowsCount('default', 'pts_mst_id_standard_weight', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "standard_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Standard Weight Found', "standard_list" => array(), 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Add Id No Standard Weight Master
     * Created By :Rahul Chauhan
     * Date:18-02-2020
     */
    public function SaveIdStandard() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $recordid = isset($_POST['recordid']) ? $_POST['recordid'] : '';
        $weight = isset($_POST['weight']) ? $_POST['weight'] : '';
        $measurement_unit = isset($_POST['measurement_unit']) ? $_POST['measurement_unit'] : '';
        $standard_data = isset($_POST['standard_data']) ? $_POST['standard_data'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $standard_array = json_decode($standard_data, true);
        $status = 'active';
        if ($recordid > 0) {
            if ($_POST['status'] == 'true') {
                $status = 'active';
            } else {
                $status = 'inactive';
            }
        }
        $auditArray = [];
        foreach ($standard_array as $key => $value) {
            $standard_array[$key]['weight'] = $weight;
            $standard_array[$key]['measurement_unit'] = $measurement_unit;
            $standard_array[$key]['remark'] = $remark;
            $standard_array[$key]['created_by'] = $this->session->userdata('empname');
            $arr = ['id_no_standard_weight' => $value['id_no_statndard_weight'], "weight" => $weight, "measurement_unit" => $measurement_unit, "status" => ucfirst($status), "remarks" => $remark];
            $auditArray[] = $this->Ptscommon->getNAUpdated($arr);
        }
        if ($recordid > 0) {
            $oldData = $this->db->get_where("pts_mst_id_standard_weight", ["id" => $recordid])->row_array();
            $master_previous_data = [];
            if (!empty($oldData)) {
                $master_previous_data = ["weight" => $oldData['weight'], "measurement_unit" => $oldData['measurement_unit'], "status" => ucfirst($oldData['status'])];
                $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
            }
            foreach ($standard_array as $key => $value) {
                unset($standard_array[$key]['created_by']); // = $this->session->userdata('empname');
                $standard_array[$key]['modified_on'] = date('Y-m-d H:i:s');
                $standard_array[$key]['modified_by'] = $this->session->userdata('empname');
                $standard_array[$key]['status'] = $status;
            }
            $res = $this->db->update("pts_mst_id_standard_weight", $standard_array[0], ["id" => $recordid]);
            if (!empty($res)) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "weight:" . $weight;
                $uniquefiled2 = "measurement_unit:" . $measurement_unit;
                $uniquefiled3 = "";
                //$inserted_id = $this->db->insert_id();
                //unset($standard_array[$key]['created_by']);
                $auditParams = array('command_type' => 'update', 'activity_name' => 'Id Standard Weight Details', 'type' => 'manage_id_standard_weight', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_id_standard_weight', 'primary_id' => $recordid, 'post_params' => $auditArray[0], 'master_previous_data' => $master_previous_data);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Id No Standard Weight Updated');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Id No Standard Weight does not Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            //echo '<pre>';print_r($auditArray);exit;
            //$this->db->delete("pts_mst_id_standard_weight", array("weight" => $weight, "measurement_unit" => $measurement_unit));
            foreach ($standard_array as $key => $arr) {
                $arr['status'] = 'active';
                $res = $this->db->insert('pts_mst_id_standard_weight', $arr);
                if (!empty($res)) {
                    /* Start - Insert elog audit histry */
                    $uniquefiled1 = "weight:" . $weight;
                    $uniquefiled2 = "measurement_unit:" . $measurement_unit;
                    $uniquefiled3 = "";
                    $inserted_id = $this->db->insert_id();

                    $auditParams = array('command_type' => 'insert', 'activity_name' => 'Id Standard Weight Details', 'type' => 'manage_id_standard_weight', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_id_standard_weight', 'primary_id' => $inserted_id, 'post_params' => $auditArray[$key]);
                    $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                    /* End -  Insert elog audit histry */
                }
            }
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Id No Standard Weight Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Id No Standard Weight does not Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
    }

    /**
     * Get Role List
     * Created By :Rahul Chauhan
     * Date:19-02-2020
     */
    public function GetRoleList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $status = isset($_GET['status']) ? $_GET['status'] : '';
        $param = array("is_active <" => 2);
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_role', $param, '', $page);
        if ($status != '') {
            foreach ($res as $key => $value) {
                if ($value['is_active'] == 0) {
                    unset($res[$key]);
                }
            }
            $res = array_values($res);
        }
//        echo '<pre>';
//        print_r($res);
//        exit;
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = $this->Ptscommon->getRowsCount('default', 'mst_role', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "role_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Role Found', "role_list" => [], 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Role Levels
     * Created By : Bhupendra kumar
     * Date:26-07-2020
     */
    public function GetRoleLevelList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array();
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_role_level', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "role_level_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Role Found', "role_level_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Assigned Role List
     * Created By :Rahul Chauhan
     * Date:19-02-2020
     */
    public function GetAssignRoleList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array();
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $res = $this->Ptscommon->getMultipleRowswithgroupby('default', 'pts_mst_role_workassign_to_mult_roles', $param, "roleid", $page);
        foreach ($res as $key => $value) {
            $role_name = $this->db->get_where("mst_role", array("id" => $value['roleid']))->row_array();
            $res[$key]['role_name'] = $role_name['role_description'];
        }
        $count = $this->Ptscommon->getMultipleRowswithgroupbyCount('default', 'pts_mst_role_workassign_to_mult_roles', $param, "roleid");
        $records_per_page = $this->Ptscommon::records_per_page;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "role_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Role Found', "role_list" => array(), 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Work Assign Role Master
     * Created By :Rahul Chauhan
     * Date:18-02-2020
     */
    public function SaveWorkAssign() {

        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $role_id = isset($_POST['role_id']) ? $_POST['role_id'] : '';
        $work_id = isset($_POST['work_id']) ? $_POST['work_id'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $isactive = isset($_POST['isactive']) ? $_POST['isactive'] : '';
        $isactive = ($isactive == 'inactive') ? 0 : 1;
        $work_array = json_decode($work_id, true);
        $main_role = $this->db->get_where('mst_role', ["id" => $role_id])->row_array();
        $role_name = !empty($main_role) ? $main_role['role_description'] : '';
        $delegated_role_names = '';
        if (!empty($work_array)) {
            $this->db->where_in('id', $work_array);
            $this->db->from('mst_role');
            $main_role = $this->db->get()->result_array();
            if (!empty($main_role)) {
                foreach ($main_role as $v) {
                    $delegated_role_names .= $v['role_description'] . ',';
                }
                $delegated_role_names = rtrim($delegated_role_names, ',');
            }
        }
        $auditArray = ['role_name' => $role_name, 'delegated_role_names' => $delegated_role_names, 'status' => ucfirst($_POST['isactive']), 'remarks' => $remark];
        $auditArray = $this->Ptscommon->getNAUpdated($auditArray);
        $tempArry = array();
        if ($this->session->userdata('empname')) {
            $created_by = $this->session->userdata('empname');
        } else {
            $created_by = "SuperAdmin";
        }
        $recordid = isset($_POST['recordid']) ? $_POST['recordid'] : '';
        if ($recordid > 0) {
            $master_previous_data = [];
            $this->db->select("pts_mst_role_workassign_to_mult_roles.isactive,pts_mst_role_workassign_to_mult_roles.remark,mst_role.role_description");
            $this->db->from("pts_mst_role_workassign_to_mult_roles");
            $this->db->join("mst_role", "pts_mst_role_workassign_to_mult_roles.workassign_to_roleid = mst_role.id", "left");
            $this->db->where("pts_mst_role_workassign_to_mult_roles.roleid", $role_id);
            $old_data = $this->db->get()->result_array();
            $old_delegeted_roles = '';
            if (!empty($old_data)) {
                foreach ($old_data as $k => $val) {
                    $old_delegeted_roles .= $val['role_description'] . ',';
                }
                $old_delegeted_roles = rtrim($old_delegeted_roles, ',');
                $old_status = $old_data[0]['isactive'] == 1 ? 'Active' : 'Inactive';
                $master_previous_data = ['delegated_role_names' => $old_delegeted_roles, 'status' => $old_status];
            }
            foreach ($work_array as $key => $value) {
                $tempArry[] = array("roleid" => $role_id, "workassign_to_roleid" => $value, "modified_by" => $created_by, "created_by" => $created_by, "modified_on" => date('Y-m-d H:i:s'), "isactive" => $isactive, "remark" => $remark);
            }
            $this->db->delete("pts_mst_role_workassign_to_mult_roles", array("roleid" => $role_id));
            $res = $this->db->insert_batch('pts_mst_role_workassign_to_mult_roles', $tempArry);
            if (!empty($res)) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "role_id:" . $role_id;
                $uniquefiled2 = "";
                $uniquefiled3 = "";
                $inserted_id = $this->db->insert_id();
                $auditParams = array('command_type' => 'update', 'activity_name' => 'Work Assign Master ', 'type' => 'work_assign_asper_rolebase', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_role_workassign_to_mult_roles', 'primary_id' => $inserted_id, 'post_params' => $auditArray, 'master_previous_data' => $master_previous_data);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Work Assign Roles updated Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Work Assign Role does not Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $param = array("roleid" => $role_id, "isactive" => 1);
            $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_role_workassign_to_mult_roles', $param);
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Some roles already assigned for selected role, please delete the previous once to assign again, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                foreach ($work_array as $key => $value) {
                    $tempArry[] = array("roleid" => $role_id, "workassign_to_roleid" => $value, "created_by" => $created_by, "modified_by" => $created_by, "modified_on" => date('Y-m-d H:i:s'), "isactive" => $isactive, "remark" => $remark);
                }
                $this->db->delete("pts_mst_role_workassign_to_mult_roles", array("roleid" => $role_id));
                $res = $this->db->insert_batch('pts_mst_role_workassign_to_mult_roles', $tempArry);
                if (!empty($res)) {

                    /* Start - Insert elog audit histry */
                    $uniquefiled1 = "role_id:" . $role_id;
                    $uniquefiled2 = "";
                    $uniquefiled3 = "";
                    $inserted_id = $this->db->insert_id();
                    $auditParams = array('command_type' => 'insert', 'activity_name' => 'Work Assign Master', 'type' => 'work_assign_asper_rolebase', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_role_workassign_to_mult_roles', 'primary_id' => $inserted_id, 'post_params' => $auditArray);
                    $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                    /* End -  Insert elog audit histry */
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Work Assign Roles Added Successfully');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Work Assign Role does not Added');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }
        }
    }

    /* This Function is used to get the process log records for report
     * Created By : Bhupendra kumar
     * Created Date : 18Feb2020
     */

    public function GetProcessLogReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $equip_code = isset($_POST['equip_code']) ? $_POST['equip_code'] : '';
        $type = isset($_POST['type']) ? $_POST['type'] : '';
        $mst_act_id = isset($_POST['mst_act_id']) ? $_POST['mst_act_id'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        $res = $this->Ptscommon->GetProcessLogReport($room_code, $equip_code, $type, $post, $mst_act_id);
        foreach ($res as $key => $value) {
            $attachments = $this->Ptscommon->GetAttachmentListforReport($value['id']);
            $res[$key]['logAttachments'] = $attachments;
            //echo "<pre/>";print_r($attachments);
        }

        //echo "<pre/>";print_r($res);exit;




        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No record found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Pre Filter Report Data
     * Created By : Bhupendra Kumar
     * Edited By: ****
     * Created Date:05-03-2020
     * Edit Date:***
     */
    public function GetPreFilterReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['room_code'] = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $post['end_date'] = isset($_POST['end_date']) ? $_POST['end_date'] : '';
        $res = $this->Ptscommon->GetPreFilterLogReportLAF($post);
        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Vertical Sampler Report Data
     * Created By : Bhupendra Kumar
     * Edited By: ****
     * Created Date:11-03-2020
     * Edit Date:***
     */
    public function GetVerticalSamplerReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['equipment_code'] = isset($_POST['equipment_code']) ? $_POST['equipment_code'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $post['end_date'] = isset($_POST['end_date']) ? $_POST['end_date'] : '';
        $res = $this->Ptscommon->GetVerticalSamplerReport($post);
        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Audit trail Report Data
     * Created By : Devendra Singh
     * Edited By: ****
     * Created Date:29-02-2020
     * Edit Date:***
     */
    public function airFilterCleaningReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['room_code'] = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $post['frequency'] = isset($_POST['frequency']) ? $_POST['frequency'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        // print_r($post);
        // die();
        $res = $this->Ptscommon->airFilterCleaningReportList($post);

        foreach ($res as $key => $val) {

            $filter = explode(",", $val['filter']);
            $clean_filter = explode(",", $val['clean_filter']);
            $replace_filter = explode(",", $val['replace_filter']);
            $filterArray = array();
            for ($i = 1; $i <= 13; $i++) {
                foreach ($filter as $val1) {

                    if ("F" . $i == $val1) {
                        if (in_array('F' . $i, $clean_filter)) {
                            $filterArray['F' . $i] = "C";
                        }
                        if (in_array('F' . $i, $replace_filter)) {
                            $filterArray['F' . $i] = "R";
                        }
                    } else {
                        if (array_key_exists('F' . $i, $filterArray)) {
                            
                        } else {
                            $filterArray['F' . $i] = "NA";
                        }
                    }
                }
            }
//            print_r($filterArray);exit;
            $res[$key]['filter'] = $filterArray;
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Audit trail Report Data
     * Created By : Bhupendra Kumar
     * Created Date:03-03-2020
     */
    public function auditTrailReportList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['user_name'] = isset($_POST['user_name']) ? $_POST['user_name'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        $post['limit'] = isset($_GET['page_limit']) ? $_GET['page_limit'] : 0;
        $post['page'] = isset($_GET['page_no']) ? $_GET['page_no'] : 0;

        $res = $this->Ptscommon->auditTrailReportList($post);
        $totalcount = $res["total_count"];
        //echo $totalcount;
        //echo "<pre/>";print_r($res["result1"]);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res["result1"], "total_count" => $totalcount);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array(), "total_count" => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Audit trail Report Data Print
     * Created By : Amitesh Yadav
     * Created Date: 04-08-2020
     */
    public function auditTrailReportListPrint() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['user_name'] = isset($_POST['user_name']) ? $_POST['user_name'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        $post['limit'] = isset($_GET['page_limit']) ? $_GET['page_limit'] : 0;
        $post['page'] = isset($_GET['page_no']) ? $_GET['page_no'] : 0;

        $res = $this->Ptscommon->auditTrailReportListPrint($post);
        // $totalcount = $res["total_count"];
        //echo $totalcount;
        //echo "<pre/>";print_r($res["result1"]);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data2" => $res["result"]);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array(), "total_count" => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Equipment Apparatus Log Report
     * Created By : Devendra Singh
     * Edited By: ****
     * Created Date:29-02-2020
     * Edit Date:***
     */
    public function GetEquipmentApparatus() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['apparatus_no'] = isset($_POST['apparatus_no']) ? $_POST['apparatus_no'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        // print_r($post);
        // die();
        $res = $this->Ptscommon->GetEquipmentApparatusLogReport($post);
        // echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Equipment Apparatus LeakTest Report
     * Created By : Bhupendra kumar
     * Edited By: ****
     * Created Date:05-04-2020
     * Edit Date:***
     */
    public function GetEquipmentApparatusLeakTest() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['apparatus_no'] = isset($_POST['apparatus_no']) ? $_POST['apparatus_no'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        // print_r($post);
        // die();
        $res = $this->Ptscommon->GetEquipmentApparatusLeakTestReport($post);
        // echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to get the swab sample records for report
     * Created By : Bhupendra kumar
     * Created Date : 18Feb2020
     */

    public function GetSwabSampleReport() {
        $this->load->model('Ptscommon');
        $post['start_date'] = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        $res = $this->Ptscommon->GetSwabSampleReport($post);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function userStatusReport() {
        $this->load->model('Adminmodel');
        $post['start_date'] = isset($_POST['start_date']) && !empty($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) && !empty($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        $post['emp_name'] = isset($_POST['emp_name']) ? $_POST['emp_name'] : '';
        $post['block_code'] = isset($_POST['block_code']) ? $_POST['block_code'] : 'Block-OSD';
        $post['limit'] = isset($_GET['page_limit']) ? $_GET['page_limit'] : 0;
        $post['page'] = isset($_GET['page_no']) ? $_GET['page_no'] : 0;
        $res = $this->Adminmodel->getuserstatusdataNew($post);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res['records'], "total_count" => $res['count']);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No record found', "row_data" => array(), "total_count" => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function fetchEmpDetails() {
        $this->load->model('Adminmodel');
        $res = $this->Adminmodel->mstEmpview();
        $this->Apifunction->response($this->Apifunction->json($res), 200);
    }

    public function fetchBlockDetails() {
        $this->load->model('Adminmodel');
        $res = $this->Adminmodel->mstBlockview();
        $this->Apifunction->response($this->Apifunction->json($res), 200);
    }

    /**
     * Save Filter Master
     * Created By :Rahul Chauhan
     * Date:21-02-2020
     */
    public function saveFilterData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $filter_data = isset($_POST['filter_data']) ? $_POST['filter_data'] : '';
        $filter_array = json_decode($filter_data, true);
        $res = $this->db->insert_batch('pts_mst_filter', $filter_array);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Filter Data Added Successfully');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Filter Data does not Added');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Save Filter Master
     * Created By :Rahul Chauhan
     * Date:21-02-2020
     */
    public function saveLineLogData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $recordid = isset($_POST['recordid']) ? $_POST['recordid'] : '';
        $line_log_name = isset($_POST['line_log_name']) ? $_POST['line_log_name'] : '';
        $line_log_code = isset($_POST['line_log_code']) ? $_POST['line_log_code'] : '';
        $block_code = isset($_POST['block_code']) ? $_POST['block_code'] : '';
        $area_code = isset($_POST['area_code']) ? $_POST['area_code'] : '';
        $room_code = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $equipmentData = isset($_POST['equipmentData']) ? $_POST['equipmentData'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $status = isset($_POST['status']) ? $_POST['status'] : '';
        $equipmentArray = json_decode($equipmentData, true);
        $equipdata = implode(',', $equipmentArray);
        $auditArray = ['line_log_code' => $line_log_code, 'block_code' => $block_code, 'area_code' => $area_code, "room_code" => $room_code, "line_log_name" => $line_log_name, "equipment_code" => $equipdata, "status" => ucfirst($status), "remarks" => $remark];
        $auditArray = $this->Ptscommon->getNAUpdated($auditArray);
        $insertArray = [];
        //$exclude = array();
        //Rahul's code commented below
        // foreach ($equipmentArray as $key => $value) {
        //     $data = $this->db->get_where("pts_mst_line_log", array("block_code" => $block_code, "area_code" => $area_code, "room_code" => $room_code, "equipment_code" => $value, "status" => "active"))->row_array();
        //     if (empty($data)) {
        //         $insertArray[] = array("block_code" => $block_code, "area_code" => $area_code, "room_code" => $room_code, "line_log_code" => $line_log_code, "line_log_name" => $line_log_name, "equipment_code" => $value);
        //     } else {
        //         $exclude[] = $value;
        //     }
        // }
        // if (!empty($insertArray)) {
        //     $res = $this->db->insert_batch('pts_mst_line_log', $insertArray);
        // }
        //End of rahul's code commented
        //Bhupendra code Below

        if ($recordid > 0) {
            $line_data = $this->db->get_where("pts_mst_line_log", array("id" => $recordid))->row_array();
            $master_previous_data = ['block_code' => $line_data['block_code'], 'area_code' => $line_data['area_code'], "room_code" => $line_data['room_code'], "line_log_name" => $line_data['line_log_name'], "equipment_code" => $line_data['equipment_code'], "status" => ucfirst($line_data['status'])];
            $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
            $equipment_data = explode(",", $line_data['equipment_code']);
            foreach ($equipment_data as $key => $value) {
                $this->db->update("mst_equipment", array("is_in_line" => "0"), array("equipment_code" => $value, "equipment_type" => "Fixed"));
            }
            foreach ($equipmentArray as $key => $value) {  //khushboo code - bug fix -v2.11(337)
                $data = $this->Ptscommon->checkSameRoomWithSameEquip($block_code, $area_code, $room_code, $value, $recordid); // bug -fix v2.11 (337)
                if (!empty($data)) {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Record Already Exist with same Room and Equipment, if you need add Something do edit please...Thank you');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }
            //echo '<pre>';print_r($master_previous_data);print_r($auditArray);exit;

            $insertArray[] = array("block_code" => $block_code, "area_code" => $area_code, "room_code" => $room_code, "line_log_name" => $line_log_name, "line_log_code" => $line_log_code, "equipment_code" => $equipdata, "remark" => $remark, "status" => $status, "modified_by" => $this->session->userdata('empname'), "modified_on" => date('Y-m-d H:i:s'));
            //echo "<pre>";print_r($insertArray);exit;
            $param = array("id" => $recordid);
            $data = $this->db->delete('pts_mst_line_log', $param);
            if (!empty($data)) {
                $res = $this->db->insert('pts_mst_line_log', $insertArray[0]);
                if (!empty($res)) {
                    foreach ($equipmentArray as $key => $value) {
                        $this->db->update("mst_equipment", array("is_in_line" => "1"), array("equipment_code" => $value, "equipment_type" => "Fixed"));
                    }
                    /* Start - Insert elog audit histry */
                    $uniquefiled1 = "block_code:" . $block_code;
                    $uniquefiled2 = "area_code:" . $area_code;
                    $uniquefiled3 = "room_code:" . $room_code;
                    $inserted_id = $this->db->insert_id();
                    $auditParams = array('command_type' => 'update', 'activity_name' => 'Line Master', 'type' => 'line_log_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_line_log', 'primary_id' => $inserted_id, 'post_params' => $auditArray, 'master_previous_data' => $master_previous_data);
                    $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                    /* End -  Insert elog audit histry */

                    $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Line Log Data Updated Successfully');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Line Log Data does not Added');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }
        } else {
            $this->db->where("line_log_name", $line_log_name);
            $this->db->or_where("line_log_code", $line_log_code);
            $exist_data = $this->db->get_where("pts_mst_line_log", array("status" => "active"))->row_array();
            if (!empty($exist_data)) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Record Already Exist with same Line name or Line Code');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
            foreach ($equipmentArray as $key => $value) {  //khushboo code - bug fix -v2.11(337)
                $data = $this->Ptscommon->checkSameRoomWithSameEquip($block_code, $area_code, $room_code, $value); // bug -fix v2.11 (337)
                if (!empty($data)) {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Record Already Exist with same Room and Equipment, if you need add Something do edit please...Thank you');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }
//            if (empty($data)) {
            $insertArray[] = array("block_code" => $block_code, "area_code" => $area_code, "room_code" => $room_code, "line_log_code" => $line_log_code, "line_log_name" => $line_log_name, "equipment_code" => $equipdata, "remark" => $remark, "created_by" => $this->session->userdata('empname'));
            //echo "<pre>";print_r($insertArray);exit;
            $res = $this->db->insert('pts_mst_line_log', $insertArray[0]);
            if (!empty($res)) {
                foreach ($equipmentArray as $key => $value) {
                    $this->db->update("mst_equipment", array("is_in_line" => "1"), array("equipment_code" => $value, "equipment_type" => "Fixed"));
                }
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "block_code:" . $block_code;
                $uniquefiled2 = "area_code:" . $area_code;
                $uniquefiled3 = "room_code:" . $room_code;
                $inserted_id = $this->db->insert_id();
                $insertArray[0]['status'] = "active";
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Line Master', 'type' => 'line_log_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_line_log', 'primary_id' => $inserted_id, 'post_params' => $auditArray);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Line Log Data Added Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Line Log Data does not Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
//            } else {
//                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Record Already Exist, if you need add Something do edit please...Thank you');
//                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
//            }
        }
        //End Of Bhupendra code
    }

    /* This Function is used to get Line Log Master List
     * Created By : Rahul Chauhan
     * Created Date : 24-02-2020
     */

    public function GetLineLogList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array();
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        if ($room_code != '') {
            $param = array("status" => "active", "room_code" => $room_code, "is_inused" => 0);
        }
        if (isset($type) && $type != '') {
            $param = array("status" => "active", "room_code" => $room_code);
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_line_log', $param, $order = null, $page);
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = $this->Ptscommon->getRowsCount('default', 'pts_mst_line_log', $param);
        //echo "<pre/>";print_r($res);exit;
        $final = array();
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $value['ischecked'] = false;
                $final[$value['line_log_name']][] = $value;
            }
            $newArray = array();

            foreach ($final as $key => $value) {
                $tempArray = array();
                $equipArray = array();
                foreach ($value as $key1 => $value1) {
                    $equipArray[] = $value1['equipment_code'];
                    if ($key == $value1['line_log_name']) {
                        $tempArray = $value1;
                    }
                }
                $tempArray['equipment_code'] = implode(",", $equipArray);
                $newArray[] = $tempArray;
            }
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "line_log_data" => $newArray, 'records_per_page' => $records_per_page, 'total_records' => $count);
            //$responseresult = array(STATUS => TRUE, "result" => TRUE, "line_log_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "line_log_data" => array(), 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to get Block List
     * Created By : Rahul Chauhan
     * Created Date : 24-02-2020
     */

    public function GetBlockList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("is_active" => "1");
        $plant_code = isset($_GET['plant_code']) ? $_GET['plant_code'] : '';
        if ($plant_code != "") {
            $param = array("is_active" => "1", "plant_code" => $plant_code);
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_block', $param);
        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "block_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "block_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to Block Wise Area List
     * Created By : Rahul Chauhan
     * Created Date : 24-02-2020
     */

    public function GetAreaList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $block_code = isset($_GET['block_code']) ? $_GET['block_code'] : '';
        $param = array("is_active" => "1", "block_code" => $block_code);
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_area', $param);
        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "area_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "area_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to save Istrument master Data
     * Created By : Rahul Chauhan
     * Created Date : 24-02-2020
     */

    public function saveInstrument() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $block_code = isset($_POST['block_code']) ? $_POST['block_code'] : '';
        $equipment_name = isset($_POST['equipment_name']) ? $_POST['equipment_name'] : '';
        $equipment_code = isset($_POST['equipment_code']) ? $_POST['equipment_code'] : '';
        $equipment_desc = isset($_POST['equipment_desc']) ? $_POST['equipment_desc'] : '';
        $equipment_type = isset($_POST['equipment_type']) ? $_POST['equipment_type'] : '';
        $status = isset($_POST['status']) ? $_POST['status'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $recordid = isset($_POST['recordid']) ? $_POST['recordid'] : '';
        // $created_by = $this->session->userdata('empname');
        $modified_by = $this->session->userdata('empname');
        $auditArray = ["instrument_code" => $equipment_code, "block_code" => $block_code, "instrument_name" => $equipment_name, "instrument_desc" => $equipment_desc, "instrument_type" => $equipment_type, "status" => ucfirst($status), "remarks" => $remark];
        $auditArray = $this->Ptscommon->getNAUpdated($auditArray);
        $insertArray = array("block_code" => $block_code, "equipment_name" => $equipment_name, "equipment_code" => $equipment_code, "equipment_desc" => $equipment_desc, "equipment_type" => $equipment_type, "remark" => $remark, "created_by" => $modified_by);
        $data = $this->db->get_where("pts_mst_instrument", array("block_code" => $block_code, "equipment_name" => $equipment_name, "equipment_code" => $equipment_code, "status" => "1"))->row_array();
        if (!empty($data)) {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'This ' . $equipment_type . 'already exists', "area_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }

        if ($recordid > 0) {
            $insertArray['status'] = $status;
            $param = array("id" => $recordid);
            $old_data = $this->db->get_where('pts_mst_instrument', $param)->row_array();
            $master_previous_data = [];
            if (!empty($old_data)) {
                $master_previous_data = ["block_code" => $old_data['block_code'], "instrument_name" => $old_data['equipment_name'], "instrument_desc" => $old_data['equipment_desc'], "instrument_type" => $old_data['equipment_type'], "status" => ucfirst($old_data['status'])];
                $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
            }
            $insertArray['modified_on'] = date('Y-m-d H:i:s');
            $insertArray['modified_by'] = $modified_by;
            $this->Ptscommon->updateToTable($db = 'default', 'pts_mst_instrument', $insertArray, $param);
            $res = $recordid;

            /* Start - Insert elog audit histry */
            $uniquefiled1 = "block_code:" . $block_code;
            $uniquefiled2 = "equipment_code:" . $equipment_code;
            $uniquefiled3 = "equipment_type:" . $equipment_type;
            $insertArray = array("block_code" => $block_code, "instrument_name" => $equipment_name, "instrument_code" => $equipment_code, "instrument_desc" => $equipment_desc, "instrument_type" => $equipment_type, "remark" => $remark, "created_by" => $modified_by);
            $auditParams = array('command_type' => 'update', 'activity_name' => 'Instrument Master', 'type' => 'instrument_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_instrument', 'primary_id' => $recordid, 'post_params' => $auditArray, "master_previous_data" => $master_previous_data);
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Instrument data updated Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong.Please try again.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $res = $this->Ptscommon->addToTable($db = 'default', 'pts_mst_instrument', $insertArray, $param = '');

            /* Start - Insert elog audit histry */
            $uniquefiled1 = "block_code:" . $block_code;
            $uniquefiled2 = "equipment_code:" . $equipment_code;
            $uniquefiled3 = "equipment_type:" . $equipment_type;
            $insertArray['status'] = "active";
            $insertArray = array("block_code" => $block_code, "instrument_name" => $equipment_name, "instrument_code" => $equipment_code, "instrument_desc" => $equipment_desc, "instrument_type" => $equipment_type, "remark" => $remark, "created_by" => $modified_by);
            $auditParams = array('command_type' => 'insert', 'activity_name' => 'Instrument Master', 'type' => 'instrument_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_instrument', 'primary_id' => $res, 'post_params' => $auditArray);
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */

            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Instrument data saved Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong.Please try again.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
    }

    /* This Function is used to get Instrument List
     * Created By : Rahul Chauhan
     * Created Date : 24-02-2020
     */

    public function GetInstrumentList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $equipment_type = isset($_GET['equipment_type']) ? $_GET['equipment_type'] : "";
        $type = isset($_GET['type']) ? $_GET['type'] : "";
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $param = array();
        if ($equipment_type != "" && $type == "") {
            $param = array("status" => "active", "equipment_type" => $equipment_type, "is_inused" => 0);
        }
        if ($equipment_type != "" && $type == "report") {
            $param = array("status" => "active", "equipment_type" => $equipment_type);
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_instrument', $param, $order = null, $page);
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = $this->Ptscommon->getRowsCount('default', 'pts_mst_instrument', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "instrument_data" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "instrument_data" => array(), 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get a person room in
     */
    public function getroomin_backup() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        if ($this->session->userdata('session_id') != FALSE) {
            $session_id = $this->session->userdata('session_id');
        } else {
            $sess = md5(microtime(true) . "." . $empid);
            $this->session->set_userdata('session_id', $sess);
            $session_id = $this->session->userdata('session_id');
        }

        $res3 = $this->Ptsadminmodel->get_dtl7('role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($res3->num_rows() < 1) {
            $result = $this->Ptscommon->roomin($roomcode, $roleid, $empid, $empname, $email, $session_id);
            if ($result == TRUE) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Room In Successful, please go ahead.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You are already in Room');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function getroomin() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        if ($this->session->userdata('session_id') != FALSE) {
            $session_id = $this->session->userdata('session_id');
        } else {
            $sess = md5(microtime(true) . "." . $empid);
            $this->session->set_userdata('session_id', $sess);
            $session_id = $this->session->userdata('session_id');
        }



        $userData = $this->db->get_where("mst_employee", array("id" => $empid, "is_active" => 1, "is_blocked" => 0))->row_array();
        if (!empty($userData)) {
            $res3 = $this->Ptsadminmodel->get_dtl7('role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'pts_trn_emp_roomin');
            if ($res3->num_rows() < 1) {
                $result = $this->Ptscommon->roomin($roomcode, $roleid, $empid, $empname, $email, $session_id, $remark);
                if ($result == TRUE) {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Room In Successful, please go ahead.');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again.');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            } else {
                $res3 = $res3->row_array();
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You are already in Room ' . $res3['room_code']);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'The user does not exist. Please provide the correct user name and password.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get a person room out
     */
    public function getroomout() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $session_id = $this->session->userdata('session_id');
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $getres = $this->Ptsadminmodel->get_dtl7('role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($getres->num_rows() > 0) {
            $roominData = $getres->row_array();
            $roomcode2 = $roominData['room_code'];

            $getroommres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'room_code', $roomcode, 'pts_trn_emp_roomin');
            if ($getroommres->num_rows() > 0) {
                $result = $this->Ptscommon->roomout($roleid, $empid, $empname, $email, $remark);
                if ($result == TRUE) {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Room Out Successful');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again.');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action because you have selected wrong room, please select the correct room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please do room in first, Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Balance Frequency List
     * Created By :Rahul Chauhan
     * Date:27-02-2020
     */
    public function GetBalanceFrequencyList() {
        $this->load->model('Ptscommon');
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $headers = apache_request_headers(); // fetch header value
        $param = array();
        $records_per_page = $this->Ptscommon::records_per_page;
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_balance_frequency_data', $param);
        $groupparams = array("balance_no", "frequency_id");
        $res_group = $this->Ptscommon->getMultipleRowswithgroupby('default', 'pts_mst_balance_frequency_data', $param, $groupparams, $page);
        $count = $this->Ptscommon->getMultipleRowswithgroupbyCount('default', 'pts_mst_balance_frequency_data', $param, $groupparams);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "balance_frequency_list" => $res_group, "balance_frequency_list_all" => $res, 'total_records' => $count, 'records_per_page' => $records_per_page);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Balance Frequency Data Found', "balance_frequency_list" => array(), 'total_records' => 0, 'records_per_page' => $records_per_page);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    /**
     * Get Room In User List List 
     * Created By :Rahul Chauhan
     * Date:28-02-2020 
     */
    public function GetRoomInUserDetail() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $system_id = isset($_GET['terminal_id']) ? $_GET['terminal_id'] : '';
        $res = $this->Ptscommon->GetRoomInUserDetail($system_id);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "roomin_user" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Vertical Sampler Found', "roomin_user" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Running Batch Details 
     * Created By :Bhupendra kumar
     * Date:02-03-2020 
     */
    public function GetrunningBatchData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $system_id = isset($_GET['terminal_id']) ? $_GET['terminal_id'] : '';
        $res = $this->Ptscommon->GetrunningBatchData($system_id);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "BatchDetail" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Record Found', "BatchDetail" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get start a batch in room
     */
    public function getbatchin() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $batchcod = isset($_POST['batchcod']) ? $_POST['batchcod'] : '';
        $prodcode = isset($_POST['prodcode']) ? $_POST['prodcode'] : '';
        $product_desc = isset($_POST['product_desc']) ? $_POST['product_desc'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        // $session_id = $this->session->userdata('session_id');
        $getres = $this->Ptsadminmodel->get_dtl7('role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($getres->num_rows() > 0) {
            $roominData = $getres->row_array();
            $roomcode2 = $roominData['room_code'];

            $getroommres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'room_code', $roomcode, 'pts_trn_emp_roomin');
            if ($getroommres->num_rows() > 0) {
                $res2 = $this->Ptsadminmodel->get_dtl3('room_code', $roomcode2, 'pts_trn_batchin');
                if ($res2->num_rows() < 1) {
                    $result = $this->Ptscommon->batchin($roomcode2, $batchcod, $prodcode, $product_desc, $roleid, $empid, $empname, $email);
                    if ($result == TRUE) {
                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Batch-In Successful, please go ahead.');
                        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    } else {
                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again.');
                        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    }
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Batch is already running in this room, Thank you...!');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    exit;
                }
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action because you have selected wrong room, please select the correct room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please do room in first, Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Room Wise Batch and Product List 
     * Created By :Rahul Chauhan
     * Date:29-02-2020
     */
    public function getRoomBatchInProductData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $param = array("room_code" => $room_code, "isactive" => "1", "batch_out_time" => null);
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_trn_batchin', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "batch_product_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Vertical Sampler Found', "batch_product_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function getbatchout() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';

        $getres = $this->Ptsadminmodel->get_dtl7('role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($getres->num_rows() > 0) {
            $roominData = $getres->row_array();
            $roomcode2 = $roominData['room_code'];

            $getroommres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_email', $email, 'room_code', $roomcode, 'pts_trn_emp_roomin');
            if ($getroommres->num_rows() > 0) {
                $res2 = $this->Ptsadminmodel->get_dtl3('room_code', $roomcode2, 'pts_trn_batchin');
                if ($res2->num_rows() > 0) {
                    $result = $this->Ptscommon->batchout($roomcode2);
                    if ($result == TRUE) {
                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Batch-Out Successful');
                        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    } else {
                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again.');
                        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    }
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Batch is not running in this room, Thank you...!');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                    exit;
                }
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action because you have selected wrong room, please select the correct room, Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please do room in first, Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to get User List
     * Created By : Bhupendra Kumar
     * Created Date : 05-03-2020
     */

    public function getUsersList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("is_active" => "1");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_employee', $param);
        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "user_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "user_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function getUsersList2() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $this->db->select("id,email2,emp_name,emp_email");
        $this->db->from("mst_employee");
        $this->db->where("is_active", "1");
        $res = $this->db->get()->result_array();
        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "user_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "user_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function getterminalnumbersList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "active");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_sys_id', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "treminaldata" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "treminaldata" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Instrument Log Report
     * Created By : Rahul Chauhan
     * Created Date:11-03-2020
     */
    public function GetInstrumentReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['instrument_no'] = isset($_POST['instrument_no']) ? $_POST['instrument_no'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        // print_r($post);
        // die();
        $res = $this->Ptscommon->GetInstrumentReport($post);
        // echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get LAF Pressure Diff Data
     * Created By : Bhupendra Kumar
     * Edited By: ****
     * Created Date:12-03-2020
     * Edit Date:***
     */
    public function lafPressureDiffReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['booth_no'] = isset($_POST['booth_no']) ? $_POST['booth_no'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $post['end_date'] = isset($_POST['end_date']) ? $_POST['end_date'] : '';
        $res = $this->Ptscommon->lafPressureDiffReport($post);
//        echo "<pre/>";print_r($res);exit;
        foreach ($res as $key => $value) {
            $attachments = $this->Ptscommon->GetAttachmentListforReport($value['id']);
            $res[$key]['logAttachments'] = $attachments;
            //echo "<pre/>";print_r($attachments);
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Vaccume Cleaner Report
     * Created By : Rahul Chauhan
     * Created Date:12-03-2020
     */
    public function getVaccumeReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['vaccume_no'] = isset($_POST['vaccume_no']) ? $_POST['vaccume_no'] : '';
        $post['room_code'] = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        // print_r($post);
        // die();
        $res = $this->Ptscommon->getVaccumeReport($post);
        // echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Stage Master Save
     * Created By :Rahul Chauhan
     * Date:13-03-2020
     */
    public function saveStage() {
//        echo $this->session->userdata('user_id');exit;
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $log = isset($_POST['log']) ? $_POST['log'] : '';
        $field_name = isset($_POST['field_name']) ? $_POST['field_name'] : '';
        $field_value = isset($_POST['field_value']) ? $_POST['field_value'] : '';
        $stage_id = isset($_POST['stage_id']) ? $_POST['stage_id'] : '';
        $status = isset($_POST['status']) ? $_POST['status'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $modified_by = $this->session->userdata('empname');
        $auditArray = ['log_name' => $log, 'field_name' => $field_name, 'field_value' => $field_value, 'status' => ucfirst($status), 'remarks' => $remark];
        $auditArray = $this->Ptscommon->getNAUpdated($auditArray);
        if ($stage_id > 0) {
            $master_previous_data = [];
            $old_data = $this->db->get_where('pts_mst_stages', array("id" => $stage_id))->row_array();
            if (!empty($old_data)) {
                $master_previous_data = ['field_name' => $old_data['field_name'], "field_value" => $old_data['field_value'], "status" => ucfirst($old_data['status'])];
                $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
            }
            $res = $this->db->update("pts_mst_stages", array("log" => $log, "field_name" => $field_name, "field_value" => $field_value, "modified_on" => date("Y-m-d H:i:s"), "modified_by" => $this->session->userdata('empname'), "status" => $status, "remark" => $remark), array("id" => $stage_id));
            if (!empty($res)) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "log:" . $log;
                $uniquefiled2 = "field_name:" . $field_name;
                $uniquefiled3 = "";
                $post = array("log" => $log, "field_name" => $field_name, "field_value" => $field_value, "modified_on" => date("Y-m-d H:i:s"), "modified_by" => $this->session->userdata('empname'), "status" => $status, "remark" => $remark);
                $auditParams = array('command_type' => 'update', 'activity_name' => 'Log Configuration Master', 'type' => 'stage_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_stages', 'primary_id' => $stage_id, 'post_params' => $auditArray, 'master_previous_data' => $master_previous_data);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */


                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Stage Data Updated Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Stage Data does not Updated');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_stages', array("log" => $log, "field_name" => $field_name, "field_value" => $field_value, "status" => "active"));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'This Record is Already Exist');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
            $post = array("log" => $log, "field_name" => $field_name, "field_value" => $field_value, "created_by" => $this->session->userdata('empname'), "remark" => $remark);
            $res = $this->Ptscommon->addToTable($db = 'default', 'pts_mst_stages', $post, $param = '');
            //print_r($res);exit;
            if (!empty($res)) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "log:" . $log;
                $uniquefiled2 = "field_name:" . $field_name;
                $uniquefiled3 = "";
                $post['status'] = "active";
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Log Configuration Master', 'type' => 'stage_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_stages', 'primary_id' => $res, 'post_params' => $auditArray);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */


                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Record Added Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Record does not Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
    }

    /**
     * Delete Stage Master
     * Created By :Rahul Chauhan
     * Date:13-03-2020
     */
    public function deleteStage() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        if ($id > 0) {
            $res = $this->db->update('pts_mst_stages', array("status" => "inactive"), array("id" => $id));
            if (!empty($res)) {

                /* Start - Insert elog audit histry */
                $activity = $this->Ptscommon->getActivityNameType('pts_mst_stages');
                $auditParams = array('command_type' => 'delete', 'activity_name' => $activity['activity_name'], 'type' => $activity['type'], 'table_name' => 'pts_mst_stages', 'primary_id' => $id, 'post_params' => array('id' => $id, "status" => 'inactive'));
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Stage Name Deleted Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Stage Name does not Deleted');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Somethig went wrong.Please try again');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     *  Save Report Header Master Data
     * Created By :Rahul Chauhan
     * Date:16-03-2020
     */
    public function saveHeader() {
//        echo 'hello';exit;
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $document_no = isset($_POST['document_no']) ? $_POST['document_no'] : '';
        $effective_date = isset($_POST['effective_date']) ? date('Y-m-d', strtotime($_POST['effective_date'])) : '';
        $retired_date = isset($_POST['retired_date']) && !empty($_POST['retired_date']) ? date('Y-m-d', strtotime($_POST['retired_date'])) : '';
        $version_no = isset($_POST['version_no']) ? $_POST['version_no'] : '';
        $user_version_no = isset($_POST['user_version_no']) ? $_POST['user_version_no'] : '';
        $form_no = isset($_POST['form_no']) ? $_POST['form_no'] : '';
        $header_id = isset($_POST['header_id']) ? $_POST['header_id'] : '';
        $header_Log_id = isset($_POST['header_Log_id']) ? $_POST['header_Log_id'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $status = isset($_POST['status']) ? $_POST['status'] : '';
        $alertondate = isset($_POST['alertondate']) ? $_POST['alertondate'] : '';
        $footer_name = isset($_POST['footer_name']) ? $_POST['footer_name'] : '';
        $log_data = $this->db->where('id', $header_Log_id)->from('pts_mst_activity')->get()->row_array();
        $log_name = !empty($log_data) ? $log_data['activity_name'] : '';
        $userlist = json_decode($_POST['userlist'], true);
        $useremail = implode(",", $userlist);
//echo $effective_date;exit;
        //To check last record's Retired date
        $auditRetiredDate = isset($_POST['retired_date']) && !empty($_POST['retired_date']) ? $_POST['retired_date'] : '';
        $auditArray = ['log_name' => $log_name, 'version' => $version_no, 'form_no' => $form_no, 'document_ref_no' => $document_no, 'footer_name' => $footer_name, 'effective_date' => $_POST['effective_date'], 'retired_date' => $auditRetiredDate, 'alert_days' => $alertondate, 'email_assigned' => $useremail, 'status' => ucfirst($status), 'remarks' => $remark];
        $auditArray = $this->Ptscommon->getNAUpdated($auditArray);
        $ExpDateRecordID = "";
        $ExpDate = "";
        $preRes = $this->Ptscommon->getMultipleRows('default', 'pts_mst_report_header', array("header_id" => $header_Log_id));
        if (!empty($preRes)) {
            $count = count($preRes);
            $ExpDate = $preRes[$count - 1]["retired_date"];
            $ExpDateRecordID = $preRes[$count - 1]["id"];
        }
        //End


        if ($header_id > 0) {
            $master_previous_data = [];
            $old_data = $this->db->get_where('pts_mst_report_header', ['id' => $header_id])->row_array();
            if (!empty($old_data)) {
                $auditRetiredDateOld = isset($old_data['retired_date']) && !empty($old_data['retired_date']) ? date('d-M-Y', strtotime($old_data['retired_date'])) : '';
                $master_previous_data = ['form_no' => $old_data['form_no'], 'document_ref_no' => $old_data['document_no'], 'footer_name' => $old_data['footer_name'], 'effective_date' => date('d-M-Y', strtotime($old_data['effective_date'])), 'retired_date' => $auditRetiredDateOld, 'alert_days' => $old_data['alertondate'], 'email_assigned' => $old_data['alert_assigned'], 'status' => ucfirst($old_data['status'])];
                $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
                //echo '<pre>';print_r($master_previous_data);exit;
            }
            if ($retired_date != '') {
                $this->db->where('effective_date <=', date('Y-m-d', strtotime($retired_date)));
            } else {
                $this->db->where('effective_date >', date('Y-m-d', strtotime($effective_date)));
            }
            $this->db->where('id!=', $header_id);
            $this->db->where('header_id', $header_Log_id);
            $exitData = $this->db->get_where("pts_mst_report_header")->result_array();
            if (!empty($exitData) && $exitData[count($exitData) - 1]['id'] > $header_id) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Already exist a header that have effective date greater than retire date');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
            if (isset($retired_date) && $retired_date != '') {
                $res = $this->db->update("pts_mst_report_header", array("header_id" => $header_Log_id, "form_no" => $form_no, "version_no" => $version_no, "user_version_no" => $user_version_no, "effective_date" => date('Y-m-d', strtotime($effective_date)), "retired_date" => date('Y-m-d', strtotime($retired_date)), "document_no" => $document_no, "modified_on" => date("Y-m-d H:i:s"), "modified_by" => $this->session->userdata('empname'), "status" => $status, "remark" => $remark, "alertondate" => $alertondate, 'footer_name' => $footer_name, 'alert_assigned' => $useremail), array("id" => $header_id));
            } else {
                $res = $this->db->update("pts_mst_report_header", array("header_id" => $header_Log_id, "form_no" => $form_no, "version_no" => $version_no, "user_version_no" => $user_version_no, "effective_date" => date('Y-m-d', strtotime($effective_date)), "retired_date" => '', "document_no" => $document_no, "modified_on" => date("Y-m-d H:i:s"), "modified_by" => $this->session->userdata('empname'), "status" => $status, "remark" => $remark, "alertondate" => $alertondate, 'footer_name' => $footer_name, 'alert_assigned' => $useremail), array("id" => $header_id));
            }

            if (!empty($res)) {
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "form_no:" . $form_no;
                $uniquefiled2 = "version_no:" . $version_no;
                $uniquefiled3 = "document_no:" . $document_no;
                $retired_date1 = '';
                if ($retired_date != '') {
                    $retired_date1 = date('d-M-Y 23:59:59', strtotime($retired_date));
                }
                $auditParams = array('command_type' => 'update', 'activity_name' => 'Report Header/Footer Master', 'type' => 'report_header', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_report_header', 'primary_id' => $header_id, 'post_params' => $auditArray, 'master_previous_data' => $master_previous_data);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Report Header Updated Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Report Header does not Updated');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_report_header', array("form_no" => $form_no, "document_no" => $document_no, "header_id" => $header_Log_id));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Header Already Exist');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }



            $post = array("header_id" => $header_Log_id, "form_no" => $form_no, "version_no" => $version_no, "user_version_no" => $user_version_no, "effective_date" => date('Y-m-d', strtotime($effective_date)), "alertondate" => $alertondate, "document_no" => $document_no, "created_by" => $this->session->userdata('empname'), "status" => $status, "remark" => $remark, 'footer_name' => $footer_name, 'alert_assigned' => $useremail);
            if (isset($retired_date) && $retired_date != '') {
                $post['retired_date'] = date('Y-m-d', strtotime($retired_date));
            }
            $res = $this->Ptscommon->addToTable($db = 'default', 'pts_mst_report_header', $post, $param = '');
            if (!empty($res)) {


                if (isset($retired_date) && $retired_date != '') {
                    $post['retired_date'] = date('d-M-Y 23:59:59', strtotime($retired_date));
                }
                $post['effective_date'] = date('d-M-Y', strtotime($effective_date));
                if ($ExpDate == "") {
                    $onedaypredate = date_create(date('Y-m-d', strtotime($effective_date)))->modify('-1 days')->format('Y-m-d');
                    $res1 = $this->db->update("pts_mst_report_header", array("retired_date" => $onedaypredate), array("id" => $ExpDateRecordID));
                }



                /* Start - Insert elog audit histry */
                $uniquefiled1 = "form_no:" . $form_no;
                $uniquefiled2 = "version_no:" . $version_no;
                $uniquefiled3 = "document_no:" . $document_no;
                $post['header_name'] = $log_name;
                unset($post['header_id']);
                $post['effective_date'] = date('d-M-Y 00:00:00', strtotime($effective_date));
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Report Header/Footer Master', 'type' => 'report_header', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_report_header', 'primary_id' => $res, 'post_params' => $auditArray);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */



                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Report Header Added Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Report Header does not Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
    }

    public function saveHeader_bhupendra() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $document_no = isset($_POST['document_no']) ? $_POST['document_no'] : '';
        $effective_date = isset($_POST['effective_date']) ? $_POST['effective_date'] : '';
        $retired_date = isset($_POST['retired_date']) && !empty($_POST['retired_date']) ? $_POST['retired_date'] : '';
        $version_no = isset($_POST['version_no']) ? $_POST['version_no'] : '';
        $form_no = isset($_POST['form_no']) ? $_POST['form_no'] : '';
        $header_id = isset($_POST['header_id']) ? $_POST['header_id'] : '';
        $header_Log_id = isset($_POST['header_Log_id']) ? $_POST['header_Log_id'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $status = isset($_POST['status']) ? $_POST['status'] : '';
        $alertondate = isset($_POST['alertondate']) ? $_POST['alertondate'] : '';
        $footer_name = isset($_POST['footer_name']) ? $_POST['footer_name'] : '';

        //To check last record's Retired date 
        $preRes = $this->Ptscommon->getMultipleRows('default', 'pts_mst_report_header', array("header_id" => $header_Log_id));
        $count = count($preRes);
        $ExpDate = $preRes[$count - 1]["retired_date"];
        $ExpDateRecordID = $preRes[$count - 1]["id"];
        //End

        if ($header_id > 0) {
            $res = $this->db->update("pts_mst_report_header", array("header_id" => $header_Log_id, "form_no" => $form_no, "version_no" => $version_no, "effective_date" => date('Y-m-d', strtotime($effective_date)), "retired_date" => date('Y-m-d', strtotime($retired_date)), "document_no" => $document_no, "modified_on" => date("Y-m-d H:i:s"), "modified_by" => $this->session->userdata('empname'), "status" => $status, "footer_name" => $footer_name, "remark" => $remark, "alertondate" => $alertondate), array("id" => $header_id));
            if (!empty($res)) {
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "form_no:" . $form_no;
                $uniquefiled2 = "version_no:" . $version_no;
                $uniquefiled3 = "document_no:" . $document_no;
                $auditParams = array('command_type' => 'update', 'activity_name' => 'Report Header/Footer Master', 'type' => 'report_header', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_report_header', 'primary_id' => $header_id, 'post_params' => array("header_id" => $header_Log_id, "form_no" => $form_no, "version_no" => $version_no, "effective_date" => date('Y-m-d', strtotime($effective_date)), "retired_date" => date('Y-m-d', strtotime($retired_date)), "alertondate" => $alertondate, "document_no" => $document_no));
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Report Header Updated Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Report Header does not Updated');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_report_header', array("form_no" => $form_no, "document_no" => $document_no, "header_id" => $header_Log_id));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Header Already Exist');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }

            $post = array("header_id" => $header_Log_id, "form_no" => $form_no, "version_no" => $version_no, "effective_date" => date('Y-m-d', strtotime($effective_date)), "retired_date" => date('Y-m-d', strtotime($retired_date)), "alertondate" => $alertondate, "document_no" => $document_no, "created_by" => $this->session->userdata('empname'), "status" => $status, "footer_name" => $footer_name, "remark" => $remark);
            $res = $this->Ptscommon->addToTable($db = 'default', 'pts_mst_report_header', $post, $param = '');
            if (!empty($res)) {

                if ($ExpDate == "") {
                    $res = $this->db->update("pts_mst_report_header", array("retired_date" => date('Y-m-d', strtotime($effective_date))), array("id" => $ExpDateRecordID));
                }

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "form_no:" . $form_no;
                $uniquefiled2 = "version_no:" . $version_no;
                $uniquefiled3 = "document_no:" . $document_no;
                unset($post['remark']); // as we are not posting any remark
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Report Header/Footer Master', 'type' => 'report_header', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_report_header', 'primary_id' => $res, 'post_params' => $post);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Report Header Added Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Report Header does not Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
    }

    /**
     * Get Filter Room List
     * @param string $method1
     * @param string $tokencheck
     */
    public function Getroomfilters_forApproval() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("is_approved" => 0);
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_filter', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "room_filterlist" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Room does not found', "room_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Filter Approved
     * Created By : Bhupendra
     * Created On : 14march2020
     */
    public function getfilterApproved() {
        $headers = apache_request_headers(); // fetch header value
        $id = $_POST['id'];
        $this->load->model('Ptscommon');
        $post = array("is_approved" => 1);
        $param = array("id" => $id);
        $res = $this->Ptscommon->updateToTable($db = 'default', 'pts_mst_filter', $post, $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Successfully Approved');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Header List
     * Created By :Rahul Chauhan
     * Date:16-03-2020
     */
//     public function getHeaderList() {
//         $this->load->model('Ptscommon');
//         $headers = apache_request_headers(); // fetch header value
//         $report_id = isset($_GET['report_id']) ? $_GET['report_id'] : '';
//         $param = array("status" => "active");
//         $this->db->select("pts_mst_report_header.*,pts_mst_report.report_name");
//         $this->db->from("pts_mst_report_header");
//         $this->db->join(" pts_mst_report", " pts_mst_report.id=pts_mst_report_header.report_id", 'left');
//         // $this->db->where("pts_mst_report_header.status", "active");
// //        $this->db->or_where("pts_mst_report_header.status", "default");
//         if ($report_id > 0) {
//             $this->db->where("pts_mst_report_header.report_id", $report_id);
//             $this->db->order_by("pts_mst_report_header.id", 'DESC');
//         }
//         $res = $this->db->get()->result_array();
//         if (!empty($res)) {
//             $responseresult = array(STATUS => TRUE, "result" => TRUE, "header_list" => $res);
//             $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
//         } else {
//             $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Header Found', "header_list" => array());
//         }
//     }


    public function getHeaderList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $header_id = isset($_GET['header_id']) ? $_GET['header_id'] : '';
        $param = array("status" => "active");
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $this->db->select("pts_mst_report_header.*,pts_mst_activity.activity_name");
        $this->db->from("pts_mst_report_header");
        $this->db->join("pts_mst_activity", " pts_mst_activity.id=pts_mst_report_header.header_id", 'left');
        if ($header_id > 0) {
            $this->db->where("pts_mst_report_header.header_id", $header_id);
            $this->db->order_by("pts_mst_report_header.id", 'DESC');
        }
        //$count = $this->db->get()->num_rows();
        $records_per_page = $this->Ptscommon::records_per_page;
        if (!empty($page)) {
            $offset = ($page - 1) * ($records_per_page);
            $this->db->limit($records_per_page, $offset);
        }
        $res = $this->db->get()->result_array();

        $this->db->select("pts_mst_report_header.*,pts_mst_activity.activity_name");
        $this->db->from("pts_mst_report_header");
        $this->db->join("pts_mst_activity", " pts_mst_activity.id=pts_mst_report_header.header_id", 'left');
        if ($header_id > 0) {
            $this->db->where("pts_mst_report_header.header_id", $header_id);
            $this->db->order_by("pts_mst_report_header.id", 'DESC');
        }
        $totalcount = $this->db->get()->num_rows();

        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "header_list" => $res, 'records_per_page' => $records_per_page, "total_records" => $totalcount);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Header Found', "header_list" => [], 'records_per_page' => $records_per_page, "total_records" => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function getHeaderByLogid() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $header_id = isset($_POST['header_id']) ? $_POST['header_id'] : '';
        $start_date = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $end_date = isset($_POST['end_date']) ? $_POST['end_date'] : '';
        $param = array("status" => "active");
        $this->db->select("pts_mst_report_header.*,pts_mst_activity.activity_name,pts_mst_report_header.user_version_no as version_no");
        $this->db->from("pts_mst_report_header");
        $this->db->join("pts_mst_activity", " pts_mst_activity.id=pts_mst_report_header.header_id", 'left');
        if ($header_id > 0) {
            $this->db->where("pts_mst_report_header.header_id", $header_id);
            $this->db->where('pts_mst_report_header.effective_date <=', date('Y-m-d', strtotime($start_date)));
            $where = " (pts_mst_report_header.retired_date >= '" . date('Y-m-d', strtotime($end_date)) . "' OR retired_date is null OR retired_date='') ";
            $this->db->where($where);
            //$this->db->where('pts_mst_report_header.retired_date >=', date('Y-m-d', strtotime($end_date)));
            $this->db->order_by("pts_mst_report_header.id", 'DESC');
        }
        $res = $this->db->get()->result_array();
		//        echo $this->db->last_query();exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "header_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Header Found', "header_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* Get Previous Product and Batch Number 
     * Created By : Bhupendra kumar
     * Date: 15-02-2020 
     */

    public function GetPreBactProduct() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $res = $this->Ptscommon->GetPreBactProduct();
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "PreBactProduct" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found', "PreBactProduct" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Previous Product and Batch Number 
     * Created By : Bhupendra kumar
     * Date: 15-02-2020 
     */
    public function GetPreBactProductofPrefilter() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $res = $this->Ptscommon->GetPreBactProductofPrefilter($room_code);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "PreBactProduct" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found', "PreBactProduct" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function udatePreFilterToStatus() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $seletion_status = isset($_GET['seletion_status']) ? $_GET['seletion_status'] : '';
        $this->db->where("is_to", "1");
        $res = $this->db->update("pts_trn_pre_filter_cleaning", array("is_to" => "0"));
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "PreBactProduct" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found', "PreBactProduct" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Delete Stage Master
     * Created By :Rahul Chauhan
     * Date:13-03-2020
     */
    public function deleteHeader() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $action = isset($_POST['action']) ? $_POST['action'] : '';
        if ($id > 0 && $action == 'Deactive') {
            $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_report_header', array("id" => $id, "status" => "default"));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => "it's default Report Header.Please set another default header first then remove it");
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $res = $this->db->update('pts_mst_report_header', array("status" => "inactive"), array("id" => $id));
                if (!empty($res)) {

                    /* Start - Insert elog audit histry */
                    $activity = $this->Ptscommon->getActivityNameType('pts_mst_report_header');
                    $auditParams = array('command_type' => 'delete', 'activity_name' => $activity['activity_name'], 'type' => $activity['type'], 'table_name' => 'pts_mst_report_header', 'primary_id' => $id, 'post_params' => array('id' => $id, "status" => 'inactive'));
                    $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                    /* End -  Insert elog audit histry */

                    $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Report Header Deleted Successfully');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Report Header does not Deleted');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }
        } elseif ($id > 0 && $action == 'Set Default') {
            $res = $this->db->update('pts_mst_report_header', array("status" => "active"), array("status" => "default"));
            $res = $this->db->update('pts_mst_report_header', array("status" => "default"), array("id" => $id));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Set Default Report Header Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Report Header does not Deleted');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Somethig went wrong.Please try again');
        }
    }

    /* Get Previous Product and Batch Number 
     * Created By : Bhupendra kumar
     * Date: 15-02-2020 
     */

    public function GetPreBactProductofprocesslog() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $res = $this->Ptscommon->GetPreBactProductofprocesslog();
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "PreBactProduct" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Activity Found', "PreBactProduct" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to get the Vertical Sampler records for report
     * Created By : Rahul Chauhan
     * Created Date : 17-03-2020
     */

    public function verticalSamplingReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $sampling_no = isset($_POST['sampling_no']) ? $_POST['sampling_no'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        $res = $this->Ptscommon->verticalSamplingReport($sampling_no, $post);

        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to get Balance Calibration records for report
     * Created By : Rahul Chauhan
     * Created Date : 22-03-2020
     */

    public function getCalibrationReportData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['room_code'] = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $post['balance_no'] = isset($_POST['balance_no']) ? $_POST['balance_no'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $post['end_date'] = isset($_POST['end_date']) ? $_POST['end_date'] : '';

        $res = $this->Ptscommon->getCalibrationReportData($post);

        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to audit log
     * Created By : Bhupendra kumar
     * Created Date : 16-03-2020
     */

    public function AuditLog($terminalID, $processname_master, $activity_sub_task_actions, $action_performed, $created_by, $oldval, $newval) {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $insertArray = array("terminalID" => $terminalID, "processname_master" => $processname_master, "activity_sub_task_actions" => $activity_sub_task_actions, "action_performed" => $action_performed, "created_by" => $created_by, "oldval" => $oldval, "newval" => $newval);
        $res = $this->Ptscommon->addToTable($db = 'default', 'pts_audit_log', $insertArray, $param = '');
    }

    /* This Function is used deactivate record
     * Created By : Bhupendra kumar
     * Created Date : 17-03-2020
     */

    public function CommonDelete() {
        $this->load->model('Ptscommon');
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $timezone = "Asia/Kolkata";
        date_default_timezone_set($timezone);
        $emp_code = $this->session->userdata('empcode');
        $roleid = $this->session->userdata('roleid');
        $block_code = $this->session->userdata('empblock_code');
        $id = $_POST['id'];
        $tblname = $_POST['tblname'];
        $colname = $_POST['colname'];
        $res = $this->Ptscommon->deleterecords($colname, $id, $this->session->userdata('empcode'), date("Y-m-d H:i:s"), $tblname);
        if (!empty($res)) {
            $param = array($colname => $id);
            $rocordres = $this->Ptscommon->getMultipleRows('default', $tblname, $param);
            if ($tblname == 'pts_mst_line_log') {
                $equipment_data = explode(",", $rocordres[0]['equipment_code']);
                foreach ($equipment_data as $key => $value) {
                    $this->db->update("mst_equipment", array("is_in_line" => "0"), array("equipment_code" => $value, "equipment_type" => "Fixed"));
                }
            }
            /* Start - Insert elog audit histry */
            $activity = $this->Ptscommon->getActivityNameType($tblname);
            $auditParams = array('command_type' => 'delete', 'activity_name' => $activity['activity_name'], 'type' => $activity['type'], 'table_name' => $tblname, 'primary_id' => $id, 'post_params' => array("Record Detail as :- " => $rocordres, "Deactivated_by" => $this->session->userdata('empcode'), "Deactivated_on" => date("Y-m-d H:i:s")));
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */

            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Successfully deactivated...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used deactivate record
     * Created By : Bhupendra kumar
     * Created Date : 19-03-2020
     */

    public function CommonDelete2() {
        $this->load->model('Ptscommon');
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $timezone = "Asia/Kolkata";
        date_default_timezone_set($timezone);
        $emp_code = $this->session->userdata('empcode');
        $roleid = $this->session->userdata('roleid');
        $block_code = $this->session->userdata('empblock_code');
        $id = $_POST['id'];
        $tblname = $_POST['tblname'];
        $colname = $_POST['colname'];
        $res = $this->Ptscommon->deleterecords2($colname, $id, $this->session->userdata('empcode'), date("Y-m-d H:i:s"), $tblname);
        if (!empty($res)) {

            /* Start - Insert elog audit histry */
            $activity = $this->Ptscommon->getActivityNameType($tblname);
            $auditParams = array('command_type' => 'delete', 'activity_name' => $activity['activity_name'], 'type' => $activity['type'], 'table_name' => $tblname, 'primary_id' => $id, 'post_params' => array($colname => $id, "isactive" => 0, "modified_by" => $this->session->userdata('empname'), "modified_on" => date("Y-m-d H:i:s")));
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */

            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Successfully deactivated...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Assigned Role for Edit
     * Created By : Bhupendra
     * Date:19-03-2020
     */
    public function GetEditableRecord() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $roleid = $_POST['roleid'];
        $param = array("roleid" => $roleid);
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_role_workassign_to_mult_roles', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "role_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Role Found', "role_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Balance calibration Standard List calibration wise
     * Created By :Rahul Chauhan
     * Date:23-03-2020
     */
    public function GetBalanceStandardData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $balance_no = isset($_GET['balance_no']) ? $_GET['balance_no'] : '';
        $param = array("status" => "active", "balance_calibration_id" => $balance_no);

        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_balance_calibration_standard', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "standard_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Standard Data Found', "standard_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Environmental Report Data
     * Created By : Bhupendra Kumar
     * Edited By: ****
     * Created Date:23-03-2020
     * Edit Date:***
     */
    public function envPressureReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['room_no'] = isset($_POST['room_no']) ? $_POST['room_no'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $post['end_date'] = isset($_POST['end_date']) ? $_POST['end_date'] : '';
        // print_r($post);exit;
        $res = $this->Ptscommon->envPressureReport($post);
//        echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Material Relocation Report Data
     * Created By : Bhupendra Kumar
     * Edited By: ****
     * Created Date:23-03-2020
     * Edit Date:***
     */
    public function materialRelocationReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['room_no'] = isset($_POST['room_no']) ? $_POST['room_no'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        $res = $this->Ptscommon->materialRelocationReport($post);
        $new_array = array();
//        foreach ($res as $key => $value) {
//
//            if (!isset($new_array[$value['id']])) {
//                $new_array[$value['id']] = $value;
//            }
//        }
//        foreach ($res as $key => $value) {
//            $tempArray[$value['doc_no']][] = $value;
//        }
//        $tempArray = $new_array;
//        echo '<pre>';print_r($tempArray);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Tablet Tooling Report Data
     * Created By : Rahul Chauhan
     * Edited By: ****
     * Created Date:25-03-2020
     * Edit Date:***
     */
    public function getToolingReportData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['product_no'] = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $post['punch_set'] = isset($_POST['punch_set']) ? $_POST['punch_set'] : '';
        $post['room_code'] = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        // print_r($post);exit;
        $res = $this->Ptscommon->getToolingReportData($post);
        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get a list of Balance Standard Calabration
     * @param string $method1 
     * @param string $tokencheck
     */
    public function Getstandardcalibration() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $bal_calib_id = isset($_POST['bal_calib_id']) ? $_POST['bal_calib_id'] : '';
        $this->Apifunction->check_variables($bal_calib_id, "Balance Standard Calabration", $numricCheck = 'no');

        $param = array("balance_calibration_id" => $bal_calib_id);
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_balance_calibration_standard', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "material_relocation" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Balance Standard Calabration Found');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Save Environmental Master Data
     * Created By : Rahul Chauhan
     * Edited By: ****
     * Created Date:01-04-2020
     * Edit Date:***
     */
    public function saveMasterData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['room_code'] = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $post['data_log_id'] = isset($_POST['logger_id']) ? $_POST['logger_id'] : 'N/A';
        $post['magnelic_id'] = isset($_POST['gauge_id']) ? $_POST['gauge_id'] : 'N/A';
        $post['globe_pressure_diff'] = isset($_POST['pressure']) ? $_POST['pressure'] : 'N/A';
        $post['temp_from'] = isset($_POST['from_temp']) ? $_POST['from_temp'] : '';
        $post['temp_to'] = isset($_POST['to_temp']) ? $_POST['to_temp'] : '';
        $post['rh_from'] = isset($_POST['from_humidity']) ? $_POST['from_humidity'] : '';
        $post['rh_to'] = isset($_POST['to_humidity']) ? $_POST['to_humidity'] : '';
        $post['from_pressure'] = isset($_POST['from_pressure']) ? $_POST['from_pressure'] : '';
        $post['to_pressure'] = isset($_POST['to_pressure']) ? $_POST['to_pressure'] : '';
        $post['is_positive'] = isset($_POST['is_positive']) ? $_POST['is_positive'] : '';
        if ($post['is_positive'] == 'true') {
            $post['positive_check'] = '1';
        } else {
            $post['positive_check'] = '0';
        }
        unset($post['is_positive']);
//        print_r($post);exit;
        $this->db->insert("pts_trn_env_cond_diff", $post);
        $last_id = $this->db->insert_id();
        if ($last_id > 0) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'data saved successfully', "last_id" => $last_id);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'something went wrong', "last_id" => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End

    /**
     * Get Acceptance Limit Data Balance wise
     * Created By : Bhupendra Kumar
     * Date: 04-02-2020
     */
    public function GetAcceptanceLimitData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $balance_no = isset($_GET['balance_no']) ? $_GET['balance_no'] : '';
        $param = array("status" => "active", "balance_calibration_id" => $balance_no);
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_balance_calibration_standard', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "AcceptanceLimit_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Standard Data Found', "AcceptanceLimit_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get All Master List
     * Created By : Rahul Chauhan
     * Date: 06-04-2020
     */
    public function getMasterData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers();
        $param = array("status" => "active");
        $form_id_to_hide = [16, 23];
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_master', $param);
        if (!empty($res)) {
            foreach ($res as $key => $v) {
                if (in_array($v['id'], $form_id_to_hide)) {
                    unset($res[$key]);
                }
            }
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "master_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Master Activity Found', "master_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get All Report List
     * Created By : Rahul Chauhan
     * Date: 06-04-2020
     */
    public function getReportList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $param = array("status" => "active");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_report', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "report_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Master Activity Found', "report_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * To check is record already exist or not
     * Created By : Bhupendra Kumar
     * Date: 04-06-2020
     */
    public function chkcode() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $tblname = isset($_POST['tblname']) ? $_POST['tblname'] : '';
        $col = isset($_POST['col']) ? $_POST['col'] : '';
        $val = isset($_POST['code']) ? $_POST['code'] : '';
        $this->load->model('Ptsadminmodel');
        $response = $this->Ptsadminmodel->checkcode($tblname, $col, $val);
        echo json_encode($response);
    }

    /**
     * Save User Privileges
     * Created By : Rahul Chauhan
     * Edited By: ****
     * Created Date:07-04-2020
     * Edit Date:***
     */
    public function saveUserPrivilege() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $module_type = isset($_POST['module_type']) ? $_POST['module_type'] : '';
        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
        $module_data = json_decode($_POST['log_data'], true);
        $insertArray = [];
        $master_logs = $report_logs = '';
        $newPrivilege = [];
        $field = 'report_name';
        if($module_type=='master'){
            $field = 'master_name';
        }

        foreach ($module_data as $key => $value) {
            $add = $edit = $view = $add1 = $edit1 = $view1 = '';
            if (isset($value['is_create']) && $value['is_create'] != '') {
                $is_create = 1;$add = 'Add, ';$add1 = 'Add';
            } else {
                $is_create = 0;
            }
            if (isset($value['is_edit']) && $value['is_edit'] != '') {
                $is_edit = 1;$edit = 'Update, ';$edit1 = 'Update, ';
                if($module_type=='report'){
                    $edit1 = 'View & Print, ';
                }
            } else {
                $is_edit = 0;
            }
            if (isset($value['is_view']) && $value['is_view'] != '') {
                $is_view = 1;$view = 'View, ';$view1 = 'View';
            } else {
                $is_view = 0;
            }
            if ((isset($value['master_name']) && $value['master_name'] != '') && ((isset($value['is_create']) && $value['is_create'] != '') || (isset($value['is_edit']) && $value['is_edit'] != '') || (isset($value['is_view']) && $value['is_view'] != ''))) {
                $master_logs .= $value[$field] . ': ' . $add . $edit . $view;
            }
            if ((isset($value['report_name']) && $value['report_name'] != '') && ((isset($value['is_create']) && $value['is_create'] != '') || (isset($value['is_edit']) && $value['is_edit'] != '') || (isset($value['is_view']) && $value['is_view'] != ''))) {
                $report_logs .= $value[$field] . ': ' . $add . $edit . $view;
            }
            $newPrivilege[$value['id']] = [0=>$value[$field],1=>$add,2=>$edit1,3=>$view];
            $insertArray[] = array("user_id" => $user_id, "module_id" => $value['id'], "module_type" => $module_type, "is_create" => intval($is_create), "is_edit" => intval($is_edit), "is_view" => intval($is_view), "created_by" => $this->session->userdata('user_id'), "status" => "active");
        }

        $master_logs = rtrim(rtrim($master_logs, ', '), ', ');
        $report_logs = rtrim(rtrim($report_logs, ', '), ', ');
        $logs1 = !empty($report_logs) ? $report_logs : '';
        $logs = !empty($master_logs) ? $master_logs : $logs1;

        $old_data = $this->Ptscommon->getUserOldRights($user_id, $module_type);
        $master_previous_data = $oldPrivilege = [];
        $old_master_logs = '';
        if (!empty($old_data)) {
            foreach ($old_data as $key => $value) {
                $add = $edit = $view = $add1 = $edit1 = $view1 = '';
                if (isset($value['is_create']) && $value['is_create'] != '') {
                    $is_create = 1;$add = 'Add, ';$add1 = 'Add';
                } else {
                    $is_create = 0;
                }
                if (isset($value['is_edit']) && $value['is_edit'] != '') {
                    $is_edit = 1;$edit = 'Update, ';$edit1 = 'Update, ';
                    if($module_type=='report'){
                        $edit1 = 'View & Print, ';
                    }
                } else {
                    $is_edit = 0;
                }
                if (isset($value['is_view']) && $value['is_view'] != '') {
                    $is_view = 1;$view = 'View, ';$view1 = 'View';
                } else {
                    $is_view = 0;
                }
                if ((isset($value['master_name']) && $value['master_name'] != '') && ((isset($value['add']) && $value['add'] != '') || (isset($value['edit']) && $value['edit'] != '') || (isset($value['view']) && $value['view'] != ''))) {
                    $old_master_logs .= $value['master_name'] . ': ' . $add . $edit . $view;
                }
                if ((isset($value['report_name']) && $value['report_name'] != '') && ((isset($value['add']) && $value['add'] != '') || (isset($value['edit']) && $value['edit'] != '') || (isset($value['view']) && $value['view'] != ''))) {
                    $old_master_logs .= $value['report_name'] . ': ' . $add . $edit . $view;
                }
                $oldPrivilege[$value['module_id']] = [0=>$value[$field],1=>$add,2=>$edit1,3=>$view];
            }

            $old_master_logs = rtrim(rtrim($old_master_logs, ', '), ', ');
            $master_previous_data = ["logs" => $old_master_logs];
            $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
        }
        $changes = $this->Ptscommon->getUserNewRolesAndResponsibiltiesChanges($newPrivilege, $oldPrivilege);
        $insertArray1 = ["user_id" => $user_id, "module_type" => $module_type, "logs" => $changes];
        $insertArray1 = $this->Ptscommon->getNAUpdated($insertArray1);
        $operation = !empty($old_data) ? 'update' : 'insert';
        $this->db->delete("pts_mst_user_mgmt", array("user_id" => $user_id, "module_type" => $module_type));
        $res = $this->db->insert_batch('pts_mst_user_mgmt', $insertArray);
        if ($res) {

            /* Start - Insert elog audit histry */
            $uniquefiled1 = "user_id:" . $user_id;
            $uniquefiled2 = "module_type:" . $module_type;
            $uniquefiled3 = "";
            $inserted_id = $this->db->insert_id();
            if ($operation == 'insert') {
                $auditParams = ['command_type' => $operation, 'activity_name' => 'User Access', 'type' => 'Roles and Responsibility', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_user_mgmt', 'primary_id' => $inserted_id, 'post_params' => $insertArray1];
            } else {
                $auditParams = ['command_type' => $operation, 'activity_name' => 'User Access', 'type' => 'Roles and Responsibility', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_user_mgmt', 'primary_id' => $inserted_id, 'post_params' => $insertArray1, 'master_previous_data' => $master_previous_data];
            }
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */

            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'data saved successfully');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'something went wrong.Plese try again.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Assigned modules
     * Created By : Rahul Chauhan
     * Date: 07-04-2020
     */
    public function getUserModuleData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $param['module_type'] = isset($_GET['module_type']) ? $_GET['module_type'] : '';
        $param['user_id'] = isset($_GET['user_id']) ? $_GET['user_id'] : '';
        $res = $this->Ptscommon->getUserModuleData($param);
        foreach ($res as $key => $value) {
            if ($res[$key]['is_create'] == 1) {
                $res[$key]['is_create'] = true;
            } else {
                $res[$key]['is_create'] = false;
            }
            if ($res[$key]['is_edit'] == 1) {
                $res[$key]['is_edit'] = true;
            } else {
                $res[$key]['is_edit'] = false;
            }
            if ($res[$key]['is_view'] == 1) {
                $res[$key]['is_view'] = true;
            } else {
                $res[$key]['is_view'] = false;
            }
            if ($res[$key]['status'] == 'active') {
                $res[$key]['active_status'] = true;
            } else {
                $res[$key]['active_status'] = false;
            }
            $res[$key]['user_id'] = $param['user_id'];
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "user_previlege_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Master Activity Found', "user_previlege_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Generic Function to check user privilege for particular Activity
     * Created By : Rahul Chauhan
     * Date: 08-04-2020
     */
    public function checkUserPrivelege($user_id, $module_id, $module_type, $action, $room_code) {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("module_type" => $module_type, "user_id" => $user_id, "module_id" => $module_id, "room_code" => $room_code);
        $res = $this->Ptscommon->checkUserPrivelege($param);
        if (!empty($res)) {
            return $res;
        } else {
            $res = array("user_id" => $param['user_id'], "module_id" => $param['module_id'], "module_type" => $param['module_type'], "is_create" => 0, "is_edit" => 0, "is_view" => 0);
            return array();
        }
    }

    /**
     * Save User Logs  Privileges
     * Created By : Rahul Chauhan
     * Edited By: ****
     * Created Date:20-04-2020
     * Edit Date:***
     */
    public function saveLogsPrivilege() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $module_type = isset($_POST['module_type']) ? $_POST['module_type'] : '';
        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
        $module_id = isset($_POST['module_id']) ? $_POST['module_id'] : '';
        $room_code = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $type_data = json_decode($_POST['type_id'], true);
        $type_id = implode(",", $type_data);

        $module = $this->db->get_where('pts_mst_activity', ["id" => $module_id])->row_array();
        $module_name = !empty($module) ? $module['activity_name'] : '';

        if (!empty($type_data)) {
            $insertArray = array("user_id" => $user_id, "module_id" => $module_id, "room_code" => $room_code, "module_type" => $module_type, "type_id" => $type_id, "is_create" => 1, "is_edit" => 1, "is_view" => 1, "created_by" => $this->session->userdata('user_id'), "status" => "active");
        } else {
            $insertArray = array("user_id" => $user_id, "module_id" => $module_id, "room_code" => $room_code, "module_type" => $module_type, "is_create" => 1, "is_edit" => 1, "is_view" => 1, "created_by" => $this->session->userdata('user_id'), "status" => "active");
        }

        $old_status = $this->Ptscommon->getUserOldLogsRights($user_id, $room_code);
        $operation = !empty($old_status) ? 'update' : 'insert';
        $master_previous_data = [];
        if (!empty($old_status)) {
            $master_previous_data = ["logs" => $old_status['activity_name'] . " : " . $old_status['type_id'], "status" => ucfirst($old_status['mstatus'])];
            $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
        }
        $this->db->delete("pts_mst_user_mgmt", array("user_id" => $user_id, "module_type" => $module_type, "module_id" => $module_id, "room_code" => $room_code));
        $res = $this->db->insert('pts_mst_user_mgmt', $insertArray);
        if ($res) {
            $insertArray = ["user_id" => $user_id, "logs" => $module_name . " : " . $type_id, "room_code" => $room_code, "module_type" => $module_type, "created_by" => $this->session->userdata('user_id'), "status" => "Active"];
            $insertArray = $this->Ptscommon->getNAUpdated($insertArray);
            /* Start - Insert elog audit histry */
            $uniquefiled1 = "user_id:" . $user_id;
            $uniquefiled2 = "module_type:" . $module_type;
            $uniquefiled3 = "";
            $inserted_id = $this->db->insert_id();
            if ($operation == 'insert') {
                $auditParams = ['command_type' => $operation, 'activity_name' => 'User Access', 'type' => 'Roles and Responsibility', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_user_mgmt', 'primary_id' => $inserted_id, 'post_params' => $insertArray];
            } else {
                $auditParams = ['command_type' => $operation, 'activity_name' => 'User Access', 'type' => 'Roles and Responsibility', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_user_mgmt', 'primary_id' => $inserted_id, 'post_params' => $insertArray, 'master_previous_data' => $master_previous_data];
            }
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */

            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'data saved successfully');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'something went wrong.Plese try again.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Assigned User Log modules
     * Created By : Rahul Chauhan
     * Date: 20-04-2020
     */
    public function getUserLogData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $param['module_type'] = isset($_GET['module_type']) ? $_GET['module_type'] : '';
        $param['user_id'] = isset($_GET['user_id']) ? $_GET['user_id'] : '';
        $res = $this->Ptscommon->getUserModuleData($param);
        foreach ($res as $key => $value) {
            if ($res[$key]['mstatus'] == 'active') {
                $res[$key]['active_status'] = true;
            } else {
                $res[$key]['active_status'] = false;
            }
            $res[$key]['user_id'] = $param['user_id'];
        }
//        print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "user_previlege_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Master Activity Found', "user_previlege_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Remove User Log
     * Created By : Rahul Chauhan
     * Date: 20-04-2020
     */
    public function removeUserLog() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $objId = isset($_POST['id']) ? $_POST['id'] : '';
        $status = isset($_POST['status']) ? $_POST['status'] : '';
        if ($status == 'false') {
            $st = 'inactive';
            $str = "Deactivated";
        } else {
            $st = 'active';
            $str = "Activated";
        }
        $old_data = $this->Ptscommon->getUserOldLogsRightsById($objId);
        $oldInsertArray = ["user_id" => $old_data['user_id'], "emp_code" => $old_data['emp_email'], "logs" => $old_data['activity_name'] . " : " . $old_data['type_id'], "room_code" => $old_data['room_code'], "module_type" => 'log', "status" => ucfirst($old_data['mstatus'])];
        $oldInsertArray = $this->Ptscommon->getNAUpdated($oldInsertArray);
        $InsertArray = ["user_id" => $old_data['user_id'], "emp_code" => $old_data['emp_email'], "logs" => $old_data['activity_name'] . " : " . $old_data['type_id'], "room_code" => $old_data['room_code'], "module_type" => 'log', "status" => ucfirst($st)];
        $InsertArray = $this->Ptscommon->getNAUpdated($InsertArray);
        $arr2 = array("status" => $st, "modified_by" => $this->session->userdata('user_id'), "modified_on" => date("Y-m-d H:i:s"));
        $this->db->where("id", $objId);
        $res = $this->db->update("pts_mst_user_mgmt", $arr2);
//        echo $this->db->last_query();exit;
        if (!empty($res)) {
            /* Start - Insert elog audit histry */
            $uniquefiled1 = "user_id:" . $old_data['user_id'];
            $uniquefiled2 = "module_type:" . 'log';
            $uniquefiled3 = "";
            $inserted_id = $objId;

            $auditParams = ['command_type' => 'update', 'activity_name' => 'User Access', 'type' => 'Roles and Responsibility', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_user_mgmt', 'primary_id' => $inserted_id, 'post_params' => $insertArray, 'master_previous_data' => $oldInsertArray];
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Log data ' . $str . ' Successfully.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please try again.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Employee Role List
     * Created By : Rahul Chauhan
     * Date: 23-04-2020
     */
    public function getEmployeRoleList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $res = $this->Ptscommon->getEmployeRoleList();
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Employee Data', "role_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please try again.', "role_data" => array());
        }
    }

    /* Get a list of Fixed Equipment For Line Log Report
     * @param string $method1
     * @param string $tokencheck
     */

    public function GetfixedequiplistForLineReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $param = array("is_active" => 1, "equipment_type" => "Fixed");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "fixed_euip_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Fixed Equipment Found', "fixed_euip_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Audit trail Report Data
     * Created By : Khushboo
     * Created Date:04-05-2020
     */
    public function auditTrailReportListNew() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['user_id'] = isset($_POST['user_id']) ? $_POST['user_id'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? $_POST['start_date'] : '';
        $post['end_date'] = isset($_POST['end_date']) ? $_POST['end_date'] : '';
        $post['limit'] = isset($_GET['page_limit']) ? $_GET['page_limit'] : 0;
        $post['page'] = isset($_GET['page_no']) ? $_GET['page_no'] : 0;

        $totalcount = $this->Ptscommon->newAuditTrailData($post, true);
        $res = $this->Ptscommon->newAuditTrailData($post, false);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res, "total_count" => $totalcount);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array(), "total_count" => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Role List
     * Created By :Rahul Chauhan
     * Date:30-04-2020
     */
    public function getApprovalActivityList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("is_active" => "1");
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_trn_workflowsteps', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "role_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Role Found', "role_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get previous Room Wise Batch and Product List 
     * Created By :Khushboo
     * Date:07-05-2020
     */
    public function getPreviousRoomBatchInProductData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $room_code = isset($_GET['room_code']) ? $_GET['room_code'] : '';
        $param = array("room_code" => $room_code);
        $res = $this->Ptscommon->previousRoomBatchAndProduct($param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "batch_product_list" => (array) $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Record Found', "batch_product_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Report Footer List
     * Created By :Rahul Chauhan
     * Date:12-05-2020
     */
    public function getFooterList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "active");
        $this->db->select("pts_mst_report_footer.*,pts_mst_report.report_name,mst_employee.emp_name");
        $this->db->from("pts_mst_report_footer");
        $this->db->join("pts_mst_report", "pts_mst_report.id=pts_mst_report_footer.report_id", 'left');
        $this->db->join("mst_employee", "pts_mst_report_footer.updated_by=mst_employee.id", "left");
        //$this->db->where("pts_mst_report_footer.status", "active");
        $res = $this->db->get()->result_array();
        foreach ($res as $key => $val) {
            $employee = $this->db->get_where("mst_employee", array("id" => $val['created_by']))->row_array();
            $res[$key]['created_name'] = $employee['emp_name'];
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "footer_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Footer Found', "footer_list" => array());
        }
    }

    /**
     *  Save Report Footer Master Data
     * Created By :Rahul Chauhan
     * Date:12-05-2020
     */
    public function saveFooter() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $report_id = isset($_POST['report_id']) ? $_POST['report_id'] : '';
        $footer_name = isset($_POST['footer_name']) ? $_POST['footer_name'] : '';
        $footer_id = isset($_POST['footer_id']) ? $_POST['footer_id'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $status = isset($_POST['status']) ? $_POST['status'] : '';
        if ($footer_id > 0) {
            $res = $this->db->update("pts_mst_report_footer", array("report_id" => $report_id, "footer_name" => $footer_name, "updated_on" => date('Y-m-d H:i:s'), "updated_by" => $this->session->userdata('user_id'), "status" => $status, "remark" => $remark), array("id" => $footer_id));
            if (!empty($res)) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "report_id:" . $report_id;
                $uniquefiled2 = "footer_name:" . $footer_name;
                $auditParams = array('command_type' => 'update', 'activity_name' => 'Report Footer Details', 'type' => 'report_header', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'table_name' => 'pts_mst_report_footer', 'primary_id' => $footer_id, 'post_params' => array("report_id" => $report_id, "footer_name" => $footer_name));
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Report Footer Updated Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Report Footer does not Updated');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_report_footer', array("report_id" => $report_id, "footer_name" => $footer_name));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Footer Already Exist');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
            $post = array("report_id" => $report_id, "footer_name" => $footer_name, "created_on" => date('Y-m-d H:i:s'), "created_by" => $this->session->userdata('user_id'), "updated_on" => date('Y-m-d H:i:s'), "updated_by" => $this->session->userdata('user_id'), "status" => $status, "remark" => $remark);
            $res = $this->Ptscommon->addToTable($db = 'default', 'pts_mst_report_footer', $post, $param = '');
            if (!empty($res)) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "report_id:" . $report_id;
                $uniquefiled2 = "footer_name:" . $footer_name;
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Report Footer Details', 'type' => 'report_header', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'table_name' => 'pts_mst_report_footer', 'primary_id' => $res, 'post_params' => $post);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Report Footer Added Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Report Footer does not Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
    }

    /**
     * Delete Report Footer Master
     * Created By :Rahul Chauhan
     * Date:12-05-2020
     */
    public function deleteFooter() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $action = isset($_POST['action']) ? $_POST['action'] : '';
        if ($id > 0 && $action == 'Deactive') {
            $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_report_footer', array("id" => $id, "status" => "default"));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => "it's default Report Header.Please set another default header first then remove it");
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $res = $this->db->update('pts_mst_report_footer', array("status" => "inactive"), array("id" => $id));
                if (!empty($res)) {

                    /* Start - Insert elog audit histry */
                    $activity = $this->Ptscommon->getActivityNameType('pts_mst_report_footer');
                    $auditParams = array('command_type' => 'delete', 'activity_name' => $activity['activity_name'], 'type' => $activity['type'], 'table_name' => 'pts_mst_report_footer', 'primary_id' => $id, 'post_params' => array('id' => $id, "status" => 'inactive'));
                    $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                    /* End -  Insert elog audit histry */

                    $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Report Footer Deleted Successfully');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Report Footer does not Deleted');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            }
        } elseif ($id > 0 && $action == 'Set Default') {
            $res = $this->db->update('pts_mst_report_footer', array("status" => "active"), array("status" => "default"));
            $res = $this->db->update('pts_mst_report_footer', array("status" => "default"), array("id" => $id));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Set Default Report Footer Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Report Footer does not Deleted');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Somethig went wrong.Please try again');
        }
    }

    /**
     * Get Block Detail
     * Created By :Rahul Chauhan
     * Date:14-05-2020
     */
    public function getBlockDetails() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $area_code = isset($_GET['area_code']) ? $_GET['area_code'] : '';
        $param = array("is_active" => "1", "area_code" => $area_code);
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_area', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "block_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Role Found', "block_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Last Approval Data of Activity
     * Created By :Rahul Chauhan
     * Date:22-05-2020
     */
    public function GetLastApproval($param) {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $res = $this->Ptscommon->GetLastApproval($param);
        if (!empty($res)) {
            return $res;
        } else {
            return array();
        }
    }

    /**
     * Get TypeA or TypeB Last Record
     * Created By : Bhupendra kumar
     * Date: 21-05-2020
     */
    public function GetLastTypeAorBrecord() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $query = "SELECT id,`activity_id`,`room_code`,from_unixtime(activity_stop) as stop_datetime FROM `pts_trn_user_activity_log` WHERE `room_code`='$roomcode' and activity_stop is not null and workflownextstep ='-1' And (`activity_id`='1' or `activity_id`='2') ORDER By id DESC limit 1";
        $res = $this->db->query($query)->row_array();
//        echo $this->db->last_query();exit;
        if (!empty($res)) {
            date_default_timezone_set('Asia/Kolkata');
            $todaydate = new DateTime();
            $todaydate = DATE_FORMAT($todaydate, 'Y-m-d');
            $start_date = new DateTime();
            $since_start = $start_date->diff(new DateTime($res['stop_datetime']));
            $days = $since_start->d;
            $hours = $since_start->h;
            $min = $since_start->i;
            $sec = $since_start->s;

            if ($days == '3' && $hours == '0' && $min == '0' && $sec == '0') {
                $query2 = "SELECT id FROM `pts_trn_env_cond_diff` WHERE `room_code`='$roomcode' and DATE_FORMAT(created_on,'%Y-%m-%d')='$todaydate'";
                $res2 = $this->db->query($query2)->row_array();
                if (!empty($res2)) {
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "res" => 'go');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "res" => 'Please set the Environment conditional/pressure before start Production/Sampling');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            } else if ($days <= '2' && $hours <= '59' && $min <= '59' && $sec <= '59') {
                $query2 = "SELECT id FROM `pts_trn_env_cond_diff` WHERE `room_code`='$roomcode' and DATE_FORMAT(created_on,'%Y-%m-%d')='$todaydate'";
                $res2 = $this->db->query($query2)->row_array();
                if (!empty($res2)) {
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "res" => 'go');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "res" => 'Please set the Environment conditional/pressure before start Production/Sampling');
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                }
            } else {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, "res" => 'Cleaning before production has been expired please do the cleaning again either Type A or Type B');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Product Found', "res" => "Please do the cleaning either Type A or Type B");
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Attach Log File
    //allow only pdf file 22sep2020
    public function attachLogFile() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value

        $insertArray = array(
            "room_code" => isset($_POST['roomcode']) ? $_POST['roomcode'] : '',
            "process_id" => isset($_POST['processid']) ? $_POST['processid'] : '',
            "activity_id" => isset($_POST['activity_id']) ? $_POST['activity_id'] : '',
            "act_id" => isset($_POST['act_id']) ? $_POST['act_id'] : '',
            "role_id" => isset($_POST['roleid']) ? $_POST['roleid'] : '',
            "uploaded_by_id" => isset($_POST['empid']) ? $_POST['empid'] : '',
            "remark" => isset($_POST['remark']) ? $_POST['remark'] : '',
            "uploaded_by" => isset($_POST['empname']) ? $_POST['empname'] : ''
        );


        $file_name = $_FILES['file']['name'][0];
        $file_ext = explode('.', $_FILES['file']['name'][0]);
        $file_size = $_FILES['file']['size'][0];
        $file_type = $_FILES['file']['type'][0];
        if ($file_type != "application/pdf") {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please Select Pdf File', "res" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            if (!file_exists('assets/files/' . $insertArray['act_id'])) {
                mkdir('assets/files/' . $insertArray['act_id'], 0777, true);
            }
            $target = "assets/files/" . $insertArray['act_id'] . "/";
            if (($file_size > 2097152)) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please Select file size less than 2MB', "res" => array());
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $file_tmp = $_FILES['file']['tmp_name'][0];
                $fileInfo = $this->db->get_where("pts_trn_log_file_details", array("room_code" => $insertArray['room_code'], "process_id" => $insertArray['process_id'], "activity_id" => $insertArray['activity_id'], "act_id" => $insertArray['act_id'], "file_name" => $file_name, "status" => "active"))->row_array();
                if (!empty($fileInfo)) {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not upload same file.First delete same file and upload it again.', "res" => array());
                    $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                } else {
                    if (move_uploaded_file($file_tmp, "assets/files/" . $insertArray['act_id'] . "/" . $file_name)) {
                        $insertArray['file_name'] = $file_name;
                        $res = $this->db->insert("pts_trn_log_file_details", $insertArray);
                        if ($res) {
                            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'File Uploaded Successfully', "res" => array());
                            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                        } else {
                            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please try again.', "res" => array());
                            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                        }
                    }
                }
            }
        }
//        if ($file_type != "application/pdf") {
//            //$responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please Select Pdf File', "res" => array());
//            //$this->Apifunction->response($this->Apifunction->json($responseresult), 200);
//        } else {
//            $file_tmp = $_FILES['file']['tmp_name'][0];
//            $fileInfo = $this->db->get_where("pts_trn_log_file_details", array("room_code" => $insertArray['room_code'], "process_id" => $insertArray['process_id'], "activity_id" => $insertArray['activity_id'], "act_id" => $insertArray['act_id'], "file_name" => $file_name, "status" => "active"))->row_array();
//            if (!empty($fileInfo)) {
//                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not upload same file.First delete same file and upload it again.', "res" => array());
//                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
//            } else {
//                if (move_uploaded_file($file_tmp, "assets/files/".$insertArray['act_id'] . "/" . $file_name)) {
//                    $insertArray['file_name'] = $file_name;
//                    $res = $this->db->insert("pts_trn_log_file_details", $insertArray);
//                    if ($res) {
//                        $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'File Uploaded Successfully', "res" => array());
//                        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
//                    } else {
//                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please try again.', "res" => array());
//                        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
//                    }
//                }
//            }
//        }
    }

    /**
     * Get All Attachment List of Log
     * Created By :Rahul Chauhan
     * Date:15-06-2020
     */
    public function GetAttachmentList($param) {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $res = $this->Ptscommon->GetAttachmentList($param);
        if (!empty($res)) {
            return $res;
        } else {
            return array();
        }
    }

    /**
     * Get Plant List
     * Created By :Rahul Chauhan
     * Date:08-06-2020
     */
    public function getPlantList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("is_active" => "1");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_plant', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "plant_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Plant Found', "plant_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get All Attachment List of Log
     * Created By :Rahul Chauhan
     * Date:15-06-2020
     */
    public function removeAttachment() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        $user_id = isset($_POST['user_id']) ? $_POST['user_id'] : '';
        $this->db->from('mst_employee');
        $this->db->where('id', $user_id);
        $userData = $this->db->get()->row_array();
        $empname = $userData['emp_name'];
        $act_id = isset($_POST['act_id']) ? $_POST['act_id'] : '';
        $file_name = isset($_POST['file_name']) ? $_POST['file_name'] : '';
        if ($id > 0) {
            date_default_timezone_set('Asia/Kolkata');
            $this->db->where("id", $id);
            $res = $this->db->update("pts_trn_log_file_details", array("status" => "inactive", "updated_by_user_name" => $empname, "updated_by" => $user_id, "updated_on" => date("Y-m-d H:i:s")));
            if ($res) {
                $path = "assets/files/" . $act_id . "/" . $file_name;
                unlink($path);
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Attachment Removed Successfully', "res" => array());
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please try again', "res" => array());
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please try again', "res" => array());
        }
    }

    /**
     * Get Active/Inactive User List
     * Created By :Rahul Chauhan
     * Date:08-06-2020
     */
    public function getUserList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $plant_code = isset($_POST['plant_code']) ? $_POST['plant_code'] : '';
        $block_code = isset($_POST['block_code']) ? $_POST['block_code'] : '';
        $from_date = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $to_date = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        $type = isset($_POST['type']) ? $_POST['type'] : '';
        if ($type == "active") {
            $status = 1;
        } else {
            $status = 0;
        }
        $param = array("status" => $status, "plant_code" => $plant_code, "block_code" => $block_code, "from_date" => $from_date, "to_date" => $to_date);
        $res = $this->Ptscommon->getUserList($param);
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $res[$key]['created_on'] = date('d-M-Y', strtotime($value['created_on']));
            }
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "user_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No User Found', "user_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    // End
    //Bhupendra's code as below:-

    /**
     * User login
     */
    public function UserAuth() {
        $headers = apache_request_headers(); // fetch header value
        $userid = isset($_POST['email']) ? $_POST['email'] : '';
        $pwd = isset($_POST['password']) ? $_POST['password'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $responseresult = "";
        $res = $this->Ptsadminmodel->userauth($userid, $pwd);
        if (!empty($res)) {
            $role_id = $res['role_id'];
            //Getting Next step of workflow of current Activity
            $wfres = $this->Ptsadminmodel->getworkflow_step($role_id, $actid, 'stop');
            if ($wfres->num_rows() > 0) {
                $workflowdata = $wfres->row_array();
                $status_id = $workflowdata['status_id'];
                $next_step = $workflowdata['next_step'];
                $roleinwf = $workflowdata['role_id'];
            }


            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => '', "Current Role : " => $role_id, "Next Role : " => $roleinwf, "Next step : " => $next_step);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $userexist = $this->Ptsadminmodel->userexist($userid);
            if (!empty($userexist)) {
                if ($userexist['is_blocked'] == 1) {
                    $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Your account has been blocked, please contact the administrator.', "userdata" => array());
                } else {
                    $attampt = $userexist['login_attampt'] + 1;
                    if ($attampt == 5) {
                        $params = array('login_attampt' => 0);
                        $userexist = $this->Ptsadminmodel->updateuser('mst_employee', $params, 'emp_email', $userid);
                    } else {
                        $params = array('login_attampt' => $attampt);
                        $userexist = $this->Ptsadminmodel->updateuser('mst_employee', $params, 'emp_email', $userid);
                    }
                    if ($attampt == 5) {
                        $params = array('is_blocked' => 1, 'login_attampt' => 0, 'block_datetime' => date('Y-m-d H:i:s'));
                        $userexist = $this->Ptsadminmodel->updateuser('mst_employee', $params, 'emp_email', $userid);
                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Your account has been blocked, please contact the administrator.', "userdata" => array());
                    } else {
                        $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Credential not match', "userdata" => array());
                    }
                }
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Credential not match', "userdata" => array());
            }
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /* This Function is used to get the coomon log records for report
     * Created By : Khushboo
     * Created Date : Date:17-06-2020
     */

    public function GetCommonLogReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $type = isset($_POST['type']) ? $_POST['type'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        $post['limit'] = isset($_GET['page_limit']) ? $_GET['page_limit'] : 0;
        $post['page'] = isset($_GET['page_no']) ? $_GET['page_no'] : 0;
        $res = $this->Ptscommon->GetCommonLogReport($type, $post);
        $totalcount = $this->Ptscommon->GetCommonLogReportCount($type, $post);

        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res, "total_count" => $totalcount);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array(), "total_count" => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * @desc common function to send mail to head/user
     * @param type $from
     * @param type $to
     * @param type $subject
     * @param type $message
     * @author Rahul Chauhan
     */
    public function commonEmailFunction($act_id) {
        $subject = "Activity Approval";
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $res = $this->Ptscommon->GetInprogressActivityListForEmail($act_id);
//        echo '<pre>'; print_r($res);exit;
        $success = array();
        $error = array();
        $i = 1;
        foreach ($res as $key => $value) {
//            echo '<pre>';print_r($value);exit;
            $email = false;
            $res[$key]['mst_act_id'] = $this->GetActivityUrl($value['processid'])['mst_act_id'];
            $res[$key]['next_approval'] = $this->getNextApprovalRoleForActivity($value['activity_id'], $value['workflownextstep']);
            $next = $this->getNextApprovalRoleForActivity($value['activity_id'], $value['workflownextstep']);
            $res[$key]['email'] = $this->getNextApprovalUserEmail($next['status_id']);
            $res[$key]['start_date'] = date('d-M-Y', strtotime($res[$key]['start_date']));
            $product_desc = $this->db->get_where("mst_product", array("product_code" => $value['product_code']))->row_array();
            if ($value['activity_stop'] == NULL && $value['workflowstatus'] > 0 && $value['next_step'] > 0 && $value['workflownextstep'] != 0) {
                $email = true;
            }
            if ($value['activity_stop'] != NULL && $value['workflowstatus'] > 0 && $value['next_step'] > 0 && $value['workflownextstep'] != 0) {
                $email = true;
            }
            $emaildata = $this->getNextApprovalUserEmail($next['role_id']);
//            echo '<pre>';print_r($emaildata);exit;
            $message = '<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
   <head>
      <meta charset="utf-8">
      <title>Approval Mailer</title>
      <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,300;1,400&display=swap" rel="stylesheet">
   </head>
   <body width="100%" style="margin: 0; padding: 30px 0 !important; background-color: #f1f1f1; font-family: Lato, sans-serif;">
      <center>
         <table width="600px" border="0" cellspacing="0" cellpadding="0" style="background-color: #fff; padding: 10px 20px; border-collapse: separate;
            border-spacing: 0px; border: 1px solid #ccc; border-radius: 10px;">
            <tr>
               <td style="font-size:18px; font-weight:bold; text-align:center;">eLogBook</td>
            </tr>
            <tr>
               <td style="height: 10px; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 1px; background-color: #efefef; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Dear Sir/Maam</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>
                  <table style="border-collapse:collapse; font-size:14px; border-color:#ccc;" border="1" cellpadding="5" cellspacing="0" width="100%">
                     <tr style="background-color:#4e73df; color:#fff;">
                        <th style="border:0px;">Doc Id</th>
                        <th style="border:0px;">Activity</th>
                        <th style="border:0px;">Batch No.</th>
                        <th style="border:0px;">Room Code</th>
                        <th style="border:0px;">Equipment code</th>
                        <th style="border:0px;">Start Date</th>
                        <th style="border:0px;">Start Time</th>
                        <th style="border:0px;">Started By/Done By</th>
                        <th style="border:0px;">Stop Date</th>
                        <th style="border:0px;">Stop Time</th>
                        <th style="border:0px;">Stop By</th>
                        <th style="border:0px;">Product Code</th>
                        <th style="border:0px;">Product Desc.</th>
                        <th style="border:0px;">Next Step (Pending Action)</th>
                     </tr>';
            $message .= '<tr>
    <td>' . $value['doc_id'] . '</td>
    <td>' . $value['home_activity'] . '</td>
    <td>' . $value['batch_no'] . '</td>
    <td>' . $value['room_code'] . '</td>
    <td>' . $value['equip_code'] . '</td>
    <td>' . $value['start_date'] . '</td>
    <td>' . $value['start_time'] . '</td>
    <td>' . $value['user_name'] . '</td>
    <td>' . $value['stop_date'] . '</td>
    <td>' . $value['stop_time'] . '</td>
    <td>' . $value['stop_by'] . '</td>
    <td>' . $value['product_code'] . '</td>
    <td>' . $product_desc['product_name'] . '</td>
    <td>' . $next['role_name'] . '</td>
  </tr>';
            $message .= '</table>
               </td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Regards</td>
            </tr>
            <tr>
               <td style="height: 10px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Admin</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
         </table>
      </center>
   </body>
</html>';
//            echo $message;exit;
//          echo '<pre>';          print_r($emaildata);exit;  
            foreach ($emaildata as $key1 => $value1) {
                $this->load->library('email', MAIL_CONFIG);
                $this->email->set_newline("\r\n");
                $this->email->from(MAIL_FROM); // change it to yours
                $this->email->to($value1['email2']); // change it to yours
                $this->email->subject($subject);
                $this->email->message($message);
                $this->email->set_mailtype(SMTP_MAIL_TYPE);
                if ($this->email->send()) {
                    $success[] = $value1['emp_name'];
                    //return true;
                } else {
                    $error[] = $value1['emp_name'];
                    //return false;
                }
            }
            $i++;
            if ($i == 10) {
                sleep(10);
                $i = 0;
                continue;
            }
        }
        return true;
    }

    public function getNextApprovalRoleForActivity($mst_act_id, $next_step) {
        $this->db->select("pts_trn_workflowsteps.*,mst_role.role_description");
        $this->db->from("pts_trn_workflowsteps");
        $this->db->join("mst_role", "mst_role.id=pts_trn_workflowsteps.status_id", 'left');
        $this->db->where("pts_trn_workflowsteps.activity_id", $mst_act_id);
        $this->db->where("mst_role.id", $next_step);

        $res = $this->db->get()->row_array();
        return $res;
    }

    public function getNextApprovalUserEmail($role_id) {
        $this->db->select("mst_employee.*");
        $this->db->from("mst_employee");
        $this->db->where("mst_employee.role_id", $role_id);
        $res = $this->db->get()->result_array();
        return $res;
    }

    public function sendPasswordReminderEmail() {
        $res = $this->db->get_where("mst_employee", array("role_id!=" => 6, "is_active" => 1, "is_blocked" => 0))->result_array();
        $this->load->library('email', MAIL_CONFIG);
        $subject = "Password Warning";
        $success = array();
        $error = array();
        foreach ($res as $key => $value) {
            if ($value['expire_on'] != "") {
                $end_date = strtotime(date('Y-m-d', strtotime($value['expire_on'])));
                $start_date = strtotime(date('Y-m-d'));
                $diff = ($end_date - $start_date) / 60 / 60 / 24;
                if ($diff == 4) {
                    $msg = '<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
   <head>
      <meta charset="utf-8">
      <title>Approval Mailer</title>
      <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,300;1,400&display=swap" rel="stylesheet">
   </head>
   <body width="100%" style="margin: 0; padding: 30px 0 !important; background-color: #f1f1f1; font-family: Lato, sans-serif;">
      <center>
         <table width="600px" border="0" cellspacing="0" cellpadding="0" style="background-color: #fff; padding: 10px 20px; border-collapse: separate;
            border-spacing: 0px; border: 1px solid #ccc; border-radius: 10px;">
            <tr>
               <td style="font-size:18px; font-weight:bold; text-align:center;">eLogBook</td>
            </tr>
            <tr>
               <td style="height: 10px; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 1px; background-color: #efefef; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Dear Sir/Maam</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Your password will be expire after 5 days.</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Regards</td>
            </tr>
            <tr>
               <td style="height: 10px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Admin</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
         </table>
      </center>
   </body>
</html>';
                    $this->email->set_newline("\r\n");
                    $this->email->from(MAIL_FROM); // change it to yours
                    $this->email->to($value['email2']); // change it to yours
                    $this->email->subject($subject);
                    $this->email->message($msg);
                    $this->email->set_mailtype(SMTP_MAIL_TYPE);
                    if ($this->email->send()) {
                        $success[] = $value['id'];
                        //return true;
                    } else {
                        $error[] = $value['id'];
                        //return false;
                    }
                }
                if ($diff == 0) {
                    $this->db->where("id", $value['id']);
                    $this->db->update("mst_employee", array("is_blocked" => 1));
                }
            }
        }
        $final_array = array("success" => $success, "error" => $error);
        $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Password Reminder Email has been sent successfuly', "email_data" => $final_array);
        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
    }

    /**
     * Server Ip Master Save
     * Created By :Rahul Chauhan
     * Date:24-06-2020
     */
    public function saveServer() {
//        echo $this->session->userdata('user_id');exit;
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $ip_address = isset($_POST['ip_address']) ? $_POST['ip_address'] : '';
        $ip_id = isset($_POST['ip_id']) ? $_POST['ip_id'] : '';
        if ($ip_id > 0) {
            $res = $this->db->update("pts_mst_server_detail", array("server_ip" => $ip_address, "modified_on" => date("Y-m-d H:i:s"), "modified_by" => $this->session->userdata('empname')), array("id" => $ip_id));
            if (!empty($res)) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "server_ip:" . $ip_address;
                $uniquefiled2 = "";
                $uniquefiled3 = "";
                $post = array("server_ip" => $ip_address, "modified_on" => date("Y-m-d H:i:s"), "modified_by" => $this->session->userdata('empname'));
                $auditParams = array('command_type' => 'update', 'activity_name' => 'Server Configuration Master', 'type' => 'manage_server', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_server_detail', 'primary_id' => $ip_id, 'post_params' => $post);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */


                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Server Data Updated Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Server Data does not Updated');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_server_detail', array("server_ip" => $ip_address, "status" => "active"));
            if (!empty($res)) {
                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'This Record is Already Exist');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
            $post = array("server_ip" => $ip_address, "created_by" => $this->session->userdata('empname'));
            $res = $this->Ptscommon->addToTable($db = 'default', 'pts_mst_server_detail', $post, $param = '');
            //print_r($res);exit;
            if (!empty($res)) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "server_ip:" . $ip_address;
                $uniquefiled2 = "";
                $uniquefiled3 = "";
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Server Configuration Master', 'type' => 'manage_server', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_server_detail', 'primary_id' => $res, 'post_params' => $post);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */


                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Record Added Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Record does not Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
    }

    /**
     * Get Server List
     * Created By :Rahul Chauhan
     * Date:24-06-2020
     */
    public function GetServerList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $param = array("status" => "active");
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_server_detail', $param, '', $page);
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = $this->Ptscommon->getRowsCount('default', 'pts_mst_server_detail', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "ip_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Ip Found', "ip_list" => [], 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Delete Stage Master
     * Created By :Rahul Chauhan
     * Date:13-03-2020
     */
    public function deleteServer() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $id = isset($_POST['id']) ? $_POST['id'] : '';
        if ($id > 0) {
            $res = $this->db->update('pts_mst_server_detail', array("status" => "inactive"), array("id" => $id));
            if (!empty($res)) {


                /* Start - Insert elog audit histry */
                $activity = $this->Ptscommon->getActivityNameType('pts_mst_server_detail');
                $auditParams = array('command_type' => 'delete', 'activity_name' => $activity['activity_name'], 'type' => $activity['type'], 'table_name' => 'pts_mst_server_detail', 'primary_id' => $id, 'post_params' => array('id' => $id, "status" => 'inactive'));
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Server Ip Deleted Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Server Ip does not Deleted');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Somethig went wrong.Please try again');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get User Status Data
     * Created By : Meghna Soni
     * Edited By: ****
     * Created Date:21-07-2020
     * Edit Date:***
     */
    public function getUserStatusReportData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post[''] = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $post['room_code'] = isset($_POST['room_code']) ? $_POST['room_code'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        // print_r($post);exit;
        $res = $this->Ptscommon->getToolingReportData($post);
        //echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*     * ********************** End ************************** */

    public function sendheadermail() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $this->db->select("pts_mst_report_header.*,pts_mst_activity.activity_name");
        $this->db->from("pts_mst_report_header");
        $this->db->join("pts_mst_activity", " pts_mst_activity.id=pts_mst_report_header.header_id", 'left');
        $res = $this->db->get()->result_array();
//        echo '<pre>';print_r($res);exit;
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                if ($value['retired_date'] != '' && $value['retired_date'] != null) {
                    $end_date = strtotime($value['retired_date']);
                    $start_date = strtotime(date('Y-m-d'));
                    $diff = ($end_date - $start_date) / 60 / 60 / 24;
                    if ($diff > -1) {
                        if ($value['alertondate'] >= $diff) {
//                            echo "<pre/>";print_r($value);
                            $usersid = explode(",", $value['alert_assigned']);
                            foreach ($usersid as $key => $val) {
//                                echo "<pre/>";print_r($val);
                                $userData = $this->db->get_where("mst_employee", array("email2" => $val, "is_active" => 1, "is_blocked" => 0))->row_array();
                                $nexverson = $this->db->get_where("pts_mst_report_header", array("id >" => $value['id'], "status" => 'active', "header_id" => $value['header_id']))->row_array();
//                                echo "<pre/>";print_r($userData);exit;
                                $this->sendHeaderReminderEmail($value, $userData['email2'], $nexverson['version_no']);
                            }
                        }
                    }
                }
            }
        }
    }

    public function sendHeaderReminderEmail($post, $email, $nextverson) {
//        echo $email;exit;
//        echo '<pre>';print_r($post);exit;
        //$this->send_email();
        $msg = '<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
   <head>
      <meta charset="utf-8">
      <title>Password</title>
      <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,300;1,400&display=swap" rel="stylesheet">
   </head>
   <body width="100%" style="margin: 0; padding: 30px 0 !important; background-color: #fff; font-family: Lato, sans-serif;">
      <left>
         <table width="600px" border="0" cellspacing="0" cellpadding="0" style="background-color: #fff; padding: 10px 20px; border-collapse: separate;
            border-spacing: 0px; border: 1px solid #ccc; border-radius: 10px;margin-left:40px">
            <tr>
               <td style="font-size:18px; font-weight:bold; text-align:center;">eLogBook</td>
            </tr>
            <tr>
               <td style="height: 10px; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 1px; background-color: #efefef; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Dear Sir/Maam</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Current version of log is going to retire soon . Please  expedite and complete work in Progress else new version cannot be made effective.</td>
            </tr>

            <tr>
               <td>            <table border="1">
            <tr><td>Form No.</td><td>Version No.</td><td>Document No.</td><td>Effective Date</td><td>Retire Date</td></tr>
            <tr><td>' . $post['form_no'] . '</td><td>' . $post['user_version_no'] . '</td><td>' . $post['document_no'] . '</td><td>' . date('d-M-Y', strtotime($post['effective_date'])) . '</td><td>' . date('d-M-Y', strtotime($post['retired_date'])) . '</td></tr><br/>
            </table><br/></td>
            </tr>
            <tr>
               <td>Regards</td>
            </tr>
            <tr>
               <td style="height: 10px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Admin</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
         </table>
      </center>
   </body>
</html>';
//echo $email;exit;
        $subject = "Header retire soon";
        $from = 'elogadmin@sunpharma.com';
        $this->load->library('email', MAIL_CONFIG);
        $this->email->set_newline("\r\n");
        $this->email->from(MAIL_FROM); // change it to yours
        $this->email->to($email); // change it to yours
        //$this->email->to('Poonam.Thakur@smhs.motherson.com'); // change it to yours
        $this->email->subject($subject);
        $this->email->message($msg);
        $this->email->set_mailtype(SMTP_MAIL_TYPE);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }

    //Rest Password
    public function resetPassword() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $objId = isset($_POST['user_id']) ? $_POST['user_id'] : '';
        $password = isset($_POST['password']) ? $_POST['password'] : '';
        $password_used = $this->Ptscommon->checkUserPasswordUsed($objId, $password);
        if ($password_used) {
            $responseresult = array(STATUS => TRUE, "result" => False, MESSAGE => 'Password Can Not Be The Same As Last 5 Passwords');
            echo json_encode($responseresult);
            exit;
        }
        $expire_on = date('Y-m-d H:i:s', strtotime("+60 days"));
        $arr2 = array("emp_password" => $password, "modified_by" => $this->session->userdata('empname'), "modified_on" => date("Y-m-d H:i:s"), "emp_remark" => "Password Reset", "is_first" => '0', "expire_on" => $expire_on);
        $this->db->where("id", $objId);
        $res = $this->db->update("mst_employee", $arr2);
        $this->db->insert("mst_employee_password_history", array("user_id" => $objId, "password" => $password, "created_by" => $password));
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Password Reset Successfully.');
            echo json_encode($responseresult);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please try again.');
            echo json_encode($responseresult);
        }
    }

    /**
     * Master trail Report Data
     * Created By : Nitin
     * Created Date:23-07-2020
     */
    public function MasterlReportList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $post['block_code'] = isset($_POST['block_code']) ? $_POST['block_code'] : '';
        $post['form_name'] = isset($_POST['form_name']) ? $_POST['form_name'] : '';

        $totalcount = $this->Ptscommon->newAuditTrailData($post, true);
        $res = $this->Ptscommon->newAuditTrailData($post, false);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res, "total_count" => $totalcount);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array(), "total_count" => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function roomConfigurationMasterDataHtml($operation, $module = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $param = ["status" => 'active'];
        if ($module == 'master') {
            $param = [];
        }
        $excel_array = [];
        $res = $this->Ptscommon->getMultipleRowsNew('default', 'pts_mst_sys_id', $param, 'created_on');
        foreach ($res as $key => $r) {
            $filters = '';
            $modified_on = $res[$key]['modified_on'] = $this->getModifiedDate($r);
            $modified_by = $res[$key]['modified_by'] = isset($r['modified_by']) ? $r['modified_by'] : $r['created_by'];

            $filter = $this->Ptscommon->getMultipleRows('default', 'pts_mst_filter', array("room_code" => $r['room_code'], "status" => "on"));
            if (!empty($filter)) {
                $filterData = array();
                foreach ($filter as $key1 => $value1) {
                    $filterData[] = $value1['filter_name'];
                }
                $filters = $res[$key]['filter'] = implode(",", $filterData);
            } else {
                $res[$key]['filter'] = "";
            }
            $row = [$r['room_code'], $r['room_name'], $r['room_type'], $r['area_id'], $r['device_id'], $r['logger_id'], $r['gauge_id'], $filters, $modified_by, $modified_on, ucwords($r['status'])];
            $excel_array[] = $row;
        }
        $data['res'] = $res;
        if ($operation == 'excel') {
            $fields = ["Room Code", "Room Name", "Room Type", "Room Area", "Device IP", "Data Logger ID", "Magnehlic Gauge ID", "Filter Desc", "Last Modified By", "Last Modified On", "Status"];
            array_unshift($excel_array, $fields);
            $this->generateExcelFile($excel_array, $master_form_url);
            exit();
        }
        return $this->load->view('template/master_report/room_master_report', $data, true);
    }

    public function assignedRoomMasterDataHtml($operation, $module = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $room_activity_list = [];
        $headers = apache_request_headers();
        $param = array("status" => 'active');
        if ($module == 'master') {
            $param = [];
        }

        $this->db->select("pts_trn_room_log_activity.*,pts_mst_sys_id.room_code,pts_mst_sys_id.room_name,pts_mst_sys_id.created_by,pts_mst_sys_id.modified_by,pts_mst_sys_id.created_on,pts_mst_sys_id.modified_on");
        $this->db->from("pts_trn_room_log_activity");
        $this->db->join("pts_mst_sys_id", " pts_trn_room_log_activity.room_id=pts_mst_sys_id.room_id", 'left');
        $this->db->group_by('pts_trn_room_log_activity.room_id');
        $this->db->order_by('pts_trn_room_log_activity.created_date', 'DESC');
        $res = $this->db->get()->result_array();
        if (!empty($res)) {
            foreach ($res as $r) {
                $param = ["room_id" => $r['room_id']];
                $room_activity_lists = $this->Ptscommon->getMultipleRows('default', 'pts_trn_room_log_activity', $param);
                $logs = '';
                if (!empty($room_activity_lists)) {
                    foreach ($room_activity_lists as $room) {
                        $logs .= $room['activity_name'] . ',';
                    }
                    $logs = trim($logs, ",");
                }
                $room_list['room_code'] = $r['room_name'] . ' (' . $r['room_code'] . ')';
                $room_list['logs'] = $logs;
                $room_list['modified_by'] = isset($r['modified_by']) ? $r['modified_by'] : $r['created_by'];
                $room_list['modified_on'] = $this->getModifiedDate($r); //isset($r['modified_on'])?date('d-M-Y H:i:s',strtotime($r['modified_on'])):!empty($r['created_on'])?date('d-M-Y H:i:s',strtotime($r['created_on'])):'';
                $room_list['status'] = isset($r['status']) ? ucwords($r['status']) : '';
                $room_activity_list [] = $room_list;
            }
            if ($operation == 'excel') {
                $fields = ["Room Name", "Log Name", "Last Modified By", "Last Modified On", "Status"];
                array_unshift($room_activity_list, $fields);
                $this->generateExcelFile($room_activity_list, $master_form_url);
                exit();
            }
            $data['res'] = $room_activity_list;
            return $this->load->view('template/master_report/room_activity_master_report', $data, true);
        }
    }

    public function workAssignAsperRoleBaseMasterDataHtml($operation, $module = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $workAssignMaster = [];
        $roleList = $this->Ptscommon->getMultipleRows('default', 'mst_role', []);
        $res = $this->Ptscommon->getMultipleRowswithgroupby('default', 'pts_mst_role_workassign_to_mult_roles', [], "roleid", '', 'created_on');
        if (!empty($res)) {
            foreach ($res as $r) {
                $param = ["roleid" => $r['roleid']];
                $roles = $this->Ptscommon->getMultipleRows('default', 'pts_mst_role_workassign_to_mult_roles', $param);
                $logs = [];
                if (!empty($roles)) {
                    $role = array_search($roles[0]['roleid'], array_column($roleList, 'id', 'role_description'));
                    foreach ($roles as $r) {
                        $key = array_search($r['workassign_to_roleid'], array_column($roleList, 'id', 'role_description'));
                        $logs [] = $key;
                    }
                    $logs = implode(',', $logs);
                }

                $row['role'] = $role;
                $row['logs'] = $logs;
                $row['modified_by'] = isset($r['modified_by']) ? $r['modified_by'] : $r['created_by'];
                $row['modified_on'] = $this->getModifiedDate($r);
                $row['status'] = (isset($r['isactive']) && $r['isactive'] == '1') ? 'Active' : 'Inactive';
                $workAssignMaster [] = $row;
            }
            $data['res'] = $workAssignMaster;
            if ($operation == 'excel') {
                $fields = ["Role Name", "Delegated Role Name", "Last Modified By", "Last Modified On", "Status"];
                array_unshift($workAssignMaster, $fields);
                $this->generateExcelFile($workAssignMaster, $master_form_url);
                exit();
            }
            return $this->load->view('template/master_report/sf_work_assign_asper_rolebase_master_report', $data, true);
        }
    }

    public function balanceMasterDataHtml($operation, $module = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $param = ["status" => 'active'];
        if ($module == 'master') {
            $param = [];
        }
        $excel_array = [];
        $res = $this->Ptscommon->getMultipleRowsNew('default', 'pts_mst_balance_calibration', $param, 'created_on');
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $row = [];
                $row[] = $res[$key]['room_code'] = $value['room_code'];
                $row[] = $res[$key]['balance_no'] = $value['balance_no'];
                $capacity = '';
                $uom = '';
                if (isset($value['capacity']) && !empty($value['capacity'])) {
                    $capacity = substr($value['capacity'], 0, -2);
                    $uom = substr($value['capacity'], -2);
                }
                $row[] = $res[$key]['capacity'] = $capacity;
                $row[] = $res[$key]['uom'] = $uom;
                $row[] = $res[$key]['least_count'] = $value['least_count'];
                $row[] = $res[$key]['modified_by'] = isset($value['modified_by']) ? $value['modified_by'] : $value['created_by'];
                $row[] = $res[$key]['modified_on'] = $this->getModifiedDate($value);
                $row[] = $res[$key]['status'] = ucwords($value['status']);
                $excel_array[] = $row;
            }
        }
        if ($operation == 'excel') {
            $fields = ["Room Code", "Balance No", "Capacity", "Measurement Unit", "Least Count", "Last Modified By", "Last Modified On", "Status"];
            array_unshift($excel_array, $fields);
            $this->generateExcelFile($excel_array, $master_form_url);
            exit();
        }
        $data['res'] = $res;

        return $this->load->view('template/master_report/balance_master_report', $data, true);
    }

    public function balanceCalibrationFrequencyMasterDataHtml($operation, $module = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $param = ["status" => 'active'];
        if ($module == 'master') {
            $param = [];
        }
        $excel_array = [];
        $frequency_list = $this->Ptscommon->getMultipleRows('default', 'pts_mst_frequency', ["status" => "active"]);
        $groupparams = ["balance_no", "frequency_id"];
        $res = $this->Ptscommon->getMultipleRowswithgroupby('default', 'pts_mst_balance_frequency_data', $param, $groupparams, '', 'created_on');
        //echo '<pre>';print_r($frequency_list);print_r($res);die;
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $row = [];
                $row[] = $res[$key]['balance_no'] = $value['balance_no'];
                $row[] = $res[$key]['frequency_name'] = array_search($value['frequency_id'], array_column($frequency_list, 'id', 'frequency_name'));
                $row[] = $res[$key]['modified_by'] = isset($value['modified_by']) ? $value['modified_by'] : $value['created_by'];
                $row[] = $res[$key]['modified_on'] = $this->getModifiedDate($value);
                $row[] = $res[$key]['status'] = ucwords($value['status']);
                $excel_array[] = $row;
            }
        }
        $data['res'] = $res;
        if ($operation == 'excel') {
            $fields = ["Balance No", "Frequency", "Last Modified By", "Last Modified On", "Status"];
            array_unshift($excel_array, $fields);
            $this->generateExcelFile($excel_array, $master_form_url);
            exit();
        }
        return $this->load->view('template/master_report/balancecalibration_frequency_master_report', $data, true);
    }

    public function tabletToolingMasterDataHtml($operation, $module = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $param = []; //["status" => 'active'];
        $res = $this->Ptscommon->getMultipleRowsNew('default', 'pts_mst_tablet_tooling', $param, 'created_on');
        $excel_array = [];
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $row = [];
                $row[] = $res[$key]['product_code'] = $value['product_code'];
                $row[] = $res[$key]['supplier'] = $value['supplier'];
                $row[] = $res[$key]['upper_punch_qty'] = $value['upper_punch'] . '/' . $res[$key]['upper_embossing'];
                $row[] = $res[$key]['lower_punch_qty'] = $value['lower_punch'] . '/' . $res[$key]['lower_embossing'];
                $row[] = $res[$key]['year'] = $value['year'];
                $row[] = $res[$key]['no_of_subset'] = $value['no_of_subset'];
                $row[] = $res[$key]['shape'] = $value['shape'];
                $row[] = $res[$key]['machine'] = $value['machine'];
                $row[] = $res[$key]['die_quantity'] = $value['die_quantity'];
                $row[] = $res[$key]['modified_by'] = isset($value['modified_by']) ? $value['modified_by'] : $value['created_by'];
                $row[] = $res[$key]['modified_on'] = $this->getModifiedDate($value);
                $row[] = $res[$key]['status'] = ucfirst($value['status']);
                $excel_array[] = $row;
            }
        }
        $data['res'] = $res;
        if ($operation == 'excel') {
            $fields = ["Product Code", "Supplier", "Upper Punch-Qty./Embossing", "Lower Punch-Qty./Embossing", "Year", "Punch Set", "Shape", "Machine", "Die- Quantity", "Last Modified By", "Last Modified On", "Status"];
            array_unshift($excel_array, $fields);
            $this->generateExcelFile($excel_array, $master_form_url);
            exit();
        }
        return $this->load->view('template/master_report/table_tooling_master_report', $data, true);
    }

    public function lineLogMasterDataHtml($operation, $module = null, $room_code = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $param = [];
        if ($room_code != '') {
            $param = ["status" => "active", "room_code" => $room_code, "is_inused" => 0];
        }
        $res = $this->Ptscommon->getMultipleRowsNew('default', 'pts_mst_line_log', $param, 'created_on');
        $final = [];
        $excel_array = [];
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $value['modified_by'] = isset($value['modified_by']) ? $value['modified_by'] : $value['created_by'];
                $value['modified_on'] = $this->getModifiedDate($value);
                $value['ischecked'] = false;
                $final[$value['line_log_name']][] = $value;
            }
            $newArray = [];

            foreach ($final as $key => $value) {
                $tempArray = $equipArray = [];
                foreach ($value as $key1 => $value1) {
                    $equipArray[] = $value1['equipment_code'];
                    if ($key == $value1['line_log_name']) {
                        $tempArray = $value1;
                    }
                }
                $tempArray['equipment_code'] = implode(",", $equipArray);
                $newArray[] = $tempArray;
            }
            $data['res'] = $newArray;
            if ($operation == 'excel') {
                $excel_array[] = ["Line Code", "Line Name", "Equipment Code", "Block Code", "Area Code", "Room Code", "Last modified By", "Last modified On", "Status"];
                foreach ($newArray as $r) {
                    $row = [];
                    $row[] = $r['line_log_code'];
                    $row[] = $r['line_log_name'];
                    $row[] = $r['equipment_code'];
                    $row[] = $r['block_code'];
                    $row[] = $r['area_code'];
                    $row[] = $r['room_code'];
                    $row[] = $r['modified_by'];
                    $row[] = $r['modified_on'];
                    $row[] = ucwords($r['status']);
                    $excel_array[] = $row;
                }
                $this->generateExcelFile($excel_array, $master_form_url);
                exit();
            }

            return $this->load->view('template/master_report/line_log_master_report', $data, true);
        }
    }

    public function standardWeightMasterDataHtml($operation, $module = null, $master_form_url) {
        $excel_array = [];
        $this->load->model('Ptscommon');
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_id_standard_weight', [], 'created_on');
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $row = [];
                $row[] = $res[$key]['weight'] = $value['weight'];
                $row[] = $res[$key]['measurement_unit'] = $value['measurement_unit'];
                $row[] = $res[$key]['id_no_statndard_weight'] = $value['id_no_statndard_weight'];
                $row[] = $res[$key]['status'] = ucwords($value['status']);
                $row[] = $res[$key]['modified_by'] = (isset($value['modified_by']) && !empty($value['modified_by'])) ? $value['modified_by'] : $value['created_by'];
                $row[] = $res[$key]['modified_on'] = $this->getModifiedDate($value);
                $excel_array[] = $row;
            }
        }
        if ($operation == 'excel') {
            $fields = ["Weight", "Measurement Unit", "ID of standard weight", "Status","Last Modified By","Last Modified On"];
            array_unshift($excel_array, $fields);
            $this->generateExcelFile($excel_array, $master_form_url);
            exit();
        }
        $data['res'] = $res;
        return $this->load->view('template/master_report/standard_weighting_master_report', $data, true);
    }

    public function workflowMasterDataHtml($operation, $module = null, $master_form_url) {
        if ($this->session->userdata('empemail')) {
            $this->load->model('Ptsadminmodel');
            $row = [];
            $wf = $this->Ptsadminmodel->getstartworkflow();
            $wf = $wf->result_array();
            if (!empty($wf) && count($wf) > 0) {
                $i = 0;
                foreach ($wf as $key => $v) {
                    $row[$key]['sn'] = ++$i;
                    $row[$key]['activity_name'] = $v['activity_name'];
                    $row[$key]['workflowtype'] = $v['workflowtype'];
                    $row[$key]['created_by'] = $v['created_by'];
                    $row[$key]['created_on'] = date('d-M-Y H:i:s', strtotime($v['created_on']));
                }
            }
            if ($operation == 'excel') {
                $head = ["S.No.", "Activity", "Type", "Created By", "Created On"];
                array_unshift($row, $head);
                $this->generateExcelFile($row, $master_form_url);
                exit();
            }
            return $this->load->view("template/master_report/sf_workflow_master_report", ['data' => $row], true);
        } else {
            return redirect(base_url() . 'User/logout');
        }
    }

    public function getModifiedDate($array) {
        $modified_on = '';
        if (isset($array['modified_on']) && !empty($array['modified_on']) && $array['modified_on'] != '0000-00-00 00:00:00') {
            return $mofified_on = date('d-M-Y H:i:s', strtotime($array['modified_on']));
        } else if (isset($array['created_on']) && !empty($array['created_on']) && $array['created_on'] != '0000-00-00 00:00:00') {
            return $mofified_on = date('d-M-Y H:i:s', strtotime($array['created_on']));
        }
        return $modified_on;
    }

    public function getInstrumentMasterDataHtml($operation, $module = null, $room_code = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $res = $this->Ptscommon->getMultipleRowsNew('default', 'pts_mst_instrument', [], 'created_on');
        $excel_array = [];
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $row = [];
                $row[] = $res[$key]['equipment_code'] = $value['equipment_code'];
                $row[] = $res[$key]['equipment_name'] = $value['equipment_name'];
                $row[] = $res[$key]['equipment_type'] = $value['equipment_type'];
                $row[] = $res[$key]['block_code'] = $value['block_code'];
                $row[] = $res[$key]['modified_by'] = $value['modified_by'];
                $row[] = $res[$key]['modified_on'] = $this->getModifiedDate($value);
                $row[] = $res[$key]['status'] = ucwords($value['status']);
                $excel_array[] = $row;
            }
        }
        $data['res'] = $res;
        if ($operation == 'excel') {
            $head = ["Instrument Code", "Instrument Name", "Instrument Type", "Block Code", "Last Modified By", "Last Modified On", "Status"];
            array_unshift($excel_array, $head);
            $this->generateExcelFile($excel_array, $master_form_url);
            exit();
        }

        return $this->load->view('template/master_report/instrument_master_report', $data, true);
    }

    public function stageMasterDataHtml($operation, $module = null, $room_code = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $row = [];
        $res = $this->Ptscommon->getMultipleRows2('default', 'pts_mst_stages', []);
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $row[$key]['log'] = $value['log'];
                $row[$key]['field_name'] = $value['field_name'];
                $row[$key]['field_value'] = $value['field_value'];
                $row[$key]['modified_by'] = isset($value['modified_by']) ? $value['modified_by'] : $value['created_by'];
                $row[$key]['modified_on'] = $this->getModifiedDate($value);
                $row[$key]['status'] = ucwords($value['status']);
            }
        }
        $data['res'] = $row;
        if ($operation == 'excel') {
            $head = ["Log Name", "Input Field", "Field Value", "Last Modified By", "Last Modified On", "Status"];
            array_unshift($row, $head);
            $this->generateExcelFile($row, $master_form_url);
            exit();
        }
        return $this->load->view('template/master_report/stage_master_report', $data, true);
    }

    public function reportHeaderMasterDataHtml($operation, $module = null, $room_code = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $param = ["status" => "active"];
        $this->db->select("pts_mst_report_header.*,pts_mst_activity.activity_name");
        $this->db->from("pts_mst_report_header");
        $this->db->join("pts_mst_activity", " pts_mst_activity.id=pts_mst_report_header.header_id", 'left');
        $this->db->order_by('created_on', 'DESC');
        $res = $this->db->get()->result_array();
        $row = [];
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $row[$key]['activity_name'] = $value['activity_name'];
                $row[$key]['form_no'] = $value['form_no'];
                $row[$key]['version_no'] = $value['version_no'];
                $row[$key]['effective_date'] = (isset($value['effective_date']) && !empty($value['effective_date'])) ? date('d-M-Y', strtotime($value['effective_date'])) : '';
                $row[$key]['retired_date'] = (isset($value['retired_date']) && !empty($value['retired_date'])) ? date('d-M-Y', strtotime($value['retired_date'])) : '';
                $row[$key]['document_no'] = $value['document_no'];
                $row[$key]['modified_by'] = isset($value['modified_by']) ? $value['modified_by'] : $value['created_by'];
                $row[$key]['modified_on'] = $this->getModifiedDate($value);
                $row[$key]['status'] = ucwords($value['status']);
            }
        }
        $data['res'] = $row; //echo '<pre>';print_r($res);die;
        if ($operation == 'excel') {
            $head = ["Header Name", "Form No.", "Version No.", "Effective Date", "Retired Date", "Document No.", "Last Modified By", "Last Modified On", "Status"];
            array_unshift($row, $head);
            $this->generateExcelFile($row, $master_form_url);
            exit();
        }
        return $this->load->view('template/master_report/header_master_report', $data, true);
    }

    public function manageBlockMasterDataHtml($operation, $module = null, $room_code = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $res = $this->Ptscommon->getMultipleRowsNew('default', 'mst_block', [], 'created_on');
        $row = [];
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $row[$key]['block_code'] = $value['block_code'];
                $row[$key]['block_name'] = $value['block_name'];
                $row[$key]['number_of_areas'] = $value['number_of_areas'];
                $row[$key]['plant_code'] = $value['plant_code'];
                $row[$key]['modified_by'] = isset($value['modified_by']) ? $value['modified_by'] : $value['created_by'];
                $row[$key]['modified_on'] = $this->getModifiedDate($value);
                $row[$key]['is_active'] = (isset($value['is_active']) && $value['is_active'] == '1') ? 'Active' : 'Inactive';
            }
        }
        $data['res'] = $row;
        if ($operation == 'excel') {
            $head = ["Block Code", "Block Name", "No. Of Sub Blocks", "Plant Code", "Last Modified By", "Last Modified On", "Status"];
            array_unshift($row, $head);
            $this->generateExcelFile($row, $master_form_url);
            exit();
        }
        return $this->load->view('template/master_report/block_master_report', $data, true);
    }

    public function subBlockMasterDataHtml($operation, $module = null, $block_code = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $param = ["is_active" => "1"];
        if ($module == 'master') {
            $param = [];
        }
        if ($block_code != "") {
            $param = ["is_active" => "1", "block_code" => $block_code];
        }
        $row = [];
        $res = $this->Ptscommon->getMultipleRowsNew('default', 'mst_department', $param, 'created_on');
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $row[$key]['department_code'] = $value['department_code'];
                $row[$key]['department_name'] = $value['department_name'];
                $row[$key]['block_code'] = $value['block_code'];
                $row[$key]['modified_by'] = isset($value['modified_by']) ? $value['modified_by'] : $value['created_by'];
                $row[$key]['modified_on'] = $this->getModifiedDate($value);
                $row[$key]['is_active'] = (isset($value['is_active']) && $value['is_active'] == '1') ? 'Active' : 'Inactive';
            }
        }
        $data['res'] = $row;
        if ($operation == 'excel') {
            $head = ["Sub Block Code", "Sub Block Name", "Block Code", "Last Modified By", "Last Modified On", "Status"];
            array_unshift($row, $head);
            $this->generateExcelFile($row, $master_form_url);
            exit();
        }
        return $this->load->view('template/master_report/sub_block_master_report', $data, true);
    }

    public function areaMasterDataHtml($operation, $module = null, $block_code = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $param = ["is_active" => "1"];
        if ($module == 'master') {
            $param = [];
        }
        if ($block_code != "") {
            $param = ["is_active" => "1", "block_code" => $block_code];
        }
        $row = [];
        $res = $this->Ptscommon->getMultipleRowsNew('default', 'mst_area', $param, 'created_on');
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $row[$key]['area_code'] = $value['area_code'];
                $row[$key]['area_name'] = $value['area_name'];
                $row[$key]['number_of_rooms'] = $value['number_of_rooms'];
                $row[$key]['block_code'] = $value['block_code'];
                $row[$key]['department_code'] = $value['department_code'];
                $row[$key]['modified_by'] = isset($value['modified_by']) ? $value['modified_by'] : $value['created_by'];
                $row[$key]['modified_on'] = $this->getModifiedDate($value);
                $row[$key]['is_active'] = (isset($value['is_active']) && $value['is_active'] == '1') ? 'Active' : 'Inactive';
            }
        }
        $data['res'] = $row;
        if ($operation == 'excel') {
            $head = ["Area Code", "Area Name", "No. Of Rooms", "Block Code", "Sub Block Code", "Last Modified By", "Last Modified On", "Status"];
            array_unshift($row, $head);
            $this->generateExcelFile($row, $master_form_url);
            exit();
        }
        return $this->load->view('template/master_report/area_master_report', $data, true);
    }

    public function productMasterDataHtml($operation, $module = null, $block_code = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $param = ["is_active" => "1"];
        if ($module == 'master') {
            $param = [];
        }
        if ($block_code != "") {
            $param = ["is_active" => "1", "block_code" => $block_code];
        }
        $row = [];
        $res = $this->Ptscommon->getMultipleRowsNew('default', 'mst_product', $param, 'created_on');
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $row[$key]['product_code'] = $value['product_code'];
                $row[$key]['product_name'] = $value['product_name'];
                $row[$key]['product_market'] = $value['product_market'];
                $row[$key]['product_remark'] = $value['product_remark'];
                $row[$key]['modified_by'] = isset($value['modified_by']) ? $value['modified_by'] : $value['created_by'];
                $row[$key]['modified_on'] = $this->getModifiedDate($value);
                $row[$key]['is_active'] = (isset($value['is_active']) && $value['is_active'] == '1') ? 'Active' : 'Inactive';
            }
        }
        $data['res'] = $row;
        if ($operation == 'excel') {
            $head = ["Product Code", "Product Name", "Market", "Remark", "Created By", "Created On", "Status"];
            array_unshift($row, $head);
            $this->generateExcelFile($row, $master_form_url);
            exit();
        }
        return $this->load->view('template/master_report/product_master_report', $data, true);
    }

    public function employeeMasterDataHtml($operation, $module = null, $block_code = null, $master_form_url) {
        if ($this->session->userdata('empemail')) {
            $empdtl = $this->Adminmodel->get_dtl1("mst_employee");
            $row = [];
            $empdtl = $empdtl->result_array();
            if (!empty($empdtl) && count($empdtl) > 0) {
                $i = 0;
                foreach ($empdtl as $key => $v) {
                    $row[$key]['sn'] = ++$i;
                    $row[$key]['emp_code'] = $v['emp_code'];
                    $row[$key]['emp_name'] = $v['emp_name'];
                    $row[$key]['emp_email'] = $v['emp_email'];
                    $row[$key]['emp_contact'] = $v['emp_contact'];
                    $rolename = $this->db->get_where("mst_role", array("id" => $v['role_id']))->row_array();
                    if (!empty($rolename) && isset($rolename['role_description'])) {
                        $rolename = $rolename['role_description'];
                    }
                    $row[$key]['role_name'] = $rolename;
                    $row[$key]['emp_code'] = $v['emp_code'];
                    $row[$key]['block_code'] = $v['block_code'];
                    $row[$key]['created_by'] = $v['created_by'];
                    $row[$key]['created_on'] = date('d-M-Y H:i:s', strtotime($v['created_on']));
                }
            }
            if ($operation == 'excel') {
                $head = ["S.No.", "Emp. Code", "Emp. Name", "Emp. Id", "Contact", "Role", "Block", "Created By", "Created On"];
                array_unshift($row, $head);
                $this->generateExcelFile($row, $master_form_url);
                exit();
            }
            return $this->load->view("template/master_report/employee_master_report", ['data' => $row], true);
        } else {
            return redirect(base_url() . 'User/logout');
        }
    }

    public function roomMasterDataHtml($operation, $module = null, $block_code = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $param = ["is_active" => "1"];
        if ($module == 'master') {
            $param = [];
        }
        $row = [];
        $res = $this->Ptscommon->getMultipleRowsNew('default', 'mst_room', $param, 'created_on');
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $row[$key]['room_code'] = $value['room_code'];
                $row[$key]['room_name'] = $value['room_name'];
                $row[$key]['area_code'] = $value['area_code'];
                $row[$key]['modified_by'] = isset($value['modified_by']) ? $value['modified_by'] : $value['created_by'];
                $row[$key]['modified_on'] = $this->getModifiedDate($value);
                $row[$key]['is_active'] = (isset($value['is_active']) && $value['is_active'] == '1') ? 'Active' : 'Inactive';
            }
        }
        $data['res'] = $row;
        if ($operation == 'excel') {
            $head = ["Room Code", "Room Name", "Area Code", "Created By", "Created On", "Status"];
            array_unshift($row, $head);
            $this->generateExcelFile($row, $master_form_url);
            exit();
        }
        return $this->load->view('template/master_report/room_list_master_report', $data, true);
    }

    public function plantMasterDataHtml($operation, $module = null, $block_code = null, $master_form_url) {
        $this->load->model('Ptscommon');
        $param = ["is_active" => "1"];
        if ($module == 'master') {
            $param = [];
        }
        $row = [];
        $res = $this->Ptscommon->getMultipleRowsNew('default', 'mst_plant', $param, 'created_on');
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $row[$key]['plant_code'] = $value['plant_code'];
                $row[$key]['plant_name'] = $value['plant_name'];
                $row[$key]['number_of_blocks'] = $value['number_of_blocks'];
                $row[$key]['modified_by'] = isset($value['modified_by']) ? $value['modified_by'] : $value['created_by'];
                $row[$key]['modified_on'] = $this->getModifiedDate($value);
                $row[$key]['is_active'] = (isset($value['is_active']) && $value['is_active'] == '1') ? 'Active' : 'Inactive';
            }
        }
        $data['res'] = $row;
        //echo '<pre>';print_r($row);die;
        if ($operation == 'excel') {
            $head = ["Plant Code", "Plant Name", "No. Of Blocks", "Created By", "Created On", "Status"];
            array_unshift($row, $head);
            $this->generateExcelFile($row, $master_form_url);
            exit();
        }
        return $this->load->view('template/master_report/plant_list_master_report', $data, true);
    }

    public function equipmentMasterDataHtml($operation, $module = null, $block_code = null, $master_form_url) {

        $this->load->model('Ptscommon');
        $param = ["is_active" => "1"];
        if ($module == 'master') {
            $param = [];
        }
        $row = [];
        $res = $this->Ptscommon->getMultipleRowsNew('default', 'mst_equipment', $param, 'created_on');
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $row[$key]['equipment_code'] = $value['equipment_code'];
                $row[$key]['equipment_name'] = $value['equipment_name'];
                $row[$key]['equipment_type'] = $value['equipment_type'];
                $row[$key]['block_code'] = $value['block_code'];
                $row[$key]['room_code'] = $value['room_code'];
                $row[$key]['modified_by'] = isset($value['modified_by']) ? $value['modified_by'] : $value['created_by'];
                $row[$key]['modified_on'] = $this->getModifiedDate($value);
                $row[$key]['is_active'] = (isset($value['is_active']) && $value['is_active'] == '1') ? 'Active' : 'Inactive';
            }
        }
        $data['res'] = $row;
        if ($operation == 'excel') {
            $head = ["Equipment Code", "Equipment Name", "Equipment Type", "Block Code", "Room Code", "Last Modified By", "Last Modified On", "Status"];
            array_unshift($row, $head);
            $this->generateExcelFile($row, $master_form_url);
            exit();
        }
        return $this->load->view('template/master_report/equipment_list_master_report', $data, true);
    }

    public function roleMasterDataHtml($operation, $module = null, $block_code = null, $master_form_url) {

        $this->load->model('Ptscommon');
        $param = ["is_active" => "1"];
        if ($module == 'master') {
            $param = [];
        }
        $row = [];
        $res = $this->Ptscommon->getMultipleRowsNew('default', 'mst_role', $param, 'created_on');
        if (!empty($res)) {
            foreach ($res as $key => $value) {
                $row[$key]['role_description'] = $value['role_description'];
                $row[$key]['role_level'] = $value['role_level'];
                $row[$key]['modified_by'] = isset($value['modified_by']) ? $value['modified_by'] : $value['created_by'];
                $row[$key]['modified_on'] = $this->getModifiedDate($value);
                $row[$key]['is_active'] = (isset($value['is_active']) && $value['is_active'] == '1') ? 'Active' : 'Inactive';
            }
        }
        $data['res'] = $row;
        if ($operation == 'excel') {
            $head = ["Role Name", "Role Level", "Last Modified By", "Last Modified On", "Status"];
            array_unshift($row, $head);
            $this->generateExcelFile($row, $master_form_url);
            exit();
        }
        return $this->load->view('template/master_report/role_master_report', $data, true);
    }

    /**
     * Master Form Report Data
     * Created By : nitin
     * Created Date:24-07-2020
     */
    public function masterReportAll() {
        $headers = apache_request_headers(); // fetch header value
        $master_form_url = $block_code = $master_form_url1 = '';
        $form_value = '';
        if ($this->input->post()) {
            $master_form_url1 = (isset($_POST['form_name']) && !empty($_POST['form_name'])) ? $_POST['form_name'] : '';
            $block_code = (isset($_POST['block_code']) && !empty($_POST['block_code'])) ? $_POST['block_code'] : '';
            $operation_type = isset($_POST['operation']) && !empty($_POST['operation']) ? $_POST['operation'] : '';
            $form_value = isset($_POST['form_val']) && !empty($_POST['form_val']) ? $_POST['form_val'] : '';
        } else if ($this->input->get()) {
            $master_form_url1 = (isset($_GET['form_name']) && !empty($_GET['form_name'])) ? $_GET['form_name'] : '';
            $block_code = isset($_GET['block_code']) && !empty($_GET['block_code']) ? $_GET['block_code'] : '';
            $operation_type = isset($_GET['operation']) && !empty($_GET['operation']) ? $_GET['operation'] : '';
            $master_form_url = isset($_GET['form_val']) && !empty($_GET['form_val']) ? $_GET['form_val'] : '';
        }

        $module = 'master';
        if (!empty($master_form_url1)) {
            switch ($master_form_url1) {
                case "manage_room":
                    $data = $this->roomConfigurationMasterDataHtml($operation_type, $module, $master_form_url);
//                    print_r($data);exit;
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "manage_room_activity":
                    $data = $this->assignedRoomMasterDataHtml($operation_type, $module, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "balance_master":
                    $data = $this->balanceMasterDataHtml($operation_type, $module, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "balance_frequency_master":
                    $data = $this->balanceCalibrationFrequencyMasterDataHtml($operation_type, $module, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "tablet_tooling":
                    $data = $this->tabletToolingMasterDataHtml($operation_type, $module, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "manage_id_standard_weight":
                    $data = $this->standardWeightMasterDataHtml($operation_type, $module, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "work_assign_asper_rolebase":
                    $data = $this->workAssignAsperRoleBaseMasterDataHtml($operation_type, $module, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "workflow":
                    $data = $this->workflowMasterDataHtml($operation_type, $module, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "line_log_master":
                    $data = $this->lineLogMasterDataHtml($operation_type, $module, $room_code = null, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "instrument_master":
                    $data = $this->getInstrumentMasterDataHtml($operation_type, $module, $room_code = null, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "stage_master":
                    $data = $this->stageMasterDataHtml($operation_type, $module, $room_code = null, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "report_header":
                    $data = $this->reportHeaderMasterDataHtml($operation_type, $module, $room_code = null, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "manage_block":
                    $data = $this->manageBlockMasterDataHtml($operation_type, $module, $room_code = null, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "create_sub_block":
                    $data = $this->subBlockMasterDataHtml($operation_type, $module, $block_code = null, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "manage_area":
                    $data = $this->areaMasterDataHtml($operation_type, $module, $block_code = null, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "manage_product":
                    $data = $this->productMasterDataHtml($operation_type, $module, $block_code = null, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "manage_employee":
                    $data = $this->employeeMasterDataHtml($operation_type, $module, $block_code, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "create_room":
                    $data = $this->roomMasterDataHtml($operation_type, $module, $block_code = null, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "create_plant":
                    $data = $this->plantMasterDataHtml($operation_type, $module, $block_code = null, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "create_equipment":
                    $data = $this->equipmentMasterDataHtml($operation_type, $module, $block_code = null, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                case "role_master":
                    $data = $this->roleMasterDataHtml($operation_type, $module, $block_code = null, $master_form_url);
                    $responseresult = array(STATUS => TRUE, "result" => TRUE, "data" => $data);
                    return $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
    }

    public function generateExcelFile($array, $master_form_url) {

        $timestamp = time();
        $filename = 'Export_excel_' . $timestamp . '.xls';
//echo $filename;exit;
        $setData = '';
        foreach ($array as $value) {
            $rowData = '';
            foreach ($value as $v) {
                $v = $this->cleanData($v) . "\t";
                $rowData .= $v;
            }
            $value = $rowData . "\n";
            $setData .= $value;
        }
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=" . $master_form_url . ".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        //file_put_contents($filename,$setData);
        echo $setData . "\n";
        exit();
    }

    function cleanData($str) {
        $str = preg_replace("/\t/", "\\t", $str);
        $str = preg_replace("/\r?\n/", "\\n", $str);
        if (strstr($str, '"')) {
            $str = '"' . str_replace('"', '""', $str) . '"';
        }
        return $str;
    }

    /**
     * Punch Set Allocation card
     * Created By :Rahul Chauhan
     * Date:04-08-2020
     */
    public function submit_punch_set_allocation() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $dimension = isset($_POST['dimension']) ? $_POST['dimension'] : '';
        $dom = isset($_POST['dom']) ? date('Y-m-d', strtotime($_POST['dom'])) : '';
        $supplier = isset($_POST['supplier']) ? $_POST['supplier'] : '';
        $tot = isset($_POST['tot']) ? $_POST['tot'] : '';
        $punch_set = isset($_POST['punch_set']) ? $_POST['punch_set'] : '';
        $no_of_subset = isset($_POST['no_of_subset']) ? $_POST['no_of_subset'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';
        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;
        $workFlow = "no";
        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workFlow = "yes";
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }
        $batch_no = "NA";
        if ($candone == 1 && $isroomin == 1) {
            $data = ["room_code" => $roomcode, "product_code" => $product_no, "dimension" => $dimension, "dom" => $dom, "supplier" => $supplier, "tot" => $tot, "punch_set" => $punch_set, "no_of_subset" => $no_of_subset, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => $workFlow];
            $data2 = ["activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2,"versionid" => $headerRecordid, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step];
            $data3 = ["approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start'];
            $uniquefiled2 = "room_code:" . $roomcode;
            $uniquefiled3 = "product_code:" . $product_no;
            $auditParams = ['command_type' => 'insert', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'Punch Set allocation Record', 'type' => 'punch_set_allocation_record','uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_punch_set_allocation'];
            $result = $this->Ptscommon->newFunctionToSubmitPunchSetAllocationRecord($data, $data2, $data3, $auditParams);
            if ($result > 0) {
                if ($next_step > 0) {
                    $this->commonEmailFunction($result);
                }
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function update_punch_set_allocation() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';

        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $booth_code = isset($_POST['booth_code']) ? $_POST['booth_code'] : '';
        $mi5_mi15 = isset($_POST['mi5_mi15']) ? $_POST['mi5_mi15'] : '';
        $mi0_mi5 = isset($_POST['mi0_mi5']) ? $_POST['mi0_mi5'] : '';
        $gi0_gi3 = isset($_POST['gi0_gi3']) ? $_POST['gi0_gi3'] : '';
        $gi15_gi5 = isset($_POST['gi15_gi5']) ? $_POST['gi15_gi5'] : '';
        $gi8_gi14 = isset($_POST['gi8_gi14']) ? $_POST['gi8_gi14'] : '';

        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $update_id = isset($_POST['update_id']) ? $_POST['update_id'] : '';
        $is_in_workflow = isset($_POST['is_in_workflow']) ? $_POST['is_in_workflow'] : '';

        if ($doc_no == '') {
            $doc_res = $this->Ptsadminmodel->get_dtl3('id', 13, 'pts_mst_document');
            $doc_row = $doc_res->row_array();

            $doc_pre = $doc_row['prefix'];
            $docno = $doc_row['docno'] + 1;
            $doc_no = $doc_pre . $docno;
        } else {
            $doc_no = $doc_no;
        }

        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        $isworkflow = 0;

        //Getting Next step of workflow of current Activity
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {
            $where = array("id" => $update_id);
            $data = array("doc_no" => $doc_no, "room_code" => $roomcode, "m1" => $mi5_mi15, "m2" => $mi0_mi5, "g1" => $gi0_gi3, "g2" => $gi15_gi5, "g3" => $gi8_gi14, "booth_no" => $booth_code, "is_in_workflow" => $is_in_workflow, "updated_by" => $empid, "updated_on" => date("Y-m-d H:i:s"), "update_remark" => $remark);
            $result = $this->Ptscommon->Com_fun_to_update_log_details('pts_trn_laf_pressure_diff', $data, $where);
            if ($result > 0) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "doc_no:" . $doc_no;
                $uniquefiled2 = "room_code:" . $roomcode;
                $uniquefiled3 = "booth_no:" . $booth_code;
                $activityData = $this->Ptscommon->getMultipleRows('default', 'pts_trn_user_activity_log', array('doc_id' => $doc_no));
                if (!empty($activityData)) {
                    $activity_id = $activityData[0]['id'];
                }
                $auditParams = array('command_type' => 'update', 'created_by' => $empid, 'created_by_name' => $empname, 'activity_name' => 'LAF Pressure Differential Record', 'type' => 'laf_pressure_record', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_laf_pressure_diff', 'primary_id' => $update_id, 'log_user_activity_id' => $activity_id, 'post_params' => $data);
                $this->Ptscommon->commonFunctionToAddAuditTransaction($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Updated.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Something went wrong please try again later.');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to update this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function GetPunchSetAllocationRecord() {
        $this->load->model('Ptscommon');
        $res = $this->Ptscommon->GetPunchSetAllocationRecord();
        foreach ($res as $key => $value) {
            $res[$key]['punch_set_data'] = $this->GetPunchSetNumbers($value['product_code']);
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => False, "punch_set_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => False, MESSAGE => 'No Punch Set Data Found', "punch_set_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function GetPunchSetNumbers($product_code) {
        $this->load->model('Ptscommon');
        $param = array("product_code" => $product_code);
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_trn_product_base_punch_set', $param);
        if (!empty($res)) {
            return $res;
        } else {
            return array();
        }
    }

    /**
     * Punch Set Allocation Report
     * Created By : Mrinal Sen Gupta
     * Created Date:07-09-2020
     */
    public function getPunchSetAllocationReport() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        ///$post['vaccume_no'] = isset($_POST['vaccume_no']) ? $_POST['vaccume_no'] : '';
        $post['start_date'] = isset($_POST['start_date']) ? date('Y-m-d', strtotime($_POST['start_date'])) : '';
        $post['end_date'] = isset($_POST['end_date']) ? date('Y-m-d', strtotime($_POST['end_date'])) : '';
        // print_r($post);
        // die();
        $res = $this->Ptscommon->getPunchSetAllocationReport($post);
        // echo "<pre/>";print_r($res);exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "row_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No recode found', "row_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function balanceMasterChanges($newData, $oldData) {
        $changes = '';
        foreach ($newData as $key => $value) {
            if (array_key_exists($key, $oldData)) {
                if ($newData[$key] != $oldData[$key]) {
                    $changes .= $key . ' : ' . $oldData[$key] . ' To ' . $newData[$key] . ',';
                }
            }
        }
        return rtrim($changes, ',');
    }

    public function GetPunchSetDataRecord() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $product_code = isset($_GET['product_code']) ? $_GET['product_code'] : '';
        $punch_set = isset($_GET['punch_set']) ? $_GET['punch_set'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $param = array("status" => "active");
        if ($product_code != '') {
            $param = array("status" => "active", "product_code" => $product_code);
        }
        if ($punch_set != '') {
            $param['no_of_subset'] = $punch_set;
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_tablet_tooling', $param, $order = null, $page);
        foreach ($res as $key => $value) {
            $dimension = $this->db->get_where("pts_trn_punch_set_allocation", array("punch_set" => $value['no_of_subset'], "product_code" => $value['product_code']))->row_array();
            if (!empty($dimension)) {
                $res[$key]['dimension'] = $dimension['dimension'];
            } else {
                $res[$key]['dimension'] = 'NA';
            }
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "tablet_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Tablet Tooling Log Found', "tablet_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function checkPunchSetDuplicacy() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers();
        $punch_set = isset($_GET['punchSet']) ? $_GET['punchSet'] : '';
        $param = ['punch_set' => $punch_set];
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_trn_punch_set_allocation', $param);
        $count = count($res);
        $responseresult = [STATUS => TRUE, "result" => TRUE, "count" => $count];
        $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
    }

    /**
     * Get Material In Batch List for out
     * Created By : Rahul Chauhan
     * Created Date:21-10-2020
     */
    public function getMaterialInBatchList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $material_code = isset($_POST['material_code']) ? $_POST['material_code'] : '';
        $this->db->select("*");
        $this->db->from("pts_trn_material_retreival_relocation");
        $this->db->where("room_code", $roomcode);
        $this->db->where("material_code", $material_code);
        $this->db->where('material_condition', 'in');
        $this->db->where('material_condition!=', 'out');
        $this->db->where("in_status", '0');
        $res = $this->db->get()->result_array();
//        echo $this->db->last_query();exit;
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "batch_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Material Found', "batch_list" => []);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    public function submit_env_con_diff_record() {
        $headers = apache_request_headers(); // fetch header value
        $roomcode = isset($_POST['roomcode']) ? $_POST['roomcode'] : '';
        $processid = isset($_POST['processid']) ? $_POST['processid'] : '';
        $actid = isset($_POST['actid']) ? $_POST['actid'] : '';
        $actname = isset($_POST['actname']) ? $_POST['actname'] : '';
        $product_no = isset($_POST['product_no']) ? $_POST['product_no'] : '';
        $batch_no = isset($_POST['batch_no']) ? $_POST['batch_no'] : '';
        $logger_id = isset($_POST['logger_id']) ? $_POST['logger_id'] : '';
        $gauge_id = isset($_POST['gauge_id']) ? $_POST['gauge_id'] : '';
        $pressure_differential = isset($_POST['pressure_differential']) ? $_POST['pressure_differential'] : '';
        $reading = isset($_POST['reading']) ? $_POST['reading'] : '';
        $temp = isset($_POST['temp']) ? $_POST['temp'] : '';
        $min_temp = isset($_POST['min_temp']) ? $_POST['min_temp'] : '';
        $max_temp = isset($_POST['max_temp']) ? $_POST['max_temp'] : '';
        $rh_value = isset($_POST['rh_value']) ? $_POST['rh_value'] : '';
        $min_rh = isset($_POST['min_rh']) ? $_POST['min_rh'] : '';
        $max_rh = isset($_POST['max_rh']) ? $_POST['max_rh'] : '';
        $from_temp = isset($_POST['from_temp']) ? $_POST['from_temp'] : '';
        $to_temp = isset($_POST['to_temp']) ? $_POST['to_temp'] : '';
        $from_humidity = isset($_POST['from_humidity']) ? $_POST['from_humidity'] : '';
        $to_humidity = isset($_POST['to_humidity']) ? $_POST['to_humidity'] : '';
        $pressure = isset($_POST['pressure']) ? $_POST['pressure'] : '';
        $from_pressure = isset($_POST['from_pressure']) ? $_POST['from_pressure'] : '';
        $to_pressure = isset($_POST['to_pressure']) ? $_POST['to_pressure'] : '';
        $roleid = isset($_POST['roleid']) ? $_POST['roleid'] : '';
        $empid = isset($_POST['empid']) ? $_POST['empid'] : '';
        $empname = isset($_POST['empname']) ? $_POST['empname'] : '';
        $email = isset($_POST['email']) ? $_POST['email'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $remark2 = $actname . ' is in progress';
        $doc_no = isset($_POST['docno']) ? $_POST['docno'] : '';
        //$id = isset($_POST['env_id']) ? $_POST['env_id'] : '';
        $headerRecordid = isset($_POST['headerRecordid']) ? $_POST['headerRecordid'] : '';
        //Check is the person room in before perform an operation 
        $isroomin = 0;
        $chkroomres = $this->Ptsadminmodel->get_dtl9('role_id', $roleid, 'emp_code', $empid, 'emp_name', $empname, 'emp_email', $email, 'pts_trn_emp_roomin');
        if ($chkroomres->num_rows() > 0) {
            $chkroomresdata = $chkroomres->row_array();
            if ($roomcode == $chkroomresdata['room_code']) {
                $isroomin = 1;
            } else {
                $isroomin = 0;
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform this action, because you are in different room : ' . $chkroomresdata['room_code'] . ', Thank you...!');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            }
        } else {
            $isroomin = 0;
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You can not perform any operation, please do room in first on home. Thank you...!');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }

        //Getting Next step of workflow of current Activity
        $isworkflow = 0;
        $wfres = $this->Ptsadminmodel->getnext_step_on_start_stop($actid, 'stop');
        if ($wfres->num_rows() > 0) {
            $isworkflow = 1;
            $workflowdata = $wfres->row_array();
            $status_id = $workflowdata['status_id'];
            $next_step = $workflowdata['next_step'];
            $roleinwf = $workflowdata['role_id'];
            $candone = "";

            $res2 = $this->Ptsadminmodel->get_dtl3('roleid', $roleinwf, 'pts_mst_role_workassign_to_mult_roles');
            foreach ($res2->result() as $r2) {
                if (($r2->workassign_to_roleid == $roleid)) {
                    $candone = 1;
                    break;
                } else {
                    $candone = 0;
                }
            }
        } 
        else {
            $candone = 1;
            $status_id = 0;
            $next_step = 0;
            $isworkflow = 0;
        }

        if ($candone == 1 && $isroomin == 1) {

            $data = ["room_code" => $roomcode, "data_log_id" => $logger_id, "magnelic_id" => $gauge_id, "pressure_diff" => $pressure_differential, "reading" => $reading, "temp" => $temp, "temp_min" => $min_temp, "temp_max" => $max_temp, "rh" => $rh_value, "rh_min" => $min_rh, "rh_max" => $max_rh, "temp_from" => $from_temp, "temp_to" => $to_temp, "rh_from" => $from_humidity, "rh_to" => $to_humidity, "globe_pressure_diff" => $pressure, "done_by_user_id" => $empid, "done_by_user_name" => $empname, "done_by_role_id" => $roleid, "done_by_email" => $email, "done_by_remark" => $remark, "is_in_workflow" => 'no', "from_pressure" => $from_pressure, "to_pressure" => $to_pressure];
            $data2 = ["activity_id" => $actid, "roomlogactivity" => $processid, "room_code" => $roomcode, "product_code" => $product_no, "batch_no" => $batch_no, "equip_code" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "activity_remarks" => $remark2,  "versionid" => $headerRecordid, "activity_start" => time(), "workflowstatus" => $status_id, "workflownextstep" => $next_step];
            $data3 = ["approval_status" => 'N/A', "user_id" => $empid, "user_name" => $empname, "user_email" => $email, "role_id" => $roleid, "activity_remarks" => $remark, "activity_time" => time(), "workflow_type" => 'start'];
            //$result = $this->Ptscommon->Com_fun_to_submit_env_log_details($isworkflow, 'pts_trn_env_cond_diff', $data, $data2, $data3, $id);
            //$result = $this->Ptscommon->newFunctionToSubmitEnvLog($isworkflow,'pts_trn_env_cond_diff', $data, $data2, $data3,$id);//commented against EL-92
            $result = $this->Ptscommon->newFunctionToSubmitEnvLog($isworkflow,'pts_trn_env_cond_diff', $data, $data2, $data3);
            if ($result > 0) {
                if ($next_step > 0) {
                    $this->commonEmailFunction($result);
                }
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Successfully Submited.', 'acttblid' => $result);
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => '0');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'You have no permission to start this record.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }
}
