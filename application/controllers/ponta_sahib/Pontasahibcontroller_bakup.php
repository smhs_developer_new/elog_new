<?php

class Pontasahibcontroller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Home Page View 
     * Created By :Rahul Chauhan
    */
    public function home(){
        $this->load->view("template/ponta_sahib/home");
        $this->load->view('footer');
    }
    
    /**
     * In Progress Activity View 
     * Created By :Rahul Chauhan
    */
    public function InProgressActivity(){
        $this->load->view("template/ponta_sahib/inprogress_activity");
        $this->load->view('footer');
    }
    
    /**
     * Pending Approval View 
     * Created By :Rahul Chauhan
    */
    public function PendingApproval(){
        $this->load->view("template/ponta_sahib/pending_approval");
        $this->load->view('footer');
    }
    
    /**
     * Process Log Equipment View 
     * Created By :Rahul Chauhan
    */
    public function ProcessLogEquipment(){
        $this->load->view("template/ponta_sahib/process_log_equipment");
        $this->load->view('footer');
    }
    
    /**
     * Process Log Add Portable Equipmet View 
     * Created By :Rahul Chauhan
    */
    public function ProcessLogPortableEquipment(){
        $this->load->view("template/ponta_sahib/process_log_portable_equipment");
        $this->load->view('footer');
    }
    
    /**
     * Return Air Filter CLeaning Record View 
     * Created By :Rahul Chauhan
    */
    public function AirFilterCleaningRecord(){
        $this->load->view("template/ponta_sahib/air_filter_cleaning_record");
        $this->load->view('footer');
    }
    
    /**
     * Material Retreival and Relocation Record for Cold Room View 
     * Created By :Rahul Chauhan
    */
    public function MaterialRetreivalRelocationRecord(){
        $this->load->view("template/ponta_sahib/material_retreival_relocation_record");
        $this->load->view('footer');
    }
    
    /**
     * Environmental Condition/Pressure Differential Record View
     * Created By :Rahul Chauhan
    */
    public function EnvironmentalConditionRecord(){
        $this->load->view("template/ponta_sahib/environmental_condition_record");
        $this->load->view('footer');
    }
    /**
     * Vaccum Cleaner Logbook View 
     * Created By :Rahul Chauhan
    */
    public function VaccumCleanerLogbook(){
        $this->load->view("template/ponta_sahib/vaccum_cleaner_logbook");
        $this->load->view('footer');
    }
    /**
     * LAF Pressure Differential Record Sheet View 
     * Created By :Rahul Chauhan
    */
    public function LAFPressureRecord(){
        $this->load->view("template/ponta_sahib/laf_pressure_record");
        $this->load->view('footer');
    }
    
    /**
     * Balance Calibration Record View 
     * Created By :Rahul Chauhan
    */
    public function BalanceCalibrationRecord(){
        $this->load->view("template/ponta_sahib/balance_calibration_record");
        $this->load->view('footer');
    }
    
    /**
     * Vertical Sampler and Dies Cleaning Usages Log View
     * Created By :Rahul Chauhan
    */
    public function VerticalSamplerDiesCleaner(){
        $this->load->view("template/ponta_sahib/vertical_sampler_dies_cleaning");
        $this->load->view('footer');
    }
    
    /**
     * Instrument Log Register View 
     * Created By :Rahul Chauhan
    */
    public function InstrumentLogRegister(){
        $this->load->view("template/ponta_sahib/instrument_log_register");
        $this->load->view('footer');
    }
    
    /**
     * Equipment/Apparatus Log Register View 
     * Created By :Rahul Chauhan
    */
    public function EquipmentApparatusLogRegister(){
        $this->load->view("template/ponta_sahib/equipment_apparatus_log_register");
        $this->load->view('footer');
    }
    
    /**
     * Swab Sample Record View 
     * Created By :Rahul Chauhan
    */
    public function SwabSampleRecord(){
        $this->load->view("template/ponta_sahib/swab_sample_record");
        $this->load->view('footer');
    }
    
    /**
     * Pre-Filter Cleaning Record of LAF View 
     * Created By :Rahul Chauhan
    */
    public function PreFilterCleaningRecord(){
        $this->load->view("template/ponta_sahib/pre_filter_cleaning_record");
        $this->load->view('footer');
    }
    
     /**
     * Tablet Tooling Log Card View 
     * Created By :Rahul Chauhan
    */
    public function LineLog(){
        $this->load->view("template/ponta_sahib/line_log");
        $this->load->view('footer');
    }
    
    /**
     * Tablet Tooling Log Card View 
     * Created By :Rahul Chauhan
    */
    public function TabletToolingLogCard(){
        $this->load->view("template/ponta_sahib/tablet_tooling_log_card");
        $this->load->view('footer');
    }
}
