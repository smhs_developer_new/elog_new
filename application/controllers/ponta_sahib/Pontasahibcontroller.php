<?php

class Pontasahibcontroller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Home Page View 
     * Created By :Rahul Chauhan
    */
    public function home(){
//        $this->load->view("template/ponta_sahib/home");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_home_new", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    /**
     * In Progress Activity View 
     * Created By :Rahul Chauhan
    */
    public function InProgressActivity(){
        // $this->load->view("template/ponta_sahib/inprogress_activity");
        // $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_inprogress_activity", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    /**
     * Pending Approval View 
     * Created By :Rahul Chauhan
    */
    public function PendingApproval(){
        // $this->load->view("template/ponta_sahib/pending_approval");
        // $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_pending_approval", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    /**
     * Process Log Equipment View 
     * Created By :Rahul Chauhan
    */
    public function ProcessLogEquipment(){
//        $this->load->view("template/ponta_sahib/process_log_equipment");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_process_log_equipment", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    /**
     * Process Log Add Portable Equipmet View 
     * Created By :Rahul Chauhan
    */
    public function ProcessLogPortableEquipment(){
//        $this->load->view("template/ponta_sahib/process_log_portable_equipment");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_process_log_portable_equipment", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    /**
     * Return Air Filter CLeaning Record View 
     * Created By :Rahul Chauhan
    */
    public function AirFilterCleaningRecord(){
//        $this->load->view("template/ponta_sahib/air_filter_cleaning_record");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_air_filter_cleaning_record", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    /**
     * Material Retreival and Relocation Record for Cold Room View 
     * Created By :Rahul Chauhan
    */
    public function MaterialRetreivalRelocationRecord(){
//        $this->load->view("template/ponta_sahib/material_retreival_relocation_record");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_material_retreival_relocation_record", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    /**
     * Environmental Condition/Pressure Differential Record View
     * Created By :Rahul Chauhan
    */
    public function EnvironmentalConditionRecord(){
//        $this->load->view("template/ponta_sahib/environmental_condition_record");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_environmental_condition_record", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * Vaccum Cleaner Logbook View 
     * Created By :Rahul Chauhan
    */
    public function VaccumCleanerLogbook(){
//        $this->load->view("template/ponta_sahib/vaccum_cleaner_logbook");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_vaccum_cleaner_logbook", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * LAF Pressure Differential Record Sheet View 
     * Created By :Rahul Chauhan
    */
    public function LAFPressureRecord(){
//        $this->load->view("template/ponta_sahib/laf_pressure_record");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_laf_pressure_record", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    /**
     * Balance Calibration Record View 
     * Created By :Rahul Chauhan
    */
    public function BalanceCalibrationRecord(){
//        $this->load->view("template/ponta_sahib/balance_calibration_record");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_balance_calibration_record", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    /**
     * Vertical Sampler and Dies Cleaning Usages Log View
     * Created By :Rahul Chauhan
    */
    public function VerticalSamplerDiesCleaner(){
//        $this->load->view("template/ponta_sahib/vertical_sampler_dies_cleaning");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_vertical_sampler_dies_cleaning", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    /**
     * Instrument Log Register View 
     * Created By :Rahul Chauhan
    */
    public function InstrumentLogRegister(){
//        $this->load->view("template/ponta_sahib/instrument_log_register");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_instrument_log_register", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    /**
     * Equipment/Apparatus Log Register View 
     * Created By :Rahul Chauhan
    */
    public function EquipmentApparatusLogRegister(){
//        $this->load->view("template/ponta_sahib/equipment_apparatus_log_register");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_equipment_apparatus_log_register", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    /**
     * Swab Sample Record View 
     * Created By :Rahul Chauhan
    */
    public function SwabSampleRecord(){
//        $this->load->view("template/ponta_sahib/swab_sample_record");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_swab_sample_record", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    /**
     * Pre-Filter Cleaning Record of LAF View 
     * Created By :Rahul Chauhan
    */
    public function PreFilterCleaningRecord(){
//        $this->load->view("template/ponta_sahib/pre_filter_cleaning_record");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_pre_filter_cleaning_record", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
     /**
     * Tablet Tooling Log Card View 
     * Created By :Rahul Chauhan
    */
    public function LineLog(){
//        $this->load->view("template/ponta_sahib/line_log");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_line_log", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    /**
     * Tablet Tooling Log Card View 
     * Created By :Rahul Chauhan
    */
    public function TabletToolingLogCard(){
//        $this->load->view("template/ponta_sahib/tablet_tooling_log_card");
//        $this->load->view('footer');
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_tablet_tooling_log_card", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    
    /**
     * Home Page View (new design)
     * Created By :Khushboo
    */
    public function sf_home(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_home_new", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * process log view View (new design)
     * Created By :Khushboo
    */
    public function sfProcessLogEquipment(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_process_log_equipment", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * Line log View (new design)
     * Created By :Khushboo
    */
    public function sfLineLog(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_line_log", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * portable equipment View (new design)
     * Created By :Khushboo
    */
    public function sfProcessLogPortableEquipment(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_process_log_portable_equipment", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * vaccume cleaner log book (new design)
     * Created By :Khushboo
    */
    public function sfVaccumCleanerLogbook(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_vaccum_cleaner_logbook", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * environmental condition record (new design)
     * Created By :Khushboo
    */
    public function sfEnvironmentalConditionRecord(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_environmental_condition_record", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * air filter cleaning record (new design)
     * Created By :Khushboo
    */
    public function sfAirFilterCleaningRecord(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_air_filter_cleaning_record", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * laf pressure record (new design)
     * Created By :Khushboo
    */
    public function sfLAFPressureRecord(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_laf_pressure_record", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * balance calibration record (new design)
     * Created By :Khushboo
    */
    public function sfBalanceCalibrationRecord(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_balance_calibration_record", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * vertical sampler dies cleaner (new design)
     * Created By :Khushboo
    */
    public function sfVerticalSamplerDiesCleaner(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_vertical_sampler_dies_cleaning", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * vertical sampler dies cleaner (new design)
     * Created By :Khushboo
    */
    public function sfInstrumentLogRegister(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_instrument_log_register", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * equipment apparatus log register (new design)
     * Created By :Khushboo
    */
    public function sfEquipmentApparatusLogRegister(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_equipment_apparatus_log_register", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * equipment apparatus leaktest (new design)
     * Created By :Bhupendra
    */
    public function sfEquipmentApparatusLeakTest(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_equipment_apparatus_leaktest", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * equipment apparatus log register (new design)
     * Created By :Khushboo
    */
    public function sfSwabSampleRecord(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_swab_sample_record", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * pre filter cleaning record (new design)
     * Created By :Khushboo
    */
    public function sfPreFilterCleaningRecord(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_pre_filter_cleaning_record", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * material and retrival relocation record (new design)
     * Created By :Khushboo
    */
    public function sfMaterialRetreivalRelocationRecord(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_material_retreival_relocation_record", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * pending aprovals (new design)
     * Created By :Khushboo
    */
    public function sfPendingApproval(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_pending_approval", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * In progress activity (new design)
     * Created By :Khushboo
    */
    public function sfInProgressActivity(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_inprogress_activity", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    /**
     * table tooling log activity (new design)
     * Created By :Khushboo
    */
    public function sfTabletToolingLogCard(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_tablet_tooling_log_card", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }

    public function testAPI(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/testAPI", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
    
    /**
     * Punch Set Alocation Record
     * Created By :Rahul Chauhan
    */
    public function punchSetAlocationRecord(){
        $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/punch_set_allocation_record", array(), true);
        $this->load->view('template/ponta_sahib/layout_default', $this->data);
    }
}
