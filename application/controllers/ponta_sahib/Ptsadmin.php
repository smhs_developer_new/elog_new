<?php

require_once("Myotworestinc.php");

class Ptsadmin extends REST2 {

    public function __construct() {
        parent::__construct();
    }

    public function addworkflow() {
        if ($this->session->userdata('empemail') && $this->session->userdata('roleid') == 2) {
            $this->load->model('Usermodel');
            $activities = $this->Usermodel->getActivitiesforworkflow();
            $role = $this->Usermodel->getRole();

            $data = array(
                'activities' => $activities,
                'role' => $role,
            );
            $this->load->view('addworkflow', ['data' => $data]);
        } else {
            return redirect(base_url() . 'User/logout');
        }
    }

    public function workflow() {
        if ($this->session->userdata('empemail')) {
            $this->load->model('Ptsadminmodel');
            $activities = $this->Ptsadminmodel->getActivitiesforworkflow();
            $role = $this->Ptsadminmodel->getRole();
            $wf = $this->Ptsadminmodel->getstartworkflow("master");
//            echo '<pre>';print_r($wf);exit;
            $stopActwf = $this->Ptsadminmodel->getstopworkflow();

            $data = array(
                'activities' => $activities,
                'role' => $role,
                'wf' => $wf,
                'stopActwf' => $stopActwf
            );

            $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_workflow", ['data' => $data], true);
            $this->load->view('template/master/layout_master', $this->data);
//            $this->load->view('template/ponta_sahib/workflow', ['data' => $data]);
        } else {
            return redirect(base_url() . 'User/logout');
        }
    }

    public function addstep($actid, $type) {
//        if ($this->session->userdata('empemail') && $this->session->userdata('roleid') == 6) {
        if ($this->session->userdata('empemail')) {
            $this->load->model('Ptsadminmodel');
            $role = $this->Ptsadminmodel->getRole();

            $data = array(
                'actid' => $actid,
                'type' => $type,
                'role' => $role,
            );

            $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_addstep", ['data' => $data], true);
            $this->load->view('template/master/layout_master', $this->data);
//            $this->load->view('template/ponta_sahib/addstep', ['data' => $data]);
        } else {
            return redirect(base_url() . 'User/logout');
        }
    }

    public function editstep($actid, $type) {
//        if ($this->session->userdata('empemail') && $this->session->userdata('roleid') == 6) {
        if ($this->session->userdata('empemail')) {
            $this->load->model('Ptsadminmodel');
            $role = $this->Ptsadminmodel->getRole();
            $wfdtl = $this->Ptsadminmodel->getworkflowbyactid($actid, $type,'master');
            $data = array(
                'actid' => $actid,
                'role' => $role,
                'type' => $type,
                'wfdtl' => $wfdtl,
            );
            $this->data['the_view_content'] = $this->load->view("template/ponta_sahib/sf_addstep", ['data' => $data], true);
            $this->load->view('template/master/layout_master', $this->data);
//            $this->load->view('template/ponta_sahib/addstep', ['data' => $data]);
        } else {
            return redirect(base_url() . 'User/logout');
        }
    }

    public function makeworkflow() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $emp_code = $this->session->userdata('empcode');
        $roleid = $this->session->userdata('roleid');
        $block_code = $this->session->userdata('empblock_code');
        $status = isset($_POST['status']) ? $_POST['status'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        if (isset($_POST['flowtype'])) {
            $flowtype = $_POST["flowtype"];
        } else {
            $flowtype = "";
        }
        if (isset($_POST['act'])) {
            $act = $_POST["act"];
        } else {
            $act = "";
        }
        if (isset($_POST['myArray'])) {
            $myArray = $_POST["myArray"];
        } else {
            $myArray = "";
        }
        $activity_name = "";
        $isExistLogWorkflow = false;
        if (!empty($act)) {
            $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_activity', array('id' => $act));
            if (!empty($res)) {
                $activity_name = $res[0]['activity_name'];
            }
            $checkActExist = $this->Ptscommon->getMultipleRows('default', 'pts_trn_workflowsteps', ['activity_id' => $act,"workflowtype"=>$flowtype]);
            if (!empty($checkActExist)) {
                $isExistLogWorkflow = true;
            }
        }
        $new_roles_list = '';
        $olddata = [];
        $this->load->model('Ptsadminmodel');
        $this->load->model('Productionmodel');
        $this->load->model('Logmaintainmodel');
        $wfresult = $this->Ptsadminmodel->getworkflow($act, $flowtype);
        try{
            $olddata = $this->Ptsadminmodel->getworkflowOld($act, $flowtype);
            $olddata = $this->Ptscommon->getNAUpdated($olddata);
        } catch (Exception $exception) {
            $this->Ptscommon->writeOnExceptionFile($exception);
        }
        foreach($myArray as $key=>$val){
            $new_roles_list.= $val['roledesc'].',';
        }
        $new_roles_list = rtrim($new_roles_list,',');
        $new_array = ['activity_name'=>$activity_name,'type'=>$flowtype,'workflow'=>$new_roles_list,'status' =>($status=='1')?'Active':'Inactive',"remarks"=>$remark];
        $new_array = $this->Ptscommon->getNAUpdated($new_array);
        if (empty($wfresult) || $status != '') {
            $this->db->delete("pts_trn_workflowsteps", array("activity_id" => $act, "workflowtype" => $flowtype));
            $k = 0;
            $count = count($myArray);
            $last = $count - 1;
            if ($count > 1) {
                $insertArrForAudit = [];
                for ($i = 0; $i < $count; $i++) {
                    if ($i == $last) {
                        $RWM = $this->Productionmodel->get_dtl2('role_id', $myArray[$i]['role'], 'mst_roleid_workflow_mapping');
                        $workflowdata = $RWM->row_array();
                        $status_id = $workflowdata['status_id'];
                        if ($status != '') {
                            if ($flowtype == 'stop') {
                                try{
                                    $AWF = $this->Logmaintainmodel->insert13para_with_tblnama('pts_trn_workflowsteps', 'status_id', $status_id, 'activity_id', $act, 'workflowtype', $flowtype, 'role_id', $myArray[$i]['role'], 'next_step', '-1', 'created_by', $this->session->userdata('empname'), 'modified_by', $this->session->userdata('empname'), 'role_name', $myArray[$i]['roledesc'], 'remark', $remark, 'is_active', $status);
                                    $insertArrForAudit[] = array('status_id' => $status_id, 'activity_id' => $act, 'activity_name' => $activity_name, 'workflowtype' => $flowtype, 'role_id' => $myArray[$i]['role'], 'next_step' => '-1', 'created_by' => $this->session->userdata('empname'), 'modified_by', $this->session->userdata('empname'), 'role_name', $myArray[$i]['roledesc'], 'remark', $remark, 'is_active', $status);
                                } catch (Exception $exception) {
                                    $this->Ptscommon->writeOnExceptionFile($exception);
                                }
                            } else if ($flowtype == 'start') {
                                try{
                                    $AWF = $this->Logmaintainmodel->insert13para_with_tblnama('pts_trn_workflowsteps', 'status_id', $status_id, 'activity_id', $act, 'workflowtype', $flowtype, 'role_id', $myArray[$i]['role'], 'next_step', '0', 'created_by', $this->session->userdata('empname'), 'modified_by', $this->session->userdata('empname'), 'role_name', $myArray[$i]['roledesc'], 'remark', $remark, 'is_active', $status);
                                    $insertArrForAudit[] = array('status_id' => $status_id, 'activity_id' => $act, 'activity_name' => $activity_name, 'workflowtype' => $flowtype, 'role_id' => $myArray[$i]['role'], 'next_step' => '0', 'created_by' => $this->session->userdata('empname'), 'modified_by', $this->session->userdata('empname'), 'role_name', $myArray[$i]['roledesc'], 'remark', $remark, 'is_active', $status);
                                } catch (Exception $exception) {
                                    $this->Ptscommon->writeOnExceptionFile($exception);
                                }
                            }
                        } else {
                            if ($flowtype == 'stop') {
                                try{
                                    $AWF = $this->Logmaintainmodel->insert13para_with_tblnama('pts_trn_workflowsteps', 'status_id', $status_id, 'activity_id', $act, 'workflowtype', $flowtype, 'role_id', $myArray[$i]['role'], 'next_step', '-1', 'created_by', $this->session->userdata('empname'), 'modified_by', NULL, 'role_name', $myArray[$i]['roledesc'], 'remark', $remark, 'is_active', '1');
                                    $insertArrForAudit[] = array('status_id' => $status_id, 'activity_id' => $act, 'activity_name' => $activity_name, 'workflowtype' => $flowtype, 'role_id' => $myArray[$i]['role'], 'next_step' => '-1', 'created_by' => $this->session->userdata('empname'), 'modified_by', NULL, 'role_name', $myArray[$i]['roledesc'], 'remark', $remark, 'is_active', '1');
                                } catch (Exception $exception) {
                                    $this->Ptscommon->writeOnExceptionFile($exception);
                                }
                            } else if ($flowtype == 'start') {
                                try{
                                    $AWF = $this->Logmaintainmodel->insert13para_with_tblnama('pts_trn_workflowsteps', 'status_id', $status_id, 'activity_id', $act, 'workflowtype', $flowtype, 'role_id', $myArray[$i]['role'], 'next_step', '0', 'created_by', $this->session->userdata('empname'), 'modified_by', NULL, 'role_name', $myArray[$i]['roledesc'], 'remark', $remark, 'is_active', '1');
                                $insertArrForAudit[] = array('status_id' => $status_id, 'activity_id' => $act, 'activity_name' => $activity_name, 'workflowtype' => $flowtype, 'role_id' => $myArray[$i]['role'], 'next_step' => '0', 'created_by' => $this->session->userdata('empname'), 'modified_by', NULL, 'role_name', $myArray[$i]['roledesc'], 'remark', $remark, 'is_active', '1');
                                } catch (Exception $exception) {
                                    $this->Ptscommon->writeOnExceptionFile($exception);
                                }
                            }
                        }
                    } else {
                        $RWM = $this->Productionmodel->get_dtl2('role_id', $myArray[$i]['role'], 'mst_roleid_workflow_mapping');
                        $workflowdata = $RWM->row_array();
                        $status_id = $workflowdata['status_id'];
                        $RWM = $this->Productionmodel->get_dtl2('role_id', $myArray[$i + 1]['role'], 'mst_roleid_workflow_mapping');
                        $workflowdata = $RWM->row_array();
                        $nxt = $workflowdata['status_id'];
                        if ($status != '') {
                            try{
                                $AWF2 = $this->Logmaintainmodel->insert13para_with_tblnama('pts_trn_workflowsteps', 'status_id', $status_id, 'activity_id', $act, 'workflowtype', $flowtype, 'role_id', $myArray[$i]['role'], 'next_step', $nxt, 'created_by', $this->session->userdata('empname'), 'modified_by', $this->session->userdata('empname'), 'role_name', $myArray[$i]['roledesc'], 'remark', $remark, 'is_active', $status);
                                $insertArrForAudit[] = array('status_id' => $status_id, 'activity_id' => $act, 'activity_name' => $activity_name, 'workflowtype' => $flowtype, 'role_id' => $myArray[$i]['role'], 'next_step' => $nxt, 'created_by' => $this->session->userdata('empname'), 'modified_by', $this->session->userdata('empname'), 'role_name', $myArray[$i]['roledesc'], 'remark', $remark, 'is_active', $status);
                            } catch (Exception $exception) {
                                $this->Ptscommon->writeOnExceptionFile($exception);
                            }
                        } else {
                            try{
                                $AWF2 = $this->Logmaintainmodel->insert13para_with_tblnama('pts_trn_workflowsteps', 'status_id', $status_id, 'activity_id', $act, 'workflowtype', $flowtype, 'role_id', $myArray[$i]['role'], 'next_step', $nxt, 'created_by', $this->session->userdata('empname'), 'modified_by', NULL, 'role_name', $myArray[$i]['roledesc'], 'remark', $remark, 'is_active', '1');
                                $insertArrForAudit[] = array('status_id' => $status_id, 'activity_id' => $act, 'activity_name' => $activity_name, 'workflowtype' => $flowtype, 'role_id' => $myArray[$i]['role'], 'next_step' => $nxt, 'created_by' => $this->session->userdata('empname'), 'modified_by', NULL, 'role_name', $myArray[$i]['roledesc'], 'remark', $remark, 'is_active', '1');
                            } catch (Exception $exception) {
                                $this->Ptscommon->writeOnExceptionFile($exception);
                            }
                        }
                    }
                }

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "role_id:" . $roleid;
                $uniquefiled2 = "";
                $uniquefiled3 = "";
                $inserted_id = !empty($AWF) ? $AWF : (!empty($AWF2) ? $AWF : 0);
                if ($isExistLogWorkflow) {
                    $auditParams = array('command_type' => 'update', 'activity_name' => 'Workflow', 'type' => 'workflow', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_workflowsteps', 'primary_id' => $AWF, 'post_params' => $new_array,'master_previous_data'=>$olddata);
                } else {
                    $auditParams = array('command_type' => 'insert', 'activity_name' => 'Workflow', 'type' => 'workflow', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_trn_workflowsteps', 'primary_id' => $AWF, 'post_params' => $new_array);
                }

                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */
                echo json_decode($AWF);
            } else {
                echo json_decode("-1");
            }
        } else {
            echo json_decode("-2");
        }
    }

    public function deleteworkflow() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $emp_code = $this->session->userdata('empcode');
        $roleid = $this->session->userdata('roleid');
        $block_code = $this->session->userdata('empblock_code');
        if (isset($_POST['act'])) {
            $act = $_POST["act"];
        } else {
            $act = "";
        }
        if (isset($_POST['type'])) {
            $type = $_POST["type"];
        } else {
            $type = "";
        }
        $AR = $this->Ptsadminmodel->deleterecords($act, $type);

        /* Start - Insert elog audit histry */
        $activity = $this->Ptscommon->getActivityNameType("pts_trn_workflowsteps");
        $auditParams = array('command_type' => 'delete', 'activity_name' => $activity['activity_name'], 'type' => $activity['type'], 'table_name' => "pts_trn_workflowsteps", 'primary_id' => $act, 'post_params' => array("activity_id" => $act, "workflowtype" => $type));
        $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
        /* End -  Insert elog audit histry */
        echo json_decode($AR);
    }

    public function compareWorkFlowArrays($newarray, $oldarray,$act = null, $activity_name = null){
        $changes = '';
        $new_role_list =[];
        $old_role_list = [];
        if(array_key_exists('workflow', $newarray)){
            $new_role_list= explode(',',$newarray['workflow']);
        }
        if(array_key_exists('workflow', $oldarray)){
            $old_role_list = array_filter(explode(',',$oldarray['workflow']));
        }
        $roles_added = array_diff($new_role_list,$old_role_list);
        $roles_removed = array_diff($old_role_list,$new_role_list);
        $changes.= !empty($roles_added)?"Roles Added : ".implode(',', $roles_added).'<br>':'';
        $changes.= !empty($roles_removed)?"Roles Removed : ".implode(',', $roles_removed).'<br>':'';

        $new_status = array_key_exists('status', $newarray)?$newarray['status']:'';
        $old_status = array_key_exists('status', $oldarray)?$oldarray['status']:'';
        $status_changes = '';
        if(!empty($new_status)|| !empty($old_status)){
            $status_changes = $old_status;
            if($new_status!=$old_status ){
                $status_changes.= !empty($old_status)?'  To ':'';
                $status_changes.=!empty($new_status)?$new_status:'';
            }
            if(!empty($status_changes)){
                $status_changes = 'Status : '.$status_changes.'<br>';
            }
        }

        
        $changes = $changes.$status_changes;
        $text = $act.' New Workflow has been Added for '.$activity_name.'<br>'.$changes;
        if(!empty($oldarray)){
            $text = $act.' Workflow has been updated for '.$activity_name.'<br>'.$changes;
        }
        return $text;
    }
}
