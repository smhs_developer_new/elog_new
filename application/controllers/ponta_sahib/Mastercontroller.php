<?php

date_default_timezone_set('Asia/Kolkata');
require_once("Myotworestinc.php");

class Mastercontroller extends REST2 {

    public function __construct() {
        parent::__construct();
        $this->load->library("email");
        if (!$this->session->userdata('empemail')) {
            return redirect('login');
        }
        if (!isset($_SERVER['HTTP_REFERER'])) {
            return redirect("login?err=1");
        }
    }

    /**
     * Master Material Page
     * Created By :Rahul Chauhan
     */
    public function ManageMaterial() {
        $this->load->view('template/master/manage_material');
        $this->load->view('footer');
    }

    /**
     * Master Room Page
     * Created By :Rahul Chauhan
     */
    public function ManageRoom() {
        //        $this->load->view( 'template/master/manage_room' );
        //        $this->load->view( 'footer' );
        $this->data['the_view_content'] = $this->load->view('template/master/sf_manage_room', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    /**
     * Master filter approval
     * Created By : Bhupendra kumar
     */
    public function FilterApproval() {
        $this->load->view('template/master/filter_approval');
        $this->load->view('footer');
    }

    /**
     * Master Room Activity Page
     * Created By :Rahul Chauhan
     */
    public function ManageRoomActivity() {
        //        $this->load->view( 'template/master/manage_room_activity' );
        //        $this->load->view( 'footer' );
        $this->data['the_view_content'] = $this->load->view('template/master/sf_manage_room_activity', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    /**
     * Balance Master Page
     * Created By :Rahul Chauhan
     */
    public function ManageBalanceMaster() {
        //        $this->load->view( 'template/master/balance_master' );
        //        $this->load->view( 'footer' );
        $this->data['the_view_content'] = $this->load->view('template/master/sf_balance_master', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    /**
     * Balance Frequency Master Page
     * Created By :Rahul Chauhan
     */
    public function ManageBalanceFrequencyMaster() {
        //        $this->load->view( 'template/master/balance_frequency_master' );
        //        $this->load->view( 'footer' );
        $this->data['the_view_content'] = $this->load->view('template/master/sf_balance_frequency_master', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    /**
     * Tablet Tooling Master Page
     * Created By :Rahul Chauhan
     */
    public function ManageTabletToolingMaster() {
        //        $this->load->view( 'template/master/tablet_tooling_master' );
        //        $this->load->view( 'footer' );
        $this->data['the_view_content'] = $this->load->view('template/master/sf_tablet_tooling_master', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    /**
     * Id No Standard Weight Master
     * Created By :Rahul Chauhan
     */
    public function ManageIdStandardWeight() {
        //        $this->load->view( 'template/master/manage_id_standard_weight' );
        //        $this->load->view( 'footer' );
        $this->data['the_view_content'] = $this->load->view('template/master/sf_manage_id_standard_weight', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    /**
     * Manage Filter Master
     * Created By :Rahul Chauhan
     */
    public function ManageFilterMaster() {
        $this->load->view('template/master/manage_filter');
        $this->load->view('footer');
    }

    /**
     * Line Log Master
     * Created By :Rahul Chauhan
     */
    public function LineLogMaster() {
        //        $this->load->view( 'template/master/line_log_master' );
        //        $this->load->view( 'footer' );
        $this->data['the_view_content'] = $this->load->view('template/master/sf_line_log_master', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    /**
     * Stage Master
     * Created By :Rahul Chauhan
     */
    public function StageMaster() {
        //        $this->load->view( 'template/master/manage_stages' );
        //        $this->load->view( 'footer' );
        $this->data['the_view_content'] = $this->load->view('template/master/sf_manage_stages', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    /**
     * User Management
     * Created By :Rahul Chauhan
     */
    public function userManagement() {
        //        $this->load->view( 'template/master/manage_stages' );
        //        $this->load->view( 'footer' );
        $this->data['the_view_content'] = $this->load->view('template/master/user_management', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    /**
     * Filter Master
     * Created By :Rahul Chauhan
     */
    public function WorkAssignAsPerRoleBase() {
        //        $this->load->view( 'template/master/work_assign_asper_rolebase' );
        //        $this->load->view( 'footer' );
        $this->data['the_view_content'] = $this->load->view('template/master/sf_work_assign_asper_rolebase', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    /**
     * Process Log Room Page Report
     * Created By :Rahul Chauhan
     */
    public function ProcessRoomLogReport() {
        $this->load->view('template/report/process_room_log_report');
    }

    /**
     * Line Log Room Page
     * Used to display the details list of line log operations
     */
    public function LineLogReport() {
        $this->load->view('template/report/line_log_report');
    }

    /**
     * Portabble Equipment Report
     * Used to display the details list of Portabble Equipment log operations
     */
    public function ProtableEquipmentReport() {
        $this->load->view('template/report/protable_equip_report');
    }

    /**
     * Swab Sample Record
     * Used to display the SWAB Samlple details list of line log operations
     */
    public function SwabSampleReport() {
        $this->load->view('template/report/swab_sample_report');
    }

    /**
     * Equipment Apparatus Report
     * Used to display the Equipment Apparatus Report details
     */
    public function EquipmentApparatusReport() {
        $this->load->view('template/report/equipment_apparatus_report');
    }

    /**
     * Equipment Apparatus Report
     * Used to display the Equipment Apparatus Report details
     */
    public function EquipmentApparatusLeakTestReport() {
        $this->load->view('template/report/equipment_apparatus_leaktest_report');
    }

    /**
     * Instrument Master Page
     * Created By :Rahul Chauhan
     */
    public function InstrumentMaster() {
        //        $this->load->view( 'template/master/instrument_master' );
        //        $this->load->view( 'footer' );
        $this->data['the_view_content'] = $this->load->view('template/master/sf_instrument_master', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    /**
     * Audit Trail Report
     * Created By :Khushboo
     */
    public function auditTrailReport() {
        $param = array("is_active" => "1");
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_employee', $param);
        $role = $this->Ptscommon->getMultipleRows('default', 'mst_role');
//        print_r($res);die;
        $this->data['userData'] = json_encode($res);
        $this->data['roleData'] = json_encode($role);
        $this->load->view('template/report/audit_trail_report', $this->data);
    }

    /**
     * Pre Filter Report
     * Created By :Khushboo
     */
    public function preFilterReport() {
        $this->load->view('template/report/pre_filter_report');
    }

    /**
     * Pre Filter Report
     * Created By :Khushboo
     */
    public function balaneCalibrationReport() {
        $this->load->view('template/report/balance_calibration_report');
    }

    /**
     * vertical Sampler Report
     * Created By : Bhupendra kumar
     */
    public function verticalSamplerReport() {
        $this->load->view('template/report/vertical_sampling_log_report');
    }

    /**
     * Leak test Report
     * Created By :Khushboo
     */
    public function leakTestReport() {
        $this->load->view('template/report/leak_test_logbook_report');
        $this->load->view('footer');
    }

    /**
     * friabilator Report
     * Created By :Khushboo
     */
    public function friabilatorReport() {
        $this->load->view('template/report/friabilator_logbook_report');
        $this->load->view('footer');
    }

    /**
     * instrument Report
     * Created By :Khushboo
     */
    public function instrumentReport() {
        $this->load->view('template/report/instrument_log_report');
    }

    /**
     * bladeSampling Report
     * Created By :Khushboo
     */
    public function bladeSamplingReport() {
        $this->load->view('template/report/vertical_sampling_log_report');
        $this->load->view('footer');
    }

    /**
     * lafPressureDiff Report
     * Created By :Khushboo
     */
    public function lafPressureDiffReport() {
        $this->load->view('template/report/laf_pressure_differential_report');
    }

    /**
     * lafPressureDiff Report
     * Created By :Khushboo
     */
    public function vaccumeCleanerReport() {
        $this->load->view('template/report/vaccume_cleaner_log_report');
    }

    /**
     * Environmental and pressure Diff Report
     * Created By :Khushboo
     */
    public function envPressureDiffReport() {
        $this->load->view('template/report/environmental_pressure_report');
    }

    /**
     * Material Relocation Report
     * Created By :Khushboo
     */
    public function materialRelocationReport() {
        $this->load->view('template/report/material_retrieval_relocation');
    }

    /**
     * air filter cleaning report Report
     * Created By :Khushboo
     */
    public function airFilterCleaningReport() {
        $this->load->view('template/report/return_air_filter_report');
    }

    /**
     * Report Header
     * Created By :Rahul Chauhan
     */
    public function reportHeader() {
        //        $this->load->view( 'template/master/report_header' );
        //        $this->load->view( 'footer' );
        $this->data['the_view_content'] = $this->load->view('template/master/sf_report_header', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    /**
     * air filter cleaning report Report
     * Created By :Khushboo
     */
    public function tabletToolingReport() {
        $this->load->view('template/report/tablet_tooling_report');
    }

    /**
     * mst block(copy code from previous)
     * Created By :Khushboo
     */
    public function mstBlock() {
        $this->load->model('Productionmodel');
        $plantcode = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_plant');
        $data = array('pcode' => $plantcode);
        // header menu code
        $result = $this->Usermodel->getActivitytype();
        // End header menu code 
//        $this->data['the_view_content'] = $this->load->view("template/master/mstBlock", ['data' => $data], true);
        $this->data['the_view_content'] = $this->load->view("template/master/block_master", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstBlock', ['data'=>$data]);
    }

    /**
     * mst block(copy code from previous)
     * Created By :Khushboo
     */
    public function checkBlockcode() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->checkBlockcode($_POST);
        echo json_encode($response);
    }

    /**
     * mst block(copy code from previous)
     * Created By :Khushboo
     */
    public function mstBlockSubmit() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->mstBlockSubmit($_POST);
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    /**
     * mst block view (copy code from previous)
     * Created By :Khushboo
     */
    public function mstBlockview() {
        $this->load->model('Adminmodel');
        $data = $this->Adminmodel->mstBlockview();
        // header menu code
        $result = $this->Usermodel->getActivitytype();
        // End header menu code
        $this->data['the_view_content'] = $this->load->view("template/master/mstBlockview", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstBlockview', ['data'=>$data]);
    }

    public function mstBlockviewnow($id) {
        $this->load->model('Adminmodel');
        $this->load->model('Productionmodel');
        $plantcode = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_plant');
        $blockdata = $this->Adminmodel->mstBlockviewnow($id);
        $data = array('pcode' => $plantcode, 'blockdata' => $blockdata);
        // header menu code
        $result = $this->Usermodel->getActivitytype();
        $this->data['the_view_content'] = $this->load->view("template/master/mstBlockviewnow", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    public function checkPlantlimit() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->checkPlantlimit($_POST);
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    public function mstBlockUpdate() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->mstBlockUpdate($_POST);
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    public function deleteBlock() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->deleteBlock($_POST);
        echo json_encode($response);
    }

    //Area code (copy from admin)
    public function mstArea() {
        $this->load->model('Productionmodel');
        $areacode = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_department', 'department_code');
        $data = array('acode' => $areacode);
        // header menu code
        $result = $this->Usermodel->getActivitytype();
        $this->data['the_view_content'] = $this->load->view("template/master/area_master", ['data' => $data], true);
//        $this->data['the_view_content'] = $this->load->view("template/master/mstArea", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//    $this->load->view('Admin/mstArea', ['data'=>$data]);
    }

    public function checkAreacode() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->checkAreacode($_POST);
        echo json_encode($response);
    }

    public function mstAreaSubmit() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->mstAreaSubmit($_POST);
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    public function mstAreaview() {
        $this->load->model('Adminmodel');
        $data = $this->Adminmodel->mstAreaview();
        // header menu code
        $result = $this->Usermodel->getActivitytype();
        $this->data['the_view_content'] = $this->load->view("template/master/mstAreaview", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//    $this->load->view('Admin/mstAreaview', ['data'=>$data]);
    }

    public function mstAreaviewnow($id) {
        $this->load->model('Adminmodel');
        $this->load->model('Productionmodel');
        $blockcode = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_department');
        $areadata = $this->Adminmodel->mstAreaviewnow($id);
        $data = array('bcode' => $blockcode, 'areadata' => $areadata);
        // header menu code
        $result = $this->Usermodel->getActivitytype();
        $this->data['the_view_content'] = $this->load->view("template/master/mstAreaviewnow", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//    $this->load->view('Admin/mstAreaviewnow', ['data'=>$data]);
    }

    public function mstAreaupdate() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->mstAreaupdate($_POST);
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    public function deleteArea() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->deleteArea($_POST);
        echo json_encode($response);
    }

    // Drain point Master pages
    public function mstDrainpoint() {
        $this->load->model('Productionmodel');
        $roomcode = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_room');
        $solcode = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_solution');
        $data = array('rcode' => $roomcode, 'scode' => $solcode);
        // header menu code
        $result = $this->Usermodel->getActivitytype();
        $this->data['the_view_content'] = $this->load->view("template/master/mstDrainpoint", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstDrainpoint', ['data' => $data]);
    }

    public function checkDraincode() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->checkDraincode($_POST);
        echo json_encode($response);
    }

    public function mstDrainpointSubmit() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->mstDrainpointSubmit($_POST);
        echo json_encode($response);
    }

    public function mstDrainpointview() {
        $this->load->model('Adminmodel');
        $data = $this->Adminmodel->mstDrainpointview();
        // header menu code
        $result = $this->Usermodel->getActivitytype();
        $this->data['the_view_content'] = $this->load->view("template/master/mstDrainpointview", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstDrainpointview', ['data' => $data]);
    }

    public function mstDrainpointviewnow($id) {
        $this->load->model('Productionmodel');
        $roomcode = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_room');
        $solcode = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_solution');
        $this->load->model('Adminmodel');
        $dpdtls = $this->Adminmodel->mstDrainpointviewnow($id);
        $data = array('rcode' => $roomcode, 'scode' => $solcode, 'dpdetails' => $dpdtls);
        // header menu code
        $result = $this->Usermodel->getActivitytype();
        $this->data['the_view_content'] = $this->load->view("template/master/mstDrainpointviewnow", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstDrainpointviewnow', ['data' => $data]);
    }

    public function mstDrainpointupdate() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->mstDrainpointupdate($_POST);
        echo json_encode($response);
    }

    public function deleteDrainpoint() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->deleteDrainpoint($_POST);
        echo json_encode($response);
    }

    public function batchDrainpoint() {

        $this->data['the_view_content'] = $this->load->view("template/master/uploadDrainpoint", array(), true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/uploadDrainpoint');
    }

    public function uploadDrainpointExcel() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);

        $this->load->model('Adminmodel');


        //if ($this->input->post('submit')) {
        //echo "file name is ". $_FILES['uploadFile']['name'];

        $result = 0;
        $path = 'uploads/';
        require_once APPPATH . "/third_party/PHPExcel/Classes/PHPExcel.php";
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'xlsx|xls|csv';
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('uploadFile')) {
            //if (!$this->upload->do_upload($_FILES['uploadFile']['name'])) {

            $error = array('error' => $this->upload->display_errors());
            $response = array("status" => 0);
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
        if (empty($error)) {
            if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
                //$import_xls_file = $_FILES['uploadFile']['name'];
            } else {
                $import_xls_file = 0;
            }
            $inputFileName = $path . $import_xls_file;



            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
                $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $flag = true;
                $i = 0;
                foreach ($allDataInSheet as $value) {
                    if ($flag) {
                        $flag = false;
                        continue;
                    }
                    $this->db->where("is_active", 1);
                    $this->db->where("drainpoint_code", $value['A']);
                    //$this->db->where("department_code",$subblock);
                    $query = $this->db->get("mst_drainpoint");
                    if ($query->num_rows() > 0) {
                        $result = $this->Adminmodel->update13_5para_with_1where_and_tbl('mst_drainpoint', 'drainpoint_code', $value['A'], 'drainpoint_name', $value['B'], 'drainpoint_remark', $value['C'], 'solution_code', $value['E'], 'modified_by', $this->session->userdata('empname'), 'modified_on', date("Y-m-d H:i:s"));
                        /* if($res>0)
                          {
                          //echo "Imported successfully";
                          $response = array("status"=>1);
                          } */
                    } else {

                        $inserdata[$i]['drainpoint_code'] = $value['A'];
                        $inserdata[$i]['drainpoint_name'] = $value['B'];
                        $inserdata[$i]['drainpoint_remark'] = $value['C'];
                        $inserdata[$i]['solution_code'] = $value['D'];
                        //$inserdata[$i]['solution_code'] = $value['E'];
                        $inserdata[$i]['created_by'] = $this->session->userdata('empname');
                        $i++;
                    }
                }

                //print_r($inserdata);
                unlink($inputFileName);

                if (isset($inserdata)) {
                    $result = $this->Adminmodel->importDataExcel($inserdata, "mst_drainpoint");
                }

                //echo "result is ".$result;

                if ($result) {
                    //echo "Imported successfully";
                    $response = array("status" => 1);
                } else {
                    //echo "ERROR !";
                    $response = array("status" => 0);
                }
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' . $e->getMessage());
            }
        } else {

            echo $error['error'];
            $response = array("status" => 0);
        }


        //}
        echo json_encode($response);
    }

    // product detals
    public function batchProduct() {

        $this->data['the_view_content'] = $this->load->view("template/master/uploadProduct", array(), true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/uploadProduct');
    }

    public function uploadProductExcel() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);

        $this->load->model('Adminmodel');


        //if ($this->input->post('submit')) {
        //echo "file name is ". $_FILES['uploadFile']['name'];

        $result = 0;
        $path = 'uploads/';
        require_once APPPATH . "/third_party/PHPExcel/Classes/PHPExcel.php";
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'xlsx|xls|csv';
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('uploadFile')) {
            //if (!$this->upload->do_upload($_FILES['uploadFile']['name'])) {

            $error = array('error' => $this->upload->display_errors());
            $response = array("status" => 0);
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
        if (empty($error)) {
            if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
                //$import_xls_file = $_FILES['uploadFile']['name'];
            } else {
                $import_xls_file = 0;
            }
            $inputFileName = $path . $import_xls_file;



            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
                $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $flag = true;
                $i = 0;
                foreach ($allDataInSheet as $value) {
                    if ($flag) {
                        $flag = false;
                        continue;
                    }
                    $this->db->where("is_active", 1);
                    $this->db->where("product_code", $value['A']);
                    //$this->db->where("department_code",$subblock);
                    $query = $this->db->get("mst_product");
                    if ($query->num_rows() > 0) {
                        $result = $this->Adminmodel->update13_5para_with_1where_and_tbl('mst_product', 'product_code', $value['A'], 'product_name', $value['B'], 'product_market', $value['C'], 'product_remark', $value['D'], 'modified_by', $this->session->userdata('empname'), 'modified_on', date("Y-m-d H:i:s"));
                        /* if($res>0)
                          {
                          //echo "Imported successfully";
                          $response = array("status"=>1);
                          } */
                    } else {

                        $inserdata[$i]['product_code'] = $value['A'];
                        $inserdata[$i]['product_name'] = $value['B'];
                        $inserdata[$i]['product_market'] = $value['C'];
                        $inserdata[$i]['product_remark'] = $value['D'];
                        $inserdata[$i]['created_by'] = $this->session->userdata('empname');
                        $i++;
                    }
                }

                //print_r($inserdata);
                unlink($inputFileName);

                if (isset($inserdata)) {
                    $result = $this->Adminmodel->importDataExcel($inserdata, "mst_product");
                }

                //echo "result is ".$result;

                if ($result) {
                    //echo "Imported successfully";
                    $response = array("status" => 1);
                } else {
                    //echo "ERROR !";
                    $response = array("status" => 0);
                }
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' . $e->getMessage());
            }
        } else {

            echo $error['error'];
            $response = array("status" => 0);
        }


        //}
        echo json_encode($response);
    }

    public function UpdateProduct() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $timezone = "Asia/Kolkata";
        date_default_timezone_set($timezone);
        $date = date("Y-m-d H:i:s");
        $array = array("product_name" => $_POST["pname"], "product_market" => $_POST["market"], "product_remark" => $_POST["remark"], "modified_by" => $this->session->userdata('empname'), "modified_on" => $date);
        $auditArray = ["product_code" => $_POST["pcode"], "product_name" => $_POST["pname"], "product_market" => $_POST["market"], 'status'=>$_POST['status'] == 'true'?'Active':'Inactive',"remarks" => $_POST["remark"]];
        $auditArray = $this->Ptscommon->getNAUpdated($auditArray);
        if ($_POST['status'] == 'true') {
            $array['is_active'] = 1;
        } else {
            $array['is_active'] = 0;
        }
        $master_previous_data =[];
        $old_data = $this->db->get_where('mst_product',['id'=>$_POST['hdd']])->row_array();
        if(!empty($old_data)){
            $master_previous_data =  ["product_name" => $old_data["product_name"], "product_market" => $old_data["product_market"], 'status'=>$old_data['is_active'] == 1?'Active':'Inactive',];
            $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
        }
        $this->db->where("id", $_POST['hdd']);
        if ($this->db->update("mst_product", $array)) {
            /* Start - Insert elog audit histry */
            $uniquefiled1 = "product_code:" . $_POST['pcode'];
            $uniquefiled2 = "product_name:" . $_POST['pname'];
            $uniquefiled3 = "";
            $auditParams = array('command_type' => 'update', 'activity_name' => 'Product Master', 'type' => 'manage_product', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_product', 'primary_id' => $_POST['hdd'], 'post_params' => $auditArray,'master_previous_data'=>$master_previous_data);
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            $response = array("status" => 1);
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Product Data Updated Successfuly.');
            $this->Apifunction->response($this->Apifunction->json($response), 200);
            /* End -  Insert elog audit histry */
        } else {
            $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Product data does not updated.');
            $this->Apifunction->response($this->Apifunction->json($response), 200);
        }
        return $response;
    }

    public function EditProduct($id) {
//                if ($this->session->userdata('empemail') && $this->session->userdata('roleid') == 6) {

        if ($this->session->userdata('empemail')) {
            $prodtl = $this->Adminmodel->get_dtl2("id", $id, "mst_product");
            $data = array(
                'prodtl' => $prodtl,
            );

            $this->data['the_view_content'] = $this->load->view("template/master/Edit_Product", ['data' => $data], true);
            $this->load->view('template/master/layout_master', $this->data);
//            $this->load->view('Admin/Edit_Product', ['data' => $data]);
//            $this->load->view('footer');
        } else {
            return redirect(base_url() . 'User/logout');
        }
    }

    public function Productlist() {
//                if ($this->session->userdata('empemail') && $this->session->userdata('roleid') == 6) {

        if ($this->session->userdata('empemail')) {
            $prodtl = $this->Adminmodel->get_dtl1("mst_product");

            $data = array(
                'prodtl' => $prodtl,
            );

            $this->data['the_view_content'] = $this->load->view("template/master/Product_List", ['data' => $data], true);
            $this->load->view('template/master/layout_master', $this->data);
//            $this->load->view('Admin/Product_List', ['data' => $data]);
        } else {
            return redirect(base_url() . 'User/logout');
        }
    }

    public function AddProduct() {
//                if ($this->session->userdata('empemail') && $this->session->userdata('roleid') == 6) {

        if ($this->session->userdata('empemail')) {
            $this->data['the_view_content'] = $this->load->view("template/master/Add_Product", array(), true);
            $this->load->view('template/master/layout_master', $this->data);
//            $this->load->view('Admin/Add_Product');
        } else {
            return redirect(base_url() . 'User/logout');
        }
    }

    public function AddNewProduct() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $array = array("product_code" => $_POST["pcode"], "product_name" => $_POST["pname"], "product_market" => $_POST["market"], "product_remark" => $_POST["remark"], "created_by" => $this->session->userdata('empname'));
        $auditArray = ["product_code" => $_POST["pcode"], "product_name" => $_POST["pname"], "product_market" => $_POST["market"], 'status'=>'Active',"remarks" => $_POST["remark"]];
        $auditArray = $this->Ptscommon->getNAUpdated($auditArray);
        $this->db->where("product_code", $_POST["pcode"]);
        $getdata = $this->db->get("mst_product")->row_array();
        if (!empty($getdata)) {
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Product Code already exist');
            $this->Apifunction->response($this->Apifunction->json($response), 200);
        } else {
            if ($this->db->insert("mst_product", $array)) {
                
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "product_code:" . $_POST['pcode'];
                $uniquefiled2 = "product_name:" . $_POST['pname'];
                $uniquefiled3 = "";
                $inserted_id = $this->db->insert_id();
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Product Master', 'type' => 'manage_product', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_product', 'primary_id' => $inserted_id, 'post_params' => $auditArray);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Product Data saved successfully.');
                $this->Apifunction->response($this->Apifunction->json($response), 200);
                /* End -  Insert elog audit histry */
            } else {
                $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Product does not Added');
                $this->Apifunction->response($this->Apifunction->json($response), 200);
            }
        }
        return $response;
    }

    public function chkcode() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $tblname = $_POST['tblname'];
        $col = $_POST['col'];
        $val = $_POST['code'];
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->checkcode($tblname, $col, $val);
        echo json_encode($response);
    }

    public function getdept() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Productionmodel');
        $block = $_POST["block"];
        $blockdtl = $this->Adminmodel->getdeptbyblock($block);
        $response = $blockdtl->result_array();
        echo json_encode($response);
    }

    public function CommonDelete() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $timezone = "Asia/Kolkata";
        date_default_timezone_set($timezone);
        $emp_code = $this->session->userdata('empcode');
        $roleid = $this->session->userdata('roleid');
        $block_code = $this->session->userdata('empblock_code');
        $id = $_POST['id'];
        $tblname = $_POST['tblname'];
        $colname = $_POST['colname'];
        $AR = $this->Adminmodel->deleterecords($colname, $id, $this->session->userdata('empcode'), date("Y-m-d H:i:s"), $tblname);
        if ($tblname == 'mst_product' || $tblname == 'mst_employee') {
            /* Start - Insert elog audit histry */
            $activity = $this->Ptscommon->getActivityNameType($tblname);
            $auditParams = array('command_type' => 'delete', 'activity_name' => $activity['activity_name'], 'type' => $activity['type'], 'table_name' => $tblname, 'primary_id' => $id, 'post_params' => array($colname => $id, "is_active" => 0, "modified_by" => $this->session->userdata('empname'), "modified_on" => date("Y-m-d H:i:s")));
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        }

        echo json_decode($AR);
    }

    // Manage Employee details

    public function UpdateEmployee() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $subblock = $_POST['subblock'];
        $block = $_POST['block'];
        $subblock = $_POST['subblock'];
        $subblock = explode(',', $subblock);
        $subblock = array_unique($subblock);
        $subblock = implode(',', $subblock);
        $area = $_POST['area'];
        $room_code = $_POST['room_code'];
        if ($_POST['is_directory'] == 'true') {
            $is_directory = '1';
            $active_directory = 'Yes';
        } else {
            $is_directory = '0';
            $active_directory = 'No';
        }
        if ($_POST['is_generate'] == 'true') {
            $is_generate = '1';
            $password_reset = 'Yes';
        } else {
            $is_generate = '0';
            $password_reset = 'No';
        }
         
        $role_privelege=$this->db->get_where("pts_mst_role_mgmt",array("role_id"=>$_POST['role']))->result_array();
        $user_privelege=$this->db->get_where("pts_mst_user_mgmt",array("user_id"=>$_POST['hdd'],"module_type"=>"log"))->result_array();
        $new_privilege=array();
        if(!empty($role_privelege)){
          foreach ($role_privelege as $key => $value) {
              if($value['module_type']!='log'){
                  $new_privilege[]=array("user_id"=>$_POST['hdd'],"module_id"=>$value['module_id'],"room_code"=>NULL,"type_id"=>NULL,"module_type"=>$value['module_type'],"is_create"=>$value['is_create'],"is_edit"=>$value['is_edit'],"is_view"=>$value['is_view'],"created_on"=>date('Y-m-d H:i:s'),"created_by"=>$this->session->userdata('empname'));
              }
            
        }  
        }
        if(!empty($user_privelege)){
            if(!empty($role_privelege)){
                foreach ($role_privelege as $key1 => $val1) {
                    if($val1['module_type']=='log'){
                    foreach ($user_privelege as $key2 => $val2) {
                        if($val1['module_id']==$val2['module_id']){
                            $new_privilege[]=array("user_id"=>$_POST['hdd'],"module_id"=>$val1['module_id'],"room_code"=>$val2['room_code'],"type_id"=>$val2['type_id'],"module_type"=>$val1['module_type'],"is_create"=>$val1['is_create'],"is_edit"=>$val1['is_edit'],"is_view"=>$val1['is_view'],"created_on"=>date('Y-m-d H:i:s'),"created_by"=>$this->session->userdata('empname'));
                        }
                    }
                    }
                }
            }
        }
        
//        echo '<pre>';print_r($new_privilege);exit;
        $role_data = $this->db->get_where("mst_role", array("id" => $_POST['role']))->row_array();
        $auditArray = ['user_id'=>$_POST['email'],'emp_code'=>$_POST['code'],'emp_name'=>$_POST['empname'],'address'=>$_POST['add'],
            'contact'=>$_POST['cont'],'email'=>$_POST['email2'],'active_directory'=>$_POST['is_directory']=='true'?'Yes':'No','is_blocked'=>$_POST['is_blocked']=='true'?'Yes':'No',
            'designation'=>$_POST['desg'],'role'=>$role_data['role_description'],'block_code' => $_POST['block'], 'Sub_block_code' => $subblock, 'area_code' => $area,'room_code'=>$_POST['room_code'],
            'is_blocked'=>$_POST['is_blocked']=='true'?'Yes':'No','status'=>$_POST['status'] == 'true'?'Active':'Inactive','remark' => $_POST['remark']];
        $auditArray = $this->Ptscommon->getNAUpdated($auditArray);
        if($_POST['is_generate']=='true'){
            $auditArray['password_reset']='Yes';
        }
        if ($_POST['is_blocked'] == 'true') {
            $is_blocked = 1;
            
        } else {
            $is_blocked = 0;
        }
        //echo $is_blocked;exit;
//        foreach ($_POST['subblock'] as $sub) {
//            $subblock = $subblock . $sub . ",";
//        }
//        $subblock = substr($subblock, 0, -1);
//        foreach ($_POST['area'] as $ar) {
//            $area = $area . $ar . ",";
//        }
//        $area = substr($area, 0, -1);


        $timezone = "Asia/Kolkata";
        date_default_timezone_set($timezone);
        $userData = $this->db->get_where("mst_employee", array("id" => $_POST['hdd']))->row_array();

        $master_previous_data = [];
        if(!empty($userData)){
            $role_data = $this->db->get_where("mst_role", array("id" => $userData['role_id']))->row_array();
            $master_previous_data = ['emp_code'=>$userData['emp_code'],'emp_name'=>$userData['emp_name'],'address'=>$userData['emp_address'],
            'contact'=>$userData['emp_contact'],'email'=>$userData['email2'],'active_directory'=>$userData['is_directory']=='1'?'Yes':'No','designation'=>$userData['designation_code'],'role'=>$role_data['role_description'],
            'block_code' => $userData['block_code'], 'Sub_block_code' => $userData['Sub_block_code'],'area_code' => $userData['area_code'],'room_code'=>$userData['room_code'],
            'is_blocked'=>$userData['is_blocked']==1?'Yes':'No','status'=>$userData['is_active']==1?'Active':'Inactive','remark' => $_POST['remark']];
            $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
        }
        //echo '<pre>'; print_r($master_previous_data); print_r($auditArray); exit;
//        if ($userData['emp_password'] != $_POST['pwd']) {
//            $this->db->select("*");
//            $this->db->from("mst_employee_password_history");
//            $this->db->limit(3);
//            $this->db->order_by('id', "DESC");
//            $this->db->where("user_id", $_POST['hdd']);
//            $result = $this->db->get()->result_array();
//            $passArray = array();
//            foreach ($result as $key => $value) {
//                $passArray[] = $value['password'];
//            }
//            if (in_array($_POST['pwd'], $passArray)) {
//                $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Your Password should not match last three password.');
//                $this->Apifunction->response($this->Apifunction->json($response), 200);
//            } else {
//                $this->db->insert("mst_employee_password_history", array("user_id" => $_POST['hdd'], "password" => $_POST['pwd'], "created_by" => $this->session->userdata('user_id')));
//            }
//        }
        if ($this->session->userdata('empemail')) {
            if ($_POST['status'] == 'true') {
                $status = 1;
                $active = 'Active';
            } else {
                $status = 0;
                $active = 'Inactive';
            }
            $email_domain_validate = $this->checkValidMailDomain($_POST['email2']);
            if (!$email_domain_validate) {
                $response = array(STATUS => TRUE, "result" => False, MESSAGE => 'Enter Authorised Domain');
                $this->Apifunction->response($this->Apifunction->json($response), 200);
            }
            $res = $this->Adminmodel->update27_12para_with_1where_and_tbl('mst_employee', 'id', $_POST['hdd'], 'email2', $_POST['email2'], 'emp_address', $_POST['add'], 'emp_contact', $_POST['cont'], 'designation_code', $_POST['desg'], 'Sub_block_code', $subblock, 'area_code', $area, 'block_code', $_POST['block'], 'role_id', $_POST['role'], 'emp_remark', $_POST['remark'], 'is_active', $status, 'modified_by', $this->session->userdata('empname'), 'modified_on', date("Y-m-d H:i:s"), 'room_code', $room_code, "is_directory", $is_directory, "is_blocked", $is_blocked, 'emp_name', $_POST['empname']);
            if ($_POST['is_generate'] == 'true') {
                $password = $this->sendPasswordEmail($_POST['email2'], $_POST['mail_pass'], $_POST['email']);

                $res = $this->Adminmodel->update9_3para_with_1where_and_tbl('mst_employee', 'id', $_POST['hdd'], 'emp_password', $_POST['emp_password'], 'modified_by', $this->session->userdata('empcode'), 'modified_on', date("Y-m-d H:i:s"));
                $this->db->where("id", $_POST['hdd']);
                $this->db->update("mst_employee", array("is_first" => '1'));
            }
            if ($res > 0) {
                //$role_data = $this->db->get_where("mst_role", array("id" => $_POST['role']))->row_array();
                if($userData['role_id']!=$_POST['role']){
                $this->db->delete("pts_mst_user_mgmt",array("user_id"=>$_POST['hdd']));
                if(!empty($new_privilege)){
                 $this->db->insert_batch('pts_mst_user_mgmt', $new_privilege);    
                }
                }
                $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Employee data updated successfully.');
                //$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "emp_code:" . $_POST['code'];
                $uniquefiled2 = "emp_email:" . $_POST['email'];
                $uniquefiled3 = "role_id:" . $_POST['role'];
                //$post = array('emp_code' => $_POST['code'], 'emp_name' => $_POST['empname'], 'email' => $_POST['email2'], 'emp_address' => $_POST['add'], 'emp_contact' => $_POST['cont'], 'user_id' => $_POST['email'],'designation' => $_POST['desg'], 'Sub_block_code' => $subblock, 'area_code' => $area, 'block_code' => $_POST['block'], 'role' => $role_data['role_description'], 'remark' => $_POST['remark'], 'status' => $status, 'modified_by' => $this->session->userdata('empname'), 'modified_on' => date("Y-m-d H:i:s"), 'room_code' => $room_code, "active_directory" => $active_directory,  "status" => $active);
//                if(!empty($user_blocked)){
//                    if($user_blocked=='Unblocked') { $post['unblocked']='Yes';}
//                    if($user_blocked=='Blocked') { $post['blocked']='Yes';}
//                }
//                if($password_reset=='Yes'){$post["password_reset"] = $password_reset;}
                $auditParams = array('command_type' => 'update', 'activity_name' => 'User Configuration', 'type' => 'manage_employee', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_employee', 'primary_id' => $_POST['hdd'], 'post_params' => $auditArray,'master_previous_data'=>$master_previous_data);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */
            } else {
                $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Employee data does not Updated.');
            }
            $this->Apifunction->response($this->Apifunction->json($response), 200);
        } else {
            return redirect(base_url() . 'User/logout');
        }
    }

    public function editemployee($id) {
//                if ($this->session->userdata('empemail') && $this->session->userdata('roleid') == 6) {

        if ($this->session->userdata('empemail')) {
            $role = $this->Adminmodel->get_dtl1("mst_role");
            $block = $this->Adminmodel->getblock("mst_block");
            $empdtl = $this->Adminmodel->get_dtl2("id", $id, "mst_employee");
            $empdtl1 = $empdtl->row_array();
            $area = $this->Adminmodel->get_dtl2("block_code", $empdtl1['block_code'], "mst_area");
            $subblock = $this->Adminmodel->get_deptByBlockcode("block_code", $empdtl1['block_code'], "mst_department");

            //getareabyblock($empdtl1['block_code']);

            $data = array(
                'empdtl' => $empdtl,
                'role' => $role,
                'block' => $block,
                'subblock' => $subblock,
                'area' => $area
            );

            $this->data['the_view_content'] = $this->load->view("template/master/Edit_Employee", ['data' => $data], true);
            $this->load->view('template/master/layout_master', $this->data);
//            $this->load->view('Admin/Edit_Employee', ['data' => $data]);
            //$this->load->view('footer');
        } else {
            return redirect(base_url() . 'User/logout');
        }
    }

    public function Employeelist() {
//                if ($this->session->userdata('empemail') && $this->session->userdata('roleid') == 6) {

        if ($this->session->userdata('empemail')) {
            $empdtl = $this->Adminmodel->get_dtl1("mst_employee");

            $data = array(
                'empdtl' => $empdtl,
            );

            $this->data['the_view_content'] = $this->load->view("template/master/Employee_List", ['data' => $data], true);
            $this->load->view('template/master/layout_master', $this->data);
//            $this->load->view('Admin/Employee_List', ['data' => $data]);
        } else {
            return redirect(base_url() . 'User/logout');
        }
    }

    public function AddEmployee() {
//                if ($this->session->userdata('empemail') && $this->session->userdata('roleid') == 6) {

        if ($this->session->userdata('empemail')) {

            $role = $this->Adminmodel->get_dtl1("mst_role");
            $block = $this->Adminmodel->getblock("mst_block");

            $data = array(
                'role' => $role,
                'block' => $block
            );

//            $this->data['the_view_content'] = $this->load->view("template/master/Add_Employee", ['data' => $data], true);
            $this->data['the_view_content'] = $this->load->view("template/master/user_configuration", ['data' => $data], true);
            $this->load->view('template/master/layout_master', $this->data);
//            $this->load->view('Admin/Add_Employee', ['data' => $data]);
        } else {
            return redirect(base_url() . 'User/logout');
        }
    }

    public function AddNewEmployee() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $block = $_POST['block'];
        $subblock = $_POST['subblock'];
        $subblock = explode(',', $subblock);
        $subblock = array_unique($subblock);
        $subblock = implode(',', $subblock);
        $area = $_POST['area'];
        $room_code = $_POST['room_code'];
        if ($_POST['is_directory'] == 'true') {
            $is_directory = '1';
            $active_directory = 'Yes';
        } else {
            $is_directory = '0';
            $active_directory = 'No';
        }
        if ($_POST['is_generate'] == 'true') {
            $is_generate = '1';
            $password_reset = 'Yes';
        } else {
            $is_generate = '0';
            $password_reset = 'No';
        }
        $role_data = $this->db->get_where("mst_role", array("id" => $_POST['role']))->row_array();
        $auditArray = ['user_id'=>$_POST['email'],'emp_code'=>$_POST['code'],'emp_name'=>$_POST['empname'],'address'=>$_POST['add'],
            'contact'=>$_POST['cont'],'email'=>$_POST['email2'],'password_generated'=>$_POST['is_generate'] == 'true'?'Yes':'No','active_directory'=>$_POST['is_directory']=='true'?'Yes':'No',
            'designation'=>$_POST['desg'],'role'=>$role_data['role_description'],'block_code' => $_POST['block'], 'Sub_block_code' => $subblock, 'area_code' => $area,'room'=>$_POST['room_code'],'status'=>'Active','remark' => $_POST['remark']];
        $auditArray = $this->Ptscommon->getNAUpdated($auditArray);
//        foreach ($_POST['subblock'] as $sub) {
//            $subblock = $subblock . $sub . ",";
//        }
//        $subblock = substr($subblock, 0, -1);
//        foreach ($_POST['area'] as $ar) {
//            $area = $area . $ar . ",";
//        }
//        $area = substr($area, 0, -1);
//        $res = 60;
//        print_r($_POST);exit;
        $email_domain_validate = $this->checkValidMailDomain($_POST['email2']);
        $this->db->where("emp_code", $_POST['code']);

        //$this->db->or_where("email2", $_POST['email2']);
        $empData = $this->db->get_where("mst_employee")->result_array();
        if (!empty($empData)) {
            $response = array(STATUS => TRUE, "result" => False, MESSAGE => 'Employee Code already exist');
            $this->Apifunction->response($this->Apifunction->json($response), 200);
            exit;
        }
        $this->db->where("emp_email", $_POST['email']);
        $empData = $this->db->get_where("mst_employee")->result_array();
        if (!empty($empData)) {
            $response = array(STATUS => TRUE, "result" => False, MESSAGE => 'User id already exist');
            $this->Apifunction->response($this->Apifunction->json($response), 200);
            exitl;
        }
        if (!$email_domain_validate) {
            $response = array(STATUS => TRUE, "result" => False, MESSAGE => 'Enter Authorised Domain');
            $this->Apifunction->response($this->Apifunction->json($response), 200);
        }

        //check if users email exists in directory
        if ($is_directory>0) {
            $userExist = file_get_contents(URL_BASE_TEST_AD_USER_EXISTENCE ."username=".$_POST['email']);
            $userCheck = json_decode($userExist, true);
            $userCheckStatus = ($userCheck['status'] == true || $userCheck['status'] == 1) ? '1' : 0;
            if($userCheckStatus==0){
                $response = array(STATUS => TRUE, "result" => False, MESSAGE => 'User does not exists in active directory');
                $this->Apifunction->response($this->Apifunction->json($response), 200);
            }
        }

        if ($_POST['is_generate'] == 'true') {
            $password = $this->sendPasswordEmail($_POST['email2'], $_POST['mail_pass'], $_POST['email']);
            $res = $this->Adminmodel->insert29para_with_tblnama('mst_employee', 'emp_code', $_POST['code'], 'emp_name', $_POST['empname'], 'email2', $_POST['email2'], 'emp_address', $_POST['add'], 'emp_contact', $_POST['cont'], 'emp_email', $_POST['email'], 'designation_code', $_POST['desg'], 'block_code', $_POST['block'], 'Sub_block_code', $subblock, 'area_code', $area, 'role_id', $_POST['role'], 'emp_remark', $_POST['remark'], 'created_by', $this->session->userdata('empname'), 'room_code', $room_code, "is_directory", $is_directory, "is_generate", $is_generate, "emp_password", $_POST['emp_password']);
            $this->db->where("id", $res);
            $this->db->update("mst_employee", array("is_first" => '1'));
        } else {
            $res = $this->Adminmodel->insert29para_with_tblnama('mst_employee', 'emp_code', $_POST['code'], 'emp_name', $_POST['empname'], 'email2', $_POST['email2'], 'emp_address', $_POST['add'], 'emp_contact', $_POST['cont'], 'emp_email', $_POST['email'], 'designation_code', $_POST['desg'], 'block_code', $_POST['block'], 'Sub_block_code', $subblock, 'area_code', $area, 'role_id', $_POST['role'], 'emp_remark', $_POST['remark'], 'created_by', $this->session->userdata('empname'), 'room_code', $room_code, "is_directory", $is_directory, "is_generate", $is_generate, "emp_password", '');
        }
        if ($res > 0) {
            //$peivilege_data = $this->db->get_where("pts_mst_role_privilege", array("role_id" => $_POST['role']))->result_array();
            $peivilege_data = $this->db->get_where("pts_mst_role_mgmt", array("role_id" => $_POST['role']))->result_array();
//            echo '<pre>';print_r($peivilege_data);exit;
            $room_list = $this->Adminmodel->getroomwiseactivity($res);
//            $room_list = $this->db->get_where("pts_mst_sys_id", array("status" => 'active', "device_id" => '10.240.240.240'))->result_array();
//            echo '<pre>';print_r($room_list);exit;
            $tempArray = array();
            if (!empty($peivilege_data)) {
                foreach ($peivilege_data as $key1 => $value1) {
                    if ($value1['module_type'] == 'log') {
                        if (!empty($room_list)) {
                            foreach ($room_list as $key => $value) {
//                            if ($value1['is_create'] == 1) {
//                                if ($value1['module_id'] == 16 || $value1['module_id'] == 17 || $value1['module_id'] == 30) {
//                                    $type_array = array("Type A Cleaning", "Type B Cleaning", "Type D Cleaning", "Maintenance", "Production", "Sampling");
//                                    $type_id = implode(",", $type_array);
//                                } else {
//                                    $type_id = NULL;
//                                }
//                            }
                                if ($value1['module_id'] == $value['mst_act_id']) {
                                    if ($value1['module_id'] == 16 || $value1['module_id'] == 17 || $value1['module_id'] == 30) {
                                        $type_array = array("Type A Cleaning", "Type B Cleaning", "Type D Cleaning", "Maintenance", "Production", "Sampling");
                                        $type_id = implode(",", $type_array);
                                    } else {
                                        $type_id = NULL;
                                    }
                                    $tempArray[] = array("user_id" => $res, "module_id" => $value1['module_id'], "room_code" => $value['room_code'], "type_id" => $type_id, "module_type" => $value1['module_type'], "is_create" => $value1['is_create'], "is_edit" => $value1['is_edit'], "is_view" => $value1['is_view'], "created_by" => $this->session->userdata('user_id'));
                                }
                            }
                        }
                    } elseif ($value1['module_type'] == 'report') {
                        $tempArray[] = array("user_id" => $res, "module_id" => $value1['module_id'], "room_code" => NULL, "type_id" => NULL, "module_type" => $value1['module_type'], "is_create" => $value1['is_create'], "is_edit" => $value1['is_edit'], "is_view" => $value1['is_view'], "created_by" => $this->session->userdata('user_id'));
//                    if ($value1['access_type'] == 'YES') {
//                        $tempArray[] = array("user_id" => $res, "module_id" => $value1['module_id'], "room_code" => NULL, "type_id" => NULL, "module_type" => $value1['module_type'], "is_create" => 1, "is_edit" => 1, "is_view" => 1, "created_by" => $this->session->userdata('user_id'));
//                    } else {
//                        $tempArray[] = array("user_id" => $res, "module_id" => $value1['module_id'], "room_code" => NULL, "type_id" => NULL, "module_type" => $value1['module_type'], "is_create" => 0, "is_edit" => 0, "is_view" => 0, "created_by" => $this->session->userdata('user_id'));
//                    }
                    } else {
                        $tempArray[] = array("user_id" => $res, "module_id" => $value1['module_id'], "room_code" => NULL, "type_id" => NULL, "module_type" => $value1['module_type'], "is_create" => $value1['is_create'], "is_edit" => $value1['is_edit'], "is_view" => $value1['is_view'], "created_by" => $this->session->userdata('user_id'));
//                    if ($value1['access_type'] == 'YES') {
//                        $tempArray[] = array("user_id" => $res, "module_id" => $value1['module_id'], "room_code" => NULL, "type_id" => NULL, "module_type" => $value1['module_type'], "is_create" => 1, "is_edit" => 1, "is_view" => 1, "created_by" => $this->session->userdata('user_id'));
//                    } else {
//                        $tempArray[] = array("user_id" => $res, "module_id" => $value1['module_id'], "room_code" => NULL, "type_id" => NULL, "module_type" => $value1['module_type'], "is_create" => 0, "is_edit" => 0, "is_view" => 0, "created_by" => $this->session->userdata('user_id'));
//                    }
                    }
                }
            }
//        echo '<pre>';print_r($tempArray);exit;
            if (!empty($tempArray)) {
                $this->db->insert_batch("pts_mst_user_mgmt", $tempArray);
            }

            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Employee data saved successfully.');
            //$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
            /* Start - Insert elog audit histry */
            $uniquefiled1 = "emp_code:" . $_POST['code'];
            $uniquefiled2 = "emp_email:" . $_POST['email'];
            $uniquefiled3 = "role_id:" . $_POST['role'];
            $auditParams = array('command_type' => 'insert', 'activity_name' => 'User Configuration', 'type' => 'manage_employee', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_employee', 'primary_id' => $res, 'post_params' => $auditArray);
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            //echo '<pre>';print_r($auditParams);exit;
            /* End -  Insert elog audit histry */
        } else {
            $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Employee data does not Added.');
        }
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    // Manage Rooms

    public function mstRoom() {
        $this->load->model('Productionmodel');
        $areacode = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_area');
        //$data[] = array('acode' => $areacode);
        $draincode = $this->Productionmodel->get_dtl2('room_code', Null, 'mst_drainpoint');
        $data = array('acode' => $areacode, 'dcode' => $draincode);
        //$dataavaialable = 1;
        $countarea = $areacode->num_rows();
        $countdp = $draincode->num_rows();
        /* if($countarea >0 && $countdp >0)
          {
          $dataavaialable = 1;
          } */
//        $this->data['the_view_content'] = $this->load->view("template/master/mstRoom", ['data' => $data], true);
        $this->data['the_view_content'] = $this->load->view("template/master/room_master", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstRoom', ['data' => $data]);
    }

    public function checkRoomcode() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->checkRoomcode($_POST);
        echo json_encode($response);
    }

    public function mstRoomSubmit() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->mstRoomSubmit($_POST);
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    public function mstRoomview() {
        $this->load->model('Adminmodel');
        $data = $this->Adminmodel->mstRoomview();
        $this->data['the_view_content'] = $this->load->view("template/master/mstRoomview", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstRoomview', ['data' => $data]);
    }

    public function mstRoomviewnow($id) {
        $this->load->model('Adminmodel');
        $this->load->model('Productionmodel');
        $areacode = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_area');
        $draincode = $this->Adminmodel->get_roomeditdrainpoint($id);
        $roomdata = $this->Adminmodel->mstRoomviewnow($id);
        $data = array('acode' => $areacode, 'roomdata' => $roomdata, 'dcode' => $draincode);
        $this->data['the_view_content'] = $this->load->view("template/master/mstRoomviewnow", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstRoomviewnow', ['data' => $data]);
    }

    public function mstRoomupdate() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->mstRoomupdate($_POST);
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    public function deleteRoom() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->deleteRoom($_POST);
        echo json_encode($response);
    }

    public function getarea() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Productionmodel');
        $subblock = $_POST["subblock"];
        $query = "";
        foreach ($subblock as $sb) {
            $query = $query . 'department_code = "' . $sb . '" OR ';
        }
        $query = substr($query, 0, -3);
        $areadtl = $this->Adminmodel->getareabysubblock($query, 'mst_area');
        $response = $areadtl->result_array();
        echo json_encode($response);
        //echo json_encode($query);
    }

    public function checkArealimit() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->checkArealimit($_POST);
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    //Plant Data
    public function mstPlant() {
//        $this->data['the_view_content'] = $this->load->view("template/master/mstPlant", array(), true);
        $this->data['the_view_content'] = $this->load->view("template/master/plant_master", array(), true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstPlant');
    }

    public function checkPlantcode() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->checkPlantcode($_POST);
        echo json_encode($response);
    }

    public function mstPlantSubmit() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->mstPlantSubmit($_POST);
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    public function mstPlantview() {
        $this->load->model('Adminmodel');
        $data = $this->Adminmodel->mstPlantview();
        $this->data['the_view_content'] = $this->load->view("template/master/mstPlantview", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstPlantview', ['data' => $data]);
    }

    public function mstPlantviewnow($id) {
        $this->load->model('Adminmodel');
        $data = $this->Adminmodel->mstPlantviewnow($id);
        $this->data['the_view_content'] = $this->load->view("template/master/mstPlantviewnow", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstPlantviewnow', ['data' => $data]);
    }

    public function mstPlantUpdate() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->mstPlantUpdate($_POST);
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    public function deletePlant() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->deletePlant($_POST);
        echo json_encode($response);
    }

    //Manage sub blockes
    public function mstDepartment() {
        $this->load->model('Productionmodel');
        $blockcode = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_block');
        $data = array('bcode' => $blockcode);
//        $this->data['the_view_content'] = $this->load->view("template/master/mstDepartment", ['data' => $data], true);
        $this->data['the_view_content'] = $this->load->view("template/master/sub_block_master", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstDepartment', ['data' => $data]);
    }

    public function checkDepartmentcode() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->checkDepartmentcode($_POST);
        echo json_encode($response);
    }

    public function checkAreasbyblock() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->checkAreasbyblock($_POST);
        echo json_encode($response);
    }

    public function mstDepartmentSubmit() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->mstDepartmentSubmit($_POST);
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    public function mstDepartmentview() {
        $this->load->model('Adminmodel');
        $data = $this->Adminmodel->mstDepartmentview();
        $this->data['the_view_content'] = $this->load->view("template/master/mstDepartmentview", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstDepartmentview', ['data' => $data]);
    }

    public function mstDepartmentviewnow($id) {
        $this->load->model('Adminmodel');
        $this->load->model('Productionmodel');
        $blockcode = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_block');
        $departmentdata = $this->Adminmodel->mstDepartmentviewnow($id);
        $areacode = $this->Adminmodel->mstGetareanow($departmentdata);
        $data = array('acode' => $areacode, 'bcode' => $blockcode, 'departmentdata' => $departmentdata);
        $this->data['the_view_content'] = $this->load->view("template/master/mstDepartmentviewnow", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstDepartmentviewnow', ['data' => $data]);
    }

    public function mstDepartmentupdate() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->mstDepartmentupdate($_POST);
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    public function deleteDepartment() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->deleteDepartment($_POST);
        echo json_encode($response);
    }

    public function checkBlocklimit() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->checkBlocklimit($_POST);
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    //equipmet form
    public function mstEquipment() {
        $this->load->model('Productionmodel');
        $block = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_block');
        $sopdata = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_sop');
        $data = array('block' => $block, 'sop' => $sopdata);
        $this->data['the_view_content'] = $this->load->view("template/master/equipment_master", ['data' => $data], true);
//        $this->data['the_view_content'] = $this->load->view("template/master/mstEquipment", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstEquipment', ['data' => $data]);
    }

    public function checkEquipmentcode() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->checkEquipmentcode($_POST);
        echo json_encode($response);
    }

    public function mstEquipmentSubmit() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $euipdata = $this->db->get_where("mst_equipment", array("equipment_code" => $_POST["ecode"]))->result_array();
        if (!empty($euipdata)) {
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Equipment Code already exist.');
            $this->Apifunction->response($this->Apifunction->json($response), 200);
            exit;
        }
        $picture = 'default.png';
        if (!empty($_FILES['file']['name'][0])) {
            $target = "img/icons/";
            $file_name = $_FILES['file']['name'][0];
            $file_ext = explode('.', $_FILES['file']['name'][0]);
            $file_size = $_FILES['file']['size'][0];
            $file_type = $_FILES['file']['type'][0];
            if (($file_size > 1048576)) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please Select file size less than 1MB', "res" => array());
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            } else {
                $file_tmp = $_FILES['file']['tmp_name'][0];
                if (move_uploaded_file($file_tmp, $target . $file_name)) {
                    $picture = $file_name;
                } else {
                    $picture = 'default.png';
                }
            }
        }
        if ($_POST["etype"] == "Fixed" || $_POST["etype"] == "Booth") {
            $arr = array("equipment_code" => $_POST["ecode"], "equipment_name" => $_POST["ename"], "equipment_icon" => $picture, "equipment_type" => $_POST["etype"], "equipment_remarks" => $_POST["eremarks"], "block_code" => $_POST["blockcode"], "area_code" => $_POST['areacode'], "room_code" => $_POST["rcode"], "drain_point_code" => "", "sop_code" => $_POST["spcode"], "created_by" => $this->session->userdata('empname'));
        } else {
            $arr = array("equipment_code" => $_POST["ecode"], "equipment_name" => $_POST["ename"], "equipment_icon" => $picture, "equipment_type" => $_POST["etype"], "equipment_remarks" => $_POST["eremarks"], "block_code" => $_POST["blockcode"], "room_code" => '', "area_code" => '', "drain_point_code" => "", "sop_code" => $_POST["spcode"], "created_by" => $this->session->userdata('empname'));
        }
        $auditArray = ["equipment_code" => $_POST["ecode"], "equipment_name" => $_POST["ename"], "equipment_icon" => $picture, "equipment_type" => $_POST["etype"], "block_code" => $_POST["blockcode"], "room_code" => $_POST["rcode"], "area_code" => $_POST['areacode'], "sop_code" => $_POST["spcode"],'status'=>'Active', "remarks" => $_POST["eremarks"]];
        $auditArray = $this->Ptscommon->getNAUpdated($auditArray);
        if ($this->db->insert("mst_equipment", $arr)) {
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Equipment data saved successfully.');

            /* Start - Insert elog audit histry */
            $uniquefiled1 = "equipment_code:" . $_POST["ecode"];
            $uniquefiled2 = "equipment_type:" . $_POST["etype"];
            $uniquefiled3 = "block_code:" . $_POST["blockcode"];
            $inserted_id = $this->db->insert_id();
//            $arr['is_active'] = "1";
//            $arr1 = [];
//            foreach ($arr as $key=>$value){
//                if($value !=''){
//                    $arr1[$key] = $value;
//                }
//            }
            $auditParams = array('command_type' => 'insert', 'activity_name' => 'Equipment Master', 'type' => 'create_equipment', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_equipment', 'primary_id' => $inserted_id, 'post_params' => $auditArray);
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        } else {
            $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Equipment does not Added.');
        }
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    public function mstEquipmentview() {
        $this->load->model('Adminmodel');
        $data = $this->Adminmodel->mstEquipmentview();
        $this->data['the_view_content'] = $this->load->view("template/master/mstEquipmentview", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstEquipmentview', ['data' => $data]);
    }

    public function mstEquipmentviewnow($id) {
        $this->load->model('Adminmodel');
        $this->load->model('Productionmodel');
        $block = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_block');
        $roomcode = $this->Productionmodel->get_dtl2('is_active', '1', 'mst_room');
        $equipmentdata = $this->Adminmodel->mstEquipmentviewnow($id);
        $arealist = $this->Adminmodel->get_detail2('block_code', $equipmentdata['block_code'], 'mst_area');
        $roomlist = $this->Adminmodel->get_detail2('area_code', $equipmentdata['area_code'], 'mst_room');
        //echo "<pre/>";
        //print_r($equipmentdata);
        //exit;
        $data = array('block' => $block, 'arealist' => $arealist, 'roomlist' => $roomlist, 'rcode' => $roomcode, 'equipmentdata' => $equipmentdata);
        $this->data['the_view_content'] = $this->load->view("template/master/mstEquipmentviewnow", ['data' => $data], true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/mstEquipmentviewnow', ['data' => $data]);
    }

    public function deleteEquipment() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->deleteEquipment($_POST);
        echo json_encode($response);
    }

    public function mstEquipmentUpdate() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        date_default_timezone_set('Asia/Kolkata');
        $date = date("Y-m-d H:i:s");
        $this->db->where("workflownextstep>", '-1');
        $logData = $this->db->get_where("pts_trn_user_activity_log", array("equip_code" => $_POST['ecode']))->result_array();
        if (!empty($logData) && $_POST['status'] == 'false') {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'You can not deactivate this.Some Logs are in progress.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            exit;
        }
        $old_data = $this->db->get_where('mst_equipment',["id"=>$_POST["equipmentid"]])->row_array();
        $master_previous_data = [];
        if(!empty($old_data)){
            $master_previous_data = ["equipment_name" => $old_data["equipment_name"], "equipment_icon" => $old_data['equipment_icon'], "equipment_type" => $old_data["equipment_type"], "block_code" => $old_data["block_code"], "room_code" => $old_data["room_code"], "area_code" => $old_data['area_code'], "sop_code" => $old_data["sop_code"],'status'=>$old_data['is_active'] == 1?'Active':'Inactive'];
            $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
        }
        //$this->load->model('Adminmodel');
        //$response = $this->Adminmodel->mstEquipmentUpdate($_POST);
        if (!empty($_FILES['file']['name'][0])) {

            $target = "img/icons/";
            $file_name = $_FILES['file']['name'][0];
            $file_ext = explode('.', $_FILES['file']['name'][0]);
            $file_size = $_FILES['file']['size'][0];
            $file_type = $_FILES['file']['type'][0];
            if (($file_size > 1048576)) {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Please Select file size less than 1MB', "res" => array());
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
                exit;
            } else {
                $file_tmp = $_FILES['file']['tmp_name'][0];
                if (move_uploaded_file($file_tmp, $target . $file_name)) {
                    $picture = $file_name;
                } else {
                    $picture = 'default.png';
                }
            }
            if ($_POST['status'] == 'true') {
                $status = 1;
            } else {
                $status = 0;
            }
            if ($_POST["etype"] == "Fixed" || $_POST["etype"] == "Booth") {
                $arr = array("equipment_name" => $_POST["ename"], "equipment_icon" => $picture, "equipment_type" => $_POST["etype"], "equipment_remarks" => $_POST["eremarks"], "block_code" => $_POST["blockcode"], "room_code" => $_POST["rcode"],"area_code" => $_POST['areacode'], "drain_point_code" => '', "sop_code" => $_POST["spcode"], "modified_by" => $this->session->userdata('empname'), "modified_on" => $date, "is_active" => $status);
            } else {
                $arr = array("equipment_name" => $_POST["ename"], "equipment_icon" => $picture, "equipment_type" => $_POST["etype"], "equipment_remarks" => $_POST["eremarks"], "block_code" => $_POST["blockcode"], "room_code" => $_POST["rcode"], "area_code" => $_POST['areacode'],"drain_point_code" => '', "sop_code" => $_POST["spcode"], "modified_by" => $this->session->userdata('empname'), "modified_on" => $date, "is_active" => $status);
            }
            $auditArray = ["equipment_code" => $_POST["ecode"], "equipment_name" => $_POST["ename"], "equipment_icon" => $picture, "equipment_type" => $_POST["etype"], "block_code" => $_POST["blockcode"], "room_code" => $_POST["rcode"], "area_code" => $_POST['areacode'], "sop_code" => $_POST["spcode"],'status'=>$_POST['status'] == 'true'?'Active':'Inactive', "remarks" => $_POST["eremarks"]];
            $auditArray = $auditArray = $this->Ptscommon->getNAUpdated($auditArray);
            $this->db->where("id", $_POST["equipmentid"]);
            if ($this->db->update("mst_equipment", $arr)) {
                $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Equipment Master Updated Successfuly.');
            } else {
                $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Equipment does not Updated.');
            }
            /* Start - Insert elog audit histry */
            $uniquefiled1 = "equipment_code:" . $_POST["ecode"];
            $uniquefiled2 = "equipment_type:" . $_POST["etype"];
            $uniquefiled3 = "block_code:" . $_POST["blockcode"];
            $inserted_id = $this->db->insert_id();
            $auditParams = array('command_type' => 'update', 'activity_name' => 'Equipment Master', 'type' => 'create_equipment', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_equipment', 'primary_id' => $_POST["equipmentid"], 'post_params' => $auditArray,'master_previous_data'=>$master_previous_data);
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        } else {
            if ($_POST['status'] == 'true') {
                $status = 1;
            } else {
                $status = 0;
            }
            if ($_POST["etype"] == "Fixed" || $_POST["etype"] == "Booth") {
                $arr = array("equipment_name" => $_POST["ename"], "equipment_type" => $_POST["etype"], "equipment_remarks" => $_POST["eremarks"], "block_code" => $_POST["blockcode"], "room_code" => $_POST["rcode"], "area_code" => $_POST['areacode'],"drain_point_code" => '', "sop_code" => $_POST["spcode"], "modified_by" => $this->session->userdata('empname'), "modified_on" => $date, "is_active" => $status);
            } else {
                $arr = array("equipment_name" => $_POST["ename"], "equipment_type" => $_POST["etype"], "equipment_remarks" => $_POST["eremarks"], "block_code" => $_POST["blockcode"], "room_code" => '', "area_code" =>'',"drain_point_code" => '', "sop_code" => $_POST["spcode"], "modified_by" => $this->session->userdata('empname'), "modified_on" => $date, "is_active" => $status);
            }
            $auditArray = ["equipment_code" => $_POST["ecode"], "equipment_name" => $_POST["ename"], "equipment_type" => $_POST["etype"], "block_code" => $_POST["blockcode"], "room_code" => $_POST["rcode"], "area_code" => $_POST['areacode'], "sop_code" => $_POST["spcode"],'status'=>$_POST['status'] == 'true'?'Active':'Inactive', "remarks" => $_POST["eremarks"]];
            $auditArray = $auditArray = $this->Ptscommon->getNAUpdated($auditArray);
            $this->db->where("id", $_POST["equipmentid"]);
            if ($this->db->update("mst_equipment", $arr)) {
                $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Equipment Master Updated Successfuly.');
            } else {
                $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Equipment does not Updated.');
            }

            $arr1 = [];
            foreach ($arr as $key=>$value){
                if($value !=''){
                    $arr1[$key] = $value;
                }
            }
            $arr1['equipment_code']= $_POST["ecode"];
            /* Start - Insert elog audit histry */
            $uniquefiled1 = "equipment_code:" . $_POST["ecode"];
            $uniquefiled2 = "equipment_type:" . $_POST["etype"];
            $uniquefiled3 = "block_code:" . $_POST["blockcode"];
            $inserted_id = $this->db->insert_id();
            $auditParams = array('command_type' => 'update', 'activity_name' => 'Equipment Master', 'type' => 'create_equipment', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_equipment', 'primary_id' => $_POST["equipmentid"], 'post_params' => $auditArray,'master_previous_data'=>$master_previous_data);
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */
        }

        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    public function batchEquipment() {

        $this->data['the_view_content'] = $this->load->view("template/master/uploadEquipment", array(), true);
        $this->load->view('template/master/layout_master', $this->data);
//        $this->load->view('Admin/uploadEquipment');
    }

    public function uploadEquipmentExcel() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);

        $this->load->model('Adminmodel');


        //if ($this->input->post('submit')) {
        //echo "file name is ". $_FILES['uploadFile']['name'];

        $result = 0;
        $path = 'uploads/';
        require_once APPPATH . "/third_party/PHPExcel/Classes/PHPExcel.php";
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'xlsx|xls';
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('uploadFile')) {
            //if (!$this->upload->do_upload($_FILES['uploadFile']['name'])) {

            $error = array('error' => $this->upload->display_errors());
            $response = array("status" => 0);
        } else {
            $data = array('upload_data' => $this->upload->data());
        }
        if (empty($error)) {
            if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
                //$import_xls_file = $_FILES['uploadFile']['name'];
            } else {
                $import_xls_file = 0;
            }
            $inputFileName = $path . $import_xls_file;



            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
                $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $flag = true;
                $i = 0;
                foreach ($allDataInSheet as $value) {
                    if ($flag) {
                        $flag = false;
                        continue;
                    }
                    $this->db->where("is_active", 1);
                    $this->db->where("equipment_code", $value['A']);
                    //$this->db->where("department_code",$subblock);
                    $query = $this->db->get("mst_equipment");
                    if ($query->num_rows() > 0) {
                        $result = $this->Adminmodel->update19_8para_with_1where_and_tbl('mst_equipment', 'equipment_code', $value['A'], 'equipment_name', $value['B'], 'equipment_type', $value['C'], 'equipment_remarks', $value['D'], 'room_code', $value['E'], 'drain_point_code', $value['F'], 'sop_code', $value['G'], 'modified_by', $this->session->userdata('empname'), 'modified_on', date("Y-m-d H:i:s"));
                        /* if($res>0)
                          {
                          //echo "Imported successfully";
                          $response = array("status"=>1);
                          } */
                    } else {

                        $inserdata[$i]['equipment_code'] = $value['A'];
                        $inserdata[$i]['equipment_name'] = $value['B'];
                        $inserdata[$i]['equipment_icon'] = 'default.png';
                        $inserdata[$i]['equipment_type'] = $value['C'];
                        $inserdata[$i]['equipment_remarks'] = $value['D'];
                        $inserdata[$i]['room_code'] = $value['E'];
                        $inserdata[$i]['drain_point_code'] = $value['F'];
                        $inserdata[$i]['sop_code'] = $value['G'];
                        $inserdata[$i]['created_by'] = $this->session->userdata('empname');
                        $i++;
                    }
                }

                //print_r($inserdata);
                unlink($inputFileName);

                if (isset($inserdata)) {
                    $result = $this->Adminmodel->importDataExcel($inserdata, "mst_equipment");
                }

                //echo "result is ".$result;

                if ($result) {
                    //echo "Imported successfully";
                    $response = array("status" => 1);
                } else {
                    //echo "ERROR !";
                    $response = array("status" => 0);
                }
            } catch (Exception $e) {
                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' . $e->getMessage());
            }
        } else {

            echo $error['error'];
            $response = array("status" => 0);
        }


        //}
        echo json_encode($response);
    }

    public function getareabyblockcode() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $data["blockselected"] = $_POST['block'];
        $response = $this->Adminmodel->getareabyblock($data["blockselected"]);
        echo json_encode($response);
    }

    public function getDrainpointlist() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->getDrainpointlist($_POST["str"]);
        echo json_encode($response);
    }

    public function getSoplist() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->getSoplist($_POST["str"]);
        echo json_encode($response);
    }

    public function ForgotPassword() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $timezone = "Asia/Kolkata";
        date_default_timezone_set($timezone);
        $this->db->select("*");
        $this->db->from("mst_employee_password_history");
        $this->db->limit(3);
        $this->db->order_by('id', "DESC");
        $this->db->where("user_id", $_POST['FPhdd']);
        $result = $this->db->get()->result_array();
        $passArray = array();
        foreach ($result as $key => $value) {
            $passArray[] = $value['password'];
        }
        if (in_array($_POST['newpwd'], $passArray)) {
            $response = array("status" => 0, "msg" => "Your Password should not match last three password");
            echo json_encode($response);
            exit;
        }
        if ($this->session->userdata('empemail') && $this->session->userdata('roleid') == 6) {
            $res = $this->Adminmodel->update9_3para_with_1where_and_tbl('mst_employee', 'id', $_POST['FPhdd'], 'emp_password', $_POST['newpwd'], 'modified_by', $this->session->userdata('empname'), 'modified_on', date("Y-m-d H:i:s"));
            if ($res > 0) {
                $response = array("status" => 1);
                //$response = array("status"=>1,"id"=>$response["id"],"msg"=>$msg["message"],"msgheader"=>$msg["msg-header"]);
            } else {
                $response = array("status" => 0, "msg" => "Password Not updated.");
            }
            echo json_encode($response);
        } else {
            return redirect(base_url() . 'User/logout');
        }
    }

    /**
     * Audit Trail Report New
     */
    public function auditTrailReportNew() {
        $this->load->view('template/report/audit_trail_report_new');
    }

    /**
     * Active Inactive User List
     */
    public function userListReport() {
        $this->load->view('template/report/active_inactive_user_list');
    }

    /**
     * Common log report for three logs(process log/line log/portable log)
     */
    public function commonLogReport() {
        $this->load->view('template/report/common_log_report');
    }

    // create xlsx
    public function createXLS() {
        // create file name
        $fileName = 'log_report.xls';
        // load excel library
        require_once APPPATH . "/third_party/PHPExcel/Classes/PHPExcel.php";

        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        $post['start_date'] = isset($_GET['start_date']) ? date('Y-m-d', strtotime($_GET['start_date'])) : '';
        $post['end_date'] = isset($_GET['end_date']) ? date('Y-m-d', strtotime($_GET['end_date'])) : '';
        $res = $this->Ptscommon->GetCommonLogReport($type, $post);

//        $empInfo = $this->export->employeeList();

        $objPHPExcel = new PHPExcel();
        $styleArray = array(
            'font' => array(
                'size' => 10,
        ));
        $objPHPExcel->getDefaultStyle()->applyFromArray($styleArray);
        $objPHPExcel->setActiveSheetIndex(0);
        $styleArray1 = array(
            'font' => array(
                'bold' => true,
                'size' => 10,
        ));

        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Doc. No.');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Block');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Room No.');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Equipment Code/Line Code');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Equipment Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Batch No.');
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Product Code');
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Product Name');
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Activity');
        $objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Start date time');
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'End Date time');
        $objPHPExcel->getActiveSheet()->SetCellValue('L1', 'Operator');
        $objPHPExcel->getActiveSheet()->SetCellValue('M1', 'Operator Date time');
        $objPHPExcel->getActiveSheet()->SetCellValue('N1', 'Checked By');
        $objPHPExcel->getActiveSheet()->SetCellValue('O1', 'Checked Date time');
        $objPHPExcel->getActiveSheet()->SetCellValue('P1', 'Verified by');
        $objPHPExcel->getActiveSheet()->SetCellValue('Q1', 'Verified Date time');
        $objPHPExcel->getActiveSheet()->SetCellValue('R1', 'Remark');
        $objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('K1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('L1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('M1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('N1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('O1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('P1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('Q1')->applyFromArray($styleArray1);
        $objPHPExcel->getActiveSheet()->getStyle('R1')->applyFromArray($styleArray1);

        // set Row
        $rowCount = 2;
        foreach ($res as $element) {
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $element['doc_id']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $element['block_code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $element['room_code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $element['equip_code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $element['equipment_name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $element['batch_no']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $element['product_code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $element['product_name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $element['logname'] . "-" . $element['activity_name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, $element['start_date_time']);
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, $element['stop_date_time']);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $rowCount, (($element['workflow_type'] == 'start' || $element['workflow_type'] == 'stop') && $element['approval_status'] == 'N/A') ? $element['user_name'] : "N/A");
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, (($element['workflow_type'] == 'start' || $element['workflow_type'] == 'stop') && $element['approval_status'] == 'N/A') ? $element['activity_time'] : "N/A");
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $rowCount, (($element['workflow_type'] == 'start' || $element['workflow_type'] == 'stop') && $element['approval_status'] == 'approve') ? $element['user_name'] : "N/A");
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $rowCount, (($element['workflow_type'] == 'start' || $element['workflow_type'] == 'stop') && $element['approval_status'] == 'approve') ? $element['activity_time'] : "N/A");
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $rowCount, 'N/A');
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $rowCount, 'N/A');
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $rowCount, $element['activity_remarks']);
            $rowCount++;
        }
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $fileName . '"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
        exit;
    }

    /**
     * Server Ip Master
     * Created By :Rahul Chauhan
     */
    public function ManageServerIp() {
        //        $this->load->view( 'template/master/manage_stages' );
        //        $this->load->view( 'footer' );
        $this->data['the_view_content'] = $this->load->view('template/master/sf_manage_server_ip', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    /**
     * Get Plant List
     * Created By :Rahul Chauhan
     * Date:13-07-2020
     */
    public function getPlantList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $module = isset($_GET['module']) ? $_GET['module'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $param = array("is_active" => "1");
        if ($module == 'master') {
            $param = array();
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_plant', $param, '', $page);
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = $this->Ptscommon->getRowsCount('default', 'mst_plant', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "plant_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Plant Found', "plant_list" => [], 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Product Master
     * Created By : Bhupendra kumar
     * Date : 14-07-2020
     */
    public function getproductList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $module = isset($_GET['module']) ? $_GET['module'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $param = array("is_active" => "1");
        if ($module == 'master') {
            $param = array();
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_product', $param, '', $page);
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = $this->Ptscommon->getRowsCount('default', 'mst_product', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "product_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No product Found', "product_list" => [], 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /*
     * Get Block List
     * Created By :Rahul Chauhan
     * Date:13-07-2020
     */

    public function getBlockList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $module = isset($_GET['module']) ? $_GET['module'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $param = array("is_active" => "1");
        if ($module == 'master') {
            $param = array();
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_block', $param, '', $page);
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = $this->Ptscommon->getRowsCount('default', 'mst_block', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "block_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Block Found', "block_list" => [], 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Sub Block List
     * Created By :Rahul Chauhan
     * Date:14-07-2020
     */
    public function getSubBlockList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $block_code = isset($_GET['block_code']) ? $_GET['block_code'] : '';
        $block_array = isset($_GET['block_array']) ? $_GET['block_array'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $module = isset($_GET['module']) ? $_GET['module'] : '';
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = 0; //not required in api but to make it consistant
        $param = array("is_active" => "1");
        if ($module == 'master') {
            $param = array();
        }
        if ($block_array != '') {
            $explode = explode(",", $block_array);
            $param = array("is_active" => "1", "block_code" => $explode);
            $res = $this->Ptscommon->getMultipleRowsforDepartment('default', 'mst_department', $param);
        } else {
            if ($block_code != "") {
                $param = array("is_active" => "1", "block_code" => $block_code);
            }
            $res = $this->Ptscommon->getMultipleRows('default', 'mst_department', $param, '', $page);
            $records_per_page = $this->Ptscommon::records_per_page;
            $count = $this->Ptscommon->getRowsCount('default', 'mst_department', $param);
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "department_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Sub Block Found', "department_list" => [], 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Sub Block List
     * Created By :Rahul Chauhan
     * Date:14-07-2020
     */
    public function getAreaList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $module = isset($_GET['module']) ? $_GET['module'] : '';
        $block_code = isset($_GET['block_code']) ? $_GET['block_code'] : '';
        $block_array = isset($_GET['block_array']) ? $_GET['block_array'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $param = array("is_active" => "1");
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = 0; //not reqired but to make it consistant
        if ($module == 'master') {
            $param = array();
        }
        if ($block_array != '') {
            $explode = explode(",", $block_array);
            $param = array("is_active" => "1", "block_code" => $explode);
            $res = $this->Ptscommon->getMultipleRowsforArea('default', 'mst_area', $param);
        } else {
            if ($block_code != "") {
                $param = array("is_active" => "1", "block_code" => $block_code);
            }
            $res = $this->Ptscommon->getMultipleRows('default', 'mst_area', $param, '', $page);
            $records_per_page = $this->Ptscommon::records_per_page;
            $count = $this->Ptscommon->getRowsCount('default', 'mst_area', $param);
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "area_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Area Found', "area_list" => [], 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Check Parent Active/Inactive status
     * Created By :Rahul Chauhan
     * Date:15-07-2020
     */
    public function checkParentStatus() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $table_name = isset($_POST['table_name']) ? $_POST['table_name'] : '';
        $field_name = isset($_POST['field_name']) ? $_POST['field_name'] : '';
        $field_value = isset($_POST['field_value']) ? $_POST['field_value'] : '';
        $param = array($field_name => $field_value);
        $res = $this->Ptscommon->getMultipleRows('default', $table_name, $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "parent_data" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Sub Block Found', "parent_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Check Block Active/Inactive status
     * Created By :Rahul Chauhan
     * Date:25-07-2020
     */
    public function checkBlock() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $table_name = isset($_POST['table_name']) ? $_POST['table_name'] : '';
        $field_name = isset($_POST['field_name']) ? $_POST['field_name'] : '';
        $field_value = isset($_POST['field_value']) ? $_POST['field_value'] : '';
        $param = array($field_name => $field_value);
        $this->db->select("is_active");
        $this->db->where_in($field_name, explode(",", $field_value));
        $data = $this->db->get($table_name)->result_array();
        $active = '1';
        foreach ($data as $key => $value) {
            if ($value['is_active'] == '0') {
                $active = '0';
                break;
            }
        }
        if (!empty($active)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "parent_data" => array("is_active" => $active));
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Sub Block Found', "parent_data" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Room List
     * Created By :Rahul Chauhan
     * Date:15-07-2020
     */
//    public function getRoomList() {
//        $this->load->model('Ptscommon');
//        $headers = apache_request_headers(); // fetch header value
//        $module = isset($_GET['module']) ? $_GET['module'] : '';
//        $area_array = isset($_GET['area_array']) ? $_GET['area_array'] : '';
//        $page = isset($_GET['page']) ? $_GET['page'] : '';
//        $param = array("is_active" => "1");
//        $records_per_page = $this->Ptscommon::records_per_page;
//        $count = 0; //not needed just to make data consistant
//        if ($module == 'master') {
//            $param = array();
//        }
//        if ($area_array != '') {
//            $explode = explode(",", $area_array);
//            $param = array("is_active" => "1", "area_code" => $explode);
//            $res = $this->Ptscommon->getMultipleRowsRoom('default', 'mst_room', $param);
//        } else {
//            $res = $this->Ptscommon->getMultipleRows('default', 'mst_room', $param, '', $page);
//            $records_per_page = $this->Ptscommon::records_per_page;
//            $count = $this->Ptscommon->getRowsCount('default', 'mst_room', $param);
//        }
//        if (!empty($res)) {
//            $responseresult = array(STATUS => TRUE, "result" => TRUE, "room_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
//            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
//        } else {
//            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Room Found', "room_list" => [], 'records_per_page' => $records_per_page, 'total_records' => 0);
//            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
//        }
//    }
    
    
    
    /**
     * New Function with bug resolved
     * Date: 6 May 2021
     */
    public function getRoomList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $module = isset($_GET['module']) ? $_GET['module'] : '';
        $area_array = isset($_GET['area_array']) ? $_GET['area_array'] : '';
        $useFor = $_GET['use_for'] ?? '';
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $param = array("is_active" => "1");
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = 0; //not needed just to make data consistant
        if ($module == 'master') {
            $param = array();
        }
        if ($area_array != '') {
            $explode = explode(",", $area_array);
            $param = array("is_active" => "1", "area_code" => $explode);
            $res = $this->Ptscommon->getMultipleRowsRoom('default', 'mst_room', $param);
        } elseif ($useFor == 'userconfig' && empty($area_array)) {
            $res = [];
        } else {
            $res = $this->Ptscommon->getMultipleRows('default', 'mst_room', $param, '', $page);
            $records_per_page = $this->Ptscommon::records_per_page;
            $count = $this->Ptscommon->getRowsCount('default', 'mst_room', $param);
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "room_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Room Found', "room_list" => [], 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Equipment List
     * Created By :Rahul Chauhan
     * Date:16-07-2020
     */
    public function getEquipmentList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $module = isset($_GET['module']) ? $_GET['module'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $param = array("is_active" => "1");
        if ($module == 'master') {
            $param = array();
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_equipment', $param, '', $page);
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = $this->Ptscommon->getRowsCount('default', 'mst_equipment', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "equipment_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Equipment Found', "equipment_list" => [], 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get User List
     * Created By :Rahul Chauhan
     * Date:16-07-2020
     */
    public function getUserList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $module = isset($_GET['module']) ? $_GET['module'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $param = array("is_active" => "1");
        if ($module == 'master') {
            $param = array();
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'mst_employee', $param, '', $page);
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = $this->Ptscommon->getRowsCount('default', 'mst_employee', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "user_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No User Found', "user_list" => [], 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Role Management
     * Created By :Rahul Chauhan
     * Date:20-07-2020
     */
    public function roleManagement() {
        //        $this->load->view( 'template/master/manage_stages' );
        //        $this->load->view( 'footer' );
        $this->data['the_view_content'] = $this->load->view('template/master/role_management', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    /**
     * Get Role Assigned modules
     * Created By : Rahul Chauhan
     * Date: 20-07-2020
     */
    public function getRoleModuleData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $param['module_type'] = isset($_GET['module_type']) ? $_GET['module_type'] : '';
        $param['role_id'] = isset($_GET['role_id']) ? $_GET['role_id'] : '';
        $this->load->model('Adminmodel');
        $res = $this->Adminmodel->getRoleModuleData($param);
        foreach ($res as $key => $value) {
            if ($res[$key]['is_create'] == 1) {
                $res[$key]['is_create'] = true;
            } else {
                $res[$key]['is_create'] = false;
            }
            if ($res[$key]['is_edit'] == 1) {
                $res[$key]['is_edit'] = true;
            } else {
                $res[$key]['is_edit'] = false;
            }
            if ($res[$key]['is_view'] == 1) {
                $res[$key]['is_view'] = true;
            } else {
                $res[$key]['is_view'] = false;
            }
        }
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "role_previlege_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Master Activity Found', "role_previlege_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Save Role Privileges
     * Created By : Rahul Chauhan
     * Edited By: ****
     * Created Date:20-07-2020
     */
    public function saveRolePrivilege() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $module_type = isset($_POST['module_type']) ? $_POST['module_type'] : '';
        $role_id = isset($_POST['role_id']) ? $_POST['role_id'] : '';
        $module_data = json_decode($_POST['log_data'], true);
        $insertArray = [];
        $roleArray = '';
        $master_logs = $report_logs = '';
        $newPrivilege = [];
        $role_row = $this->db->get("pts_mst_master")->result_array();
        if (!empty($role_row)) {
            foreach ($role_row as $v)
                $role_name[$v['id']] = $v['master_name'];
        }
        $field = 'master_name';
        if($module_type=='master'){
            $field = 'master_name';
        }else{
            $field = 'report_name';
        }
        foreach ($module_data as $key => $value) {
            $add = $edit = $view = $add1 = $edit1 = $view1 = '';
            if (isset($value['is_create']) && $value['is_create'] != '') {
                $is_create = 1;$add = 'Add, ';$add1 = 'Add';
            } else {
                $is_create = 0;
            }
            if (isset($value['is_edit']) && $value['is_edit'] != '') {
                $is_edit = 1;$edit = 'Update, ';$edit1 = 'Update, ';
                if($module_type=='report'){
                    $edit1 = 'View & Print, ';
                }
            } else {
                $is_edit = 0;
            }
            if (isset($value['is_view']) && $value['is_view'] != '') {
                $is_view = 1;$view = 'View, ';$view1 = 'View';
            } else {
                $is_view = 0;
            }

            //$insertArray[] = ["role_id" => $role_id, "module_id" => $value['id'], "module_type" => $module_type, "is_create" => intval($is_create), "is_edit" => intval($is_edit), "is_view" => intval($is_view), "created_by" => $this->session->userdata('user_id'), "status" => "active"];
            //$roleArray.= $value[$field]." : ".$can_create.",".$can_edit.",".$can_view.',';
            if ((isset($value['master_name']) && $value['master_name'] != '') && ((isset($value['is_create']) && $value['is_create'] != '') || (isset($value['is_edit']) && $value['is_edit'] != '') || (isset($value['is_view']) && $value['is_view'] != ''))) {
                $master_logs .= $value[$field] . ': ' . $add . $edit . $view;
            }
            if ((isset($value['report_name']) && $value['report_name'] != '') && ((isset($value['is_create']) && $value['is_create'] != '') || (isset($value['is_edit']) && $value['is_edit'] != '') || (isset($value['is_view']) && $value['is_view'] != ''))) {
                $report_logs .= $value[$field] . ': ' . $add . $edit . $view;
            }
            $newPrivilege[$value['id']] = [0=>$value[$field],1=>$add,2=>$edit1,3=>$view];
            $insertArray[] = ["role_id" => $role_id, "module_id" => $value['id'], "module_type" => $module_type, "is_create" => intval($is_create), "is_edit" => intval($is_edit), "is_view" => intval($is_view), "created_by" => $this->session->userdata('user_id'), "status" => "active"];
        }

        $master_logs = rtrim(rtrim($master_logs, ', '), ', ');
        $report_logs = rtrim(rtrim($report_logs, ', '), ', ');
        $logs1 = !empty($report_logs) ? $report_logs : '';
        $logs = !empty($master_logs) ? $master_logs : $logs1;
        //$roleArray = rtrim($roleArray,',');
        $oldRolesList = '';
        $master_previous_data = [];
        $old_data = $this->Ptscommon->getOldRolesAndResponsibilties($role_id, $module_type);
        $master_previous_data = $oldPrivilege = [];
        $operation = 'insert';
        $old_master_logs = '';
        if (!empty($old_data)) {
            $operation = 'update';
            foreach ($old_data as $key => $value) {
                $add = $edit = $view = $add1 = $edit1 = $view1 = '';
                if (isset($value['is_create']) && $value['is_create'] != '') {
                    $is_create = 1;$add = 'Add, ';$add1 = 'Add';
                } else {
                    $is_create = 0;
                }
                if (isset($value['is_edit']) && $value['is_edit'] != '') {
                    $is_edit = 1;$edit = 'Update, ';$edit1 = 'Update, ';
                    if($module_type=='report'){
                        $edit1 = 'View & Print, ';
                    }
                } else {
                    $is_edit = 0;
                }
                if (isset($value['is_view']) && $value['is_view'] != '') {
                    $is_view = 1;$view = 'View, ';$view1 = 'View';
                } else {
                    $is_view = 0;
                }
                if ((isset($value['log_name']) && $value['log_name'] != '') && ((isset($value['add']) && $value['add'] != '') || (isset($value['edit']) && $value['edit'] != '') || (isset($value['view']) && $value['view'] != ''))) {
                    $old_master_logs .= $value['log_name'] . ': ' . $add . $edit . $view;
                }
                $oldPrivilege[$value['module_id']] = [0=>$value['log_name'],1=>$add,2=>$edit1,3=>$view];
            }
            $old_master_logs = rtrim(rtrim($old_master_logs, ', '), ', ');
            $master_previous_data = ["logs" => $old_master_logs];
            $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
        }//echo '<pre>';print_r($newPrivilege);print_r($newPrivilege);exit;
        $changes = $this->Ptscommon->getUserNewRolesAndResponsibiltiesChanges($newPrivilege, $oldPrivilege);//echo '<pre>';print_r($changes);exit;
        //echo '<pre>';print_r($changes);print_r($newPrivilege);exit;
        $auditArray = ['role_id'=>$role_id,'module_type' => $module_type,"Roles_Assigned"=>$changes];
        $auditArray = $this->Ptscommon->getNAUpdated($auditArray);//echo '<pre>';print_r($auditArray);print_r($operation);exit;
        //$new_data = $this->Ptscommon->getNewRolesAndResponsibiltiesChanges($roleArray, $oldData);
        $this->db->delete("pts_mst_role_mgmt", array("role_id" => $role_id, "module_type" => $module_type));
        $res = $this->db->insert_batch('pts_mst_role_mgmt', $insertArray);
        if ($res) {

            /* Start - Insert elog audit histry */
            $uniquefiled1 = "role_id:" . $role_id;
            $uniquefiled2 = "module_type:" . $module_type;
            $uniquefiled3 = "";
            $inserted_id = $this->db->insert_id();
            if($operation=='insert'){
                $auditParams = array('command_type' => $operation, 'activity_name' => 'User Access', 'type' => 'Roles and Responsibility', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_user_mgmt', 'primary_id' => $inserted_id, 'post_params' => $auditArray);
            }else{
                $auditParams = array('command_type' => $operation, 'activity_name' => 'User Access', 'type' => 'Roles and Responsibility', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_user_mgmt', 'primary_id' => $inserted_id, 'post_params' => $auditArray,'master_previous_data'=>$master_previous_data);
            }
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */

            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'data saved successfully');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'something went wrong.Plese try again.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Get Assigned User Log modules
     * Created By : Rahul Chauhan
     * Date: 20-04-2020
     */
    public function getRoleLogData() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        //$this->getValidateCustomHeader('POST',$headers,false);// validate headers is corrected or not
        //$response=json_decode(file_get_contents('php://input',true));
        $param['module_type'] = isset($_GET['module_type']) ? $_GET['module_type'] : '';
        $param['role_id'] = isset($_GET['role_id']) ? $_GET['role_id'] : '';
        $res = $this->Ptscommon->getRoleModuleData($param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "role_previlege_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Master Activity Found', "role_previlege_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Save Role Logs  Privileges
     * Created By : Rahul Chauhan
     * Edited By: ****
     * Created Date:20-07-2020
     */
    public function saveRoleLogsPrivilege() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $module_type = isset($_POST['module_type']) ? $_POST['module_type'] : '';
        $role_id = isset($_POST['role_id']) ? $_POST['role_id'] : '';
        $log_data = json_decode($_POST['log_data'], true);
        $type_id = 'Type A Cleaning,Type B Cleaning,Type D Cleaning,Maintenance,Production,Sampling';
        $insertArray = array();
        $newRolesAssigned ='';
        foreach ($log_data as $key => $value) {
            if(in_array($value,['16','17','30'] )){
                $insertArray[] = array("role_id" => $role_id, "module_id" => $value['module_id'], "module_type" => $module_type, "type_id" => $type_id, "is_create" => $value['is_create'], "is_edit" => $value['is_edit'], "is_view" => $value['is_view'], "created_by" => $this->session->userdata('user_id'), "status" => "active");
            }else {
                $insertArray[] = array("role_id" => $role_id, "module_id" => $value['module_id'], "module_type" => $module_type, "type_id" => NULL, "is_create" => $value['is_create'], "is_edit" => $value['is_edit'], "is_view" => $value['is_view'], "created_by" => $this->session->userdata('user_id'), "status" => "active");
            }
            if($value['is_create'] ==1 && $value['is_edit']==1 && $value['is_view']){
                $x = !empty($value['type_id'])?' : ':'';
                $newRolesAssigned.= $value['activity_name'].$x.',';
            }
        }
        $newRolesAssigned =  rtrim($newRolesAssigned,',');
        $a = ["role_id" => $role_id, "module_type" => $module_type,'Roles_Assigned'=>$newRolesAssigned];
        $a = $this->Ptscommon->getNAUpdated($a);
        $log_data_old_rights = '';
        $master_previous_data = [];
        $operation = 'insert';
        $oldLogRoleRight = $this->Ptscommon->getLogOldAccessRights($role_id);

        if(!empty($oldLogRoleRight)){
            $operation = 'update';
            foreach($oldLogRoleRight as $key=>$value){
                $x = !empty($value['type_id'])?' : ':'';
                $log_data_old_rights.= $value['activity_name'].$x.',';
            }
            $log_data_old_rights = rtrim($log_data_old_rights,',');
            $master_previous_data = ['Roles_Assigned'=>$log_data_old_rights];
            $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
        }
        $this->db->delete("pts_mst_role_mgmt", array("role_id" => $role_id, "module_type" => $module_type));
        $res = $this->db->insert_batch('pts_mst_role_mgmt', $insertArray);
        if ($res) {

            /* Start - Insert elog audit histry */
            $uniquefiled1 = "role_id:" . $role_id;
            $uniquefiled2 = "module_type:" . $module_type;
            $uniquefiled3 = "";
            $inserted_id = $this->db->insert_id();
            //array_unshift($insertArray, $a);
            if($operation=='insert'){
                $auditParams = array('command_type' => $operation, 'activity_name' => 'User Access', 'type' => 'Roles and Responsibility', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_user_mgmt', 'primary_id' => $inserted_id, 'post_params' => $a);
            }else{
                $auditParams = array('command_type' => $operation, 'activity_name' => 'User Access', 'type' => 'Roles and Responsibility', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_user_mgmt', 'primary_id' => $inserted_id, 'post_params' => $a,'master_previous_data'=>$master_previous_data);
            }
            $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
            /* End -  Insert elog audit histry */

            $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'data saved successfully');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'something went wrong.Plese try again.');
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    /**
     * Role Master
     * Created By :Rahul Chauhan
     * Created date:21-07-2020
     */
    public function roleMaster() {
        $this->data['the_view_content'] = $this->load->view('template/master/role_master', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    // public function saveRole() {
    //     $this->load->model('Ptscommon');
    //     $headers = apache_request_headers(); // fetch header value
    //     $role_name = isset($_POST['role_name']) ? $_POST['role_name'] : '';
    //     $role_level = isset($_POST['role_level']) ? $_POST['role_level'] : '';
    //     $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
    //     $prerole = isset($_POST['prerole']) ? $_POST['prerole'] : '';
    //     $roledata = $this->db->get_where("mst_role", array("role_description" => $role_name))->row_array();
    //     if (!empty($roledata)) {
    //         $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Role Name already exist.');
    //     } else {
    //         if ($prerole > 0) {
    //             $res = $this->Ptscommon->getMultipleRows('default', 'mst_role', array('id > ' => $prerole, 'id != ' => -1, 'id != ' => 0));
    //         }
    //         if (count($res) > 0) {
    //             $array = array("id" => $res[0]["id"], "role_level" => $res[0]["id"], "role_code" => 'Role' . $res[0]["id"], "role_description" => $role_name, "role_remark" => $remark, "created_by" => $this->session->userdata('empname'));
    //             for ($i = count($res) - 1; $i >= 0; $i --) {
    //                 $rolid = $res[$i]["id"] + 1;
    //                 $array2 = array("id" => $rolid, "role_level" => $rolid, "role_code" => 'Role' . $rolid, "role_description" => $res[$i]["role_description"], "role_remark" => $res[$i]["role_remark"], "created_by" => $res[$i]["created_by"]);
    //                 $this->db->update("mst_role", $array2, array("id" => $res[$i]["id"]));
    //                 $this->db->update("mst_roleid_workflow_mapping", array("status_id" => $rolid, "role_id" => $rolid), array("role_id" => $res[$i]["id"]));
    //                 $this->db->update("mst_employee", array("role_id" => $rolid), array("role_id" => $res[$i]["id"]));
    //                 $this->db->update("pts_trn_workflowsteps", array("role_id" => $rolid, "status_id" => $rolid), array("role_id" => $res[$i]["id"]));
    //                 $this->db->update("pts_mst_role_workassign_to_mult_roles", array("workassign_to_roleid" => $rolid), array("workassign_to_roleid" => $res[$i]["id"]));
    //                 $this->db->update("pts_mst_role_workassign_to_mult_roles", array("roleid" => $rolid), array("roleid" => $res[$i]["id"]));
    //                 $this->db->update("pts_mst_role_workassign_to_mult_roles", array("roleid" => $rolid), array("roleid" => $res[$i]["id"]));
    //                 $this->db->update("pts_trn_emp_roomin", array("role_id" => $rolid), array("role_id" => $res[$i]["id"]));
    //                 $this->db->update("pts_trn_batchin", array("role_id" => $rolid), array("role_id" => $res[$i]["id"]));
    //             }
    //             $WfresdistbyActid = $this->db->query('Select distinct activity_id from pts_trn_workflowsteps');
    //             foreach ($WfresdistbyActid->result_array() as $key => $value) {
    //                 $this->db->select("*");
    //                 $this->db->from("pts_trn_workflowsteps");
    //                 $this->db->where("activity_id", $value['activity_id']);
    //                 $WfresbyActid = $this->db->get()->result_array();
    //                 for ($i = 0; $i < count($WfresbyActid) - 1; $i++) {
    //                     $this->db->update("pts_trn_workflowsteps", array("next_step" => $WfresbyActid[$i + 1]['role_id']), array("role_id" => $WfresbyActid[$i]["role_id"], "id" => $WfresbyActid[$i]["id"]));
    //                 }
    //             }
    //             // $wfres = $this->Ptscommon->getMultipleRows('default', 'pts_trn_workflowsteps', array());
    //             // foreach ($wfres as $key => $value) {
    //             //     if($value['next_step'] != 0 && $value['next_step'] != -1)
    //             //     {
    //             //         $this->db->update("pts_trn_workflowsteps", array("next_step" => $wfres[$key+1]['role_id']), array("role_id" => $value['role_id']));
    //             //     }
    //             // }
    //             if ($this->db->insert("mst_role", $array)) {
    //                 $currentroleid = $this->db->insert_id();
    //                 $array2 = array("status_id" => $currentroleid, "role_id" => $currentroleid, "created_by" => $this->session->userdata('empname'));
    //                 $this->db->insert("mst_roleid_workflow_mapping", $array2);
    //                 $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Role data saved successfully.');
    //                 /* Start - Insert elog audit histry */
    //                 $uniquefiled1 = "role_code:" . 'Role' . $role_level;
    //                 $uniquefiled2 = "role_level:" . $role_level;
    //                 $uniquefiled3 = "role_name:" . $role_name;
    //                 $inserted_id = $this->db->insert_id();
    //                 $auditParams = array('command_type' => 'insert', 'activity_name' => 'Role Master', 'type' => 'role_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_block', 'primary_id' => $inserted_id, 'post_params' => $array);
    //                 $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
    //                 /* End -  Insert elog audit histry */
    //             } else {
    //                 $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Role does not Added.');
    //             }
    //         } else {
    //             $array = array("role_description" => $role_name, "role_remark" => $remark, "created_by" => $this->session->userdata('empname'));
    //             if ($this->db->insert("mst_role", $array)) {
    //                 $currentroleid = $this->db->insert_id();
    //                 $this->db->update("mst_role", array("role_level" => $currentroleid, "role_code" => 'Role' . $currentroleid), array("id" => $currentroleid));
    //                 $array2 = array("status_id" => $currentroleid, "role_id" => $currentroleid, "created_by" => $this->session->userdata('empname'));
    //                 $this->db->insert("mst_roleid_workflow_mapping", $array2);
    //                 $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Role data saved successfully.');
    //                 /* Start - Insert elog audit histry */
    //                 $uniquefiled1 = "role_code:" . 'Role' . $role_level;
    //                 $uniquefiled2 = "role_level:" . $role_level;
    //                 $uniquefiled3 = "role_name:" . $role_name;
    //                 $inserted_id = $this->db->insert_id();
    //                 $auditParams = array('command_type' => 'insert', 'activity_name' => 'Role Master', 'type' => 'role_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_block', 'primary_id' => $inserted_id, 'post_params' => $array);
    //                 $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
    //                 /* End -  Insert elog audit histry */
    //             } else {
    //                 $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Role does not Added.');
    //             }
    //         }
    //     }
    //     $this->Apifunction->response($this->Apifunction->json($response), 200);
    // }

    public function saveRole() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $role_name = isset($_POST['role_name']) ? $_POST['role_name'] : '';
        $role_level = isset($_POST['role_level']) ? $_POST['role_level'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $prerole = isset($_POST['prerole']) ? $_POST['prerole'] : '';
        $roledata = $this->db->get_where("mst_role", array("role_description" => $role_name))->row_array();
        if (!empty($roledata)) {
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Role Name already exist.');
        } else {

            if ($prerole > 0) {
                $res = $this->Ptscommon->getMultipleRows('default', 'mst_role', array('id > ' => $prerole, 'id != ' => -1, 'id != ' => 0));
            }
            if (count($res) > 0) {
                $array = array("id" => $res[0]["id"], "role_level" => $res[0]["id"], "role_code" => 'Role' . $res[0]["id"], "role_description" => $role_name, "role_remark" => $remark, "created_by" => $this->session->userdata('empname'));
                for ($i = count($res) - 1; $i >= 0; $i--) {
                    $rolid = $res[$i]["id"] + 1;
                    $array2 = array("id" => $rolid, "role_level" => $rolid, "role_code" => 'Role' . $rolid, "role_description" => $res[$i]["role_description"], "role_remark" => $res[$i]["role_remark"], "created_by" => $res[$i]["created_by"]);
                    $this->db->update("mst_role", $array2, array("id" => $res[$i]["id"]));
                    $this->db->update("mst_roleid_workflow_mapping", array("status_id" => $rolid, "role_id" => $rolid), array("role_id" => $res[$i]["id"]));
                    $this->db->update("mst_employee", array("role_id" => $rolid), array("role_id" => $res[$i]["id"]));
                    $this->db->update("pts_trn_workflowsteps", array("role_id" => $rolid, "status_id" => $rolid), array("role_id" => $res[$i]["id"]));
                    $this->db->update("pts_mst_role_workassign_to_mult_roles", array("workassign_to_roleid" => $rolid), array("workassign_to_roleid" => $res[$i]["id"]));
                    $this->db->update("pts_mst_role_workassign_to_mult_roles", array("roleid" => $rolid), array("roleid" => $res[$i]["id"]));
                    $this->db->update("pts_mst_role_workassign_to_mult_roles", array("roleid" => $rolid), array("roleid" => $res[$i]["id"]));
                    $this->db->update("pts_trn_emp_roomin", array("role_id" => $rolid), array("role_id" => $res[$i]["id"]));
                    $this->db->update("pts_trn_batchin", array("role_id" => $rolid), array("role_id" => $res[$i]["id"]));
                    $this->db->update("pts_trn_user_activity_log", array("workflowstatus" => $rolid), array("workflowstatus" => $res[$i]["id"]));
                    $this->db->update("pts_trn_user_activity_log", array("workflownextstep" => $rolid), array("workflownextstep" => $res[$i]["id"]));
                    $this->db->update("pts_trn_activity_approval_log", array("role_id" => $rolid), array("role_id" => $res[$i]["id"]));
                    $this->db->update("pts_mst_role_mgmt", array("role_id" => $rolid), array("role_id" => $res[$i]["id"]));
                }
                $WfresdistbyActid = $this->db->query('Select distinct activity_id from pts_trn_workflowsteps');
                foreach ($WfresdistbyActid->result_array() as $key => $value) {
                    $this->db->select("*");
                    $this->db->from("pts_trn_workflowsteps");
                    $this->db->where("activity_id", $value['activity_id']);
                    $WfresbyActid = $this->db->get()->result_array();
                    for ($i = 0; $i < count($WfresbyActid) - 1; $i++) {
                        if ($WfresbyActid[$i]['next_step']>0) {
                            $this->db->update("pts_trn_workflowsteps", array("next_step" => $WfresbyActid[$i + 1]['role_id']), array("role_id" => $WfresbyActid[$i]["role_id"], "id" => $WfresbyActid[$i]["id"]));
                        }
                    }
                }
                // $wfres = $this->Ptscommon->getMultipleRows('default', 'pts_trn_workflowsteps', array());
                // foreach ($wfres as $key => $value) {
                //     if($value['next_step'] != 0 && $value['next_step'] != -1)
                //     {
                //         $this->db->update("pts_trn_workflowsteps", array("next_step" => $wfres[$key+1]['role_id']), array("role_id" => $value['role_id']));
                //     }
                // }


                if ($this->db->insert("mst_role", $array)) {
                    $currentroleid = $this->db->insert_id();
                    $array2 = array("status_id" => $currentroleid, "role_id" => $currentroleid, "created_by" => $this->session->userdata('empname'));
                    $this->db->insert("mst_roleid_workflow_mapping", $array2);
                    $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Role data saved successfully.');
                    /* Start - Insert elog audit histry */
                    $uniquefiled1 = "role_code:" . 'Role' . $role_level;
                    $uniquefiled2 = "role_level:" . $role_level;
                    $uniquefiled3 = "role_name:" . $role_name;
                    $inserted_id = $this->db->insert_id();
                    $array = ["role_name" => $role_name,'status'=>'Active', "remarks" => $remark];
                    $array = $this->Ptscommon->getNAUpdated($array);
                    $auditParams = array('command_type' => 'insert', 'activity_name' => 'Role Master', 'type' => 'role_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_role', 'primary_id' => $currentroleid, 'post_params' => $array);
                    $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                    /* End -  Insert elog audit histry */
                } else {
                    $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Role does not Added.');
                }
            } else {
                $array = array("role_description" => $role_name, "role_remark" => $remark, "created_by" => $this->session->userdata('empname'));
                if ($this->db->insert("mst_role", $array)) {
                    $currentroleid = $this->db->insert_id();
                    $this->db->update("mst_role", array("role_level" => $currentroleid, "role_code" => 'Role' . $currentroleid), array("id" => $currentroleid));
                    $array2 = array("status_id" => $currentroleid, "role_id" => $currentroleid, "created_by" => $this->session->userdata('empname'));
                    $this->db->insert("mst_roleid_workflow_mapping", $array2);
                    $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Role data saved successfully.');
                    /* Start - Insert elog audit histry */
                    $uniquefiled1 = "role_code:" . 'Role' . $role_level;
                    $uniquefiled2 = "role_level:" . $role_level;
                    $uniquefiled3 = "role_name:" . $role_name;
                    $inserted_id = $this->db->insert_id();
                    $array = ["role_name" => $role_name, 'status'=>'Active', "remarks" => $remark];
                    $array = $this->Ptscommon->getNAUpdated($array);
                    $auditParams = array('command_type' => 'insert', 'activity_name' => 'Role Master', 'type' => 'role_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_role', 'primary_id' => $currentroleid, 'post_params' => $array);
                    $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                    /* End -  Insert elog audit histry */
                } else {
                    $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Role does not Added.');
                }
            }
        }
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    public function updateRole() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $role_name = isset($_POST['role_name']) ? $_POST['role_name'] : '';
        $role_level = isset($_POST['role_level']) ? $_POST['role_level'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $role_id = isset($_POST['role_id']) ? $_POST['role_id'] : '';
        if ($_POST['status'] == 'true') {
            $is_active = 1;
            $status = 'Active';
        } else {
            $is_active = 0;
            $status = 'Inactive';
        }
        $audit_array = ['role_name'=>$role_name,'status'=>$status,'remarks'=>$remark];
        $master_previous_data = [];
        $old_data = $this->db->get_where('mst_role',['id='=>$role_id])->row_array();
        if(empty($old_data)){
            return $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Role Does Not Exist.');
        }else{
            $master_previous_data = ['status'=>$old_data['is_active']==0?'Inactive':'Active'];
        }
        $employeeData = $this->db->get_where("mst_employee", array("role_id" => $role_id, "is_active" => 1))->row_array();
        $workFlowData = $this->db->get_where("pts_trn_workflowsteps", array("role_id" => $role_id, "is_active" => 1))->row_array();
        if ($_POST['status'] == 'false' && (!empty($employeeData) || !empty($workFlowData))) {
            $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'User/Work Flow  exist on this Role Name.');
        } else {
            $array = array("role_level" => $role_level, "role_code" => 'Role' . $role_level, "role_description" => $role_name, "role_remark" => $remark, "modified_by" => $this->session->userdata('empname'), "modified_on" => date("Y-m-d H:i:s"), "is_active" => $is_active);
            $this->db->where("id", $role_id);
            if ($this->db->update("mst_role", $array)) {
                $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Role data updated successfully.');
                /* Start - Insert elog audit histry */
                $uniquefiled1 = "role_code:" . 'Role' . $role_level;
                $uniquefiled2 = "role_level:" . $role_level;
                $uniquefiled3 = "role_name:" . $role_name;
                $inserted_id = $this->db->insert_id();
                $auditParams = array('command_type' => 'update', 'activity_name' => 'Role Master', 'type' => 'role_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'mst_role', 'primary_id' => $role_id, 'post_params' => $audit_array,"master_previous_data"=>$master_previous_data);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */
            } else {
                $response = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Role does not updated.');
            }
        }
        $this->Apifunction->response($this->Apifunction->json($response), 200);
    }

    /**
     * Master Report New
     */
    public function masterTrailReport() {//master_trail_report_new
        $this->load->view('template/report/master_trail_report_new');
    }

    /**
     * Master Form Data  Page
     * Created By :Nitin Mittal
     * Created date:24-07-2020
     */
    public function ManageMaster() {
        $this->load->view('template/report/master_trail_report_new');
    }

    public function sendPasswordEmail($email, $password, $empid) {
        $msg = '<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
   <head>
      <meta charset="utf-8">
      <title>Password</title>
      <link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,400;0,700;1,300;1,400&display=swap" rel="stylesheet">
   </head>
   <body width="100%" style="margin: 0; padding: 30px 0 !important; background-color: #fff; font-family: Lato, sans-serif;">
      <left>
         <table width="600px" border="0" cellspacing="0" cellpadding="0" style="background-color: #fff; padding: 10px 20px; border-collapse: separate;
            border-spacing: 0px; border: 1px solid #ccc; border-radius: 10px;margin-left:40px">
            <tr>
               <td style="font-size:18px; font-weight:bold; text-align:center;">eLogBook</td>
            </tr>
            <tr>
               <td style="height: 10px; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 1px; background-color: #efefef; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Dear Sir/Maam</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>User Id <b>' . $empid . '</b>.</td>
            </tr>
            <tr>
               <td>Your First Time password is <b>' . $password . '</b>.</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;">Please login and Reset your Password</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"><a href="' . base_url() . 'login">Reset Password</a></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Regards</td>
            </tr>
            <tr>
               <td style="height: 10px; width: 100%;"></td>
            </tr>
            <tr>
               <td>Admin</td>
            </tr>
            <tr>
               <td style="height: 20px; width: 100%;"></td>
            </tr>
         </table>
      </center>
   </body>
</html>';

        $subject = "Password";
        $from = 'elogadmin@sunpharma.com';
        $this->load->library('email', MAIL_CONFIG);
        $this->email->set_newline("\r\n");
        $this->email->from(MAIL_FROM); // change it to yours
        $this->email->to($email); // change it to yours
        //$this->email->to('Poonam.Thakur@smhs.motherson.com'); // change it to yours
        $this->email->subject($subject);
        $this->email->message($msg);
        $this->email->set_mailtype(SMTP_MAIL_TYPE);
        if ($this->email->send()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * User Status Report
     * Used to display user status report
     */
    public function UserStatusReportView() {
        //echo "hi";
        $this->load->view('template/report/user_status_report');
    }

    /**
     * user status report function
     * Created By :Meghna
     */
    public function getuserstatusdata() {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        $this->load->model('Adminmodel');
        $response = $this->Adminmodel->getuserstatusdata($_POST);
        echo json_encode($response);
    }

    /**
     * Email Domain Check
     * check email domain is allowed on not
     * @param email
     * return type bool
     * Created By :Nitin
     */
    public function checkValidMailDomain($email) {
        $this->load->model('Ptscommon');
        if (!empty($email)) {
            $domain_name = substr(strrchr($email, "@"), 1);
            $param = ["domain_name" => $domain_name, 'is_active' => 1];
            $res = $this->Ptscommon->getMultipleRows('default', 'mst_valid_domains', $param);
            if (!empty($res)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    
    /**
     * Tablet Tooling Master Page
     * Created By :Rahul Chauhan
     */
    public function TabletToolingMaster() {
        //        $this->load->view( 'template/master/tablet_tooling_master' );
        //        $this->load->view( 'footer' );
        $this->data['the_view_content'] = $this->load->view('template/master/tablet_tooling', array(), true);
        $this->load->view('template/master/layout_master', $this->data);
    }

    /**
     * Add Tablet Tooling Log Master
     * Created By :Rahul Chauhan
     * Date:05-02-2020
     */
    public function AddTabletTooling() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $recordid = isset($_POST['recordid']) ? $_POST['recordid'] : '';
        $product_code = isset($_POST['product_code']) ? $_POST['product_code'] : '';
        $no_of_subset = isset($_POST['no_of_subset']) ? $_POST['no_of_subset'] : '';
        $supplier = isset($_POST['supplier']) ? $_POST['supplier'] : '';
        $year = isset($_POST['year']) ? $_POST['year'] : '';
        $shape = isset($_POST['shape']) ? $_POST['shape'] : '';
        $upper_punch = isset($_POST['upper_punch']) ? $_POST['upper_punch'] : '';
        $upper_embossing = isset($_POST['upper_embossing']) ? $_POST['upper_embossing'] : '';
        $lower_punch = isset($_POST['lower_punch']) ? $_POST['lower_punch'] : '';
        $lower_embossing = isset($_POST['lower_embossing']) ? $_POST['lower_embossing'] : '';
        $die_quantity = isset($_POST['die_quantity']) ? $_POST['die_quantity'] : '';
        $machine = isset($_POST['machine']) ? $_POST['machine'] : '';
        $remark = isset($_POST['remark']) ? $_POST['remark'] : '';
        $product_desc = $this->db->get_where('mst_product',['product_code='=>$product_code])->row_array();
        $product_name = !empty($product_desc)?$product_desc['product_name']:'N/A';
        $tablet_array = ["product_code" => $product_code, "no_of_subset" => $no_of_subset, "supplier" => $supplier, "year" => $year, "shape" => $shape, "upper_punch" => $upper_punch, "upper_embossing" => $upper_embossing, "lower_punch" => $lower_punch, "lower_embossing" => $lower_embossing, "die_quantity" => $die_quantity, "machine" => $machine, "remark" => $remark];
        $tablet_array_audit = ["product_code" => $product_code, "punch_set" => $no_of_subset, "supplier" => $supplier, "year" => $year, "shape" => $shape, "upper_punch" => $upper_punch, "upper_embossing" => $upper_embossing, "lower_punch" => $lower_punch, "lower_embossing" => $lower_embossing, "die_quantity" => $die_quantity, "machine" => $machine];
        if ($recordid > 0) {
            if ($_POST['status'] == 'true') {
                $tablet_array['status'] = 'active';
                $tablet_array_audit['status'] = 'Active';
            } else {
                $tablet_array['status'] = 'inactive';
                $tablet_array_audit['status'] = 'Inactive';
            }
            $tablet_array_audit["remark"] = $remark;
            $tablet_array_audit = $this->Ptscommon->getNAUpdated($tablet_array_audit);
            $tablet_array['modified_on'] = date('Y-m-d H:i:s');
            $tablet_array['modified_by'] = $this->session->userdata('empname');
            $res = $recordid;
            $master_previous_data = [];
            $old_data = $this->db->get_where('pts_mst_tablet_tooling',['id='=>$recordid])->row_array();
            if(empty($old_data)){
                return $response = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Table Tooling Does Not Exist.');
            }else{
                $master_previous_data = [
                    'supplier'=>$old_data['supplier'],
                    'upper_punch'=>$old_data['upper_punch'],
                    'upper_embossing'=>$old_data['upper_embossing'],
                    'lower_punch'=>$old_data['lower_punch'],
                    'lower_embossing'=>$old_data['lower_embossing'],
                    'year'=>$old_data['year'],
                    'dimension'=>$old_data['dimension'],
                    'shape'=>$old_data['shape'],
                    'machine'=>$old_data['machine'],
                    'die_quantity'=>$old_data['die_quantity'],
                    'status'=>$old_data['status']=='active'?'Active':'Inactive'
                ];
                $master_previous_data = $this->Ptscommon->getNAUpdated($master_previous_data);
            }
            $this->db->where("id", $recordid);
            $this->db->update("pts_mst_tablet_tooling", $tablet_array);
            if (!empty($res)) {

                /* Start - Insert elog audit histry */
                $uniquefiled1 = "product_code:" . $product_code;
                $uniquefiled2 = "";
                $uniquefiled3 = "";
                $auditParams = ['command_type' => 'update', 'activity_name' => 'Tablet Tooling Master', 'type' => 'tablet_tooling_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_tablet_tooling_log', 'primary_id' => $res, 'post_params' => $tablet_array_audit,"master_previous_data"=>$master_previous_data];
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Tablet Tooling Master Updated Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Tablet Tooling Master does not Updated');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        } else {
            $tablet_array['created_by'] = $this->session->userdata('empname');
            $res = $this->db->insert("pts_mst_tablet_tooling", $tablet_array);
            if (!empty($res)) {
                /* Start - Insert elog audit histry */
                $tablet_array['status'] = 'active';
                $uniquefiled1 = "product_code:" . $product_code;
                $uniquefiled2 = "";
                $uniquefiled3 = "";
                $inserted_id = $this->db->insert_id();
                $tablet_array_audit["product_name"]=$product_name;
                $tablet_array_audit['status'] = 'Active';
                $tablet_array_audit["remark"] = $remark;
                $tablet_array_audit = $this->Ptscommon->getNAUpdated($tablet_array_audit);
                $auditParams = array('command_type' => 'insert', 'activity_name' => 'Tablet Tooling Master', 'type' => 'tablet_tooling_master', 'uniquefiled1' => $uniquefiled1, 'uniquefiled2' => $uniquefiled2, 'uniquefiled3' => $uniquefiled3, 'table_name' => 'pts_mst_tablet_tooling_log', 'primary_id' => $inserted_id, 'post_params' => $tablet_array_audit);
                $this->Ptscommon->commonFunctionToAddAuditTrail($auditParams);
                /* End -  Insert elog audit histry */

                $responseresult = array(STATUS => TRUE, "result" => TRUE, MESSAGE => 'Tablet Tooling Master Data Added Successfully');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            } else {
                $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'Tablet Tooling Master Data does not Added');
                $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
            }
        }
    }

    /**
     * Get Tablet Tooling Log Card List
     * Created By :Rahul Chauhan
     * Date:05-02-2020
     */
    public function GetTabletToolingLogList() {
        $this->load->model('Ptscommon');
        $headers = apache_request_headers(); // fetch header value
        $product_code = isset($_GET['product_code']) ? $_GET['product_code'] : '';
        $page = isset($_GET['page']) ? $_GET['page'] : '';
        $module = isset($_GET['module']) ? $_GET['module'] : '';
        $param = array("is_active" => "1");

        $param = array("status" => "active");
        if ($product_code != '') {
            $param = array("status" => "active", "product_code" => $product_code);
        }
        if ($module == 'master') {
            $param = array();
        }
        $res = $this->Ptscommon->getMultipleRows('default', 'pts_mst_tablet_tooling', $param, $order = null, $page);
        $records_per_page = $this->Ptscommon::records_per_page;
        $count = $this->Ptscommon->getRowsCount('default', 'pts_mst_tablet_tooling', $param);
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "tablet_list" => $res, 'records_per_page' => $records_per_page, 'total_records' => $count);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Tablet Tooling Log Found', "tablet_list" => array(), 'records_per_page' => $records_per_page, 'total_records' => 0);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

    //Get Room wise Block Data
    public function getRoomwiseBlockList() {
        $this->db->select("mst_block.block_code,mst_block.block_name,mst_room.room_code,mst_room.room_name");
        $this->db->from("mst_block");
        $this->db->join("mst_department", "mst_department.block_code=mst_block.block_code");
        $this->db->join("mst_area", "mst_area.department_code=mst_department.department_code");
        $this->db->join("mst_room", "mst_room.area_code=mst_area.area_code");
        $res = $this->db->get()->result_array();
        if (!empty($res)) {
            $responseresult = array(STATUS => TRUE, "result" => TRUE, "block_list" => $res);
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        } else {
            $responseresult = array(STATUS => TRUE, "result" => FALSE, MESSAGE => 'No Tablet Tooling Log Found', "block_list" => array());
            $this->Apifunction->response($this->Apifunction->json($responseresult), 200);
        }
    }

	 
	 
	 /**
     * Punch Set Allocation Report
     * Created By :Mrinal
     */
    public function PunchSetAllocationReport() {
        $this->load->view('template/report/punch_set_allocation_report');
    }

}
