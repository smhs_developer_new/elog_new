<?php

class Report extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('empemail')) {
            return redirect("login");
        }
        if (!isset($_SERVER['HTTP_REFERER'])) {
            return redirect("login?err=1");
            //$this->load->view('login');
        }
    }

    public function index()
    {
        $empblock_code = $this->session->userdata('empblock_code');
        $this->load->model('Productionmodel');
        $area = $this->Productionmodel->get_dtl2('block_code', $empblock_code, 'mst_area');

        $data = array('area' => $area);

        // header menu code
        $result = $this->Usermodel->getActivitytype();
        $this->load->view('header', ['result' => $result]);
        // End header menu code
        $this->load->view('Report', ['data' => $data]);
        $this->load->view('footer');
    }

    public function Reports()
    {
        // header menu code
        $result = $this->Usermodel->getActivitytype();
        $this->load->view('header', ['result' => $result]);
        // End header menu code
        $this->load->view('Reports');
        $this->load->view('footer');
    }

    public function StatusReport()
    {
        $empblock_code = $this->session->userdata('empblock_code');
        $this->load->model('Productionmodel');
        $area = $this->Productionmodel->get_dtl2('block_code', $empblock_code, 'mst_area');
        $Status = $this->Productionmodel->get_dtl1('mst_status');

        $data = array('area' => $area, 'status' => $Status);

        // header menu code
        $result = $this->Usermodel->getActivitytype();
        $this->load->view('header', ['result' => $result]);
        // End header menu code
        $this->load->view('StatusReport', ['data' => $data]);
        $this->load->view('footer');
    }

    public function getRoom()
    {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        if (isset($_POST['areacode'])) {$area_code = $_POST["areacode"];} else { $area_code = "";}
        $this->load->model('Productionmodel');
        $room = $this->Productionmodel->get_dtl2('area_code', $area_code, 'mst_room');
        echo json_encode($room->result_array());
    }

    public function getdata()
    {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        if (isset($_POST['time'])) {$time = $_POST["time"];} else { $time = "";}
        if (isset($_POST['area_code'])) {$area_code = $_POST["area_code"];} else { $area_code = "";}
        if (isset($_POST['room_code'])) {$room_code = $_POST["room_code"];} else { $room_code = "";}
        $this->load->model('Reportmodel');
        $sqdata = $this->Reportmodel->getdata($time, $area_code, $room_code);

        echo json_encode($sqdata);
    }
    public function getcustomdata()
    {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        if (isset($_POST['fromtime'])) {$fromtime = $_POST["fromtime"];} else { $fromtime = "";}
        if (isset($_POST['totime'])) {$totime = $_POST["totime"];} else { $totime = "";}
        if (isset($_POST['area_code'])) {$area_code = $_POST["area_code"];} else { $area_code = "";}
        if (isset($_POST['room_code'])) {$room_code = $_POST["room_code"];} else { $room_code = "";}
        $this->load->model('Reportmodel');
        $sqdata = $this->Reportmodel->getcustomdata($fromtime, $totime, $area_code, $room_code);

        echo json_encode($sqdata);
    }
    public function getstatusreport()
    {
        header('Access-Control-Allow-Origin: *');
        header("Content-Type: application/json", true);
        if (isset($_POST['status'])) {$status = $_POST["status"];} else { $status = "";}
        if (isset($_POST['time'])) {$time = $_POST["time"];} else { $time = "";}
        if (isset($_POST['area_code'])) {$area_code = $_POST["area_code"];} else { $area_code = "";}
        if (isset($_POST['room_code'])) {$room_code = $_POST["room_code"];} else { $room_code = "";}
        $this->load->model('Reportmodel');
        $sqdata = $this->Reportmodel->getstatusreport($time, $area_code, $room_code, $status);

        echo json_encode($sqdata);
    }
}
